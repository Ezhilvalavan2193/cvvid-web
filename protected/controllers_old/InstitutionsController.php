<?php 
use Stripe\Charge;
use Stripe\Invoice;
use Stripe\InvoiceItem;
use Stripe\Stripe;
use Stripe\Token;
use Stripe\Customer;
use Stripe\Plan;
use Stripe\Subscription;

class InstitutionsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/admin';
	public $institutionid = 0;
	public $edit = 0;
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
// 			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
		
		);
	}

	public function beforeAction($action)
	{
	    Yii::app()->user->setReturnUrl(Yii::app()->request->getUrl());
	    $requestUrl=Yii::app()->request->url;
	    if (Yii::app()->user->isGuest)
	        $this->redirect(Yii::app()->createUrl('site/login'));
	    else if( Yii::app()->user->getState('role') == "admin") 
	        $this->layout = "admin";
	    else 
	        $this->layout = "main";   
	    
	        return parent::beforeAction($action);
	} 
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Institutions;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Institutions']))
		{
			$model->attributes=$_POST['Institutions'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Institutions']))
		{
			$model->attributes=$_POST['Institutions'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Institutions');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Institutions('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Institutions']))
			$model->attributes=$_GET['Institutions'];

		$fullname = "";
		$namesearch = 0;
		$userarr = array();
		if(isset($_GET['fullname']))
		{
		    $namesearch = 1;
		    $fullname = $_GET['fullname'];
		    $cri = new CDbCriteria();
		    $cri->alias = 'i';		    
		    $cri->join = "inner join cv16_users u on u.id = i.admin_id";
		    $cri->condition = "concat_ws(' ',u.forenames,u.surname) like '%".$fullname."%'";
		    $searchusers = Institutions::model()->findAll($cri);
		    $userarr = array();
		    foreach ($searchusers as $users)
		        $userarr[] = $users['admin_id'];
		        
		}
		
		$this->render('admin',array(
		    'model'=>$model,'namesearch'=>$namesearch,'namearray'=>$userarr,'fullname'=>$fullname
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Institutions the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Institutions::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Institutions $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='institutions-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actioneditDetails($id)
	{
            
	    $model = Institutions::model()->findByPk($id);
	    $usermodel = Users::model()->findByPk($model->admin_id);
            $planmodel = new Plans(); 
	    $this->render('editpage',array(
	        'model'=>$model,'planmodel'=>$planmodel,'usermodel'=>$usermodel
	    ));
	}
        
        public function actionapprove($id)
	{
            
	    $model = Institutions::model()->findByPk($id);
	    $usermodel = Users::model()->findByPk($model->admin_id);
            $planmodel = new Plans(); 
	    $this->render('approve',array(
	        'model'=>$model,'planmodel'=>$planmodel,'usermodel'=>$usermodel
	    ));
	}
        
        /**
          * Displays a particular model.
          * @param integer $id the ID of the model to be displayed
          */
         public function actionupgrade() {
             $this->layout = 'main';
             $this->render('upgrade');
         }
	
	public function actionsaveSubscription($id)
	{
            require Yii::app()->basePath . '/extensions/stripe/init.php';
            Stripe::setApiKey(Yii::app()->params['SECRET_Key']);
	    
	    $price = $_POST['Plans']['price'];
	    $interval = $_POST['interval'];
	    $expiry = $_POST['active_to'];
	    $d = str_replace('/', '-', $expiry);
            $expiry_date = date('Y-m-d', strtotime($d));
	    $now = new DateTime();
            
            $planmodel = new Plans();
            
            $planmodel->attributes = $_POST['Plans'];
            $planval = $planmodel->validate();
            
            $instmodel = Institutions::model()->findByPk($id);
            
            if ($planval) {
                
                $instmodel->status = 'awaiting_payment';
                $instmodel->save();
	   
                $planmodel->owner_type = "Institution";
                $planmodel->owner_id = $id;
                $planmodel->name = $instmodel->name;
                $planmodel->stripe_plan = strtolower(str_replace(' ', '_', $instmodel->name));
                $planmodel->quantity = $instmodel->num_pupils;
                $planmodel->price = $price;
                $planmodel->interval = $interval;
                $planmodel->interval_count = 1;
                $planmodel->active_to = $expiry_date;
                $planmodel->created_at = $now->format('Y-m-d H:i:s');
                if($planmodel->save()){
                    
                    $planid = str_replace(' ', '_', $instmodel->name);

                    $plan = Plan::create(array(
                                "amount" => $_POST['Plans']['price'],
                                "interval" => $_POST['interval'],
                                    "product" => array(
                                        "name" => $instmodel->name
                                    ),
                               // "name" => $instmodel->name,
                                "currency" => "gbp",
                                "id" => $planid
                    ));
                    
                    Institutions::model()->updateByPk($id, array('stripe_plan'=>$planid,'stripe_active'=>1));
                    
                    Yii::app()->user->setFlash('success', ' Successfully updated institution');
                    $this->redirect($this->createUrl('institutions/overview'));
                    
                }

            }
	    $usermodel = Users::model()->findByPk($instmodel->admin_id);
            $this->render('editpage',array(
	        'model'=>$instmodel,'planmodel'=>$planmodel,'usermodel'=>$usermodel
	    ));
	}
        
        public function actionsaveStatus($id)
	{
            $instmodel = Institutions::model()->findByPk($id);
	    $status = $_POST['status'];
	    $instmodel->status = $status;
            $instmodel->save();
            
            Yii::app()->user->setFlash('success', ' Successfully updated institution');
            $this->redirect($this->createUrl('institutions/tabeditinstitute',array('id'=>$instmodel->id)));
            
	}
        
        public function actionsaveApprove($id)
	{
            $instmodel = Institutions::model()->findByPk($id);
            $usermodel = Users::model()->findByPk($instmodel->admin_id);
            
    	    $status = $_POST['status'];
                
    	    $instmodel->status = $status;
            $instmodel->save(false);
            
            $usermodel->status = $status;
            $usermodel->save(false);
            
            Yii::app()->user->setFlash('success', ' Successfully Approved institution');
            $this->redirect($this->createUrl('institutions/pending',array('id'=>$instmodel->id)));
            
	}
        
        
       public function actionupdatequantity()
	{
	  
            $subscriber = Institutions::model()->findByAttributes(array('admin_id'=>Yii::app()->user->getState('userid')));
            $student_cnt = TutorGroupsStudents::model()->countByAttributes(array('institution_id' => $subscriber->id));
            if($_POST['quantity'] > $subscriber->num_pupils){
                
                $model = InstitutionUpgrade::model()->findByAttributes(array('id'=>$subscriber->id));
                if($model == null)
                {
                    $quantity = $_POST['quantity'];
                    $imodel = new InstitutionUpgrade();                    
                    $imodel->num_pupils = $quantity;
                    $imodel->status = "pending";
                    $imodel->institution_id = $subscriber->id;
                    $imodel->save();
                }
                else 
                {
                    Yii::app()->user->setFlash('success', 'Sorry,your request cannot be processed,you already have pending upgrade in process');
                    $this->redirect($this->createUrl('institutions/mysubscription'));
                }
                Yii::app()->user->setFlash('success', 'Your upgrade request is waiting for approval,you will get confirmation soon.');
                $this->redirect($this->createUrl('institutions/mysubscription'));
            }
            Yii::app()->user->setFlash('error', 'Please reduce your number of active accounts to reduce you subscription quantity.');
            $this->render('subscription');
            
	}
        
        public function actioncardupdate() {
        
          //echo var_dump($_POST);
        $now = new DateTime();
        $model = Institutions::model()->findByAttributes(array('admin_id'=>Yii::app()->user->getState('userid')));
        $usermodel = Users::model()->findByPk(array('id'=>Yii::app()->user->getState('userid')));
        
        require Yii::app()->basePath . '/extensions/stripe/init.php';
	Stripe::setApiKey(Yii::app()->params['SECRET_Key']);
                
        $token = $_POST['stripeToken'];
        
        $cu = Customer::retrieve($model->stripe_id);
        $cu->source = $token; // obtained with Stripe.js
        $cu->save();
        
        $model->last_four = $_POST['cvc'];
        $model->updated_at = $now->format('Y-m-d H:i:s');
        $model->save();
        
        Yii::app()->user->setFlash('success', 'Your card information has been successfully updated');
        $this->redirect(Yii::app()->createUrl('institutions/mysubscription'));


    }
        
     public function actionupgradestore() {
        //echo var_dump($_POST);
        $now = new DateTime();
	    $model = Institutions::model()->findByAttributes(array('admin_id'=>Yii::app()->user->getState('userid')));
        $usermodel = Users::model()->findByPk(array('id'=>Yii::app()->user->getState('userid')));
        
             $amount = 0;
             if($model['payment_type'] != "fixed")
                $amount = (int)$model['amount'];
             else
                $amount = (int)$model['amount'] + (int)$model['additional_amount'];
                
            require Yii::app()->basePath . '/extensions/stripe/init.php';
    	    Stripe::setApiKey(Yii::app()->params['SECRET_Key']);
                    
            $planmodel = Plans::model()->findByAttributes(array('owner_id'=>Yii::app()->user->getState('userid')));
            
            if($planmodel == null)
            {
                $datetime = new DateTime();      //default 1 year
                $datetime->add(new DateInterval('P1Y'));
                $trial = $datetime->format('Y-m-d H:i:s');  
                               
                $planmodel = new Plans();
                $planmodel->price = $amount;
                $planmodel->active_to = $trial;
                $planmodel->owner_type = "Institution";
                $planmodel->owner_id = $model->id;
                $planmodel->name = $model->name;
                $planmodel->stripe_plan = strtolower(str_replace(' ', '_', $model->name));
                $planmodel->quantity = $model['num_pupils'];
                $planmodel->created_at = $now->format('Y-m-d H:i:s');
                if($planmodel->save())
                {
                    if(!$this->planExists($planmodel->stripe_plan))
                    {
                        $plan = Plan::create(array(
                            "amount" => $amount,
                            "interval" => "year",
                            "product" => array(
                                "name" => $model->name
                            ),
                            //"name" => $model->name,
                            "currency" => "gbp",
                            "id" => $planmodel->stripe_plan
                        ));
                    }
                }
                
            }
            $token = $_POST['stripeToken'];
            $plan = $planmodel->stripe_plan;
            
            $planarr = Plan::retrieve($plan);
            
            $amount = $planarr->amount;
            $planname = $planarr->name;
            $currency = $planarr->currency;
         
         //        create subscriber
             $customer = Customer::create(array(
                'source'   => $token,
                'email'    => $model->email,
                'plan'     => $plan,
                'description' => $model->name
            ));
             
             
    //        $charge = Charge::create(array(
    //             "amount" => $amount,
    //             "currency" => $currency,
    //             "customer" => $customer->id,
    //             "description" => $planname
    //         ));
    
    
    // ////         create subscription
    //         $subscription = Subscription::create(array(
    //             "customer" => $customer->id,
    //             "items" => array(
    //                 array(
    //                     "plan" => $plan,
    //                 ),
    //             )
    //         ));
            
      
        $model->stripe_active = 1;
        $model->stripe_id = $customer->id;
        $model->stripe_subscription = $customer->subscriptions->data[0]->id;
        $model->stripe_plan = $plan;
        $model->last_four = $_POST['cvc'];
        
        $model->updated_at = $now->format('Y-m-d H:i:s');
        $model->save();
        
        Yii::app()->user->setFlash('success', 'Successfully updated your membership');
        $this->redirect(Yii::app()->createUrl('institutions/edit', array('id' => $model->id)));
        
    }
    public function planExists($plan)
    {
        try {
            Plan::retrieve($plan);
            return true;
        } catch (Exception $e) {
        }
        return false;
    }
    
        /**
        * Displays a particular model.
        * @param integer $id the ID of the model to be displayed
        */
       public function actionmysubscription() {
           $this->layout = 'main';
           $this->render('subscription');
       }
	
	public function actiontabeditinstitute($id)
	{
	    $model = Institutions::model()->findByPk($id);
	    $this->render('tabeditinstitute',array(
	        'model'=>$model,
	    ));
	}
	public function actiontabeditadmin($id)
	{
	    $model = Institutions::model()->findByPk($id);
	    $usermodel  = Users::model()->findByPk($model->admin_id);  
	    $this->render('tabeditadmin',array(
	        'model'=>$model,'newusermodel'=>new Users(),'usermodel'=>$usermodel
	    ));
	}
	public function actiontabeditteachers($id)
	{
	    $model = Institutions::model()->findByPk($id);
	    $this->render('tabeditteachers',array(
	        'model'=>$model,
	    ));
	}
	public function actiontabedittutorgroups($id)
	{
	    $model = Institutions::model()->findByPk($id);
	    $this->render('tabedittutorgroups',array(
	        'model'=>$model,
	    ));
	}
        public function actiontabeditstudents($id)
	{
	    $model = Institutions::model()->findByPk($id);
	    $this->render('tabeditstudents',array(
	        'model'=>$model,
	    ));
	}
	
	public function actioncreateStudent($id)
	{
	    $model = Institutions::model()->findByPk($id);
	    
	    $profilemodel = new Profiles();
	    $usermodel = new Users();
	    $addressmodel = new Addresses();
	     $tgsmodel = new TutorGroupsStudents();
	    $this->render('studentForm',array(
	        'model'=>$model,'profilemodel'=>$profilemodel,'$tgsmodel'=>$tgsmodel,'usermodel'=>$usermodel,'addressmodel'=>$addressmodel
	    ));
	}
	public function actioncreateIStudent()
	{
	    $model = new Institutions();
	    
	    $profilemodel = new Profiles();
	    $usermodel = new Users();
	    $addressmodel = new Addresses();
	    
	    $this->render('studentiForm',array(
	        'model'=>$model,'profilemodel'=>$profilemodel,'usermodel'=>$usermodel,'addressmodel'=>$addressmodel
	    ));
	}
	public function actioncreateInstitution()
	{
	    $model = Institutions::model();
	    $addressmodel = Addresses::model();
	    $this->render('createinstitution',array(
	        'model'=>$model,'usermodel'=>Users::model(),'planmodel'=>Plans::model(),'addressmodel'=>$addressmodel
	    ));
	}
	
	public function actionsaveInstitution() {
        $now = new DateTime();
        $model = new Institutions();
        $usermodel = new Users();
        $addressmodel = Addresses::model();
        $planmodel = new Plans();
        $iusermodel = new InstitutionUsers();
        $pmodel = new Profiles();
        
        if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
           $usermodel->setScenario('confirm');
        
        if(isset($_POST['Institutions']))
        {
             $model->attributes = $_POST['Institutions'];
        }
         if(isset($_POST['Users']))
         {
             $usermodel->attributes = $_POST['Users'];
         }
         if(isset($_POST['Addresses']))
         {
             $addressmodel->attributes = $_POST['Addresses'];
         }

        require Yii::app()->basePath . '/extensions/stripe/init.php';
        Stripe::setApiKey(Yii::app()->params['SECRET_Key']);

       
        if(isset($_POST['Institutions']))
        {
             
            $valid = $model->validate();
            $valid = $usermodel->validate() && $valid;
            $valid = $addressmodel->validate() && $valid;
       

        if ($valid) {

            $usermodel->type = 'institution_admin';
            $usermodel->status = $_POST['status'];
            $usermodel->created_at = $now->format('Y-m-d H:i:s');
            $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
            $usermodel->save();

            $model->admin_id = $usermodel->id;
            $model->status = $_POST['status'];
            $model->email = $usermodel->email;
            $model->created_at = $now->format('Y-m-d H:i:s');
            $model->save();

            $iusermodel->institution_id = $model->id;
            $iusermodel->user_id = $usermodel->id;
            $iusermodel->role = "admin";
            $iusermodel->created_at = $now->format('Y-m-d H:i:s');
            $iusermodel->save();

            if ($_POST['active_to'] != "" && $_POST['active_to'] != "") {

                $d = str_replace('/', '-', $_POST['active_to']);
                $expiry_date = date('Y-m-d', strtotime($d));

                $planmodel->attributes = $_POST['Plans'];
                $planmodel->active_to = $expiry_date;
                $planmodel->owner_type = "Institution";
                $planmodel->owner_id = $model->id;
                $planmodel->name = $model->name;
                $planmodel->stripe_plan = strtolower(str_replace(' ', '_', $model->name));
                $planmodel->quantity = $model->num_pupils;
                $planmodel->created_at = $now->format('Y-m-d H:i:s');
                if ($planmodel->save()) {

                    $planid = str_replace(' ', '_', $model->name);

                    $plan = Plan::create(array(
                                "amount" => $_POST['Plans']['price'],
                                "interval" => $_POST['interval'],
                                "product" => array(
                                    "name" => $model->name
                                ),
                               // "name" => $model->name,
                                "currency" => "gbp",
                                "id" => $planid
                    ));
                    
                    Institutions::model()->updateByPk($model->id, array('stripe_plan'=>$planid,'stripe_active'=>1));
                }
            }
            
            $addressmodel->created_at = $now->format('Y-m-d H:i:s');
            $addressmodel->model_type = 'Institution';
            $addressmodel->model_id = $model->id;
            $addressmodel->save();
            
            $pmodel->owner_id = $model->id;
            $pmodel->owner_type = "Institution";
            if ($_POST['photo_id'] > 0)
                $pmodel->photo_id = $_POST['photo_id'];
            $pmodel->type = "institution";
            $pmodel->slug = strtolower(str_replace(' ', '-', $model->name));
            $pmodel->created_at = $now->format('Y-m-d H:i:s');
            if ($pmodel->save()) {
                Yii::app()->user->setFlash('success', ' Successfully created institution');
                $this->redirect($this->createUrl('institutions/overview'));
            }
        }
         }
        
        $this->render('createinstitution', array(
            'model' => $model, 'usermodel' => $usermodel, 'planmodel' => $planmodel, 'addressmodel' => $addressmodel
        ));
    }

    public function actionupdateInstitution($id)
    {    	    
        $now = new DateTime();

        $model = Institutions::model()->findByPk($id);
        $usermodel = Users::model()->findByPk($model->admin_id);
        $pmodel = Profiles::model()->findByAttributes(array('owner_id'=>$model->id,'type'=>'institution'));
        $pwd = $usermodel->password;
        if(isset($_POST['Institutions']))
        {
            $model->attributes = $_POST['Institutions'];
        }
        if(isset($_POST['Users']))
        {
             $usermodel->attributes = $_POST['Users'];
        }

        if(isset($_POST['Institutions']))
        {
            $valid = $model->validate();
            $valid = $usermodel->validate() && $valid;
            
            if($valid)
            {
            $model->status = $_POST['status'];
            $model->email = $_POST['Users']['email'];
            $model->updated_at = $now->format('Y-m-d H:i:s');
            if($model->save())
            {
                if($_POST['Users']['password'] == "")
                    $usermodel->password = $pwd;
                else 
                    $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
                $usermodel->updated_at = $now->format('Y-m-d H:i:s');
                if($usermodel->save())
                {
                    if(isset($_POST['photo_id']) && $_POST['photo_id'] > 0)
                    {
                         $pmodel->photo_id = $_POST['photo_id'];
                        $pmodel->save();
                    }
                 
                  Yii::app()->user->setFlash('success', 'Successfully updated Institution');
                  $this->redirect($this->createUrl('institutions/tabeditinstitute',array('id'=>$id)));
                }
               
            }
             
            }

        }

        $this->render('tabeditinstitute',array(
            'model'=>$model,
        ));

    }
	
	public function actionupdateCurrentAdmin($id)
	{
	    $now = new DateTime();
	    $model = Institutions::model()->findByPk($id);
	    $usermodel = Users::model()->findByPk($model->admin_id);
	    $pwd = $usermodel->password;
	    $usermodel->attributes = $_POST['Users'];
	    $usermodel->updated_at = $now->format('Y-m-d H:i:s');
	    if($_POST['Users']['password'] == "")
	        $usermodel->password = $pwd;
	    else 
	        $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
	    if($usermodel->save())  
	        $this->redirect($this->createUrl('institutions/tabeditinstitute',array('id'=>$id)));
	    
	        $this->render('tabeditadmin',array(
	            'model'=>$model,'usermodel'=>$usermodel,'newusermodel'=>$newusermodel
	        ));
// 	    $this->render('editsubscriptionpage',array(
// 	        'model'=>$model,
// 	    ));
	}
	
	public function actionaddNewAdmin($id)
	{
	    $now = new DateTime();
	    $model = Institutions::model()->findByPk($id);
	    $olduser = Users::model()->findByPk($model->admin_id);
	    
	    Users::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')),'id =:id',array(':id'=>$model->admin_id));
	    
	    $newusermodel = new Users();
	    $newusermodel->attributes = $_POST['Users'];
	    $newusermodel->type = $olduser->type;
	    $newusermodel->created_at = $now->format('Y-m-d H:i:s')                                                                                       ;
	    $newusermodel->status = "active";
	    $newusermodel->password = $_POST['Users']['password'];
	    $newusermodel->stripe_active = $olduser->stripe_active;
	    if($newusermodel->save())
	    {
	        Institutions::model()->updateAll(array('admin_id'=>$newusermodel->id),'id =:id',array(':id'=>$id));	    
	        $this->redirect($this->createUrl('institutions/tabeditinstitute',array('id'=>$id)));
	    }
	    
	    $this->render('tabeditadmin',array(
	        'model'=>$model,'usermodel'=>$olduser,'newusermodel'=>$newusermodel
	    ));

	}
	
	function resize_image($image,$image_type,$fullpath,$destpath,$width,$height)
	{
	    $size_arr = getimagesize($fullpath);
	    
	    list($width_orig, $height_orig, $img_type) = $size_arr;
	    $ratio_orig = $width_orig/$height_orig;
	    
	    $tempimg = imagecreatetruecolor($width, $height);
	    imagecopyresampled($tempimg, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
	    
	    if($image_type == "image/jpg" || $image_type == "image/jpeg")
	        imagejpeg($tempimg, $destpath);
	        else if( $image_type == "image/png" )
	            imagepng($tempimg, $destpath);
	            else if( $image_type == "image/gif" )
	                imagegif($tempimg, $destpath);
	}
	
	public function actioncreateTeacher($id)
	{
	    $instmodel = Institutions::model()->findByPk($id);
	    $usermodel = new Users();
	    $this->render('teacherForm',array(
	        'model'=>$instmodel,'usermodel'=>$usermodel
	    ));
	}
	public function actionsaveTeacher($id)
	{
	    $instmodel = Institutions::model()->findByPk($id);
	    $now = new DateTime();
	    $usermodel = new Users();
	    $usermodel->attributes = $_POST['Users'];
	    $usermodel->type = "teacher";
	    $usermodel->password =  password_hash($_POST['Users']['password'], 1,['cost' => 10]);
	    $usermodel->created_at = $now->format('Y-m-d H:i:s');
	    $usermodel->status = "active";
	    if($usermodel->save())
	    {
	        $iusermodel = new InstitutionUsers();
	        $iusermodel->institution_id = $id;
	        $iusermodel->user_id = $usermodel->id;
	        $iusermodel->role = "teacher";
	        $iusermodel->created_at = $now->format('Y-m-d H:i:s');
	        $iusermodel->save();
	        $this->redirect($this->createUrl('institutions/tabeditteachers',array('id'=>$id)));
	    }
	    $this->render('teacherForm',array(
	        'model'=>$instmodel,'usermodel'=>$usermodel
	    ));
	    
	}
	public function actionueditTeacher($id)
	{
	    $iusermodel = InstitutionUsers::model()->findByPk($id);
	    $usermodel = Users::model()->findByPk($iusermodel->user_id);
	    $model = Institutions::model()->findByPk($iusermodel->institution_id);
	    $this->render('uteacherForm',array(
	        'model'=>$model,'iusermodel'=>$iusermodel,'usermodel'=>$usermodel
	    ));
	}
	public function actioneditTeacher($id)
	{
	    $iusermodel = InstitutionUsers::model()->findByPk($id);
	    $usermodel = Users::model()->findByPk($iusermodel->user_id);
	    $model = Institutions::model()->findByPk($iusermodel->institution_id);
	    $this->render('teacherForm',array(
	        'model'=>$model,'iusermodel'=>$iusermodel,'usermodel'=>$usermodel
	    ));
	}
	public function actionupdateiTeacher($id)
	{
	    if($_POST['user_id'] > 0)
	    {
	        $now = new DateTime();
	        $usermodel = Users::model()->findByPk($_POST['user_id']);	
	        $pwd = $usermodel->password;
	        $usermodel->attributes = $_POST['Users'];
	        
	        if ($_POST['Users']['password'] == "")
	            $usermodel->password = $pwd;
	        else
	            $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
	        $usermodel->updated_at = $now->format('Y-m-d H:i:s');
	        $usermodel->save();
	    }
	    $this->redirect($this->createUrl('institutions/insTeachers',array('id'=>$id)));
	}
	public function actionupdateTeacher($id)
	{
	    if($_POST['user_id'] > 0)
	    {
    	    $now = new DateTime();
    	    $usermodel = Users::model()->findByPk($_POST['user_id']);
    	    $pwd = $usermodel->password;
    	    $usermodel->attributes = $_POST['Users'];
    	    if ($_POST['Users']['password'] == "")
    	        $usermodel->password = $pwd;
	        else
	            $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
    	    $usermodel->updated_at = $now->format('Y-m-d H:i:s');
    	    $usermodel->save();
	    }
	    
	    $this->redirect($this->createUrl('institutions/tabeditteachers',array('id'=>$id)));
	}
	public function actioncreateTutor($id)
	{
	    $model = Institutions::model()->findByPk($id);
	    $usermodel = new Users();
	    $tutormodel = new TutorGroups();
	    $this->render('createtutor',array(
	        'model'=>$model,'usermodel'=>$usermodel,'tutormodel'=>$tutormodel
	    ));
	}
	public function actionsaveTutor($id)
	{	    
	    $now = new DateTime();
	    $model = Institutions::model()->findByPk($id);
	    $tgmodel = new TutorGroups();
	    $tmodel = new Users();
	    $tgmodel->attributes = $_POST['TutorGroups'];
	    
	    if($_POST['TutorGroups']['teacher_id'] <= 0)
	    {	       
	        $tmodel->attributes = $_POST['Users'];
	        $tmodel->type = "teacher";
	        $tmodel->status = "active";
	        $tmodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
	        $tmodel->created_at = $now->format('Y-m-d H:i:s');
	        if($tmodel->save())
	            $tgmodel->teacher_id = $tmodel->id;
	    }
	   
	        $grad_date = null;
	        if($_POST['graduation_date'] != ""){
	            $d = str_replace('/', '-', $_POST["graduation_date"]);
	            $grad_date = date('Y-m-d', strtotime($d));
	        }
	           // $grad_date = date_format(new DateTime($_POST['graduation_date']),"Y-m-d");
	        $tgmodel->institution_id = $id;
	        $tgmodel->graduation_date= $grad_date;
	        $tgmodel->created_at = $now->format('Y-m-d H:i:s');
	        $tgmodel->account_limit = $_POST['allocated_students'];
	        if($tgmodel->save())
	        {
	            $forenames_arr = $_POST['forenames'];
	            array_pop($forenames_arr);
	            $surname_arr = $_POST['surname'];
	            array_pop($surname_arr);
	            $password_arr = $_POST['password'];
	            array_pop($password_arr);
	            $email_arr = $_POST['email'];
	            array_pop($email_arr);
	           // $confirmpwd_arr = $_POST['confirmpassword'];
	            
	            
	            for($i = 0; $i < count($forenames_arr); $i++)
	            {	                
	                $umodel = new Users();
	                $umodel->forenames = $forenames_arr[$i];
	                $umodel->surname = $surname_arr[$i];
	                $umodel->password = password_hash($password_arr[$i], 1,['cost' => 10]);
	                $umodel->email = $email_arr[$i];
	                $umodel->type = "candidate";
	                $umodel->status = "active";
	                $umodel->stripe_active = 1;
	                $umodel->created_at = $now->format('Y-m-d H:i:s');
	                if($umodel->save())
	                {
	                    $pmodel = new Profiles();
	                    $pmodel->owner_id = $umodel->id;
	                    $pmodel->owner_type = "Candidate";
	                    $pmodel->type = "candidate";
	                    $pmodel->slug = $this->generateSlug(null,$umodel->forenames."-".$umodel->surname);
	                    $pmodel->created_at = $now->format('Y-m-d H:i:s');
	                    $pmodel->save();
	                    
    	                $tgsmodel = new TutorGroupsStudents();
    	                $tgsmodel->institution_id = $id;
    	                $tgsmodel->tutor_group_id = $tgmodel->id;
    	                $tgsmodel->student_id = $umodel->id;
    	                $tgsmodel->graduation_date = $grad_date;
    	                $tgsmodel->created_at = $now->format('Y-m-d H:i:s');
    	                $tgsmodel->save();
	                }
	               
	            }
	            Yii::app()->user->setFlash('success', 'Tutor Group created Successfully.');
	            $this->redirect($this->createUrl('institutions/edittutor',array('id'=>$tgmodel->id)));
	        }
	    
	        
	        $this->render('createtutor',array(
	            'model'=>$model,'usermodel'=>$tmodel,'tutormodel'=>$tgmodel
	        ));
	      
	}
	public function actioneditTutor($id)
	{
	    $tutormodel = TutorGroups::model()->findByPk($id);
	    $model = Institutions::model()->findByPk($tutormodel->institution_id);
	    $usermodel = new Users();
	    
	    $this->render('edittutor',array(
	        'model'=>$model,'usermodel'=>$usermodel,'tutormodel'=>$tutormodel
	    ));
	    
	}
	public function actionupdateTutor($id)
	{
	    
	    $tgmodel = TutorGroups::model()->findByPk($id);
	    $model = Institutions::model()->findByPk($tgmodel->institution_id);
	    $usermodel = new Users();
	  
	    $now = new DateTime();
	    $tgmodel->attributes = $_POST['TutorGroups'];
	    
	    if($_POST['TutorGroups']['teacher_id'] <= 0)
	    {
	        $tmodel = new Users();
	        $tmodel->attributes = $_POST['Users'];
	        $tmodel->type = "teacher";
	        $tmodel->status = "active";
	        $tmodel->created_at = $now->format('Y-m-d H:i:s');
	        
	        if($tmodel->save())
	            $tgmodel->teacher_id = $tmodel->id;
	    }
	    
	    $grad_date = null;
	    if($_POST['graduation_date'] != "")
	    {
	        $d = str_replace('/', '-', $_POST["graduation_date"]);
	        $grad_date = date('Y-m-d', strtotime($d));
	    }
	        //$grad_date = date_format(new DateTime($_POST['graduation_date']),"Y-m-d");
	    
	        $tgmodel->graduation_date= $grad_date;
	        $tgmodel->updated_at = $now->format('Y-m-d H:i:s');
	        $tgmodel->account_limit = $_POST['allocated_students'];
	        $tgmodel->save();      
	        	        
	        $this->render('edittutor',array(
	            'model'=>$model,'usermodel'=>$usermodel,'tutormodel'=>$tgmodel
	        ));
	        
	}
	
	public function actiongetInstituteDetails()
	{
	    $model = Institutions::model()->findByPk($_POST['id']);
	    
	    $umodel = Users::model()->findByPk($model->admin_id);
	    $student_cnt = TutorGroupsStudents::model()->countByAttributes(array('institution_id'=>$model->id));
	    $cnt = $student_cnt.' / '.$model->num_pupils;
	    
	    $iarr = array();
	    $iarr['name'] = $model->name;
	    $iarr['admin'] = $umodel->forenames." ".$umodel->surname;
	    $iarr['student_count'] = $cnt;
	    echo json_encode($iarr);
	}
	
	public function actiongetTutorDetails()
	{
	    $model = TutorGroups::model()->findByPk($_POST['id']);
	    $imodel = Institutions::model()->findByPk($model->institution_id);
	    $umodel = Users::model()->findByPk($model->teacher_id);
	    
	    $student_cnt = TutorGroupsStudents::model()->countByAttributes(array('institution_id'=>$imodel->id));
	    $cnt = $student_cnt.' / '.$model->account_limit;
	    
	    $iarr = array();
	    $iarr['name'] = $model->name;
	    $iarr['teacher'] = $umodel->forenames." ".$umodel->surname;
	    $iarr['student_count'] = $cnt;
	    echo json_encode($iarr);
	}
	
	public function actiongetTutors()
	{
	    $tgmodel = TutorGroups::model()->findAllByAttributes(array('institution_id'=>$_POST['id'],'deleted_at'=>null));
	    $tutor_arr = CHtml::listData($tgmodel, 'id', 'name');
	    //$tutor_arr = array('0'=>'') + $tutor_arr;
	    echo CHtml::tag('option',
	        array('value'=>''),'',true);
	    foreach($tutor_arr as $value=>$name)
	    {
	        echo CHtml::tag('option',
	            array('value'=>$value),CHtml::encode($name),true);
	    }
	}
	public function actionsaveStudent($id)
	{
        $now = new DateTime();
        $dobformat = null;
        if($dobformat != null)
        {
            $d = str_replace('/', '-', $_POST["dob"]);
            $dobformat = date('Y-m-d', strtotime($d));
        }
        $model = Institutions::model()->findByPk($id);
       $tgmodel = new TutorGroups();
        
        $usermodel = new Users();
        $usermodel->attributes = $_POST['Users'];
        
        $tgsmodel = new TutorGroupsStudents();
        $tgsmodel->tutor_group_id = $_POST['tutor_group_id'];
        
        $pmodel = new Profiles();
        $pmodel->attributes = $_POST['Profiles'];
        
        $addressmodel = new Addresses();
        $addressmodel->attributes = $_POST['Addresses'];
        
        $valid = $usermodel->validate();
        $valid = $tgsmodel->validate() && $valid;
        $valid = $model->validate() && $valid;
        $valid = $pmodel->validate() && $valid;
        if($valid)
        {        
        $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
        $usermodel->type = "candidate";
        $usermodel->status = "active";
        $usermodel->stripe_active = 1;
        $usermodel->dob = $dobformat;
        $usermodel->created_at = $now->format('Y-m-d H:i:s');
        $usermodel->save();
        $tgsmodel = new TutorGroupsStudents();
        $tgsmodel->institution_id = $id;
        $tgsmodel->tutor_group_id = $_POST['tutor_group_id'];
        $tgsmodel->student_id = $usermodel->id;
        $tgsmodel->created_at = $now->format('Y-m-d H:i:s');
        $tgsmodel->save();
        $pmodel = new Profiles();
        $pmodel->attributes = $_POST['Profiles'];
        $pmodel->owner_id = $usermodel->id;
        $pmodel->type = "candidate";
         $pmodel->visibility = 0;
        $pmodel->owner_type = "Candidate";
        if ($_POST['photo_id'] > 0)
            $pmodel->photo_id = $_POST['photo_id'];
        $pmodel->created_at = $now->format('Y-m-d H:i:s');
        $pmodel->save();
        $addressmodel = new Addresses();
        $addressmodel->attributes = $_POST['Addresses'];
        $addressmodel->model_id = $usermodel->id;
        $addressmodel->model_type = "Candidate";
        $addressmodel->created_at = $now->format('Y-m-d H:i:s');
        if ($addressmodel->save()) {
            Yii::app()->user->setFlash('success', 'Successfully created Student');
            $this->redirect($this->createUrl('institutions/tabeditstudents',array('id'=>$model->id)));
        }
}
       $this->render('studentForm',array(
           'model'=>$model,'profilemodel'=>$pmodel,'tgsmodel'=>$tgsmodel,'usermodel'=>$usermodel,'addressmodel'=>$addressmodel
        ));
    }
    public function actionsaveiStudent()
    {
        $now = new DateTime();
        $d = str_replace('/', '-', $_POST["dob"]);
        $dobformat = date('Y-m-d', strtotime($d));
        
        
        $tgsmodel = new TutorGroupsStudents();
        $model = new Institutions();
        $tgmodel = new TutorGroups();
        $usermodel = new Users();
        $profilemodel = new Profiles();
        $addressmodel = new Addresses();
        
        
        $usermodel = new Users();
        $usermodel->attributes = $_POST['Users'];
        $usermodel->type = "candidate";
        $usermodel->status = "active";
        $usermodel->stripe_active = 1;
        $usermodel->dob = $dobformat;
        $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
        $usermodel->created_at = $now->format('Y-m-d H:i:s');
        $usermodel->save();
        $tgsmodel = new TutorGroupsStudents();
        $tgsmodel->institution_id = $_POST['Institutions']['id'];
        $tgsmodel->tutor_group_id = $_POST['tutor_group_id'];
        $tgsmodel->student_id = $usermodel->id;
        $tgsmodel->created_at = $now->format('Y-m-d H:i:s');
        $tgsmodel->save();
        $pmodel = new Profiles();
        $pmodel->attributes = $_POST['Profiles'];
        $pmodel->owner_id = $usermodel->id;
         $pmodel->visibility = 0;
        $pmodel->type = "candidate";
        $pmodel->owner_type = "Candidate";
        if ($_POST['photo_id'] > 0)
            $pmodel->photo_id = $_POST['photo_id'];
        
            $pmodel->created_at = $now->format('Y-m-d H:i:s');
            $pmodel->save();
            $addressmodel = new Addresses();
            $addressmodel->attributes = $_POST['Addresses'];
            $addressmodel->model_id = $usermodel->id;
            $addressmodel->model_type = "Candidate";
            $addressmodel->created_at = $now->format('Y-m-d H:i:s');
            if ($addressmodel->save()) {
                Yii::app()->user->setFlash('success', 'Successfully created Student');
                $this->redirect($this->createUrl('institutions/students'));
            }
            
            $this->render('studentiForm',array(
                'model'=>$model,'profilemodel'=>$profilemodel,'usermodel'=>$usermodel,'addressmodel'=>$addressmodel
            ));
    }
    public function actioneditStudent($id)
	{
	   $tgsmodel = TutorGroupsStudents::model()->findByPk($id);
	   $model = Institutions::model()->findByPk($tgsmodel->institution_id);
	   $tgmodel = TutorGroups::model()->findByPk($tgsmodel->tutor_group_id);
	   $usermodel = Users::model()->findByPk($tgsmodel->student_id);
// 	   echo "fdg==".$usermodel->id;die();
	   $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id,'type'=>'candidate'));
	   $cri = new CDbCriteria();
	   $cri->condition = "model_id = ".$usermodel->id." and model_type like '%institution%' and deleted_at is null";
	   
	   $addressmodel = Addresses::model()->find($cri);
	   if($addressmodel == null )
	       $addressmodel = new Addresses();
	   $this->render('studentEditForm',array(
	       'model'=>$model,'usermodel'=>$usermodel,'tutormodel'=>$tgmodel,'tgsmodel'=>$tgsmodel,
	       'profilemodel'=>$profilemodel,'addressmodel'=>$addressmodel
	   ));
	}
	
	public function actionupdateStudent($id)
	{
	    
	    $now = new DateTime();
            $d = str_replace('/','-',$_POST["dob"]);
            $dobformat = date('Y-m-d' , strtotime($d));
	    //$dobformat = date_format(new DateTime($_POST["dob"]),"Y-m-d");
	    $tgsmodel = TutorGroupsStudents::model()->findByPk($id);
	    $usermodel = Users::model()->findByPk($tgsmodel->student_id);
	    $pwd = $usermodel->password;
	    
	    $model = new Institutions();
	    $tgmodel = new TutorGroups();
	    $usermodel = new Users();
	    $profilemodel = new Profiles();
	    $addressmodel = new Addresses();
	    
	    if($_POST["Users"]["password"] != "" && ($_POST["Users"]["password"] != $_POST["confirmpassword"]))
	        $pwderr =  "Password not matched";
        else
        {
    	    $usermodel->attributes = $_POST['Users'];
    	    
    	    if($_POST['Users']['password'] == "")
    	        $usermodel->password = $pwd;
    	    else 
    	        $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
    	        
    	        $usermodel->dob = $dobformat;
    	    $usermodel->updated_at = $now->format('Y-m-d H:i:s');
    	   
    	    if($usermodel->save())
    	    {	        
    	        
                $amodel = Addresses::model()->findByAttributes(array('model_id'=>$usermodel->id));
                if($amodel == null)
                {
                    $addressmodel = new Addresses();
                    $addressmodel->attributes = $_POST['Addresses'];            
                    $addressmodel->created_at = $now->format('Y-m-d H:i:s');
                    $addressmodel->model_id = $usermodel->id;
                    $addressmodel->model_type = "Candidate";
                }
                else {
                    $addressmodel = Addresses::model()->findByPk($amodel->id);
                    $addressmodel->attributes = $_POST['Addresses'];            
                    $addressmodel->updated_at = $now->format('Y-m-d H:i:s');
                }
                         
                $addressmodel->save();
                
                $pmodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id));
    	        $pmodel->attributes = $_POST['Profiles'];
    	         $pmodel->visibility = 0;
                if($_POST['photo_id'] > 0)
    	        $pmodel->photo_id = $_POST['photo_id'];
    	        $pmodel->updated_at = $now->format('Y-m-d H:i:s');
    	        $pmodel->save();
    	        
    	        
    	     }
    	     
        }
        
        $model = Institutions::model()->findByPk($tgsmodel->institution_id);
        $tgmodel = TutorGroups::model()->findByPk($tgsmodel->tutor_group_id);
        $usermodel = Users::model()->findByPk($tgsmodel->student_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id,'type'=>'candidate'));
        $addressmodel = Addresses::model()->findByPk($addressmodel->id);
        
        $this->render('studentEditForm',array(
            'model'=>$model,'usermodel'=>$usermodel,'tutormodel'=>$tgmodel,'tgsmodel'=>$tgsmodel,
            'profilemodel'=>$profilemodel,'addressmodel'=>$addressmodel,'pwderr'=>$pwderr,'slugerr'=>$slugerr
        ));
	}
	
	public function actiondeleteStudent($id)
	{
	    $now = new DateTime();
	    $tgsmodel = TutorGroupsStudents::model()->findByPk($id);
	    $userid = $tgsmodel->student_id;
	    
	    $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$userid,'type'=>'candidate'));
	    
	    Addresses::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')),'model_id =:model_id',array(':model_id'=>$userid));
	    
	    if($profilemodel->photo_id > 0)
	        Media::model()->deleteAllByAttributes(array('media_id'=>$profilemodel->photo_id));
	    
        Profiles::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s'),'photo_id'=>null,'cover_id'=>null),'owner_id =:owner_id and type =:type',array(':owner_id'=>$userid,':type'=>'candidate'));
        Users::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')),'id =:id',array(':id'=>$userid));
        
	    TutorGroupsStudents::model()->deleteAllByAttributes(array('id'=>$id));
	    	
	        
	   // $this->redirect($this->createUrl('institutions/editSubscriptionDetails',array('id'=>$instid)));
	}
	
	public function actionProfile($id)
	{
		  $this->layout = "main";
	    $this->institutionid = $id;
	    $model = Institutions::model()->findByPk($id);
	    $usermodel = Users::model()->findByPk($model->admin_id);
	    $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$id,'type'=>'institution'));
	    $this->render('profile',array(
	        'model'=>$model,'usermodel'=>$usermodel,'profilemodel'=>$profilemodel
	    ));
	}
	public function actionEdit($id)
	{
            $this->layout = "main";
	    $this->edit = 1;
	    $this->institutionid = $id;
	    $model = Institutions::model()->findByPk($id);
	    $usermodel = Users::model()->findByPk($model->admin_id);
	    $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$id,'type'=>'institution'));
	    $this->render('editprofile',array(
	        'model'=>$model,'usermodel'=>$usermodel,'profilemodel'=>$profilemodel
	    ));
	}
	public function actionsaveInsBackground($id)
	{
    	 $body = $_POST['content'];    	
    	 Institutions::model()->updateAll(array('body'=>$body),'id =:id',array(':id'=>$id));    	 
    	 $model = Institutions::model()->findByPk($id);
    	 echo $this->renderPartial('instbackground',array('model'=>$model));
	}
	public function actionchangeVisiblity($id)
	{
	    $status = $_POST['status'];
	    $model = Institutions::model()->findByPk($id);
	    $pmodel = Profiles::model()->findByAttributes(array('owner_id'=>$model->id,'type'=>'institution'));
	    $profilemodel = Profiles::model()->findByPk($pmodel->id);
	    $profilemodel->visibility = ($status == "Public" ? 0 : 1);
	    $profilemodel->save();
	}
	public function actionchangeStatus($id)
	{
	    $status = $_POST['status'];
	   
	    $model = Institutions::model()->findByPk($id);
	    $pmodel = Profiles::model()->findByAttributes(array('owner_id'=>$model->id,'type'=>'institution'));
	    $pub = (strtolower($status) == 'active' ? 0 : 1);
	    
	    Profiles::model()->updateAll(array('published'=>$pub),'id =:id',array(':id'=>$pmodel->id));
	}
	
	public function actionuserAccountInfo($id)
	{
            $this->layout='main';
	    $model = Institutions::model()->findByPk($id);
	    $usermodel = Users::model()->findByPk($model->admin_id);
	    $profile = Profiles::model()->findByAttributes(array('owner_id'=>$id));
	    $pmodel = Profiles::model()->findByPk($profile['id']);
	    $cri = new CDbCriteria();
	    $cri->condition = "model_id =".$id." and model_type like '%institutions%' and deleted_at is null";
	    $addressmodel = Addresses::model()->find($cri);
	    if($addressmodel == null)
	        $addressmodel = new Addresses();
	    $this->render('accountinfo',array('model'=>$model,'pmodel'=>$pmodel,'usermodel'=>$usermodel,'addressmodel'=>$addressmodel));
	}
	
	
	public function actionupdateAccountInfo($id)
	{
	    $this->layout='main';
	    $now = new DateTime();	  
	    $model = Institutions::model()->findByPk($id);
	    $model->attributes=$_POST['Institutions'];
	    
	    $usermodel = Users::model()->findByPk($model->admin_id);
	    $usermodel->attributes=$_POST['Users'];
	    
	    $pmodel = Profiles::model()->findByAttributes(array('owner_id'=>$id));
	    
	    $profilemodel = Profiles::model()->findByPk($pmodel->id);
	    $profilemodel->attributes=$_POST['Profiles'];
	    
	    $cri = new CDbCriteria();
	    $cri->condition = "model_id =".$id." and model_type like '%institutions%' and deleted_at is null";
	    $addressmodel = Addresses::model()->find($cri);
	    if($addressmodel == null)
	        $addressmodel = new Addresses();
	    
	    $addressmodel->attributes=$_POST['Addresses'];
	    $addressmodel->model_id = $id;
	    $addressmodel->model_type = "Institutions";
	    	    
	    $valid = $usermodel->validate();
	    $valid = $model->validate() && $valid;
	    $valid = $profilemodel->validate() && $valid;
	    $valid = $addressmodel->validate() && $valid;
	    if($valid){      	    
    	   
    	    $model->updated_at = $now->format('Y-m-d H:i:s');
    	    if($model->save())
    	    {    	 
        	        $addressmodel->updated_at = $now->format('Y-m-d H:i:s');
        	        $addressmodel->save();
    	    }
           // $usermodel->current_job = $_POST['Users']['current_job'];
            $usermodel->updated_at = $now->format('Y-m-d H:i:s');
            if($usermodel->save())
            {
                $profilemodel->updated_at = $model->created_at;
              //  $profilemodel->slug = $_POST['Profiles']['slug'];
                $profilemodel->save();
                
                 Yii::app()->user->setFlash('success', 'Successfully updated profile');
                 $this->redirect($this->createUrl('institutions/edit',array('id'=>$model->id)));
             
            }
	    }
	   
	    $this->render('accountinfo',array('model'=>$model,'pmodel'=>$profilemodel,'usermodel'=>$usermodel));
	}
	public function actionsaveCoverPhoto()
	{
	    $profileid = $_POST['profileid'];
	    $pmodel = Profiles::model()->findByPk($profileid);
	    $now = new DateTime();
	    if($pmodel->cover_id > 0)
	    {
	        $mediamodel = Media::model()->findByPk($pmodel->cover_id);
	        $mediamodel->updated_at = $now->format('Y-m-d H:i:s');
	    }
	    else
	    {
	        $mediamodel = new Media();
	        $mediamodel->created_at = $now->format('Y-m-d H:i:s');
	        $mediamodel->collection_name = "sample";
	        $mediamodel->model_id = $profileid;
	    }
	    $filename = $_FILES['cover']['name'];
	    $image_type = $_FILES['cover']['type'];
	    $file = $_FILES['cover']['tmp_name'];
	    $name = pathinfo( $_FILES['cover']['name'], PATHINFO_FILENAME);
	    
	    $mediamodel->name = $name;
	    $mediamodel->file_name = $filename;
	    $mediamodel->size = $_FILES['cover']['size'];
	    
	    if($mediamodel->save())
	    {
	        $mediafolder  = "images/media/".$mediamodel->id."/conversions";
	        
	        if (!file_exists($mediafolder)) {
	            mkdir($mediafolder, 0777, true);
	        }
	        
	        $imgprefix  = "images/media/".$mediamodel->id;
	        $fullpath = "images/media/".$mediamodel->id."/".$filename;
	        
	        Profiles::model()->updateAll(array('cover_id'=>$mediamodel->id),'id =:id',array(':id'=>$profileid));
	        
	        if(move_uploaded_file($_FILES['cover']['tmp_name'], $fullpath))
	        {
	            //create cover
	            $coverwidth  = 1000;
	            $coverheight = 400;
	            $coverdestpath = $imgprefix."/conversions/cover.jpg";
	            
	            //create icon
	            $iconwidth  = 40;
	            $iconheight = 40;
	            $icondestpath = $imgprefix."/conversions/icon.jpg";
	            
	            //create joblisting
	            $jlwidth  = 150;
	            $jlheight = 85;
	            $jldestpath = $imgprefix."/conversions/joblisting.jpg";
	            
	            //create search
	            $searchwidth  = 167;
	            $searchheight = 167;
	            $searchdestpath = $imgprefix."/conversions/search.jpg";
	            
	            //create thumb
	            $thumbwidth  = 126;
	            $thumbheight = 126;
	            $thumbdestpath = $imgprefix."/conversions/thumb.jpg";
	            
	            if( $image_type == "image/jpg" || $image_type == "image/jpeg") {
	                
	                $image = imagecreatefromjpeg($fullpath);
	                
	                $this->resize_image($image,$image_type,$fullpath,$coverdestpath,$coverwidth,$coverheight);//create cover
	                $this->resize_image($image,$image_type,$fullpath,$icondestpath,$iconwidth,$iconheight);//create icon
	                $this->resize_image($image,$image_type,$fullpath,$jldestpath,$jlwidth,$jlheight);//create joblisting
	                $this->resize_image($image,$image_type,$fullpath,$searchdestpath,$searchwidth,$searchheight);//create search
	                $this->resize_image($image,$image_type,$fullpath,$thumbdestpath,$thumbwidth,$thumbheight);//create thumb
	                
	            }
	            elseif( $image_type == "image/gif" )  {
	                
	                $image = imagecreatefromgif($fullpath);
	                
	                $this->resize_image($image,$image_type,$fullpath,$coverdestpath,$coverwidth,$coverheight);//create cover
	                $this->resize_image($image,$image_type,$fullpath,$icondestpath,$iconwidth,$iconheight);//create icon
	                $this->resize_image($image,$image_type,$fullpath,$jldestpath,$jlwidth,$jlheight);//create joblisting
	                $this->resize_image($image,$image_type,$fullpath,$searchdestpath,$searchwidth,$searchheight);//create search
	                $this->resize_image($image,$image_type,$fullpath,$thumbdestpath,$thumbwidth,$thumbheight);//create thumb
	                
	            }
	            elseif( $image_type == "image/png" ) {
	                
	                $image = imagecreatefrompng($fullpath);
	                
	                $this->resize_image($image,$image_type,$fullpath,$coverdestpath,$coverwidth,$coverheight);//create cover
	                $this->resize_image($image,$image_type,$fullpath,$icondestpath,$iconwidth,$iconheight);//create icon
	                $this->resize_image($image,$image_type,$fullpath,$jldestpath,$jlwidth,$jlheight);//create joblisting
	                $this->resize_image($image,$image_type,$fullpath,$searchdestpath,$searchwidth,$searchheight);//create search
	                $this->resize_image($image,$image_type,$fullpath,$thumbdestpath,$thumbwidth,$thumbheight);//create thumb
	            }
	            
	        }
	        
	        echo Yii::app()->baseUrl."/".$fullpath;
	    }
	    	    
	}
	
	public function actionOverview()
	{
	    $model=new Institutions('search');
	    $model->unsetAttributes();  // clear any default values
	    if(isset($_GET['Institutions']))
	    {
	        $model->attributes=$_GET['Institutions'];
	    }
        $this->render('overview',array(
            'model'=>$model
        ));
	}
        
        public function actionpending()
	{
	    $model=new Institutions('search');
	    $model->unsetAttributes();  // clear any default values
	    if(isset($_GET['Institutions']))
	    {
	        $model->attributes=$_GET['Institutions'];
	    }
        $this->render('pending',array(
            'model'=>$model
        ));
	}
        
	public function actionTeachers()
	{
	    $model=new InstitutionUsers('search');
	    $insmodel = new Institutions();
	    $model->unsetAttributes();  // clear any default values
	    $namesearch = 0;
	    $fullname = '';
	    $userarr = array();
	    if(isset($_GET['InstitutionUsers']))
	    {
	        $model->attributes = $_GET['InstitutionUsers'];
	    }
	    if(isset($_GET['Institutions']))
	    {
	        $model->institution_id = $_GET['Institutions']['id'];
	        $insmodel->id = $model->institution_id;
	    }
	    if(isset($_GET['fullname']) && $_GET['fullname'] != "")
	    {
	        $namesearch = 1;
	        $fullname = $_GET['fullname'];
	        $cri = new CDbCriteria();
	        $cri->alias = "iu";
	        $cri->join = "inner join cv16_users u on u.id = iu.user_id";
	        $cri->condition = "concat_ws(' ',u.forenames,u.surname) like '%".$fullname."%'";
	        $searchusers = InstitutionUsers::model()->findAll($cri);	 
	        $userarr = array();
	        foreach ($searchusers as $users)
	           $userarr[] = $users['user_id'];        
	        
	    }
	    
        $this->render('teachers',array(
            'model'=>$model,'insmodel'=>$insmodel,'namesearch'=>$namesearch,'namearray'=>$userarr,'fullname'=>$fullname
        ));
	}
	public function actionTutorGroups()
	{
	    $model=new TutorGroups('search');
	    $model->unsetAttributes();  // clear any default values
	    $insmodel = new Institutions();
	    $namesearch = 0;
	    $fullname = '';
	    $userarr = array();
	    if(isset($_GET['TutorGroups']))
	        $model->attributes=$_GET['TutorGroups'];
	        
        if(isset($_GET['Institutions']))
        {
            $model->institution_id = $_GET['Institutions']['id'];
            $insmodel->id = $model->institution_id;
        }
        if(isset($_GET['fullname']) && $_GET['fullname'] != "")
        {
            $namesearch = 1;
            $fullname = $_GET['fullname'];
            $cri = new CDbCriteria();
            $cri->alias = "tg";
            $cri->join = "inner join cv16_users u on u.id = tg.teacher_id";
            $cri->condition = "concat_ws(' ',u.forenames,u.surname) like '%".$fullname."%'";
            $searchusers = TutorGroups::model()->findAll($cri);
            $userarr = array();
            foreach ($searchusers as $users)
                $userarr[] = $users['teacher_id'];
                
        }
        
        $this->render('tutorgroups',array(
            'model'=>$model,'insmodel'=>$insmodel,'namesearch'=>$namesearch,'namearray'=>$userarr,'fullname'=>$fullname
        ));
	}
	public function actionStudents()
	{
	    $model=new TutorGroupsStudents('search');
	    $model->unsetAttributes();  // clear any default values
	    
	    $insmodel = new Institutions();
	    $namesearch = 0;
	    $fullname = '';
	    $userarr = array();
	    
	    if(isset($_GET['TutorGroupsStudents']))
	        $model->attributes=$_GET['TutorGroupsStudents'];
	    
        if(isset($_GET['Institutions']))
        {
            $model->institution_id = $_GET['Institutions']['id'];
            $insmodel->id = $model->institution_id;
        }
        if(isset($_GET['fullname']) && $_GET['fullname'] != "" )
        {
            $namesearch = 1;
            $fullname = $_GET['fullname'];
            $cri = new CDbCriteria();
            $cri->alias = "tgs";
            $cri->join = "inner join cv16_users u on u.id = tgs.student_id";
            $cri->condition = "concat_ws(' ',u.forenames,u.surname) like '%".$fullname."%'";
            $searchusers = TutorGroupsStudents::model()->findAll($cri);
            $userarr = array();
            foreach ($searchusers as $users)
                $userarr[] = $users['student_id'];
                
        }
	        
        $this->render('students',array(
            'model'=>$model,'insmodel'=>$insmodel,'namesearch'=>$namesearch,'namearray'=>$userarr,'fullname'=>$fullname
        ));
	}
        
        public function actioncreateteachers($id)
	{
            $this->layout='main';
	    $usermodel = new Users();	
	    $model = InstitutionUsers::model()->findByAttributes(array("institution_id"=>$id));
	    $this->render('createteacher',array(
	        'model'=>$model,'usermodel'=>new Users(),'insmodel'=>new Institutions()
	    ));
	}
        
	public function actionaddTeacher()
	{
            $this->layout='admin';
	    $usermodel = new Users();	    
	    $this->render('insteacher',array(
	        'model'=>new InstitutionUsers(),'usermodel'=>new Users(),'insmodel'=>new Institutions()
	    ));
	}

	public function actionsaveInsTeacher() {
	
        $now = new DateTime();
        $usermodel = new Users();
        $iumodel = new InstitutionUsers();
        
        if(isset($_POST['Users']))
        {
            $usermodel->attributes = $_POST['Users'];
        }
        if(isset($_POST['InstitutionUsers']))
        {
             $iumodel->attributes = $_POST['InstitutionUsers'];
        }
        
        if(isset($_POST['Users']))
        {
            $valid = $usermodel->validate();
            
           // $valid = $iumodel->validate() && $valid;
            
            if($valid)
            {
                $usermodel->type = "teacher";
                $usermodel->created_at = $now->format('Y-m-d H:i:s');
                $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
                if($usermodel->save())
                {
                    if (Yii::app()->user->getState('role') == 'admin')
                    {   
                         $iumodel->institution_id = $_POST['Institutions']['id'];
                    } else 
                    {
                        $model = Institutions::model()->findByAttributes(array('admin_id' => Yii::app()->user->getState('userid')));
                        $iumodel->institution_id = $model->id;
                    }
                    $iumodel->user_id = $usermodel->id;
                    $iumodel->role = $usermodel->type;
                    $iumodel->created_at = $now->format('Y-m-d H:i:s');
                    if ($iumodel->save()) {
                        if (Yii::app()->user->getState('role') == 'admin') {
                            Yii::app()->user->setFlash('success', 'Successfully created');
                            $this->redirect($this->createUrl('institutions/teachers'));
                        } else {
                            $this->redirect($this->createUrl('institutions/insteachers', array('id' => $model->id)));
                        }
                    }
                }
               
            }
           
        }
        
        if(Yii::app()->user->getState('role') != 'admin')
        {
            $this->layout = "main";
            
            $this->render('createteacher', array(
                'model' => $iumodel, 'usermodel' => $usermodel, 'insmodel' => new Institutions()
            ));
        }
        else 
        {
            $this->render('insteacher', array(
                'model' => $iumodel, 'usermodel' => $usermodel, 'insmodel' => new Institutions()
            ));
        }
    }

    public function actioneditInsTeacher($id)
	{
        $this->layout='admin';
	    $iusermodel = InstitutionUsers::model()->findByPk($id);
	    $usermodel = Users::model()->findByPk($iusermodel->user_id);
	    $insmodel = Institutions::model()->findByPk($iusermodel->institution_id);
	    $this->render('insteacher',array(
	        'model'=>$iusermodel,'usermodel'=>$usermodel,'insmodel'=>$insmodel
	    ));
	}
	
	public function actionupdateInsTeacher($id){
	    
	    $now = new DateTime();
	    $iumodel = InstitutionUsers::model()->findByPk($id);
	    $iumodel->attributes = $_POST['InstitutionUsers'];
	    $iumodel->updated_at = $now->format('Y-m-d H:i:s');
	    $iumodel->save();
            $usermodel = Users::model()->findByPk($iumodel->user_id);
            $pwd = $usermodel->password;
            $usermodel->attributes = $_POST['Users'];
            $usermodel->updated_at = $now->format('Y-m-d H:i:s');
            if($_POST['Users']['password'] == "")
                $usermodel->password = $pwd;
            else
                $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
            if($usermodel->save()){
                 if(Yii::app()->user->getState('role') == 'admin')
                {
                    $this->redirect($this->createUrl('institutions/teachers'));
                }
                else
                {
                    $this->redirect($this->createUrl('institutions/insteachers',array('id'=>$iumodel->institution_id)));
                }
            }
            if(Yii::app()->user->getState('role') == 'admin')
            {
    	        $this->render('insteacher', array(
                    'model' => $iumodel, 'usermodel' => $usermodel, 'insmodel' => new Institutions()
                ));
            }
            else
            {
                $this->layout = "main";
                $this->render('createteacher', array(
                    'model' => $iumodel, 'usermodel' => $usermodel, 'insmodel' => new Institutions()
                ));
            }
	   
	}
	public function actioncreateTutorGroup()
	{
	            $model = new Institutions();	            
	            $usermodel = new Users();
	            $tutormodel = new TutorGroups();
	            $this->render('createtutorgroup',array(
	                'model'=>$model,'usermodel'=>$usermodel,'tutormodel'=>$tutormodel
	            ));
	}
	public function actionsampleDownload() {
            
	    return Yii::app()->getRequest()->sendFile("sample_students.csv",file_get_contents("doc/sample_students.csv"));
   }

   public function actionsaveTutorGroup() {
      
       $id = $_POST['institution_id'];
       $model = new Institutions();
       $tgmodel = new TutorGroups();
       $tmodel = new Users();
       $now = new DateTime();
       if(isset($_POST['TutorGroups']))
       {
           $tgmodel->attributes = $_POST['TutorGroups'];
           $tgmodel->institution_id = $id;
           
//            if ($_POST['TutorGroups']['teacher_id'] <= 0) {
//                $tmodel->attributes = $_POST['Users'];
//                $tmodel->type = "teacher";
//                $tmodel->status = "active";
//                $tmodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
//                $tmodel->created_at = $now->format('Y-m-d H:i:s');
//                if ($tmodel->save())
//                    $tgmodel->teacher_id = $tmodel->id;
//            }
           
       }
            if(!isset($_POST['TutorGroups']['teacher_id']))
                $tgmodel->setScenario('teacher');
           $valid = $tgmodel->validate();
           
           if($valid)
           {
              
                $grad_date = null;
                if ($_POST['graduation_date'] != "") {
                    $d = str_replace('/', '-', $_POST["graduation_date"]);
                    $grad_date = date('Y-m-d', strtotime($d));
                }
                                
                $tgmodel->graduation_date = $grad_date;
                $tgmodel->created_at = $now->format('Y-m-d H:i:s');
                $tgmodel->account_limit = $_POST['allocated_students'];
                if ($tgmodel->save()) {                    
                    
                    $teacherarr = isset($_POST['TutorGroups']['teacher_id']) ? $_POST['TutorGroups']['teacher_id'] : array();
                    if(count($teacherarr) > 0)
                    {
                        for ($i = 0; $i < count($teacherarr); $i++) 
                        {                         
                            $tgteachers = new TutorGroupsTeachers();
                            $tgteachers->tutor_group_id = $tgmodel->id;
                            $tgteachers->institution_id = $tgmodel->institution_id;
                            $tgteachers->teacher_id = $teacherarr[$i];
                            $tgteachers->created_at = $now->format('Y-m-d H:i:s');
                            $tgteachers->save();
                        }
                    }
                                        
                    $forenames_arr = $_POST['forenames'];
                    array_pop($forenames_arr);
                    $surname_arr = $_POST['surname'];
                    array_pop($surname_arr);
                    $password_arr = $_POST['password'];
                    array_pop($password_arr);
                    $email_arr = $_POST['email'];
                    array_pop($email_arr);
                    // $confirmpwd_arr = $_POST['confirmpassword'];


                    for ($i = 0; $i < count($forenames_arr); $i++) {
                        $umodel = new Users();
                        $umodel->forenames = $forenames_arr[$i];
                        $umodel->surname = $surname_arr[$i];
                        $umodel->password = password_hash($password_arr[$i], 1,['cost' => 10]);
                        $umodel->email = $email_arr[$i];
                        $umodel->type = "candidate";
                        $umodel->stripe_active = 1;
                        $umodel->status = "active";
                        $umodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
                        $umodel->created_at = $now->format('Y-m-d H:i:s');
                        
                        if ($umodel->save()) {
                            $pmodel = new Profiles();
                            $pmodel->owner_id = $umodel->id;
                            $pmodel->owner_type = "Candidate";
                             $pmodel->visibility = 0;
                            $pmodel->type = "candidate";
                            $pmodel->slug = $this->generateSlug(null,$umodel->forenames . "-" . $umodel->surname);
                            $pmodel->created_at = $now->format('Y-m-d H:i:s');
                            if(!$pmodel->save())
                            {
                                
                            }

                            $tgsmodel = new TutorGroupsStudents();
                            $tgsmodel->institution_id = $id;
                            $tgsmodel->tutor_group_id = $tgmodel->id;
                            $tgsmodel->student_id = $umodel->id;
                            $tgsmodel->graduation_date = $grad_date;
                            $tgsmodel->created_at = $now->format('Y-m-d H:i:s');
                            $tgsmodel->save();
                            
                            Yii::app()->user->setFlash('success', 'Successfully updated.');
                           
                            
                        }
                    }
                }
                
                $this->redirect($this->createUrl('institutions/TutorGroups'));
       }
        
       $this->render('createtutorgroup',array(
	                'model'=>$model,'usermodel'=>$tmodel,'tutormodel'=>$tgmodel
	   ));
    }

    public function actioneditTutorGroup($id)
	{
	    $tutormodel = TutorGroups::model()->findByPk($id);
	    $model = Institutions::model()->findByPk($tutormodel->institution_id);
	    $usermodel = new Users();//Users::model()->findByPk($tutormodel->teacher_id);
	    $this->render('edittutorgroup',array(
	        'model'=>$model,'usermodel'=>$usermodel,'tutormodel'=>$tutormodel,'institution_id'=>$tutormodel->institution_id
	    ));
	}
	public function actionupdateTutorGroup($id)
	{
// 	    $id = $_POST['institution_id'];
	    
	    $now = new DateTime();
	    $tmodel = new Users();
	    $tgmodel = TutorGroups::model()->findByPk($id);
	    $model = Institutions::model()->findByPk($tgmodel->institution_id);
	    $tgmodel->attributes = $_POST['TutorGroups'];
	    $tgmodel->institution_id = $id;
// 	    if(isset($_POST['TutorGroups']['teacher_id']) && $_POST['TutorGroups']['teacher_id'] <= 0)
// 	    {	       
// 	        $tmodel->attributes = $_POST['Users'];
// 	        $tmodel->type = "teacher";
// 	        $tmodel->status = "active";
// 	        $tmodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
// 	        $tmodel->created_at = $now->format('Y-m-d H:i:s');
// 	        if($tmodel->save())
// 	            $tgmodel->teacher_id = $tmodel->id;
// 	    }
	    
	    $grad_date = null;
	    if($_POST['graduation_date'] != "")
	    {
	        $d = str_replace('/', '-', $_POST["graduation_date"]);
	        $grad_date = date('Y-m-d', strtotime($d));
	    }
	      //  $grad_date = date_format(new DateTime($_POST['graduation_date']),"Y-m-d");
	        
	        $tgmodel->graduation_date= $grad_date;
	        $tgmodel->updated_at = $now->format('Y-m-d H:i:s');
	        $tgmodel->account_limit = $_POST['allocated_students'];
	        
	        if(!isset($_POST['TutorGroups']['teacher_id']))
	            $tgmodel->setScenario('teacher');
	        if($tgmodel->save())
	        {
	            $teacherarr = isset($_POST['TutorGroupsTeachers']['teacher_id']) ? $_POST['TutorGroupsTeachers']['teacher_id'] : array();
	            if(count($teacherarr) > 0)
	            {
	                TutorGroupsTeachers::model()->deleteAllByAttributes(array('tutor_group_id'=>$tgmodel->id,'institution_id'=>$tgmodel->institution_id));
	                
	                for ($i = 0; $i < count($teacherarr); $i++)
	                {
	                    $tgteachers = new TutorGroupsTeachers();
	                    $tgteachers->tutor_group_id = $tgmodel->id;
	                    $tgteachers->institution_id = $tgmodel->institution_id;
	                    $tgteachers->teacher_id = $teacherarr[$i];
	                    $tgteachers->created_at = $now->format('Y-m-d H:i:s');
	                    $tgteachers->updated_at = $now->format('Y-m-d H:i:s');
	                    $tgteachers->save();
	                }
	            }
	            
	             $this->redirect($this->createUrl('institutions/TutorGroups'));
	        }
             $this->render('edittutorgroup',array(
                 'model'=>$model,'usermodel'=>$tmodel,'tutormodel'=>$tgmodel,'institution_id'=>$tgmodel->institution_id
             ));
	}
	
	public function actiongetNumPupils()
	{
	    $model = Institutions::model()->findByPk($_POST['id']);
	    $tgs_cnt = TutorGroupsStudents::model()->countByAttributes(array('institution_id'=>$model->id));
	    echo $model->num_pupils - $tgs_cnt;
	}
	
	public function actionexportTutorsCSV()
	{
	    Yii::import('ext.ECSVExport');
	    $criteria = new CDbCriteria();
	    $criteria->alias = "iu";
	    $criteria->select = "CONCAT_WS(' ', u.forenames, u.surname) as names,u.email,i.name,u.created_at";
	    $criteria->join = "inner join cv16_users u on u.id = iu.user_id
                           inner join cv16_institutions i on i.id = iu.institution_id";
	    $criteria->condition = "iu.role = 'teacher'";
	    	    
	    $dataProvider = new CActiveDataProvider('InstitutionUsers', array(
	        'criteria'=>$criteria
	    ));
	    
	    $csv = new ECSVExport($dataProvider);
	    $content = $csv->toCSV();
	    Yii::app()->getRequest()->sendFile('export.csv', $content, "text/csv", false);
	}
	
	public function actioncreateInstitutionAdmin()
	{
	    $usermodel = new Users();
	    $this->render('insadmin',array(
	        'model'=>new InstitutionUsers(),'usermodel'=>new Users(),'insmodel'=>new Institutions()
	    ));
	}
	public function actionexportStudentsCSV()
	{
	    Yii::import('ext.ECSVExport');
	    $criteria = new CDbCriteria();
	    $criteria->alias = "tgs";
	    $criteria->select = "CONCAT_WS(' ', u.forenames, u.surname) as names,u.email,i.name as school,tgs.graduation_date,u.created_at";
	    $criteria->join = "inner join cv16_users u on u.id = tgs.student_id
                           inner join cv16_institutions i on i.id = tgs.institution_id";
	    
	    $dataProvider = new CActiveDataProvider('TutorGroupsStudents', array(
	        'criteria'=>$criteria
	    ));
	    
	    $csv = new ECSVExport($dataProvider);
	    $content = $csv->toCSV();
	    Yii::app()->getRequest()->sendFile('export.csv', $content, "text/csv", false);
	}
	public function actionexportTeachersCSV()
	{
	    Yii::import('ext.ECSVExport');
	    $criteria = new CDbCriteria();
	    $criteria->alias = "iu";
	    $criteria->select = "CONCAT_WS(' ', u.forenames, u.surname) as names,u.email,i.name as school,u.created_at";
	    $criteria->join = "inner join cv16_users u on u.id = iu.user_id
                           inner join cv16_institutions i on i.id = iu.institution_id";
	    $criteria->condition = "iu.role ='teacher'";
	    $dataProvider = new CActiveDataProvider('InstitutionUsers', array(
	        'criteria'=>$criteria
	    ));
	    
	    $csv = new ECSVExport($dataProvider);
	    $content = $csv->toCSV();
	    Yii::app()->getRequest()->sendFile('export.csv', $content, "text/csv", false);
	}
	public function actioneditInsStudent($id)
	{
	    $tgsmodel = TutorGroupsStudents::model()->findByPk($id);
	    $model = Users::model()->findByPk($tgsmodel->student_id);
	    
	    $this->render('tabeditstudent',array(
	        'model'=>$model,'tgsmodel'=>$tgsmodel,'imodel'=>new Institutions()
	    ));
	}
        
        public function actiontabeditstudent($id)
	{
	    $tgsmodel = TutorGroupsStudents::model()->findByPk($id);
            
	    $model = Users::model()->findByPk($tgsmodel->student_id);
	    $imodel = Institutions::model()->findByPk($tgsmodel->institution_id);
	    $this->render('tabeditstudent',array(
	        'model'=>$model,'tgsmodel'=>$tgsmodel,'imodel'=>$imodel
	    ));
	}
        
        public function actiontabeditprofile($id)
	{
	    $tgsmodel = TutorGroupsStudents::model()->findByPk($id);
            
	    $model = Users::model()->findByPk($tgsmodel->student_id);
	    
	    $this->render('tabeditprofile',array(
	        'model'=>$model,'tgsmodel'=>$tgsmodel
	    ));
	}
        
        public function actiontabeditdocuments($id)
	{
	    $tgsmodel = TutorGroupsStudents::model()->findByPk($id);
            
	    $model = Users::model()->findByPk($tgsmodel->student_id);
	    
	    $this->render('tabeditdocuments',array(
	        'model'=>$model,'tgsmodel'=>$tgsmodel
	    ));
	}
        public function actiontabeditapplications($id)
	{
	    $tgsmodel = TutorGroupsStudents::model()->findByPk($id);
            
	    $model = Users::model()->findByPk($tgsmodel->student_id);
	    
	    $this->render('tabeditapplications',array(
	        'model'=>$model,'tgsmodel'=>$tgsmodel
	    ));
	}
	public function actionupdateStudentDetails($id)
	{	    
	    $now = new DateTime();
	    $usermodel = Users::model()->findByPk($id);
	    $pwd = $usermodel->password;
	    $usermodel->attributes = $_POST["Users"];
	    if($_POST["Users"]["password"] == "")
	        $usermodel->password = $pwd;
	    else 
	        $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
	    
	        $usermodel->type = "candidate";
                $d = str_replace('/','-',$_POST["dob"]);
                $dobformat = date('Y-m-d' , strtotime($d));
	        //$dobformat = date_format(new DateTime($_POST["dob"]),"Y-m-d");
	        $usermodel->dob = $dobformat;
	        $usermodel->updated_at = $now->format('Y-m-d H:i:s');
	        if($usermodel->save())
	        {
	            $cri = new CDbCriteria();
	            $cri->condition = "model_id = ".$id." and model_type like '%candidate%' and deleted_at is null";
	            $addressmodel = Addresses::model()->find($cri);
	            if($addressmodel == null)
	            {
	                $addressmodel = new Addresses();
	                $addressmodel->created_at = $now->format('Y-m-d H:i:s');
	            }
	            else
	                $addressmodel->updated_at = $now->format('Y-m-d H:i:s');
	                
	                $addressmodel->attributes = $_POST["Addresses"];
	                $addressmodel->model_id = $id;
	                if($addressmodel->save())
	                {
	                    $pmodel = Profiles::model()->findByAttributes(array('owner_id'=>$id,'type'=>$usermodel->type));
	                    $profilemodel = Profiles::model()->findByPk($pmodel->id);
	                    $profilemodel->slug = $_POST["Profiles"]['slug'];
                            if($_POST['photo_id'] > 0)
	                    $profilemodel->photo_id = $_POST['photo_id'];
	                    $profilemodel->updated_at = $now->format('Y-m-d H:i:s');
	                    $profilemodel->save();
	                }
	                 $insid = $_POST["Institutions"]['id'];
	                $this->redirect(Yii::app()->createUrl('institutions/students'));
	               
	        }	        
	        $this->render('tabeditstudent',array(
	            'model'=>$usermodel,'tgsmodel'=>new TutorGroupsStudents(),'imodel'=>new Institutions()
	        ));
	        
	}
	
	public function actionsaveInsAdmin()
	{
	    //pending having some doubts
	    
	}
	
	
	public function actionaccountSubscription()
	{
	    $this->layout = "main";
	    $model = Institutions::model()->findByAttributes(array('admin_id'=>Yii::app()->user->getState('userid')));
	 
	    $this->render('subscription',array(
	        'model'=>$model
	    ));
	}
	public function actionupdateSubscription()
	{
	    $this->layout = "main";
	    $model = Institutions::model()->findByAttributes(array('admin_id'=>Yii::app()->user->getState('userid')));
	    $model->num_pupils = $_POST['num_qty'];
	    $model->save();
	    $this->redirect(Yii::app()->createUrl('institutions/accountSubscription'));
	}
	public function actionPayment()
	{
	    $this->layout = "main";
	    $now = new DateTime();
	    $model = Institutions::model()->findByAttributes(array('admin_id'=>Yii::app()->user->getState('userid')));
	   
	    require Yii::app()->basePath . '/extensions/stripe/init.php';
	    Stripe::setApiKey(Yii::app()->params['stripeKey']);
	    
	    $amount = 25 * 100;
	    
	    $expiry = explode('/', $_POST['expiry']);
	    $token = Token::create(
	        array(
	            "card" => array(
	                "name" => $_POST['card_holder'],
	                "number" => $_POST['card_number'],
	                "exp_month" => $expiry[0],
	                "exp_year" => $expiry[1],
	                "cvc" => $_POST['cvc']
	            )
	          )
	        );
	  
	    if($model->stripe_active == 1)
	    {
	        $customerid = $model->stripe_id;
	        $subscriptionid = $model->stripe_subscription;
	        $stripe_plan = $model->stripe_plan;
	        
	        $charge = Charge::create(array(	            
	            "amount" => $amount,
	            "currency" => "gbp",
	            "customer" => $customerid,
	            "description" => "Account number has been to ".$model->num_pupils,
	            'source'   => $token,
	            'receipt_email'=> $model->email
	        ));
	    }
	    else 
	    {
	        $planlwr = strtolower($model->name);
	        $planid = str_replace(' ', '_', $planlwr);
	      
    	    $plan = Plan::create(array(
    	        "amount" => $amount,
    	        "interval" => "year",
    	        "product" => array(
    	            "name" => $model->name
    	        ),
    	       // "name" => $model->name,
    	        "currency" => "gbp",
    	        "id" => $planid
    	    ));
	    
	        
	        
    	    $customer = Customer::create(array(
    	        'source'   => $token,
    	        'email'    => $model->email,
    	        'plan'     => $planid
    	        
    	        // 	            'account_balance' => $setupFee,
    	    // 	            'description' => "Charge with one time setup fee"
    	    ));
    	    // Create the charge on Stripe's servers - this will charge the user's card
    	    
//     	        $charge = Charge::create(array(
    	            
//     	            "amount" => $amount, 
//     	            "currency" => "gbp",
//     	            "customer" => $customer->id,
// //     	            "description" => "Ex1 charge"
//     	        ));
    	    
//     	        $subscription = Subscription::create(array(
//     	            "customer" => $customer->id,
//     	            "items" => array(
//         	                array(
//         	                    "plan" => $planid,
//             	                ),
//         	            )
//     	        ));
    	        
    	        $invoiceitem = InvoiceItem::create(array(
    	            "customer" => $customer->id,
    	            "amount" => $amount,
    	            "currency" => "gbp",
    	            "description" => "Subscription"    	             
    	           )
    	        );
    	        
    	        $invoice = Invoice::create(array(
    	            "customer" => $customer->id
    	        ));
    	        
    	        $model->stripe_active = 1;
    	        $model->status = "acive";
    	        $model->stripe_id = $customer->id;
    	        $model->stripe_subscription = $customer->subscriptions->data[0]->id;
    	        $model->stripe_plan = $planid;
    	        $model->last_four = $_POST['cvc'];
    	        $model->updated_at = $now->format('Y-m-d H:i:s');
    	        $model->save();
    	        
    	       /*  $mPDF1 = Yii::app()->ePdf->mpdf();
    	        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
    	        $stylesheet = file_get_contents(Yii::app()->getBaseUrl(true) . '/themes/Dev/css/style.css');
    	        $mPDF1->WriteHTML($stylesheet, 1);
    	        $mPDF1->WriteHTML($this->renderPartial('invoice',array('model'=>$model), true));
    	        $mPDF1->Output(); */
    	        
    	        
	       }
	       
	       $this->redirect(Yii::app()->createUrl('institutions/accountSubscription'));
	        
	}
	
	public function actiondeleteInstitution($id)
	{
	    $now = new DateTime();
	    $model = Institutions::model()->findByPk($id);
	    $insmodel = Institutions::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')),'id =:id',array(':id'=>$id));
	    $usermodel = Users::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')),'id =:id',array(':id'=>$model->admin_id));
	    $profiles = Profiles::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')),'owner_id =:owner_id',array(':owner_id'=>$model->admin_id));
	    $plansmodel = Plans::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')),'owner_id =:owner_id',array(':owner_id'=>$id));
	    $this->redirect(Yii::app()->createUrl('institutions/overview'));
	}
	    
	public function actionInsTeachers($id)
	{
            $this->layout='main';
	    $model = Institutions::model()->findByPk($id);
	    
	    $this->render("iteachers",array('model'=>$model,'instusers'=>InstitutionUsers::model()));
	}
	public function actionInsClasses($id)
	{
              $this->layout='main';
	    $model = Institutions::model()->findByPk($id);
	    
	    $this->render("iclasses",array('model'=>$model,'tutorgroups'=>TutorGroups::model()));
	}
	public function actionInsStudents($id)
	{
             $this->layout='main';
	    $model = Institutions::model()->findByPk($id);
	    
	    $this->render("istudents",array('model'=>$model,'tutorgroupstud'=>TutorGroupsStudents::model()));
	}
	public function actiongetInsTeachers()
	{
	    $insmodel = Institutions::model()->findByPk($_POST['id']);
	    $insusersmodel = InstitutionUsers::model()->findAllByAttributes(array('institution_id'=>$insmodel->id,'role'=>'teacher'));
	  
// 	    echo CHtml::tag('option',
// 	        array('value'=>'0'),'- New Teacher -',true);
	    foreach($insusersmodel as $insusr)
	    {
	        $umodel = Users::model()->findByPk($insusr['user_id']);
	        echo CHtml::tag('option',
	            array('value'=>$insusr['user_id']),CHtml::encode($umodel->forenames." ".$umodel->surname),true);
	    }
	}
	public function actioncreateClass()
	{
	    $model = Institutions::model()->findByAttributes(array('admin_id'=>Yii::app()->user->getState('userid')));
	    $this->render("class",array('model'=>$model,'tutormodel'=>TutorGroups::model(),'usermodel'=>new Users()));
	}
	public function actioneditClass($id)
	{
	    $tgmodel = TutorGroups::model()->findByPk($id);
	    $usermodel = new Users();//Users::model()->findByPk($tgmodel->teacher_id);
	    $model = Institutions::model()->findByPk($tgmodel->institution_id);
	    $this->render("editclass",array('model'=>$model,'tutormodel'=>$tgmodel,'usermodel'=>$usermodel));
	}
	public function actionsaveClass()
	{
	    
	    $model = Institutions::model()->findByAttributes(array('admin_id'=>Yii::app()->user->getState('userid')));
	    $id = $model->id;
	    $now = new DateTime();
	    $tgmodel = new TutorGroups();
	    $tmodel = new Users();
	    $iumodel = new InstitutionUsers();
	    $tgmodel->attributes = $_POST['TutorGroups'];
// 	    if($_POST['TutorGroups']['teacher_id'] <= 0)
// 	    {
	       
// 	        $tmodel->attributes = $_POST['Users'];
// 	        $tmodel->type = "teacher";
// 	        $tmodel->status = "active";
// 	        $tmodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
// 	        $tmodel->created_at = $now->format('Y-m-d H:i:s');
// 	        if($tmodel->save()){	            
	            
// 	            $iumodel->institution_id = $model->id;	            
// 	            $iumodel->user_id = $tmodel->id;
// 	            $iumodel->role = $tmodel->type;
// 	            $iumodel->created_at = $now->format('Y-m-d H:i:s');
// 	            $iumodel->save();
	            
// 	            $tgmodel->teacher_id = $tmodel->id;
// 	        }
// 	    }
	    
	    $grad_date = null;
	    if($_POST['graduation_date'] != "")
	    {
// 	        $grad_date = date_format(new DateTime($_POST['graduation_date']),"Y-m-d");
            $d = str_replace('/', '-', $_POST["graduation_date"]);
            $grad_date = date('Y-m-d', strtotime($d));
	    }
	        $tgmodel->institution_id = $id;
	        $tgmodel->graduation_date= $grad_date;
	        $tgmodel->created_at = $now->format('Y-m-d H:i:s');
	        $tgmodel->account_limit = $_POST['allocated_students'];
	        if($tgmodel->save())
	        {
	            $teacherarr = isset($_POST['TutorGroups']['teacher_id']) ? $_POST['TutorGroups']['teacher_id'] : array();
	            if(count($teacherarr) > 0)
	            {
	                for ($i = 0; $i < count($teacherarr); $i++)
	                {
	                    $tgteachers = new TutorGroupsTeachers();
	                    $tgteachers->tutor_group_id = $tgmodel->id;
	                    $tgteachers->institution_id = $tgmodel->institution_id;
	                    $tgteachers->teacher_id = $teacherarr[$i];
	                    $tgteachers->created_at = $now->format('Y-m-d H:i:s');
	                    $tgteachers->save();
	                }
	            }
	            	            
	            $forenames_arr = $_POST['forenames'];
	            array_pop($forenames_arr);
	            $surname_arr = $_POST['surname'];
	            array_pop($surname_arr);
	            $email_arr = $_POST['email'];
	            array_pop($email_arr);
	            $password_arr = $_POST['password'];
	            array_pop($password_arr);
	            // $confirmpwd_arr = $_POST['confirmpassword'];
	            
	            
	           // for($i = 0; $i < $_POST['allocated_students']; $i++)
	           for($i = 0; $i < count($forenames_arr); $i++)
	            {
	                $umodel = new Users();
	                $umodel->forenames = $forenames_arr[$i];
	                $umodel->surname = $surname_arr[$i];
	                $umodel->password = password_hash($password_arr[$i], 1,['cost' => 10]);
	                $umodel->email = $email_arr[$i];
	                $umodel->type = "candidate";
	                $umodel->status = "active";
	                $umodel->stripe_active =1;
	                $umodel->created_at = $now->format('Y-m-d H:i:s');
	                if($umodel->save(false))
	                {
	                    $pmodel = new Profiles();
	                    $pmodel->owner_id = $umodel->id;
	                    $pmodel->owner_type = "Candidate";
	                     $pmodel->visibility = 0;
	                    $pmodel->type = "candidate";
	                    $pmodel->slug = $this->generateSlug(null,$umodel->forenames."-".$umodel->surname);
	                    $pmodel->created_at = $now->format('Y-m-d H:i:s');
	                    $pmodel->save(false);
	                    
	                    $tgsmodel = new TutorGroupsStudents();
	                    $tgsmodel->institution_id = $id;
	                    $tgsmodel->tutor_group_id = $tgmodel->id;
	                    $tgsmodel->student_id = $umodel->id;
	                    $tgsmodel->graduation_date = $grad_date;
	                    $tgsmodel->created_at = $now->format('Y-m-d H:i:s');
	                    $tgsmodel->save(false);
	                }
	                
	            }
	            $this->redirect($this->createUrl('institutions/InsClasses',array('id'=>$model->id)));
	        }
	        
	        $this->render("class",array('model'=>$model,'tutormodel'=>$tgmodel,'usermodel'=>new Users()));
	        
	}
	public function actionupdateClass($id)
	{
	    $tgmodel = TutorGroups::model()->findByPk($id);
	    $model = Institutions::model()->findByPk($tgmodel->institution_id);
	    $id = $model->id;
	    $now = new DateTime();
	    
	   
	    $grad_date = null;
	    if($_POST['graduation_date'] != "")
	    {
	        $d = str_replace('/', '-', $_POST["graduation_date"]);
	        $grad_date = date('Y-m-d', strtotime($d));
	    }
	       // $grad_date = date_format(new DateTime($_POST['graduation_date']),"Y-m-d");
    
        $tgmodel->attributes = $_POST['TutorGroups'];
        $tgmodel->graduation_date= $grad_date;
        $tgmodel->updated_at = $now->format('Y-m-d H:i:s');
        $tgmodel->account_limit = $_POST['allocated_students'];
        if($tgmodel->save())
        {
            $teacherarr = isset($_POST['TutorGroupsTeachers']['teacher_id']) ? $_POST['TutorGroupsTeachers']['teacher_id'] : array();
            if(count($teacherarr) > 0)
            {
                TutorGroupsTeachers::model()->deleteAllByAttributes(array('tutor_group_id'=>$tgmodel->id,'institution_id'=>$tgmodel->institution_id));
                
                for ($i = 0; $i < count($teacherarr); $i++)
                {
                    $tgteachers = new TutorGroupsTeachers();
                    $tgteachers->tutor_group_id = $tgmodel->id;
                    $tgteachers->institution_id = $tgmodel->institution_id;
                    $tgteachers->teacher_id = $teacherarr[$i];
                    $tgteachers->created_at = $now->format('Y-m-d H:i:s');
                    $tgteachers->updated_at = $now->format('Y-m-d H:i:s');
                    $tgteachers->save();
                }
            }
            
            $this->redirect($this->createUrl('institutions/InsClasses',array('id'=>$model->id)));
        }
	        
        $usermodel = Users::model()->findByPk($tgmodel->teacher_id);       
        $this->render("editclass",array('model'=>$model,'tutormodel'=>$tgmodel,'usermodel'=>$usermodel));
	        
	}
	public function actionbulkStudent()
	{
        $this->layout='main';
        if(Yii::app()->user->getState('role') == "institution_admin")
        {
	       $model = Institutions::model()->findByAttributes(array('admin_id'=>Yii::app()->user->getState('userid')));
	       $this->render("students_bulk",array('model'=>$model,'tutormodel'=>TutorGroups::model(),'usermodel'=>new Users()));
        }
        else 
        {
            $iumodel = InstitutionUsers::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid')));
            $model = Institutions::model()->findByPk($iumodel->institution_id);
            $tgtmodel = TutorGroupsTeachers::model()->findByAttributes(array('teacher_id'=>Yii::app()->user->getState('userid'),'institution_id'=>$model->id));
            $tutormodel = TutorGroups::model()->findByPk($tgtmodel->tutor_group_id);
            $this->render("bulk_upload",array('model'=>$model,'tutormodel'=>$tutormodel,'usermodel'=>new Users()));
        }
	   
	}
	public function actiongetAllocStudents()
	{
	    $insmodel = Institutions::model()->findByAttributes(array('admin_id'=>Yii::app()->user->getState('userid'))); 
	    $tgmodel = TutorGroups::model()->findByPk($_POST['id']);
	    $tgs_cnt = TutorGroupsStudents::model()->countByAttributes(array('tutor_group_id'=>$_POST['id'],'institution_id'=>$insmodel->id));
            //echo $tgs_cnt;die();
	    echo json_encode(array('total'=>$tgmodel->account_limit,'count'=>$tgs_cnt));
	    
	    // echo $tgmodel->account_limit.' / '.$tgs_cnt;
	}
	public function actionsaveBulkStudent()
	{
	    if(Yii::app()->user->getState('role') == "teacher")
	    {
	        $iumodel = InstitutionUsers::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid')));
	        $model = Institutions::model()->findByPk($iumodel->institution_id);
	        $tgtmodel = TutorGroupsTeachers::model()->findByAttributes(array('teacher_id'=>Yii::app()->user->getState('userid'),'institution_id'=>$model->id));
	        $tutormodel = TutorGroups::model()->findByPk($tgtmodel->tutor_group_id);
	        $id = $iumodel->institution_id;
	        $tutorid = $tutormodel->id;
	        $url = $this->createUrl('institutions/teacherdashboard',array('id'=>Yii::app()->user->getState('userid')));
	    }
	    else 
	    {
	        $insmodel = Institutions::model()->findByAttributes(array('admin_id'=>Yii::app()->user->getState('userid')));
	        $id = $insmodel->id;
	        $tutorid = $_POST['TutorGroups']['id'];
	        $url = $this->createUrl('institutions/InsStudents',array('id'=>$id));	        
	    }
	    
	    $now = new DateTime();
	    
	   
        $forenames_arr = $_POST['forenames'];
        array_pop($forenames_arr);
        $surname_arr = $_POST['surname'];
        array_pop($surname_arr);
        $password_arr = $_POST['password'];
        array_pop($password_arr);
        $email_arr = $_POST['email'];
        array_pop($email_arr);
       
        for($i = 0; $i < count($forenames_arr); $i++)
        {
            $umodel = new Users();
            $umodel->forenames = $forenames_arr[$i];
            $umodel->surname = $surname_arr[$i];
            $umodel->password =  password_hash($password_arr[$i], 1,['cost' => 10]);
            $umodel->email = $email_arr[$i];
            $umodel->type = "candidate";
            $umodel->status = "active";
            $model->stripe_active =1;
            $umodel->created_at = $now->format('Y-m-d H:i:s');
            if($umodel->save(false))
            {
                $pmodel = new Profiles();
                $pmodel->owner_id = $umodel->id;
                 $pmodel->visibility = 0;
                $pmodel->owner_type = "Candidate";
                $pmodel->type = "candidate";
                $pmodel->slug = $umodel->forenames."-".$umodel->surname;
                $pmodel->created_at = $now->format('Y-m-d H:i:s');
                $pmodel->save(false);
                
                $tgsmodel = new TutorGroupsStudents();
                $tgsmodel->institution_id = $id;
                $tgsmodel->tutor_group_id = $tutorid;
                $tgsmodel->student_id = $umodel->id;
                $tgsmodel->created_at = $now->format('Y-m-d H:i:s');
                $tgsmodel->save(false);
            }
           
            
        }
	      
        $this->redirect($this->createUrl($url));
	        
	}

	public function actionaddStudent()
	{
        $this->layout='main';
        if(Yii::app()->user->getState('role') == "institution_admin")
        {
            $model = Institutions::model()->findByAttributes(array('admin_id'=>Yii::app()->user->getState('userid')));
            $this->render("addstudent",array('model'=>$model,'tutormodel'=>TutorGroups::model(),'usermodel'=>new Users(),'studmodel'=>new TutorGroupsStudents()));
        }
        else 
        {
            $iumodel = InstitutionUsers::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid')));
            $model = Institutions::model()->findByPk($iumodel->institution_id);
            $tutorgroupteachermodel = TutorGroupsTeachers::model()->findByAttributes(array('teacher_id'=>Yii::app()->user->getState('userid'),'institution_id'=>$model->id));
            $tutormodel = TutorGroups::model()->findByPk($tutorgroupteachermodel->tutor_group_id);
            $tgsmodel = TutorGroupsStudents::model()->findAllByAttributes(array('tutor_group_id'=>$tutormodel->id));
            $this->render("addteacherstudent",array('model'=>$model,'tutormodel'=>$tutormodel,'usermodel'=>new Users(),'tgsmodel'=>$tgsmodel,'studmodel'=>new TutorGroupsStudents()));
        }
	   	   
	}
	public function actionsaveInsStudent()
	{
	    $now = new DateTime();
	    if(Yii::app()->user->getState('role') == "institution_admin")
	    {
	        $insmodel = Institutions::model()->findByAttributes(array('admin_id'=>Yii::app()->user->getState('userid')));
	        $id = $insmodel->id;	        
	        $tutorid = $_POST['TutorGroups']['id'];
	        $url = $this->createUrl('institutions/InsStudents',array('id'=>$id));
	    }
	    else 
	    {
	        $iumodel = InstitutionUsers::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid')));
	        $model = Institutions::model()->findByPk($iumodel->institution_id);
	        $tgtmodel = TutorGroupsTeachers::model()->findByAttributes(array('teacher_id'=>Yii::app()->user->getState('userid'),'institution_id'=>$model->id));
	        $tutormodel = TutorGroups::model()->findByPk($tgtmodel->tutor_group_id);
	        $id = $model->id;	        
	        $tutorid = $tutormodel->id;
	        $url = $this->createUrl('institutions/teacherdashboard',array('id'=>Yii::app()->user->getState('userid')));
	    }
	    	    
	    $tgsmodel = new TutorGroupsStudents();
	    
	    $umodel = new Users();
	    $umodel->attributes = $_POST['Users'];
	    $umodel->type = "candidate";
	    $umodel->status = "active";
	    $umodel->stripe_active = 1;
	    $umodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
	    $umodel->created_at = $now->format('Y-m-d H:i:s');

	    if($umodel->save())
	    {
	        $pmodel = new Profiles();
	        $pmodel->owner_id = $umodel->id;
	        $pmodel->owner_type = "Candidate";
	        $pmodel->visibility = 0;
	        $pmodel->type = "candidate";
	        $pmodel->slug = $this->generateSlug(null,$umodel->forenames."-".$umodel->surname);
	        $pmodel->created_at = $now->format('Y-m-d H:i:s');
	        $pmodel->save();
	          
	        $grad_date = null;
	        $d = str_replace('/', '-', $_POST["graduation_date"]);
	        $grad_date = date('Y-m-d', strtotime($d));
// 	        if($_POST['graduation_date'] != "")
	           // $grad_date = date_format(new DateTime($_POST['graduation_date']),"Y-m-d");
	        
	       
	        $tgsmodel->institution_id = $id;
	        $tgsmodel->tutor_group_id = $tutorid;
	        $tgsmodel->graduation_date = $grad_date;
	        $tgsmodel->student_id = $umodel->id;
	        $tgsmodel->created_at = $now->format('Y-m-d H:i:s');
	        $tgsmodel->save();
	        
	        $this->redirect($url);
	    }
	    
	    if(Yii::app()->user->getState('role') == "institution_admin")
	    {
	        $this->render("addstudent",array('model'=>$insmodel,'tutormodel'=>TutorGroups::model(),'usermodel'=>$umodel,'studmodel'=>$tgsmodel));
	    }
	    else
	    {
	        $iumodel = InstitutionUsers::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid')));
	        $model = Institutions::model()->findByPk($iumodel->institution_id);
	        $tgtmodel = TutorGroupsTeachers::model()->findByAttributes(array('teacher_id'=>Yii::app()->user->getState('userid'),'institution_id'=>$model->id));
	        $tutormodel = TutorGroups::model()->findByPk($tgtmodel->tutor_group_id);
	        $tgsmodel = TutorGroupsStudents::model()->findAllByAttributes(array('tutor_group_id'=>$tutormodel->id));
	        $this->render("addteacherstudent",array('model'=>$model,'tutormodel'=>$tutormodel,'usermodel'=>$umodel,'tgsmodel'=>$tgsmodel,'studmodel'=>$tgsmodel));
	    }
	    
	    
	}
	public function actionmodifyStudent($id)
	{
	    $tgsmodel = TutorGroupsStudents::model()->findByPk($id);
	    $usermodel = Users::model()->findByPk($tgsmodel->student_id);
	    $tgmodel = TutorGroups::model()->findByPk($tgsmodel->tutor_group_id);
	    $model = Institutions::model()->findByPk($tgsmodel->institution_id);
	    $this->render("addstudent",array('model'=>$model,'tutormodel'=>$tgmodel,'usermodel'=>$usermodel,'studmodel'=>$tgsmodel));
	}
	public function actionupdateInsStudent($id,$target = "")
	{
	    $tgsmodel = TutorGroupsStudents::model()->findByPk($id);
	    $usermodel = Users::model()->findByPk($tgsmodel->student_id);
	    $tgmodel = TutorGroups::model()->findByPk($tgsmodel->tutor_group_id);
	    $model = Institutions::model()->findByPk($tgsmodel->institution_id);
	    $pwderr = "";
	   
	        $id = $tgsmodel->institution_id;
    	    $now = new DateTime();
    	    $tutorid = $tgsmodel->tutor_group_id;
    	    
    	    $usermodel = Users::model()->findByPk($tgsmodel->student_id);
    	    $pwd = $usermodel->password;
    	    $usermodel->attributes = $_POST['Users'];
    	    if(trim($_POST['Users']['password']) == "")
    	        $usermodel->password = $pwd;
    	    else 
    	        $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
    	    $usermodel->updated_at = $now->format('Y-m-d H:i:s');
    	    if($usermodel->save())
    	    {
    	        $grad_date = null;
    	        if($_POST['graduation_date'] != "")
    	        {
    	            $d = str_replace('/', '-', $_POST["graduation_date"]);
    	            $grad_date = date('Y-m-d', strtotime($d));
    	        }
//     	            $grad_date = date_format(new DateTime($_POST['graduation_date']),"Y-m-d");
    	          
    	            $tgsmodel->tutor_group_id = $tutorid;
    	            $tgsmodel->graduation_date = $grad_date;
    	            $tgsmodel->updated_at = $now->format('Y-m-d H:i:s');
    	           if($tgsmodel->save())
    	           {
    	               if(Yii::app()->user->getState('role') == "institution_admin")
    	               {
        	               if($target != "")
    	                       $this->redirect($this->createUrl($target));
    	                   else
        	                   $this->redirect($this->createUrl('institutions/InsStudents',array('id'=>$id)));
    	               }
    	               else 
    	               {
    	                   $this->redirect($this->createUrl('institutions/teacherdashboard',array('id'=>Yii::app()->user->getState('userid'))));
    	               }
    	           }
    	    }
    	    
    	    if(Yii::app()->user->getState('role') == "institution_admin")
    	    {
    	        $this->render("addstudent",array('model'=>$model,'tutormodel'=>$tgmodel,'usermodel'=>$usermodel,'studmodel'=>$tgsmodel));
    	    }
        	else
        	{
        	    $iumodel = InstitutionUsers::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid')));
        	    $model = Institutions::model()->findByPk($iumodel->institution_id);
        	    $tgtmodel = TutorGroupsTeachers::model()->findByAttributes(array('teacher_id'=>Yii::app()->user->getState('userid'),'institution_id'=>$model->id));
        	    $tutormodel = TutorGroups::model()->findByPk($tgtmodel->tutor_group_id);
        	    $tgsmodel = TutorGroupsStudents::model()->findAllByAttributes(array('tutor_group_id'=>$tutormodel->id));
        	    $this->render("addteacherstudent",array('model'=>$model,'tutormodel'=>$tutormodel,'usermodel'=>$usermodel,'tgsmodel'=>$tgsmodel,'studmodel'=>$tgsmodel));
        	}
	}
	public function actiondeleteStudents()
	{
	    $idstring = implode(',', $_POST['ids']);
	    $ids = $_POST['ids'];
	    $now = new DateTime();
	    
	    for($i = 0; $i < count($ids); $i++)
	    {	        
	        $tgsmodel = TutorGroupsStudents::model()->findByPk($ids[$i]);
	        Users::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')),'id =:id',array(':id'=>$tgsmodel->student_id));
	    }
	    
	    $crit = new CDbCriteria();
	    $crit->condition = "id in (".$idstring.")";
	    TutorGroupsStudents::model()->deleteAll($crit);
	}
	public function actioncheckEmails(){
	    $index = array();
	    $cnt = 1;
        foreach ($_POST['emails'] as $email)
        {
            $cri = new CDbCriteria();
            $cri->condition = "email ='".$email."'";
            $model = Users::model()->find($cri);
            if($model != null)
                $index[] = $cnt;
                
            $cnt++;
        }
        if(count($index) > 0)
         echo json_encode($index);
        else 
            echo -1;
    }
    public function actionteacherdashboard($id)
    {
        $this->layout='main';
//         $insusrmodel = InstitutionUsers::model()->findByPk($id);
//         $insmodel = Institutions::model()->findByPk($insusrmodel->institution_id);
        $usermodel = Users::model()->findByPk($id);
        
       $tgmodel = new TutorGroups();//::model()->findByAttributes(array('teacher_id'=>$id));
        
       $iumodel = InstitutionUsers::model()->findByAttributes(array('user_id'=>$id,'role'=>'teacher'));
       $insmodel = Institutions::model()->findByPk($iumodel->institution_id);
        
        $this->render("//teachers/dashboard",array('model'=>$insmodel,'usermodel'=>$usermodel,'tutorgroup'=>$tgmodel));
        
    }
    public function actionsaveMessage() {
        $this->layout = "main";
        $conid = $_POST['id'];
        $fromuser = Yii::app()->user->getState('userid');
        $touser = $_POST['msg_to'];
        $msg = $_POST['message'];
        $now = new DateTime();
        $convmodel = Conversations::model()->findByPk($_POST['id']);
        $usermodel = Users::model()->findByPk($fromuser);
        $convmsgmodel = new ConversationMessages();
        $convmsgmodel->conversation_id = $conid;
        $convmsgmodel->user_id = $fromuser;
        $convmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
        $convmsgmodel->message = $msg;
        $convmsgmodel->created_at = $now->format('Y-m-d H:i:s');
        $convmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($convmsgmodel->save()) {
            $conmemmodel_sender = new ConversationMembers();
            $conmemmodel_sender->conversation_id = $convmodel->id;
            $conmemmodel_sender->user_id = Yii::app()->user->getState('userid');
            $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
                        $conmemmodel_sender->last_read = NULL;
//            $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
            $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
            $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
            if ($conmemmodel_sender->save()) {
                $usermodel1 = Users::model()->findByPk($_POST['msg_to']);
                $conmemmodel_rec = new ConversationMembers();
                $conmemmodel_rec->conversation_id = $convmodel->id;
                $conmemmodel_rec->user_id = $_POST['msg_to'];
                $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
                $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                if ($conmemmodel_rec->save()) {
                        $activitymodel = new Activities();
                        $activitymodel->user_id = $fromuser;
                        $activitymodel->type = "message";
                        $activitymodel->model_id = $convmsgmodel->id;
                        $activitymodel->model_type = "App\Messaging\ConversationMessage";
                        $activitymodel->subject = "You sent a message in conversation: " . $convmodel->subject;
                        $activitymodel->message = $msg;
                        $activitymodel->created_at = $now->format('Y-m-d H:i:s');
                        $activitymodel->updated_at = $now->format('Y-m-d H:i:s');
                        $activitymodel->save();
                        $activitymodel = new Activities();
                        $activitymodel->user_id = $touser;
                        $activitymodel->type = "message";
                        $activitymodel->model_id = $convmsgmodel->id;
                        $activitymodel->model_type = "App\Messaging\ConversationMessage";
                        $activitymodel->subject = "You sent a message in conversation: " . $convmodel->subject;
                        $activitymodel->message = $msg;
                        $activitymodel->created_at = $now->format('Y-m-d H:i:s');
                        $activitymodel->updated_at = $now->format('Y-m-d H:i:s');
                        $activitymodel->save();
                }
           }
        }
        Yii::app()->user->setFlash('success', 'Message sent successfully');
        $this->renderPartial('inbox_messages', array('convmodel' => $convmodel, 'to_user' => $_POST['msg_to']));
    }
    
    public function actionInbox() {
        $this->layout = "main";
//         $insusrmodel = InstitutionUsers::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid')));
//         $insmodel = Institutions::model()->findByPk($insusrmodel->institution_id);
        $model = Users::model()->findByPk(Yii::app()->user->getState('userid'));
        $tgtmodel = TutorGroupsTeachers::model()->findByAttributes(array('teacher_id'=>Yii::app()->user->getState('userid')));
        $tgmodel = TutorGroups::model()->findByPk($tgtmodel->tutor_group_id);
        $this->render("inbox", array('model' => $model,'tgmodel'=>$tgmodel));
    }
    
    public function actionsendMessage() {
        
        $now = new DateTime();
        $conmodel = new Conversations();
        $conmodel->subject = $_POST['subject'];
        $conmodel->created_at = $now->format('Y-m-d H:i:s');
        $conmodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($conmodel->save()) {
            $usermodel = Users::model()->findByPk(Yii::app()->user->getState('userid'));
            $conmsgmodel = new ConversationMessages();
            $conmsgmodel->conversation_id = $conmodel->id;
            $conmsgmodel->user_id = Yii::app()->user->getState('userid');
            $conmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
            $conmsgmodel->message = $_POST['message'];
            $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
            $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($conmsgmodel->save()) {
                $conmemmodel_sender = new ConversationMembers();
                $conmemmodel_sender->conversation_id = $conmodel->id;
                $conmemmodel_sender->user_id = Yii::app()->user->getState('userid');
                $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
                $conmemmodel_sender->last_read = NULL;
//                $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
                $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                if ($conmemmodel_sender->save()) {
                    $usermodel1 = Users::model()->findByPk($_POST['user_id']);
                    $conmemmodel_rec = new ConversationMembers();
                    $conmemmodel_rec->conversation_id = $conmodel->id;
                    $conmemmodel_rec->user_id = $_POST['user_id'];
                    $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
                    $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                    $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                    if ($conmemmodel_rec->save()) {
                        $actmodel_sender = new Activities();
                        $actmodel_sender->user_id = Yii::app()->user->getState('userid');
                        $actmodel_sender->type = "message";
                        $actmodel_sender->model_id = $conmsgmodel->id;
                        $actmodel_sender->model_type = "ConversationMessage";
                        $actmodel_sender->subject = "You sent a message in conversation: " . $_POST['subject'];
                        $actmodel_sender->message = $_POST['message'];
                        $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                        $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                        if ($actmodel_sender->save()) {
                            $actmodel_rec = new Activities();
                            $actmodel_rec->user_id = $_POST['user_id'];
                            $actmodel_rec->type = "message";
                            $actmodel_rec->model_id = $conmsgmodel->id;
                            $actmodel_rec->model_type = "ConversationMessage";
                            $actmodel_rec->subject = "You received a message in conversation: " . $_POST['subject'];
                            $actmodel_rec->message = $_POST['message'];
                            $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                            $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                            $actmodel_rec->save();
                        }
                    }
                }
            }
        }
    }
    public function getValidatedSlug($slug)
    {
        $cri = new CDbCriteria();
        $cri->condition = "slug ='".$slug."'";
        $pmodel = Profiles::model()->findAll($cri);
        
        $cri1 = new CDbCriteria();
        $cri1->condition = "slug like '".$slug."-%'";
        $pmodel1 = Profiles::model()->findAll($cri1);
        
        if(count($pmodel1) == 0 && count($pmodel) == 1)
        {
            return strtolower($slug)."-1";
        }
        else if(count($pmodel1) > 0)
        {
            return (strtolower($slug)."-".(count($pmodel1)+1));
        }
        else if(count($pmodel) == 0)
            return strtolower($slug);
        else 
            return (strtolower($slug)."-".rand());
        
    }
    public function actiondeleteTutor()
    {
        $tgmodel = TutorGroups::model()->findByPk($_POST['id']);
        $tgsmodel = TutorGroupsStudents::model()->findAllByAttributes(array('tutor_group_id'=>$_POST['id']));
        foreach ($tgsmodel as $stud)
        {
            Profiles::model()->deleteAllByAttributes(array('owner_id'=>$stud['student_id'],'type'=>'candidate'));
            Users::model()->deleteAllByAttributes(array('id'=>$stud['student_id']));
        }
        TutorGroupsStudents::model()->deleteAllByAttributes(array('tutor_group_id'=>$_POST['id']));
        TutorGroups::model()->deleteAllByAttributes(array('id'=>$_POST['id']));
    }
    public function actiondeleteTeacher($id)
    {
       $now = new DateTime();
       InstitutionUsers::model()->deleteAllByAttributes(array('user_id'=>$id,'role'=>'teacher'));
       $umodel = Users::model()->updateByPk($id,array('deleted_at'=>$now->format('Y-m-d H:i:s')));
       Profiles::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')),'owner_id =:owner_id',array(':owner_id'=>$id));
       
       $this->redirect(Yii::app()->createUrl('institutions/teachers'));
    }
    public function actiondeleteTutorGroup($id)
    {
        $now = new DateTime();        
        TutorGroups::model()->updateByPk($id,array('deleted_at'=>$now->format('Y-m-d H:i:s')));        
        $this->redirect(Yii::app()->createUrl('institutions/tutorgroups'));
    }
   public function actionteacherClass($id)
   {
       $model = Institutions::model()->findByPk($id);
       $this->render("teacherclass",array('model'=>$model,'usermodel'=>new Users(),'tutormodel'=>new TutorGroups()));
   }
   public function actionteacherSaveClass()
   {
       $iumodel = InstitutionUsers::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid'),'role'=>'teacher'));
       $model = Institutions::model()->findByPk($iumodel->institution_id);
       $usermodel = Users::model()->findByPk($iumodel->user_id);
       $id = $iumodel->institution_id;
       $now = new DateTime();
       $tmodel = new Users();
       $tgmodel = new TutorGroups();
       $tgmodel->attributes = $_POST['TutorGroups'];
       $grad_date = null;
       if($_POST['graduation_date'] != "")
       {
           // 	        $grad_date = date_format(new DateTime($_POST['graduation_date']),"Y-m-d");
           $d = str_replace('/', '-', $_POST["graduation_date"]);
           $grad_date = date('Y-m-d', strtotime($d));
       }
       $tgmodel->teacher_id = Yii::app()->user->getState('userid');
       $tgmodel->institution_id = $id;
       $tgmodel->graduation_date= $grad_date;
       $tgmodel->created_at = $now->format('Y-m-d H:i:s');
       $tgmodel->account_limit = $_POST['allocated_students'];
       if($tgmodel->save())
       {

       		$tgtmodel = new TutorGroupsTeachers();
           $tgtmodel->teacher_id = Yii::app()->user->getState('userid');
           $tgtmodel->institution_id = $id;
           $tgtmodel->tutor_group_id = $tgmodel->id;
           $tgtmodel->created_at = $now->format('Y-m-d H:i:s');
           $tgtmodel->save();

           $forenames_arr = $_POST['forenames'];
           array_pop($forenames_arr);
           $surname_arr = $_POST['surname'];
           array_pop($surname_arr);
           $email_arr = $_POST['email'];
           array_pop($email_arr);
           $password_arr = $_POST['password'];
           array_pop($password_arr);
           // $confirmpwd_arr = $_POST['confirmpassword'];
           
           
           // for($i = 0; $i < $_POST['allocated_students']; $i++)
           for($i = 0; $i < count($forenames_arr); $i++)
           {
               $umodel = new Users();
               $umodel->forenames = $forenames_arr[$i];
               $umodel->surname = $surname_arr[$i];
               $umodel->password = password_hash($password_arr[$i], 1,['cost' => 10]);
               $umodel->email = $email_arr[$i];
               $umodel->type = "candidate";
               $umodel->stripe_active = 1;
               $umodel->status = "active";
               $umodel->created_at = $now->format('Y-m-d H:i:s');
               if($umodel->save(false))
               {
                   $pmodel = new Profiles();
                   $pmodel->owner_id = $umodel->id;
                   $pmodel->owner_type = "Candidate";
                   $pmodel->type = "candidate";
                    $pmodel->visibility = 0;
                    $pmodel->slug = $this->generateSlug(null,$umodel->forenames."-".$umodel->surname);
                   $pmodel->created_at = $now->format('Y-m-d H:i:s');
                   $pmodel->save(false);
                   
                   $tgsmodel = new TutorGroupsStudents();
                   $tgsmodel->institution_id = $id;
                   $tgsmodel->tutor_group_id = $tgmodel->id;
                   $tgsmodel->student_id = $umodel->id;
                   $tgsmodel->graduation_date = $grad_date;
                   $tgsmodel->created_at = $now->format('Y-m-d H:i:s');
                   $tgsmodel->save(false);
               }
               
           }
           $this->redirect($this->createUrl('institutions/teacherdashboard',array('id'=>Yii::app()->user->getState('userid'))));
       }
       
       $this->render("teacherclass",array('model'=>$model,'tutormodel'=>$tgsmodel));
       
   }
   
//    public function actionrequestForm()
//    {
//        $uid = Yii::app()->user->getState('userid');
//        $insmodel = Institutions::model()->findByAttributes(array('admin_id'=>$uid)); 
//        $this->render("requestForm",array('model'=>$insmodel));
//    }
   public function actionupgradeInstitution($id)
   {
       $adminid = Yii::app()->user->getState('userid');
       $umodel = Users::model()->findByPk($adminid);
       $insmodel = Institutions::model()->findByPk($id);
       $msg = "<b>Institution Upgrade Details</b><br>";
       $msg .= "Number Of Students : ". $_POST['number_students']."<br>";
       $msg .= "Payment Type : ". ($_POST['payment_option'] == 'month' ? '6 months' : '1 year' )."<br>";
       
       Institutions::model()->updateByPk($id, array('payment_type'=>$_POST['payment_option'],'num_pupils'=>$_POST['number_students']));
      
       $datetime = new DateTime();      //default 1 year
       $datetime->add(new DateInterval('P1Y'));
       $trial = $datetime->format('Y-m-d H:i:s');  
       
        $amount = 0;
       
       if($_POST['payment_option'] == "free")
       {
           if($_POST['free_date'] == 'month')     //6 months        
           {
               $datetime = new DateTime();
               $datetime->add(new DateInterval('P6M'));
               $trial = $datetime->format('Y-m-d H:i:s');
           }
           
           $msg .= "Subscription Duration : ". $_POST['free_date']."<br>";
       }
       else if($_POST['payment_option'] == "fixed")       
       {
           Institutions::model()->updateByPk($id, array('amount'=>$_POST['amount']));
           $msg .= "Amount : ". $_POST['amount']."<br>";
           $amount = $_POST['amount'];
       }
       else if($_POST['payment_option'] == "specific")
       {
           Institutions::model()->updateByPk($id, array('amount'=>$_POST['amount'],'additional_amount'=>$_POST['additional_amount']));
           $msg .= "Amount : ". $_POST['amount']."<br>";
           $msg .= "Additional Amount : ". $_POST['additional_amount']."<br>";
           
           $amount = (int)$_POST['amount'] + (int)$_POST['additional_amount'];
       }
       
       Institutions::model()->updateByPk($id, array('trial_ends_at'=>$trial,'status'=>$_POST['status']));
       Users::model()->updateByPk($insmodel->admin_id, array('status'=>'active'));
       /* 
       $now = new DateTime();
       $planmodel = new Plans();
       $planmodel->price = $amount;
       $planmodel->active_to = $trial;
       $planmodel->owner_type = "Institution";
       $planmodel->owner_id = $id;
       $planmodel->name = $insmodel->name;
       $planmodel->stripe_plan = strtolower(str_replace(' ', '_', $insmodel->name));
       $planmodel->quantity = $_POST['number_students'];
       $planmodel->created_at = $now->format('Y-m-d H:i:s');
       $planmodel->save();
        */
       $body = $msg;
       // Send Upgrade Requset mail to Admin
       Yii::import('application.extensions.phpmailer.JPhpMailer');
       $mail = new JPhpMailer;
       $mail->Host = 'mail.cvvid.com';
       $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
       $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
       $mail->SMTPSecure = 'ssl';
       $mail->SetFrom($umodel->email, $umodel->forenames." ".$umodel->surname);
       $mail->Subject = 'Upgrade Request Details';
       $mail->MsgHTML($body);
       $mail->AddAddress($insmodel->email);
       $mail->Send();
     
       //Yii::app()->user->setFlash('success', 'Mail sent successfully!');
       
       Yii::app()->user->setFlash('success', ' Successfully Approved institution');
       $this->redirect($this->createUrl('institutions/pending',array('id'=>$insmodel->id)));
       //$this->redirect($this->createUrl('institutions/approve',array('id'=>$id)));
   }
   
   
   public function str_slug($title, $separator = '-')
   {
       
       // $title = static::ascii($title);
       
       // Convert all dashes/underscores into separator
       $flip = $separator == '-' ? '_' : '-';
       
       $title = preg_replace('!['.preg_quote($flip).']+!u', $separator, $title);
       
       // Remove all characters that are not the separator, letters, numbers, or whitespace.
       $title = preg_replace('![^'.preg_quote($separator).'\pL\pN\s]+!u', '', mb_strtolower($title));
       
       // Replace all separator characters and whitespace by a single separator
       $title = preg_replace('!['.preg_quote($separator).'\s]+!u', $separator, $title);
       
       return trim($title, $separator);
       
       
       
       // return Str::slug($title, $separator);
   }
   
   
   public function generateSlug($id = null,$slug)
   {
       $cri = new CDbCriteria();
       $cri->condition = "slug REGEXP '^$slug(-[0-9]*)?$'";
       $pmdoel = Profiles::model()->findAll($cri);
       
       $cri1 = new CDbCriteria();
       $cri1->condition = "slug REGEXP '^$slug(-[0-9]*)?$' and id != ".$id;
       $index = ($id) ? count(Profiles::model()->findAll($cri1)) : count($pmdoel);
       
       return ($index > 0) ? "{$slug}-{$index}" : $slug;
       
   }
}
