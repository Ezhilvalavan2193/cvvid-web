<?php

class MediaController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Media;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Media']))
		{
			$model->attributes=$_POST['Media'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Media']))
		{
			$model->attributes=$_POST['Media'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
        
        /**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actioninsertMedia()
        {
               $filename = $_FILES['media']['name'];
               $image_type = $_FILES['media']['type'];
               $file = $_FILES['media']['tmp_name'];
               $name = pathinfo($_FILES['media']['name'], PATHINFO_FILENAME);
               
               $mediamodel = new Media;
               
               $cri = new CDbCriteria();
               $cri->select = 'max(order_column) as order_column';
               $value = $mediamodel->find($cri);
               
               $now = new DateTime();
               
               $mediamodel->model_id = 1;
               $mediamodel->name = $name;
               $mediamodel->file_name = $filename;
               $mediamodel->size = $_FILES['media']['size'];
               $mediamodel->model_type = 'App\Site';
               $mediamodel->collection_name = $_POST['collection_name'];
               $mediamodel->order_column = $value->order_column + 1;
               $mediamodel->created_at = $now->format('Y-m-d H:i:s');
               if ($mediamodel->save()) {
                   
                    $mediafolder = "images/media/" . $mediamodel->id . "/conversions";

                    if (!file_exists($mediafolder)) {
                        mkdir($mediafolder, 0777, true);
                    }

                   $imgprefix = "images/media/" . $mediamodel->id;
                   $fullpath = "images/media/" . $mediamodel->id . "/" . $filename;

                   if (move_uploaded_file($_FILES['media']['tmp_name'], $fullpath)) {
                       //create cover
                       $coverwidth = 1000;
                       $coverheight = 400;
                       $coverdestpath = $imgprefix . "/conversions/cover.jpg";

                       //create icon
                       $iconwidth = 40;
                       $iconheight = 40;
                       $icondestpath = $imgprefix . "/conversions/icon.jpg";

                       //create joblisting
                       $jlwidth = 150;
                       $jlheight = 85;
                       $jldestpath = $imgprefix . "/conversions/joblisting.jpg";

                       //create search
                       $searchwidth = 167;
                       $searchheight = 167;
                       $searchdestpath = $imgprefix . "/conversions/search.jpg";

                       //create thumb
                       $thumbwidth = 126;
                       $thumbheight = 126;
                       $thumbdestpath = $imgprefix . "/conversions/thumb.jpg";

                       if ($image_type == "image/jpg" || $image_type == "image/jpeg") {

                           $image = imagecreatefromjpeg($fullpath);

                           $this->resize_image($image, $image_type, $fullpath, $coverdestpath, $coverwidth, $coverheight); //create cover
                           $this->resize_image($image, $image_type, $fullpath, $icondestpath, $iconwidth, $iconheight); //create icon
                           $this->resize_image($image, $image_type, $fullpath, $jldestpath, $jlwidth, $jlheight); //create joblisting
                           $this->resize_image($image, $image_type, $fullpath, $searchdestpath, $searchwidth, $searchheight); //create search
                           $this->resize_image($image, $image_type, $fullpath, $thumbdestpath, $thumbwidth, $thumbheight); //create thumb
                       } elseif ($image_type == "image/gif") {

                           $image = imagecreatefromgif($fullpath);

                           $this->resize_image($image, $image_type, $fullpath, $coverdestpath, $coverwidth, $coverheight); //create cover
                           $this->resize_image($image, $image_type, $fullpath, $icondestpath, $iconwidth, $iconheight); //create icon
                           $this->resize_image($image, $image_type, $fullpath, $jldestpath, $jlwidth, $jlheight); //create joblisting
                           $this->resize_image($image, $image_type, $fullpath, $searchdestpath, $searchwidth, $searchheight); //create search
                           $this->resize_image($image, $image_type, $fullpath, $thumbdestpath, $thumbwidth, $thumbheight); //create thumb
                       } elseif ($image_type == "image/png") {

                           $image = imagecreatefrompng($fullpath);

                           $this->resize_image($image, $image_type, $fullpath, $coverdestpath, $coverwidth, $coverheight); //create cover
                           $this->resize_image($image, $image_type, $fullpath, $icondestpath, $iconwidth, $iconheight); //create icon
                           $this->resize_image($image, $image_type, $fullpath, $jldestpath, $jlwidth, $jlheight); //create joblisting
                           $this->resize_image($image, $image_type, $fullpath, $searchdestpath, $searchwidth, $searchheight); //create search
                           $this->resize_image($image, $image_type, $fullpath, $thumbdestpath, $thumbwidth, $thumbheight); //create thumb
                       }
                   }
                   
                   echo Yii::app()->baseUrl."/".$fullpath;
               }
               
              // echo $filename;
           }
           
           
       /**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actioninsertMediaManager()
        {
               $filename = $_FILES['media']['name'];
               $image_type = $_FILES['media']['type'];
               $file = $_FILES['media']['tmp_name'];
               $name = pathinfo($_FILES['media']['name'], PATHINFO_FILENAME);
               
               $mediamodel = new Media;
               
               $cri = new CDbCriteria();
               $cri->select = 'max(order_column) as order_column';
               $value = $mediamodel->find($cri);
               
               $now = new DateTime();
               
               $mediamodel->model_id = $_POST['model_id'];
               $mediamodel->name = $name;
               $mediamodel->file_name = $filename;
               $mediamodel->size = $_FILES['media']['size'];
               $mediamodel->model_type = $_POST['model_type'];
               $mediamodel->collection_name = $_POST['collection_name'];
               $mediamodel->order_column = $value->order_column + 1;
               $mediamodel->created_at = $now->format('Y-m-d H:i:s');
               if ($mediamodel->save()) {
                   
                    $mediafolder = "images/media/" . $mediamodel->id . "/conversions";

                    if (!file_exists($mediafolder)) {
                        mkdir($mediafolder, 0777, true);
                    }

                   $imgprefix = "images/media/" . $mediamodel->id;
                   $fullpath = "images/media/" . $mediamodel->id . "/" . $filename;

                   if (move_uploaded_file($_FILES['media']['tmp_name'], $fullpath)) {
                       //create cover
                       $coverwidth = 1000;
                       $coverheight = 400;
                       $coverdestpath = $imgprefix . "/conversions/cover.jpg";

                       //create icon
                       $iconwidth = 40;
                       $iconheight = 40;
                       $icondestpath = $imgprefix . "/conversions/icon.jpg";

                       //create joblisting
                       $jlwidth = 150;
                       $jlheight = 85;
                       $jldestpath = $imgprefix . "/conversions/joblisting.jpg";

                       //create search
                       $searchwidth = 167;
                       $searchheight = 167;
                       $searchdestpath = $imgprefix . "/conversions/search.jpg";

                       //create thumb
                       $thumbwidth = 126;
                       $thumbheight = 126;
                       $thumbdestpath = $imgprefix . "/conversions/thumb.jpg";

                       if ($image_type == "image/jpg" || $image_type == "image/jpeg") {

                           $image = imagecreatefromjpeg($fullpath);

                           $this->resize_image($image, $image_type, $fullpath, $coverdestpath, $coverwidth, $coverheight); //create cover
                           $this->resize_image($image, $image_type, $fullpath, $icondestpath, $iconwidth, $iconheight); //create icon
                           $this->resize_image($image, $image_type, $fullpath, $jldestpath, $jlwidth, $jlheight); //create joblisting
                           $this->resize_image($image, $image_type, $fullpath, $searchdestpath, $searchwidth, $searchheight); //create search
                           $this->resize_image($image, $image_type, $fullpath, $thumbdestpath, $thumbwidth, $thumbheight); //create thumb
                       } elseif ($image_type == "image/gif") {

                           $image = imagecreatefromgif($fullpath);

                           $this->resize_image($image, $image_type, $fullpath, $coverdestpath, $coverwidth, $coverheight); //create cover
                           $this->resize_image($image, $image_type, $fullpath, $icondestpath, $iconwidth, $iconheight); //create icon
                           $this->resize_image($image, $image_type, $fullpath, $jldestpath, $jlwidth, $jlheight); //create joblisting
                           $this->resize_image($image, $image_type, $fullpath, $searchdestpath, $searchwidth, $searchheight); //create search
                           $this->resize_image($image, $image_type, $fullpath, $thumbdestpath, $thumbwidth, $thumbheight); //create thumb
                       } elseif ($image_type == "image/png") {

                           $image = imagecreatefrompng($fullpath);

                           $this->resize_image($image, $image_type, $fullpath, $coverdestpath, $coverwidth, $coverheight); //create cover
                           $this->resize_image($image, $image_type, $fullpath, $icondestpath, $iconwidth, $iconheight); //create icon
                           $this->resize_image($image, $image_type, $fullpath, $jldestpath, $jlwidth, $jlheight); //create joblisting
                           $this->resize_image($image, $image_type, $fullpath, $searchdestpath, $searchwidth, $searchheight); //create search
                           $this->resize_image($image, $image_type, $fullpath, $thumbdestpath, $thumbwidth, $thumbheight); //create thumb
                       }
                   }
                   
                   echo Yii::app()->baseUrl."/".$fullpath;
               }
               
              // echo $filename;
           }
           
           
           /**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
        public function actiondeleteMedia() {

            $id = $_POST['id'];
            $mediamodel = Media::model()->deleteByPk($id);
            
            $files = 'images/media/'.$id; // get all file names
            
            //call our function
            $this->delete_directory($files);

        }
        
        function delete_directory($dirname) {
            if (is_dir($dirname))
                $dir_handle = opendir($dirname);
            if (!$dir_handle)
                return false;
            while ($file = readdir($dir_handle)) {
                if ($file != "." && $file != "..") {
                    if (!is_dir($dirname . "/" . $file))
                        unlink($dirname . "/" . $file);
                    else
                        $this->delete_directory($dirname . '/' . $file);
                }
            }
            closedir($dir_handle);
            rmdir($dirname);
            return true;
        }

        function resize_image($image,$image_type,$fullpath,$destpath,$width,$height)
	{		
		$size_arr = getimagesize($fullpath);
		
	 		list($width_orig, $height_orig, $img_type) = $size_arr;
	    	$ratio_orig = $width_orig/$height_orig;  
	    			
	    	$tempimg = imagecreatetruecolor($width, $height);
	    	imagecopyresampled($tempimg, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
	    	
	    	if($image_type == "image/jpg" || $image_type == "image/jpeg") 
	    		imagejpeg($tempimg, $destpath);	    		
	    	else if( $image_type == "image/png" )  
	    		imagepng($tempimg, $destpath);
	    	else if( $image_type == "image/gif" )
	    		imagegif($tempimg, $destpath);
	}

           /**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Media');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Media('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Media']))
			$model->attributes=$_GET['Media'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
        
        public function actionsearchAdmin()
	{
	    $model=new Media('search');
	    $model->unsetAttributes();  // clear any default values
	    if(isset($_GET['Media']))
	    {
	        $model->attributes=$_GET['Media'];	        
	    } 
	    
            $this->render('admin',array(
                'model'=>$model,'issearch'=>1));
            }
            
            
           public function actiongetLoadData() {
                
                $no = $_POST['getresult'];
                
                $cri = new CDbCriteria();
                $cri->limit = 20;
                $cri->offset = $no;
                $cri->order = 'order_column DESC';
                $model = Media::model()->findAll($cri);
                foreach ($model as $media) {
                    if(file_exists('images/media/' . $media['id'] . '/conversions/thumb.jpg'))
                    {
                        echo '<div class = "MediaAdmin__item">';
                        echo '<div class = "MediaAdmin__item__inner">';
                        echo '<img id = "img-media" src ="'.Yii::app()->baseUrl . '/images/media/' . $media['id'] . '/conversions/thumb.jpg" alt = "' . $media['name'] . '" class = "img-responsive">';
                        echo '<button class = "MediaAdmin__item__delete" id = "' . $media['id'] . '" type = "button"><i class = "fa fa-close"></i></button>';
                        echo '</div>';
                        echo '</div>';
                    }
                }
            }
            
            
            public function actiongetData() {
                
                $val = $_POST['val'];
                
                $cri = new CDbCriteria();
                $cri->addSearchCondition('name', $val);
                $model = Media::model()->findAll($cri);
                foreach ($model as $media) {
                    if(file_exists('images/media/' . $media['id'] . '/conversions/thumb.jpg'))
                    {
                        echo '<div class = "MediaAdmin__item">';
                        echo '<div class = "MediaAdmin__item__inner">';
                        echo '<img id = "img-media" src ="'.Yii::app()->baseUrl . '/images/media/' . $media['id'] . '/conversions/thumb.jpg" alt = "' . $media['name'] . '" class = "img-responsive">';
                        echo '<button class = "MediaAdmin__item__delete" id = "' . $media['id'] . '" type = "button"><i class = "fa fa-close"></i></button>';
                        echo '</div>';
                        echo '</div>';
                    }
                }
            }

    /**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Media the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Media::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Media $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='media-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function beforeAction($action)
	{
	    Yii::app()->user->setReturnUrl(Yii::app()->request->getUrl());
	    $requestUrl=Yii::app()->request->url;
	    if (Yii::app()->user->isGuest)
	        $this->redirect(Yii::app()->createUrl('site/login'));
	        
	        return parent::beforeAction($action);
	} 
}
