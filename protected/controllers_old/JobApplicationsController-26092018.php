<?php

class JobApplicationsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new JobApplications;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['JobApplications']))
		{
			$model->attributes=$_POST['JobApplications'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['JobApplications']))
		{
			$model->attributes=$_POST['JobApplications'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
        
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionupdateStatus()
	{
            $application_id  = $_POST['application_id'];
            $value = $_POST['value'];
            
            $status = 0;
            if($value == 'pending'){
               $status = 0;
            } else if($value == 'shortlist'){
               $status = 1; 
            }else if($value == 'invite'){
               $status = 2; 
            } else if($value == 'interview'){
               $status = 3; 
            } else if($value == 'reject'){
               $status = -1; 
            }
            
           
            $model = JobApplications::model()->findByPk($application_id);
            $model->status = $status;
            if($model->save())
            {                
                $jmodel = Jobs::model()->findByPk($model->job_id);  
                
                $umodel = Users::model()->findByPk($model->user_id);
                if($value == 'shortlist')
                {
                    $this->sendMessage("You are Shortlisted", "Congratulations, You have been shortlisted for ".$jmodel->title,Yii::app()->user->getState('userid'),$model->user_id);
                    
                                      
                    $body = $this->renderPartial("//emails/shortlisted", array('model'=>$umodel), true);
                    // Send employer viewed notifcation to candidate
                    Yii::import('application.extensions.phpmailer.JPhpMailer');
                    $mail = new JPhpMailer;
                    $mail->Host = 'smtp.gmail.com';
                    $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
                    $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
                    $mail->SMTPSecure = 'ssl';
                    $mail->SetFrom('mail@cvvid.com', 'CVVID');
                    $mail->Subject = "Congratulations, You have been shortlisted for ".$jmodel->title;
                    $mail->MsgHTML($body);
                    $mail->AddAddress($umodel->email, $umodel->forenames);
                    $mail->Send();
                }
                else if($value == 'reject')
                {
                    $this->sendMessage("You are Rejected","Sorry to inform that you didn't get shortlisted this time",Yii::app()->user->getState('userid'),$model->user_id);
                    
                    $body = $this->renderPartial("//emails/rejected", array('model'=>$umodel), true);
                    // Send employer viewed notifcation to candidate
                    Yii::import('application.extensions.phpmailer.JPhpMailer');
                    $mail = new JPhpMailer;
                    $mail->Host = 'smtp.gmail.com';
                    $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
                    $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
                    $mail->SMTPSecure = 'ssl';
                    $mail->SetFrom('mail@cvvid.com', 'CVVID');
                    $mail->Subject = "Didn't get shortlisted this time";
                    $mail->MsgHTML($body);
                    $mail->AddAddress($umodel->email, $umodel->forenames);
                    $mail->Send();
                }
                else if($value == 'invite')
                {
                    $this->sendMessage("You are invited for interview","You are invited for interview for the position ".$jmodel->title,Yii::app()->user->getState('userid'),$model->user_id);
                    
                }
                else if($value == 'interview')
                {
                    $this->sendMessage("Your interview process completed ","Your interview process has been completed, we will update you on further process",Yii::app()->user->getState('userid'),$model->user_id);
                    
                }
            }
            
            Yii::app()->user->setFlash('success', 'Successfully updated applications');
	}
        
	public function actionupdateReject()
	{
            $application_id  = $_POST['application_id'];
            $msg = $_POST['msg'];
            
            $status = -1; 
                 
            $model = JobApplications::model()->findByPk($application_id);
            $model->status = $status;
            $model->message = $msg;
            if($model->save())
                $this->sendMessage("Sorry,You are Rejected",$msg,Yii::app()->user->getState('userid'),$model->user_id);
	}
        
        
	public function actionupdateInvite()
	{
//             $interview_date  = $_POST['interview_date'];
            $application_id  = $_POST['application_id'];
            $msg = $_POST['msg'];
            
            $status = 2; 
                   
            if($_POST["interview_date"] != "")
            {
                $myDateTime = DateTime::createFromFormat('m/d/Y h:i A', $_POST["interview_date"]);
                $interview_date = $myDateTime->format('Y-m-d H:i:s');
            }
            // $d = str_replace('/', '-', $_POST["interview_date"]);
            // $interview_date = date('Y-m-d H:i:s', strtotime($d));
           
            $model = JobApplications::model()->findByPk($application_id);
            $model->interview_date = $interview_date;
            $model->status = $status;
            $model->message = $msg;
            $model->save();
            
            $jobs = Jobs::model()->findByPk($model->job_id);
            $user = Users::model()->findByPk($model->user_id);
            
            $job = $jobs->title;
            $name = $user->forenames;
            $email = $user->email;
            $message = $msg;
            
            $indate = date("l jS \of F Y h:i:s A", strtotime($interview_date));
            
            $subject = "Your interview has been scheduled for a week on $indate, please confirm that this time is suitable";
                        
            $this->sendMessage($subject,$message,Yii::app()->user->getState('userid'),$user->id);
            
            $this->notifyinterview($job, $name,$email, $message, $subject);
            
             Yii::app()->user->setFlash('success', 'Successfully sent interview invites');
            
	}
        
        /**
     * Notify the user
     */
    public function notifyinterview($job, $name, $email, $message, $subject) {
        
        $body = $this->renderPartial("//emails/interview", array('job' => $job, 'name' => $name,'message'=>$message, 'subject' => $subject), true);
        // Send register notifcation to employer admin
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->Host = 'smtp.gmail.com';
        $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
        $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
        $mail->SMTPSecure = 'ssl';
        $mail->SetFrom('mail@cvvid.com', 'CVVID');
        $mail->Subject = 'You are invited for an interview';
        $mail->MsgHTML($body);
        $mail->AddAddress($email, $name);
        $mail->Send();
    }
        
    public function actionupdateAccept()
    {
        $now = new DateTime();
        $application_id  = $_POST['application_id'];
        $msg  = $_POST['msg'];

        $status = 4; 

        $model = JobApplications::model()->findByPk($application_id);
        $model->status = $status;
        $model->message = $msg;
        $model->accepted_date = $now->format('Y-m-d H:i:s');
        if($model->save())
            $this->sendMessage("You are selected",$message,Yii::app()->user->getState('userid'),$model->user_id);
    }
        
        
        /**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionrejectModal()
	{
            $application_id  = $_POST['application_id'];
            
            echo $this->renderPartial('_rejectmodal', array('application_id' => $application_id));
	}
        /**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actioninterviewModal()
	{
            $application_id  = $_POST['application_id'];
            
            echo $this->renderPartial('_interviewmodal', array('application_id' => $application_id));
	}
        /**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionacceptModal()
	{
            $application_id  = $_POST['application_id'];
            
            echo $this->renderPartial('_acceptmodal', array('application_id' => $application_id));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('JobApplications');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new JobApplications('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['JobApplications']))
			$model->attributes=$_GET['JobApplications'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return JobApplications the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=JobApplications::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param JobApplications $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='job-applications-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function beforeAction($action)
	{
	    Yii::app()->user->setReturnUrl(Yii::app()->request->getUrl());
	    $requestUrl=Yii::app()->request->url;
	    if (Yii::app()->user->isGuest && (strpos($requestUrl, 'verifyCandidate') !== false))
	        $this->redirect(Yii::app()->createUrl('site/login'));
	        
	        return parent::beforeAction($action);
	} 
	public function actionsendReferenceMail()
	{
	    $jobappmodel = JobApplications::model()->findByPk($_POST['job_application_id']);
	    $jobmodel = Jobs::model()->findByPk($jobappmodel->job_id);
	    $jobownermodel = Users::model()->findByPk($jobmodel->user_id);
	    
	    $umodel = Users::model()->findByPk($jobappmodel->user_id);
	  // $tr = "http://".$_SERVER['SERVER_NAME'].Yii::app()->createUrl('jobs/verifyCandidate',array('id'=>$jobappmodel->id));
	 //  $url = "<a href='$tr'>Click here to verify candidate</a>";
	   
	    $body  = "We have received your mail from candidate ".$umodel->forenames." ".$umodel->surname." on applying for job as reference.please reply to this mail if this information is legit";
// 	   $body .= "<p>If you think this is correct, please click on below link to verify.</p>";
	//  $body .= "<p>".$url."</p>";

	    Yii::import('application.extensions.phpmailer.JPhpMailer');
	    $mail = new JPhpMailer;
	    $mail->Host = 'mail.cvvid.com';
	    $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
	    $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
	    $mail->SMTPSecure = 'ssl';
// 	    $mail->SetFrom('mail@cvvid.com', 'CVVID');
	   $mail->SetFrom($jobownermodel->email,$jobownermodel->forenames);
	    $mail->Subject = 'Reference Verfication Mail';
	    $mail->MsgHTML($body);
	    $mail->AddAddress($jobappmodel->reference_mail);
	    if($mail->Send())
	    {	        
	        JobApplications::model()->updateByPk($_POST['job_application_id'], array('reference_status'=>1));	    
	    }
	    $this->redirect($this->createUrl("jobs/verifycandidate",array('id'=>$jobappmodel->id)));
	}
	public function actionsaveReference()
	{
	    Yii::app()->user->setFlash('success', 'Thanks for your feedback.');
	    $this->redirect(Yii::app()->user->returnUrl);
	}
	
	
	public function sendMessage($subject,$msg,$from,$to) {
	    
	    $now = new DateTime();
	    $conmodel = new Conversations();
	    $conmodel->subject = $subject;
	    $conmodel->created_at = $now->format('Y-m-d H:i:s');
	    $conmodel->updated_at = $now->format('Y-m-d H:i:s');
	    if ($conmodel->save()) {
	        $usermodel = Users::model()->findByPk($from);
	        $conmsgmodel = new ConversationMessages();
	        $conmsgmodel->conversation_id = $conmodel->id;
	        $conmsgmodel->user_id = Yii::app()->user->getState('userid');
	        $conmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
	        $conmsgmodel->message = $msg;
	        $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
	        $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
	        if ($conmsgmodel->save()) {
	            $conmemmodel_sender = new ConversationMembers();
	            $conmemmodel_sender->conversation_id = $conmodel->id;
	            $conmemmodel_sender->user_id = $from;
	            $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
	            $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
	            $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
	            $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
	            if ($conmemmodel_sender->save()) {
	                $usermodel1 = Users::model()->findByPk($to);
	                $conmemmodel_rec = new ConversationMembers();
	                $conmemmodel_rec->conversation_id = $conmodel->id;
	                $conmemmodel_rec->user_id =$to;
	                $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
	                $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
	                $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
	                if ($conmemmodel_rec->save()) {
	                    $actmodel_sender = new Activities();
	                    $actmodel_sender->user_id = $from;
	                    $actmodel_sender->type = "message";
	                    $actmodel_sender->model_id = $conmsgmodel->id;
	                    $actmodel_sender->model_type = "ConversationMessage";
	                    $actmodel_sender->subject = "You sent a message in conversation: " . $subject;
	                    $actmodel_sender->message = $msg;
	                    $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
	                    $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
	                    if ($actmodel_sender->save()) {
	                        $actmodel_rec = new Activities();
	                        $actmodel_rec->user_id = $to;
	                        $actmodel_rec->type = "message";
	                        $actmodel_rec->model_id = $conmsgmodel->id;
	                        $actmodel_rec->model_type = "ConversationMessage";
	                        $actmodel_rec->subject = "You received a message in conversation: " . $subject;
	                        $actmodel_rec->message = $msg;
	                        $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
	                        $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
	                        $actmodel_rec->save();
	                    }
	                }
	            }
	        }
	    }
	}
	
	
	public function actionupdateAppointment()
	{
	    $application_id  = $_POST['application_id'];
	    $msg = $_POST['msg'];
	    
	    $status = 5;
	    if($_POST["appointment_date"] != "")
	    {
	        $myDateTime = DateTime::createFromFormat('m/d/Y h:i A', $_POST["appointment_date"]);
	        $appointment_date = $myDateTime->format('Y-m-d H:i:s');
	    }
	    
	    $model = JobApplications::model()->findByPk($application_id);
	    $model->appointment_date = $appointment_date;
	    $model->status = $status;
	    $model->message = $msg;
	    $model->save();
	    
	    $jobs = Jobs::model()->findByPk($model->job_id);
	    $user = Users::model()->findByPk($model->user_id);
	    
	    $job = $jobs->title;
	    $name = $user->forenames;
	    $email = $user->email;
	    $message = $msg;
	    
	    $indate = date("l jS \of F Y h:i:s A", strtotime($appointment_date));
	    
	    $subject = "Your appointment has been scheduled on $appointment_date, please confirm that this time is suitable";
	    
	    $this->sendMessage($subject,$message,Yii::app()->user->getState('userid'),$user->id);//$model->user_id);
	    
	    $this->notifyappointment($job, $name,$email, $message, $subject);
	    
	    Yii::app()->user->setFlash('success', 'Successfully sent appointment invitation');
	}
	
	/**
	 * Notify the user
	 */
	public function notifyappointment($job, $name, $email, $message, $subject) {
	    
	    $body = $this->renderPartial("//emails/interview", array('job' => $job, 'name' => $name,'message'=>$message, 'subject' => $subject), true);
	    // Send register notifcation to employer admin
	    Yii::import('application.extensions.phpmailer.JPhpMailer');
	    $mail = new JPhpMailer;
	    $mail->Host = 'smtp.gmail.com';
	    $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
	    $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
	    $mail->SMTPSecure = 'ssl';
	    $mail->SetFrom('info@cvvid.com', 'CVVID');
	    $mail->Subject = 'You are invited for an appointment';
	    $mail->MsgHTML($body);
	    $mail->AddAddress($email, $name);
	    $mail->Send();
	}
}
