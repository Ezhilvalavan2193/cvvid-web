<?php
use Stripe\Charge;
use Stripe\Invoice;
use Stripe\InvoiceItem;
use Stripe\Stripe;
use Stripe\Token;
use Stripe\Customer;
use Stripe\Plan;
use Stripe\Subscription;
use Stripe\Coupon;

class AgencyController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/admin';
	public $agencyid = 0;
	public $edit = 0;
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
// 			array('deny',  // deny all users
// 				'users'=>array('*'),
// 			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Agency;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Agency']))
		{
			$model->attributes=$_POST['Agency'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Agency']))
		{
			$model->attributes=$_POST['Agency'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Agency');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Agency('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Agency']))
			$model->attributes=$_GET['Agency'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Agency the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Agency::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	public function beforeAction($action)
	{
	    //Yii::app()->user->setReturnUrl(Yii::app()->request->getUrl());
	    $requestUrl=Yii::app()->request->url;
	   
	    if(isset($_SERVER['HTTPS'])){
	        $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
	    }
	    else{
	        $protocol = 'http';
	    }
	    $canurl = $protocol."://".$_SERVER['SERVER_NAME'].$requestUrl;
	    
	    $this->_canonicalUrl = $canurl;
	    
	    if (Yii::app()->user->isGuest)
    	        $this->redirect(Yii::app()->createUrl('site/login'));

	        return parent::beforeAction($action);
	} 
	/**
	 * Performs the AJAX validation.
	 * @param Agency $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='agency-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionedit($id)
	{
	    $this->layout = "main";
	    $this->edit = 1;
	    $this->agencyid = $id;
	    $model = Agency::model()->findByPk($id);
	    $usermodel = Users::model()->findByPk($model->user_id);
	    $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$id,'type'=>'agency'));
	    $this->render('editprofile',array(
	        'model'=>$model,'usermodel'=>$usermodel,'profilemodel'=>$profilemodel
	    ));
	}
	public function actionemployers($id)
	{
	    $this->layout='main';
	    $agencymodel = Agency::model()->findByPk($id);
	    $model = AgencyEmployers::model();
	    $usermodel = Users::model();
	    $addressmodel = Addresses::model();
	    $profilemodel = Profiles::model();
	    
	    $this->render('employers',array(
	        'model'=>$model,'usermodel' => $usermodel, 'addressmodel' => $addressmodel, 'profilemodel' => $profilemodel,'agencymodel'=>$agencymodel
	    ));
	}	
	public function actioncreateemployer($id)
	{
	    if(Yii::app()->user->getState('role') != 'admin')
	       $this->layout='main';
	    $model = Agency::model()->findByPk($id);
	    $aempmodel = new AgencyEmployers();
	    $empmodel = new Employers();
	    $usermodel = new Users();
	    $addressmodel = new Addresses();
	    $this->render("employerform",array('model'=>$model,'agencyempmodel'=>$aempmodel,'empmodel'=>$empmodel,'usermodel'=>$usermodel,'addressmodel'=>$addressmodel));
	}
	public function actionsaveEmployer($id)
	{
	    if(Yii::app()->user->getState('role') != 'admin')
	        $this->layout='main';
	    $agencymodel = Agency::model()->findByPk($id);
	    $model = new Employers;
	    $empusermodel = new EmployerUsers;
	    $usermodel = new Users;
	    $profilemodel = new Profiles;
	    $addressmodel = new Addresses;
	    $agencyempmodel = new AgencyEmployers();
	    $now = new DateTime();
	      
	        if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
	            $usermodel->setScenario('confirm');
	            
	            if(isset($_POST['Users'])){
	                $usermodel->attributes=$_POST['Users'];
	            }
	            if(isset($_POST['Employers'])){
	                $model->attributes=$_POST['Employers'];
	            }
	            if(isset ($_POST["Addresses"])){
	                $addressmodel->attributes = $_POST["Addresses"];
	            }
	            if(isset ($_POST["Profiles"])){
	                $profilemodel->attributes = $_POST["Profiles"];
	            }
	            if(isset($_POST['Users']))
	            {
	                $valid = $usermodel->validate();
	                $valid = $model->validate() && $valid;
	                $valid = $addressmodel->validate() && $valid;
	                
	                if($valid){
	                    $usermodel->created_at = $now->format('Y-m-d H:i:s');
	                    $usermodel->type = 'employer';
	                    $usermodel->location = $_POST['Addresses']['county'].($_POST['Addresses']['county']!=''? ', '.$_POST['Addresses']['country']: '');
	                    $usermodel->location_lat = $_POST['Addresses']['latitude'];
	                    $usermodel->location_lng = $_POST['Addresses']['longitude'];
	                    $usermodel->tel = $_POST['Employers']['tel'];
	                    $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
	                    $usermodel->api_key = md5(uniqid(rand(), true));
	                    $usermodel->save();
	                    
	                    // insert employer details
	                    $model->email = $usermodel->email;
	                    $model->created_at = $now->format('Y-m-d H:i:s');
	                    $model->user_id = $usermodel->id;
	                    if($model->save())
	                    {
	                        //employer user update
	                        $empusermodel->employer_id = $model->id;
	                        $empusermodel->user_id = $usermodel->id;
	                        $empusermodel->role = 'admin';
	                        $empusermodel->created_at = $now->format('Y-m-d H:i:s');
	                        $empusermodel->save();
	                        
	                        //create Agency employer
	                     
	                        $agencyempmodel->agency_id = $agencymodel->id;
	                        $agencyempmodel->employer_id = $model->id;
	                        $agencyempmodel->add_jobs = $_POST['AgencyEmployers']['add_jobs'];
	                        $agencyempmodel->created_at = $now->format('Y-m-d H:i:s');
	                        $agencyempmodel->save();
	                        
	                        
	                        $agencymodel->employer_vacancy = 0;
	                        $agencymodel->save();
	                    }
	                    $profilemodel->owner_id = $model->id;
	                    $profilemodel->owner_type = 'employer';
	                    if($_POST['industry_id'] > 0)
	                        $profilemodel->industry_id = $_POST['industry_id'];
                            $profilemodel->type = 'employer';
                            $profilemodel->slug = $this->getValidatedSlug($model->name);
                            $profilemodel->save();
                            //address update
                            $addressmodel->created_at = $now->format('Y-m-d H:i:s');
                            $addressmodel->model_type = 'employer';
                            $addressmodel->model_id = $model->id;
                            if ($addressmodel->save()) {

                                $umodel = Users::model()->findByPk($model->user_id);

                                $this->notifyemployer($model, $planname = "");

                                if(Yii::app()->user->getState('role')== 'admin')
                                   $this->redirect($this->createUrl('agency/tabemployers', array('id' => $agencymodel->id)));
                                else
                                    $this->redirect($this->createUrl('agency/employers', array('id' => $agencymodel->id)));
                            }
	                        
	                }
	            }
	            $this->render('employerform', array('model' => $agencymodel,'agencyempmodel' => $agencyempmodel,'empmodel' => $model,'profilemodel' => $profilemodel, 'addressmodel' => $addressmodel, 'usermodel' => $usermodel));
	}
	
	public function actioneditemployer($id)
	{
	    if(Yii::app()->user->getState('role') != 'admin')
	       $this->layout='main';
	    $agencyempmodel = AgencyEmployers::model()->findByPk($id);
	    $model = Agency::model()->findByPk($agencyempmodel->agency_id);
	    
	    $empmodel = Employers::model()->findByPk($agencyempmodel->employer_id);
	    $usermodel = Users::model()->findByPk($empmodel->user_id);
	    $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$model->id));
	    
	    $cri = new CDbCriteria();
	    $cri->condition = "model_id =".$empmodel->id." and model_type like '%employer%' and deleted_at is null";
	    $addressmodel = Addresses::model()->find($cri);
	    $this->render("employerform",array('model'=>$model,'agencyempmodel'=>$agencyempmodel,'empmodel'=>$empmodel,'usermodel'=>$usermodel,'profilemodel' => $profilemodel,'addressmodel'=>$addressmodel));
	}
	
	public function actionupdateEmployer($id)
	{
	    if(Yii::app()->user->getState('role') != 'admin')
	       $this->layout='main';
	    $agencyempmodel = AgencyEmployers::model()->findByPk($id);
	    $empid = $agencyempmodel->employer_id;
	    $agencyid = $agencyempmodel->agency_id;
	    
	      
	    $agencymodel = Agency::model()->findByPk($agencyid);
	    $model = Employers::model()->findByPk($empid);
	    $usermodel = Users::model()->findByPk($model->user_id);
	    $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$model->id));
	    
	    $cri = new CDbCriteria();
	    $cri->condition = "model_id =".$empid." and model_type like '%employer%' and deleted_at is null";
	    $addressmodel = Addresses::model()->find($cri);
	    
	    $now = new DateTime();
	    
	    if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
	        $usermodel->setScenario('confirm');
	        
	        if(isset($_POST['Users'])){
	            $usermodel->attributes=$_POST['Users'];
	        }
	        if(isset($_POST['Employers'])){
	            $model->attributes=$_POST['Employers'];
	        }
	        if(isset ($_POST["Addresses"])){
	            $addressmodel->attributes = $_POST["Addresses"];
	        }
	        if(isset ($_POST["Profiles"])){
	            $profilemodel->attributes = $_POST["Profiles"];
	        }
	        if(isset($_POST['Users']))
	        {
	            $valid = $usermodel->validate();
	            $valid = $model->validate() && $valid;
	            $valid = $addressmodel->validate() && $valid;
	            
	            if($valid){
	                $usermodel->updated_at = $now->format('Y-m-d H:i:s');
	                $usermodel->location = $_POST['Addresses']['county'].($_POST['Addresses']['county']!=''? ', '.$_POST['Addresses']['country']: '');
	                $usermodel->location_lat = $_POST['Addresses']['latitude'];
	                $usermodel->location_lng = $_POST['Addresses']['longitude'];
	                $usermodel->tel = $_POST['Employers']['tel'];
	                $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
	                $usermodel->save();
	                
	                $agencyempmodel->add_jobs = $_POST['AgencyEmployers']['add_jobs'];
	                $agencyempmodel->save();
	                
	                // insert employer details
	                $model->email = $usermodel->email;
	                $model->updated_at = $now->format('Y-m-d H:i:s');
	                $model->save();
	                
	                if($_POST['industry_id'] > 0)
	                    $profilemodel->industry_id = $_POST['industry_id'];
	                    
	                    $profilemodel->slug = $this->getValidatedSlug($model->name);
	                    $profilemodel->save();
	                    //address update
	                    $addressmodel->updated_at = $now->format('Y-m-d H:i:s');
	                    if ($addressmodel->save()) {
	                        if(Yii::app()->user->getState('role') == 'admin')
	                           $this->redirect($this->createUrl('agency/tabemployers', array('id' => $agencymodel->id)));
	                        else 
	                            $this->redirect($this->createUrl('agency/employers', array('id' => $agencymodel->id)));
	                        
	                    }
	                    
	            }
	        }
	        $this->render("employerform",array('model'=>$agencymodel,'agencyempmodel'=>$agencyempmodel,'empmodel'=>$empmodel,'usermodel'=>$usermodel,'profilemodel' => $profilemodel,'addressmodel'=>$addressmodel));
	}
	public function actioncandidates($id)
	{
	    $this->layout='main';
	    $agencymodel = Agency::model()->findByPk($id);
	    $model = AgencyCandidates::model();
	    $usermodel = Users::model();
	    $addressmodel = Addresses::model();
	    $profilemodel = Profiles::model();
	    
	    $this->render('candidates',array(
	        'model'=>$model,'usermodel' => $usermodel, 'addressmodel' => $addressmodel, 'profilemodel' => $profilemodel,'agencymodel'=>$agencymodel
	    ));
	}
	public function actionviewcandidates($id)
	{
	    if(Yii::app()->user->getState('role') != 'admin')
	        $this->layout='main';
	    
	    $asmodel = AgencyStaff::model()->findByPk($id);
	    $id = $asmodel->user_id;
	    $agencymodel = Agency::model()->findByPk($asmodel->agency_id);
	    
	    $model = AgencyCandidates::model();
	    $usermodel = Users::model();
	    $addressmodel = Addresses::model();
	    $profilemodel = Profiles::model();
	    
	    if(Yii::app()->user->getState('role') == 'admin')
    	    $this->render('tabcandidates',array(
    	        'staff_id'=>$id,'model'=>$model,'usermodel' => $usermodel, 'addressmodel' => $addressmodel, 'profilemodel' => $profilemodel,'agencymodel'=>$agencymodel
    	    ));
    	else
    	    $this->render('candidates',array(
    	        'staff_id'=>$id,'model'=>$model,'usermodel' => $usermodel, 'addressmodel' => $addressmodel, 'profilemodel' => $profilemodel,'agencymodel'=>$agencymodel
    	    ));
	}
	public function actioncreatecandidate($id)
	{
	    if(Yii::app()->user->getState('role') != 'admin')
	        $this->layout='main';
	    $model = Agency::model()->findByPk($id);
	    $acanmodel = new AgencyCandidates();
	    $usermodel = new Users();
	    $addressmodel = new Addresses();
	    $this->render("candidateform",array('model'=>$model,'agencycanmodel'=>$acanmodel,'usermodel'=>$usermodel,'addressmodel'=>$addressmodel));
	}
	public function actionsaveCandidate($id)
	{
	    if(Yii::app()->user->getState('role') != 'admin')
	       $this->layout='main';
	    $model = Agency::model()->findByPk($id);
	    $usermodel = new Users;
	    $profilemodel = new Profiles;
	    $addressmodel = new Addresses;
	    $now = new DateTime();
	    
	    if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
	        $usermodel->setScenario('confirm');
	        
	        if(isset($_POST['Users'])){ 
	            $usermodel->attributes=$_POST['Users'];
	        }
	        if(isset ($_POST["Addresses"])){
	            $addressmodel->attributes = $_POST["Addresses"];
	        }
	        if(isset($_POST['Users']))
	        {
	            $valid = $usermodel->validate();
	            $valid = $addressmodel->validate() && $valid;
	            
	            if($valid){
	                $d = str_replace('/', '-', $_POST["dob"]);
	                $dobformat = date('Y-m-d', strtotime($d));
	                $usermodel->dob = $dobformat;
	                
	                $usermodel->created_at = $now->format('Y-m-d H:i:s');
	                $usermodel->type = 'candidate';
	                $usermodel->stripe_active = 1;
	                $usermodel->trial_ends_at = Carbon::now()->addYear()->toDateTimeString();
	                
	                $usermodel->location = $_POST['Addresses']['county'].($_POST['Addresses']['county']!=''? ', '.$_POST['Addresses']['country']: '');
	                $usermodel->location_lat = $_POST['Addresses']['latitude'];
	                $usermodel->location_lng = $_POST['Addresses']['longitude'];
	                $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
	                $usermodel->api_key = md5(uniqid(rand(), true));
	                if($usermodel->save())
	                {
	                    
	                    //create Agency employer
	                    $agencycanmodel = new AgencyCandidates();
	                    $agencycanmodel->agency_id = $model->id;
	                    $agencycanmodel->user_id = $usermodel->id;
	                    $agencycanmodel->created_at = $now->format('Y-m-d H:i:s');
	                    if($agencycanmodel->save())
	                    {
	                        $staffid = $_POST['staff_id'];
	                        
	                        $acanmodel = AgencyCandidates::model()->findByPk($agencycanmodel->id);
	                        
	                        $staffcandmodel = AgencyStaffCandidates::model()->findByAttributes(array('agency_candidate_id'=>$agencycanmodel->id,'agency_id'=>$model->id,'staff_id'=>$staffid));
	                        if($staffcandmodel == null)
	                        {
	                            $staffcanmodel = new AgencyStaffCandidates();
	                            $staffcanmodel->staff_id = $staffid;
	                            $staffcanmodel->agency_candidate_id = $agencycanmodel->id;
	                            $staffcanmodel->agency_id = $model->id;
	                            $staffcanmodel->created_at = $now->format('Y-m-d H:i:s');
	                            $staffcanmodel->save();
	                        }
	                    }
	                    
	                    $model->candidate_vacancy = 0;
	                    $model->save();
	                }
	                $profilemodel->owner_id = $usermodel->id;
	                $profilemodel->owner_type = 'candidate';
	                                   
	                    $profilemodel->type = 'candidate';
	                    $profilemodel->slug = $this->getValidatedSlug($usermodel->forenames."-".$usermodel->surname);
	                    $profilemodel->save();
	                    //address update
	                    $addressmodel->created_at = $now->format('Y-m-d H:i:s');
	                    $addressmodel->model_type = 'Candidate';
	                    $addressmodel->model_id = $usermodel->id;
	                    if ($addressmodel->save()) {
	                        
	                        $umodel = Users::model()->findByPk($model->user_id);
	                        
	                        $this->notifycandidate($usermodel, $_POST['Users']['password']);
	                        
	                        if(Yii::app()->user->getState('role') == 'admin')
	                            $this->redirect($this->createUrl('agency/tabcandidates', array('id' => $model->id)));
	                        else    
	                            $this->redirect($this->createUrl('agency/candidates', array('id' => $model->id)));
	                        
	                    }
	                    
	            }
	        }
	        $this->render('candidateform', array('model' => $model,'agencycanmodel' => new AgencyCandidates(),'profilemodel' => $profilemodel, 'addressmodel' => $addressmodel, 'usermodel' => $usermodel));
	} 
	
	public function actioneditcandidate($id)
	{
	    if(Yii::app()->user->getState('role') != 'admin')
	        $this->layout='main';
	    $agencycanmodel = AgencyCandidates::model()->findByPk($id);
	    $model = Agency::model()->findByPk($agencycanmodel->agency_id);
	    
	    $usermodel = Users::model()->findByPk($agencycanmodel->user_id);
	    $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id));
	    
	    $cri = new CDbCriteria();
	    $cri->condition = "model_id =".$usermodel->id." and model_type like '%candidate%' and deleted_at is null";
	    $addressmodel = Addresses::model()->find($cri);
	    $this->render("candidateform",array('model'=>$model,'agencycanmodel'=>$agencycanmodel,'usermodel'=>$usermodel,'profilemodel' => $profilemodel,'addressmodel'=>$addressmodel));
	}
	
	public function actionupdateCandidate($id)
	{
	    if(Yii::app()->user->getState('role') != 'admin')
	       $this->layout='main';
	    $agencycanmodel = AgencyCandidates::model()->findByPk($id);
	    $userid = $agencycanmodel->user_id;
	    $agencyid = $agencycanmodel->agency_id;
	    
	    
	    $agencymodel = Agency::model()->findByPk($agencyid);
	    $usermodel = Users::model()->findByPk($userid);
	    $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id));
	    
	    $cri = new CDbCriteria();
	    $cri->condition = "model_id =".$userid." and model_type like '%candidate%' and deleted_at is null";
	    $addressmodel = Addresses::model()->find($cri);
	    
	    $now = new DateTime();
	    
	    if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
	        $usermodel->setScenario('confirm');
	        
	        if(isset($_POST['Users'])){
	            $usermodel->attributes=$_POST['Users'];
	        }
	        if(isset ($_POST["Addresses"])){
	            $addressmodel->attributes = $_POST["Addresses"];
	        }
	        if(isset ($_POST["Profiles"])){
	            $profilemodel->attributes = $_POST["Profiles"];
	        }
	        if(isset($_POST['Users']))
	        {
	            $valid = $usermodel->validate();
// 	            $valid = $model->validate() && $valid;
	            $valid = $addressmodel->validate() && $valid;
	            
	            if($valid){
	                $usermodel->updated_at = $now->format('Y-m-d H:i:s');
	                $usermodel->location = $_POST['Addresses']['county'].($_POST['Addresses']['county']!=''? ', '.$_POST['Addresses']['country']: '');
	                $usermodel->location_lat = $_POST['Addresses']['latitude'];
	                $usermodel->location_lng = $_POST['Addresses']['longitude'];
// 	                $usermodel->tel = $_POST['Employers']['tel'];
	                $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
	                $usermodel->save();
	                	                    
                    $profilemodel->slug = $this->getValidatedSlug($usermodel->forenames."-".$usermodel->surname);
                    $profilemodel->save();
                    //address update
                    $addressmodel->updated_at = $now->format('Y-m-d H:i:s');
                    if ($addressmodel->save()) {
                        
                        if(Yii::app()->user->getState('role') == 'admin')
                            $this->redirect($this->createUrl('agency/tabcandidates', array('id' => $agencymodel->id)));
                        else
                            $this->redirect($this->createUrl('agency/candidates', array('id' => $agencymodel->id)));
                        
                    }
	                    
	            }
	        }
	        
	        $this->render("candidateform",array('model'=>$agencymodel,'agencycanmodel'=>$agencycanmodel,'usermodel'=>$usermodel,'profilemodel' => $profilemodel,'addressmodel'=>$addressmodel));
	}
	public function actionstaff($id)
	{
	    $this->layout='main';
	    $agencymodel = Agency::model()->findByPk($id);
	    $model = AgencyStaff::model();
	    $usermodel = Users::model();
	    $addressmodel = Addresses::model();
	    $profilemodel = Profiles::model();
	    
	    $this->render('staff',array(
	        'model'=>$model,'usermodel' => $usermodel, 'addressmodel' => $addressmodel, 'profilemodel' => $profilemodel,'agencymodel'=>$agencymodel
	    ));
	}	
	public function actioncreatestaff($id)
	{
	    if(Yii::app()->user->getState('role') != 'admin')
	        $this->layout='main';
	    $model = Agency::model()->findByPk($id);
	    $astafmodel = new AgencyStaff();
	    $usermodel = new Users();
	    $addressmodel = new Addresses();
	    $this->render("staffform",array('model'=>$model,'agencystaffmodel'=>$astafmodel,'usermodel'=>$usermodel,'addressmodel'=>$addressmodel));
	}
	public function actionsaveStaff($id)
	{
	    if(Yii::app()->user->getState('role') != 'admin')
	        $this->layout='main';
	    $model = Agency::model()->findByPk($id);
	    $usermodel = new Users;
	    $profilemodel = new Profiles;
	    $addressmodel = new Addresses;
	    
	    $now = new DateTime();
	    
	    if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
	        $usermodel->setScenario('confirm');
	        
	        if(isset($_POST['Users'])){
	            $usermodel->attributes=$_POST['Users'];
	        }
	        if(isset ($_POST["Addresses"])){
	            $addressmodel->attributes = $_POST["Addresses"];
	        }
	        if(isset($_POST['Users']))
	        {
	            $valid = $usermodel->validate();
	            $valid = $addressmodel->validate() && $valid;
	            
	            if($valid){
	                $usermodel->created_at = $now->format('Y-m-d H:i:s');
	                $usermodel->type = 'staff';
	                $usermodel->location = $_POST['Addresses']['county'].($_POST['Addresses']['county']!=''? ', '.$_POST['Addresses']['country']: '');
	                $usermodel->location_lat = $_POST['Addresses']['latitude'];
	                $usermodel->location_lng = $_POST['Addresses']['longitude'];
	                $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
	                if($usermodel->save())
	                {
	                    //create Agency employer
	                    $agencystaffmodel = new AgencyStaff();
	                    $agencystaffmodel->agency_id = $model->id;
	                    $agencystaffmodel->user_id = $usermodel->id;
	                    
	                    $agencystaffmodel->add_permission = $_POST['AgencyStaff']['add_permission'];
// 	                    if(isset($_POST['AgencyStaff']))
// 	                        $agencystaffmodel->is_employer = $_POST['AgencyStaff'];
	                        
	                    $agencystaffmodel->created_at = $now->format('Y-m-d H:i:s');
	                    $agencystaffmodel->save();
	                    
	                    $model->staff_vacancy = 0;
	                    $model->save();
	                }
	                $profilemodel->owner_id = $usermodel->id;
	                $profilemodel->owner_type = 'staff';
	                
	                $profilemodel->type = 'staff';
	                $profilemodel->slug = $this->getValidatedSlug($usermodel->forenames."-".$usermodel->surname);
	                $profilemodel->save();
	                //address update
	                $addressmodel->created_at = $now->format('Y-m-d H:i:s');
	                $addressmodel->model_type = 'Staff';
	                $addressmodel->model_id = $usermodel->id;
	                if ($addressmodel->save()) {
	                    
	                    $umodel = Users::model()->findByPk($model->user_id);
	                    
	                    $this->notifycandidate($usermodel, $_POST['Users']['password']);
	                    
	                    if(Yii::app()->user->getState('role') == 'admin')	                        
	                       $this->redirect($this->createUrl('agency/tabstaff', array('id' => $model->id)));
	                    else
	                        $this->redirect($this->createUrl('agency/staff', array('id' => $model->id)));
	                }
	                
	            }
	        }
	        $this->render('staffform', array('model' => $model,'agencystaffmodel' => new AgencyStaff(),'profilemodel' => $profilemodel, 'addressmodel' => $addressmodel, 'usermodel' => $usermodel));
	}
	
	public function actioneditstaff($id)
	{
	    if(Yii::app()->user->getState('role') != 'admin')
	        $this->layout='main';
	    $agencystaffmodel = AgencyStaff::model()->findByPk($id);
	    $model = Agency::model()->findByPk($agencystaffmodel->agency_id);
	    
	    $usermodel = Users::model()->findByPk($agencystaffmodel->user_id);
	    $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id));
	    
	    $cri = new CDbCriteria();
	    $cri->condition = "model_id =".$usermodel->id." and model_type like '%staff%' and deleted_at is null";
	    $addressmodel = Addresses::model()->find($cri);
	    $this->render("staffform",array('model'=>$model,'agencystaffmodel'=>$agencystaffmodel,'usermodel'=>$usermodel,'profilemodel' => $profilemodel,'addressmodel'=>$addressmodel));
	}
	
	public function actionupdateStaff($id)
	{
	    if(Yii::app()->user->getState('role') != 'admin')
	       $this->layout='main';
	    $agencystaffmodel = AgencyStaff::model()->findByPk($id);
	    
	    if(isset($_POST['AgencyStaff']))
	    {
	       // $agencystaffmodel->is_employer = $_POST['AgencyStaff'][''];
	        $agencystaffmodel->add_permission = $_POST['AgencyStaff']['add_permission'];
	        $agencystaffmodel->save();
	    }
	    $userid = $agencystaffmodel->user_id;
	    $agencyid = $agencystaffmodel->agency_id;
	    
	    
	    $agencymodel = Agency::model()->findByPk($agencyid);
	    $usermodel = Users::model()->findByPk($userid);
	    $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id));
	    
	    $cri = new CDbCriteria();
	    $cri->condition = "model_id =".$userid." and model_type like '%staff%' and deleted_at is null";
	    $addressmodel = Addresses::model()->find($cri);
	    
	    $now = new DateTime();
	    
	    if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
	        $usermodel->setScenario('confirm');
	        
	        if(isset($_POST['Users'])){
	            $usermodel->attributes=$_POST['Users'];
	        }
	        if(isset ($_POST["Addresses"])){
	            $addressmodel->attributes = $_POST["Addresses"];
	        }
	        if(isset ($_POST["Profiles"])){
	            $profilemodel->attributes = $_POST["Profiles"];
	        }
	        if(isset($_POST['Users']))
	        {
	            $valid = $usermodel->validate();
	            // 	            $valid = $model->validate() && $valid;
	            $valid = $addressmodel->validate() && $valid;
	            
	            if($valid){
	                $usermodel->updated_at = $now->format('Y-m-d H:i:s');
	                $usermodel->location = $_POST['Addresses']['county'].($_POST['Addresses']['county']!=''? ', '.$_POST['Addresses']['country']: '');
	                $usermodel->location_lat = $_POST['Addresses']['latitude'];
	                $usermodel->location_lng = $_POST['Addresses']['longitude'];
	                // 	                $usermodel->tel = $_POST['Employers']['tel'];
	                $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
	                $usermodel->save();
	                
	                $profilemodel->slug = $this->getValidatedSlug($usermodel->forenames."-".$usermodel->surname);
	                $profilemodel->save();
	                //address update
	                $addressmodel->updated_at = $now->format('Y-m-d H:i:s');
	                if ($addressmodel->save()) {
	                    if(Yii::app()->user->getState('role') == 'admin')
	                       $this->redirect($this->createUrl('agency/tabstaff', array('id' => $agencymodel->id)));
	                    else 
	                        $this->redirect($this->createUrl('agency/staff', array('id' => $agencymodel->id)));
	                }
	                
	            }
	        }
	        $this->render("staffform",array('model'=>$agencymodel,'agencystaffmodel'=>$agencystaffmodel,'usermodel'=>$usermodel,'profilemodel' => $profilemodel,'addressmodel'=>$addressmodel));
	}
	
	public function actiondeleteCandidates()
	{
        $idstring = implode(',', $_POST['ids']);
        $ids = $_POST['ids'];
        $now = new DateTime();
        
        for($i = 0; $i < count($ids); $i++)
        {            
             $acanmodel = AgencyCandidates::model()->findByPk($ids[$i]); 
             $umodel = Users::model()->findByPk($acanmodel->user_id);
             $cri = new CDbCriteria();
             $cri->condition = "owner_id =".$umodel->id." and owner_type like '%candidate%'";
                 
             Profiles::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')),$cri);
             Users::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')),'id =:id',array(':id'=>$umodel->id));
             
             AgencyStaffCandidates::model()->deleteAllByAttributes(array('agency_candidate_id'=>$ids[$i]));
             AgencyCandidates::model()->deleteByPk($ids[$i]);
        } 	 
        
	}
	public function actiondeleteEmployers()
	{
	    $idstring = implode(',', $_POST['ids']);
	    $ids = $_POST['ids'];
	    $now = new DateTime();
	    
	    for($i = 0; $i < count($ids); $i++)
	    {
	        $now = new DateTime();
	        $aempmodel = AgencyEmployers::model()->findByPk($ids[$i]); 
	        $empmodel = Employers::model()->findByPk($aempmodel->employer_id);	        
	        $empmodel->deleted_at = $now->format('Y-m-d H:i:s');
	        $empmodel->save();
	        
	        $usermodel = Users::model()->findByPk($empmodel->user_id);
	        $usermodel->deleted_at = $now->format('Y-m-d H:i:s');
	        $usermodel->save();
	        
	        $cri = new CDbCriteria();
	        $cri->condition = "type like '%employer%' and owner_id=".$empmodel->id;
	        $profilemodel = Profiles::model()->find($cri);
	        $profilemodel->deleted_at = $now->format('Y-m-d H:i:s');
	        $profilemodel->save();
	        
	        $cri1 = new CDbCriteria();
	        $cri1->condition = "model_type like '%employer%' and model_id=".$empmodel->id." and deleted_at is null";
	        $addressmodel = Addresses::model()->find($cri1);
	        $addressmodel->deleted_at = $now->format('Y-m-d H:i:s');
	        $addressmodel->save();
	        
	        $umodel = Users::model()->findByPk($ids[$i]);
	        Users::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')),'id =:id',array(':id'=>$umodel->id));
	        Employers::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')),'id =:id',array(':id'=>$umodel->id));
	        
	        AgencyEmployers::model()->deleteByPk($ids[$i]);
	    }
	    
	    	 
	}
	public function actiondeleteStaff()
	{
	    $idstring = implode(',', $_POST['ids']);
	    $ids = $_POST['ids'];
	    $now = new DateTime();
	    
	    for($i = 0; $i < count($ids); $i++)
	    {
	        $astaffmodel = AgencyStaff::model()->findByPk($ids[$i]); 
	        $umodel = Users::model()->findByPk($astaffmodel->user_id);
	        $cri = new CDbCriteria();
	        $cri->condition = "owner_id =".$umodel->id." and owner_type like '%staff%'";
	        
	        Profiles::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')),$cri);
	        Users::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')),'id =:id',array(':id'=>$umodel->id));
	        
	        AgencyStaff::model()->deleteByPk($ids[$i]); 
	        
	        $cri1 = new CDbCriteria();
	        $cri1->condition = "staff_id =".$astaffmodel->user_id;
	        AgencyStaffCandidates::model()->deleteAll($cri1);
	    }
	}
	public function actionassignCandidates($id)
	{
	    $agencymodel = Agency::model()->findByPk($id);
	    $staffid = $_POST['staff_id'];
	    
	    $idstring = implode(',', $_POST['ids']);
	    $ids = $_POST['ids'];
	    $now = new DateTime();
	    for($i = 0; $i < count($ids); $i++)
	    {
	        $acanmodel = AgencyCandidates::model()->findByPk($ids[$i]);
	        
	        $staffcandmodel = AgencyStaffCandidates::model()->findByAttributes(array('agency_candidate_id'=>$ids[$i],'agency_id'=>$agencymodel->id,'staff_id'=>$staffid));
	        if($staffcandmodel == null)
	        {
    	        $staffcanmodel = new AgencyStaffCandidates();
    	        $staffcanmodel->staff_id = $staffid;
    	        $staffcanmodel->agency_candidate_id = $acanmodel->id;
    	        $staffcanmodel->agency_id = $agencymodel->id;
    	        $staffcanmodel->created_at = $now->format('Y-m-d H:i:s');
    	        $staffcanmodel->save();
	        }
	    }   
	    
	    Yii::app()->user->setFlash('assign', 'Candidates assigned to staff.');
	    if(Yii::app()->user->getState('role') == 'admin')
	        $this->redirect($this->createUrl('agency/tabcandidates',array('id'=>$agencymodel->id)));
	    else    
	        $this->redirect($this->createUrl('agency/candidates',array('id'=>$agencymodel->id)));
	}
	
	public function actionvacancies($id)
	{
	    $this->layout='main';
	    $agencymodel = Agency::model()->findByPk($id);
	    $model = AgencyVacancies::model();
	    
	    $this->render('vacancies',array(
	        'model'=>$model,'agencymodel'=>$agencymodel
	    ));
	}
	public function actioncreatevacancy($id)
	{
	    if(Yii::app()->user->getState('role') != 'admin')
	       $this->layout='main';
	    $agencymodel = Agency::model()->findByPk($id);
	    $avacmodel = new AgencyVacancies();
	    $usermodel = new Users();
	    $model = new Jobs();
	    $jobskillmodel = JobSkill::model();
	    $this->render("vacancyform",array('model'=>$model,'agencyvacmodel'=>$avacmodel,'usermodel'=>$usermodel,'agencymodel'=>$agencymodel,'jobskillmodel'=>$jobskillmodel));
	}
	public function actionsaveVacancy($id)
	{
	    if(Yii::app()->user->getState('role') != 'admin')
	       $this->layout='main';
	    $model=new Jobs;
	    $agencymodel = Agency::model()->findByPk($id);
	    $avacmodel = new AgencyVacancies();
	    $industrymodel = Industries::model();
	    $skillcategmodel = SkillCategories::model();
	    $skillsmodel = Skills::model();
	    $jobskillmodel = JobSkill::model();
	    
	    $now = new DateTime();
	    
	    $model->attributes = $_POST['Jobs'];
	    $model->owner_id = $id;
	    $model->description = $_POST["description"];
	    if(isset($_POST['competitive_salary']))
	    {
	        $model->competitive_salary = 1;
	        $model->salary_min = 0;
	        $model->salary_max = 0;
	    }
	    if(!isset($_POST['skills']))
	        $model->setScenario("skillsrequired");
	        if ($model->validate()) {
	            //  insert jobs details
	            
	            $model->owner_type = 'agency';
	            $model->user_id = Yii::app()->user->getState('userid');
	            $model->industry_id = $_POST["industry_id"];
	            $d = str_replace('/','-',$_POST["end_date"]);
	            $endformat = date('Y-m-d' , strtotime($d));
	            //$endformat = date_format(new DateTime($_POST["end_date"]),"Y-m-d");
	            $model->end_date = $endformat;
	            // $model->skill_category_id = $_POST["skill_category_id"];
	            $model->type = $_POST["type"];
	            $model->video_only = isset($_POST["video_only"]) ? 1 : 0;
	            $model->additional = $_POST["additional"];
	            $model->salary_type = $_POST["salary_type"];
	            $model->created_at = $now->format('Y-m-d H:i:s');
	            
	            $model->employer_id = $_POST["employer_id"];
	            if($model->save())
	            {
	                $agencymodel->job_vacancy = 0;
	                $agencymodel->save();
	                
	                if(isset($_POST['skills']))
	                {
	                    $skills = $_POST['skills'];
	                    foreach ($skills as $skills_val){
	                        $jobskillmodel=new JobSkill;
	                        $jobskillmodel->job_id = $model->id;
	                        $jobskillmodel->skill_id = $skills_val;
	                        if(isset($_POST[$skills_val]))
	                            $jobskillmodel->vacancies = $_POST[$skills_val];
	                            $jobskillmodel->save();
	                    }
	                }
	                if(isset($_FILES['jdfile']['name']) && $_FILES['jdfile']['name'] != "")
	                {
	                    $mediamodel = new Media();
	                    $mediamodel->model_type = "Agency";
	                    $mediamodel->model_id = $id;
	                    $mediamodel->collection_name = "documents";
	                    $mediamodel->name = pathinfo($_FILES['jdfile']['name'], PATHINFO_FILENAME);
	                    $mediamodel->file_name = $_FILES['jdfile']['name'];
	                    $mediamodel->size = $_FILES['jdfile']['size'];
	                    $mediamodel->disk = "documents";
	                    $mediamodel->created_at = $now->format('Y-m-d H:i:s');
	                    if($mediamodel->save())
	                    {
	                        $mediafolder = "images/media/" . $mediamodel->id . "/conversions";
	                        
	                        if (!file_exists($mediafolder)) {
	                            mkdir($mediafolder, 0777, true);
	                        }
	                        $filename = $_FILES['jdfile']['name'];
	                        $fullpath = "images/media/" . $mediamodel->id . "/" . $filename;
	                        if (move_uploaded_file($_FILES['jdfile']['tmp_name'], $fullpath)) {
	                            
	                        }
	                        Jobs::model()->updateByPk($model->id,array('media_id'=>$mediamodel->id));
	                    }
	                }
	                
	                $avacmodel->agency_id = $agencymodel->id;
	                $avacmodel->job_id = $model->id;
	                $avacmodel->created_at = $now->format('Y-m-d H:i:s');
	                $avacmodel->save();
	                
// 	                if($agencymodel->email != "")
// 	                    $this->notifyjob($model, $agencymodel->email, $agencymodel->name);
	                    
	                    Yii::app()->user->setFlash('success', 'Successfully created job');
	                    
	                    if(Yii::app()->user->getState('role') == 'admin')
	                        $this->redirect(Yii::app()->createUrl('agency/tabvacancies', array('id' => $agencymodel->id)));
	                    else    
	                       $this->redirect(Yii::app()->createUrl('agency/vacancies', array('id' => $agencymodel->id)));
	            }
	        }
	        
	        $this->render('vacancies',array(
	            'model'=>$avacmodel,'agencymodel'=>$agencymodel
	        ));
	}
	
	public function actioneditVacancy($id)
	{
	    if(Yii::app()->user->getState('role') != 'admin')
	       $this->layout='main';
	    $avacmodel = AgencyVacancies::model()->findByPk($id);
	    $agencymodel = Agency::model()->findByPk($avacmodel->agency_id);
	    $model = Jobs::model()->findByPk($avacmodel->job_id);
	    $jobskillmodel = JobSkill::model()->findAllByAttributes(array('job_id'=>$model->id));
	    $this->render("vacancyform",array('model'=>$model,'agencyvacmodel'=>$avacmodel,'agencymodel'=>$agencymodel,'jobskillmodel'=>$jobskillmodel));
	}
	
	public function actionupdateVacancy($id)
	{
	    if(Yii::app()->user->getState('role') != 'admin')
	       $this->layout='main';
	    $now = new DateTime();
	    $avacmodel = AgencyVacancies::model()->findByPk($id);
	    $agencymodel = Agency::model()->findByPk($avacmodel->agency_id);
	    $model = Jobs::model()->findByPk($avacmodel->job_id);
	    $jobskillmodel = JobSkill::model()->findAllByAttributes(array('job_id'=>$model->id));
	    if(isset($_POST['Jobs']))
	    {
	        $model->attributes = $_POST["Jobs"];
	        if(!isset($_POST['skills']))
	            $model->setScenario("skillsrequired");
	            if ($model->validate()) {
	                //  insert jobs details
	                
	                $model->industry_id = $_POST["industry_id"];
	                $d = str_replace('/','-',$_POST["end_date"]);
	                $endformat = date('Y-m-d' , strtotime($d));
	                // $endformat = date_format(new DateTime($_POST["end_date"]),"Y-m-d");
	                $model->end_date = $endformat;
	                //$model->skill_category_id = $_POST["skill_category_id"];
	                $model->type = $_POST["type"];
	                $model->description = $_POST["description"];
	                $model->video_only = isset($_POST["video_only"]) ? 1 : 0;
	                $model->additional = $_POST["additional"];
	                $model->location_lat = $_POST["location_lat"];
	                $model->location_lng = $_POST["location_lng"];
	                $model->updated_at = $now->format('Y-m-d H:i:s');
	                
	                $model->employer_id = $_POST["employer_id"];
	                if($model->save())
	                {
	                    $skills = $_POST['skills'];
	                    
	                    $jkillmodel= JobSkill::model()->deleteAllByAttributes(array('job_id'=>$model->id));
	                    
	                    foreach ($skills as $skills_val){
	                        $jobskillmodel=new JobSkill;
	                        $jobskillmodel->job_id = $model->id;
	                        $jobskillmodel->skill_id = $skills_val;
	                        $jobskillmodel->save();
	                    }
	                    
	                    if(isset($_FILES['jdfile']['name']) && $_FILES['jdfile']['name'] != "")
	                    {
	                        if($model->media_id > 0)
	                        {
	                            $mediamodel = Media::model()->findByPk($model->media_id);
	                            $mediamodel->updated_at = $now->format('Y-m-d H:i:s');
	                        }
	                        else
	                        {
	                            $mediamodel = new Media();
	                            $mediamodel->created_at = $now->format('Y-m-d H:i:s');
	                        }
	                        $mediamodel->model_type = "Agency";
	                        $mediamodel->model_id = $model->owner_id;
	                        $mediamodel->collection_name = "documents";
	                        $mediamodel->name = pathinfo($_FILES['jdfile']['name'], PATHINFO_FILENAME);
	                        $mediamodel->file_name = $_FILES['jdfile']['name'];
	                        $mediamodel->size = $_FILES['jdfile']['size'];
	                        $mediamodel->disk = "documents";
	                        
	                        if($mediamodel->save())
	                        {
	                            $mediafolder = "images/media/" . $mediamodel->id . "/conversions";
	                            
	                            if (!file_exists($mediafolder)) {
	                                mkdir($mediafolder, 0777, true);
	                            }
	                            $filename = $_FILES['jdfile']['name'];
	                            $fullpath = "images/media/" . $mediamodel->id . "/" . $filename;
	                            if (move_uploaded_file($_FILES['jdfile']['tmp_name'], $fullpath)) {
	                                
	                            }
	                            Jobs::model()->updateByPk($model->id,array('media_id'=>$mediamodel->id));
	                        }
	                    }
	                    
	                    Yii::app()->user->setFlash('success', 'Successfully updated job');
	                    if(Yii::app()->user->getState('role') == 'admin')
	                       $this->redirect(Yii::app()->createUrl('agency/tabvacancies', array('id' => $agencymodel->id)));
	                    else
	                        $this->redirect(Yii::app()->createUrl('agency/vacancies', array('id' => $agencymodel->id)));
	                    
	                }
	            }
	            
	            $this->render('vacancies',array(
	                'model'=>$avacmodel,'agencymodel'=>$agencymodel
	            ));
	    }
	    
	}
	public function actiondeleteVacancies()
	{
	    $idstring = implode(',', $_POST['ids']);
	    $ids = $_POST['ids'];
	    $now = new DateTime();
	    
	    for($i = 0; $i < count($ids); $i++)
	    {
	        $avacmodel = AgencyVacancies::model()->findByPk($ids[$i]);
	        $umodel = Users::model()->findByPk($avacmodel->agency_id);
	       	        	        
	        $cri1 = new CDbCriteria();
	        $cri1->condition = "job_id =".$avacmodel->job_id;
	        JobSkill::model()->deleteAll($cri1);
	        
	        Jobs::model()->deleteByPk($avacmodel->job_id);
	        
	        AgencyVacancies::model()->deleteByPk($ids[$i]);
	    }
	}
	
	public function actionInbox() {
	    $this->layout = "main";
	    $agencymodel = Agency::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
	    $model = Users::model()->findByPk($agencymodel->user_id);
	    $this->render("inbox", array('model' => $model,'agencymodel'=>$agencymodel));
	}
	
	public function actiongetInboxMessages() {
	    $this->layout = "main";
	    $convmodel = Conversations::model()->findByPk($_POST['id']);
	    $this->renderPartial('inbox_messages', array('convmodel' => $convmodel, 'to_user' => $_POST['msg_to']));
	}
	
	public function actionsendMessage() {
	    
	    $now = new DateTime();
	    $conmodel = new Conversations();
	    $conmodel->subject = $_POST['subject'];
	    $conmodel->created_at = $now->format('Y-m-d H:i:s');
	    $conmodel->updated_at = $now->format('Y-m-d H:i:s');
	    if ($conmodel->save()) {
	        $usermodel = Users::model()->findByPk(Yii::app()->user->getState('userid'));
	        $conmsgmodel = new ConversationMessages();
	        $conmsgmodel->conversation_id = $conmodel->id;
	        $conmsgmodel->user_id = Yii::app()->user->getState('userid');
	        $conmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
	        $conmsgmodel->message = $_POST['message'];
	        $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
	        $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
	        if ($conmsgmodel->save()) {
	            $conmemmodel_sender = new ConversationMembers();
	            $conmemmodel_sender->conversation_id = $conmodel->id;
	            $conmemmodel_sender->user_id = Yii::app()->user->getState('userid');
	            $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
	            $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
	            $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
	            $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
	            if ($conmemmodel_sender->save()) {
	                $usermodel1 = Users::model()->findByPk($_POST['user_id']);
	                $conmemmodel_rec = new ConversationMembers();
	                $conmemmodel_rec->conversation_id = $conmodel->id;
	                $conmemmodel_rec->user_id = $_POST['user_id'];
	                $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
	                $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
	                $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
	                if ($conmemmodel_rec->save()) {
	                    $actmodel_sender = new Activities();
	                    $actmodel_sender->user_id = Yii::app()->user->getState('userid');
	                    $actmodel_sender->type = "message";
	                    $actmodel_sender->model_id = $conmsgmodel->id;
	                    $actmodel_sender->model_type = "ConversationMessage";
	                    $actmodel_sender->subject = "You sent a message in conversation: " . $_POST['subject'];
	                    $actmodel_sender->message = $_POST['message'];
	                    $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
	                    $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
	                    if ($actmodel_sender->save()) {
	                        $actmodel_rec = new Activities();
	                        $actmodel_rec->user_id = $_POST['user_id'];
	                        $actmodel_rec->type = "message";
	                        $actmodel_rec->model_id = $conmsgmodel->id;
	                        $actmodel_rec->model_type = "ConversationMessage";
	                        $actmodel_rec->subject = "You received a message in conversation: " . $_POST['subject'];
	                        $actmodel_rec->message = $_POST['message'];
	                        $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
	                        $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
	                        $actmodel_rec->save();
	                    }
	                }
	            }
	        }
	    }
	}
	
	public function actionsaveMessage() {
	    $this->layout = "main";
	    $conid = $_POST['id'];
	    $fromuser = Yii::app()->user->getState('userid');
	    $touser = $_POST['msg_to'];
	    $msg = $_POST['message'];
	    $now = new DateTime();
	    
	    $convmodel = Conversations::model()->findByPk($_POST['id']);
	    $usermodel = Users::model()->findByPk($fromuser);
	    
	    $convmsgmodel = new ConversationMessages();
	    $convmsgmodel->conversation_id = $conid;
	    $convmsgmodel->user_id = $fromuser;
	    $convmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
	    $convmsgmodel->message = $msg;
	    $convmsgmodel->created_at = $now->format('Y-m-d H:i:s');
	    $convmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
	    if ($convmsgmodel->save()) {
	        $conmemmodel_sender = new ConversationMembers();
	        $conmemmodel_sender->conversation_id = $convmodel->id;
	        $conmemmodel_sender->user_id = Yii::app()->user->getState('userid');
	        $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
	        $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
	        $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
	        $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
	        if ($conmemmodel_sender->save()) {
	            $usermodel1 = Users::model()->findByPk($_POST['user_id']);
	            $conmemmodel_rec = new ConversationMembers();
	            $conmemmodel_rec->conversation_id = $convmodel->id;
	            $conmemmodel_rec->user_id = $_POST['msg_to'];
	            $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
	            $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
	            $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
	            if ($conmemmodel_rec->save()) {
	                $activitymodel = new Activities();
	                $activitymodel->user_id = $fromuser;
	                $activitymodel->type = "message";
	                $activitymodel->model_id = $convmsgmodel->id;
	                $activitymodel->model_type = "App\Messaging\ConversationMessage";
	                $activitymodel->subject = "You sent a message in conversation: " . $convmodel->subject;
	                $activitymodel->message = $msg;
	                $activitymodel->created_at = $now->format('Y-m-d H:i:s');
	                $activitymodel->updated_at = $now->format('Y-m-d H:i:s');
	                $activitymodel->save();
	                
	                $activitymodel = new Activities();
	                $activitymodel->user_id = $touser;
	                $activitymodel->type = "message";
	                $activitymodel->model_id = $convmsgmodel->id;
	                $activitymodel->model_type = "App\Messaging\ConversationMessage";
	                $activitymodel->subject = "You sent a message in conversation: " . $convmodel->subject;
	                $activitymodel->message = $msg;
	                $activitymodel->created_at = $now->format('Y-m-d H:i:s');
	                $activitymodel->updated_at = $now->format('Y-m-d H:i:s');
	                $activitymodel->save();
	            }
	        }
	    }
	    Yii::app()->user->setFlash('success', 'Message sent successfully');
	    $this->renderPartial('inbox_messages', array('convmodel' => $convmodel, 'to_user' => $_POST['msg_to']));
	}
	
	public function actionuserAccountInfo()
	{
	    $this->layout = "main";
	    $model = Agency::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
	    $usermodel = Users::model()->findByPk($model->user_id);
	    
	    $cri = new CDbCriteria();
	    $cri->condition = "model_id =".$model->id." and model_type like '%agency%' and deleted_at is null";
	    $addressmodel = Addresses::model()->find($cri);
	    
	    $this->render('useraccountinfo', array(
	        'model' => $model, 'usermodel' => $usermodel,'addressmodel'=>$addressmodel
	    ));
	}
	
	public function actionupdateAgency($id)
	{
             $this->layout = "main";
             
	    $model = Agency::model()->findByPk($id);
	    $usermodel = Users::model()->findByPk($model->user_id);
	    $cri = new CDbCriteria();
	    $cri->condition = "model_id =".$model->id." and model_type like '%agency%' and deleted_at is null";
	    $addressmodel = Addresses::model()->find($cri);
	    $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$model->id,'type'=>'agency'));
	    
	    $now = new DateTime();
	        
	        if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
	            $usermodel->setScenario('confirm');
	            
	            if(isset($_POST['Users'])){
	                $usermodel->attributes=$_POST['Users'];
	            }
	            if(isset($_POST['Agency'])){
	                $model->attributes=$_POST['Agency'];
	            }
	            if(isset ($_POST["Addresses"])){
	                $addressmodel->attributes = $_POST["Addresses"];
	            }
	            if(isset ($_POST["Profiles"])){
	                $profilemodel->attributes = $_POST["Profiles"];
	            }
	            if(isset($_POST['Users']))
	            {
	                $valid = $usermodel->validate();
	                $valid = $model->validate() && $valid;
	                $valid = $addressmodel->validate() && $valid;
// 	                $valid = $profilemodel->validate() && $valid;
	                
	                if($valid){
	                    
	                    $pwd = $usermodel->password;
	                    if ($_POST["Users"]["password"] == "")
	                        $usermodel->password = $pwd;
	                    
	                    $usermodel->created_at = $now->format('Y-m-d H:i:s');
	                    $usermodel->type = 'agency';
	                    $usermodel->location = $_POST['Addresses']['county'].($_POST['Addresses']['county']!=''? ', '.$_POST['Addresses']['country']: '');
	                    $usermodel->location_lat = $_POST['Addresses']['latitude'];
	                    $usermodel->location_lng = $_POST['Addresses']['longitude'];
	                    $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
	                    $usermodel->save();
	                    
	                    $profilemodel->photo_id = null;
	                    if ($_POST['photo_id'] > 0)
	                        $profilemodel->photo_id = $_POST['photo_id'];
                        $profilemodel->updated_at = $now->format('Y-m-d H:i:s');
                        $profilemodel->save();
	                        
	                    $model->email = $usermodel->email;
	                    // Check if using free trial then set membership trial date
	                    $model->trial_ends_at = Carbon::now()->addYear()->toDateTimeString();
	                    $model->created_at = $now->format('Y-m-d H:i:s');
	                    $model->user_id = $usermodel->id;
	                    $model->industry_id = $_POST['industry_id'];
	                    $model->save();
	                    
	                    //address update
	                    $addressmodel->attributes = $_POST["Addresses"];
	                    $addressmodel->created_at = $now->format('Y-m-d H:i:s');
	                    $addressmodel->model_type = 'Agency';
	                    $addressmodel->model_id = $model->id;
	                    if ($addressmodel->save()) {
	                        
	                        if(Yii::app()->user->getState('role') == 'admin')
	                            $this->redirect($this->createUrl('agency/tabedit', array('id' => $model->id)));
	                        else    
	                            $this->redirect($this->createUrl('agency/edit', array('id' => $model->id)));
	                      
	                    }
	                }
	                
	            }
	            
	            $this->render('useraccountinfo', array(
	                'model' => $model, 'usermodel' => $usermodel,'addressmodel'=>$addressmodel
	            ));
	}
	public function actioncreateAgency()
	{
	    $model = new Agency;
	    $usermodel = new Users;
	    $profilemodel = new Profiles;
	    $addressmodel = new Addresses;
	    //  $this->is_premiun = 0;
	    $this->render('agencyadmin', array('model' => $model, 'addressmodel' => $addressmodel, 'usermodel' => $usermodel,'profilemodel'=>$profilemodel));
	}
	
	public function actionsaveAgency()
	{	   
        $model = new Agency;
        $usermodel = new Users;
        $profilemodel = new Profiles;
        $addressmodel = new Addresses;
        
        $now = new DateTime();
         
            if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
                $usermodel->setScenario('confirm');
                
                if(isset($_POST['Users'])){
                    $usermodel->attributes=$_POST['Users'];
                }
                if(isset($_POST['Agency'])){
                    $model->attributes=$_POST['Agency'];
                }
                if(isset ($_POST["Addresses"])){
                    $addressmodel->attributes = $_POST["Addresses"];
                }
                if(isset ($_POST["Profiles"])){
                    $profilemodel->attributes = $_POST["Profiles"];
                }
                if(isset($_POST['Users']))
                {
                    $valid = $usermodel->validate();
                    $valid = $model->validate() && $valid;
                    $valid = $addressmodel->validate() && $valid;
                    $valid = $profilemodel->validate() && $valid;
                    
                    if($valid){
                        $usermodel->created_at = $now->format('Y-m-d H:i:s');
                        $usermodel->type = 'agency';
                        $usermodel->location = $_POST['Addresses']['county'].($_POST['Addresses']['county']!=''? ', '.$_POST['Addresses']['country']: '');
                        $usermodel->location_lat = $_POST['Addresses']['latitude'];
                        $usermodel->location_lng = $_POST['Addresses']['longitude'];
                        $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
                        $usermodel->save();
                        
                        $model->email = $usermodel->email;
                        // Check if using free trial then set membership trial date
                        $model->trial_ends_at = Carbon::now()->addYear()->toDateTimeString();
                        $model->created_at = $now->format('Y-m-d H:i:s');
                        $model->user_id = $usermodel->id;
                        $model->industry_id = $_POST['industry_id'];
                        $model->save();
                        
                        $profilemodel->owner_id = $model->id;
                        $profilemodel->owner_type = 'Agency';
                        $profilemodel->type = 'agency';
                        $profilemodel->slug = $_POST["Profiles"]['slug'];
                        if ($_POST['photo_id'] > 0)
                            $profilemodel->photo_id = $_POST['photo_id'];
                        $profilemodel->save();
                        
                        //address update
                        $addressmodel->attributes = $_POST["Addresses"];
                        $addressmodel->created_at = $now->format('Y-m-d H:i:s');
                        $addressmodel->model_type = 'Agency';
                        $addressmodel->model_id = $model->id;
                        if ($addressmodel->save()) {
                            
                                $this->redirect($this->createUrl('agency/overview'));
                           
                        }
                    }
                    
                }
                
                $this->render('agencyadmin', array('model' => $model, 'addressmodel' => $addressmodel, 'usermodel' => $usermodel,'profilemodel'=>$profilemodel));
	                
	}
	
	public function actionoverview()
	{
	    $model=new Agency('search');
	    $model->unsetAttributes();  // clear any default values
	    if(isset($_GET['Agency']))
	    {
	        $model->attributes=$_GET['Agency'];
	    }
	    $this->render('overview',array(
	        'model'=>$model
	    ));
	}
	
	public function actiontabedit($id)
	{
	    $model = Agency::model()->findByPk($id);
	    
	    $usermodel = Users::model()->findByPk($model->user_id);
	    
	    $cri = new CDbCriteria();
	    $cri->condition = "model_id =".$model->id." and model_type like '%agency%' and deleted_at is null";
	    $addressmodel = Addresses::model()->find($cri);
	    
	    $this->render('tabeditagency',array(
	        'model'=>$model,'usermodel' => $usermodel,'addressmodel'=>$addressmodel
	    ));
	}
	public function actiontabcandidates($id)
	{
	    $agencymodel = Agency::model()->findByPk($id);
	    $model = AgencyCandidates::model();
	    $usermodel = Users::model();
	    $addressmodel = Addresses::model();
	    $profilemodel = Profiles::model();
	    
	    $this->render('tabcandidates',array(
	        'model'=>$model,'usermodel' => $usermodel, 'addressmodel' => $addressmodel, 'profilemodel' => $profilemodel,'agencymodel'=>$agencymodel
	    ));
	}
	
	public function actiontabemployers($id)
	{
	    $agencymodel = Agency::model()->findByPk($id);
	    $model = AgencyEmployers::model();
	    $usermodel = Users::model();
	    $addressmodel = Addresses::model();
	    $profilemodel = Profiles::model();
	    
	    $this->render('tabemployers',array(
	        'model'=>$model,'usermodel' => $usermodel, 'addressmodel' => $addressmodel, 'profilemodel' => $profilemodel,'agencymodel'=>$agencymodel
	    ));
	}
	
	public function actiontabstaff($id)
	{
	    $agencymodel = Agency::model()->findByPk($id);
	    $model = AgencyStaff::model();
	    $usermodel = Users::model();
	    $addressmodel = Addresses::model();
	    $profilemodel = Profiles::model();
	    
	    $this->render('tabstaff',array(
	        'model'=>$model,'usermodel' => $usermodel, 'addressmodel' => $addressmodel, 'profilemodel' => $profilemodel,'agencymodel'=>$agencymodel
	    ));
	}
	
	public function actiontabvacancies($id)
	{
	    $agencymodel = Agency::model()->findByPk($id);
	    $model = AgencyVacancies::model();
	    
	    $this->render('tabvacancies',array(
	        'model'=>$model,'agencymodel'=>$agencymodel
	    ));
	}
	
	public function actionprofile($id)
	{
	    $this->layout = "main";
	    $this->agencyid = $id;
	    $model = Agency::model()->findByPk($id);
	    $usermodel = Users::model()->findByPk($model->user_id);
	    $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$id,'type'=>'agency'));
	    
	    $this->render('profile', array(
	        'model' => $model,'usermodel'=>$usermodel,'profilemodel'=>$profilemodel
	    ));
	}
	/**
	 * Notify the Agency
	 */
	public function notifyjob($model, $email, $name)
	{
	    $body = $this->renderPartial("//emails/jobs-created",array('model' => $model, 'email' => $email,'name'=>$name),true);
	    // Send register notifcation to employer admin
	    Yii::import('application.extensions.phpmailer.JPhpMailer');
	    $mail = new JPhpMailer;
	    $mail->Host = 'smtp.gmail.com';
	    $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
	    $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
	    $mail->SMTPSecure = 'ssl';
	    $mail->SetFrom('mail@cvvid.com', 'CVVID');
	    $mail->Subject = 'Thank you for uploading your vacancy';
	    $mail->MsgHTML($body);
	    $mail->AddAddress($email, $name);
	    $mail->Send();
	    
	}
	/**
	 * Notify the user
	 */
	public function notifyemployer($model, $password) {
	    $body = $this->renderPartial("//emails/registered-employer", array('model' => $model, 'planname' => ''), true);
	    // Send register notifcation to employer admin
	    Yii::import('application.extensions.phpmailer.JPhpMailer');
	    $mail = new JPhpMailer;
	    $mail->Host = 'smtp.gmail.com';
	    $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
	    $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
	    $mail->SMTPSecure = 'ssl';
	    $mail->SetFrom('mail@cvvid.com', 'CVVID');
	    $mail->Subject = 'Your account details for CVVid.com';
	    $mail->MsgHTML($body);
	    $mail->AddAddress($model->email, $model->name);
	    $mail->Send();
	}
	/**
	 * Notify the user
	 */
	public function notifycandidate($model, $password) {
	    $body = $this->renderPartial("//emails/user-account", array('user' => $model, 'password' => $password), true);
	    // Send register notifcation to employer admin
	    Yii::import('application.extensions.phpmailer.JPhpMailer');
	    $mail = new JPhpMailer;
	    $mail->Host = 'smtp.gmail.com';
	    $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
	    $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
	    $mail->SMTPSecure = 'ssl';
	    $mail->SetFrom('mail@cvvid.com', 'CVVID');
	    $mail->Subject = 'Your account details for CVVid.com';
	    $mail->MsgHTML($body);
	    $mail->AddAddress($model->email, $model->forenames . " " . $model->surname);
	    $mail->Send();
	}
	public function getValidatedSlug($slug)
	{
	    $cri = new CDbCriteria();
	    $cri->condition = "slug ='".$slug."'";
	    $pmodel = Profiles::model()->findAll($cri);
	    
	    $cri1 = new CDbCriteria();
	    $cri1->condition = "slug like '".$slug."-%'";
	    $pmodel1 = Profiles::model()->findAll($cri1);
	    
	    if(count($pmodel1) == 0 && count($pmodel) == 1)
	    {
	        return strtolower($slug)."-1";
	    }
	    else if(count($pmodel1) > 0)
	    {
	        return (strtolower($slug)."-".(count($pmodel1)+1));
	    }
	    else if(count($pmodel) == 0)
	        return strtolower($slug);
	        else
	            return (strtolower($slug)."-".rand());
	            
	}
	
	public function actioncheckPackage()
	{
	    $model = Agency::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
	    $user = $_POST['user'];
	    if($user == "candidate")
	       $vacancy = $model['candidate_vacancy']; 
       else if($user == "employer")
           $vacancy = $model['employer_vacancy']; 
        else if($user == "staff")
           $vacancy = $model['staff_vacancy']; 
        else
           $vacancy = $model['job_vacancy']; 
        
           if($vacancy <= 0)
               echo 0;
           else 
               echo 1;
	}
	
	public function actionuserpackage($type)
	{
	    $this->layout = "main";
	    $model = Agency::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
	    $this->render('upgrade',array('model'=>$model,'type'=>$type));
	}
	
	public function actionupgradeUser()
	{
	    //echo var_dump($_POST);
	    $now = new DateTime();
	    $model = Users::model()->findByPk(Yii::app()->user->getState('userid'));
	    
	    $agencymodel = Agency::model()->findByAttributes(array("user_id"=>$model->id));
	    
	    require Yii::app()->basePath . '/extensions/stripe/init.php';
	    Stripe::setApiKey(Yii::app()->params['SECRET_Key']);
	    
// 	    $plan = $_POST['subscription_plan'];
	    $token = $_POST['stripeToken'];
	    
	    $amount = $_POST['amount'];
	    
	    $customer = Customer::create(array(
	        'source'   => $token,
	        'email'    => $agencymodel->email,
	        'description' => $agencymodel->name,
	    ));
	    
	    $charge = Charge::create(array(
	        "amount" => $amount * 100,
	        "currency" => "GBP",
	        "customer" => $customer->id,
	        "receipt_email" => $agencymodel->email,
	    ));
	    
	    Yii::app()->user->setFlash('success', 'Successfully updated your membership');
	   
	    if($_POST['user_type'] == "staff")
	    {
	        $agencymodel->staff_vacancy = 1;
	        $agencymodel->save();
	        $this->redirect(Yii::app()->createUrl('agency/staff', array('id' => $agencymodel->id)));
	    }
	    else if($_POST['user_type'] == "employer")
	    {
	        $agencymodel->employer_vacancy = 1;
	        $agencymodel->save();
	        $this->redirect(Yii::app()->createUrl('agency/employers', array('id' => $agencymodel->id)));
	    }
	    else if($_POST['user_type'] == "candidate")
	    {
	        $agencymodel->candidate_vacancy = 1;
	        $agencymodel->save();
	        $this->redirect(Yii::app()->createUrl('agency/candidates', array('id' => $agencymodel->id)));
	    }
	    else
	    {
	        $agencymodel->job_vacancy = 1;
	        $agencymodel->save();
	        $this->redirect(Yii::app()->createUrl('agency/vacancies', array('id' => $agencymodel->id)));
	    }
	}
	
	public function actionagencyAsCandidate($id)
	{
	    $agencycanmodel = AgencyCandidates::model()->findByPk($id);	    
	    $usermodel = Users::model()->findByPk($agencycanmodel->user_id);
	    $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id));
	    
	    $model = new LoginForm();
	    echo $model->agencylogin($usermodel->id) ? $this->redirect($this->createUrl('users/edit', array('slug'=>$profilemodel['slug']))) : "";
	    // die();
	    // if($model->agencylogin($usermodel->id)) {	
	    //    echo $this->redirect($this->createUrl('users/edit', array('slug'=>$profilemodel['slug'])));
	    // }
	}
}
