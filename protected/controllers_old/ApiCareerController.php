<?php header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Authorization,Origin, X-Requested-With, Content-Type, Accept");
use Spipu\Html2Pdf\Tag\Address;
use Stripe\Charge;
use Stripe\Invoice;
use Stripe\InvoiceItem;
use Stripe\Stripe;
use Stripe\Token;
use Stripe\Customer;
use Stripe\Plan;
use Stripe\Subscription;
use Stripe\Coupon;
use Stripe\InvalidRequestError;



//header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}"); 
class ApiCareerController extends Controller {

// Members
    /**
     * Key which has to be in HTTP USERNAME and PASSWORD headers
     */
    Const APPLICATION_ID = 'ASCCPE';

    /**
     * Default response format
     * either 'json' or 'xml'
     */
    private $format = 'json';

    /**
     * @return array action filters
     */
    public function filters() {
        return array();
    }

//        Login api


    private function _sendResponse($status = 200, $body = array(), $content_type = 'text/html',$column = "") {

// set the status
// and the content type
        header('Content-type: ' . $content_type);

// this should be templated in a real-world solution
        $arr = array();
        $arr['status'] = $status;
        $arr['message'] = $this->_getStatusCodeMessage($status,$column);
        $arr['data'] = $body;
        echo str_replace("null", '""', json_encode($arr, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
        Yii::app()->end();
    }

    private function _sendResponsewithMessage($status = 200, $message = '', $content_type = 'text/html') {
// set the status
// and the content type
        header('Content-type: ' . $content_type);

// this should be templated in a real-world solution
        $arr = array();
        $arr['status'] = $status;
        $arr['message'] = $message;
        echo str_replace("null", '""', json_encode($arr, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES));
        Yii::app()->end();
    }

    private function _getStatusCodeMessage($status,$column) {
        $codes = Array(
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            405 => 'Invalid Authorization Credentials',
            406 => 'Access Denied. Invalid Api key',
            407 => 'Api key is misssing',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
            502 => 'Email already Exists',
            503 => 'Invalid Email Error',
            504 => 'Invalid Password Error',
            505 => 'Passenger Details Missing',
            506 => 'Car Model Not Found',
            507 => 'No Results Found',
            508 => 'Upload failed',
            509 => 'Updated failed',
            510 => 'This Slug is already in use',
            511 => $column.' key does not exists',
            512 => 'Mandatory field missing',
            513 => 'We cant find a user with that e-mail address.'
        );
        return (isset($codes[$status])) ? $codes[$status] : '';
    }

    private function _checkAuth() {
        list($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']) = explode(':' , base64_decode(substr($_SERVER['HTTP_AUTHORIZATION'], 6)));
    // list($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW']) = explode(':' , base64_decode(substr($_SERVER['REDIRECT_REDIRECT_HTTP_AUTHORIZATION'], 6)));
            if (!isset($_SERVER['PHP_AUTH_USER']) && !isset($_SERVER['PHP_AUTH_PW'])) {
                return false;
            } 
            else {
                    if ($_SERVER['PHP_AUTH_USER'] == 'cvvid' && $_SERVER['PHP_AUTH_PW'] == 'cvvid@123') {
                        return true;
                    } 
                    else
                        return false;
            }       
// Check if we have the USERNAME and PASSWORD HTTP headers set?
// if(!(isset($_SERVER['HTTP_X_USERNAME']) and isset($_SERVER['HTTP_X_PASSWORD']))) {
//  // Error: Unauthorized
//  $this->_sendResponse(401);
// }
// $username = $_SERVER['HTTP_X_USERNAME'];
// $password = $_SERVER['HTTP_X_PASSWORD'];
// // Find the user
// $user = Member::model()->find('LOWER(username)=?',array(strtolower($username)));
// if($user===null) {
//  // Error: Unauthorized
//  $this->_sendResponse(401, 'Error: User Name is invalid');
// } else if(!$user->validatePassword($password)) {
//  // Error: Unauthorized
//  $this->_sendResponse(401, 'Error: User Password is invalid');
// }
    }

    public function getValidatedSlug($slug) {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());
        $cri = new CDbCriteria();
        $cri->condition = "slug ='" . $slug . "'";
        $pmodel = Profiles::model()->findAll($cri);

        $cri1 = new CDbCriteria();
        $cri1->condition = "slug like '" . $slug . "-%'";
        $pmodel1 = Profiles::model()->findAll($cri1);

        if (count($pmodel1) == 0 && count($pmodel) == 1) {
            return strtolower($slug) . "-1";
        } else if (count($pmodel1) > 0) {
            return (strtolower($slug) . "-" . (count($pmodel1) + 1));
        } else if (count($pmodel) == 0)
            return strtolower($slug);
        else
            return (strtolower($slug) . "-" . rand());
    }

    /**
     * Returns the json or xml encoded array
     *
     * @param mixed $model
     * @param mixed $array Data to be encoded
     * @access private
     * @return void
     */
    private function _getObjectEncoded($model, $array) {
        if (isset($_GET['format']))
            $this->format = $_GET['format'];

        if ($this->format == 'json') {
            return
            ;
        } elseif ($this->format == 'xml') {
            $result = '<?xml version="1.0">';
            $result .= "\n<$model>\n";
            foreach ($array as $key => $value)
                $result .= "    <$key>" . utf8_encode($value) . "</$key>\n";
            $result .= '</' . $model . '>';
            return $result;
        } else {
            return;
        }
    }
    
    /* check auth */
    
       /**
     * Validating user api key
     * If the api key is there in db, it is a valid key
     * @param String $api_key user api key
     * @return boolean
     */
    public function isValidApiKey($api_key) {
        
        $count = Users::model()->findByAttributes(array('api_key' => $api_key));
       
        return count($count) > 0;
    }
    
    
    // api key authorization
        
    public function apikeyAuth()
    {
        //echo var_dump($_SERVER['HTTP_AUTHORIZATION']);die();
        // Getting request headers
        $authHeader = $_SERVER['HTTP_AUTHORIZATION'];
      //  $headers = apache_request_headers();
        // Verifying Authorization Header
         if (isset($authHeader) && $authHeader != '') {
            // get the api key
            $api_key = $authHeader;
            
            // validating api key
            if (!$this->isValidApiKey($api_key)) {
                //Access Denied. Invalid Api key
                $this->_sendResponse(406, array());
            }
 
        }else{
            //Api key is misssing
             $this->_sendResponse(407, array());
        }
    }
    
    /**
     * Generating random Unique MD5 String for user Api key
     */
    private function generateApiKey() {
        return md5(uniqid(rand(), true));
    }
    
    
     public function actionlogin() {
        
        //if(!$this->_checkAuth())
         // $this->_sendResponse(405, array());
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $email = $_POST['email'];
        $password = $_POST['password'];
        
        $cri_u = new CDbCriteria();
        $cri_u->alias = 'u';
        $cri_u->select = 'u.id, u.type, u.forenames, u.surname,u.api_key';
        $cri_u->condition = "u.email = '$email' and u.status = 'active' and u.deleted_at is null";
        $umodel = Users::model()->find($cri_u); 
        
//        echo var_dump($umodel);die();
        
        if($umodel == NULL)
           $this->_sendResponse(504, array());
        
        $cri = new CDbCriteria();
        $cri->select = 'u.id, u.type, u.forenames, u.surname, p.id as profile_id,u.api_key';
        $cri->alias = 'u';
        $cri->condition = "u.id ='".$umodel->id."'";
        $cri->join = "left outer join cv16_profiles p on p.owner_id = u.id and p.type like '%student%'";
        $model = Users::model();
        $usrarr = $model->find($cri);

        $cri1 = new CDbCriteria();
        $cri1->alias = 'i';
        $cri1->select = "i.name,i.id as institution_id,u.id as user_id,p.id as profile_id,u.forenames,u.type,u.surname,u.email,u.stripe_active,u.current_job,u.location,u.api_key";
        $cri1->join = "left outer join cv16_users u on u.id = i.admin_id
                       left outer join cv16_profiles p on p.owner_id = u.id and p.type like '%institution%'";
        $cri1->condition = "i.admin_id = '" . $usrarr['id'] . "'";

        $cri2 = new CDbCriteria();
        $cri2->alias = 'e';
        $cri2->select = "e.name,e.id,u.id as user_id,p.id as profile_id,u.forenames,u.type,u.surname,u.email,u.stripe_active,u.current_job,u.location,u.api_key";
        $cri2->join = "left outer join cv16_employer_users eu on eu.employer_id = e.id                        
                        left outer join cv16_users u on u.id = eu.user_id
                        left outer join cv16_profiles p on p.owner_id = e.id and p.type like '%employer%'";
        $cri2->condition = "eu.user_id =" . $usrarr['id'];
        
        $user_model = Users::model()->findByAttributes(array('email' => $email));
        
//        echo var_dump($umodel);die();

        if ($usrarr['type'] == 'institution_admin')
            $result = Institutions::model()->getCommandBuilder()->createFindCommand(Institutions::model()->tableSchema, $cri1)->queryAll();
        else if($usrarr['type'] == 'student' || $usrarr['type'] == 'candidate')
            $result = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
        else if ($usrarr['type'] == 'employer')
            $result = Employers::model()->getCommandBuilder()->createFindCommand(Employers::model()->tableSchema, $cri2)->queryAll();
        if (count($result) <= 0) {
            $this->_sendResponse(503, array());
        } else if (!password_verify($password, $user_model->password)) {
            $this->_sendResponse(504, array());
        } else
            $this->_sendResponse(200, $result);
    }


    public function actionforgetpassword() {
        
         if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
               
        // collect user input data
        if (isset($_POST['email'])) {
            
            $email = $_POST['email'];
            
            $count = Users::model()->countByAttributes(array('email' => $email));
            
            if ($count > 0) {
                                
                $now = new DateTime();
                $token = $this->random(50);
                $passmodel = new PasswordResets();
                $passmodel->email = $email;
                $passmodel->token = $token;
                $passmodel->created_at = $now->format('Y-m-d H:i:s');
                if ($passmodel->save()) {
                    
                    $this->notifyresetpassword($email, $token);
                    
                    $this->_sendResponsewithMessage(200, sprintf("Reset link sent to your mail.", ""));
                }
            } else {
                $this->_sendResponse(513, array());
               
            }
        }
        
    }
    
    /* get body */

    public function actiongetpersonalstatement() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());
        $this->apikeyAuth();

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $user = Users::model()->findByPk($id);
        if ($user == null) {
            $this->_sendResponse(404, array());
        }
        $cri = new CDbCriteria();
        $cri->condition = 'user_id=' . $user->id;
        $userstatement = UserStatement::model()->getCommandBuilder()->createFindCommand(UserStatement::model()->tableSchema, $cri)->queryAll();
        if (!empty($userstatement)) {
            $this->_sendResponse(200, $userstatement);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    
    
    /**
     * Notify the user
     */
    public function notifyresetpassword($email, $token) {
        $body = $this->renderPartial("//emails/reset-password", array('email' => $email, 'token' => $token), true);
        // Send register notifcation to employer admin
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->Host = 'mail.cvvid.com';
        $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
        $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
        $mail->SMTPSecure = 'ssl';
        $mail->SetFrom('mail@cvvid.com', 'CVVID');
        $mail->Subject = 'Your account details for CVVid.com';
        $mail->MsgHTML($body);
        $mail->AddAddress($email);
        $mail->Send();
    }
    

    public function notifyforgetpassword($email) {
        $body = "change password ";
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->Host = 'cvvid.com';
        $mail->Username = 'vinoth25pms@gmail.com';
        $mail->Password = '9865295902';
        $mail->SMTPSecure = 'ssl';
        $mail->SetFrom('mail@cvvid.com', 'cvvid');
        $mail->Subject = 'Your password details for cvvid.com';
        $mail->MsgHTML($body);
        $mail->AddAddress($email);
        $mail->AddReplyTo('cvvid@gmail.com', 'Reply to cvvid');
        $mail->Send();
    }

    public function actioncreatecandidate() {
        //if(!$this->_checkAuth())
        //  $this->_sendResponse(405, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $usermodel = new Users();
        $profilemodel = new Profiles();
        $addressmodel = new Addresses();
        $now = new DateTime();

        // Generating API key
        $api_key = $this->generateApiKey();
            
            $errors = array();

        $usertable = Yii::app()->db->schema->getTable(Users::model()->tableName());
        $profiletable = Yii::app()->db->schema->getTable(Profiles::model()->tableName());
        $addresstable = Yii::app()->db->schema->getTable(Addresses::model()->tableName());
//         if(!isset($table->columns['somecolumn'])) {
//             // Column doesn't exist
//         }

        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {

                if ($var == "email") {
                    $mmodel = Users::model()->findByAttributes(array('email' => $value));
                    if ($mmodel != null) {
                        $this->_sendResponse(502, array());
                        die();
                    }
                }
                $usermodel->$var = $value;
            } else if ($addressmodel->hasAttribute($var)) {

                $addressmodel->$var = $value;
            } else if ($profilemodel->hasAttribute($var)) {

                if ($var == "slug") {
                    $pmodel = Profiles::model()->findByAttributes(array('slug' => $value));
                    if ($pmodel != null) {
                        $this->_sendResponse(510, array());
                        die();
                    }
                }
                $profilemodel->$var = $value;
            } else if ($var == 'ispremium') {
                //  $this->_sendResponse(400, array());
            } else if (!isset($usertable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($profiletable->columns[$var])) {  // Column doesn't exist
            echo "ssss=".$profiletable->columns[$var];
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($addresstable->columns[$var])) {  // Column doesn't exist
            // echo "ssss=".$addresstable->columns[$var];
            // die();
                $this->_sendResponse(511, array(), 'text/html', $var);
            } 
             else {
                $this->_sendResponse(500, array());
            }
        }
        if(isset($_POST['county']) && $_POST['country'])
        $usermodel->location = $_POST['county'] . ($_POST['county'] != '' ? ', ' . $_POST['country'] : '');
        if (isset($_POST['latitude']) && $_POST['latitude'])
            $usermodel->location_lat = $_POST['latitude'];
        if (isset($_POST['longitude']) && $_POST['longitude'])
            $usermodel->location_lng = $_POST['longitude'];
        // Check if using free trial then set membership trial date
        if (isset($_POST['ispremium']) && $_POST['ispremium'])
            $usermodel->trial_ends_at = Carbon::now()->addYear()->toDateTimeString();
        $usermodel->type = 'candidate';
        $usermodel->created_at = $now->format('Y-m-d H:i:s');
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $email = $usermodel->email;
        $usermodel->api_key = $api_key;


       // $profilemodel->slug = $usermodel->forenames . '' . $usermodel->surname;
        $profilemodel->slug =$this->getValidatedSlug($_POST['slug']);

         $valid = $usermodel->validate();
         $valid = $addressmodel->validate() && $valid;
         $valid = $profilemodel->validate() && $valid;

        if ($valid) {

            $usermodel->save();

            $addressmodel->model_id = $usermodel->id;
            $addressmodel->model_type = "Candidate";
            $addressmodel->created_at = $now->format('Y-m-d H:i:s');
            if ($addressmodel->save()) {

                $profilemodel->owner_type = "Candidate";
                $profilemodel->owner_id = $usermodel->id;
                $profilemodel->slug =$this->getValidatedSlug($_POST['slug']);
                $profilemodel->created_at = $usermodel->created_at;
                $profilemodel->type = $usermodel->type;
                if ($profilemodel->save()) {

                    $cri = new CDbCriteria();
                    $cri->alias = 'u';
                    $cri->select = 'u.id, u.type,u.email,u.mobile, u.forenames, u.surname,u.api_key, p.id as profile_id,p.slug';
                    $cri->join = "left outer join cv16_profiles p on p.owner_id = u.id";
                    $cri->condition = "u.id = " . $usermodel->id;

                    $users = Users::model()->getCommandBuilder()->createFindCommand(Users::model()->tableSchema, $cri)->queryAll();
                    
                    $cri_a = new CDbCriteria();
                    $cri_a->alias = 'u';
                    $cri_a->select = 'u.id, u.type,u.email, u.forenames, u.surname';
                    $cri_a->condition = "u.email = 'info@cvvid.com' and u.type like '%admin%'";
                                        
                    $admin_user = Users::model()->find($cri_a);     // 
                    
                    //send message to employer inbox
                    $subject = "Welcome to CVVID!";
                    $message = "Thank you for signing up and welcome to CVVID!";
                    
                    $this->sendMessage($subject,$message,$admin_user->id,$usermodel->id);
                                        
                    $this->notifycreatecandidate($email);
                    $this->_sendResponse(200, $users);
                } 
            } 
        } else {
            // $errors[] = $usermodel->errors;
            // $errors[] = $profilemodel->errors;
            // $errors[] = $addressmodel->errors;
            $usrerr = array();
            
            $usercolumns = Users::model()->getAttributes();
            $addresscolumns = Addresses::model()->getAttributes();
            $profilecolumns = Profiles::model()->getAttributes();

            foreach ( $usercolumns as $key=>$val) {

                if($usermodel->getError($key) != "")
                {
                   
                        $usrerr[$key] = $usermodel->getError($key);
                    }
            }
            foreach ($addresscolumns as $key=>$val) {
                if($addressmodel->getError($key) != null)
                        $usrerr[$key] = $addressmodel->getError($key);
            }
            foreach ($profilecolumns as $key=>$val) {
                if($profilemodel->getError($key) != null)
                        $usrerr[$key] = $profilemodel->getError($key);
            }
             
                    // if(count($errors) > 0)
                    // {
                       $this->_sendResponse(512, $usrerr);
                    // }
            //$this->_sendResponse(512, $usermodel->errors);
        }
                 
    }


    public function notifycreatecandidate($email) {
        $body = "register candidate registered successfully";
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->Host = 'cvvid.com';
        $mail->Username = 'vinoth25pms@gmail.com';
        $mail->Password = '9865295902';
        $mail->SMTPSecure = 'ssl';
        $mail->SetFrom('mail@cvvid.com', 'cvvid');
        $mail->Subject = 'Your are registered candidate details for cvvid.com';
        $mail->MsgHTML($body);
        $mail->AddAddress($email);
        $mail->AddReplyTo('cvvid@gmail.com', 'Reply to cvvid');
        $mail->Send();
    }

    public function actiongetuserdetails() {
        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());
          $this->apikeyAuth();
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $model = Users::model()->findByPk($_GET['id']);
        if($model == null){
            $this->_sendResponse(500, array());
        }

        $cri = new CDbCriteria();
        $cri->alias = 'u';
        $cri->select = "u.id as user_id,u.trial_ends_at as trails,m.file_name,(SELECT count(*) FROM cv16_profile_views WHERE profile_id = p.id) as num_views,p.visibility,(SELECT count(*) FROM cv16_profile_favourites WHERE profile_id = p.id) as num_likes,CONCAT('https://cvvid.com/images/media/',p.photo_id,'/',m.file_name) as photo,CONCAT('https://cvvid.com/images/defaultprofile.jpg') as photodefault,CONCAT('https://cvvid.com/images/media/',p.cover_id,'/conversions/cover.jpg') as cover,CONCAT('https://cvvid.com/images/CoverPlaceholder.jpg') as coverdefault,u.forenames,u.surname,u.email,u.stripe_active,u.current_job,u.location,p.slug,p.photo_id,p.cover_id,p.id as profile_id,a.address,a.town,a.county,a.country,a.postcode";
        $cri->join = "inner join cv16_profiles p on p.owner_id = u.id
                      left outer join cv16_media m on m.id = p.photo_id
                      left outer join cv16_addresses a on a.model_id = u.id";
        $cri->condition = "u.id = " . $model->id;

        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (!empty($users)) {
            $this->_sendResponse(200, $users);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    public function actionfetchuserdetails() {

        // echo var_dump($_SERVER['HTTP_AUTHORIZATION']);
        // die();
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

          $this->apikeyAuth();
        
    if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $id = $_GET['id'];
        $usermodel = Users::model()->findByPk($id);
        if ($usermodel == null) {
            $this->_sendResponse(500, array());
        }
        $cri = new CDbCriteria();
        $cri->alias = 'us';
        $cri->select = 'us.id, us.type, us.forenames, us.surname, us.email, us.password,DATE_FORMAT(us.dob, "%d/%m/%Y") as dob,us.status, us.is_premium, us.tel, us.mobile, us.current_job, us.location, us.location_lat,'
                . ' us.location_lng, us.remember_token, us.created_at, us.updated_at, us.deleted_at, us.stripe_active, us.stripe_id, us.stripe_subscription, us.stripe_plan,'
                . ' us.last_four, us.card_expiry, us.card_expiry_sent, us.trial_ends_at, us.subscription_ends_at,hob.activity,hob.description,lan.name,lan.proficiency,ad.address,ad.town,ad.postcode,ad.county,ad.country';
        $cri->join = 'left outer join cv16_addresses  ad on ad.model_id = us.id left outer join cv16_user_qualifications qua on qua.user_id = us.id left outer join cv16_user_hobbies hob on hob.user_id = us.id left outer join cv16_user_languages lan on lan.user_id = us.id left outer join cv16_profiles pro on pro.owner_id = us.id';
        $cri->condition = 'us.id = ' . $id;
        $cri->group = 'us.id';
        $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
        if (!empty($users)) {
            $this->_sendResponse(200, $users);
        } else {
            $this->_sendResponse(507, array());
        } 
    }

    public function actionoldupdatecandidate() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }
        $id = $_GET['id'];
        $usermodel = Users::model()->findByPk($id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $id));
        $now = new DateTime();
        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                $usermodel->$var = $value;
            } elseif ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else {
                $this->_sendResponse(400, array());
            }
        }
        $usermodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($usermodel->save()) {
            $profile = new Profiles();
            $profile->type = 'candidate';
            $profile->owner_type = 'Candidate';
            $profile->owner_id = $id;
            $profile->industry_id = $profilemodel['industry_id'];
            $profile->photo_id = $profilemodel['photo_id'];
            $profile->cover_id = $profilemodel['cover_id'];
            $profile->visibility = $profilemodel['visibility'];
            $profile->published = $profilemodel['published'];
            $profile->hired = $profilemodel['hired'];
            $profile->updated_at = $now->format('Y-m-d H:i:s');
            if ($profilemodel->save()) {
                $this->_sendResponse(200, array());
            }
        } else {
            $this->_sendResponse(500, array());
        }
    }

    public function actionupdatecandidate() {
        // if(!$this->_checkAuth())
        //  $this->_sendResponse(405, array());
        $this->apikeyAuth();
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }
        $id = $_GET['id'];
        $usermodel = Users::model()->findByPk($id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $id));
        $now = new DateTime();
        $usertable = Yii::app()->db->schema->getTable(Users::model()->tableName());
        $profiletable = Yii::app()->db->schema->getTable(Profiles::model()->tableName());
       
        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                $usermodel->$var = $value;
            } elseif ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else if (!isset($usertable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($profiletable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            }  else {
                $this->_sendResponse(400, array());
            }
        }
        $usermodel->updated_at = $now->format('Y-m-d H:i:s');
         $valid = $usermodel->validate();
        
         $valid = $profilemodel->validate() && $valid;
        if ($valid) {
            $usermodel->save();
            $profile = new Profiles();
            $profile->type = 'candidate';
            $profile->owner_type = 'Candidate';
            $profile->slug = $this->getValidatedSlug($_POST['slug']);
            $profile->owner_id = $id;
            $profile->industry_id = $profilemodel['industry_id'];
            $profile->photo_id = $profilemodel['photo_id'];
            $profile->cover_id = $profilemodel['cover_id'];
            $profile->visibility = $profilemodel['visibility'];
            $profile->published = $profilemodel['published'];
            $profile->hired = $profilemodel['hired'];
            $profile->updated_at = $now->format('Y-m-d H:i:s');
            $profilemodel->save();
            if ($profilemodel->save()) {
                $this->_sendResponse(200, sprintf("successfully updated"));
            }
        } else {
            $usrerr = array();
            $usercolumns = Users::model()->getAttributes();
            $profilecolumns = Profiles::model()->getAttributes();

            foreach ($usercolumns as $key => $val) {
                if ($usermodel->getError($key) != "") {
                    $usrerr[$key] = $usermodel->getError($key);
                }
            }
           
            foreach ($profilecolumns as $key => $val) {
                if ($profilemodel->getError($key) != null)
                    $usrerr[$key] = $profilemodel->getError($key);
            }

            $this->_sendResponse(512, $usrerr);
        }
    }



    /* cover photo update */

    public function actionupdatecoverphoto() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        if (!isset($_POST['mediaid']) || !isset($_POST['profileid']))
            $this->_sendResponse(500, array());

        $mediaid = $_POST['mediaid'];
        $profileid = $_POST['profileid'];

        $model = Profiles::model()->findByPk($profileid);

        $model->cover_id = $mediaid;
        if ($model->save()) {
            $cri = new CDbCriteria();
            $cri->select = 'id,cover_id';
            $cri->condition = "id = " . $profileid;
            $model = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
            $this->_sendResponse(200, $model);
        } else {
            $this->_sendResponse(400, array());
        }
    }





    /* cover photo update */


    /* supporting documentation */
    /* create supporting documentation */

    public function actioncreateuserdocumentation() {
        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());


        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);

        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        
           $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));
        $now = new DateTime();

        $userid = $id;
        $docname = $_POST['name'];
        $category_id = $_POST['category_id'];
        $published = $_POST['published'];
        $now = new DateTime();

        if (isset($_FILES['doc_file']['name']) && $_FILES['doc_file']['name'] != "") {
            $filename = $_FILES['doc_file']['name'];
            $size = $_FILES['doc_file']['size'];
            $name = pathinfo($_FILES['doc_file']['name'], PATHINFO_FILENAME);

            $mediamodel = new Media();
            $mediamodel->model_type = "Candidate";
            $mediamodel->model_id = $userid;
            $mediamodel->collection_name = "documents";
            $mediamodel->name = $name;
            $mediamodel->file_name = $filename;
            $mediamodel->size = $size;
            $mediamodel->disk = "documents";
            $mediamodel->created_at = $now->format('Y-m-d H:i:s');
            $mediamodel->save();
        }
        $model = new UserDocuments();
        $model->user_id = $userid;
        $model->media_id = $mediamodel->id;
        $model->name = $docname;
        $model->category_id = $category_id;
        $model->published = $published;
        $model->created_at = $now->format('Y-m-d H:i:s');
        if ($model->save()) {
            $pdmodel = new ProfileDocuments();
            $pdmodel->profile_id = $profilemodel->id;
            $pdmodel->user_document_id = $model->id;
            $pdmodel->created_at = $now->format('Y-m-d H:i:s');
            if ($pdmodel->save()) {
                $cri = new CDbCriteria();
                $cri->select = 'id, user_id, media_id, name, category_id, published';
                $cri->condition = 'id=' . $model->id;
                $model = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
                $this->_sendResponse(200, $model);
            } else {
                $this->_sendResponse(500, array());
            }
        } else {
            echo var_dump($model->getErrors());
        }
    }

    /* create supporting documentation */

    // get all profile views
    public function actiongetallprofileviews() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());
         $this->apikeyAuth();

        $id = isset($_GET['profile_id']) ? $_GET['profile_id'] : null;
      
        $profilemodel = Profiles::model()->findByPk($id);
        if ($id != $profilemodel->id)
            $this->_sendResponse(404, array());
        // $id = $_GET['docid'];
        

        $model = new ProfileViews();
        $cri = new CDbCriteria();
        $cri->alias = "pv";
        $cri->select = "u.forenames,u.surname,u.current_job,u.location,pv.view_count,pv.id";
        $cri->join = "inner join cv16_users u on u.id = pv.model_id";
        $cri->condition = "pv.profile_id =".$id;
        $docs = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
        if (empty($docs)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $docs);
        }
    }

    /* upgrade stripe intergration */

    public function actionupgradestore() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $model = Users::model()->findByPk($id);
        if ($id != $model->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $now = new DateTime();

        require Yii::app()->basePath . '/extensions/stripe/init.php';
        Stripe::setApiKey(Yii::app()->params['SECRET_Key']);

        $plan = $_POST['plan_id'];
        $token = $_POST['token'];
        $trial_ends = Carbon::now()->addYear()->toDateTimeString();

        $planarr = Plan::retrieve($plan);

        $amount = $planarr->amount;
        $planname = $planarr->name;
        $currency = $planarr->currency;

        //        create subscriber
        $customer = Customer::create(array(
                    'source' => $token,
                    'email' => $model->email,
                    'plan' => $plan,
                    'description' => $model->forenames . " " . $model->surname
        ));
        $model->stripe_active = 1;
        $model->stripe_id = $customer->id;
        $model->stripe_subscription = $customer->subscriptions->data[0]->id;
        $model->stripe_plan = $plan;
        $model->last_four = $_POST['cvc'];
        $model->trial_ends_at = $trial_ends;
        $model->updated_at = $now->format('Y-m-d H:i:s');
        if ($model->save(false)) {
            
        $this->notifyemployerupgrade($model, $customer->id);

        $subscriber = Employers::model()->findByPk($model->id);

        $invoiceitemlist = Invoice::all(array("customer" => $subscriber->stripe_id));
        foreach ($invoiceitemlist->autoPagingIterator() as $invoice) {
             $link = $invoice['invoice_pdf'];
        }

        $message = "Successfully updated your membership <a href='$link'>Click here to download your invoice</a>";

        $this->sendMessageE("Successfully updated your membership",$message, 57, $model->user_id);
        
          $this->_sendResponsewithMessage(200, sprintf("Successfully updated your membership", ""));
        } else {
            $this->_sendResponse(500, array());
        }
    }
    
    /**
     * Notify the user
     */
    public function notifyemployerupgrade($model, $customerid) {
        $body = $this->renderPartial("//emails/invoice", array('customerid' => $customerid), true);
        // Send register notifcation to employer admin
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->Host = 'mail.cvvid.com';
        $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
        $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
        $mail->SMTPSecure = 'ssl';
        $mail->SetFrom('info@cvvid.com', 'CVVID');
        $mail->Subject = 'Invoice details for CVVid.com';
        $mail->MsgHTML($body);
        $mail->AddAddress($model->email, $model->name);
        $mail->Send();
    }
    
    public function sendMessageE($subject,$message,$from,$to) {
        
        $now = new DateTime();
        $conmodel = new Conversations();
        $conmodel->subject = $subject;
        $conmodel->created_at = $now->format('Y-m-d H:i:s');
        $conmodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($conmodel->save()) {
            $usermodel = Users::model()->findByPk($from);
            $conmsgmodel = new ConversationMessages();
            $conmsgmodel->conversation_id = $conmodel->id;
            $conmsgmodel->user_id = $from;
            $conmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
            $conmsgmodel->message = $message;
            $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
            $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($conmsgmodel->save()) {
                $conmemmodel_sender = new ConversationMembers();
                $conmemmodel_sender->conversation_id = $conmodel->id;
                $conmemmodel_sender->user_id = $from;
                $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
                $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
                $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                if ($conmemmodel_sender->save()) {
                    $usermodel1 = Users::model()->findByPk($to);
                    $conmemmodel_rec = new ConversationMembers();
                    $conmemmodel_rec->conversation_id = $conmodel->id;
                    $conmemmodel_rec->user_id = $to;
                    $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
                    $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                    $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                    if ($conmemmodel_rec->save()) {
                        $actmodel_sender = new Activities();
                        $actmodel_sender->user_id = $from;
                        $actmodel_sender->type = "message";
                        $actmodel_sender->model_id = $conmsgmodel->id;
                        $actmodel_sender->model_type = "ConversationMessage";
                        $actmodel_sender->subject = "You sent a message in conversation: " . $subject;
                        $actmodel_sender->message = $message;
                        $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                        $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                        if ($actmodel_sender->save()) {
                            $actmodel_rec = new Activities();
                            $actmodel_rec->user_id = $to;
                            $actmodel_rec->type = "message";
                            $actmodel_rec->model_id = $conmsgmodel->id;
                            $actmodel_rec->model_type = "ConversationMessage";
                            $actmodel_rec->subject = "You received a message in conversation: " . $subject;
                            $actmodel_rec->message = $message;
                            $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                            $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                            $actmodel_rec->save();
                        }
                    }
                }
            }
        }
    }

    /* upgrade stripe intergration */

    public function actionupgradestoreemployer() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);
        // $model = Users::model()->findByPk($id);
        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $now = new DateTime();
        $model = Employers::model()->findByAttributes(array('user_id' => $id));
        //    $usermodel = Users::model()->findByPk(array('id'=>$id));

        require Yii::app()->basePath . '/extensions/stripe/init.php';

        Stripe::setApiKey(Yii::app()->params['SECRET_Key']);

        $plan = $_POST['plan_id'];
        $token = $_POST['token'];
        $trial_ends = Carbon::now()->addYear()->toDateTimeString();

        $planarr = Plan::retrieve($plan);

        $amount = $planarr->amount;
        $planname = $planarr->name;
        $currency = $planarr->currency;
        
//        echo var_dump($planarr);
//        die();
        try{
        //        create subscriber
        $customer = Customer::create(array(
                    'source' => $token,
                    'email' => $model->email,
                    'plan' => $plan,
                    'description' => $model->name
        ));
        }catch(Exception $ex)
        {
//            echo var_dump($ex->getMessage());
//            die();
        } 


        $model->stripe_active = 1;
        $model->stripe_id = $customer->id;
        $model->stripe_subscription = $customer->subscriptions->data[0]->id;
        $model->stripe_plan = $plan;
        $model->last_four = $_POST['cvc'];
        $model->trial_ends_at = $trial_ends;
        $model->updated_at = $now->format('Y-m-d H:i:s');
        if ($model->save(false)) {
            
            $this->notifyemployerupgrade($model, $customer->id);

            $subscriber = Employers::model()->findByPk($model->id);

            $invoiceitemlist = Invoice::all(array("customer" => $subscriber->stripe_id));
            foreach ($invoiceitemlist->autoPagingIterator() as $invoice) {
                 $link = $invoice['invoice_pdf'];
            }

            $message = "Successfully updated your membership <a href='$link'>Click here to download your invoice</a>";

            $this->sendMessageE("Successfully updated your membership",$message, 57, $model->user_id);

            $this->_sendResponsewithMessage(200, sprintf("Successfully updated your membership", ""));
        } else {
            $this->_sendResponse(500, array());
        }
    }

    // get plan

    public function actiongetPlan() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

         $this->apikeyAuth();

//        $id = isset($_GET['id']) ? $_GET['id'] : null;
//        $usermodel = Users::model()->findByPk($id);
//        if ($id != $usermodel->id)
//            $this->_sendResponse(404, array());

        $now = new DateTime();

        $monplans[] = Membership::plan('Monthly Subscription', 'premium_monthly')
                ->price(25)
                ->price_suffix('per month')
                ->description('Billed Monthly')
                ->monthly()
                ->permissions([
            'send_messages' => 'Send messages',
            'apply_jobs' => 'Apply for jobs',
            'view_favourites' => 'View those who have favourited you',
            'view_views' => 'View those who have viewed your profile'
        ]);

        $annplans[] = Membership::plan('Annual Subscription', 'premium_annual')
                ->price(252)
                ->price_suffix('per year')
                ->description('Save 10%')
                ->permissions([
            'send_messages' => 'Send messages',
            'apply_jobs' => 'Apply for jobs',
            'view_favourites' => 'View those who have favourited you',
            'view_views' => 'View those who have viewed your profile'
        ]);

        try {
            //$products  = Plan::all();
            $results['response'] = "Success";
            $results['plans'] = array_merge($monplans, $annplans);
        } catch (Exception $e) {
            $results['response'] = "Error";
            $results['plans'] = $e->getMessage();
        }
        echo json_encode($results);
    }

    // get employer plan

    public function actiongetEPlan() {

//        $this->apikeyAuth();
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

//        $id = isset($_GET['id']) ? $_GET['id'] : null;
//        $usermodel = Users::model()->findByPk($id);
//        if ($id != $usermodel->id)
//            $this->_sendResponse(404, array());

        $now = new DateTime();

        $monplans[] = Membership::plan('Employer Monthly Subscription', 'employer_vacancy')
                ->user_type('employer')
                ->price(50)
                ->price_suffix('per month')
                ->description('Billed Monthly')
                ->monthly()
                ->permissions([
            'post_jobs' => 'Post Jobs',
            'send_messages' => 'Send messages',
        ]);

        $annplans[] = Membership::plan('Employer Annual Subscription', 'employer_annual')
                ->user_type('employer')
                ->price(450)
                ->price_suffix('per year')
                ->description('Save 10%')
                ->permissions([
            'post_jobs' => 'Post Jobs',
            'send_messages' => 'Send messages',
        ]);

        try {
            //$products  = Plan::all();
            $results['response'] = "Success";
            $results['plans'] = array_merge($monplans, $annplans);
        } catch (Exception $e) {
            $results['response'] = "Error";
            $results['plans'] = $e->getMessage();
        }
        echo json_encode($results);
    }


    // get employer plan

    /* edit supporting documentation */
    public function actionupdateuserdocumentation() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];

        $docmodel = UserDocuments::model()->findByPk($id);
        $userid = $docmodel->user_id;
        $usermodel = Users::model()->findByPk($userid);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $now = new DateTime();
        $userid = $usermodel->id;
        $docname = $_POST['name'];
        $category_id = $_POST['category_id'];
        $published = $_POST['published'];
        $now = new DateTime();
        if ($docmodel->media_id > 0)
            $mediamodel = Media::model()->findByPk($_POST['media_id']);
        else
            $mediamodel = new Media();
        if (isset($_FILES['doc_file']['name']) && $_FILES['doc_file']['name'] != "") {
            $filename = $_FILES['doc_file']['name'];
            $size = $_FILES['doc_file']['size'];
            $name = pathinfo($_FILES['doc_file']['name'], PATHINFO_FILENAME);
            $mediamodel->model_type = "Candidate";
            $mediamodel->model_id = $userid;
            $mediamodel->collection_name = "documents";
            $mediamodel->name = $name;
            $mediamodel->file_name = $filename;
            $mediamodel->size = $size;
            $mediamodel->disk = "documents";
            $mediamodel->created_at = $now->format('Y-m-d H:i:s');
            $mediamodel->save();
        }
        $model = UserDocuments::model()->findByAttributes(array('id' => $id));
        $model->user_id = $userid;
        $model->name = $docname;
        $model->category_id = $category_id;
        $model->published = $published;
        $model->created_at = $now->format('Y-m-d H:i:s');
        if ($model->save()) {
            $pdmodel = ProfileDocuments::model()->findByAttributes(array('profile_id' => $profilemodel->id));
            $pdmodel->profile_id = $profilemodel->id;
            $pdmodel->user_document_id = $model->id;
            $pdmodel->created_at = $now->format('Y-m-d H:i:s');
            if ($pdmodel->save()) {
                $this->_sendResponsewithMessage(200, sprintf("Successfully Documentation updated", ""));
            } else {
                $this->_sendResponse(500, array());
            }
        } else {
            echo var_dump($model->getErrors());
        }
    }

    /* edit supporting documentation */

    /* get supporting documentation */

    // public function actionprofiledocumentation() {
    //    if (!isset($_GET['id']) || $_GET['id'] == NULL)
    //         $this->_sendResponse(404, array());
    //     $id = $_GET['id'];        
    //     $model = UserDocuments::model()->findByPk($id);
    //     $cri = new CDbCriteria();
    //     $cri->alias = "ud";
    //     $cri->select = "ud.name,ud.id,m.file_name as filename,mc.name as category";
    //     $cri->join = "left outer join cv16_profile_documents pd on pd.user_document_id = ud.id "
    //             . "left outer join cv16_media m on m.id = ud.media_id "
    //             . " left outer join cv16_media_categories mc on mc.id = ud.category_id";
    //     $cri->condition = "pd.user_document_id =" . $id;
    //     $docs = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
    //     if (empty($docs)) {
    //         $this->_sendResponse(404, array());
    //     } else {
    //         $this->_sendResponse(200, $docs);
    //     }
    // }

    public function actiongetprofiledocumentation() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        $this->apikeyAuth();

        $docid = isset($_GET['id']) ? $_GET['id'] : null;
        $docmodel = UserDocuments::model()->findByPk($docid);
        if ($docid != $docmodel->id)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $model = new UserDocuments();
        $cri = new CDbCriteria();
        $cri->alias = "ud";
        $cri->select = "ud.name,ud.id,m.file_name as filename,mc.name as category,ud.published";
        $cri->join = "inner join cv16_profile_documents pd on pd.user_document_id = ud.id"
                . " inner join cv16_media m on m.id = ud.media_id "
                . " inner join cv16_media_categories mc on mc.id = ud.category_id";
        $cri->condition = "pd.user_document_id =" . $id;
        $docs = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
        if (empty($docs)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $docs);
        }
    }

    /* get supporting documentation */


    /* membership */

    // public function actiongetmembership() {
    //     $membershipbasic = [];
    //     $membershippremium = [];
    //     $membershipbasic[0]['title'] = "Basic";
    //     $membershipbasic[1]['price'] = "Free";
    //     $membershipbasic[2]['description'] = "Enjoy the basic features of the CVVid platform.";
    //     $membershipbasic[3]['desc_title'] = "";
    //     $membershipbasic[4]['desc_list'][0] = "Create your own CVVid profile";
    //     $membershipbasic[4]['desc_list'][1] = "Upload your portfolio";
    //     $membershipbasic[4]['desc_list'][2] = "Browse all vacancies";
    //     $membershipbasic[4]['desc_list'][3] = "Create your CVVid profile PDF";
    //     $membershippremium[0]['title'] = "Premium";
    //     $membershippremium[1]['price'] = "250.00";
    //     $membershippremium[2]['description'] = "Enjoy the full benefits of the CVVid platform.";
    //     $membershippremium[3]['desc_title'] = "All Basic features plus";
    //     $membershippremium[4]['desc_list'][0] = "Browse and apply for jobs";
    //     $membershippremium[4]['desc_list'][1] = "Unique real-time messaging platform";
    //     $membershippremium[4]['desc_list'][2] = "See who has visited your profile";
    //     $membershippremium[4]['desc_list'][3] = "See who has liked your profile";
    //     $result = array();
    //     $result = array_merge($membershipbasic, $membershippremium);
    //     // $membershipbasic['BASIC'] = $chequelines;
    //     // $membershippremium['PREMIUM'] = $chequedetails;
    //     if (!empty($result)) {
    //         $this->_sendResponse(200, $result);
    //     } else {
    //         $this->_sendResponse(507, array());
    //     }
    // }


    public function actiongetmembership() {
        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());
          $this->apikeyAuth();

        $membershipbasic = [];
        $membershippremium = [];
        $membershipbasic['title'] = "Basic";
        $membershipbasic['price'] = "Free";
        $membershipbasic['description'] = "Enjoy the basic features of the CVVid platform.";
        $membershipbasic['desc_title'] = "";
        $membershipbasic['desc_list'][0] = "Create your own CVVid profile";
        $membershipbasic['desc_list'][1] = "Upload your portfolio";
        $membershipbasic['desc_list'][2] = "Browse all vacancies";
        $membershipbasic['desc_list'][3] = "Create your CVVid profile PDF";


        $membershippremium['title1'] = "Premium";
        $membershippremium['price1'] = "250.00";
        $membershippremium['description1'] = "Enjoy the full benefits of the CVVid platform.";
        $membershippremium['desc_title1'] = "All Basic features plus";
        $membershippremium['desc_list1'][0] = "Browse and apply for jobs";
        $membershippremium['desc_list1'][1] = "Unique real-time messaging platform";
        $membershippremium['desc_list1'][2] = "See who has visited your profile";
        $membershippremium['desc_list1'][3] = "See who has liked your profile";

        $result = array();

        $result['basic'] = $membershipbasic; //+ $membershippremium;
        $result['premium'] = $membershippremium;
        // $result = array_merge($membershipbasic, $membershippremium);
        // $membershipbasic['BASIC'] = $chequelines;
        // $membershippremium['PREMIUM'] = $chequedetails;

        if (!empty($result)) {
            $this->_sendResponse(200, array($result));
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /* membership */


    /* get all profile documentation */

    public function actiongetallprofiledocumentation() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());
        $this->apikeyAuth();

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $model = UserDocuments::model();
        $cri = new CDbCriteria();
        $cri->alias = "ud";
        $cri->select = "ud.name as docname,ud.id,mc.name as category,m.file_name,m.model_type,m.model_id,m.collection_name,m.name,m.file_name,m.disk,m.size,m.manipulations,m.custom_properties,m.order_column";
        $cri->join = "left join cv16_media m on m.id = ud.media_id "
                . " inner join cv16_media_categories mc on mc.id = ud.category_id";
        $cri->condition = "ud.user_id =" . $id;
        $docs = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
        if (empty($docs)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $docs);
        }
    }

    /* get all profile documentation */

    /* delete profile documentations */

    public function actiondeleteprofiledocumentation() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        $this->apikeyAuth();


        $docid = isset($_GET['id']) ? $_GET['id'] : null;
        $model = UserDocuments::model()->findByPk($docid);
        $userid = $model->user_id;

        $docmodel = Users::model()->findByPk($userid);

        $cri = new CDbCriteria();
        $cri->condition = "model_id =" . $model->media_id;
        Media::model()->deleteAll($cri);

        $cri1 = new CDbCriteria();
        $cri1->condition = "user_document_id =" . $docid;
        ProfileDocuments::model()->deleteAll($cri1);

        $cri2 = new CDbCriteria();
        $cri2->condition = "id =" . $docid;
        UserDocuments::model()->deleteAll($cri2);
        //  UserDocuments::model()->deleteAll(array('condition' => "id = ':id'",'params' => array(':id' => $id),  ));
        //   ProfileDocuments::model()->deleteAll(array( 'condition' => "user_document_id = ':id'",'params' => array(':id' => $id),  ));
        $this->_sendResponse(200, "Delete Successfully");
    }

    /* delete profile documentations */

    public function actiongetallprofilelikes() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());
         $this->apikeyAuth();

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $profilemodel = Profiles::model()->findByPk($id);
        if ($id != $profilemodel->id)
            $this->_sendResponse(404, array());
        //    echo var_dump($profilemodel);die();
        $model = ProfileLikes::model();
        $cri = new CDbCriteria();
        $cri->alias = "pf";
        $cri->select = "p.slug,CONCAT('https://cvvid.com/api/images/media/',m.id,'/',m.file_name) as photo,p.photo_id";
        $cri->join = "inner join cv16_profiles p on p.id = pf.like_id
                      left outer join cv16_media m on m.id = p.photo_id";
        $cri->condition = "pf.profile_id = " . $id;
        // echo var_dump($cri);die();
        $docs = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
        if (empty($docs)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $docs);
        }
    }

//    get all profile experience
    public function actiongetallprofessionalexperience() {
         // if(!$this->_checkAuth())
         // $this->_sendResponse(405, array() );
        $this->apikeyAuth();
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);
        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

// $id = $_GET['docid'];

        $model = new UserExperiences();
        $cri = new CDbCriteria();
        $cri->alias = "ux";
        $cri->select = "ux.id,ux.company_name,ux.location,ux.position,ux.start_date,ux.end_date,ux.description";
        $cri->join = "inner join cv16_profile_experiences px on px.user_experience_id = ux.id";
        $cri->condition = "ux.user_id =" . $id;
        $exps = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
        if (empty($exps)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $exps);
        }
    }

    public function actionupdateVideoName() { 
        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        $this->apikeyAuth();


        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $vmodel = Videos::model()->findByPk($id);
        if ($id != $vmodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $now = new DateTime();

        $videoname = $_POST['name'];
        
        $vmodel->name = $videoname;
        $vmodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($vmodel->save()) {
            $this->_sendResponsewithMessage(200, sprintf("Successfully updated", ""));
        } else {
            $this->_sendResponse(509, array());
        }
    }

    // /new function

    function rrmdir($dir) {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir . "/" . $object) == "dir")
                        rrmdir($dir . "/" . $object);
                    else
                        unlink($dir . "/" . $object);
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }

    public function actionprofileVideoDelete() {
        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        $this->apikeyAuth();



        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $id = $_POST['id'];

        $vmodel = Videos::model()->findByPk($id);

        $videoid = $vmodel->id;

        $pvmodel = ProfileVideos::model()->findByAttributes(array('video_id' => $videoid));

        Yii::import('application.extensions.amazon.components.*');

        $bucket = Yii::app()->params['AWS_BUCKET'];

        $s3 = new A2S3();

        try {

            // Delete an object from the bucket.
            $s3->deleteObject([
                'Bucket' => $bucket,
                'Key' => $vmodel->video_id
            ]);

            // $s3->deleteObject(new DeleteObjectRequest($bucket, $vmodel->video_id));
        } catch (S3Exception $e) {
            // Catch an S3 specific exception.
            $this->_sendResponse(507, $e->getMessage());
            // echo $e->getMessage();
        }


        if ($pvmodel != null) {
            $mediamodel = Media::model()->findByAttributes(array('model_id' => $pvmodel['video_id'], 'collection_name' => 'video'));


            if ($mediamodel != null) {
                if (is_dir('images/media/' . $mediamodel->id))
                    $this->rrmdir('images/media/' . $mediamodel->id . '/');
                else
                    $mediamodel = Media::model()->deleteAllByAttributes(array('model_id' => $pvmodel['video_id'], 'collection_name' => 'video'));
                $vmodel = Videos::model()->deleteByPk($videoid);
                ProfileVideos::model()->deleteAllByAttributes(array('video_id' => $videoid));
            }
        }

        $this->_sendResponsewithMessage(200, sprintf("Successfully deleted", ""));
    }

    public function actionprofileVideoDefault() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());
         $this->apikeyAuth();
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $videoid = $_POST['id'];
        $vmodel = Videos::model()->findByPk($videoid);
        $userid = $vmodel->model_id;
        $usermodel = Users::model()->findByPk($userid);
        $pmodel = Profiles::model()->findByAttributes(array('owner_id' => $userid, 'type' => $usermodel->type));
        ProfileVideos::model()->updateAll(array('is_default' => 0), 'profile_id =:profile_id', array(':profile_id' => $pmodel->id));
        ProfileVideos::model()->updateAll(array('is_default' => 1), 'video_id =:video_id', array(':video_id' => $videoid));
        $this->_sendResponsewithMessage(200, sprintf("Successfully maked default", ""));
    }

    // get all category
    public function actiongetallcategory() {
     // if(!$this->_checkAuth())
     //     $this->_sendResponse(405, array());
         $this->apikeyAuth();
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }


        $cri = new CDbCriteria();
        $cri->order = 'name ASC';
//        $cri->condition = "deleted_at is null";

        $model = SkillCategories::model();

        $Industries = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (count($Industries) <= 0) {
            $this->_sendResponse(503, array());
        } else
            $this->_sendResponse(200, $Industries);
    }

// getallskills
    public function actiongetallskills() {
     // if(!$this->_checkAuth())
     //     $this->_sendResponse(405, array());

         $this->apikeyAuth();

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());


        $cri = new CDbCriteria();
        $cri->condition = "skill_category_id=" . $_GET['id'];

        $model = Skills::model();

        $Industries = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (count($Industries) <= 0) {
            $this->_sendResponse(503, array());
        } else
            $this->_sendResponse(200, $Industries);
    }

    /* update profile picture */

    public function actionupdateprofilepicture() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        $this->apikeyAuth();

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        if (!isset($_POST['mediaid']) || !isset($_POST['profileid']))
            $this->_sendResponse(500, array());
        $mediaid = $_POST['mediaid'];
        $profileid = $_POST['profileid'];
        $model = Profiles::model()->findByPk($profileid);
        $model->photo_id = $mediaid;
        if ($model->save()) {
            $this->_sendResponsewithMessage(200, sprintf("Profile picture updated successfully", ""));
        } else {
            $this->_sendResponse(500, array());
        }
    }

    public function actioninsertMediaManager() {
        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());



        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        if (!isset($_GET['id']) || !isset($_GET['id']))
            $this->_sendResponse(500, array());


        $filename = $_FILES['media']['name'];
//        $image_type = $_FILES['media']['type'];
        $file = $_FILES['media']['tmp_name'];
        $name = pathinfo($_FILES['media']['name'], PATHINFO_FILENAME);
        
        $image_type = pathinfo(basename($_FILES['media']['name']), PATHINFO_EXTENSION);


        $mediamodel = new Media;

        $cri = new CDbCriteria();
        $cri->select = 'max(order_column) as order_column';
        $value = $mediamodel->find($cri);

        $now = new DateTime();

        $mediamodel->model_id = $_POST['model_id'];
        $mediamodel->name = $name;
        $mediamodel->file_name = $filename;
        $mediamodel->size = $_FILES['media']['size'];
        $mediamodel->model_type = $_POST['model_type'];
        $mediamodel->collection_name = $_POST['collection_name'];
        $mediamodel->order_column = $value->order_column + 1;
        $mediamodel->created_at = $now->format('Y-m-d H:i:s');
        if ($mediamodel->save()) {

            $mediafolder = "images/media/" . $mediamodel->id . "/conversions";

            if (!file_exists($mediafolder)) {
                mkdir($mediafolder, 0777, true);
            }

            $imgprefix = "images/media/" . $mediamodel->id;
            $fullpath = "images/media/" . $mediamodel->id . "/" . $filename;

            if (move_uploaded_file($_FILES['media']['tmp_name'], $fullpath)) {
                //create cover
                $coverwidth = 1000;
                $coverheight = 400;
                $coverdestpath = $imgprefix . "/conversions/cover.jpg";

                //create icon
                $iconwidth = 40;
                $iconheight = 40;
                $icondestpath = $imgprefix . "/conversions/icon.jpg";

                //create joblisting
                $jlwidth = 150;
                $jlheight = 85;
                $jldestpath = $imgprefix . "/conversions/joblisting.jpg";

                //create search
                $searchwidth = 167;
                $searchheight = 167;
                $searchdestpath = $imgprefix . "/conversions/search.jpg";

                //create thumb
                $thumbwidth = 126;
                $thumbheight = 126;
                $thumbdestpath = $imgprefix . "/conversions/thumb.jpg";

                if ($image_type == "jpg" || $image_type == "jpeg") {

                    $image = imagecreatefromjpeg($fullpath);

                    $this->resize_image($image, $image_type, $fullpath, $coverdestpath, $coverwidth, $coverheight); //create cover
                    $this->resize_image($image, $image_type, $fullpath, $icondestpath, $iconwidth, $iconheight); //create icon
                    $this->resize_image($image, $image_type, $fullpath, $jldestpath, $jlwidth, $jlheight); //create joblisting
                    $this->resize_image($image, $image_type, $fullpath, $searchdestpath, $searchwidth, $searchheight); //create search
                    $this->resize_image($image, $image_type, $fullpath, $thumbdestpath, $thumbwidth, $thumbheight); //create thumb
                } elseif ($image_type == "image/gif") {

                    $image = imagecreatefromgif($fullpath);

                    $this->resize_image($image, $image_type, $fullpath, $coverdestpath, $coverwidth, $coverheight); //create cover
                    $this->resize_image($image, $image_type, $fullpath, $icondestpath, $iconwidth, $iconheight); //create icon
                    $this->resize_image($image, $image_type, $fullpath, $jldestpath, $jlwidth, $jlheight); //create joblisting
                    $this->resize_image($image, $image_type, $fullpath, $searchdestpath, $searchwidth, $searchheight); //create search
                    $this->resize_image($image, $image_type, $fullpath, $thumbdestpath, $thumbwidth, $thumbheight); //create thumb
                } elseif ($image_type == "png") {

                    $image = imagecreatefrompng($fullpath);

                    $this->resize_image($image, $image_type, $fullpath, $coverdestpath, $coverwidth, $coverheight); //create cover
                    $this->resize_image($image, $image_type, $fullpath, $icondestpath, $iconwidth, $iconheight); //create icon
                    $this->resize_image($image, $image_type, $fullpath, $jldestpath, $jlwidth, $jlheight); //create joblisting
                    $this->resize_image($image, $image_type, $fullpath, $searchdestpath, $searchwidth, $searchheight); //create search
                    $this->resize_image($image, $image_type, $fullpath, $thumbdestpath, $thumbwidth, $thumbheight); //create thumb
                }
            }

            $mediaid = $mediamodel->id;
            $model = Profiles::model()->findByAttributes(array('owner_id' => $_GET['id']));

            //echo var_dump($model);die();

            $model->photo_id = $mediaid;
            if ($model->save()) {
                $result = array();

                $result['url'] = Yii::app()->baseUrl . "/" . $fullpath;

                if (!empty($result)) {
                    $this->_sendResponse(200, $result);
                } else {
                    $this->_sendResponse(507, array());
                }
            } else
                $this->_sendResponse(507, array());
        }

        // echo $filename;
    }

    /* update profile picture */
    /* supporting documentation */
    /* career expereience */

    public function actiongetcarrerexperience() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        $this->apikeyAuth();

        if (!isset($_GET['id']) || $_GET['id'] == NULL)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];

        $pskillsmodel = ProfileSkills::model()->findAllByAttributes(array('profile_id' => $id));

        $pskillarr = array();

        foreach ($pskillsmodel as $pskill)
            $pskillarr[] = $pskill['skill_id'];

        $skillcateg = array();
        $skillarr = array();
        $cri = new CDbCriteria();
        $cri->alias = "s";
        $cri->select = 'distinct s.skill_category_id';
        $cri->join = "inner join cv16_profile_skills ps on ps.skill_id = s.id "
                . "inner join cv16_skill_categories sc on sc.id = s.skill_category_id";
        $cri->condition = "ps.profile_id=" . $id;
        $skillmodel = Skills::model()->findAll($cri);

        foreach ($skillmodel as $skill) {
            $skillcateg[] = $skill['skill_category_id'];
        }


        $subarr = array();
        $result = array();

        for ($i = 0; $i < count($skillcateg); $i++) {
            $skillcategmodel = SkillCategories::model()->findByPk($skillcateg[$i]);
            $catarr = array();
            $catarr['name'] = $skillcategmodel->name;
            $catarr['id'] = $skillcategmodel->id;

            $ids = implode(',', $pskillarr);
            $pcri = new CDbCriteria();
            $pcri->select = 'id, name';
            $pcri->condition = "id in (" . $ids . ") and skill_category_id =" . $skillcateg[$i];
//            $skillmodel = Skills::model()->findAll($pcri);

            $skillmodel = Skills::model()->getCommandBuilder()->createFindCommand(Skills::model()->tableSchema, $pcri)->queryAll();
            $catarr['subcategory'] = $skillmodel;
            $result[] = $catarr;
        }

        if (!empty($result)) {
            $this->_sendResponse(200, $result);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    public function actionvideoupload() {
        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());
        if (!isset($_GET['profile_id']) || $_GET['profile_id'] == 0)
            $this->_sendResponse(404, array());


        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        
        $now = new DateTime();
        $profilemodel = Profiles::model()->findByPk($_GET['profile_id']);
        $userid = $profilemodel->owner_id;
        $name = str_replace(' ', '_', $_POST['video_name']);
        $videoname = str_replace(' ', '_', $_FILES['video']['name']);
        $size = $_FILES['video']['size'];
        $image_type = $_FILES['video']['type'];
        $tmp = $_FILES['video']['tmp_name'];
        $ext = $this->getExtension($videoname);

        $usermodel = Users::model()->findByPk($userid);
        $pmodel = Profiles::model()->findByAttributes(array('owner_id' => $userid, 'type' => $usermodel->type));

        Yii::import('application.extensions.amazon.components.*');

        $bucket = Yii::app()->params['AWS_BUCKET'];
        $s3 = new A2S3();

        $vname = pathinfo($name, PATHINFO_FILENAME);

        $actual_image_name = $vname . time() . "." . $ext;
        // $target_path = Yii::app()->getBaseUrl(true); // Target path where file is to be stored
        // $target_path = $target_path . basename($_FILES['video']['name']);
        if (strlen($name) > 0) {
            try {

                $response = $s3->putObject(array(
                    'SourceFile' => $tmp,
                    'Bucket' => $bucket,
                    'Key' => $actual_image_name,
                    'ACL' => 'public-read',
                    'x-amz-storage-class' => 'REDUCED_REDUNDANCY'
                ));

                //  $message = "S3 Upload Successful.";
                //  $s3file = 'http://' . $bucket . '.s3.amazonaws.com/' . $actual_image_name;
                //  echo $s3file;die();
                // echo "<img src='$s3file'/>";
                //  echo 'S3 File URL:' . $s3file;
                //    $vname = pathinfo($name, PATHINFO_FILENAME);

                $duration = 6;

                $videomodel = new Videos();
                $videomodel->model_id = $userid;
                $videomodel->video_id = $actual_image_name;
                $videomodel->name = $vname;
                $videomodel->duration = 0;
                $videomodel->model_type = $usermodel->type;
                $videomodel->category_id = 1;
                $videomodel->is_processed = 1;
                $videomodel->state = 1;
                $videomodel->status = "active";
                $videomodel->created_at = $now->format('Y-m-d H:i:s');
                if ($videomodel->save()) {

                    $pvideomodel = new ProfileVideos();
                    $pvideomodel->profile_id = $pmodel->id;
                    $pvideomodel->video_id = $videomodel->id;
                    $pvideomodel->created_at = $now->format('Y-m-d H:i:s');
                    $pvideomodel->state = 1;
                    $pvideomodel->save();

                    $sec = 0;

                    $fullpath = 'https://' . $bucket . '.s3.amazonaws.com/' . $actual_image_name;

                    $cmd = "ffmpeg -i $fullpath 2>&1";
                    $ffmpeg = shell_exec($cmd);

                    $search = "/Duration: (.*?)\./";
                    preg_match($search, $ffmpeg, $matches);

                    if (isset($matches[1])) {
                        $data['duration'] = $matches[1];
                        $time_sec = explode(':', $data['duration']);
                        $sec = ($time_sec['0'] * 3600) + ($time_sec['1'] * 60) + $time_sec['2'];
                    }

                    Videos::model()->updateByPk($videomodel->id, array('duration' => $sec));

                    $mediamodel = new Media();
                    $mediamodel->model_id = $videomodel->id;
                    $mediamodel->disk = "media";
                    $mediamodel->model_type = $usermodel->type;
                    $mediamodel->collection_name = "video";
                    $mediamodel->name = $vname;
                    $mediamodel->file_name = $vname . ".jpg";
                    $mediamodel->created_at = $now->format('Y-m-d H:i:s');
                    $mediamodel->size = (int) $sec;
                    if ($mediamodel->save()) {
                        //$filename = $vname;

                        Media::model()->updateByPk($mediamodel->id, array('model_id' => $videomodel->id, 'size' => $sec));

                        $iname = $vname . ".jpg";
                        $mediafolder = "images/media/" . $mediamodel->id;

                        if (!file_exists($mediafolder)) {
                            mkdir($mediafolder, 0777, true);
                        }

                        $imgfullpath = "images/media/" . $mediamodel->id . "/" . $iname;
                        $imgprefix = "images/media/" . $mediamodel->id;

                        $ffurl = "ffmpeg -i $fullpath -ss 00:00:2 -vframes 1 $imgfullpath";

                        $process = exec($ffurl);

                        $jlwidth = 350;
                        $jlheight = 190;
                        $jldestpath = $imgprefix . "/conversions/profile.jpg";

                        //create search
                        $searchwidth = 167;
                        $searchheight = 167;
                        $searchdestpath = $imgprefix . "/conversions/search.jpg";

                        //create thumb
                        $thumbwidth = 126;
                        $thumbheight = 126;
                        $thumbdestpath = $imgprefix . "/conversions/thumb.jpg";
                        $image_type == "image/jpg";

                        $image = imagecreatefromjpeg($imgfullpath);

                        $this->resize_image($image, $image_type, $imgfullpath, $jldestpath, $jlwidth, $jlheight); //create joblisting
                        $this->resize_image($image, $image_type, $imgfullpath, $searchdestpath, $searchwidth, $searchheight); //create search
                        $this->resize_image($image, $image_type, $imgfullpath, $thumbdestpath, $thumbwidth, $thumbheight); //create thumb
                        // echo "The video has been uploaded";
                    }
                    // else
                    // {
                    //     // echo var_dump($mediamodel->getErrors());
                    //  }

                    $this->_sendResponsewithMessage(200, sprintf("The video has been uploaded"));
                } else {
                    echo var_dump($videomodel->getErrors());
                }
            } catch (S3Exception $e) {
                // Catch an S3 specific exception.
                $this->_sendResponse(507, $e->getMessage());
                // echo $e->getMessage();
            }
        }


        /* try {
          //            //throw exception if can't move the file
          //            if (!move_uploaded_file($_FILES['video']['tmp_name'], $target_path)) {
          //               // throw new Exception('Could not move file');
          //            }

          $now = new DateTime();
          $userid = $_GET['profile_id'];
          $name = $_POST['video_name'];
          $size = $_FILES['video']['size'];
          $image_type = $_FILES['video']['type'];
          $filename = $_FILES['video']['name'];

          $usermodel = Users::model()->findByPk($userid);
          $pmodel = Profiles::model()->findByAttributes(array('owner_id' => $userid, 'type' => $usermodel->type));

          $vname = pathinfo($name, PATHINFO_FILENAME);

          $mediamodel = new Media();
          $mediamodel->disk = "media";
          $mediamodel->model_type = $usermodel->type;
          $mediamodel->collection_name = "video";
          $mediamodel->name = $vname;
          $mediamodel->file_name = $vname . ".jpg";
          $mediamodel->created_at = $now->format('Y-m-d H:i:s');
          $mediamodel->size = 0; //(int)$duration;
          if ($mediamodel->save()) {
          $mediafolder = "images/media/" . $mediamodel->id;

          if (!file_exists($mediafolder)) {
          mkdir($mediafolder, 0777, true);
          }

          $fullpath = "images/media/" . $mediamodel->id . "/" . $name;

          if (move_uploaded_file($_FILES['video']['tmp_name'], $fullpath)) {
          $videomodel = new Videos();
          $videomodel->model_id = $userid;
          $videomodel->video_id = $fullpath;
          $videomodel->name = $vname;
          $videomodel->duration = 0;
          $videomodel->model_type = $usermodel->type;
          $videomodel->category_id = 1;
          $videomodel->is_processed = 1;
          $videomodel->state = 1;
          $videomodel->status = "active";
          $videomodel->created_at = $now->format('Y-m-d H:i:s');
          if ($videomodel->save()) {

          $pvideomodel = new ProfileVideos();
          $pvideomodel->profile_id = $pmodel->id;
          $pvideomodel->video_id = $videomodel->id;
          $pvideomodel->created_at = $now->format('Y-m-d H:i:s');
          $pvideomodel->state = 1;
          $pvideomodel->save();

          $sec = 0;

          $cmd = "ffmpeg -i $fullpath 2>&1";
          $ffmpeg = shell_exec($cmd);

          $search = "/Duration: (.*?)\./";
          preg_match($search, $ffmpeg, $matches);

          if (isset($matches[1])) {
          $data['duration'] = $matches[1];
          $time_sec = explode(':', $data['duration']);
          $sec = ($time_sec['0'] * 3600) + ($time_sec['1'] * 60) + $time_sec['2'];
          }

          Videos::model()->updateByPk($videomodel->id, array('duration' => $sec));
          Media::model()->updateByPk($mediamodel->id, array('model_id' => $videomodel->id, 'size' => $sec));

          $iname = $vname . ".jpg";
          $imgfullpath = "images/media/" . $mediamodel->id . "/" . $iname;
          $imgprefix = "images/media/" . $mediamodel->id;


          $process = exec("ffmpeg -i $fullpath -ss 00:00:2 -vframes 1 $imgfullpath");

          $jlwidth = 350;
          $jlheight = 190;
          $jldestpath = $imgprefix . "/conversions/profile.jpg";

          //create search
          $searchwidth = 167;
          $searchheight = 167;
          $searchdestpath = $imgprefix . "/conversions/search.jpg";

          //create thumb
          $thumbwidth = 126;
          $thumbheight = 126;
          $thumbdestpath = $imgprefix . "/conversions/thumb.jpg";
          $image_type == "image/jpg";

          $image = imagecreatefromjpeg($imgfullpath);

          $this->resize_image($image, $image_type, $imgfullpath, $jldestpath, $jlwidth, $jlheight); //create joblisting
          $this->resize_image($image, $image_type, $imgfullpath, $searchdestpath, $searchwidth, $searchheight); //create search
          $this->resize_image($image, $image_type, $imgfullpath, $thumbdestpath, $thumbwidth, $thumbheight); //create thumb

          echo "The video has been uploaded";

          }
          }
          }
          } catch (Exception $e) {
          die('File did not upload: ' . $e->getMessage());
          } */
    }

    // get image 
    public function actionmedialibrary() {
        
        
          // if (!$this->_checkAuth())
          //  $this->_sendResponse(405, array());

        if (!isset($_GET['id']) && !isset($_GET['type']))
            $this->_sendResponse(500, array());


        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        if ($_GET['type'] == 'candidate') {
            $mediamodel = Media::model()->findAll(array(
                'condition' => 'model_id=:model_id AND model_type=:model_type AND collection_name=:collection_name',
                'params' => array(':model_id' => $_GET['id'], ':model_type' => 'App\\Users\\Candidate', ':collection_name' => 'photos'),
            ));
        } else if ($_GET['type'] == 'employer') {
            $mediamodel = Media::model()->findAll(array(
                'condition' => 'model_id=:model_id AND model_type=:model_type AND collection_name=:collection_name',
                'params' => array(':model_id' => $_GET['id'], ':model_type' => 'App\Employer', ':collection_name' => 'photos'),
            ));
        } else if ($_GET['type'] == 'institution') {
            $mediamodel = Media::model()->findAll(array(
                'condition' => 'model_id=:model_id AND model_type=:model_type AND collection_name=:collection_name',
                'params' => array(':model_id' => $_GET['id'], ':model_type' => 'App\Education\Institution', ':collection_name' => 'photos'),
            ));
        }

        $samplearr = array();
        $photosarr = array();
        $mediaarr1 = array();
        $mediaarr2 = array();
        $result = array();

        if (count($mediamodel) > 0) {

            foreach ($mediamodel as $media) {
                $mediaarr1['id'] = $media['id'];
                $mediaarr1['url'] = 'https://cvvid.com/images/media/' . $media['id'] . '/conversions/search.jpg';
                $photosarr[] = $mediaarr1;
            }
        }
        $result['photos'] = $photosarr;

        $mediamodel1 = Media::model()->findAll(array(
            'condition' => 'model_id=:model_id AND collection_name=:collection_name',
            'params' => array(':model_id' => 1, ':collection_name' => 'sample'),
        ));

        if (count($mediamodel1) > 0) {

            foreach ($mediamodel1 as $media) {
                $mediaarr2['id'] = $media['id'];
                $mediaarr2['url'] = 'https://cvvid.com/images/media/' . $media['id'] . '/conversions/search.jpg';
                $samplearr[] = $mediaarr2;
            }
        }
        $result['sample'] = $samplearr;

        if (count($result) > 0)
            $this->_sendResponse(200, array($result));
        else
            $this->_sendResponse(500, array());
    }

    public function getExtension($str) {
        $i = strrpos($str, ".");
        if (!$i) {
            return "";
        }
        $l = strlen($str) - $i;
        $ext = substr($str, $i + 1, $l);
        return $ext;
    }

    function resize_image($image, $image_type, $fullpath, $destpath, $width, $height) {
        $size_arr = getimagesize($fullpath);

        list($width_orig, $height_orig, $img_type) = $size_arr;
        $ratio_orig = $width_orig / $height_orig;

        $tempimg = imagecreatetruecolor($width, $height);
        imagecopyresampled($tempimg, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

        if ($image_type == "image/jpg" || $image_type == "image/jpeg")
            imagejpeg($tempimg, $destpath);
        else if ($image_type == "image/png")
            imagepng($tempimg, $destpath);
        else if ($image_type == "image/gif")
            imagegif($tempimg, $destpath);
    }

    public function actionbusinesscard() {

        $this->apikeyAuth();

         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $actual_link = "http://cvvidcareers.co.uk";
        $usermodel = Users::model()->findByPk($id);
        $cri = new CDbCriteria();
        $cri->alias = 'u';
        $cri->select = "u.forenames,u.surname,u.email,p.slug";
        $cri->join = 'inner join cv16_profiles p on p.owner_id = u.id';
        $cri->condition = 'u.id=' . $id;
        $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
        foreach ($users as $key => $value) {
            $viewservice = $value;
        }
        $viewservice['slug'] = $actual_link . '/' . $users[0]['slug'];
        if (!empty($users)) {
            $this->_sendResponse(200, array($viewservice));
        } else {
            $this->_sendResponse(507, array());
        }
    }

    public function actionviewBCard($id) {

        $this->apikeyAuth();

        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());
        $usermodel = Users::model()->findByPk($id);

        $mPDF1 = Yii::app()->ePdf->mpdf();
        $mPDF1 = Yii::app()->ePdf->mpdf('c', 'A7-L');
        //      $stylesheet = file_get_contents(Yii::app()->getBaseUrl(true) . '/themes/Dev/css/style.css');
        //      $mPDF1->WriteHTML($stylesheet, 1);
        // $mPDF1->AddPage('L');
        // $mPDF1->SetDisplayMode('fullwidth');
        $mPDF1->WriteHTML($this->renderPartial('business-card', array('model' => $usermodel), true));
        $mPDF1->Output("images/pdf-" . $id . ".pdf", 'F');

        if (!empty($usermodel)) {
            $this->_sendResponse(200, array(array("url" => Yii::app()->getBaseUrl(true) . "/images/pdf-" . $id . ".pdf")));
        } else {
            $this->_sendResponse(507, array());
        }
        // $this->render('businesscard',array('model'=>$usermodel));
    }

    public function actiondownloadResume($id) {

        $usermodel = Users::model()->findByPk($id);

        $mPDF1 = Yii::app()->ePdf->mpdf();
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
 
        $mPDF1->WriteHTML($this->renderPartial('resume', array('model' => $usermodel), true));
        $mPDF1->Output("images/resume-" . $id . ".pdf", 'F');

        if (!empty($usermodel)) {
            $this->_sendResponse(200, array(array("url" => Yii::app()->getBaseUrl(true) . "/images/resume-" . $id . ".pdf")));
        } else {
            $this->_sendResponse(507, array());
        }
        // $this->render('businesscard',array('model'=>$usermodel));
    }

     public function actioncreateemployer() {

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $usermodel = new Users;
        $empmodel = new Employers;
        $empusermodel = new EmployerUsers;
        $profilemodel = new Profiles;
        $addressmodel = new Addresses;

        // Generating API key
        $api_key = $this->generateApiKey();
        $now = new DateTime();

        $usertable = Yii::app()->db->schema->getTable(Users::model()->tableName());
        $employerstable = Yii::app()->db->schema->getTable(Employers::model()->tableName());
        $employerusersstable = Yii::app()->db->schema->getTable(EmployerUsers::model()->tableName());
        $profiletable = Yii::app()->db->schema->getTable(Profiles::model()->tableName());
        $addresstable = Yii::app()->db->schema->getTable(Addresses::model()->tableName());




        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                if ($var == "email") {
                    $mmodel = Users::model()->findByAttributes(array('email' => $value));
                    if ($mmodel != null) {
                        $this->_sendResponse(502, array());
                        die();
                    }
                }
                $usermodel->$var = $value;
            } else if ($empmodel->hasAttribute($var)) {
                $empmodel->$var = $value;
            } else if ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else if ($empusermodel->hasAttribute($var)) {
                $empusermodel->$var = $value;
            } else if ($addressmodel->hasAttribute($var)) {
                $addressmodel->$var = $value;
            } else if ($var == 'ispremium') {
                
            } else if (!isset($usertable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($employerstable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($employerusersstable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($profiletable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($addresstable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else {
                $this->_sendResponse(500, array());
            }
        }

        $usermodel->created_at = $now->format('Y-m-d H:i:s');
        $usermodel->type = 'employer';
        if (isset($_POST['county']) && $_POST['country'])
        $usermodel->location = $_POST['county'] . ($_POST['county'] != '' ? ', ' . $_POST['country'] : '');
        if (isset($_POST['latitude']) && $_POST['latitude'])
            $usermodel->location_lat = $_POST['latitude'];
        if (isset($_POST['longitude']) && $_POST['longitude'])
            $usermodel->location_lng = $_POST['longitude'];
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $email = $usermodel->email;
        $usermodel->api_key = $api_key;
        $usermodel->careers = 1;

//        $empusermodel->employer_id = $empmodel->id;
        $empmodel->name = $_POST['name'];
        $empusermodel->employer_id = $empmodel->id;
       // $profilemodel->slug = $this->getValidatedSlug($usermodel->forenames . "-" . $usermodel->surname);
        $profilemodel->slug =$this->getValidatedSlug($_POST['slug']);
        $valid = $usermodel->validate();
        $valid = $addressmodel->validate() && $valid;
        $valid = $profilemodel->validate() && $valid;
        $valid = $empmodel->validate() && $valid;
//        $valid = $empusermodel->validate() && $valid;
        if ($valid) {
            $usermodel->save();

            $empmodel->name = $_POST['name'];
            $empmodel->user_id = $usermodel->id;
            $empmodel->email = $usermodel->email;
            $empmodel->tel = $usermodel->mobile;
            $empmodel->location = $usermodel->location;
            $empmodel->website = $_POST['website'];
            $empmodel->location_lat = $usermodel->location_lat;
            $empmodel->location_lng = $usermodel->location_lng;
            $empmodel->created_at = $now->format('Y-m-d H:i:s');
            if (isset($_POST['ispremium']) && $_POST['ispremium'])
                $empmodel->trial_ends_at = Carbon::now()->addYear()->toDateTimeString();
            $empmodel->save();
            $empusermodel->employer_id = $empmodel->id;
            $empusermodel->user_id = $usermodel->id;
            $empusermodel->role = 'admin';
            $empusermodel->created_at = $now->format('Y-m-d H:i:s');
            $empusermodel->save();
            // if($_POST['industry_id'] > 0)
            $profilemodel->industry_id = $_POST['industry_id'];
            $profilemodel->type = 'employer';
            $profilemodel->owner_id = $empmodel->id;
            $profilemodel->owner_type = 'employer';
            $profilemodel->slug =$this->getValidatedSlug($_POST['slug']);
            $profilemodel->save();
           
            $addressmodel->created_at = $now->format('Y-m-d H:i:s');
            $addressmodel->model_type = 'employer';
            $addressmodel->model_id = $empmodel->id;
            if ($addressmodel->save()) {
                $cri = new CDbCriteria();
                $cri->alias = 'em';
                $cri->select = 'eu.employer_id as employer_id,eu.user_id,eu.id,em.forenames,em.surname,em.email,em.mobile,em.api_key,p.id as profile_id,p.slug';
                $cri->join = "left outer join cv16_profiles p on p.owner_id = em.id left outer join cv16_employer_users eu on eu.user_id = em.id";
                $cri->condition = "em.id = " . $usermodel->id;
                $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                $this->notifycreateemployer($email);
                $this->_sendResponse(200, $users);
            }
        } else {

            // $errors[] = $usermodel->errors;
            // $errors[] = $profilemodel->errors;
            // $errors[] = $addressmodel->errors;
            $usrerr = array();

            $usercolumns = Users::model()->getAttributes();
            $employercolumns = Employers::model()->getAttributes();
            $employeruserscolumns = EmployerUsers::model()->getAttributes();
            $addresscolumns = Addresses::model()->getAttributes();
            $profilecolumns = Profiles::model()->getAttributes();

            foreach ($usercolumns as $key => $val) {
                if ($usermodel->getError($key) != "") {
                    $usrerr[$key] = $usermodel->getError($key);
                }
            }
            foreach ($addresscolumns as $key => $val) {
                if ($addressmodel->getError($key) != null)
                    $usrerr[$key] = $addressmodel->getError($key);
            }
            foreach ($profilecolumns as $key => $val) {
                if ($profilemodel->getError($key) != null)
                    $usrerr[$key] = $profilemodel->getError($key);
            }
            foreach ($employercolumns as $key => $val) {
                if ($empmodel->getError($key) != null)
                    $usrerr[$key] = $empmodel->getError($key);
            }
//            foreach ($employeruserscolumns as $key => $val) {
//                if ($empusermodel->getError($key) != null)
//                    $usrerr[$key] = $empusermodel->getError($key);
//            }
            $this->_sendResponse(512, $usrerr);
        }
    }



public function actioncreatepremiumemployer() {

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $usermodel = new Users;
        $empmodel = new Employers;
        $empusermodel = new EmployerUsers;
        $profilemodel = new Profiles;
        $addressmodel = new Addresses;

        // Generating API key
        $api_key = $this->generateApiKey();
        $now = new DateTime();

        $usertable = Yii::app()->db->schema->getTable(Users::model()->tableName());
        $employerstable = Yii::app()->db->schema->getTable(Employers::model()->tableName());
        $employerusersstable = Yii::app()->db->schema->getTable(EmployerUsers::model()->tableName());
        $profiletable = Yii::app()->db->schema->getTable(Profiles::model()->tableName());
        $addresstable = Yii::app()->db->schema->getTable(Addresses::model()->tableName());

        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                if ($var == "email") {
                    $mmodel = Users::model()->findByAttributes(array('email' => $value));
                    if ($mmodel != null) {
                        $this->_sendResponse(502, array());
                        die();
                    }
                }
                $usermodel->$var = $value;
            } else if ($empmodel->hasAttribute($var)) {
                $empmodel->$var = $value;
            } else if ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else if ($empusermodel->hasAttribute($var)) {
                $empusermodel->$var = $value;
            } else if ($addressmodel->hasAttribute($var)) {
                $addressmodel->$var = $value;
            } else if ($var == 'ispremium') {
                
            }else if($var == 'cvc'){
                 // Column doesn't exist
            }else if($var == 'plan_id'){
                 // Column doesn't exist
            }else if($var == 'token'){
                 // Column doesn't exist
            }else if (!isset($usertable->columns[$var])) { 
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($employerstable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($employerusersstable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($profiletable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($addresstable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else {
                $this->_sendResponse(500, array());
            }
        }

        $usermodel->created_at = $now->format('Y-m-d H:i:s');
        $usermodel->type = 'employer';
        if (isset($_POST['county']) && $_POST['country'])
        $usermodel->location = $_POST['county'] . ($_POST['county'] != '' ? ', ' . $_POST['country'] : '');
        if (isset($_POST['latitude']) && $_POST['latitude'])
            $usermodel->location_lat = $_POST['latitude'];
        if (isset($_POST['longitude']) && $_POST['longitude'])
            $usermodel->location_lng = $_POST['longitude'];
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $email = $usermodel->email;
        $usermodel->api_key = $api_key;
        $usermodel->careers = 1;

//        $empusermodel->employer_id = $empmodel->id;
        $empmodel->name = $_POST['name'];
        $empusermodel->employer_id = $empmodel->id;
       // $profilemodel->slug = $this->getValidatedSlug($usermodel->forenames . "-" . $usermodel->surname);
        $profilemodel->slug =$this->getValidatedSlug($_POST['slug']);
        $valid = $usermodel->validate();
        $valid = $addressmodel->validate() && $valid;
        $valid = $profilemodel->validate() && $valid;
        $valid = $empmodel->validate() && $valid;
//        $valid = $empusermodel->validate() && $valid;
        if ($valid) {
            $usermodel->save();

            $empmodel->name = $_POST['name'];
            $empmodel->user_id = $usermodel->id;
            $empmodel->email = $usermodel->email;
            $empmodel->tel = $usermodel->mobile;
            $empmodel->location = $usermodel->location;
            $empmodel->website = $_POST['website'];
            $empmodel->location_lat = $usermodel->location_lat;
            $empmodel->location_lng = $usermodel->location_lng;
            $empmodel->created_at = $now->format('Y-m-d H:i:s');
            if (isset($_POST['ispremium']) && $_POST['ispremium'])
                $empmodel->trial_ends_at = Carbon::now()->addYear()->toDateTimeString();
            $empmodel->save();
            $empusermodel->employer_id = $empmodel->id;
            $empusermodel->user_id = $usermodel->id;
            $empusermodel->role = 'admin';
            $empusermodel->created_at = $now->format('Y-m-d H:i:s');
            $empusermodel->save();
            // if($_POST['industry_id'] > 0)
            $profilemodel->industry_id = $_POST['industry_id'];
            $profilemodel->type = 'employer';
            $profilemodel->owner_id = $empmodel->id;
            $profilemodel->owner_type = 'employer';
            $profilemodel->slug =$this->getValidatedSlug($_POST['slug']);
            $profilemodel->save();
           
            $addressmodel->created_at = $now->format('Y-m-d H:i:s');
            $addressmodel->model_type = 'employer';
            $addressmodel->model_id = $empmodel->id;
            if ($addressmodel->save()) {
                
                $usermodel = Users::model()->findByPk($usermodel->id);

                $now = new DateTime();
                $model = Employers::model()->findByAttributes(array('user_id' => $usermodel->id));
                //    $usermodel = Users::model()->findByPk(array('id'=>$id));

                require Yii::app()->basePath . '/extensions/stripe/init.php';

                Stripe::setApiKey(Yii::app()->params['SECRET_Key']);

                $plan = $_POST['plan_id'];
                $token = $_POST['token'];
                $trial_ends = Carbon::now()->addYear()->toDateTimeString();

                $planarr = Plan::retrieve($plan);

                $amount = $planarr->amount;
                $planname = $planarr->name;
                $currency = $planarr->currency;

                try{
                //        create subscriber
                $customer = Customer::create(array(
                            'source' => $token,
                            'email' => $model->email,
                            'plan' => $plan,
                            'description' => $model->name
                ));
                }catch(Exception $ex)
                {
                } 

                $model->stripe_active = 1;
                $model->stripe_id = $customer->id;
                $model->stripe_subscription = $customer->subscriptions->data[0]->id;
                $model->stripe_plan = $plan;
                $model->last_four = $_POST['cvc'];
                $model->trial_ends_at = $trial_ends;
                $model->updated_at = $now->format('Y-m-d H:i:s');
                $model->save(false);
                
                $cri = new CDbCriteria();
                $cri->alias = 'em';
                $cri->select = 'eu.employer_id as employer_id,eu.user_id,eu.id,em.forenames,em.surname,em.email,em.mobile,em.api_key,p.id as profile_id,p.slug';
                $cri->join = "left outer join cv16_profiles p on p.owner_id = em.id left outer join cv16_employer_users eu on eu.user_id = em.id";
                $cri->condition = "em.id = " . $usermodel->id;
                $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                $this->notifycreateemployer($email);
                $this->_sendResponse(200, $users);
            }
        } else {

            // $errors[] = $usermodel->errors;
            // $errors[] = $profilemodel->errors;
            // $errors[] = $addressmodel->errors;
            $usrerr = array();

            $usercolumns = Users::model()->getAttributes();
            $employercolumns = Employers::model()->getAttributes();
            $employeruserscolumns = EmployerUsers::model()->getAttributes();
            $addresscolumns = Addresses::model()->getAttributes();
            $profilecolumns = Profiles::model()->getAttributes();

            foreach ($usercolumns as $key => $val) {
                if ($usermodel->getError($key) != "") {
                    $usrerr[$key] = $usermodel->getError($key);
                }
            }
            foreach ($addresscolumns as $key => $val) {
                if ($addressmodel->getError($key) != null)
                    $usrerr[$key] = $addressmodel->getError($key);
            }
            foreach ($profilecolumns as $key => $val) {
                if ($profilemodel->getError($key) != null)
                    $usrerr[$key] = $profilemodel->getError($key);
            }
            foreach ($employercolumns as $key => $val) {
                if ($empmodel->getError($key) != null)
                    $usrerr[$key] = $empmodel->getError($key);
            }
//            foreach ($employeruserscolumns as $key => $val) {
//                if ($empusermodel->getError($key) != null)
//                    $usrerr[$key] = $empusermodel->getError($key);
//            }
            $this->_sendResponse(512, $usrerr);
        }
    }







    public function actioncreateemployervideo() {
        $this->apikeyAuth();

        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(407, array());


        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }


        $now = new DateTime();
        $empid = $_GET['id'];
        $name = str_replace(' ', '_', $_POST['video_name']);
        $videoname = str_replace(' ', '_', $_FILES['video']['name']);
        $size = $_FILES['video']['size'];
        $image_type = $_FILES['video']['type'];
        $tmp = $_FILES['video']['tmp_name'];
        $ext = $this->getExtension($videoname);

        $empmodel = Employers::model()->findByPk($empid);

        Yii::import('application.extensions.amazon.components.*');

        $bucket = Yii::app()->params['AWS_BUCKET'];
        $s3 = new A2S3();

        $vname = pathinfo($name, PATHINFO_FILENAME);

        $actual_image_name = $vname . time() . "." . $ext;

        if (strlen($name) > 0) {

            try {
                //                $s3->putObject(array(
                //                    'Bucket' => $bucket,
                //                    'Key' => $image_name_actual,
                //                    'SourceFile' => $tmp,
                //                    'StorageClass' => 'REDUCED_REDUNDANCY'
                //                ));

                $response = $s3->putObject(array(
                    'SourceFile' => $tmp,
                    'Bucket' => $bucket,
                    'Key' => $actual_image_name,
                    'ACL' => 'public-read',
                    'x-amz-storage-class' => 'REDUCED_REDUNDANCY'
                ));

                //  $message = "S3 Upload Successful.";
                //  $s3file = 'http://' . $bucket . '.s3.amazonaws.com/' . $actual_image_name;
                //  echo $s3file;die();
                // echo "<img src='$s3file'/>";
                //  echo 'S3 File URL:' . $s3file;
                //    $vname = pathinfo($name, PATHINFO_FILENAME);

                $duration = 6;

                $videomodel = new Videos();
                $videomodel->model_id = $empid;
                $videomodel->video_id = $actual_image_name;
                $videomodel->name = $vname;
                $videomodel->duration = (int) $duration;
                $videomodel->model_type = 'employer';
                $videomodel->category_id = 1;
                $videomodel->is_processed = 1;
                $videomodel->state = 1;
                $videomodel->status = "active";
                $videomodel->created_at = $now->format('Y-m-d H:i:s');
                if ($videomodel->save()) {

                    Employers::model()->updateByPk($empmodel->id, array('video_id' => $videomodel->id));

                    $sec = 0;

                    $fullpath = 'https://' . $bucket . '.s3.amazonaws.com/' . $actual_image_name;

                    $cmd = "ffmpeg -i $fullpath 2>&1";
                    $ffmpeg = shell_exec($cmd);

                    $search = "/Duration: (.*?)\./";
                    preg_match($search, $ffmpeg, $matches);

                    if (isset($matches[1])) {
                        $data['duration'] = $matches[1];
                        $time_sec = explode(':', $data['duration']);
                        $sec = ($time_sec['0'] * 3600) + ($time_sec['1'] * 60) + $time_sec['2'];
                    }

                    Videos::model()->updateByPk($videomodel->id, array('duration' => $sec));

                    $mediamodel = new Media();
                    $mediamodel->model_id = $videomodel->id;
                    $mediamodel->disk = "media";
                    $mediamodel->model_type = $videomodel->model_type;
                    $mediamodel->collection_name = "video";
                    $mediamodel->name = $vname;
                    $mediamodel->file_name = $vname . ".jpg";
                    $mediamodel->created_at = $now->format('Y-m-d H:i:s');
                    $mediamodel->size = (int) $duration;

                    if ($mediamodel->save()) {
                        //$filename = $vname;

                        Media::model()->updateByPk($mediamodel->id, array('model_id' => $videomodel->id, 'size' => $sec));

                        $iname = $vname . ".jpg";
                        $mediafolder = "images/media/" . $mediamodel->id;

                        if (!file_exists($mediafolder)) {
                            mkdir($mediafolder, 0777, true);
                        }

                        $imgfullpath = "images/media/" . $mediamodel->id . "/" . $iname;
                        $imgprefix = "images/media/" . $mediamodel->id;

                        $ffurl = "ffmpeg -i $fullpath -ss 00:00:2 -vframes 1 $imgfullpath";

                        $process = exec($ffurl);

                        $jlwidth = 350;
                        $jlheight = 190;
                        $jldestpath = $imgprefix . "/conversions/profile.jpg";

                        //create search
                        $searchwidth = 167;
                        $searchheight = 167;
                        $searchdestpath = $imgprefix . "/conversions/search.jpg";

                        //create thumb
                        $thumbwidth = 126;
                        $thumbheight = 126;
                        $thumbdestpath = $imgprefix . "/conversions/thumb.jpg";
                        $image_type == "image/jpg";

                        $image = imagecreatefromjpeg($imgfullpath);

                        $this->resize_image($image, $image_type, $imgfullpath, $jldestpath, $jlwidth, $jlheight); //create joblisting
                        $this->resize_image($image, $image_type, $imgfullpath, $searchdestpath, $searchwidth, $searchheight); //create search
                        $this->resize_image($image, $image_type, $imgfullpath, $thumbdestpath, $thumbwidth, $thumbheight); //create thumb

                        $this->_sendResponsewithMessage(200, sprintf("The video has been uploaded"));
                    }
                }
            } catch (S3Exception $e) {
                // Catch an S3 specific exception.
                echo $e->getMessage();
            }
        }
    }
    
    public function actionapplycoupon()
    {
        $answer = FALSE;
        $coupon_id  = $_POST['coupon_id'];
        
        require Yii::app()->basePath . '/extensions/stripe/init.php';
        Stripe::setApiKey(Yii::app()->params['SECRET_Key']);
        
        
        // needs if coupon_id is not blank
        try {
            $coupon = Coupon::retrieve($coupon_id);

            $result = array();
        
            if ($coupon->valid) {
                $result = array("status" => TRUE ,"discount"=>($coupon->percent_off));
                $this->_sendResponse(200, $result);
//                echo json_encode(array("status" => TRUE ,"discount"=>($coupon->percent_off)));
            }else{
                $result = array("status" => FALSE ,"discount"=> 0);
                $this->_sendResponse(500, $result);
//                echo json_encode(array("status" => FALSE ,"discount"=> 0));
            }
            //$couponid = $coupon->id;
        } catch (\Stripe\Error\InvalidRequest $e) {
            // $answer is already set to false
            $result = array("status" => FALSE ,"discount" => 0);
            $this->_sendResponse(404, $result);
           // echo json_encode((array("status" => FALSE ,"discount" => 0)));
            // echo $answer = FALSE;
        }
        
//        if (empty($result)) {
//            $this->_sendResponse(404, array());
//        } else {
//            $this->_sendResponse(200, $result);
//        }
        
        //            if ($coupon->valid) {
        //                $answer = "11";
        //            }
        //            echo $answer;  //this sends 0 or 1
    }

    /* list of videos */

    public function actiongetcompanyprofilevideos() {
        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());
        $this->apikeyAuth();

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $id = $_GET['id'];
        $model = Employers::model()->findByPk($id);
        if ($model == null) {
            $this->_sendResponse(500, array());
        }

        if ($model->video_id <= 0)
            $this->_sendResponse(404, array());

        $vmodel = Videos::model()->findByPk($model->video_id);
       
        $bucket = Yii::app()->params['AWS_BUCKET'];

        $fullpath = 'https://' . $bucket . '.s3.amazonaws.com/' . $vmodel->video_id;

        if (empty($fullpath)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $fullpath);
        }
    }

    public function actiondeleteCompanyProfile() {
        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        $this->apikeyAuth();

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $id = $_GET['id'];
        $emodel = Employers::model()->findByPk($id);
        if ($emodel == null) {
            $this->_sendResponse(500, array());
        }

        $now = new DateTime();
        Employers::model()->updateByPk($id, array('video_id' => null, 'updated_at' => $now->format('Y-m-d H:i:s')));
        $this->_sendResponse(200, sprintf("Deleted successfully"));
    }

    public function notifycreateemployer($email) {
        $body = "register emplyer successfully";
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->Host = 'cvvid.com';
        $mail->Username = 'vinoth25pms@gmail.com';
        $mail->Password = '9865295902';
        $mail->SMTPSecure = 'ssl';
        $mail->SetFrom('mail@cvvid.com', 'cvvid');
        $mail->Subject = 'Your successfully registered employer for cvvid.com';
        $mail->MsgHTML($body);
        $mail->AddAddress($email);
        // $mail->AddAddress('vinoth25991@gmail.com');
        $mail->AddReplyTo('cvvid@gmail.com', 'Reply to cvvid');
        $mail->Send();
    }

    // public function actionfetchemployerprofiledetails() {
    //     if (!isset($_GET['id']) || $_GET['id'] == 0)
    //         $this->_sendResponse(404, array());
    //      $id = $_GET['id'];
    //      $employermodel = EmployerUsers::model()->findByAttributes(array('user_id'=>$id));
    //      $cri = new CDbCriteria();
    //      $cri->alias = 'emp';
    //      $cri->select = 'us.forenames,us.surname,us.email,us.current_job,us.location,ad.county,ad.country';
    //      $cri->join = 'left outer join cv16_users us on us.id = emp.user_id '
    //              . 'left outer join cv16_addresses ad on ad.model_id = us.id '
    //              . 'left outer join cv16_profiles prof on prof.owner_id = emp.user_id';
    //      $cri->condition = 'emp.user_id ='.$id;
    //      $users = $employermodel->getCommandBuilder()->createFindCommand($employermodel->tableSchema, $cri)->queryAll();
    //      $this->_sendResponse(200, $users);
    // }

    /* fetch employer details */
    public function actionfetchemployerprofiledetails() {

        $this->apikeyAuth();


         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $id = $_GET['id'];
        $employermodel = EmployerUsers::model()->findByAttributes(array('user_id' => $id));
        if ($employermodel == NULL) {
            $this->_sendResponse(404, array());
        }
        $cri = new CDbCriteria();
        $cri->alias = 'emp';
        $cri->select = 'e.id,e.trial_ends_at as trails,e.subscription_ends_at,us.forenames,us.surname,us.email,(SELECT count(*) FROM cv16_profile_likes WHERE profile_id = prof.id) as num_likes,CONCAT("https://cvvid.com/images/media/",prof.photo_id,"/",m.file_name) as photo,CONCAT("https://cvvid.com/images/defaultprofile.jpg") as photodefault,CONCAT("https://cvvid.com/images/media/",prof.cover_id,"/conversions/cover.jpg") as cover,CONCAT("https://cvvid.com/images/CoverPlaceholder.jpg") as coverdefault,us.current_job,us.location,ad.county,ad.country,'
                . 'e.user_id, e.name, e.email, e.body, e.location as emp_loc, e.location_lat, e.location_lng, e.website, e.tel,'
                . 'ad.street_number, ad.address, ad.town, ad.postcode, ad.county, ad.country, ad.location, ad.latitude, ad.longitude,'
                . 'prof.id as profile_id,prof.slug,prof.industry_id,e.stripe_active, prof.photo_id, prof.cover_id, prof.visibility, prof.published, prof.hired, prof.num_likes, prof.num_views';
        $cri->join = 'left outer join cv16_employers e on e.user_id = emp.user_id left outer join cv16_users us on us.id = emp.user_id left outer join cv16_addresses ad on ad.model_id = us.id and ad.model_type like "%employer%" left outer join cv16_profiles prof on prof.owner_id = e.id and prof.type like "%employer%"  left outer join cv16_media m on m.id = prof.photo_id';
        $cri->condition = 'emp.user_id =' . $id;

    //     echo var_dump($cri);die();

        $users = $employermodel->getCommandBuilder()->createFindCommand($employermodel->tableSchema, $cri)->queryAll();
        foreach ($users as $user) {
            $employer_id = $user['id'];
            $location = $user['emp_loc'];
            $stripe_active = $user['stripe_active'];
            $trails = $user['trails'];
            $subscription_ends_at = $user['subscription_ends_at'];
        }
    
        $account_type = "";
        if(Membership::subscribed($stripe_active, $trails, $subscription_ends_at)) {
            $account_type = "Premium Account";                 
        }
        else{
            $account_type = "Basic Account"; 
        }

        $upgrade_status = "0";


        if (Membership::onTrail($trails) || $stripe_active) {
            $upgrade_status = "1";
        }
        
        $cri = new CDbCriteria();
        $cri->condition = "model_id =".$employer_id." and model_type like '%employer%' and deleted_at is null";
        $addressmodel = Addresses::model()->find($cri);
        if($addressmodel != null)
        {
            $address = "";
            if($addressmodel->address != "")
                $address = $addressmodel->address.",";
            if($addressmodel->town != "")
                $address .= $addressmodel->town.",";
            if($addressmodel->postcode != "")
                $address .= $addressmodel->postcode.",";
            if($addressmodel->country != "")
                $address .= $addressmodel->country;
        }else{
            $address = $location;
        }

        $use = array("upgrade" => $upgrade_status, "account_type" => $account_type,"location" => $address);

        $arr3 = array_merge($users[0], $use);
        // echo var_dump($arr3);die();
        // $users['data'] = 
//         $upgrade_status = "0";
//         if(Membership::onTrail($trail) || $usermodel->stripe_active)
//      {
//             $upgrade_status = "1";
//         }

        if (empty($arr3)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, array($arr3));
        }
    }

    /* fetch employer details */


    /* update employer details */

    public function actionoldupdateemployer() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        $this->apikeyAuth();


        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }
        $id = $_GET['id'];

        $usermodel = Users::model()->findByPk($id);
        $empusermodel = EmployerUsers::model()->findByAttributes(array('user_id' => $id));
        $employermodel = Employers::model()->findByAttributes(array('user_id' => $id));
        $userid = $employermodel->user_id;
        $cri = new CDbCriteria();
        $cri->condition = "model_id = ".$usermodel->id." and model_type like '%employer%' and deleted_at is null";
        $addressmodel = Addresses::model()->find($cri);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $userid));
        $now = new DateTime();
        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                $usermodel->$var = $value;
            } else if ($employermodel->hasAttribute($var)) {
                $employermodel->$var = $value;
            } elseif ($addressmodel->hasAttribute($var)) {
                $addressmodel->$var = $value;
            } elseif ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else {
                $this->_sendResponse(400, array());
            }
        }
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $usermodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($usermodel->save()) {
            $empusermodel = EmployerUsers::model()->findByAttributes(array('user_id' => $id));
            $employermodel->name = $usermodel->forenames;
            $employermodel->email = $usermodel->email;
            $employermodel->location = $usermodel->location;
            if ($employermodel->save()) {
                $addressmodel = Addresses::model()->findByAttributes(array('model_id' => $usermodel->id, 'model_type' => 'employer'));
                $addressmodel->location = $addressmodel['location'];
                if ($addressmodel->save()) {
                    $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $userid));
                    $profilemodel->slug = $usermodel->forenames . '' . $usermodel->surname;
                    if ($profilemodel->save()) {
                        $cri = new CDbCriteria();
                        $cri->select = '*';
                        $cri->condition = "id = " . $id;
                        $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                        $this->_sendResponse(200, $users);
                    } else {
                        echo var_dump($profilemodel->getErrors());
                    }
                } else {
                    echo var_dump($addressmodel->getErrors());
                }
            } else {
                echo var_dump($employermodel->getErrors());
            }
        } else {
            echo var_dump($usermodel->getErrors());
        }
    }

    public function actionupdateemployer() {
        // if(!$this->_checkAuth())
        //  $this->_sendResponse(405, array());

        $this->apikeyAuth();
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $id = $_GET['id'];

        $usermodel = Users::model()->findByPk($id);
        if($usermodel == null){$this->_sendresponse(404,array());}
        $empusermodel = EmployerUsers::model()->findByAttributes(array('user_id' => $id));
        $employermodel = Employers::model()->findByAttributes(array('user_id' => $id));
        $userid = $employermodel->user_id;
        $cri = new CDbCriteria();
        $cri->condition = "model_id = " . $usermodel->id . " and model_type like '%employer%' and deleted_at is null";
        $addressmodel = Addresses::model()->find($cri);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $userid));
        $now = new DateTime();

        $usertable = Yii::app()->db->schema->getTable(Users::model()->tableName());
        $employerstable = Yii::app()->db->schema->getTable(Employers::model()->tableName());
        $employerusersstable = Yii::app()->db->schema->getTable(EmployerUsers::model()->tableName());
        $profiletable = Yii::app()->db->schema->getTable(Profiles::model()->tableName());
        $addresstable = Yii::app()->db->schema->getTable(Addresses::model()->tableName());



        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                $usermodel->$var = $value;
            } else if ($employermodel->hasAttribute($var)) {
                $employermodel->$var = $value;
            } elseif ($addressmodel->hasAttribute($var)) {
                $addressmodel->$var = $value;
            } elseif ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else if (!isset($usertable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($employerstable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($employerusersstable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($profiletable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($addresstable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else {
                $this->_sendResponse(400, array());
            }
        }


        
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $usermodel->updated_at = $now->format('Y-m-d H:i:s');
       
        $profilemodel->slug = $this->getValidatedSlug($_POST['slug']);

        $valid = $usermodel->validate();
        $valid = $addressmodel->validate() && $valid;
        $valid = $employermodel->validate() && $valid;
         $valid = $profilemodel->validate() && $valid;

        if ($valid) {
            $usermodel->save();
            $employermodel->email = $usermodel->email;
            $employermodel->location = $usermodel->location;
            $employermodel->save();
            $addressmodel = Addresses::model()->findByAttributes(array('model_id' => $usermodel->id, 'model_type' => 'employer'));
            $addressmodel->location = $addressmodel['location'];
            $addressmodel->save();          
            if ($profilemodel->save()) {
                
                $cri = new CDbCriteria();
                $cri->select = '*';
                $cri->condition = "id = " . $id;
                $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                $this->_sendResponse(200, $users);
            }
            // else {
            //   echo var_dump($profilemodel->getErrors()); die();
            // }
        } else {

            // $errors[] = $usermodel->errors;
            // $errors[] = $profilemodel->errors;
            // $errors[] = $addressmodel->errors;
            $usrerr = array();

            $usercolumns = Users::model()->getAttributes();
            $employercolumns = Employers::model()->getAttributes();
            $employeruserscolumns = EmployerUsers::model()->getAttributes();
            $addresscolumns = Addresses::model()->getAttributes();
            $profilecolumns = Profiles::model()->getAttributes();

            foreach ($usercolumns as $key => $val) {
                if ($usermodel->getError($key) != "") {
                    $usrerr[$key] = $usermodel->getError($key);
                }
            }
            foreach ($addresscolumns as $key => $val) {
                if ($addressmodel->getError($key) != null)
                    $usrerr[$key] = $addressmodel->getError($key);
            }
            foreach ($profilecolumns as $key => $val) {
                if ($profilemodel->getError($key) != null)
                    $usrerr[$key] = $profilemodel->getError($key);
            }
            foreach ($employercolumns as $key => $val) {
                if ($employermodel->getError($key) != null)
                    $usrerr[$key] = $employermodel->getError($key);
            }

            $this->_sendResponse(512, $usrerr);
        }
    }


/* update employer details */

    public function actionaddnewjob() {
        

         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $employer = Employers::model()->findByPk($id);
        $userid = $employer->user_id;

        $jobskillmodel = new JobSkill();
        $jobmodel = new Jobs();
        $now = new DateTime();
        foreach ($_POST as $var => $value) {
            if ($jobmodel->hasAttribute($var)) {
                $jobmodel->$var = $value;
            } elseif ($var == 'skills') {
               // $skills = $value;
            } else {
                $this->_sendResponse(400, array());
            }
        }
        $jobmodel->owner_id = $id;
        $jobmodel->owner_type = 'employer';
        $jobmodel->user_id = $userid;
        $d = str_replace('/', '-', $_POST["end_date"]);
        $endformat = date('Y-m-d', strtotime($d));
        if(isset($_POST['competitive_salary']) && $_POST['competitive_salary'] == "1")
        {
            $jobmodel->competitive_salary = 1;
            $jobmodel->salary_min = 0;
            $jobmodel->salary_max = 0;
        }
        $jobmodel->end_date = $endformat;
        $jobmodel->created_at = $now->format('Y-m-d H:i:s');
        if ($jobmodel->save()) {
            
            $skills = $_POST['skills'];

//echo var_dump($skills); die();

          $skill_arr = explode(',', $skills);
            
            foreach ($skill_arr as $value) {
                
                //$skillsmodel = Skills::model()->findByAttributes(array('name' => $value));
                
                $cri = new CDbCriteria();
                $cri->condition = "name='".$value."'";  
                $skillsmodel = Skills::model()->find($cri);

                $jobskillmodels = new JobSkill();
                $jobskillmodels->job_id = $jobmodel->id;
                $jobskillmodels->skill_id = $skillsmodel->id;
                $jobskillmodels->save();
            }
            $cri = new CDbCriteria();
            $cri->select = 'user_id, industry_id, company_name, title, salary_type, salary_min, salary_max, end_date, skill_category_id, location, location_lat, location_lng, type, description, additional';
            $cri->condition = 'id=' . $jobmodel->id;
            $users = $jobmodel->getCommandBuilder()->createFindCommand($jobmodel->tableSchema, $cri)->queryAll();
            if (!empty($users)) {
                $this->_sendResponse(200, $users);
            } else {
                $this->_sendResponse(200, array());
            }
        }
    }

    public function actioncreateindustry() {
        $this->apikeyAuth();
        // if(!$this->_checkAuth())
        //  $this->_sendResponse(405, array());
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $industrymodel = new Industries();
        $now = new DateTime();
        $industrytable = Yii::app()->db->schema->getTable(Industries::model()->tableName());
        foreach ($_POST as $var => $value) {
            if ($industrymodel->hasAttribute($var)) {
                $industrymodel->$var = $value;
            } else if (!isset($industrytable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else {
                $this->_sendResponse(400, array());
            }
        }
        $industrymodel->created_at = $now->format('Y-m-d H:i:s');
        $valid = $industrymodel->validate();
        if ($valid) {
            if ($industrymodel->save()) {
                $cri = new CDbCriteria();
                $cri->select = 'id,name';
                $cri->condition = 'id=' . $industrymodel->id;
                $industry = $industrymodel->getCommandBuilder()->createFindCommand($industrymodel->tableSchema, $cri)->queryAll();
                $this->_sendResponse(200, $industry);
            }
        } else {
            $usrerr = array();
            $industrycolumns = Industries::model()->getAttributes();
            foreach ($industrycolumns as $key => $val) {
                if ($industrymodel->getError($key) != "") {
                    $usrerr[$key] = $industrymodel->getError($key);
                }
            }
            $this->_sendResponse(512, $usrerr);
        }
    }

    public function actiongetindustry() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());
  //      $this->apikeyAuth();

        $industrymodel = Industries::model();
        $cri = new CDbCriteria();
        $cri->select = 'id,name';
        $industry = $industrymodel->getCommandBuilder()->createFindCommand($industrymodel->tableSchema, $cri)->queryAll();
        if (!empty($industry)) {
            $this->_sendResponse(200, $industry);
        } else {
            $this->_sendResponse(507, array());
        }
    }

     public function actioncreateskillcategory() {

        // if(!$this->_checkAuth())
        //  $this->_sendResponse(405, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
            
        $skillcategory = new SkillCategories();
        $now = new DateTime();
        $skillcategorytable = Yii::app()->db->schema->getTable(SkillCategories::model()->tableName());
        foreach ($_POST as $var => $value) {

            if ($skillcategory->hasAttribute($var)) {
                
                $skillcategory->$var = $value;
            } elseif (!isset($skillcategorytable->columns[$var])) {
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else {
                $this->_sendResponse(400, array());
            }
        }
        $skillcategory->created_at = $now->format('Y-m-d H:i:s');
        $valid = $skillcategory->validate();
        if ($valid) {
            if ($skillcategory->save()) {
                $cri = new CDbCriteria();
                $cri->select = 'id,name';
                $cri->condition = 'id=' . $skillcategory->id;
                $skillcategory = $skillcategory->getCommandBuilder()->createFindCommand($skillcategory->tableSchema, $cri)->queryAll();
                $this->_sendResponse(200, $skillcategory);
            }
        } else {
            $usrerr = array();
            $skillcategorycolumns = SkillCategories::model()->getAttributes();
            foreach ($skillcategorycolumns as $key => $val) {
                if ($skillcategory->getError($key) != "") {
                    $usrerr[$key] = $skillcategory->getError($key);
                }
            }
            $this->_sendResponse(512, $usrerr);
        }
    }

    public function actionNewcareerskill() {
        // $this->apikeyAuth();
        
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $skillsmodel = new Skills();
        $now = new DateTime();
        $newskiltable= Yii::app()->db->schema->getTable(Skills::model()->tableName());
        
        foreach ($_POST as $var => $value) {
            if ($skillsmodel->hasAttribute($var)) {
                $skillsmodel->$var = $value;
            } elseif (!isset($newskiltable->columns[$var])) {
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else {
                $this->_sendResponse(400, array());
            }
        }
        
        $skillsmodel->skill_category_id = $_POST['skill_category_id'];
        $skillsmodel->name = $_POST['name'];
        $skillsmodel->created_at = $now->format('Y-m-d H:i:s');
        $valid = $skillsmodel->validate();
        if($valid) {
        if ($skillsmodel->save()) {
            $cri = new CDbCriteria();
            $cri->select = 'id,name,skill_category_id';
            $cri->condition = 'id=' . $skillsmodel->id;
            $skills = $skillsmodel->getCommandBuilder()->createFindCommand($skillsmodel->tableSchema, $cri)->queryAll();
                $this->_sendResponse(200, $skills);
        } 
        }else {
              $usrerr = array();
            $newcareerskillcolumns = SkillCategories::model()->getAttributes();
            foreach ($newcareerskillcolumns as $key => $val) {
                if ($skillsmodel->getError($key) != "") {
                    $usrerr[$key] = $skillsmodel->getError($key);
                }
            }
            $this->_sendResponse(512, $usrerr);
        } 
    }

    public function actiongetcareerskill() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        $this->apikeyAuth();


        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        //   $skillcategorymodel = SkillCategories::model()->findByPk($id);
        $id = $_GET['id'];
        $skillsmodel = Skills::model()->findByAttributes(array('skill_category_id' => $id));
        $cri = new CDbCriteria();
        $cri->alias = 'sk';
        $cri->select = 'sk.id,sk.name,sk.skill_category_id';
        $cri->join = 'left outer join cv16_skill_categories skc on skc.id = sk.skill_category_id';
        $cri->condition = 'sk.skill_category_id=' . $id;
        $skills = $skillsmodel->getCommandBuilder()->createFindCommand($skillsmodel->tableSchema, $cri)->queryAll();
        if (!empty($skills)) {
            $this->_sendResponse(200, $skills);
        } else {
            $this->_sendResponse(500, array());
        }
    }

    /* employer->addjob->getparticularid->to show all employer */

    public function actiongetjob() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        $this->apikeyAuth();

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];
        $jobsmodel = Jobs::model()->findByPk($id);
        if ($jobsmodel == NULL) {
            $this->_sendResponse(500, array());
        }
        $cri = new CDbCriteria();
        $cri->alias = 'j';
        $cri->select = 'distinct j.id, j.owner_id, j.owner_type, j.user_id, j.industry_id, j.company_name,'
                . ' j.title, j.salary_type, j.salary_min, j.salary_max, j.end_date, j.skill_category_id, '
                . 'j.location, j.location_lat, j.location_lng, j.type, j.description, j.additional, j.vacancies, j.created_at,'
                . ' j.status, j.closed_date, j.successful_applicant_id, j.num_views,em.name,j.competitive_salary';
        $cri->join = "left outer join  cv16_industries i on i.id = j.industry_id
                      left outer join cv16_employers em on em.id = j.owner_id and j.owner_type like '%employer%'";
        $cri->condition = 'j.id=' . $id;
        
//        $jobmodel = Jobs::model()->findAll($cri);
             
//        $totarr = array();
//        
//        $jobarr = array();
//        
//        foreach ($jobmodel as $job)
//        {
//           
//            $jobarr[] = $job;
//            
//        }
//        $totarr[] = array_merge($jobarr,array('successful_applicant'=>null));
//        
        
        $jobs = $jobsmodel->getCommandBuilder()->createFindCommand($jobsmodel->tableSchema, $cri)->queryAll();
        if (!empty($jobs)) {
            $this->_sendResponse(200, array(array_merge($jobs[0], array('posted_job'=>$this->timeAgo($jobs[0]['created_at'])))));
        } else {
            $this->_sendResponse(507, array());
        }
    }

    public function actionfetchjobemployer() {

        $this->apikeyAuth();

         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];
        $jobsmodel = Jobs::model()->findByPk($id);
        $cri = new CDbCriteria();
        $cri->select = 'id, owner_id, owner_type, user_id, industry_id, company_name,'
                . ' title, salary_type, salary_min, salary_max, end_date, skill_category_id, '
                . 'location, location_lat, location_lng, type, description, additional,'
                . ' created_at, updated_at, status, closed_date, successful_applicant_id, num_views';
        $cri->condition = 'id=' . $id;
        $jobs = $jobsmodel->getCommandBuilder()->createFindCommand($jobsmodel->tableSchema, $cri)->queryAll();
        if (!empty($jobs)) {
            $this->_sendResponse(200, $jobs);
        } else {
            $this->_sendResponse(500, array());
        }
    }

    /* public or private */

    public function actionemployerupdateprivatepublic() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());
        $this->apikeyAuth();

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }
        $employer = Employers::model()->findByPk($id);
        $ownerid = $employer->user_id;
        $profile = Profiles::model()->findByAttributes(array('owner_id' => $ownerid, 'owner_type' => 'employer'));
        if ($profile->visibility == 0) {
            $profile->visibility = '1';
        } else {
            $profile->visibility = '0';
        }
        if ($profile->save()) {
            $this->_sendResponse(200, array());
        } else {
            $this->_sendResponse(500, array());
        }
    }

    /* active or inactive */

    public function actionemployerchangestatus() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());
        $this->apikeyAuth();

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }
        $employer = Employers::model()->findByPk($id);
        $ownerid = $employer->user_id;
        $usermodel = Users::model()->findByAttributes(array('id' => $ownerid, 'type' => 'employer'));
        if ($usermodel->status == 'active') {
            $usermodel->status = 'inactive';
        } else {
            $usermodel->status = 'active';
        }
        if ($usermodel->save()) {
            $this->_sendResponse(200, array());
        } else {
            $this->_sendResponse(500, array());
        }
    }

    /* Employer View button */

    public function actionemployerview() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        $this->apikeyAuth();


        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];
        $count = 0;
        $viewemployer = array();

        $employer = Employers::model()->findByPk($id);
        $cri = new CDbCriteria();
        $cri->alias = 'emp';
        $cri->select = 'emp.name,emp.email,emp.location,emp.tel,prof.slug';
        $cri->join = 'inner join cv16_users user on user.id = emp.user_id  '
                . 'inner join cv16_profiles prof on prof.owner_id = user.id';
        $cri->condition = 'emp.id=' . $id;
        $employer = $employer->getCommandBuilder()->createFindCommand($employer->tableSchema, $cri)->queryAll();

        $job = Jobs::model()->findByAttributes(array('owner_id' => $id));
        $cri = new CDbCriteria();
        $cri->select = 'company_name,title,salary_type,salary_min,salary_max,end_date,location,salary_type';
        $cri->condition = 'owner_id=' . $id;
        $job = $job->getCommandBuilder()->createFindCommand($job->tableSchema, $cri)->queryAll();


        $count1 = count($employer);
        for ($i = 0; $i < $count1; $i++) {
            $viewemployer[$i] = $employer[$i];
        }

        $count2 = count($job);
        $count2 = $count2 + $count1;
        $j = 0;
        for ($i = $count1; $i < $count2; $i++) {
            $viewemployer[$i] = $job[$j];
            $j++;
        }

        if (!empty($viewemployer)) {
            $this->_sendResponse(200, $viewemployer);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /* Employer View button */

    /* User */
    /* Add User Create */

    public function actionoldadduser() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        $this->apikeyAuth();


        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $empolyer = Employers::model()->findByPk($id);
        $employerid = $empolyer->id;

        $usermodel = new Users();
        $empusermodel = new EmployerUsers;
        $now = new DateTime();

        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                $usermodel->$var = $value;
            } elseif ($empusermodel->hasAttribute($var)) {
                $empusermodel->$var = $value;
            } else {
                $this->_sendResponse(400, array());
            }
        }
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $usermodel->type = 'employer';
        if ($usermodel->save()) {
            $empusermodel->employer_id = $employerid;
            $empusermodel->user_id = $usermodel->id;
            $empusermodel->role = 'employer';
            $empusermodel->created_at = $now->format('Y-m-d H:i:s');
            if ($empusermodel->save()) {
                $cri = new CDbCriteria();
                $cri->select = 'id, type, forenames, surname, email, password, dob, status, is_premium, tel, mobile, current_job, location, location_lat, location_lng';
                $cri->condition = 'id =' . $usermodel->id;
                $user = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                $this->_sendResponse(200, $user);
            } else {
                $this->_sendResponse(507, array());
            }
        } else {
            $this->_sendResponse(507, array());
        }
    }


    public function actionadduser() {
        // if(!$this->_checkAuth())
        //  $this->_sendResponse(405, array());

        $this->apikeyAuth();


        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $empolyer = Employers::model()->findByPk($id);
        $employerid = $empolyer->id;

        $usermodel = new Users();
        $empusermodel = new EmployerUsers;
        $now = new DateTime();

        $usertable = Yii::app()->db->schema->getTable(Users::model()->tableName());
        $empusertable = Yii::app()->db->schema->getTable(EmployerUsers::model()->tableName());


        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                $usermodel->$var = $value;
            } elseif ($empusermodel->hasAttribute($var)) {
                $empusermodel->$var = $value;
            } elseif (!isset($usertable->columns[$var])) {
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else {
                $this->_sendResponse(400, array());
            }
        }
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $usermodel->type = 'employer';
        $valid = $usermodel->validate();
        
        if ($valid) {
            if ($usermodel->save()) {
                $empusermodel->employer_id = $employerid;
                $empusermodel->user_id = $usermodel->id;
                $empusermodel->role = 'employer';
                $empusermodel->created_at = $now->format('Y-m-d H:i:s');
                if ($empusermodel->save()) {
                    $cri = new CDbCriteria();
                    $cri->select = 'id, type, forenames, surname, email, password, dob, status, is_premium, tel, mobile, current_job, location, location_lat, location_lng';
                    $cri->condition = 'id =' . $usermodel->id;
                    $user = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                    $this->_sendResponse(200, $user);
                }
            }
        } else {
            $usrerr = array();
            $usercolumns = Users::model()->getAttributes();
            $employercolumns = EmployerUsers::model()->getAttributes();          

            foreach ($usercolumns as $key => $val) {
                if ($usermodel->getError($key) != "") {
                    $usrerr[$key] = $usermodel->getError($key);
                }
            }           
            $this->_sendResponse(512, $usrerr);
        }
    }
    /* Add User Create */
    /* get all */

    public function actiongetalluser() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        $this->apikeyAuth();


        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());

        $id = $_GET['id'];
        $count = 0;
        $viewemployeuser = array();

        $emodel = Employers::model()->findByPk($id);
        $empid = $emodel->id;

        $empuser = EmployerUsers::model()->findByAttributes(array("employer_id" => $empid, "role" => 'employer'));
    if($empuser == NULL){
            $this->_sendResponse(500,array());
        }
        $cri = new CDbCriteria();
        $cri->alias = 'empuser';
        $cri->select = "user.id,user.forenames,user.status, user.surname, user.email,user.created_at,(select count(*) from cv16_jobs where user_id ='$emodel->user_id') as postedjobs";
        $cri->join = 'left outer join cv16_users user on user.id = empuser.user_id';
        $cri->condition = "empuser.employer_id ='" . $id . "' and empuser.role ='employer' ";
        $empuser = $empuser->getCommandBuilder()->createFindCommand($empuser->tableSchema, $cri)->queryAll();

        $count1 = count($empuser);
        for ($i = 0; $i < $count1; $i++) {
            $viewemployeuser[$i] = $empuser[$i];
        }
        if (!empty($viewemployeuser)) {
            $this->_sendResponse(200, $viewemployeuser);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /* get all */

    /* fetch user */

    public function actionfetchuser() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        $this->apikeyAuth();


        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());

        $id = $_GET['id'];
        $usermodel = Users::model()->findByPk($id);
        $cri = new CDbCriteria();
        $cri->select = 'forenames, surname, email';
        $cri->condition = 'id=' . $id;
        $user = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
        if (!empty($user)) {
            $this->_sendResponse(200, $user);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /* fetch user */
    /* update user */

    public function actionoldupdateuser() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());
        $this->apikeyAuth();


        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        // else {
        //     $_POST = $put_vars;
        // }

        $usermodel = Users::model()->findByPk($id);

        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                $usermodel->$var = $value;
            } else if ($var == "password") {
                if ($value == "")
                    $usermodel->password = $usermodel->password;
                else
                    $usermodel->password = password_hash($value, 1, ['cost' => 10]);
            }
            else {
                $this->_sendResponse(400, array());
            }
        }

        if ($usermodel->save()) {
            $cri = new CDbCriteria();
            $cri->select = 'id,forenames,surname,email';
            $cri->condition = 'id=' . $id;
            $user = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
            $this->_sendResponse(200, $user);
        } else {
            echo var_dump($usermodel->getErrors());
            $this->_sendResponse(507, array());
        }
    }


     public function actionupdateuser() {
        // if(!$this->_checkAuth())
        //  $this->_sendResponse(405, array());
        $this->apikeyAuth();
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $usermodel = Users::model()->findByPk($id);
        $usertable = Yii::app()->db->schema->getTable(Users::model()->tableName());
        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                $usermodel->$var = $value;
            } else if ($var == "password") {
                if ($value == "")
                    $usermodel->password = $usermodel->password;
                else
                    $usermodel->password = password_hash($value, 1, ['cost' => 10]);
            } elseif (!isset($usertable->columns[$var])) {
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else {
                $this->_sendResponse(400, array());
            }
        }
        $valid = $usermodel->validate();
        if ($valid) {
            if ($usermodel->save()) {
                $cri = new CDbCriteria();
                $cri->select = 'id,forenames,surname,email';
                $cri->condition = 'id=' . $id;
                $user = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                $this->_sendResponse(200, $user);
            }
        } else {
            $usercolumns = Users::model()->getAttributes();
            foreach ($usercolumns as $key => $val) {
                if ($usermodel->getError($key) != "") {
                    $usrerr[$key] = $usermodel->getError($key);
                }
            }
            $this->_sendResponse(512, $usrerr);
        }
    }


    /* update user */

    /* delete user */

    public function actiondeleteuser() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());
        $this->apikeyAuth();


        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];
        $employeruser = EmployerUsers::model()->findByAttributes(array('user_id' => $id));
        $cri = new CDbCriteria();
        $cri->condition = 'user_id=' . $id;
        EmployerUsers::model()->deleteAll($cri);

        $usermodel = Users::model()->findByPk($id);
        $now = new DateTime();
        $usermodel->deleted_at = $now->format('Y-m-d H:i:s');
        $usermodel->save();
        $this->_sendResponse(200, $usermodel);
    }

    /* delete user */

    /* activate user */

    public function actionactivateuser() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());
        $this->apikeyAuth();

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];
        $cri = new CDbCriteria();
        $cri->condition = "id =" . $id;
        Users::model()->updateAll(array('status' => 'active'), $cri);
        $usermodel = Users::model()->findByPk($id);
        $this->_sendResponse(200, sprintf("successfully Activated"));
        //Yii::app()->user->setFlash('success', 'Successfully Activated');
    }

    /* block user */

    public function actionblockuser() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());
        $this->apikeyAuth();

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];

        $cri = new CDbCriteria();
        $cri->condition = "id =" . $id;
        Users::model()->updateAll(array('status' => 'blocked'), $cri);

        $usermodel = Users::model()->findByPk($id);

        $this->_sendResponse(200, sprintf("Blocked successfully"));
        //Yii::app()->user->setFlash('success', 'Successfully Blocked');
    }

    /* User */

    /* Employer inbox */

    public function actionsendmessage() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());
        $this->apikeyAuth();

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());

        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $now = new DateTime();
        $conmodel = new Conversations();
        $conmodel->subject = $_POST['subject'];
        $conmodel->created_at = $now->format('Y-m-d H:i:s');
        $conmodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($conmodel->save()) {
            $usermodel = Users::model()->findByPk($id);
            $conmsgmodel = new ConversationMessages();
            $conmsgmodel->conversation_id = $conmodel->id;
            $conmsgmodel->user_id = $usermodel->id;
            $conmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
            $conmsgmodel->message = $_POST['message'];
            $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
            $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($conmsgmodel->save()) {
                $conmemmodel_sender = new ConversationMembers();
                $conmemmodel_sender->conversation_id = $conmodel->id;
                $conmemmodel_sender->user_id = $usermodel->id;
                $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
                $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
                $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                if ($conmemmodel_sender->save()) {
                    $usermodel1 = Users::model()->findByPk($_POST['user_id']);
                    $conmemmodel_rec = new ConversationMembers();
                    $conmemmodel_rec->conversation_id = $conmodel->id;
                    $conmemmodel_rec->user_id = $_POST['user_id'];
                    $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
                    $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                    $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                    if ($conmemmodel_rec->save()) {
                        $actmodel_sender = new Activities();
                        $actmodel_sender->user_id = $usermodel->id;
                        $actmodel_sender->type = "message";
                        $actmodel_sender->model_id = $conmsgmodel->id;
                        $actmodel_sender->model_type = "ConversationMessage";
                        $actmodel_sender->subject = "You sent a message in conversation: " . $_POST['subject'];
                        $actmodel_sender->message = $_POST['message'];
                        $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                        $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                        if ($actmodel_sender->save()) {
                            $actmodel_rec = new Activities();
                            $actmodel_rec->user_id = $_POST['user_id'];
                            $actmodel_rec->type = "message";
                            $actmodel_rec->model_id = $conmsgmodel->id;
                            $actmodel_rec->model_type = "ConversationMessage";
                            $actmodel_rec->subject = "You received a message in conversation: " . $_POST['subject'];
                            $actmodel_rec->message = $_POST['message'];
                            $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                            $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                            if ($actmodel_rec->save()) {
                                $this->_sendResponse(200, sprintf("Message Inserted Successfully"));
                            } else {
                                $this->_sendResponse(507, array());
                            }
                        }
                    }
                }
            }
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /* Employer inbox */

    /* Employer Office */
    /* add office location */

    public function actionaddofficelocation() {
        $this->apikeyAuth();
        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());

        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $model = new EmployerOffices;
        $imodel = Employers::model()->findByPk($id);
        $address = new Addresses();
        $now = new DateTime();

        foreach ($_POST as $var => $value) {
            if ($address->hasAttribute($var)) {
                $address->$var = $value;
            } else {
                $this->_sendResponse(500, array());
            }
        }

        $address->model_type = 'Office';
        $address->location = $_POST['county'] . ($_POST['county'] != '' ? ', ' . $_POST['country'] : '');
        $address->model_id = $imodel->id;
        $address->created_at = $now->format('Y-m-d H:i:s');
        $address->head_office = $_POST['head_office'];
        if ($address->save()) {

            $model->employer_id = $id;
            $model->address_id = $address->id;
            $model->created_at = $now->format('Y-m-d H:i:s');
            if ($model->save()) {
                $this->_sendResponse(200, sprintf("Added successfully"));
            } else {
                $this->_sendResponse(507, array());
            }
        }
    }

    function getLatLong($lonlat) {
        $formattedAddr = str_replace(' ', '+', $lonlat);
        $geocodeFromAddr = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . $formattedAddr . '&sensor=false');
        echo $geocodeFromAddr;
        die();
        $result = json_decode($geocodeFromAddr, TRUE);
        $lat = $result['results'][0]['geometry']['location']['lat'];
        $lng = $result['results'][0]['geometry']['location']['lng'];
        $loc = $result['results'][0]['address_components']['long_name'];
        $loc2 = $result['results'][0]['address_components']['short_name'];
        $data['longitute'] = $lat;
        $data['latitide'] = $lng;
        $data['location1'] = $loc;
        $data['location2'] = $loc2;
        return $data;
    }

    /* add office location */

    /* getall office */
    /* getall office */

    public function actiongetallofficeaddress() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());
        $this->apikeyAuth();

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];
        $employer = Employers::model()->findByPk($id);        
        $userid = $employer->user_id;
        $empofc = EmployerOffices::model();
        //$address = Addresses::model();//->findByAttributes(array('model_id' => $id, 'model_type' => 'employer'));
        $cri = new CDbCriteria();
        $cri->alias = "eo";
        $cri->select = 'eo.id,eo.status,a.street_number, a.address, a.town, a.postcode, a.county, a.country, a.location';
        $cri->join = "inner join cv16_addresses a on a.id = eo.address_id";
        $cri->condition = 'eo.employer_id=' . $id;
        $address = $empofc->getCommandBuilder()->createFindCommand($empofc->tableSchema, $cri)->queryAll();
        if (!empty($address)) {
            $this->_sendResponse(200, $address);
        } else {
            $this->_sendResponse(500, array());
        }
    }

    /* get office */

    /* fetch office */

    public function actionfetchoffice() {
        $this->apikeyAuth();
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());

        $id = $_GET['id'];

        $empofc = EmployerOffices::model()->findByPk($id);

        if ($empofc != NULL) {
            $address = Addresses::model()->findByPk($empofc->address_id);

            $cri = new CDbCriteria();
            $cri->select = 'id, model_id, model_type, street_number, address, town,head_office, postcode, county, country, location, latitude, longitude, created_at';
            $cri->condition = 'id=' . $address->id;

            $address = $address->getCommandBuilder()->createFindCommand($address->tableSchema, $cri)->queryAll();
            if (!empty($address)) {
                $this->_sendResponse(200, $address);
            } else {
                $this->_sendResponse(500, array());
            }
        } else {
            $this->_sendResponse(400, array());
        }
    }

    /* fetch office */

    /* delete office */

    public function actiondeleteoffice() {
        $this->apikeyAuth();

         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];
        
        $empoffice = EmployerOffices::model()->findByPk($id);
        
        $cri = new CDbCriteria();
        $cri->condition = 'id=' . $empoffice->address_id;
        Addresses::model()->deleteAll($cri);
        
        $cri1 = new CDbCriteria();
        $cri1->condition = 'id=' . $empoffice->id;
        EmployerOffices::model()->deleteAll($cri1);
        
        $this->_sendResponse(200, sprintf("Deleted successfully"));
    }

    /* delete office */
    /* edit office */

    public function actionupdateofficelocation() {
        $this->apikeyAuth();

         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());

        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }


        $model = EmployerOffices::model()->findByPk($id);
        $imodel = Employers::model()->findByPk($model->employer_id);
        $address = Addresses::model()->findByPk($model->address_id);
        $now = new DateTime();

        foreach ($_POST as $var => $value) {
            if ($address->hasAttribute($var)) {
                $address->$var = $value;
            } else {
                $this->_sendResponse(500, array());
            }
        }

        $address->model_type = 'Office';
        $address->location = $_POST['county'] . ($_POST['county'] != '' ? ', ' . $_POST['country'] : '');
        $address->model_id = $imodel->id;
        $address->created_at = $now->format('Y-m-d H:i:s');
        $address->head_office = $_POST['head_office'];
        if ($address->save()) {

            $this->_sendResponse(200, sprintf("Updated0 successfully"));
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /* edit office */

    public function actioneditbody() {
        $this->apikeyAuth();

         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $now = new DateTime();
        $employer = Employers::model()->findByPk($id);
        $employer->body = $_POST['body'];
        $employer->updated_at = $now->format('Y-m-d H:i:s');
        if ($employer->save()) {
            $cri = new CDbCriteria();
            $cri->select = 'id, body';
            $cri->condition = 'id=' . $id;
            $employer = $employer->getCommandBuilder()->createFindCommand($employer->tableSchema, $cri)->queryAll();
            $this->_sendResponse(200, $employer);
        } else {
            $this->_sendResponse(500, array());
        }
    }
    
    /* edit office */

    public function actioneditpersonalstatement() {
        $this->apikeyAuth();


        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $now = new DateTime();
        $user = Users::model()->findByPk($id);
        
        $statement = UserStatement::model()->findByAttributes(array('user_id'=>$user->id));
        $statement->description = $_POST['statement'];
        $statement->updated_at = $now->format('Y-m-d H:i:s');
        if ($statement->save()) {
            $this->_sendResponse(200, sprintf("Updated Successfully !!!"));
        } else {
            $this->_sendResponse(500, array());
        }
    }


    /* get body */

    public function actiongetcompanybackground() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());
        $this->apikeyAuth();

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $employer = Employers::model()->findByPk($id);
        if ($employer == null) {
            $this->_sendResponse(404, array());
        }
        $cri = new CDbCriteria();
        $cri->select = 'id,body';
        $cri->condition = 'id=' . $id;
        $employer = $employer->getCommandBuilder()->createFindCommand($employer->tableSchema, $cri)->queryAll();
        if (!empty($employer)) {
            $this->_sendResponse(200, $employer);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /* get body */

    // candidate get user list


    public function actiongetconversationmembers() {
        $this->apikeyAuth();
        // if(!$this->_checkAuth())
        //    $this->_sendResponse(405, array() );
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];


        $cri = new CDbCriteria();
        $cri->condition = "user_id =" . $id;
        $cri->group = "conversation_id";
        $cri->order = "created_at desc";
        $convmembers = ConversationMembers::model()->findAll($cri);


        $subarr = array();
        $result = array();

        foreach ($convmembers as $mem) {
            $crit = new CDbCriteria();
            $crit->condition = "conversation_id = " . $mem['conversation_id'] . " and user_id not in (" . $id . ")";
            $convmem = ConversationMembers::model()->find($crit);
            $created = date_format(new DateTime($convmem['created_at']), "Y-m-d");

            $conv = Conversations::model()->findByPk($convmem['conversation_id']);

            $msgcri = new CDbCriteria();
            $msgcri->condition = "conversation_id = " . $mem['conversation_id'] . " and created_at ='" . $mem['created_at'] . "'";
            $convmsg = ConversationMessages::model()->find($msgcri);

            $catarr = array();

            $catarr['conversation_id'] = $convmsg['conversation_id'];
            $catarr['user_id'] = $convmem['user_id'];
            $catarr['name'] = $convmem['name'];
            $catarr['created_at'] = $created;
            $catarr['subject'] = $conv->subject;
            $catarr['message'] = $convmsg['message'];

            $result[] = $catarr;
        }

        // $cri = new CDbCriteria();
        // $cri->alias = 'cm';
        //  $cri->select = 'c.id,c.subject,cmes.message,cmes.name,cm.created_at,cm.user_id';
        // $cri->condition = "cm.user_id =".$id;
        // $cri->join = 'inner join cv16_conversations c on c.id = cm.id 
        //               inner join cv16_conversation_messages cmes on cmes.conversation_id = c.id';
        // $cri->order = "created_at desc";
        // $convmodel = ConversationMembers::model();
        // $convmodel = $convmodel->getCommandBuilder()->createFindCommand($convmodel->tableSchema, $cri)->queryAll();      
        if (!empty($result)) {
            $this->_sendResponse(200, $result);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /* Employer Office */

    public function actiongetmessage() {
        $this->apikeyAuth();
        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];


        $now = new DateTime();
        $ccri = new CDbCriteria();
        $ccri->condition = 'conversation_id =' . $id;
        ConversationMembers::model()->updateAll(array('last_read' => $now->format('Y-m-d H:i:s')), $ccri);

        $cri1 = new CDbCriteria();
        $cri1->condition = "conversation_id =" . $id;
        $cri1->order = "created_at desc";
        $convmodel = ConversationMessages::model()->getCommandBuilder()->createFindCommand(ConversationMessages::model()->tableSchema, $cri1)->queryAll();


        if (!empty($convmodel)) {
            $this->_sendResponse(200, $convmodel);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /* save message */

    public function actionsendmessages() {
        $this->apikeyAuth();
        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());
        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());

        $conid = $_GET['id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $fromuser = $_POST['msg_from'];
        ;
        $touser = $_POST['msg_to'];
        $msg = $_POST['message'];

        $now = new DateTime();

        $convmodel = Conversations::model()->findByPk($conid);

        $usermodel = Users::model()->findByPk($fromuser);

        $convmsgmodel = new ConversationMessages();
        $convmsgmodel->conversation_id = $conid;
        $convmsgmodel->user_id = $fromuser;
        $convmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
        $convmsgmodel->message = $msg;
        $convmsgmodel->created_at = $now->format('Y-m-d H:i:s');
        $convmsgmodel->updated_at = $now->format('Y-m-d H:i:s');

        if ($convmsgmodel->save()) {

            $activitymodel = new Activities();
            $activitymodel->user_id = $fromuser;
            $activitymodel->type = "message";
            $activitymodel->model_id = $convmsgmodel->id;
            $activitymodel->model_type = "App\Messaging\ConversationMessage";
            $activitymodel->subject = "You sent a message in conversation: " . $convmodel->subject;
            $activitymodel->message = $msg;
            $activitymodel->created_at = $now->format('Y-m-d H:i:s');
            $activitymodel->updated_at = $now->format('Y-m-d H:i:s');
            $activitymodel->save();

            $activitymodel = new Activities();
            $activitymodel->user_id = $touser;
            $activitymodel->type = "message";
            $activitymodel->model_id = $convmsgmodel->id;
            $activitymodel->model_type = "App\Messaging\ConversationMessage";
            $activitymodel->subject = "You sent a message in conversation: " . $convmodel->subject;
            $activitymodel->message = $msg;
            $activitymodel->created_at = $now->format('Y-m-d H:i:s');
            $activitymodel->updated_at = $now->format('Y-m-d H:i:s');

            if ($activitymodel->save()) {

                $now = new DateTime();
                $ccri = new CDbCriteria();
                $ccri->condition = 'conversation_id =' . $convmsgmodel->conversation_id;
                ConversationMembers::model()->updateAll(array('last_read' => $now->format('Y-m-d H:i:s')), $ccri);

                $cri1 = new CDbCriteria();
                $cri1->condition = "id =" . $convmsgmodel->id;
                $cri1->order = "created_at desc";
                $convmodeln = ConversationMessages::model()->getCommandBuilder()->createFindCommand(ConversationMessages::model()->tableSchema, $cri1)->queryAll();

                if (!empty($convmodeln)) {
                    $this->_sendResponse(200, $convmodeln);
                } else {
                    $this->_sendResponse(507, array());
                }

                // $this->_sendResponse(200, sprintf("Successfully inserted",""));
            } else {
                $this->_sendResponse(507, array());
            }
        }
    }

    /* save message */

    public function actionupdateapplicationstatus() {
        $this->apikeyAuth();

         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());

        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $jobappmodel = JobApplications::model()->findByPk($id);
        $email = $jobappmodel->reference_mail;

        $status = $_POST['status'];

        $jobappmodel->status = $status;

        if ($jobappmodel->status == 1) {
            $this->notifyshortlist($email);
        } else if ($jobappmodel->status == 2) {
            $this->notifyinvite($email);
        } else if ($jobappmodel->status == 3) {
            $this->notifyinterview($email);
        } else if ($jobappmodel->status == -1) {
            $this->notifyreject($email);
        }

        if ($jobappmodel->save())
            $this->_sendResponse(200, sprintf("Successfully updated", ""));
        else
            $this->_sendResponse(500, array());
    }

    public function notifyshortlist($email) {
        $body = "shortlist for jobs";
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->Host = 'cvvid.com';
        $mail->Username = 'vinoth25pms@gmail.com';
        $mail->Password = '9865295902';
        $mail->SMTPSecure = 'ssl';
        $mail->SetFrom('mail@cvvid.com', 'cvvid');
        $mail->Subject = 'Shortlist for jobs details for cvvid.com';
        $mail->MsgHTML($body);
        // $mail->AddAddress($email);
        $mail->AddAddress('vinoth25pms@gmail.com');
        $mail->AddReplyTo('cvvid@gmail.com', 'Reply to cvvid');
        $mail->Send();
    }

    public function notifyinvite($email) {
        $body = "invite for jobs";
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->Host = 'cvvid.com';
        $mail->Username = 'vinoth25pms@gmail.com';
        $mail->Password = '9865295902';
        $mail->SMTPSecure = 'ssl';
        $mail->SetFrom('mail@cvvid.com', 'cvvid');
        $mail->Subject = 'Invite for jobs details for cvvid.com';
        $mail->MsgHTML($body);
        // $mail->AddAddress($email);
        $mail->AddAddress('vinoth25pms@gmail.com');
        $mail->AddReplyTo('cvvid@gmail.com', 'Reply to cvvid');
        $mail->Send();
    }

    public function notifyinterview($email) {
        $body = "interview for jobs";
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->Host = 'cvvid.com';
        $mail->Username = 'vinoth25pms@gmail.com';
        $mail->Password = '9865295902';
        $mail->SMTPSecure = 'ssl';
        $mail->SetFrom('mail@cvvid.com', 'cvvid');
        $mail->Subject = 'Interview for jobs details for cvvid.com';
        $mail->MsgHTML($body);
        // $mail->AddAddress($email);
        $mail->AddAddress('vinoth25pms@gmail.com');
        $mail->AddReplyTo('cvvid@gmail.com', 'Reply to cvvid');
        $mail->Send();
    }

    public function notifyreject($email) {
        $body = "reject for jobs";
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->Host = 'cvvid.com';
        $mail->Username = 'vinoth25pms@gmail.com';
        $mail->Password = '9865295902';
        $mail->SMTPSecure = 'ssl';
        $mail->SetFrom('mail@cvvid.com', 'cvvid');
        $mail->Subject = 'Reject for jobs details for cvvid.com';
        $mail->MsgHTML($body);
        // $mail->AddAddress($email);
        $mail->AddAddress('vinoth25pms@gmail.com');
        $mail->AddReplyTo('cvvid@gmail.com', 'Reply to cvvid');
        $mail->Send();
    }

    /* employer search candidate */

    public function actionsearchcandidates() {

//        $this->apikeyAuth();

         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $skillcategid = isset($_GET['skill_category']) ? $_GET['skill_category'] : array();
        //$skillarr = isset($_GET['skills']) ? $_GET['skills'] : array();   
        if (isset($_GET['location'])) {
            $location = $_GET['location'];
        }
        $lat = $_GET['latitude'];
        $long = $_GET['longitude'];
        $radius = isset($_GET['radius']) ? $_GET['radius'] : 0;

        $skilltxt = isset($_GET['skills']) ? $_GET['skills'] : "";

        $myArray = explode(',', $skilltxt);

        $skillarr = array();
        foreach ($myArray as $my_Array) {

            $skillsmodel = Skills::model()->findByAttributes(array('name' => $my_Array));
            $skillarr[] = $skillsmodel['id'];
        }

        $skillids = implode(',', $skillarr);
        $hasvideo = isset($_GET['video']) ? 1 : 0;

        //$skillids = implode(',', $skillarr);
        $pmodel = Profiles::model();
        $cri = new CDbCriteria();
        $cri->alias = "p";
        $cri->select = "u.id,CONCAT_WS(' ', u.forenames, u.surname) as name,u.location,s.name as skills,u.current_job
                        ,IF(p.photo_id > 0,CONCAT('https://cvvid.com/images/media/',m.id,'/','conversions/search.jpg'),CONCAT('https://cvvid.com/images/defaultprofile.jpg')) as photo, u.created_at";
        $cri->distinct = true;
        $cri->join = "inner join cv16_users u on u.id = p.owner_id "
                . "left outer join cv16_profile_skills ps on ps.profile_id = p.id "
                . "left outer join cv16_media m on m.id = p.photo_id "
                . "left outer join cv16_skills s on s.id = ps.skill_id "
                . "left outer join cv16_skill_categories sc on sc.id = s.skill_category_id "
                . "left outer join cv16_profile_videos pv on pv.profile_id = p.id";
        if ($skillcategid > 0)
            $cri->addCondition("s.skill_category_id =" . $skillcategid);
        if ($skillids != "")
            $cri->addCondition('ps.skill_id in (' . $skillids . ')', 'AND');
         if ($lat != 0 && $long != 0) {
            if ($radius > 0) {
                $cri->select = "*,(3959 * acos( cos( radians(".$lat.") ) * cos( radians(location_lat) ) *
                                cos(radians(location_lng) - radians(".$long.")) + sin(radians(".$lat.")) *
                                sin(radians(location_lat)) )) as distance";   
                $cri->having = "distance < " . $radius;
            } else {
                $cri->addCondition("ROUND(u.location_lat, 2) like '%" . round($lat, 2) . "%' and ROUND(u.location_lng, 2) like '%" . round($long, 2). "%'");
            }
        }
//        if ($lat != "") {
//            if ($radius > 0) {
//                $cri->select = "(6371 * acos( cos( radians(" . $lat . ") ) * cos( radians(" . $lat . ") ) *
//                                cos(radians(" . $long . ") - radians(" . $long . ")) + sin(radians(" . $lat . ")) *
//                                sin(radians(" . $lat . ")) )) as distance";
//
//                $cri->having = "distance < " . $radius;
//            } else {
//                //   $cri->addCondition("u.location like '%" . $location . "%'");
//                $cri->addCondition("u.location_lat =" . $lat . " and u.location_lng=" . $long);
//            }
//        }
        if($hasvideo > 0)
          $cri->addCondition("pv.video_id > 0");
        $cri->addCondition("p.visibility = 1 and p.type like '%student%' and u.deleted_at is null");
        $cri->group = "p.id";
        
//        echo var_dump($cri);
//        die();
        $pmodel = $pmodel->getCommandBuilder()->createFindCommand($pmodel->tableSchema, $cri)->queryAll();

        if (!empty($pmodel)) {
            $this->_sendResponse(200, $pmodel);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /* employer search candidate */

    public function actionoldreccreateemployer() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $usermodel = new Users;
        $empmodel = new Employers;
        $empusermodel = new EmployerUsers;
        $profilemodel = new Profiles;
        $addressmodel = new Addresses;
        $agencymodel = new AgencyEmployers();

        $now = new DateTime();
        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                if ($var == "email") {
                    $mmodel = Users::model()->findByAttributes(array('email' => $value));
                    if ($mmodel != null) {
                        $this->_sendResponse(502, array());
                        die();
                    }
                }
                $usermodel->$var = $value;
            } else if ($empmodel->hasAttribute($var)) {
                $empmodel->$var = $value;
            } else if ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else if ($empusermodel->hasAttribute($var)) {
                $empusermodel->$var = $value;
            } else if ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else if ($addressmodel->hasAttribute($var)) {
                $addressmodel->$var = $value;
            } else if ($agencymodel->hasAttribute($var)) {
                $agencymodel->$var = $value;
            } else if ($var == 'ispremium') {
                
            } else {
                $this->_sendResponse(500, array());
            }
        }
        $usermodel->created_at = $now->format('Y-m-d H:i:s');
        $usermodel->type = 'employer';
        if ((isset($_POST['location']))) {
            $lonlat = $this->getLatLong($_POST['location']);
        }
        $usermodel->location = $_POST['county'] . ($_POST['county'] != '' ? ', ' . $_POST['country'] : '');
        $usermodel->location_lat = $lonlat['location_lat'];
        $usermodel->location_lng = $lonlat['location_lng'];
        $usermodel->tel = $_POST['tel'];
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        if ($usermodel->save()) {
            $empmodel->name = $_POST['name'];
            $empmodel->user_id = $usermodel->id;
            $empmodel->email = $usermodel->email;
            $empmodel->tel = $usermodel->mobile;
            $empmodel->location = $usermodel->location;
            $empmodel->website = $_POST['slug'];
            $empmodel->location_lat = $usermodel->location_lat;
            $empmodel->location_lng = $usermodel->location_lng;
            $empmodel->created_at = $now->format('Y-m-d H:i:s');
            if (isset($_POST['ispremium']) && $_POST['ispremium'])
                $empmodel->trial_ends_at = Carbon::now()->addYear()->toDateTimeString();
            if ($empmodel->save()) {
                $empusermodel->employer_id = $empmodel->id;
                $empusermodel->user_id = $usermodel->id;
                $empusermodel->role = 'admin';
                $empusermodel->created_at = $now->format('Y-m-d H:i:s');
                if ($empusermodel->save()) {
                    // if($_POST['industry_id'] > 0)
                    $profilemodel->industry_id = $_POST['industry_id'];
                    $profilemodel->type = 'employer';
                    $profilemodel->owner_id = $usermodel->id;
                    $profilemodel->owner_type = 'employer';
                    $profilemodel->slug = $usermodel->forenames . '' . $usermodel->surname;
                    if ($profilemodel->save()) {
                        $addressmodel->created_at = $now->format('Y-m-d H:i:s');
                        $addressmodel->model_type = 'employer';
                        $addressmodel->model_id = $usermodel->id;
                        if ($addressmodel->save()) {
                            $agencymodel->employer_id = $empmodel->id;
                            $agencymodel->created_at = $now->format('Y-m-d H:i:s');
                            if ($agencymodel->save()) {
                                $cri = new CDbCriteria();
                                $cri->select = 'id,forenames,surname,email,mobile';
                                $cri->condition = "id = " . $usermodel->id;
                                $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                                $this->_sendResponse(200, $users);
                            }
                        }
                    } else {
                        echo var_dump($profilemodel->getErrors());
                    }
                } else {
                    echo var_dump($empusermodel->getErrors());
                }
            } else {
                echo var_dump($empmodel->getErrors());
            }
        } else {
            echo ($usermodel->getErrors());
            $this->_sendResponse(500, array());
        }
    }

    public function actionreccreateemployer() {
        // if(!$this->_checkAuth())
        //  $this->_sendResponse(405, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $usermodel = new Users;
        $empmodel = new Employers;
        $empusermodel = new EmployerUsers;
        $profilemodel = new Profiles;
        $addressmodel = new Addresses;
        $agencymodel = new AgencyEmployers();

        $usertable = Yii::app()->db->schema->getTable(Users::model()->tableName());
        $employerstable = Yii::app()->db->schema->getTable(Employers::model()->tableName());
        $employerusersstable = Yii::app()->db->schema->getTable(EmployerUsers::model()->tableName());
        $profiletable = Yii::app()->db->schema->getTable(Profiles::model()->tableName());
        $addresstable = Yii::app()->db->schema->getTable(Addresses::model()->tableName());

        $now = new DateTime();
        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                if ($var == "email") {
                    $mmodel = Users::model()->findByAttributes(array('email' => $value));
                    if ($mmodel != null) {
                        $this->_sendResponse(502, array());
                        die();
                    }
                }
                $usermodel->$var = $value;
            } else if ($empmodel->hasAttribute($var)) {
                $empmodel->$var = $value;
            } else if ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else if ($empusermodel->hasAttribute($var)) {
                $empusermodel->$var = $value;
            }else if ($addressmodel->hasAttribute($var)) {
                $addressmodel->$var = $value;
            } else if ($agencymodel->hasAttribute($var)) {
                $agencymodel->$var = $value;
            } else if ($var == 'ispremium') {
                
            } else if (!isset($usertable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($employerstable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($employerusersstable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($profiletable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($addresstable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else {
                $this->_sendResponse(500, array());
            }
        }
        $usermodel->created_at = $now->format('Y-m-d H:i:s');
        $usermodel->type = 'employer';
        if (isset($_POST['county']) && $_POST['country'])
            $usermodel->location = $_POST['county'] . ($_POST['county'] != '' ? ', ' . $_POST['country'] : '');
        if (isset($_POST['latitude']) && $_POST['latitude'])
            $usermodel->location_lat = $_POST['location_lat'];
        if (isset($_POST['longitude']) && $_POST['longitude'])
            $usermodel->location_lng = $_POST['location_lng'];
        $usermodel->tel = $_POST['tel'];
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);

        $empmodel->name = $_POST['name'];
     //   $profilemodel->slug = $this->getValidatedSlug($usermodel->forenames . "-" . $usermodel->surname);
        $profilemodel->slug = $this->getValidatedSlug($_POST['slug']);
        $valid = $usermodel->validate();
        $valid = $addressmodel->validate() && $valid;
        $valid = $profilemodel->validate() && $valid;
        $valid = $empmodel->validate() && $valid;
//        $valid = $empusermodel->validate() && $valid;
        if ($valid) {
            if ($usermodel->save()) {
                $empmodel->name = $_POST['name'];
                $empmodel->user_id = $usermodel->id;
                $empmodel->email = $usermodel->email;
                $empmodel->tel = $usermodel->mobile;
                $empmodel->location = $usermodel->location;
                $empmodel->location_lat = $usermodel->location_lat;
                $empmodel->location_lng = $usermodel->location_lng;
                $empmodel->created_at = $now->format('Y-m-d H:i:s');
                if (isset($_POST['ispremium']) && $_POST['ispremium'])
                    $empmodel->trial_ends_at = Carbon::now()->addYear()->toDateTimeString();
                if ($empmodel->save()) {
                    $empusermodel->employer_id = $empmodel->id;
                    $empusermodel->user_id = $usermodel->id;
                    $empusermodel->role = 'admin';
                    $empusermodel->created_at = $now->format('Y-m-d H:i:s');
                    if ($empusermodel->save()) {
                        // if($_POST['industry_id'] > 0)
                        $profilemodel->industry_id = $_POST['industry_id'];
                        $profilemodel->type = 'employer';
                        $profilemodel->owner_id = $usermodel->id;
                        $profilemodel->owner_type = 'employer';
                        $profilemodel->slug = $this->getValidatedSlug($_POST['slug']);
                        if ($profilemodel->save()) {
                            $addressmodel->created_at = $now->format('Y-m-d H:i:s');
                            $addressmodel->model_type = 'employer';
                            $addressmodel->model_id = $usermodel->id;
                            if ($addressmodel->save()) {
                                $agencymodel->employer_id = $empmodel->id;
                                $agencymodel->created_at = $now->format('Y-m-d H:i:s');
                                if ($agencymodel->save()) {
                                    $cri = new CDbCriteria();
                                    $cri->alias = 'recemp';
                                    $cri->select = 'recemp.id,recemp.forenames,recemp.surname,recemp.email,recemp.mobile,recemp.api_key,p.slug';
                                    $cri->join = 'left outer join cv16_profiles p on p.owner_id = recemp.id';
                                    $cri->condition = "recemp.id = " . $usermodel->id;
                                    $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                                    $this->_sendResponse(200, $users);
                                }
                            }
                        }
                    }
                }
            }
        } else {
            $usrerr = array();

            $usercolumns = Users::model()->getAttributes();
            $employercolumns = Employers::model()->getAttributes();
            $employeruserscolumns = EmployerUsers::model()->getAttributes();
            $addresscolumns = Addresses::model()->getAttributes();
            $profilecolumns = Profiles::model()->getAttributes();

            foreach ($usercolumns as $key => $val) {
                if ($usermodel->getError($key) != "") {
                    $usrerr[$key] = $usermodel->getError($key);
                }
            }
            foreach ($addresscolumns as $key => $val) {
                if ($addressmodel->getError($key) != null)
                    $usrerr[$key] = $addressmodel->getError($key);
            }
            foreach ($profilecolumns as $key => $val) {
                if ($profilemodel->getError($key) != null)
                    $usrerr[$key] = $profilemodel->getError($key);
            }
            foreach ($employercolumns as $key => $val) {
                if ($empmodel->getError($key) != null)
                    $usrerr[$key] = $empmodel->getError($key);
            }
//            foreach ($employeruserscolumns as $key => $val) {
//                if ($empusermodel->getError($key) != null)
//                    $usrerr[$key] = $empusermodel->getError($key);
//            }
            $this->_sendResponse(512, $usrerr);
        }
    }




    ///   /    get recruitment staff
    public function actiongetrecemployer() {

        $this->apikeyAuth();


         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $model = Agency::model()->findByPk($_GET['id']);
        $cri = new CDbCriteria();
        $cri->alias = 'i';
        $cri->select = "s.id  as staff_id,u.id as employer_id,u.name, u.website, u.email ";

        $cri->join = "left outer join cv16_agency_employers s on s.agency_id = i.id
                  left outer join cv16_employers u on u.id = s.employer_id";

        $cri->condition = "i.id =" . $_GET['id'];
        // echo var_dump($cri);
        // die();

        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }

    /* delete rec candidate */

    public function actiondeleterecemployer() {

        $this->apikeyAuth();

         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $deletedraftmsg = AgencyEmployers::model();
        $cri = new CDbCriteria();
        $cri->condition = "employer_id = " . $_GET['id'];
        AgencyEmployers::model()->deleteAll($cri);
        $employer = Employers::model()->findByAttributes(array('id' => $id));
        $userid = $employer->user_id;
        $cri = new CDbCriteria();
        $cri->condition = "id = " . $_GET['id'];
        Users::model()->deleteAll($cri);
        $deletedraftmsg = Users::model()->findByAttributes(array('id' => $userid));
        $cri = new CDbCriteria();
        $cri->condition = "id = " . $_GET['id'];
        Users::model()->deleteAll($cri);
        $this->_sendResponse(200, "Deleted Successfully");
    }

    public function actioncreatevacancy() {
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $agency = Agency::model()->findByPk($id);
        $userid = $agency->user_id;
        $jobskillmodel = new JobSkill();
        $jobmodel = new Jobs();
        $now = new DateTime();
        foreach ($_POST as $var => $value) {
            if ($jobmodel->hasAttribute($var)) {
                $jobmodel->$var = $value;
            } elseif ($var == 'skills') {
                $skills = $value;
            } else {
                $this->_sendResponse(400, array());
            }
        }
        $jobmodel->owner_id = $id;
        $jobmodel->owner_type = 'agency';
        $jobmodel->user_id = $userid;
        $d = str_replace('/', '-', $_POST["end_date"]);
        $endformat = date('Y-m-d', strtotime($d));
        $jobmodel->end_date = $endformat;
        $jobmodel->created_at = $now->format('Y-m-d H:i:s');

        if (isset($_POST['location'])) {
            $lonlat = $this->getLatLong($_POST['location']);
        }
        
        $jobmodel->location_lat = $lonlat['location_lat'];
        $jobmodel->location_lng = $lonlat['location_lng'];
        if ($jobmodel->save()) {

            foreach ($skills as $value) {
                $jobskillmodels = new JobSkill();
                $jobskillmodels->job_id = $jobmodel->id;
                $jobskillmodels->skill_id = $value['skill_id'];
                $jobskillmodels->vacancies = $value['vacancies'];
                $jobskill = $jobskillmodels->save();
            }
            if ($jobskill) {
                $cri = new CDbCriteria();
                $cri->select = 'id, owner_id, owner_type, user_id, industry_id, company_name, title, salary_type, salary_min, salary_max, end_date, skill_category_id, location, location_lat, location_lng, type, description, additional';
                $cri->condition = 'id=' . $jobmodel->id;
                $users = $jobmodel->getCommandBuilder()->createFindCommand($jobmodel->tableSchema, $cri)->queryAll();
                if (!empty($users)) {
                    $this->_sendResponse(200, $users);
                } else {
                    $this->_sendResponse(200, array());
                }
            } else {
                echo var_dump($jobskillmodels->getErrors());
            }
        } else {
            echo var_dump($jobmodel->getErrors());
        }
    }

    /* delete vacancy recruit */

    public function actiondeletevacancy() {
        $this->apikeyAuth();

         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];

        $jobs = Jobs::model()->findByPk($id);
        $cri = new CDbCriteria();
        $cri->condition = 'id=' . $id;
        Jobs::model()->deleteAll($cri);

        $jobsskill = JobSkill::model()->findByAttributes(array('job_id' => $id));
        $cri = new CDbCriteria();
        $cri->condition = 'job_id=' . $id;
        JobSkill::model()->deleteAll($cri);
        $this->_sendResponse(200, "Deleted Successfully");
    }

    /* delete vacancy recruit */

    public function actiongetallvacancy() {
        $this->apikeyAuth();
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $agency = Agency::model()->findByPk($id);
        $cri = new CDbCriteria();
        $cri->alias = 'agency';
        $cri->select = 'jobs.title, jobs.salary_type,jobs.vacancies';
        $cri->join = 'inner join cv16_jobs jobs on agency.id = jobs.owner_id ';
        $cri->condition = 'agency.id=' . $id;
        $agency = $agency->getCommandBuilder()->createFindCommand($agency->tableSchema, $cri)->queryAll();
        if (!empty($agency)) {
            $this->_sendResponse(200, $agency);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    public function actionoldfetchvacancy() {
         if(!$this->_checkAuth())
          $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];

        $jobs = Jobs::model()->findByPk($id);

        $cri = new CDbCriteria();
        $cri->alias = 'j';
        $cri->select = 's.name,sc.name,j.industry_id, j.company_name, j.title, j.salary_type, j.salary_min, j.salary_max, j.end_date, j.skill_category_id, j.location, j.location_lat, j.location_lng';
        $cri->join = 'left outer join cv16_job_skill js on js.job_id = j.id '
                . 'left outer join cv16_skills s on s.id = js.skill_id '
                . 'left outer join cv16_skill_categories sc on sc.id = s.skill_category_id';
        $cri->condition = 'j.id=' . $id;
        $jobs = $jobs->getCommandBuilder()->createFindCommand($jobs->tableSchema, $cri)->queryAll();
        if (!empty($jobs)) {
            $this->_sendResponse(200, $jobs);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    public function actionupdatevacancy() {
        $this->apikeyAuth();
         // if(!$this->_checkAuth())
         //  $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        } else {
            $_POST = $put_vars;
        }
        $agency = Agency::model()->findByPk($id);
        $userid = $agency->user_id;


        $jobmodel = Jobs::model()->findByAttributes(array('owner_id' => $id));

        $skills = array();
        $now = new DateTime();

        foreach ($_POST as $var => $value) {
            if ($jobmodel->hasAttribute($var)) {
                $jobmodel->$var = $value;
            } elseif ($var == 'skills') {
                $skills = array_merge($skills, $value);
            } else {
                $this->_sendResponse(400, array());
            }
        }
        $jobmodel->owner_id = $id;
        $jobmodel->owner_type = 'agency';
        $jobmodel->user_id = $userid;
        $d = str_replace('/', '-', $_POST["end_date"]);
        $endformat = date('Y-m-d', strtotime($d));
        $jobmodel->end_date = $endformat;
        $jobmodel->created_at = $now->format('Y-m-d H:i:s');
        if (isset($_POST['location'])) {
            $lonlat = $this->getLatLong($_POST['location']);
        }
        $jobmodel->location_lat = $lonlat['location_lat'];
        $jobmodel->location_lng = $lonlat['location_lng'];
        if ($jobmodel->save()) {
            $jobskillmodels = JobSkill::model()->deleteAllByAttributes(array('job_id' => $jobmodel->id));
            foreach ($skills as $value => $key) {
                $jobskillmodels = new JobSkill();
                $jobskillmodels->job_id = $jobmodel->id;
                $jobskillmodels->skill_id = $key['skill_id'];
                $jobskillmodels->vacancies = $key['vacancies'];
                $jobskillmodels->save();
            }
            $cri = new CDbCriteria();
            $cri->select = 'id, owner_id, owner_type, user_id, industry_id, company_name, title, salary_type, salary_min, salary_max, end_date, skill_category_id, location, location_lat, location_lng, type, description, additional';
            $cri->condition = 'id=' . $jobmodel->id;
            $users = $jobmodel->getCommandBuilder()->createFindCommand($jobmodel->tableSchema, $cri)->queryAll();
            if (!empty($users)) {
                $this->_sendResponse(200, $users);
            } else {
                $this->_sendResponse(507, array());
            }
        } else {
            $this->_sendResponse(507,$jobmodel->getErrors());
        }
    }

    public function actionfetchvacancy() {
        $this->apikeyAuth();

        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];

        $viewservice = [];
        $jobsmodel = Jobs::model()->findByPk($_GET['id']);
        $cri = new CDbCriteria();
        $cri->select = 'id, owner_id, owner_type, user_id, industry_id, company_name, title, salary_type, salary_min, salary_max, end_date, skill_category_id, location, location_lat, location_lng, type, description, additional';
        $cri->condition = 'id=' . $_GET['id'];
        $jobs = $jobsmodel->getCommandBuilder()->createFindCommand($jobsmodel->tableSchema, $cri)->queryAll();

        $jobskillmodel = JobSkill:: model()->findByAttributes(array('job_id' => $_GET['id']));
        $cri = new CDbCriteria();
        $cri->alias = 'js';
        $cri->select = 's.name,sc.name as Name';
        $cri->join = 'left outer join cv16_skills s on s.id = js.skill_id left outer join cv16_skill_categories sc on sc.id = s.skill_category_id';
        $cri->condition = 'js.job_id =' . $_GET['id'];
        $jobskill = $jobskillmodel->getCommandBuilder()->createFindCommand($jobskillmodel->tableSchema, $cri)->queryAll();

        foreach ($jobs as $key => $value) {
            $viewservice = $value;
        }
        $viewservice['skills'] = $jobskill;

        if (!empty($jobs)) {
            $this->_sendResponse(200, $viewservice);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    public function actionrecruitdeletevacancy() {
        $this->apikeyAuth();

        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $jobs = Jobs::model()->findByPk($id);
        $cri = new CDbCriteria();
        $cri->condition = 'id=' . $id;
        Jobs::model()->deleteAll($cri);

        $jobsskill = JobSkill::model()->findByAttributes(array('job_id' => $id));
        $cri = new CDbCriteria();
        $cri->condition = 'job_id=' . $id;
        JobSkill::model()->deleteAll($cri);
        $this->_sendResponse(200, sprintf("Deleted Successfully"));
    }

    public function actionfetchcandidatedetails() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $model = Users::model()->findByPk($_GET['id']);
        $cri = new CDbCriteria();
        $cri->alias = 'u';
        $cri->select = "u.id as user_id,u.trial_ends_at as trails,u.subscription_ends_at,p.hired,m.file_name,(SELECT count(*) FROM cv16_profile_views WHERE profile_id = p.id) as num_views,p.visibility,(SELECT count(*) FROM cv16_profile_favourites WHERE profile_id = p.id) as num_likes,CONCAT('https://cvvid.com/images/media/',p.photo_id,'/',m.file_name) as photo,CONCAT('https://cvvid.com/images/defaultprofile.jpg') as photodefault,CONCAT('https://cvvid.com/images/media/',p.cover_id,'/conversions/cover.jpg') as cover,CONCAT('https://cvvid.com/images/CoverPlaceholder.jpg') as coverdefault,u.forenames,u.surname,u.email,u.stripe_active,u.current_job,u.location,p.slug,p.photo_id,p.cover_id,p.id as profile_id,a.address,a.town,a.county,a.country,a.postcode";
        $cri->join = "inner join cv16_profiles p on p.owner_id = u.id
                      left outer join cv16_media m on m.id = p.photo_id
                      left outer join cv16_addresses a on a.model_id = u.id and a.model_type like '%student%'";
        $cri->condition = "u.id = " . $model->id . " and u.type like '%student%' and p.owner_type like '%student%'";
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        foreach ($users as $user) {
            $stripe_active = $user['stripe_active'];
            $trails = $user['trails'];
            $subscription_ends_at = $user['subscription_ends_at'];
        }
        $upgrade_status = "0";
        
         $account_type = "";
        if(Membership::subscribed($stripe_active, $trails, $subscription_ends_at)) {
            $account_type = "Premium Account";                 
        }
        else{
            $account_type = "Basic Account"; 
        }


        if (Membership::onTrail($trails) || $stripe_active) {
            $upgrade_status = "1";
        }

        $use = array("upgrade" => $upgrade_status,"account_type" => $account_type);

        $arr3 = array_merge($users[0], $use);
        // echo var_dump($arr3);die();
        // $users['data'] = 
//         $upgrade_status = "0";
//         if(Membership::onTrail($trail) || $usermodel->stripe_active)
//      {
//             $upgrade_status = "1";
//         }

        if (empty($arr3)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, array($arr3));
        }
    }

    public function actioncandidatehiredstatus() {
    $this->apikeyAuth();

        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || ($_GET['id'] == 0 ))
            $this->_sendResponse(400, array());
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }
        $id = $_GET['id'];
        $usermodel = Users::model()->findByPk($id);
        $cri = new CDbCriteria();
        $cri->condition = "owner_id = " . $id . " and type like '%student%'";
        $profilemodel = Profiles::model()->find($cri);
        $profilemodel->hired = $_POST['hired'];
        //$profilemodel->slug = $this->getValidatedSlug($usermodel->forenames."-".$usermodel->surname);
        if ($profilemodel->save()) {
            $this->_sendResponse(200, sprintf("Updated status successfully"));
        } else {
            echo var_dump($profilemodel->getErrors());
            //$this->_sendResponse(500, array());
        }
    }

    /* active or inactive */

    public function actioncandidatechangestatus() {

        $this->apikeyAuth();

        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }
        $usermodel = Users::model()->findByPk($id);
        if ($usermodel->status == 'active') {
            $usermodel->status = 'inactive';
        } else {
            $usermodel->status = 'active';
        }
        if ($usermodel->save()) {
            $this->_sendResponse(200, sprintf("Updated status successfully"));
        } else {
            $this->_sendResponse(500, array());
        }
    }

    /* public or private */

    public function actioncandidateprivatepublic() {
        $this->apikeyAuth();

        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }
        $usermodel = Users::model()->findByPk($id);
        $cri = new CDbCriteria();
        $cri->condition = "owner_id = " . $id . " and owner_type like '%Candidate%'";
        $profile = Profiles::model()->find($cri);
        if ($profile->visibility == 0) {
            $profile->visibility = '1';
        } else {
            $profile->visibility = '0';
        }
        if ($profile->save()) {
            $this->_sendResponse(200, sprintf("inserted successfully"));
        } else {
            $this->_sendResponse(500, array());
        }
    }

    /* search jobs */

    public function actionsearchjobs() {

        $skillcategid = isset($_GET['skill_category']) ? $_GET['skill_category'] : 0;
        $skilltxt = isset($_GET['skills']) ? $_GET['skills'] : "";

        $location = isset($_GET['location']) ? $_GET['location'] : "";
        $lat = isset($_GET['latitude']) ? $_GET['latitude'] : 0;
        $long = isset($_GET['longitude']) ? $_GET['longitude'] : 0;
        $jobtype = isset($_GET['jobtype']) ? $_GET['jobtype'] : "";
        $salary = isset($_GET['salary']) ? $_GET['salary'] : "";
        $radius = isset($_GET['distance']) ? $_GET['distance'] : 0;
        
        $jobtitle = isset($_GET['job_type']) ? $_GET['job_type'] : "";


        $skills = $_GET['skills'];

        $myArray = explode(',', $skilltxt);

        $skillarr = array();
        foreach ($myArray as $my_Array) {

            $skillsmodel = Skills::model()->findByAttributes(array('name' => $my_Array));
            $skillarr[] = $skillsmodel['id'];
        }

        $skillids = implode(',', $skillarr);
//        $jobmodel = Jobs::model();
        $cri = new CDbCriteria();
        $cri->alias = "j";
        $cri->select = "j.id,j.title,j.salary_min,j.salary_max,j.salary_type,j.location,j.created_at,j.location_lat,j.location_lng";
        $cri->join = "left outer join cv16_job_skill js on js.job_id = j.id
                      left outer join cv16_skills s on s.id = js.skill_id
                      left outer join cv16_skill_categories sc on sc.id = j.skill_category_id";
        if ($skillcategid > 0)
            $cri->addCondition("j.skill_category_id =" . $skillcategid);
        if($jobtitle != "")
            $cri->addCondition("j.title like '%" . $jobtitle . "%'");
        if ($skillids != "")
            $cri->addCondition('js.skill_id in (' . $skillids . ')', 'AND');
        if ($lat != 0 && $long != 0) {
            if ($radius > 0) {
                $cri->select = "j.*,(3959 * acos( cos( radians(".$lat.") ) * cos( radians(location_lat) ) *
                                cos(radians(location_lng) - radians(".$long.")) + sin(radians(".$lat.")) *
                                sin(radians(location_lat)) )) as distance";   
                $cri->having = "distance < " . $radius;
            } else {
                $cri->addCondition("ROUND(j.location_lat, 2) like '%" . round($lat, 2) . "%' and ROUND(j.location_lng, 2) like '%" . round($long, 2). "%'");
            }
        }
        if ($salary > 0) {
            $cri->addCondition("j.salary_min >= " . $salary);
        }
        if ($jobtype != "") {
            $cri->addCondition("j.type like '%" . $jobtype . "%'");
        }

        $cri->addCondition("j.end_date >= '" . date('Y-m-d') . "'");

        $cri->group = "j.id";

         $jobmodel = Jobs::model()->getCommandBuilder()->createFindCommand(Jobs::model()->tableSchema, $cri)->queryAll();

        $totarr = array();
        foreach ($jobmodel as $job)
        {
            $jobarr = array();
            
//            echo var_dump($job['id']);
//            die();
//            
            $cri1= new CDbCriteria();
            $cri1->alias = "j";
            $cri1->select = "j.id,j.title,j.salary_min,j.salary_max,j.salary_type,j.location,j.created_at,j.location_lat,j.location_lng";
            $cri1->condition = "j.id =".$job['id'];
            $jmodel = Jobs::model()->getCommandBuilder()->createFindCommand(Jobs::model()->tableSchema, $cri1)->queryAll();

          
            $totarr[] = array_merge($jmodel[0],array('posted_job'=>$this->timeAgo($job['created_at'])));
        }

        if (!empty($totarr)) {
            $this->_sendResponse(200, $totarr);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /* search jobs */


    /* get media for particular id */

    public function actiongetmedia() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];
        $mediamodel = Media::model()->findByPk($id);
        $cri = new CDbCriteria();
        $cri->select = 'id, model_type, model_id, collection_name, name, file_name, disk, size, manipulations, custom_properties';
        $cri->condition = 'id=' . $id;
        $mediamodel = $mediamodel->getCommandBuilder()->createFindCommand($mediamodel->tableSchema, $cri)->queryAll();
        if (!empty($mediamodel)) {
            $this->_sendResponse(200, $mediamodel);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /* get media for particular id */


    /* delete support documentation for particular user id */

    public function actiondeletesupportdocumentation() {

        $this->apikeyAuth();

        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];
        $usermodel = Users::model()->findByPk($id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $id));
        $profileid = $profilemodel->id;
        $userdocumentmodel = UserDocuments::model()->findByAttributes(array('user_id' => $id));
        $mediaid = $userdocumentmodel->user_id;
        $mediamodel = Media::model()->findByAttributes(array('model_id' => $mediaid));
        $cri = new CDbCriteria();
        $cri->condition = 'media_id = ' . $id;
        Media::model()->deleteAll($cri);
        $userdocumentmodel = UserDocuments::model()->findByAttributes(array('user_id' => $id));
        $cri = new CDbCriteria();
        $cri->condition = 'user_id = ' . $id;
        UserDocuments::model()->deleteAll($cri);
        $profiledocumentmodel = ProfileDocuments::model()->findByAttributes(array('profile_id' => $profileid));
        $cri = new CDbCriteria();
        $cri->condition = 'profile_id = ' . $id;
        ProfileDocuments::model()->deleteAll($cri);
        $this->_sendResponse(200, "Deleted Successfully");
    }

    /* delete support documentation for particular user id */

    /* list of videos */

    public function actiongetallprofilevideos() {

        $this->apikeyAuth();

        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $profilemodel = Profiles::model()->findByPk($id);
        if ($id != $profilemodel->id)
            $this->_sendResponse(404, array());

// $id = $_GET['docid'];

        $model = new ProfileVideos();
        $cri = new CDbCriteria();
        $cri->alias = "pv";
        $cri->select = "v.name as name,v.id,v.duration,pv.is_default,pv.state,CONCAT('https://cvvid.com/images/media/',m.id,'/',m.file_name) as photo,CONCAT('https://cvvid-new.s3.amazonaws.com/',v.video_id) as video";
        $cri->join = "inner join cv16_media m on m.model_id = pv.video_id
                      inner join cv16_videos v on v.id = pv.video_id";
        $cri->condition = "pv.profile_id =" . $id;
        $docs = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($docs)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $docs);
        }
    }

    /* list of videos */

    /* fetch professional */

    public function actionfetchprofessional() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $userexperiencemodel = UserExperiences::model()->findByPk($id);
        if ($userexperiencemodel == null) {
            $this->_sendResponse(507, array());
        }
        $cri = new CDbCriteria();
        $startyear = $userexperiencemodel->start_date;
        $startmonth = $userexperiencemodel->start_date;
        $endmonth = $userexperiencemodel->end_date;
        $endyear = $userexperiencemodel->end_date;
        $cri->select = "id, user_id,  MONTH('$startmonth') as startmonth,YEAR('$startyear') as startyear, MONTH('$endmonth') as endmonth,YEAR('$endyear') as endyear, location, company_name, industry, job_role, position, description";
        $cri->condition = 'id=' . $id;
        
        $userexperience = $userexperiencemodel->getCommandBuilder()->createFindCommand($userexperiencemodel->tableSchema, $cri)->queryAll();
        if (!empty($userexperience)) {
            $this->_sendResponse(200, $userexperience);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /* fetch professional */

//    get profile Completion
    public function actiongetprofilecompletion() {

        $this->apikeyAuth();

        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());



        if (!isset($_GET['profile_id']) || $_GET['profile_id'] == 0)
            $this->_sendResponse(404, array());

        $percentage = 0;
        $profiledocs = ProfileDocuments::model()->findAllByAttributes(array('profile_id' => $_GET['profile_id']));
        if (count($profiledocs) > 0)
            $percentage += '15';

        $profileexp = ProfileExperiences::model()->findAllByAttributes(array('profile_id' => $_GET['profile_id']));
        if (count($profileexp) > 0)
            $percentage += '15';

        $profilehob = ProfileHobbies::model()->findAllByAttributes(array('profile_id' => $_GET['profile_id']));
        if (count($profilehob) > 0)
            $percentage += '15';

        $profilelang = ProfileLanguages::model()->findAllByAttributes(array('profile_id' => $_GET['profile_id']));
        if (count($profilelang) > 0)
            $percentage += '15';

        $profilequal = ProfileQualifications::model()->findAllByAttributes(array('profile_id' => $_GET['profile_id']));
        if (count($profilequal) > 0)
            $percentage += '15';

        $profileskill = ProfileSkills::model()->findAllByAttributes(array('profile_id' => $_GET['profile_id']));
        if (count($profileskill) > 0)
            $percentage += '15';

        $profilevid = ProfileVideos::model()->findAllByAttributes(array('profile_id' => $_GET['profile_id']));
        if (count($profilevid) > 0)
            $percentage += '10';

        $pcentage = array();

        if ($percentage != "")
            $pcentage['percentage'] = $percentage;
        else
            $pcentage['percentage'] = 0;

        if (empty($percentage)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, array($pcentage));
        }
    }

    /* fetch educational */

    public function actionfetcheducation() {

        $this->apikeyAuth();

        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $userqualificationmodel = UserQualifications::model()->findByPk($id);
        if ($userqualificationmodel == null) {
            $this->_sendResponse(507, array());
        }

        $completionyear = $userqualificationmodel->completion_date;
        $completionmonth = $userqualificationmodel->completion_date;
        $cri = new CDbCriteria();
        $cri->select = "id, user_id, institution, location, field, level, completion_date, grade, description, YEAR('$completionyear') as completionyear,MONTH('$completionmonth') as completionmonth";
        $cri->condition = 'id =  ' . $id;
        $userqualificationmodel = $userqualificationmodel->getCommandBuilder()->createFindCommand($userqualificationmodel->tableSchema, $cri)->queryAll();
        if (!empty($userqualificationmodel)) {
            $this->_sendResponse(200, $userqualificationmodel);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /* fetch educational */


    /* update basic detail */

    public function actionupdatebasicdetail() {

        $this->apikeyAuth();

        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $id = $_GET['id'];
        $usermodel = Users::model()->findByPk($id);
       if($usermodel == Null){
        $this->_sendresponse(404,array());
       }
        $dob = $usermodel->dob;

          

        $now = new DateTime();
        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                if ($var == "dob") {
                    if ($value == "null-null-null")
                        $usermodel->$var = $dob;
                    else
                        $usermodel->$var = $value;
                }
                $usermodel->$var = $value;
            } else {
                $this->_sendresponse(400, array());
            }
        }
        $usermodel->created_at = $now->format('Y-m-d H:i:s');
        if ($usermodel->save()) {
            $cri = new CDbCriteria();
            $cri->select = 'id, forenames, surname, email, password, dob';
            $cri->condition = 'id=' . $id;
            $usermodel = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
            $this->_sendResponse(200, $usermodel);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /* update basic detail */






    /* update password */

    public function actionupdatepassword() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $id = $_GET['id'];
        $usermodel = Users::model()->findByPk($id);
        if($usermodel == Null){
            $this->_sendresponse(404,array());
        }
        $oldpass = $usermodel->password;
        $currentpassword = $_POST['current_password'];
        $newpassword = password_hash($_POST['new_password'], 1, ['cost' => 10]);
        if (password_verify($currentpassword, $oldpass)) {
            $usermodel->password = $newpassword;
            $usermodel->save();
            $profile = Profiles::model()->findByAttributes(array('owner_id' => $id));
            $this->_sendResponse(200, sprintf("Password change successfully"));
        } else {
            $this->_sendResponse(507, sprintf("The current password you have entered is incorrect"));
        }
    }

      /* update password */

    public function actionupdatevisible() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $id = $_GET['id'];
        $usermodel = Users::model()->findByPk($id);
        if($usermodel == Null){
            $this->_sendresponse(404,array());
        }
    
        $profile = Profiles::model()->findByAttributes(array('owner_id' => $id,'type'=>'candidate'));
        $profile->visibility = $_POST['visiblity'];
        $profile->save();
        
        
        $this->_sendResponse(200, sprintf("changed successfully"));
        
    }
    /* update password */


    /* update slug */

    public function actionupdateslug() {

        $this->apikeyAuth();

        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $id = $_GET['id'];

        $slug = $this->getValidatedSlug($_POST['slug']);

        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $id));
          if($profilemodel == Null){
            $this->_sendresponse(404,array());
        }

        $profilemodel->slug = $slug;
        if ($profilemodel->save()) {
            $cri = new CDbCriteria();
            $cri->select = 'slug';
            $cri->condition = 'owner_id=' . $id;
            $pemodel = Profiles::model()->getCommandBuilder()->createFindCommand(Profiles::model()->tableSchema, $cri)->queryAll();
            $this->_sendResponse(200, $pemodel);
        } else {
            // echo var_dump($profilemodel->errors);
            $this->_sendResponse(507, array());
        }
    }

    /* update slug */



    /* update location */

    public function actionupdatelocation() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $id = $_GET['id'];
        $type = $_GET['type'];

        $now = new DateTime();

        $cri = new CDbCriteria();
        $cri->condition = "model_id = " . $id . " and model_type like '%" . $type . "%' and deleted_at is null";

        $address = Addresses::model()->find($cri);


        // echo var_dump($address);die();
        //   $address = Addresses::model()->findByAttributes(array('owner_id' => $id,'last_name'=>$lastName));
        if ($address == NULL) {
             $address = new Addresses();
            $address->model_id = $id;
            $address->model_type = $type;

            // $this->_sendResponse(404,array());
        }

        $address->latitude = $_POST['latitude'];
        $address->longitude = $_POST['longitude'];
        $address->country = $_POST['country'];
        $address->county = $_POST['county'];
        $address->town = $_POST['town'];
        $address->address = $_POST['address'];
        $address->postcode = $_POST['postcode'];
        $address->location = $_POST['location'];

        $address->updated_at = $now->format('Y-m-d H:i:s');
        if ($address->save()) {
            $cri = new CDbCriteria();
            $cri->select = 'id, model_id, model_type, street_number, address, town, postcode, county, country, location, latitude, longitude';
            $cri->condition = 'model_id=' . $id;
            $address = $address->getCommandBuilder()->createFindCommand($address->tableSchema, $cri)->queryAll();              
            $this->_sendResponse(200, $address);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    public function getPostcode($lonlat) {
        $formattedAddr = str_replace(' ', '+', $lonlat);
        $geocodeFromAddr = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . $formattedAddr . '&sensor=false');
        $result = json_decode($geocodeFromAddr, TRUE);
        $lat = $result['results'][0]['geometry']['location']['lat'];
        $lng = $result['results'][0]['geometry']['location']['lng'];
        $loc = $result['results'][0]['address_components'][1]['long_name'];
        $loc2 = $result['results'][0]['address_components'][1]['short_name'];
        $location = $result['results'][0]['formatted_address'];
        $data['longitute'] = $lat;
        $data['latitide'] = $lng;
        $data['long_name'] = $longname;
        $data['short_name'] = $shortname;
        $data['formatted_address'] = $location;
        return $data;
    }

    /* update location */



    /* fetch hobbies */

    public function actionfetchhobbies() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $hobbies = UserHobbies::model()->findByPk($id);
        if ($hobbies == NULL) {
            $this->_sendResponse(404, array());
        }
        $cri = new CDbCriteria();
        $cri->select = 'id, user_id, activity, description';
        $cri->condition = 'id=' . $id;
        $hobbies = $hobbies->getCommandBuilder()->createFindCommand($hobbies->tableSchema, $cri)->queryAll();
        if (!empty($hobbies)) {
            $this->_sendResponse(200, $hobbies);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /* fetch hobbies */

    /* fetch language */

    public function actionfetchgoal() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        
        $language = UserGoal::model()->findByPk($id);
        if($language == null){
            $this->_sendresponse(404,array());
        }
        $cri = new CDbCriteria();
        $cri->select = 'id, user_id, activity, description';
        $cri->condition = 'id=' . $id;
        $language = $language->getCommandBuilder()->createFindCommand($language->tableSchema, $cri)->queryAll();
        if (!empty($language)) {
            $this->_sendResponse(200, $language);
        } else {
            $this->_sendResponse(507, array());
        }
    }
    
    /* fetch hobbies */

    public function actionfetchresources() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        
        $user = Users::model()->findByPk($id);
        
        $resource = StudentJournal::model()->findByAttributes(array('student_id' => $id));
        if ($resource == NULL) {
            $this->_sendResponse(404, array());
        }
        $cri = new CDbCriteria();
        $cri->select = 'id, student_id, institution, education, work_experience as experience, volunteering, curriculum_activities as curricular, goals, hobbies';
        $cri->condition = 'student_id=' . $id;
        $resource = $resource->getCommandBuilder()->createFindCommand($resource->tableSchema, $cri)->queryAll();
        
        if (!empty($resource)) {
            
            $resource = array_merge($resource[0], array("name" => $user->forenames." ".$user->surname));
            
            $this->_sendResponse(200, array($resource));
        } else {
            $this->_sendResponse(507, array());
        }
    }
    
    //    update hobbies
    public function actionupdateresources() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $resource = StudentJournal::model()->findByAttributes(array('student_id' => $id));
        
        if($resource == null)
        {
            $resource = new StudentJournal();
            $resource->student_id = $id;
        }

        if (json_decode(file_get_contents("php://input"), true)) {
            $put_vars = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }

        $now = new DateTime();

        foreach ($put_vars as $var => $value) {
            if ($resource->hasAttribute($var))
                $resource->$var = $value;
        }
        if ($resource->save()) {
            $this->_sendResponsewithMessage(200, sprintf("Successfully Resources updated", ""));
        } else {
            $this->_sendResponse(500, array());
        }
    }

    /* fetch language */


    /* applied job */

    public function actionappliedjob() {
        $this->apikeyAuth();

        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());

        $user_id = $_GET['id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $job_id = $_POST['job_id'];
        
        $now = new DateTime();
        
        $jmodel = new JobApplications();
        $jmodel->job_id = $job_id;
        $jmodel->user_id = $user_id;
        $jmodel->status = 0;
        if(trim($_POST['summary']) != "")
            $jmodel->summary = trim($_POST['summary']);
        if(trim($_POST['reference']) != "")
            $jmodel->reference_mail = trim($_POST['reference']);
        $jmodel->created_at = $now->format('Y-m-d H:i:s');
        if($jmodel->save())
        {
            $jobmodel = Jobs::model()->findByPk($job_id);
            $empmodel = Employers::model()->findByPk($jobmodel->owner_id);
            //send message to employer inbox
            $subject = "CANDIDATE HAS APPLIED FOR THE VACANCY";
            $message = "Candidate has applied for the post ".$jobmodel->title;
            $this->sendMessage($subject,$message,$user_id,$empmodel->user_id);
            
            $model = Users::model()->findByPk($user_id);
            $body = $this->renderPartial("//emails/job-applied", array('model'=>$model), true);
            // Send employer viewed notifcation to candidate
            Yii::import('application.extensions.phpmailer.JPhpMailer');
            $mail = new JPhpMailer;
            $mail->Host = 'smtp.gmail.com';
            $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
            $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
            $mail->SMTPSecure = 'ssl';
            $mail->SetFrom('mail@cvvid.com', 'CVVID');
            $mail->Subject = "Applied for Job - Confirmation mail";
            $mail->MsgHTML($body);
            $mail->AddAddress($model->email, $model->forenames);
            $mail->Send();
            
            
            //mail to employer for job applied            
           
            $body = $this->renderPartial("//emails/employer-job-applied", array('model'=>$empmodel,'umodel'=>$model), true);
            Yii::import('application.extensions.phpmailer.JPhpMailer');
            $mail = new JPhpMailer;
            $mail->Host = 'smtp.gmail.com';
            $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
            $mail->Password = Yii::app()->params['EMAIL_PASSWORD']; 
            $mail->SMTPSecure = 'ssl';
            $mail->SetFrom('mail@cvvid.com', 'CVVID');
            $mail->Subject = "CANDIDATE HAS APPLIED FOR THE VACANCY YOU POSTED - LOG IN TO VIEW THEIR VIDEO AND PROFILE";
            $mail->MsgHTML($body);
            $mail->AddAddress("karthikeyan2193@gmail.com", $empmodel->name);
            $mail->Send();
            
            $this->_sendResponsewithMessage(200, sprintf("Successfully Applied for job", ""));
        } else {
            $this->_sendResponse(507, array());
        }
        
    }

    /* applied job */




    /* check candidate applied job */

    public function actioncheckcandidatejob() {

        $this->apikeyAuth();

        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];
        $jobapplication = JobApplications::model()->findByAttributes(array('user_id' => $id));
        if (count($jobapplication) == 0) {
            $this->_sendResponsewithMessage(200, sprintf(" Your Are Not Applied Job", ""));
        } else {
            $this->_sendResponsewithMessage(200, sprintf(" Already Applied Job", ""));
        }
    }

    /* check candidate applied job */

    /* view job */

    public function actionviewjob() {
        $this->apikeyAuth();

        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];
        $jobs = Jobs::model()->findByPk($id);
        $cri = new CDbCriteria();
        $cri->select = 'id,company_name, title, salary_type, salary_min, salary_max, end_date, skill_category_id, location, location_lat, location_lng, type, description, additional, created_at, updated_at';
        $cri->condition = 'id=' . $id;
        $jobs = $jobs->getCommandBuilder()->createFindCommand($jobs->tableSchema, $cri)->queryAll();
        if (!empty($jobs)) {
            $this->_sendResponse(200, $jobs);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /* view job */

    // End Vinoth API //
    // Start ilai API //
//    create language
    public function actioncreategoal() {
        $this->apikeyAuth();

        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

    
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);
        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));
        $now = new DateTime();

        $model = new UserGoal();

        foreach ($_POST as $var => $value) {
            if ($model->hasAttribute($var))
                $model->$var = $value;
            else
                $this->_sendResponse(500, array());
        }

        $model->user_id = $usermodel->id;
        if ($model->save()) {
            $pqmodel = new ProfileGoal();
            $pqmodel->profile_id = $profilemodel->id;
            $pqmodel->user_goal_id = $model->id;
            $pqmodel->created_at = $now->format('Y-m-d H:i:s');
            if ($pqmodel->save()) {
                $prolan = UserGoal::model()->findByPk($model->id);
                $this->_sendResponse(200, sprintf("inserted successfully"));
            } else {
                $this->_sendResponse(500, array());
            }
        }
    }

    //    update language
    public function actionupdategoal() {
        $this->apikeyAuth();

        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        //           if (!$this->_checkAuth())
        //            $this->_sendResponse(405, array());
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $userlanmodel = UserGoal::model()->findByPk($id);
        if ($id != $userlanmodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $put_vars = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }


        $now = new DateTime();


        foreach ($put_vars as $var => $value) {
            if ($userlanmodel->hasAttribute($var)) {
                $userlanmodel->$var = $value;
            } else {
                $this->_sendResponse(500, array());
            }
        }
        $usermodel = Users::model()->findByPk($userlanmodel->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));

        if ($userlanmodel->save()) {

            $pemodel = ProfileGoal::model()->findByAttributes(array('profile_id' => $profilemodel->id, 'user_language_id' => $userlanmodel->id));
            $pemodel->updated_at = $now->format('Y-m-d H:i:s');
            $pemodel->save();
            if ($pemodel->save()) {
                $this->_sendResponsewithMessage(200, sprintf("Goals Successfully updated", ""));
            } else {
                $this->_sendResponse(500, array());
            }
        } else {
            echo var_dump($userlanmodel->errors);
        }
    }
    
    /// Get languages

    public function actiongetcurricular() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        
        $id = $_GET['id'];
        
        $model = UserExtra::model();
        $cri = new CDbCriteria();
        $cri->select = "id, activity, description";
        $cri->condition = "user_id =  " . $id . " ";
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
        if (!empty($users)) {
            $this->_sendResponse(200, $users);
        } else {
            $this->_sendResponse(507, array());
        }
    }
    
     //    create curricular
    public function actioncreatecurricular() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);
        
        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());
        
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        
        // echo var_dump($_POST);die();
        
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));
        
        $now = new DateTime();
        $model = new UserExtra();
        
        foreach ($_POST as $var => $value) {
            if ($model->hasAttribute($var))
                $model->$var = $value;
            else
                $this->_sendResponse(500, array());
        }
        $model->user_id = $usermodel->id;
        if ($model->save()) {
            $pqmodel = new ProfileExtra();
            $pqmodel->profile_id = $profilemodel->id;
            $pqmodel->user_extra_id = $model->id;
            $pqmodel->created_at = $now->format('Y-m-d H:i:s');
            if ($pqmodel->save()) {
                //$prohob = UserExtra::model()->findByPk($model->id);
                $this->_sendResponse(200, sprintf("inserted successfully"));
            } else {
                $this->_sendResponse(500, array());
            }
        }else{
            echo var_dump($model->errors);die();
        }
    }
    
    /* fetch hobbies */

    public function actionfetchcurricular() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        
        $id = $_GET['id'];
        $hobbies = UserExtra::model()->findByPk($id);
        if ($hobbies == NULL) {
            $this->_sendResponse(404, array());
        }
        $cri = new CDbCriteria();
        $cri->select = 'id, user_id, activity, description';
        $cri->condition = 'id=' . $id;
        $hobbies = $hobbies->getCommandBuilder()->createFindCommand($hobbies->tableSchema, $cri)->queryAll();
        if (!empty($hobbies)) {
            $this->_sendResponse(200, $hobbies);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    //    update hobbies
    public function actionupdatecurricular() {

        $this->apikeyAuth();

        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        
        $userhobmodel = UserExtra::model()->findByPk($id);
        
        if ($id != $userhobmodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $put_vars = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }

        $now = new DateTime();

        foreach ($put_vars as $var => $value) {
            if ($userhobmodel->hasAttribute($var))
                $userhobmodel->$var = $value;
            else
                $this->_sendResponse(500, array());
        }
        $usermodel = Users::model()->findByPk($userhobmodel->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));


        if ($userhobmodel->save()) {
            $pemodel = ProfileHobbies::model()->findByAttributes(array('profile_id' => $profilemodel->id, 'user_extra_id' => $userhobmodel->id));
            $pemodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($pemodel->save()) {
                $this->_sendResponsewithMessage(200, sprintf("Successfully Curricular updated", ""));
            } else {
                $this->_sendResponse(500, array());
            }
        }
    }
    
    /* language delete */

    public function actiondeletecurricular() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];

        $cri = new CDbCriteria();
        $cri->condition = "id = " . $_GET['id'];
        UserExtra::model()->deleteAll($cri);
        $this->_sendResponse(200, "Deleted Successfully");
    }


    /// Get languages

    public function actiongetgoals() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        
        $model = UserGoal::model();
        $cri = new CDbCriteria();
        $cri->select = "id, activity, description";
        $cri->condition = "user_id =  " . $id . " ";
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
        if (!empty($users)) {
            $this->_sendResponse(200, $users);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /* language delete */

    public function actiondeletegoal() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $deletedraftmsg = UserGoal::model();
        $cri = new CDbCriteria();
        $cri->condition = "id = " . $_GET['id'];
        UserGoal::model()->deleteAll($cri);
        $this->_sendResponse(200, "Deleted Successfully");
    }

    //    create hobbies
    public function actioncreatehobbies() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);
        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        
        // echo var_dump($_POST);die();
        
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));
        $now = new DateTime();
        $model = new UserHobbies();
        foreach ($_POST as $var => $value) {
            if ($model->hasAttribute($var))
                $model->$var = $value;
            else
                $this->_sendResponse(500, array());
        }
        $model->user_id = $usermodel->id;
        $model->created_at = $now->format('Y-m-d H:i:s');
        if ($model->save()) {
            $pqmodel = new ProfileHobbies();
            $pqmodel->profile_id = $profilemodel->id;
            $pqmodel->user_hobby_id = $model->id;
            $pqmodel->created_at = $now->format('Y-m-d H:i:s');
            if ($pqmodel->save()) {
                $prohob = UserHobbies::model()->findByPk($model->id);
                $this->_sendResponse(200, sprintf("inserted successfully"));
            } else {
                $this->_sendResponse(500, array());
            }
        }else{
            echo var_dump($model->errors);die();
        }
    }

    //    update hobbies
    public function actionupdatehobbies() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $userhobmodel = UserHobbies::model()->findByPk($id);
        if ($id != $userhobmodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $put_vars = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }

        $now = new DateTime();

        foreach ($put_vars as $var => $value) {
            if ($userhobmodel->hasAttribute($var))
                $userhobmodel->$var = $value;
            else
                $this->_sendResponse(500, array());
        }
        $usermodel = Users::model()->findByPk($userhobmodel->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));


        $userhobmodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($userhobmodel->save()) {
            $pemodel = ProfileHobbies::model()->findByAttributes(array('profile_id' => $profilemodel->id, 'user_hobby_id' => $userhobmodel->id));
            $pemodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($pemodel->save()) {
                $this->_sendResponsewithMessage(200, sprintf("Successfully Hobbies updated", ""));
            } else {
                $this->_sendResponse(500, array());
            }
        }
    }

    /// Get Hobbies

    public function actiongethobbies() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $model = UserHobbies::model();
        $cri = new CDbCriteria();
        $cri->select = "*";
        $cri->condition = "user_id =  " . $id . " ";
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
        if (!empty($users)) {
            $this->_sendResponse(200, $users);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /* language Hobbies */

    public function actiondeletehobbies() {

        $this->apikeyAuth();

        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $deletedraftmsg = UserHobbies::model();
        $cri = new CDbCriteria();
        $cri->condition = "id = " . $_GET['id'];
        UserHobbies::model()->deleteAll($cri);
        $this->_sendResponse(200, "Deleted Successfully");
    }

    //    create education
    public function actioncreateeducation() {
      $this->apikeyAuth();
        //           if (!$this->_checkAuth())
        //            $this->_sendResponse(405, array());
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);
        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));

        $now = new DateTime();
        $model = new UserQualifications();
        $pqmodel = new ProfileQualifications();

        $cmonth = "";
        $cyear = "";
        foreach ($_POST as $var => $value) {
            if ($model->hasAttribute($var))
                $model->$var = $value;
            else if ($var == "completion_month")
                $cmonth = $value;
            else if ($var == "completion_year")
                $cyear = $value;
            else
                $this->_sendresponse(400, array());
        }

        $completion_date = $cyear . "-" . $cmonth . "-01";
        $model->completion_date = $completion_date;
        $model->user_id = $usermodel->id;
        //  $model->completion_date = $completion_date;
        $model->created_at = $now->format('Y-m-d H:i:s');
        // echo var_dump($model);
        // die();
        if ($model->save()) {
            $pqmodel = new ProfileQualifications();
            $pqmodel->profile_id = $profilemodel->id;
            $pqmodel->user_qualification_id = $model->id;
            $pqmodel->created_at = $now->format('Y-m-d H:i:s');

            if ($pqmodel->save()) {
                $proexp = UserQualifications::model()->findByPk($model->id); 
                $this->_sendResponse(200, sprintf("Inserted Successfully"));
            } else {
                $this->_sendResponse(500, array());
            }
        }
    }

    //    update education
    public function actionupdateeducation() {

        $this->apikeyAuth();

                  // if (!$this->_checkAuth())
                  //  $this->_sendResponse(405, array());
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $userqualmodel = UserQualifications::model()->findByPk($id);
        if ($id != $userqualmodel->id)
            $this->_sendResponse(404, array());

        $usermodel = Users::model()->findByPk($userqualmodel->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));

        if (json_decode(file_get_contents("php://input"), true)) {
            $put_vars = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }


        $now = new DateTime();

        foreach ($put_vars as $var => $value) {
            if ($userqualmodel->hasAttribute($var))
                $userqualmodel->$var = $value;
            else
                $this->_sendResponse(500, array());
        }

        // $completion_date = $_POST['completion_year'] . "-" . $_POST['completion_month'] . "-01";

        $userqualmodel->user_id = $usermodel->id;
        // $userqualmodel->completion_date = $completion_date;
        // $userqualmodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($userqualmodel->save()) {
            $pemodel = ProfileQualifications::model()->findByAttributes(array('profile_id' => $profilemodel->id, 'user_qualification_id' => $userqualmodel->id));
            $pemodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($pemodel->save()) {
                $this->_sendResponsewithMessage(200, sprintf("Education updated Successfully", ""));
            } else {
                $this->_sendResponse(500, array());
            }
        }
    }

    /// Get education

    public function actiongeteducation() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $model = UserQualifications::model();
        $cri = new CDbCriteria();
        $cri->select = "*";
        $cri->condition = "user_id =  " . $id . " ";
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
        if (!empty($users)) {
            $this->_sendResponse(200, $users);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /* delete education */

    public function actiondeleteeducation() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $deletedraftmsg = UserQualifications::model();
        $cri = new CDbCriteria();
        $cri->condition = "id = " . $_GET['id'];
        UserQualifications::model()->deleteAll($cri);
        $this->_sendResponse(200, "Deleted Successfully");
    }

    ///  /    create Professional Experience
    public function actioncreateprofessionalexprience() {

        $this->apikeyAuth();


               // if(!$this->_checkAuth())
               //   $this->_sendResponse(405, array());



        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);

        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));
        $model = new UserExperiences();

        $now = new DateTime();
        //$iscurrent = 0;
        $start_month = null;
        $start_year = null;
        $end_month = null;
        $end_year = null;
        foreach ($_POST as $var => $value) {
            if ($model->hasAttribute($var))
                $model->$var = $value;
            else if ($var == "is_current")
                $iscurrent = $value;
            else if ($var == "start_month")
                $start_month = $value;
            else if ($var == "start_year")
                $start_year = $value;
            else if ($var == "end_month")
                $end_month = $value;
            else if ($var == "end_year")
                $end_year = $value;
            else
                $this->_sendResponse(500, array());
        }
        $model->user_id = $usermodel->id;
        $model->start_date = $start_year . "-" . $start_month . "-01";
        $model->end_date = $iscurrent == "1" ? null : ($end_year . "-" . $end_month . "-01");
        $model->created_at = $now->format('Y-m-d H:i:s');

        // Try to save the model
        if ($model->save()) {
            $pemodel = new ProfileExperiences();
            $pemodel->profile_id = $profilemodel->id;
            $pemodel->user_experience_id = $model->id;
            $pemodel->created_at = $now->format('Y-m-d H:i:s');
            $pemodel->save();
        }

        // $model = new UserExperiences();
        $cri = new CDbCriteria();
        $cri->alias = "ux";
        $cri->select = "ux.company_name,ux.location,ux.position,ux.start_date,ux.end_date,ux.description";
        $cri->join = "inner join cv16_profile_experiences px on px.user_experience_id = ux.id";
        $cri->condition = "px.user_experience_id =" . $model->id;
        $exps = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($exps)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $exps);
        }
    }

//getskill id

    public function getSkillID($name) {
        if(!$this->_checkAuth())
          $this->_sendResponse(405, array());

        // if (!isset($_GET['id']) || $_GET['id'] == 0)
        //     $this->_sendResponse(404, array());
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $cri = new CDbCriteria();
        $cri->condition = "name = '" . $name . "'";
        $model = Skills::model()->find($cri);
        return $model['id'];
    }

//    update Professional Experience
    public function actionupdateprofessionalexprience() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //  $this->_sendResponse(405, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $put_vars = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $userexmodel = UserExperiences::model()->findByPk($id);
        if ($id != $userexmodel->id)
            $this->_sendResponse(404, array());

        $usermodel = Users::model()->findByPk($userexmodel->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));

        $now = new DateTime();
        $iscurrent = 0;
        $start_month = null;
        $start_year = null;
        $end_month = null;
        $end_year = null;
        foreach ($_POST as $var => $value) {
            if ($userexmodel->hasAttribute($var))
                $userexmodel->$var = $value;
            else if ($var == "is_current")
                $iscurrent = $value;
            else if ($var == "start_month")
                $start_month = $value;
            else if ($var == "start_year")
                $start_year = $value;
            else if ($var == "end_month")
                $end_month = $value;
            else if ($var == "end_year")
                $end_year = $value;
            else
                $this->_sendResponse(500, array());
        }

        $userexmodel->start_date = $start_year . "-" . $start_month . "-01";
        $userexmodel->end_date = $iscurrent == 1 ? null : ($end_year . "-" . $end_month . "-01");
        $userexmodel->updated_at = $now->format('Y-m-d H:i:s');
        // Try to save the model
        if ($userexmodel->save()) {
            $pemodel = ProfileExperiences::model()->findByAttributes(array('profile_id' => $profilemodel->id, 'user_experience_id' => $userexmodel->id));
            $pemodel->updated_at = $now->format('Y-m-d H:i:s');
            $pemodel->save();
        }

        $model = new ProfileExperiences();
        $cri = new CDbCriteria();
        $cri->alias = "px";
        $cri->select = "ux.company_name,ux.location,ux.position,ux.start_date,ux.end_date,ux.description";
        $cri->join = "inner join cv16_user_experiences ux on ux.id = px.user_experience_id";
        $cri->condition = "px.user_experience_id=" . $id;
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }

    /* get professional */

    public function actiongetprofessional() {

        $this->apikeyAuth();        

        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $model = UserExperiences::model();


        $cri = new CDbCriteria();
        $sql = $model->find($cri);
        $startday = $sql['start_date'];
        $startmonth = $sql['start_date'];
        $startyear = $sql['start_date'];
        $endday = $sql['end_date'];
        $endmonth = $sql['end_date'];
        $endyear = $sql['end_date'];
//        $cri->select = "id, user_id, IF('$startday' = '0000-00-00', '0000-00-00', DAY('$startday')) as startdate ,  MONTH('$startmonth') as startmonth, YEAR('$startyear') as startyear,IF('$endday' = '0000-00-00', '0000-00-00', DAY('$endday')) as enddate, MONTH('$endmonth') as endmonth,YEAR('$endyear') as endyear,location, company_name, industry, job_role, position, description";
        $cri->condition = "user_id =  " . $id . " ";
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
        if (!empty($users)) {
            $this->_sendResponse(200, $users);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /* get professional */

    /* delete professional */

    public function actiondeleteprofessional() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $deletedraftmsg = UserExperiences::model();
        $cri = new CDbCriteria();
        $cri->condition = "id = " . $_GET['id'];
        UserExperiences::model()->deleteAll($cri);
        $this->_sendResponse(200, "Deleted Successfully");
    }

    ///  /    create  skills
    public function actionoldilayarajacreatecareer() {
        $this->apikeyAuth();
        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());
        $usermodel = Users::model()->findByPk($id);


        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));

        $now = new DateTime();
        $skills = json_decode($_POST['skills'], true);




        foreach ($skills as $skill) {

            $sid = $this->getSkillID($skill);
            $deletedraftmsg = ProfileSkills::model();
            $cri = new CDbCriteria();
            $cri->condition = "profile_id =" . $profilemodel->id . " and skill_id =" . $sid;
            ProfileSkills::model()->deleteAll($cri);

            $model = new ProfileSkills();
            $model->profile_id = intval($profilemodel->id);
            $model->skill_id = $sid;
            if (!$model->save()) {
                $this->_sendResponse(404, $model->getErrors());
            }
        }
        $model = new ProfileSkills();
        $cri = new CDbCriteria();

        $cri->select = "*";
        $cri->condition = "profile_id =" . $profilemodel->id;
        $exps = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($exps)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $exps);
        }
    }

       public function actioncreatecareer() {
    $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);

        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));


        $now = new DateTime();
        
        $skills = $_POST['skills'];
        
        $myArray = explode(',', $skills);
        
        foreach($myArray as $my_Array){
            
            $skillsmodel = Skills::model()->findByAttributes(array('name' => $my_Array));
             
            $model = new ProfileSkills();
            $model->profile_id = intval($profilemodel->id);
            $model->skill_id = $skillsmodel->id;
            $model->save();
            
        }
   
        $model = new ProfileSkills();
        $cri = new CDbCriteria();

        $cri->select = "*";
        $cri->condition = "profile_id =" . $profilemodel->id;
        $exps = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($exps)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $exps);
        }
    }
    
    
      public function actionupdatecareer() {
          
    $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);

        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));

        $now = new DateTime();
        
        $skills = $_POST['skills'];
        
        $myArray = explode(',', $skills);
        
                foreach($myArray as $my_Array){
                    $skillsmodel = Skills::model()->findByAttributes(array('name' => $my_Array));
            
            $skillcatid = $skillsmodel->skill_category_id;
            
            $skilldeletemodel = Skills::model()->findAllByAttributes(array('skill_category_id' => $skillcatid));
            
            foreach ($skilldeletemodel as $delskill)
            {
                $cri = new CDbCriteria();
                $cri->condition = "skill_id=" . $delskill->id." and profile_id=".intval($profilemodel->id);
                ProfileSkills::model()->deleteAll($cri);
            }
                }
        
        foreach($myArray as $my_Array){
            
            $skillsmodel = Skills::model()->findByAttributes(array('name' => $my_Array));
            
            
            $model = new ProfileSkills();
            $model->profile_id = intval($profilemodel->id);
            $model->skill_id = $skillsmodel->id;
            $model->save();
            
        }
   
        $model = new ProfileSkills();
        $cri = new CDbCriteria();

        $cri->select = "*";
        $cri->condition = "profile_id =" . $profilemodel->id;
        $exps = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($exps)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $exps);
        }
    }
    

    public function actionupdateskills() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());


        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);

        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));
        $deletedraftmsg = ProfileSkills::model();
        $cri = new CDbCriteria();
        $cri->condition = "profile_id =" . $profilemodel->id;
        ProfileSkills::model()->deleteAll($cri);
        $now = new DateTime();
        $skills = $_POST['skills'];

        foreach ($skills as $skill) {
            $model = new ProfileSkills();
            $model->profile_id = intval($profilemodel->id);
            $model->skill_id = $skill;
            $model->save();
        }
        $model = new ProfileSkills();
        $cri = new CDbCriteria();

        $cri->select = "*";
        $cri->condition = "profile_id =" . $profilemodel->id;
        $exps = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($exps)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $exps);
        }
    }

    /// Get professional

    public function actiongetskillcategory() {
        $this->apikeyAuth();

        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        $model = SkillCategories::model();
        $cri = new CDbCriteria();
        $cri->select = "*";
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
        if (!empty($users)) {
            $this->_sendResponse(200, $users);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /// Get professional

    public function actiongetskillsubcategory() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $model = Skills::model();
        $cri = new CDbCriteria();
        $cri->select = "*";
        $cri->condition = "skill_category_id =  " . $id . " ";
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
        if (!empty($users)) {
            $this->_sendResponse(200, $users);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    public function actiongetskills() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['profile_id']) || $_GET['profile_id'] == 0)
            $this->_sendResponse(404, array());

        $profile_id = $_GET['profile_id'];
        $category_id = $_GET['category_id'];

        $pskillsmodel = ProfileSkills::model()->findAllByAttributes(array('profile_id' => $profile_id));



        $pskillarr = array();
        foreach ($pskillsmodel as $pskill)
            $pskillarr[] = $pskill['skill_id'];

        $ids = implode(',', $pskillarr);



        $pcri = new CDbCriteria();
        $pcri->select = 'id, name';
        $pcri->condition = "id in (" . $ids . ") and skill_category_id =" . $category_id;
        $skillmodel = Skills::model()->getCommandBuilder()->createFindCommand(Skills::model()->tableSchema, $pcri)->queryAll();

        if (!empty($skillmodel)) {
            $this->_sendResponse(200, $skillmodel);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    //// create institution
   

    public function actionoldcreateinstitution() {

         //$this->apikeyAuth();

                //  if (!$this->_checkAuth())
                  // $this->_sendResponse(405, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $now = new DateTime();
        $instmodel = new Institutions();
        $iusermodel = new InstitutionUsers();
        $usermodel = new Users();
        $profilemodel = new Profiles();
        $addressmodel = new Addresses();
        
        // Generating API key
        $api_key = $this->generateApiKey();

        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                if ($var == "email") {
                    $mmodel = Users::model()->findByAttributes(array('email' => $value));
                    if ($mmodel != null) {
                        $this->_sendResponse(502, array());
                        die();
                    }
                }
                $usermodel->$var = $value;
            } else if ($instmodel->hasAttribute($var)) {
                $instmodel->$var = $value;
            } else if ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else if ($addressmodel->hasAttribute($var)) {
                $addressmodel->$var = $value;
            } else {
                $this->_sendResponse(500, array());
            }
        }

        $usermodel->status = 'pending';
        $usermodel->location = $_POST['county'] . ($_POST['county'] != '' ? ', ' . $_POST['country'] : '');
        if (isset($_POST['latitude']) && $_POST['latitude'])
            $usermodel->location_lat = $_POST['latitude'];
        if (isset($_POST['longitude']) && $_POST['longitude'])
            $usermodel->location_lng = $_POST['longitude'];
        $usermodel->type = 'institution_admin';
        $usermodel->created_at = $now->format('Y-m-d H:i:s');
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $usermodel->api_key = $api_key;
        $usermodel->save();
        //  instituion insert
        $instmodel->admin_id = $usermodel->id;
        $instmodel->email = $usermodel->email;
        $instmodel->status = 'pending';
        $instmodel->created_at = $now->format('Y-m-d H:i:s');
        if ($instmodel->save()) {
            $iusermodel->institution_id = $instmodel->id;
            $iusermodel->user_id = $usermodel->id;
            $iusermodel->role = "admin";
            $iusermodel->created_at = $now->format('Y-m-d H:i:s');
            $iusermodel->save();
        }
        //profile insert
        $slug = str_replace(" ", "-", $instmodel->name);
        $profilemodel->owner_type = "Institution";
        $profilemodel->owner_id = $instmodel->id;
        $profilemodel->created_at = $usermodel->created_at;
        $profilemodel->type = "institution";
        $profilemodel->slug = lcfirst($slug);
        $profilemodel->save();
        //address insert
        $addressmodel->model_id = $instmodel->id;
        $addressmodel->model_type = "Institutions";
        $addressmodel->created_at = $now->format('Y-m-d H:i:s');
        if ($addressmodel->save()) {


             $cri = new CDbCriteria();
            $cri->alias = "u";
            $cri->select = "u.id,u.forenames,u.surname,u.email,u.mobile,ins.id as institution_id,u.type,u.api_key";
            $cri->join = "inner join cv16_institutions ins on ins.admin_id = u.id";
            $cri->condition = "u.id=" . $usermodel->id;
            $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
             $this->_sendResponse(200, $users);           
        } else {

            // Errors occurred
            $msg = "<h1>Error</h1>";
            $msg .= sprintf("Couldn't create model <b>%s</b>", "");
            $msg .= "<ul>";
            foreach ($usermodel->errors as $attribute => $attr_errors) {
                $msg .= "<li>Attribute: $attribute</li>";
                $msg .= "<ul>";
                foreach ($attr_errors as $attr_error)
                    $msg .= "<li>$attr_error</li>";
                $msg .= "</ul>";
            }
            $msg .= "</ul>";
            $this->_sendResponse(500, array());
        }
    }


     public function actioncreateinstitution() {

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $now = new DateTime();
        $instmodel = new Institutions();
        $iusermodel = new InstitutionUsers();
        $usermodel = new Users();
        $profilemodel = new Profiles();
        $addressmodel = new Addresses();
        
        // Generating API key
        $api_key = $this->generateApiKey();
        
        $usertable = Yii::app()->db->schema->getTable(Users::model()->tableName());
        $instuitiontable = Yii::app()->db->schema->getTable(Institutions::model()->tableName());
        $instuitionusersstable = Yii::app()->db->schema->getTable(InstitutionUsers::model()->tableName());
        $profiletable = Yii::app()->db->schema->getTable(Profiles::model()->tableName());
        $addresstable = Yii::app()->db->schema->getTable(Addresses::model()->tableName());
        
        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                if ($var == "email") {
                    $mmodel = Users::model()->findByAttributes(array('email' => $value));
                    if ($mmodel != null) {
                        $this->_sendResponse(502, array());
                        die();
                    }
                }
                $usermodel->$var = $value;
            } else if ($instmodel->hasAttribute($var)) {
                $instmodel->$var = $value;
            } else if ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else if ($addressmodel->hasAttribute($var)) {
                $addressmodel->$var = $value;
            }else if (!isset($usertable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($instuitionusersstable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($instuitiontable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($profiletable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($addresstable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            }  else {
                $this->_sendResponse(500, array());
            }
        }

        $usermodel->status = 'pending';
        if (isset($_POST['county']) && $_POST['country'])
        $usermodel->location = $_POST['county'] . ($_POST['county'] != '' ? ', ' . $_POST['country'] : '');
        if (isset($_POST['latitude']) && $_POST['latitude'])
            $usermodel->location_lat = $_POST['latitude'];
        if (isset($_POST['longitude']) && $_POST['longitude'])
            $usermodel->location_lng = $_POST['longitude'];
        $usermodel->type = 'institution_admin';
        $usermodel->created_at = $now->format('Y-m-d H:i:s');
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $usermodel->api_key = $api_key;
        
        $profilemodel->slug = $this->getValidatedSlug($_POST['slug']);
        $instmodel->name = $_POST['name'];
        $valid = $usermodel->validate();
        $valid = $addressmodel->validate() && $valid;
        $valid = $profilemodel->validate() && $valid;
        $valid = $instmodel->validate() && $valid;
        
        if($valid) {
        $usermodel->save();
        //  instituion insert
        $instmodel->admin_id = $usermodel->id;
        $instmodel->email = $usermodel->email;
        $instmodel->status = 'pending';
        $instmodel->created_at = $now->format('Y-m-d H:i:s');
        if ($instmodel->save()) {
            $iusermodel->institution_id = $instmodel->id;
            $iusermodel->user_id = $usermodel->id;
            $iusermodel->role = "admin";
            $iusermodel->created_at = $now->format('Y-m-d H:i:s');
            $iusermodel->save();
        }
        //profile insert
       // $slug = str_replace(" ", "-", $instmodel->name);
        $profilemodel->owner_type = "Institution";
        $profilemodel->owner_id = $instmodel->id;
        $profilemodel->created_at = $usermodel->created_at;
         $profilemodel->slug = $this->getValidatedSlug($_POST['slug']);
        $profilemodel->type = "institution";
      //  $profilemodel->slug = lcfirst($slug);
        $profilemodel->save();
        //address insert
        $addressmodel->model_id = $instmodel->id;
        $addressmodel->model_type = "Institutions";
        $addressmodel->created_at = $now->format('Y-m-d H:i:s');
        if ($addressmodel->save()) {
            $cri = new CDbCriteria();
            $cri->alias = "u";
            $cri->select = "u.id,u.forenames,u.surname,u.email,u.mobile,ins.id as institution_id,u.type,u.api_key,p.slug";
            $cri->join = "inner join cv16_institutions ins on ins.admin_id = u.id left outer join cv16_profiles p on p.owner_id = ins.id ";
            $cri->condition = "u.id=" . $usermodel->id;
            $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
             $this->_sendResponse(200, $users);           
        }
        } else {
            $usrerr = array();
            $usercolumns = Users::model()->getAttributes();
            $instuitioncolumns = Institutions::model()->getAttributes();
            $addresscolumns = Addresses::model()->getAttributes();
            $profilecolumns = Profiles::model()->getAttributes();

            foreach ($usercolumns as $key => $val) {
                if ($usermodel->getError($key) != "") {
                    $usrerr[$key] = $usermodel->getError($key);
                }
            }
            foreach ($addresscolumns as $key => $val) {
                if ($addressmodel->getError($key) != null)
                    $usrerr[$key] = $addressmodel->getError($key);
            }
            foreach ($profilecolumns as $key => $val) {
                if ($profilemodel->getError($key) != null)
                    $usrerr[$key] = $profilemodel->getError($key);
            }
            foreach ($instuitioncolumns as $key => $val) {
                if ($instmodel->getError($key) != null)
                    $usrerr[$key] = $instmodel->getError($key);
            }
            $this->_sendResponse(512, $usrerr);
        }
    }



    ///   /    get institutions
    public function actiongetinstitutions() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
 
        $usermodel = Users::model()->findByPk($id);   
       if($usermodel == null){
        $this->_sendresponse(500,array());
       }
        
        $model = Institutions::model()->findByAttributes(array('admin_id'=>$usermodel->id));

        $cri = new CDbCriteria();
        $cri->alias = 'i';
        $cri->select = "i.id as institution_id,i.name,m.file_name,CONCAT('https://cvvid.com/images/media/',p.photo_id,'/',m.file_name) as photo,CONCAT('https://cvvid.com/images/defaultprofile.jpg') as photodefault,CONCAT('https://cvvid.com/images/media/',p.cover_id,'/conversions/cover.jpg') as cover,CONCAT('https://cvvid.com/images/CoverPlaceholder.jpg') as coverdefault,u.forenames,u.surname,u.email,u.id as user_id,i.stripe_active,u.current_job,u.type,u.location,p.slug,p.photo_id,p.cover_id,p.id as profile_id,a.address,a.town,a.county,a.country,a.postcode";
        $cri->join = "left outer join cv16_profiles p on p.owner_id = i.id
                      left outer join cv16_users u on u.id = i.admin_id
                      left outer join cv16_media m on m.id = p.photo_id
                      left outer join cv16_addresses a on a.model_id = i.id";
        $cri->condition = "i.id = " . $model->id." and u.type like '%institution%'";      
        $cri->group = "i.id";

        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(507, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }

    public function actionoldaddteacher() {

        $this->apikeyAuth();


                  // if (!$this->_checkAuth())
                  //  $this->_sendResponse(405, array());

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $insmodel = Institutions::model()->findByPk($id);
        if ($id != $insmodel->id)
            $this->_sendResponse(404, array());
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }


        $now = new DateTime();
        $usermodel = new Users();
        $iumodel = new InstitutionUsers();
        $api_key = $this->generateApiKey();

        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                $usermodel->$var = $value;
            } else {
                $this->_sendResponse(500, array());
            }
        }


        $usermodel->type = "teacher";
        $usermodel->created_at = $now->format('Y-m-d H:i:s');
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $usermodel->api_key = $api_key;
        if ($usermodel->save()) {

            $iumodel->institution_id = $id;
            $iumodel->user_id = $usermodel->id;
            $iumodel->role = $usermodel->type;
            $iumodel->created_at = $now->format('Y-m-d H:i:s');
            if ($iumodel->save()) {
                $cri = new CDbCriteria();
                $cri->select = 'id,forenames,surname,email,mobile,api_key';
                $cri->condition = "id = " . $usermodel->id;
                $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                $this->_sendResponse(200, $users);
            } else {
                $this->_sendResponse(500, array());
            }
        } else
            $this->_sendResponse("Email already exist");
    }

     public function actionaddteacher() {
        $this->apikeyAuth();
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $insmodel = Institutions::model()->findByPk($id);
        if ($id != $insmodel->id)
            $this->_sendResponse(404, array());
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }


        $now = new DateTime();
        $usermodel = new Users();
        $iumodel = new InstitutionUsers();
        $api_key = $this->generateApiKey();
        $usertable = Yii::app()->db->schema->getTable(Users::model()->tableName());
        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                $usermodel->$var = $value;
            } else if (!isset($usertable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else {
                $this->_sendResponse(500, array());
            }
        }


        $usermodel->type = "teacher";
        $usermodel->created_at = $now->format('Y-m-d H:i:s');
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $usermodel->api_key = $api_key;

        $valid = $usermodel->validate();
        if ($valid) {
            if ($usermodel->save()) {
                $iumodel->institution_id = $id;
                $iumodel->user_id = $usermodel->id;
                $iumodel->role = $usermodel->type;
                $iumodel->created_at = $now->format('Y-m-d H:i:s');
                if ($iumodel->save()) {
                    $cri = new CDbCriteria();
                    $cri->select = 'id,forenames,surname,email,mobile,api_key';
                    $cri->condition = "id = " . $usermodel->id;
                    $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                    $this->_sendResponse(200, $users);
                }
            }
        } else {
            $usrerr = array();
            $usercolumns = Users::model()->getAttributes();
            foreach ($usercolumns as $key => $val) {
                if ($usermodel->getError($key) != "") {
                    $usrerr[$key] = $usermodel->getError($key);
                }
            }
            $this->_sendResponse(512, $usrerr);
        }
    }



    public function actionoldeditteacher() {

        $this->apikeyAuth();


                  // if (!$this->_checkAuth())
                  //  $this->_sendResponse(405, array());


        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $iumodel = InstitutionUsers::model()->findByPk($id);
        

        if ($id != $iumodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }



        $usermodel = Users::model()->findByAttributes(array('id'=>$iumodel->user_id));

        $pwd = $usermodel->password;
        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                $usermodel->$var = $value;
            } else if ($var == 'users') {
                $_POST['users'] = $value;
            } else {
                $this->_sendResponse(400, array());
                // echo var_dump ($usermodel->errors);
            }
        }


        $now = new DateTime();

        $iumodel->updated_at = $now->format('Y-m-d H:i:s');
        $iumodel->save();

        $usermodel->attributes = $_POST['users'];
        $usermodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($_POST['password'] == "")
            $usermodel->password = $pwd;
        else
            $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        if ($usermodel->save()) {

            $cri = new CDbCriteria();
            $cri->select = 'id,forenames,surname,email,mobile';
            $cri->condition = "id = " . $usermodel->id;
            $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
            $this->_sendResponse(200, $users);
        } else {
             echo var_dump($usermodel->getErrors());
            $this->_sendResponse(500, array());
        }
    }


     public function actioneditteacher() {

        $this->apikeyAuth();

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $iumodel = InstitutionUsers::model()->findByPk($id);


        if ($id != $iumodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }



        $usermodel = Users::model()->findByAttributes(array('id' => $iumodel->user_id));
        $pwd = $usermodel->password;
        $usertable = Yii::app()->db->schema->getTable(Users::model()->tableName());
        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                $usermodel->$var = $value;
            } else if ($var == 'users') {
                $_POST['users'] = $value;
            } elseif (!isset($usertable->columns[$var])) {
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else {
                $this->_sendResponse(400, array());
                // echo var_dump ($usermodel->errors);
            }
        }


        $now = new DateTime();

        $iumodel->updated_at = $now->format('Y-m-d H:i:s');
        $iumodel->save();

        $usermodel->attributes = $_POST['users'];
        $usermodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($_POST['password'] == "")
            $usermodel->password = $pwd;
        else
            $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);

        $valid = $usermodel->validate();
        if ($valid) {
            if ($usermodel->save()) {
                $cri = new CDbCriteria();
                $cri->select = 'id,forenames,surname,email,mobile';
                $cri->condition = "id = " . $usermodel->id;
                $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                $this->_sendResponse(200, $users);
            }
        } else {
            $usrerr = array();
            $usercolumns = Users::model()->getAttributes();
            foreach ($usercolumns as $key => $val) {
                if ($usermodel->getError($key) != "") {
                    $usrerr[$key] = $usermodel->getError($key);
                }
            }
            $this->_sendResponse(512, $usrerr);
        }
    }

    /* Teacher delete */

    public function actiondeleteteacher() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $deletedraftmsg = InstitutionUsers::model();
        $cri = new CDbCriteria();
        $cri->condition = "user_id = " . $_GET['id'];
        InstitutionUsers::model()->deleteAll($cri);
        $deletedraftmsg = Users::model();
        $cri = new CDbCriteria();
        $cri->condition = "id = " . $_GET['id'];
        Users::model()->deleteAll($cri);
        $this->_sendResponse(200, "Deleted Successfully");
    }

    //// Add class


    public function actionoldsaveclass() {

        $this->apikeyAuth();
     $id = isset($_GET['id']) ? $_GET['id'] : null;
       

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }


        $usermodel = Users::model()->findByPk($id);
        

        if ($id != $usermodel->id)
            $this->_sendResponse(400, array());
       $model = Institutions::model()->findByAttributes(array('admin_id' => $usermodel->id));

        $id = $model->id;
        $now = new DateTime();
        $tgmodel = new TutorGroups();
        $tmodel = new Users();
        $iumodel = new InstitutionUsers();
        

       foreach ($_POST as $var => $value) {
            if ($tgmodel->hasAttribute($var)) {
                $tgmodel->$var = $value;
            } elseif ($tmodel->hasAttribute($var)) {
                $tmodel->$var = $value;
            } else {
                $this->_sendResponse(400, array());
            }
        }
        //  echo var_dump($_POST['teacher_id']);
        //  die();
        if ($_POST['teacher_id'] <= 0) {
            $tmodel->type = "teacher";
            $tmodel->status = "active";
            $tmodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
            $tmodel->created_at = $now->format('Y-m-d H:i:s');

            if ($tmodel->save()) {

                $iumodel->institution_id = $model->id;
                $iumodel->user_id = $tmodel->id;
                $iumodel->role = $tmodel->type;
                $iumodel->created_at = $now->format('Y-m-d H:i:s');
                $iumodel->save();

                $tgmodel->teacher_id = $tmodel->id;
            }
        }

        $grad_date = null;
        if ($_POST['graduation_date'] != "") {
            //   $grad_date = date_format(new DateTime($_POST['graduation_date']),"Y-m-d");
            $d = str_replace('/', '-', $_POST["graduation_date"]);
            $grad_date = date('Y-m-d', strtotime($d));
        }
        $tgmodel->institution_id = $id;
        $tgmodel->graduation_date = $grad_date;
        $tgmodel->created_at = $now->format('Y-m-d H:i:s');
        // $tgmodel->account_limit = $_POST['allocated_students'];//
        if ($tgmodel->save()) {
            $forenames_arr = $_POST['forenames'];
            $surname_arr = $_POST['surname'];
            $email_arr = $_POST['email'];
            $password_arr = $_POST['password'];
            
            for ($i = 0; $i < count($forenames_arr); $i++) {

                $umodel = new Users();
                $umodel->forenames = $forenames_arr[$i];
                $umodel->surname = $surname_arr[$i];
                $umodel->password = $password_arr[$i];
                $umodel->email = $email_arr[$i];
                $umodel->type = "candidate";
                $umodel->status = "active";
                $umodel->created_at = $now->format('Y-m-d H:i:s');
                if ($umodel->save(false)) {
                    $pmodel = new Profiles();
                    $pmodel->owner_id = $umodel->id;
                    $pmodel->owner_type = "Candidate";
                    $pmodel->type = "candidate";
                    $pmodel->slug = $this->getValidatedSlug($umodel->forenames . "-" . $umodel->surname);
                    $pmodel->created_at = $now->format('Y-m-d H:i:s');
                    if($pmodel->save())
{
                    $tgsmodel = new TutorGroupsStudents();
                    $tgsmodel->institution_id = $id;
                    $tgsmodel->tutor_group_id = $tgmodel->id;
                    $tgsmodel->student_id = $umodel->id;
                    $tgsmodel->graduation_date = $grad_date;
                    $tgsmodel->created_at = $now->format('Y-m-d H:i:s');
                    $tgsmodel->save(false);
                }
                else {
                    echo var_dump($pmodel->getErrors());
                }
                } else {
                    echo var_dump($umodel->getErrors());
                }
            }
            $this->_sendResponse(200, sprintf("Successfully class added", ""));
        } else {
            echo var_dump($tgmodel->getErrors());
        }
    }

      public function actionsaveclass() {

        $this->apikeyAuth();
        $id = isset($_GET['id']) ? $_GET['id'] : null;


        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }


        $usermodel = Users::model()->findByPk($id);


        if ($id != $usermodel->id)
            $this->_sendResponse(400, array());
        $model = Institutions::model()->findByAttributes(array('admin_id' => $usermodel->id));

        $id = $model->id;
        $now = new DateTime();
        $tgmodel = new TutorGroups();
        $tmodel = new Users();
        $pmodel = new Profiles();
        $iumodel = new InstitutionUsers();
        $usertable = Yii::app()->db->schema->getTable(Users::model()->tableName());
        $profiletable = Yii::app()->db->schema->getTable(Profiles::model()->tableName());
        $tutorgroupstable = Yii::app()->db->schema->getTable(TutorGroups::model()->tableName());


        foreach ($_POST as $var => $value) {
            if ($tgmodel->hasAttribute($var)) {
                $tgmodel->$var = $value;
            } elseif ($pmodel->hasAttribute($var)) {
                $pmodel->$var = $value;
            } elseif ($tmodel->hasAttribute($var)) {
                $tmodel->$var = $value;
            } else if (!isset($usertable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($profiletable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            }else {
                $this->_sendResponse(400, array());
            }
        }
        //  echo var_dump($_POST['teacher_id']);
        //  die();
        if ($_POST['teacher_id'] <= 0) {
            $tmodel->type = "teacher";
            $tmodel->status = "active";
            $tmodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
            $tmodel->created_at = $now->format('Y-m-d H:i:s');

            if ($tmodel->save()) {

                $iumodel->institution_id = $model->id;
                $iumodel->user_id = $tmodel->id;
                $iumodel->role = $tmodel->type;
                $iumodel->created_at = $now->format('Y-m-d H:i:s');
                $iumodel->save();

                $tgmodel->teacher_id = $tmodel->id;
            }
        }

        $grad_date = null;
        if ($_POST['graduation_date'] != "") {
            //   $grad_date = date_format(new DateTime($_POST['graduation_date']),"Y-m-d");
            $d = str_replace('/', '-', $_POST["graduation_date"]);
            $grad_date = date('Y-m-d', strtotime($d));
        }
        $tgmodel->institution_id = $id;
        $tgmodel->graduation_date = $grad_date;
        $tgmodel->created_at = $now->format('Y-m-d H:i:s');
        // $tgmodel->account_limit = $_POST['allocated_students'];//
        if ($tgmodel->save()) {
            $forenames_arr = $_POST['forenames'];
            $surname_arr = $_POST['surname'];
            $email_arr = $_POST['email'];
            $password_arr = $_POST['password'];



            for ($i = 0; $i < count($forenames_arr); $i++) {

                $umodel = new Users();
                $umodel->forenames = $forenames_arr[$i];
                $umodel->surname = $surname_arr[$i];
                $umodel->password = $password_arr[$i];
                $umodel->email = $email_arr[$i];
                $umodel->type = "candidate";
                $umodel->status = "active";
                $umodel->created_at = $now->format('Y-m-d H:i:s');
                $pmodel->slug = $this->getValidatedSlug($_POST['slug']);
                $valid = $umodel->validate();
                $valid = $pmodel->validate() && $valid;
                if ($valid) {
                    if ($umodel->save(false)) {
                        $pmodel = new Profiles();
                        $pmodel->owner_id = $umodel->id;
                        $pmodel->owner_type = "Candidate";
                        $pmodel->type = "candidate";
                        $pmodel->slug = $this->getValidatedSlug($_POST['slug']);                     
                        $pmodel->created_at = $now->format('Y-m-d H:i:s');
                        if ($pmodel->save()) {
                            $tgsmodel = new TutorGroupsStudents();
                            $tgsmodel->institution_id = $id;
                            $tgsmodel->tutor_group_id = $tgmodel->id;
                            $tgsmodel->student_id = $umodel->id;
                            $tgsmodel->graduation_date = $grad_date;
                            $tgsmodel->created_at = $now->format('Y-m-d H:i:s');
                            $tgsmodel->save(false);
                            $this->_sendResponse(200, sprintf("Successfully class added", ""));
                        }
                    }
                } else {
                    $usrerr = array();
                    $usercolumns = Users::model()->getAttributes();
                    foreach ($usercolumns as $key => $val) {
                        if ($umodel->getError($key) != "") {
                            $usrerr[$key] = $umodel->getError($key);
                        }
                    }
                    $this->_sendResponse(512, $usrerr);
                }
            }
        }
    }



    public function actioneditclass() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $tgmodel = TutorGroups::model()->findByPk($id);
        if ($id != $tgmodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        } 

        $model = Institutions::model()->findByPk($tgmodel->institution_id);
        $id = $model->id;
        $now = new DateTime();

        foreach ($_POST as $var => $value) {
            if ($tgmodel->hasAttribute($var)) {
                $tgmodel->$var = $value;
            } else if ($var == 'TutorGroups') {
                
            } else {
                //  echo $var;
                $this->_sendResponse(400, array());
                //  echo var_dump($tgmodel->errors);
            }
        }
        // die();


        $grad_date = null;
        if ($_POST['graduation_date'] != "") {
            $d = str_replace('/', '-', $_POST["graduation_date"]);
            $grad_date = date('Y-m-d', strtotime($d));
        }
        // $grad_date = date_format(new DateTime($_POST['graduation_date']),"Y-m-d");

        $tgmodel->attributes = $_POST['TutorGroups'];
        $tgmodel->graduation_date = $grad_date;
        $tgmodel->updated_at = $now->format('Y-m-d H:i:s');
        //  $tgmodel->account_limit = $_POST['allocated_students'];
        if ($tgmodel->save()) {
            $this->_sendResponsewithMessage(200, sprintf("Successfully updated class", ""));
        } else {
            $this->_sendResponse(500, array());
        }
    }

    /* class delete */

    public function actiondeleteclass() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $deletedraftmsg = TutorGroups::model();
        $cri = new CDbCriteria();
        $cri->condition = "id = " . $_GET['id'];
        TutorGroups::model()->deleteAll($cri);
        $this->_sendResponse(200, "Deleted Successfully");
    }

///  Add student

    public function actionoldaddstudent() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);
        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

        $now = new DateTime();

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        } else {
            $_POST = $_POST;
        }


        $tgsmodel = new TutorGroupsStudents();

        $umodel = new Users();

        foreach ($_POST as $var => $value) {
            if ($umodel->hasAttribute($var)) {
                $umodel->$var = $value;
            } else if ($tgsmodel->hasAttribute($var)) {
                $tgsmodel->$var = $value;
            } else if ($var == 'tutorgroup_id') {
                
            } else {
                // echo $var
                $this->_sendResponse(400, array());
            }
        }

        $insmodel = Institutions::model()->findByAttributes(array('admin_id' => $usermodel->id));
        $id = $insmodel->id;
        $tutorid = $_POST['tutorgroup_id'];
        $instid = $_POST['institution_id'];


        $umodel->type = "candidate";
        $umodel->status = "active";
        $umodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $umodel->created_at = $now->format('Y-m-d H:i:s');
        if ($umodel->save()) {
            $pmodel = new Profiles();
            $pmodel->owner_id = $umodel->id;
            $pmodel->owner_type = "Candidate";
            $pmodel->type = "candidate";
            $pmodel->slug = $this->getValidatedSlug($umodel->forenames . "-" . $umodel->surname);
            $pmodel->created_at = $now->format('Y-m-d H:i:s');
            $pmodel->save();

            $grad_date = null;
            $d = str_replace('/', '-', $_POST["graduation_date"]);
            $grad_date = date('Y-m-d', strtotime($d));
//          if($_POST['graduation_date'] != "")
            // $grad_date = date_format(new DateTime($_POST['graduation_date']),"Y-m-d");
            $tgsmodel = new TutorGroupsStudents();
            $tgsmodel->institution_id = $instid;
            $tgsmodel->tutor_group_id = $tutorid;
            $tgsmodel->graduation_date = $grad_date;
            $tgsmodel->student_id = $umodel->id;
            $tgsmodel->created_at = $now->format('Y-m-d H:i:s');

            if ($tgsmodel->save()) {
                $this->_sendResponsewithMessage(200, sprintf("Successfully added student", ""));
            } else {
                echo var_dump($tgsmodel->getErrors());
                $this->_sendResponse(500, array());
            }
        } else
            $this->_sendResponse(502, array());
    }




     public function actionaddstudent() {
       $this->apikeyAuth();
        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $usermodel = Users::model()->findByPk($id);
        if ($id != $usermodel->id)
            $this->_sendResponse(404, array());

        $now = new DateTime();

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }


        $tgsmodel = new TutorGroupsStudents();
        $umodel = new Users();
        $pmodel = new Profiles();
        $usertable = Yii::app()->db->schema->getTable(Users::model()->tableName());
        $profiletable = Yii::app()->db->schema->getTable(Profiles::model()->tableName());

        foreach ($_POST as $var => $value) {
            if ($umodel->hasAttribute($var)) {
                $umodel->$var = $value;
            } else if ($tgsmodel->hasAttribute($var)) {
                $tgsmodel->$var = $value;
            }else if ($pmodel->hasAttribute($var)) {
                $pmodel->$var = $value;
            } else if ($var == 'tutorgroup_id') {
                
            } elseif (!isset($usertable->columns[$var])) {
                $this->_sendResponse(511, array(), 'text/html', $var);
            } elseif (!isset($profiletable->columns[$var])) {
                $this->_sendResponse(511, array(), 'text/html', $var);
            }
            else {
                $this->_sendResponse(400, array());
            }
        }

        $insmodel = Institutions::model()->findByAttributes(array('admin_id' => $usermodel->id));
        $id = $insmodel->id;
        $tutorid = $_POST['tutorgroup_id'];
        $instid = $_POST['institution_id'];
        $umodel->type = "candidate";
        $umodel->status = "active";
        $umodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $umodel->created_at = $now->format('Y-m-d H:i:s');
        $pmodel->slug = $this->getValidatedSlug($umodel->forenames . "-" . $umodel->surname);
        $valid = $umodel->validate();
        $valid = $pmodel->validate() && $valid;
        if ($valid) {
            if ($umodel->save()) {
                $pmodel = new Profiles();
                $pmodel->owner_id = $umodel->id;
                $pmodel->owner_type = "Candidate";
                $pmodel->slug = $this->getValidatedSlug($umodel->forenames . "-" . $umodel->surname);
                $pmodel->type = "candidate";
                $pmodel->created_at = $now->format('Y-m-d H:i:s');
                $pmodel->save();

                $grad_date = null;
                $d = str_replace('/', '-', $_POST["graduation_date"]);
                $grad_date = date('Y-m-d', strtotime($d));
//          if($_POST['graduation_date'] != "")
                // $grad_date = date_format(new DateTime($_POST['graduation_date']),"Y-m-d");
                $tgsmodel = new TutorGroupsStudents();
                $tgsmodel->institution_id = $instid;
                $tgsmodel->tutor_group_id = $tutorid;
                $tgsmodel->graduation_date = $grad_date;
                $tgsmodel->student_id = $umodel->id;
                $tgsmodel->created_at = $now->format('Y-m-d H:i:s');
                $tgsmodel->save();
                $this->_sendResponsewithMessage(200, sprintf("Successfully added student", ""));
            }
        } else {
            $usrerr = array();
             $usercolumns = Users::model()->getAttributes();
            $profilecolumns = Profiles::model()->getAttributes();

            foreach ($usercolumns as $key => $val) {
                if ($umodel->getError($key) != "") {
                    $usrerr[$key] = $umodel->getError($key);
                }
            }
          
            foreach ($profilecolumns as $key => $val) {
                if ($pmodel->getError($key) != null)
                    $usrerr[$key] = $pmodel->getError($key);
            }

            $this->_sendResponse(512, $usrerr);
        }
    }


    public function actionoldeditstudent() {
        $this->apikeyAuth();

                  // if (!$this->_checkAuth())
                  //  $this->_sendResponse(405, array());

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $tgsmodel = TutorGroupsStudents::model()->findByPk($id);
        if ($id != $tgsmodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        } else {
            $_POST = $_POST;
        }

        $usermodel = Users::model()->findByPk($tgsmodel->student_id);
        $tgmodel = TutorGroups::model()->findByPk($tgsmodel->tutor_group_id);
        $model = Institutions::model()->findByPk($tgsmodel->institution_id);
        $usermodel = Users::model()->findByPk($tgsmodel->student_id);
        $pwd = $usermodel->password;

        $id = $tgsmodel->institution_id;
        $now = new DateTime();
        $tutorid = $tgsmodel->tutor_group_id;

        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                $usermodel->$var = $value;
            } else if ($tgsmodel->hasAttribute($var)) {
                $tgsmodel->$var = $value;
            } else if ($var == 'tutorgroup_id') {
                
            } else {
                $this->_sendResponse(400, array());
            }
        }
        $tutorid = $_POST['tutorgroup_id'];
        $instid = $_POST['institution_id'];


        if (trim($_POST['password']) == "")
            $usermodel->password = $pwd;
        else
            $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $usermodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($usermodel->save()) {
            $grad_date = null;
            if ($_POST['graduation_date'] != "") {
                $d = str_replace('/', '-', $_POST["graduation_date"]);
                $grad_date = date('Y-m-d', strtotime($d));
            }
            $tgsmodel->institution_id = $instid;
            $tgsmodel->tutor_group_id = $tutorid;
            $tgsmodel->graduation_date = $grad_date;
            $tgsmodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($tgsmodel->save()) {
                $this->_sendResponsewithMessage(200, sprintf("Successfully updated student", ""));
            } else {
                $this->_sendResponse(500, array());
            }
        }
    }

     public function actioneditstudent() {
        // $this->apikeyAuth();

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $tgsmodel = TutorGroupsStudents::model()->findByPk($id);
        

        if ($id != $tgsmodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $tutorid = $tgsmodel->tutor_group_id;
        $id = $tgsmodel->institution_id;

        $usermodel = Users::model()->findByPk($tgsmodel->student_id);
        $tgmodel = TutorGroups::model()->findByPk($tgsmodel->tutor_group_id);
        $model = Institutions::model()->findByPk($tgsmodel->institution_id);
        $usermodel = Users::model()->findByPk($tgsmodel->student_id);
        $pwd = $usermodel->password;

        $now = new DateTime();

        $usertable = Yii::app()->db->schema->getTable(Users::model()->tableName());
        $tutortable = Yii::app()->db->schema->getTable(TutorGroups::model()->tableName());
        $instable = Yii::app()->db->schema->getTable(Institutions::model()->tableName());


        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                $usermodel->$var = $value;
            } else if ($tgsmodel->hasAttribute($var)) {
                $tgsmodel->$var = $value;
            } else if ($var == 'tutorgroup_id') {
                
            } else if ($tgmodel->hasAttribute($var)) {
                $tgmodel->$var = $value;
            } elseif (!isset($usertable->columns[$var])) {
                $this->_sendResponse(511, array(), 'text/html', $var);
            } elseif (!isset($tutortable->columns[$var])) {
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if ($var == 'tutorgroup_id') {
                
            } elseif (!isset($instable->columns[$var])) {
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else {
                $this->_sendResponse(400, array());
            }
        }
        $tutorid = $_POST['tutorgroup_id'];
        $instid = $_POST['institution_id'];
        $instuitionname = $_POST['name'];


        if (trim($_POST['password']) == "")
            $usermodel->password = $pwd;
        else
            $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $usermodel->updated_at = $now->format('Y-m-d H:i:s');

        $valid = $usermodel->validate();
        $valid = $tgmodel->validate() && $valid;
       
        if ($valid) {


            if ($usermodel->save()) {
                $grad_date = null;
                if ($_POST['graduation_date'] != "") {
                    $d = str_replace('/', '-', $_POST["graduation_date"]);
                    $grad_date = date('Y-m-d', strtotime($d));
                }
                $tgsmodel->institution_id = $instid;
                $tgsmodel->tutor_group_id = $tutorid;
                $tgsmodel->graduation_date = $grad_date;
                $tgsmodel->updated_at = $now->format('Y-m-d H:i:s');
                $tgsmodel->save();
                $tgmodel->name = $instuitionname;
                $tgmodel->save();
                $this->_sendResponsewithMessage(200, sprintf("Successfully updated student", ""));
            }
        } else {
            $usrerr = array();
            $usercolumns = Users::model()->getAttributes();
            $tutorcolumns = TutorGroups::model()->getAttributes();

            foreach ($usercolumns as $key => $val) {
                if ($usermodel->getError($key) != "") {
                    $usrerr[$key] = $usermodel->getError($key);
                }
            }

            foreach ($tutorcolumns as $key => $val) {
                if ($tgmodel->getError($key) != null)
                    $usrerr[$key] = $tgmodel->getError($key);
            }
            $this->_sendResponse(512, $usrerr);
        }
    }

    /* Student
      delete */

    public function actiondeletestudent() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $deletedraftmsg = TutorGroupsStudents::model();
        $cri = new CDbCriteria();
        $cri->condition = "student_id = " . $_GET['id'];
        TutorGroupsStudents::model()->deleteAll($cri);
        $deletedraftmsg = Users::model();
        $cri = new CDbCriteria();
        $cri->condition = "id = " . $_GET['id'];
        Users::model()->deleteAll($cri);
        $this->_sendResponse(200, "Deleted Successfully");
    }

    ///   /    get student
    public function actiongetstudent() {
        $this->apikeyAuth();
        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $model = Users::model()->findByPk($_GET['id']);

        if($model == null){
             $this->_sendResponse(500, array());
        }
        $cri = new CDbCriteria();
        $cri->alias = 'i';
        $cri->select = "i.id,i.forenames, i.surname, t.teacher_id, t.name,t.graduation_date,t. created_at";
        $cri->join = "left outer join cv16_tutor_groups t on t.teacher_id = i.id";
        $cri->condition = "t.teacher_id =" . $_GET['id'];
        
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }

    /// Get teacher
    public function actiongeteacher() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
            $id = $_GET['id'];
        $model = InstitutionUsers::model()->findByPk($_GET['id']);
        if($model == null) {
             $this->_sendResponse(505, array());
        }
        $cri = new CDbCriteria();
        $cri->alias = 'i';
        $cri->select = "i.id,u.forenames,t.account_limit,u.surname, t.teacher_id, t.name,t.graduation_date,t. created_at";

        $cri->join = "left outer join cv16_users u on u.id = i.user_id

                      left outer join cv16_tutor_groups t on t.institution_id = i.id";

        $cri->condition = "i.institution_id =" . $_GET['id'];

        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }

    ///   /    get student
    public function actiongetclass() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $model = TutorGroups::model();
       
        $cri = new CDbCriteria();
        $cri->alias = 'i';
        $cri->select = "i.name,i.account_limit, t.forenames,i.graduation_date,i. created_at";

        $cri->join = "left outer join cv16_users t on t.id = i.teacher_id";

        $cri->condition = "i.institution_id =" . $_GET['id'];

        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }

    ///   /    fetch class
    public function actionfetchclass() {

        $this->apikeyAuth();

        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $model = TutorGroups::model();
      
        $cri = new CDbCriteria();

        $cri->select = "*";
        $cri->condition = "institution_id =" . $_GET['id'];

        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }

    ///   /    fetch student
    public function actionfetchstudent() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $model = TutorGroupsStudents::model();
        if($model == null) {
             $this->_sendResponse(500, array());
        }
        $cri = new CDbCriteria();

        $cri->alias = 'i';
        $cri->select = "t.forenames,t.id";

        $cri->join = "left outer join cv16_users t on t.id = i.student_id";

        $cri->condition = "i.institution_id =" . $_GET['id'];

        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }

    ///   /    fetch teacher
    public function actionfetchteacher() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $model = TutorGroupsTeachers::model();
        if($model == null) {
             $this->_sendResponse(500, array());
        }
        $cri = new CDbCriteria();

        $cri->alias = 'i';
        $cri->select = "t.forenames,t.id";

        $cri->join = "left outer join cv16_users t on t.id = i.teacher_id";

        $cri->condition = "i.institution_id =" . $_GET['id'];

        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();


        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }

    /* Employer inbox */

//    public function actionsendMessage() {
//        if (!isset($_GET['id']) || $_GET['id'] == 0)
//            $this->_sendResponse(400, array());
//        $id = $_GET['id'];
//
//        if (json_decode(file_get_contents("php://input"), true)) {
//            $_POST = json_decode(file_get_contents("php://input"), true);
//        }
//
//        $now = new DateTime();
//        $conmodel = new Conversations();
//        $conmodel->subject = $_POST['subject'];
//        $conmodel->created_at = $now->format('Y-m-d H:i:s');
//        $conmodel->updated_at = $now->format('Y-m-d H:i:s');
//        if ($conmodel->save()) {
//            $usermodel = Users::model()->findByPk($id);
//            $conmsgmodel = new ConversationMessages();
//            $conmsgmodel->conversation_id = $conmodel->id;
//            $conmsgmodel->user_id = $usermodel->id;
//            $conmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
//            $conmsgmodel->message = $_POST['message'];
//            $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
//            $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
//            if ($conmsgmodel->save()) {
//                $conmemmodel_sender = new ConversationMembers();
//                $conmemmodel_sender->conversation_id = $conmodel->id;
//                $conmemmodel_sender->user_id = $usermodel->id;
//                $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
//                $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
//                $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
//                $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
//                if ($conmemmodel_sender->save()) {
//                    $usermodel1 = Users::model()->findByPk($_POST['user_id']);
//                    $conmemmodel_rec = new ConversationMembers();
//                    $conmemmodel_rec->conversation_id = $conmodel->id;
//                    $conmemmodel_rec->user_id = $_POST['user_id'];
//                    $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
//                    $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
//                    $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
//                    if ($conmemmodel_rec->save()) {
//                        $actmodel_sender = new Activities();
//                        $actmodel_sender->user_id = $usermodel->id;
//                        $actmodel_sender->type = "message";
//                        $actmodel_sender->model_id = $conmsgmodel->id;
//                        $actmodel_sender->model_type = "ConversationMessage";
//                        $actmodel_sender->subject = "You sent a message in conversation: " . $_POST['subject'];
//                        $actmodel_sender->message = $_POST['message'];
//                        $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
//                        $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
//                        if ($actmodel_sender->save()) {
//                            $actmodel_rec = new Activities();
//                            $actmodel_rec->user_id = $_POST['user_id'];
//                            $actmodel_rec->type = "message";
//                            $actmodel_rec->model_id = $conmsgmodel->id;
//                            $actmodel_rec->model_type = "ConversationMessage";
//                            $actmodel_rec->subject = "You received a message in conversation: " . $_POST['subject'];
//                            $actmodel_rec->message = $_POST['message'];
//                            $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
//                            $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
//                            if ($actmodel_rec->save()) {
//                                $this->_sendResponse(200, array());
//                            } else {
//                                $this->_sendResponse(507, array());
//                            }
//                        }
//                    }
//                }
//            }
//        } else {
//            $this->_sendResponse(507, array());
//        }
//    }
    /* Employer inbox */


//  ///   /    get message
//  public function actiongetmessage() {
//    if (!isset($_GET['id']) || $_GET['id'] == 0)
//        $this->_sendResponse(404, array());
//
//    $model = ConversationMessages::model()->findByAttributes(array('user_id' =>  $_GET['id']));
//   $cri = new CDbCriteria();
//   // $cri->alias ='i';
//        $cri->select ="*";
//
//      //  $cri->join = "left outer join cv16_users t on t.id = i.teacher_id";
//
//     
//        $cri->condition = "user_id =".$_GET['id'];
//
//   
//    $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();
//
//    if (empty($users)) {
//        $this->_sendResponse(404, array());
//    } else {
//        $this->_sendResponse(200, $users);
//    }
//}
    ///   /    get counts
    public function actiongetcounts() {


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $model = Institutions::model()->findByPk($id);
        if($model == null) {
             $this->_sendResponse(500, array());
        }
        $cri = new CDbCriteria();
        $cri->alias = 'c';
        $cri->select = "c.num_pupils,(SELECT count(*) FROM cv16_tutor_groups_students WHERE institution_id = c.id) as num_count";
        $cri->condition = "c.id =" . $_GET['id'];
        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
//     $modelt =  TutorGroupsStudents::model();
//     $cri->select ="institution_id";
//     $cri->condition = "institution_id =".$_GET['id'];
//     $users = $modelt->getCommandBuilder()->createFindCommand($modelt->tableSchema, $cri)->queryAll();
//    count($modelt);
    }

///update school
    public function actionupdateschool() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $userhobmodel = Institutions::model()->findByPk($id);
        if ($id != $userhobmodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $put_vars = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }

        $now = new DateTime();

        foreach ($put_vars as $var => $value) {
            if ($userhobmodel->hasAttribute($var))
                $userhobmodel->$var = $value;
            else
                $this->_sendResponse(500, array());
        }
        $userhobmodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($userhobmodel->save()) {

            $this->_sendResponsewithMessage(200, sprintf("Successfully  updated", ""));
        } else {
            $this->_sendResponse(500, array());
        }
    }

    public function actionoldcreaterecruitment() {


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $usermodel = new Users();
        $profilemodel = new Profiles();
        $addressmodel = new Addresses();
        $agencymodel = new Agency();
        $now = new DateTime();

        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                if ($var == "email") {
                    $mmodel = Users::model()->findByAttributes(array('email' => $value));
                    if ($mmodel != null) {
                        $this->_sendResponse(502, array());
                        die();
                    }
                }
                $usermodel->$var = $value;
            } else if ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else if ($addressmodel->hasAttribute($var)) {
                $addressmodel->$var = $value;
            } else if ($agencymodel->hasAttribute($var)) {
                $agencymodel->$var = $value;
            } else if ($var == 'ispremium') {
                // $this->_sendResponse(400, array());
            } else {
                $this->_sendResponse(500, array());
            }
        }

        $usermodel->location = $_POST['county'] . ($_POST['county'] != '' ? ', ' . $_POST['country'] : '');
        if (isset($_POST['latitude']) && $_POST['latitude'])
            $usermodel->location_lat = $_POST['latitude'];
        if (isset($_POST['longitude']) && $_POST['longitude'])
            $usermodel->location_lng = $_POST['longitude'];
        // Check if using free trial then set membership trial date
        if (isset($_POST['ispremium']) && $_POST['ispremium'])
            $usermodel->trial_ends_at = Carbon::now()->addYear()->toDateTimeString();
        $usermodel->type = 'Agency';
        $usermodel->created_at = $now->format('Y-m-d H:i:s');
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        if ($usermodel->save()) {
            $profilemodel->owner_type = "Agency";
            $profilemodel->owner_id = $usermodel->id;
            $profilemodel->created_at = $usermodel->created_at;
            $profilemodel->type = $usermodel->type;
            if ($profilemodel->save()) {
                $addressmodel->model_id = $usermodel->id;
                $addressmodel->model_type = "Agency";
                $addressmodel->created_at = $now->format('Y-m-d H:i:s');
                if ($addressmodel->save()) {
                    $agencymodel->user_id = $usermodel->id;
                    $agencymodel->email = $_POST['email'];
                    $agencymodel->created_at = $now->format('Y-m-d H:i:s');

                    if ($agencymodel->save()) {
                        $cri = new CDbCriteria();
                        $cri->select = 'id,forenames,surname,email';
                        $cri->condition = "id = " . $usermodel->id;
                        $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                        $this->_sendResponse(200, $users);
                    } else {
                        echo var_dump($agencymodel->getErrors());
                    }
                } else {
                    echo var_dump($addressmodel->getErrors());
                }
            } else {
                echo var_dump($profilemodel->getErrors());
            }
        } else {
            $this->_sendResponse(500, array());
        }
    }


    public function actioncreaterecruitment() {
        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());
        $api_key = $this->generateApiKey();
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $usermodel = new Users();
        $profilemodel = new Profiles();
        $addressmodel = new Addresses();
        $agencymodel = new Agency();
        $now = new DateTime();

        $usertable = Yii::app()->db->schema->getTable(Users::model()->tableName());
        $profiletable = Yii::app()->db->schema->getTable(Profiles::model()->tableName());
        $addresstable = Yii::app()->db->schema->getTable(Addresses::model()->tableName());
        $agencytable = Yii::app()->db->schema->getTable(Agency::model()->tableName());

        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                if ($var == "email") {
                    $mmodel = Users::model()->findByAttributes(array('email' => $value));
                    if ($mmodel != null) {
                        $this->_sendResponse(502, array());
                        die();
                    }
                }
                $usermodel->$var = $value;
            } else if ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else if ($addressmodel->hasAttribute($var)) {
                $addressmodel->$var = $value;
            } else if ($agencymodel->hasAttribute($var)) {
                $agencymodel->$var = $value;
            } else if ($var == 'ispremium') {
                // $this->_sendResponse(400, array());
            } else if (!isset($usertable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($profiletable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($addresstable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            }else if (!isset($agencytable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else {
                $this->_sendResponse(500, array());
            }
        }
        if (isset($_POST['county']) && $_POST['country'])
            $usermodel->location = $_POST['county'] . ($_POST['county'] != '' ? ', ' . $_POST['country'] : '');
        if (isset($_POST['latitude']) && $_POST['latitude'])
            $usermodel->location_lat = $_POST['latitude'];
        if (isset($_POST['longitude']) && $_POST['longitude'])
            $usermodel->location_lng = $_POST['longitude'];
        // Check if using free trial then set membership trial date
        if (isset($_POST['ispremium']) && $_POST['ispremium'])
            $usermodel->trial_ends_at = Carbon::now()->addYear()->toDateTimeString();
        $usermodel->type = 'Agency';
        $usermodel->created_at = $now->format('Y-m-d H:i:s');
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $usermodel->api_key = $api_key;
        
        $profilemodel->slug = $this->getValidatedSlug($_POST['slug']);

        $valid = $usermodel->validate();
        $valid = $addressmodel->validate() && $valid;
        $valid = $profilemodel->validate() && $valid;


        if ($valid) {
            if ($usermodel->save()) {
                $profilemodel->owner_type = "Agency";
                $profilemodel->owner_id = $usermodel->id;
                $profilemodel->slug = $this->getValidatedSlug($_POST['slug']);
                $profilemodel->created_at = $usermodel->created_at;
                $profilemodel->type = $usermodel->type;
                if ($profilemodel->save()) {
                    $addressmodel->model_id = $usermodel->id;
                    $addressmodel->model_type = "Agency";
                    $addressmodel->created_at = $now->format('Y-m-d H:i:s');
                    if ($addressmodel->save()) {
                        $agencymodel->user_id = $usermodel->id;
                        $agencymodel->email = $_POST['email'];
                        $agencymodel->created_at = $now->format('Y-m-d H:i:s');
                        if ($agencymodel->save()) {
                            $cri = new CDbCriteria();
                            $cri->alias = 'rec';
                            $cri->select = 'rec.id,rec.forenames,rec.surname,rec.email,rec.mobile,p.slug,rec.api_key';
                            $cri->join = 'left outer join cv16_profiles p on p.owner_id = rec.id';
                            $cri->condition = "rec.id = " . $usermodel->id;                           
                            $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                            $this->_sendResponse(200, $users);
                        }
                    }
                }
            }
        } else {
            $usrerr = array();
            $usercolumns = Users::model()->getAttributes();
            $addresscolumns = Addresses::model()->getAttributes();
            $profilecolumns = Profiles::model()->getAttributes();
            $agencycolumns = Agency::model()->getAttributes();

            foreach ($usercolumns as $key => $val) {
                if ($usermodel->getError($key) != "") {
                    $usrerr[$key] = $usermodel->getError($key);
                }
            }
            foreach ($addresscolumns as $key => $val) {
                if ($addressmodel->getError($key) != null)
                    $usrerr[$key] = $addressmodel->getError($key);
            }
            foreach ($profilecolumns as $key => $val) {
                if ($profilemodel->getError($key) != null)
                    $usrerr[$key] = $profilemodel->getError($key);
            }
            foreach ($agencycolumns as $key => $val) {
                if ($agencymodel->getError($key) != null)
                    $usrerr[$key] = $agencymodel->getError($key);
            }
            $this->_sendResponse(512, $usrerr);
        }
    }




    public function actionoldeditrecruitment() {
        $this->apikeyAuth();

                  // if (!$this->_checkAuth())
                  //  $this->_sendResponse(405, array());

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $insmodel = Agency::model()->findByPk($id);
        if ($id != $insmodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $put_vars = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }

        //  $pwd = $usermodel->password;

        $now = new DateTime();

        $usermodel = Users::model()->findByPk($insmodel->user_id);
        $pmodel = Profiles::model()->findByAttributes(array('owner_id' => $insmodel->user_id));
        $profilemodel = Profiles::model()->findByPk($pmodel->id);
        $cri = new CDbCriteria();
        $cri->condition = "model_id = ".$insmodel->user_id." and model_type like '%agency%' and deleted_at is null";
        
        $addressmodel = Addresses::model()->find($cri);

        foreach ($put_vars as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                if ($var == "password" && $value == "") {
                    $usermodel->password = password_hash($value, 1, ['cost' => 10]);
                } else if ($var == "password" && $value != "") {
                    
                } else {
                    $usermodel->$var = $value;
                }
            } else if ($insmodel->hasAttribute($var)) {
                $insmodel->$var = $value;
            } else if ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else if ($insmodel->hasAttribute($var)) {
                $insmodel->$var = $value;
            } else if ($addressmodel->hasAttribute($var)) {
                $addressmodel->$var = $value;
            } else {

                $this->_sendResponse(400, array());
            }
        }

        $insmodel->updated_at = $now->format('Y-m-d H:i:s');
        $insmodel->save();
        $addressmodel->updated_at = $now->format('Y-m-d H:i:s');
        $addressmodel->save();

        $usermodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($usermodel->save()) {
            $profilemodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($profilemodel->save()) {
                $this->_sendResponsewithMessage(200, sprintf("Successfully updated account", ""));
            } else {
                $this->_sendResponse(500, array());
            }
        }
    }


     public function actioneditrecruitment() {
        $this->apikeyAuth();

        // if (!$this->_checkAuth())
        //  $this->_sendResponse(405, array());

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $insmodel = Agency::model()->findByPk($id);
        if ($id != $insmodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $put_vars = json_decode(file_get_contents("php://input"), true);
        } else {
            $put_vars = $_POST;
        }
        $now = new DateTime();
        $usermodel = Users::model()->findByPk($insmodel->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $insmodel->user_id));
   //     $profilemodel = Profiles::model()->findByPk($pmodel->id);
        $cri = new CDbCriteria();
        $cri->condition = "model_id = " . $insmodel->user_id . " and model_type like '%agency%' and deleted_at is null";
        $addressmodel = Addresses::model()->find($cri);

        $usertable = Yii::app()->db->schema->getTable(Users::model()->tableName());
        $profiletable = Yii::app()->db->schema->getTable(Profiles::model()->tableName());
        $addresstable = Yii::app()->db->schema->getTable(Addresses::model()->tableName());

        foreach ($put_vars as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                if ($var == "password" && $value == "") {
                    $usermodel->password = password_hash($value, 1, ['cost' => 10]);
                } else if ($var == "password" && $value != "") {
                    
                } else {
                    $usermodel->$var = $value;
                }
            } else if ($insmodel->hasAttribute($var)) {
                $insmodel->$var = $value;
            } else if ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else if ($addressmodel->hasAttribute($var)) {
                $addressmodel->$var = $value;
            } else if (!isset($usertable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($profiletable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($addresstable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else {
                $this->_sendResponse(400, array());
            }
        }

        $insmodel->updated_at = $now->format('Y-m-d H:i:s');
        $insmodel->save();
        $addressmodel->updated_at = $now->format('Y-m-d H:i:s');
        $addressmodel->save();
        $usermodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($usermodel->save()) {
            $profilemodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($profilemodel->save()) {
                $this->_sendResponsewithMessage(200, sprintf("Successfully updated account", ""));
            }
        } else {
            $usrerr = array();
            $usercolumns = Users::model()->getAttributes();
            $addresscolumns = Addresses::model()->getAttributes();
            $profilecolumns = Profiles::model()->getAttributes();
           

            foreach ($usercolumns as $key => $val) {
                if ($usermodel->getError($key) != "") {
                    $usrerr[$key] = $usermodel->getError($key);
                }
            }
            foreach ($addresscolumns as $key => $val) {
                if ($addressmodel->getError($key) != null)
                    $usrerr[$key] = $addressmodel->getError($key);
            }
            foreach ($profilecolumns as $key => $val) {
                if ($profilemodel->getError($key) != null)
                    $usrerr[$key] = $profilemodel->getError($key);
            }
           
            $this->_sendResponse(512, $usrerr);
        }
    }



    ///   /    get recruitmentagency
    public function actiongetrecruitmentagency() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $model = Agency::model()->findByAttributes(array('user_id' => $_GET['id']));
        if($model == null){
            $this->_sendResponse(500, array());
        }
        $cri = new CDbCriteria();
        $cri->alias = 'i';
        $cri->select = "i.id as agency_id, i.name ,m.file_name,CONCAT('https://cvvid.com/images/media/',p.photo_id,'/',m.file_name) as photo,CONCAT('https://cvvid.com/images/defaultprofile.jpg') as photodefault,CONCAT('https://cvvid.com/images/media/',p.cover_id,'/conversions/cover.jpg') as cover,CONCAT('https://cvvid.com/images/CoverPlaceholder.jpg') as coverdefault,u.forenames,u.surname,u.email,u.id as user_id,u.stripe_active,u.current_job,u.type,u.location,p.slug,p.photo_id,p.cover_id,p.id as profile_id,a.address,a.town,a.county,a.country,a.postcode";
        $cri->join = "left outer join cv16_profiles p on p.owner_id = i.user_id
                  left outer join cv16_users u on u.id = i.user_id
                  left outer join cv16_media m on m.id = p.photo_id
                  left outer join cv16_addresses a on a.model_id = i.user_id";


        $cri->condition = "i.user_id = " . $_GET['id'] . "";

        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(507, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }

/// create recruitment candidate


    public function actionoldcreatereccandidate() {

         if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $model = Agency::model()->findByPk($_GET['id']);
       if($model == null) {
         $this->_sendResponse(500, array());
       }
        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $usermodel = new Users();
        $profilemodel = new Profiles();
        $addressmodel = new Addresses();
        $agencymodel = new AgencyCandidates();
        $now = new DateTime();
        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                if ($var == "email") {
                    $mmodel = Users::model()->findByAttributes(array('email' => $value));
                    if ($mmodel != null) {
                        $this->_sendResponse(502, array());
                        die();
                    }
                }
                $usermodel->$var = $value;
            } else if ($addressmodel->hasAttribute($var)) {
                $addressmodel->$var = $value;
            } else if ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else if ($agencymodel->hasAttribute($var)) {
                $agencymodel->$var = $value;
            } else if ($var == 'ispremium') {
                //  $this->_sendResponse(400, array());
            } else {
                $this->_sendResponse(500, array());
            }
        }
        $usermodel->location = $_POST['county'] . ($_POST['county'] != '' ? ', ' . $_POST['country'] : '');
        if (isset($_POST['latitude']) && $_POST['latitude'])
            $usermodel->location_lat = $_POST['latitude'];
        if (isset($_POST['longitude']) && $_POST['longitude'])
            $usermodel->location_lng = $_POST['longitude'];
        // Check if using free trial then set membership trial date
        if (isset($_POST['ispremium']) && $_POST['ispremium'])
            $usermodel->trial_ends_at = Carbon::now()->addYear()->toDateTimeString();
        $usermodel->type = 'candidate';
        $usermodel->created_at = $now->format('Y-m-d H:i:s');
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        if ($usermodel->save()) {
            $addressmodel->model_id = $usermodel->id;
            $addressmodel->model_type = "Candidate";
            $addressmodel->created_at = $now->format('Y-m-d H:i:s');
            if ($addressmodel->save()) {
                $profilemodel->owner_type = "Candidate";
                $profilemodel->owner_id = $usermodel->id;
                $profilemodel->slug = $usermodel->forenames . '' . $usermodel->surname;
                $profilemodel->created_at = $usermodel->created_at;
                $profilemodel->type = $usermodel->type;
                if ($profilemodel->save()) {
                    $mmodel = AgencyCandidates::model()->findByAttributes(array('user_id' => $value));
                    $agencymodel->user_id = $usermodel->id;
                     $agencymodel->agency_id = $model->id;
                    $agencymodel->created_at = $now->format('Y-m-d H:i:s');

                    if ($agencymodel->save()) {
                        $cri = new CDbCriteria();
                        $cri->select = 'id,forenames,surname,email,mobile';
                        $cri->condition = "id = " . $usermodel->id;
                        $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                        $this->_sendResponse(200, $users);
                    } else {
                        echo var_dump($agencymodel->getErrors());
                    }
                } else {
                    $this->_sendResponsewithMessage(200, sprintf("choose different name", ""));
                }
            } else {
                echo $addressmodel->getErrors();
            }
        } else {
            echo var_dump($usermodel->getErrors());
            $this->_sendResponse(500, array());
        }
    }


    public function actioncreatereccandidate() {

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $model = Agency::model()->findByPk($_GET['id']);
        if ($model == null) {
            $this->_sendResponse(500, array());
        }
        $api_key = $this->generateApiKey();
        
        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $usermodel = new Users();
        $profilemodel = new Profiles();
        $addressmodel = new Addresses();
        $agencymodel = new AgencyCandidates();
        $now = new DateTime();

        $usertable = Yii::app()->db->schema->getTable(Users::model()->tableName());
        $profiletable = Yii::app()->db->schema->getTable(Profiles::model()->tableName());
        $addresstable = Yii::app()->db->schema->getTable(Addresses::model()->tableName());
        $agencytable = Yii::app()->db->schema->getTable(AgencyCandidates::model()->tableName());

        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                if ($var == "email") {
                    $mmodel = Users::model()->findByAttributes(array('email' => $value));
                    if ($mmodel != null) {
                        $this->_sendResponse(502, array());
                        die();
                    }
                }
                $usermodel->$var = $value;
            } else if ($addressmodel->hasAttribute($var)) {
                $addressmodel->$var = $value;
            } else if ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else if ($agencymodel->hasAttribute($var)) {
                $agencymodel->$var = $value;
            } else if ($var == 'ispremium') {
                //  $this->_sendResponse(400, array());
            } else if (!isset($usertable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($profiletable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($addresstable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($agencytable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else {
                $this->_sendResponse(500, array());
            }
        }
        if (isset($_POST['county']) && $_POST['country'])
            $usermodel->location = $_POST['county'] . ($_POST['county'] != '' ? ', ' . $_POST['country'] : '');
        if (isset($_POST['latitude']) && $_POST['latitude'])
            $usermodel->location_lat = $_POST['latitude'];
        if (isset($_POST['longitude']) && $_POST['longitude'])
            $usermodel->location_lng = $_POST['longitude'];
        // Check if using free trial then set membership trial date
        if (isset($_POST['ispremium']) && $_POST['ispremium'])
            $usermodel->trial_ends_at = Carbon::now()->addYear()->toDateTimeString();
        $usermodel->type = 'candidate';
        $usermodel->created_at = $now->format('Y-m-d H:i:s');
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);
        $usermodel->api_key = $api_key;

       // $profilemodel->slug = $this->getValidatedSlug($usermodel->forenames . "-" . $usermodel->surname);
        $profilemodel->slug = $this->getValidatedSlug($_POST['slug']);
        $valid = $usermodel->validate();
        $valid = $addressmodel->validate() && $valid;
        $valid = $profilemodel->validate() && $valid;
//        $valid = $agencymodel->validate() && $valid;
        if ($valid) {
            if ($usermodel->save()) {
                $addressmodel->model_id = $usermodel->id;
                $addressmodel->model_type = "Candidate";
                $addressmodel->created_at = $now->format('Y-m-d H:i:s');
                if ($addressmodel->save()) {
                    $profilemodel->owner_type = "Candidate";
                    $profilemodel->owner_id = $usermodel->id;
                    $profilemodel->slug = $this->getValidatedSlug($_POST['slug']);
                    $profilemodel->created_at = $usermodel->created_at;
                    $profilemodel->type = $usermodel->type;
                    if ($profilemodel->save()) {
                        $mmodel = AgencyCandidates::model()->findByAttributes(array('user_id' => $value));
                        $agencymodel->user_id = $usermodel->id;
                        $agencymodel->agency_id = $model->id;
                        $agencymodel->created_at = $now->format('Y-m-d H:i:s');

                        if ($agencymodel->save()) {
                            $cri = new CDbCriteria();
                            $cri->alias = 'reccan';
                            $cri->select = 'reccan.id,reccan.forenames,reccan.surname,reccan.email,reccan.mobile,p.slug,reccan.api_key';
                            $cri->join = 'left outer join cv16_profiles p on p.owner_id = reccan.id';
                            $cri->condition = "reccan.id = " . $usermodel->id;
                            $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                            $this->_sendResponse(200, $users);
                        }
                    }
                }
            }
        } else {
             $usrerr = array();
            $usercolumns = Users::model()->getAttributes();
            $addresscolumns = Addresses::model()->getAttributes();
            $profilecolumns = Profiles::model()->getAttributes();
            $agencycolumns = Agency::model()->getAttributes();

            foreach ($usercolumns as $key => $val) {
                if ($usermodel->getError($key) != "") {
                    $usrerr[$key] = $usermodel->getError($key);
                }
            }
            foreach ($addresscolumns as $key => $val) {
                if ($addressmodel->getError($key) != null)
                    $usrerr[$key] = $addressmodel->getError($key);
            }
            foreach ($profilecolumns as $key => $val) {
                if ($profilemodel->getError($key) != null)
                    $usrerr[$key] = $profilemodel->getError($key);
            }
            
            $this->_sendResponse(512, $usrerr);
        }
    }


    

///   /    get recruitment candidate
    public function actiongetreccandidate() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $model = Agency::model()->findByPk($_GET['id']);
        if($model==null){
            $this->_sendresponse(500,array());
        }
        $cri = new CDbCriteria();
        $cri->alias = 'i';
        $cri->select = "c.id  as candidate_id,u.id,u.forenames, u.surname, u.email,u.current_job";
        $cri->join = "left outer join cv16_agency_candidates c on c.agency_id = i.id
                  left outer join cv16_users u on u.id = c.user_id";
        $cri->condition = "i.id =" . $_GET['id'];
        // echo var_dump($cri);
        // die();

        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }

    /* delete rec candidate */

    public function actiondeletereccandidate() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $deletedraftmsg = AgencyCandidates::model();
        $cri = new CDbCriteria();
        $cri->condition = "user_id = " . $_GET['id'];
        AgencyCandidates::model()->deleteAll($cri);
        $deletedraftmsg = Users::model();
        $cri = new CDbCriteria();
        $cri->condition = "id = " . $_GET['id'];
        Users::model()->deleteAll($cri);
        $this->_sendResponse(200, "Deleted Successfully");
    }

///   /    fetch  recruitment staff
    public function actionfetchrecstaff() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $model = AgencyStaff::model()->findByAttributes(array('agency_id' => $_GET['id']));
        $cri = new CDbCriteria();
        $cri->alias = 'i';
        $cri->select = "i.user_id,u.forenames, u.surname, u.email,u.current_job";

        $cri->join = "left outer join cv16_users u on u.id = i.user_id";

        $cri->condition = "i.agency_id =" . $_GET['id'];
        // echo var_dump($cri);
        // die();

        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }

/// create assign staff 

    public function actionassignstaff() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        $id = isset($_GET['id']) ? $_GET['id'] : null;
        $candimodel = AgencyCandidates::model()->findByPk($id);
        if ($id != $candimodel->id)
            $this->_sendResponse(404, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $staffcanmodel = AgencyStaffCandidates::model()->findByAttributes(array('agency_candidate_id' => $_GET['id']));

        if ($staffcanmodel == null) {
            $staffcanmodel = new AgencyStaffCandidates();
            // $staffcanmodel->userid = $_GET['id'];   
        }

        $now = new DateTime();
        foreach ($_POST as $var => $value) {
            if ($staffcanmodel->hasAttribute($var)) {
                // if ($var == "email") {
                //     $mmodel = Users::model()->findByAttributes(array('email' => $value));
                //     if ($mmodel != null) {
                //         $this->_sendResponse(502, array());
                //         die();
                //     }
                // }
                $staffcanmodel->$var = $value;
            } else {
                $this->_sendResponse(500, array());
            }
        }

        $staffcanmodel->agency_id = $candimodel->agency_id;
        $staffcanmodel->agency_candidate_id = $candimodel->id;
        $staffcanmodel->created_at = $now->format('Y-m-d H:i:s');
        if ($staffcanmodel->save()) {
            $cri = new CDbCriteria();
            $cri->select = 'id,staff_id';
            $cri->condition = "agency_candidate_id = " . $candimodel->id;
            $users = $staffcanmodel->getCommandBuilder()->createFindCommand($staffcanmodel->tableSchema, $cri)->queryAll();
            $this->_sendResponse(200, $users);
        } else {
            // echo var_dump($usermodel->getErrors());
            $this->_sendResponse(500, array());
        }
    }

///   /    get recruitment staff
    public function actiongetstaff() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $model = Agency::model()->findByPk($_GET['id']);
        $cri = new CDbCriteria();
        $cri->alias = 'i';
        $cri->select = "s.id  as staff_id,u.id,u.forenames, u.surname, u.email,
    (SELECT count(*) FROM cv16_agency_staff_candidates WHERE staff_id = s.user_id) as num_count";

        $cri->join = "left outer join cv16_agency_staff s on s.agency_id = i.id
                  left outer join cv16_users u on u.id = s.user_id";

        $cri->condition = "i.id =" . $_GET['id'];
        // echo var_dump($cri);
        // die();

        $users = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($users)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $users);
        }
    }
    
    public function actionassignjob()
    {
        $jobid = $_GET['id'];
        $empuser = $_POST['userid'];
        $now =  new DateTime();
        $jmodel = Jobs::model()->findByPk($jobid);
        Jobs::model()->updateByPk($jobid,array('user_id'=>$empuser,'updated_at'=>$now->format('Y-m-d H:i:s')));
    }
    
    public function actiondeletejob()
    {
        $jobid = $_GET['id'];
        
        $now = new DateTime();
            
        $cri = new CDbCriteria();
        $cri->condition = "id in (".$jobid.")";
        Jobs::model()->deleteAll($cri);

        $cri1 = new CDbCriteria();
        $cri1->condition = "job_id in (".$jobid.")";
        JobSkill::model()->deleteAll($cri1);
      }

/// create recruitment staff

     public function actioncreaterecstaff() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $usermodel = new Users();
        $profilemodel = new Profiles();
        $addressmodel = new Addresses();
        $agencymodel = new AgencyStaff();
        $now = new DateTime();
        
        $usertable = Yii::app()->db->schema->getTable(Users::model()->tableName());
        $profiletable = Yii::app()->db->schema->getTable(Profiles::model()->tableName());
        $addresstable = Yii::app()->db->schema->getTable(Addresses::model()->tableName());
        $agencytable = Yii::app()->db->schema->getTable(AgencyStaff::model()->tableName());
        
        foreach ($_POST as $var => $value) {
            if ($usermodel->hasAttribute($var)) {
                if ($var == "email") {
                    $mmodel = Users::model()->findByAttributes(array('email' => $value));
                    if ($mmodel != null) {
                        $this->_sendResponse(502, array());
                        die();
                    }
                }
                $usermodel->$var = $value;
            } else if ($addressmodel->hasAttribute($var)) {
                $addressmodel->$var = $value;
            } else if ($profilemodel->hasAttribute($var)) {
                $profilemodel->$var = $value;
            } else if ($agencymodel->hasAttribute($var)) {
                $agencymodel->$var = $value;
            } else if ($var == 'ispremium') {
                //  $this->_sendResponse(400, array());
            } else if (!isset($usertable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($profiletable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($addresstable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else {
                $this->_sendResponse(500, array());
            }
        }
        if (isset($_POST['county']) && $_POST['country'])
        $usermodel->location = $_POST['county'] . ($_POST['county'] != '' ? ', ' . $_POST['country'] : '');
        if (isset($_POST['latitude']) && $_POST['latitude'])
            $usermodel->location_lat = $_POST['latitude'];
        if (isset($_POST['longitude']) && $_POST['longitude'])
            $usermodel->location_lng = $_POST['longitude'];
        // Check if using free trial then set membership trial date
        if (isset($_POST['ispremium']) && $_POST['ispremium'])
            $usermodel->trial_ends_at = Carbon::now()->addYear()->toDateTimeString();
        $usermodel->type = 'candidate';
        $usermodel->created_at = $now->format('Y-m-d H:i:s');
        $usermodel->password = password_hash($_POST['password'], 1, ['cost' => 10]);

        
        $profilemodel->slug = $this->getValidatedSlug($usermodel->forenames . "-" . $usermodel->surname);
        $valid = $usermodel->validate();
        $valid = $addressmodel->validate() && $valid;
        $valid = $profilemodel->validate() && $valid;

        if ($valid) {
            if ($usermodel->save()) {
                $addressmodel->model_id = $usermodel->id;
                $addressmodel->model_type = "Candidate";
                $addressmodel->created_at = $now->format('Y-m-d H:i:s');
                if ($addressmodel->save()) {
                    $profilemodel->owner_type = "Candidate";
                    $profilemodel->owner_id = $usermodel->id;
                    
                    $profilemodel->created_at = $usermodel->created_at;
                    $profilemodel->type = $usermodel->type;
                    if ($profilemodel->save()) {
                        // $mmodel = AgencyCandidates::model()->findByAttributes(array('user_id' => $value));
                        $agencymodel->user_id = $usermodel->id;
                        //  $agencymodel->agency_id = $usermodel->id;
                        $agencymodel->created_at = $now->format('Y-m-d H:i:s');

                        if ($agencymodel->save()) {
                            $cri = new CDbCriteria();
                            $cri->select = 'id,forenames,surname,email,mobile';
                            $cri->condition = "id = " . $usermodel->id;
                            $users = $usermodel->getCommandBuilder()->createFindCommand($usermodel->tableSchema, $cri)->queryAll();
                            $this->_sendResponse(200, $users);
                        }
                    } 
                } 
            }
        } else {
            $usrerr = array();
            $usercolumns = Users::model()->getAttributes();
            $addresscolumns = Addresses::model()->getAttributes();
            $profilecolumns = Profiles::model()->getAttributes();
            $agencycolumns = AgencyStaff::model()->getAttributes();

            foreach ($usercolumns as $key => $val) {
                if ($usermodel->getError($key) != "") {
                    $usrerr[$key] = $usermodel->getError($key);
                }
            }
            foreach ($addresscolumns as $key => $val) {
                if ($addressmodel->getError($key) != null)
                    $usrerr[$key] = $addressmodel->getError($key);
            }
            foreach ($profilecolumns as $key => $val) {
                if ($profilemodel->getError($key) != null)
                    $usrerr[$key] = $profilemodel->getError($key);
            }

            $this->_sendResponse(512, $usrerr);
        }
    }

    // end ilai API //
    // new API

    public function actiongetallprofilefavourites() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        $id = isset($_GET['profile_id']) ? $_GET['profile_id'] : null;
        $profilemodel = Profiles::model()->findByPk($id);
        if ($id != $profilemodel->id)
            $this->_sendResponse(404, array());

        $model = new ProfileFavourites();
        $cri = new CDbCriteria();
        $cri->alias = "pf";
        $cri->select = "p.slug,CONCAT('https://cvvid.com/api/images/media/',m.id,'/',m.file_name) as photo,p.photo_id";
        $cri->join = "inner join cv16_profiles p on p.id = pf.profile_id
                      inner join cv16_media m on m.id = p.photo_id";
        $cri->condition = "pf.favourite_id = " . $id;
        // echo var_dump($cri);die();
        $docs = $model->getCommandBuilder()->createFindCommand($model->tableSchema, $cri)->queryAll();

        if (empty($docs)) {
            $this->_sendResponse(404, array());
        } else {
            $this->_sendResponse(200, $docs);
        }
    }

    /* check slug */

    public function actionoldcheckslug() {
      
        if (!isset($_GET['id']) || $_GET['id'] == NULL)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $slug = $_POST['slug'];
        $usermodel = Users::model()->findByPk($id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $id));
        if ($slug == $profilemodel->slug) {
            $cri = new CDbCriteria();
            $cri->select = 'slug';
            $cri->condition = 'owner_id=' . $id;
            $profile = $profilemodel->getCommandBuilder()->createFindCommand($profilemodel->tableSchema, $cri)->queryAll();
            if (!empty($profile))
            // $this->_sendResponse(200, $profile);
                $this->_sendResponse(200, sprintf("Already Exists"));
        } else {
            $this->_sendResponse(507, sprintf("Slug Available"));
        }
    }

public function actioncheckslug() {

    // $this->apikeyAuth();


    // if(!$this->_checkAuth())
    //       $this->_sendResponse(405, array());

        $slug = $_GET['slug'];
        $pmodel = Profiles::model();
        $cri = new CDbCriteria();
        $cri->condition = "slug ='" . $slug . "'";
        $pmodel = Profiles::model()->getCommandBuilder()->createFindCommand($pmodel->tableSchema, $cri)->queryAll();
        if (!empty($pmodel)) {
            $this->_sendResponse(200, sprintf("Already Exists"));
        } else {
            $this->_sendResponse(507, sprintf("Slug Available"));
        }
    }


    /* check slug */

    /* unread inbox count */

    public function actionunreadmessagecount() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == NULL)
            $this->_sendResponse(404, array());
        $msg = [];
        $id = $_GET['id'];
        $usermodel = Users::model()->findByPk($id);
        $convmem = ConversationMembers::model()->findAllByAttributes(array('user_id' => $id, 'last_read' => null));
        $count = count($convmem);
        $msg['count'] = $count;
        if (!empty($msg)) {
            $this->_sendResponse(200, $msg);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /* unread inbox count */


    /* employer company profile details */

    public function actionoldupdatecompanydetails() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == NULL)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $employer = Employers::model()->findByPk($id);
        $userid = $employer->user_id;
        $profile = Profiles::model()->findByAttributes(array('owner_id' => $userid, 'type' => 'employer'));
        $employer->name = $_POST['name'];
        $employer->website = $_POST['website'];
        $employer->location = $_POST['location'];
        if ($employer->save()) {
            $profile->industry_id = $_POST['industry_id'];
            if ($profile->save()) {
                $cri = new CDbCriteria();
                $cri->select = 'name, website, location';
                $cri->condition = 'id=' . $id;
                $employer = $employer->getCommandBuilder()->createFindCommand($employer->tableSchema, $cri)->queryAll();
                if (!empty($employer)) {
                    $this->_sendResponse(200, $employer);
                } else {
                    $this->_sendResponse(507, array());
                }
            } else {
                $this->_sendResponse(507, array());
            }
        }
    }

   public function actionupdatecompanydetails() {
        $this->apikeyAuth();
        if (!isset($_GET['id']) || $_GET['id'] == NULL)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $employer = Employers::model()->findByPk($id);
        $userid = $employer->user_id;
        $employertable = Yii::app()->db->schema->getTable(Employers::model()->tableName());
        $profiletable = Yii::app()->db->schema->getTable(Profiles::model()->tableName());
        $profile = Profiles::model()->findByAttributes(array('owner_id' => $userid, 'type' => 'employer'));

        foreach ($_POST as $var => $value) {
            if ($employer->hasAttribute($var)) {
                $employer->$var = $value;
            } elseif ($profile->hasAttribute($var)) {
                $profile->$var = $value;
            } else if (!isset($employertable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else if (!isset($profiletable->columns[$var])) {  // Column doesn't exist
                $this->_sendResponse(511, array(), 'text/html', $var);
            } else {
                $this->_sendResponse(400, array());
            }
        }

        $employer->name = $_POST['name'];
        $employer->website = $_POST['website'];
        $employer->location = $_POST['location'];

        $valid = $employer->validate();
        $valid = $profile->validate() && $valid;
        if ($valid) {
            if ($employer->save()) {
                $profile->industry_id = $_POST['industry_id'];
                if ($profile->save()) {
                    $cri = new CDbCriteria();
                    $cri->select = 'name, website, location';
                    $cri->condition = 'id=' . $id;
                    $employer = $employer->getCommandBuilder()->createFindCommand($employer->tableSchema, $cri)->queryAll();
                    $this->_sendResponse(200, $employer);
                }
            }
        } else {
            $usrerr = array();
            $employercolumns = Employers::model()->getAttributes();
            $profilecolumns = Profiles::model()->getAttributes();

            foreach ($employercolumns as $key => $val) {
                if ($employer->getError($key) != "") {
                    $usrerr[$key] = $employer->getError($key);
                }
            }

            foreach ($profilecolumns as $key => $val) {
                if ($profile->getError($key) != null)
                    $usrerr[$key] = $profile->getError($key);
            }
            $this->_sendResponse(512, $usrerr);
        }
    }

    /* employer company profile details */
    /* fetch employer company profile details */

    public function actionfetchcompanydetails() {

        $this->apikeyAuth();

        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == NULL)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];
        $employer = Employers::model()->findByPk($id);
        if ($employer == NULL) {
            $this->_sendResponse(500, array());
        }
        $cri = new CDbCriteria();
        $cri->alias = 'emp';
        $cri->select = 'emp.name,emp.website,emp.location,prof.industry_id';
        $cri->join = 'inner join cv16_profiles prof on prof.owner_id = emp.id';
        $cri->condition = 'emp.id=' . $id;
        $cri->group = 'emp.id=' . $id;
        $employer = $employer->getCommandBuilder()->createFindCommand($employer->tableSchema, $cri)->queryAll();
        if (!empty($employer)) {
            $this->_sendResponse(200, $employer);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    /* fetch employer company profile details */

    // posted jobs

    public function actiongetallpostedjobs() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $empmodel = Employers::model()->findByPk($id);

        $jobmodel = Jobs::model();

        $cri = new CDbCriteria();
        $cri->condition = 'owner_id=' . $empmodel->id;

        $results = $jobmodel->getCommandBuilder()->createFindCommand($jobmodel->tableSchema, $cri)->queryAll();
        if (!empty($results)) {
            $this->_sendResponse(200, $results);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    //  get applications
    public function actiongetalljobapplications() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $jobmodel = JobApplications::model();

        $cri2 = new CDbCriteria();
        $cri2->alias = 'j';
        $cri2->select = 'j.id,us.forenames,us.surname,us.email,j.created_at,j.status';
        $cri2->join = 'inner join cv16_users us on us.id = j.user_id';
        $cri2->condition = "j.status < 1 and j.job_id = " . $id;
        $results = $jobmodel->getCommandBuilder()->createFindCommand($jobmodel->tableSchema, $cri2)->queryAll();
        if (!empty($results)) {
            $this->_sendResponse(200, $results);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    // get getalljobInterviewees

    public function actiongetalljobInterviewees() {

        $this->apikeyAuth();

        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $jobmodel = JobApplications::model();

        $cri2 = new CDbCriteria();
        $cri2->alias = 'j';
        $cri2->select = 'j.id,us.forenames,us.surname,us.email,j.created_at,j.status';
        $cri2->join = 'inner join cv16_users us on us.id = j.user_id';
        $cri2->condition = "j.status >= 2 and j.job_id = " . $id;

//$count['counts'] = $results;
        $results = $jobmodel->getCommandBuilder()->createFindCommand($jobmodel->tableSchema, $cri2)->queryAll();
        //   $results['counts'] = $results1;



        if (!empty($results)) {
            $this->_sendResponse(200, $results);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    //  get shortlisted
    public function actiongetalljobshortlisted() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $jobmodel = JobApplications::model();

        $cri2 = new CDbCriteria();
        $cri2->alias = 'j';
        $cri2->select = 'j.id,us.forenames,us.surname,us.email,j.created_at,j.status';
        $cri2->join = 'inner join cv16_users us on us.id = j.user_id';
        $cri2->condition = "j.status = 1 and j.job_id = " . $id;

//$count['counts'] = $results;
        $results = $jobmodel->getCommandBuilder()->createFindCommand($jobmodel->tableSchema, $cri2)->queryAll();
        //   $results['counts'] = $results1;



        if (!empty($results)) {
            $this->_sendResponse(200, $results);
        } else {
            $this->_sendResponse(507, array());
        }
    }

    //  get counts
    public function actiongetalljobcount() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $jobmodel = JobApplications::model();

        $cri2 = new CDbCriteria();
        $cri2->alias = 'j';
        $cri2->select = 'j.id,us.forenames,us.surname,us.email,j.created_at,j.status';
        $cri2->join = 'inner join cv16_users us on us.id = j.user_id';
        $cri2->condition = "j.status < 1 and j.job_id = " . $id;

        $cri1 = new CDbCriteria();
        $cri1->condition = "status >= 2 and job_id = " . $id;
        // $count = count(JobApplications::model()->findAll($cri1));

        $appcount = 0;
        $shortcount = 0;
        $intercount = 0;

        if (count(JobApplications::model()->findAll($cri2)) > 0)
            $appcount = count(JobApplications::model()->findAll($cri2));

        if (JobApplications::model()->countByAttributes(array('status' => 1, 'job_id' => $id)) > 0)
            $shortcount = JobApplications::model()->countByAttributes(array('status' => 1, 'job_id' => $id));

        if (count(JobApplications::model()->findAll($cri1)) > 0)
            $intercount = count(JobApplications::model()->findAll($cri1));

        $results = array();

        $results['appcount'] = $appcount;
        $results['shortcount'] = $shortcount;
        $results['intcount'] = $intercount;

        if (!empty($results)) {
            $this->_sendResponse(200, array($results));
        } else {
            $this->_sendResponse(507, array());
        }
    }

      /*update add job*/
          
          public function actionupdatenewjob() {

            $this->apikeyAuth();


          //   if(!$this->_checkAuth())
          // $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());
        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        $jobmodel = Jobs::model()->findByPk($id);
        $jobskillmodel = JobSkill::model()->findByAttributes(array('job_id' => $id));
        $now = new DateTime();
        foreach ($_POST as $var => $value) {
            if ($jobmodel->hasAttribute($var)) {
                $jobmodel->$var = $value;
            } elseif ($var == 'skills') {
                $skills = $value;
            } else {
                $this->_sendResponse(400, array());
            }
        }
        $d = str_replace('/', '-', $_POST["end_date"]);
        $endformat = date('Y-m-d', strtotime($d));
        $jobmodel->end_date = $endformat;
        $jobmodel->created_at = $now->format('Y-m-d H:i:s');
        if ($jobmodel->save()) {
            $jobskillmodel = JobSkill :: model()->deleteAllByAttributes(array('job_id' => $id));
            foreach ($skills as $value) {
                $jobskillmodels = new JobSkill();
                $jobskillmodels->job_id = $jobmodel->id;
                $jobskillmodels->skill_id = $value['skill_id'];
                $jobskillmodels->vacancies = $value['vacancies'];
                 $jobskill = $jobskillmodels->save();
            }
            if ($jobskill) {
                $cri = new CDbCriteria();
                $cri->select = 'user_id, industry_id, company_name, title, salary_type, salary_min, salary_max, end_date, skill_category_id, location, location_lat, location_lng, type, description';
                $cri->condition = 'id=' . $jobmodel->id;
                $users = $jobmodel->getCommandBuilder()->createFindCommand($jobmodel->tableSchema, $cri)->queryAll();
                if (!empty($users)) {
                    $this->_sendResponse(200, $users);
                } else {
                    $this->_sendResponse(507, array());
                }
            } else {
               $this->_sendResponse(507, $jobskillmodels->getErrors());
            }
        } else {
            $this->_sendResponse(507, $jobmodel->getErrors());
        }
    }
     /*update add job*/

    public function actioninviteforinterview() {

        $this->apikeyAuth();


        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());

        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $interview_date = $_POST['interview_date'];
        $msg = $_POST['msg'];

        $status = 2;

        $model = JobApplications::model()->findByPk($id);
        $model->interview_date = $interview_date;
        $model->status = $status;
        $model->message = $msg;
        if ($model->save()) {

            $jobs = Jobs::model()->findByPk($model->job_id);
            $user = Users::model()->findByPk($jobs->user_id);

            $job = $jobs->title;
            $name = $user->forenames;
            $email = $user->email;
            $message = $msg;

            $this->notifyinterview($job, $name, $email, $message);

            //  $this->notifyinvite($email);

            $this->_sendResponse(200, sprintf("Successfully Invited", ""));
        } else
            $this->_sendResponse(500, array());
    }

    // invite for interview
    // accept application

    public function actionacceptapplication() {

        $this->apikeyAuth();

        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());

        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $now = new DateTime();
        // $application_id  = $_POST['application_id'];
        $msg = $_POST['msg'];

        $status = 4;

        $model = JobApplications::model()->findByPk($id);
        $model->status = $status;
        $model->message = $msg;
        $model->accepted_date = $now->format('Y-m-d H:i:s');
        if ($model->save()) {
            $this->_sendResponse(200, sprintf("Successfully Accepted", ""));
        } else
            $this->_sendResponse(500, array());
    }

    // reject application

    public function actionrejectapplication() {

        $this->apikeyAuth();

        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());

        $id = $_GET['id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }

        $now = new DateTime();
        // $application_id  = $_POST['application_id'];
        $msg = $_POST['msg'];

        $status = -1;

        $model = JobApplications::model()->findByPk($id);
        $model->status = $status;
        $model->message = $msg;
        if ($model->save()) {
            $this->_sendResponse(200, sprintf("Successfully Rejected", ""));
        } else
            $this->_sendResponse(500, array());
    }
    
    
    /*31.5.2018*/
/*get message*/   
    public function actiongetusermessage() {

        $this->apikeyAuth();

        
        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
        $id = $_GET['id'];
        $convmodel = ConversationMembers::model();
        if ($convmodel == NULL) {
            $this->_sendResponse(404, array());
        }
        $cri = new CDbCriteria();
        $cri->alias = "cm";
        $cri->select = 'cm.conversation_id,c.subject,cmes.message,cmes.name,cm.created_at,cm.user_id';
        $cri->join = 'LEFT OUTER join cv16_conversations c on c.id = cm.id'
                . ' LEFT OUTER JOIN cv16_conversation_messages cmes on cmes.conversation_id = c.id';
        $cri->condition = "cm.user_id =" . $id;
        $cri->order = "created_at desc";
        $convmodel = $convmodel->getCommandBuilder()->createFindCommand($convmodel->tableSchema, $cri)->queryAll();
        if (empty($convmodel)) {
            $this->_sendResponse(507, array());
        } else {
            $this->_sendResponse(200, $convmodel);
        }
    }
     /*get message*/ 
    /*31.5.2018*/    
	
	
     public function actioncheckapplyjob() {

      //  $this->apikeyAuth();

        // if(!$this->_checkAuth())
        //   $this->_sendResponse(405, array());

        if (!isset($_GET['id']) || $_GET['id'] == 0)
              $this->_sendResponse(400, array());

        $id = $_GET['id'];
        $user_id = $_GET['user_id'];

        if (json_decode(file_get_contents("php://input"), true)) {
            $_POST = json_decode(file_get_contents("php://input"), true);
        }
        
        $usermodel = Users::model()->findByPk($user_id);
        $model = Jobs::model()->findByPk($id);
        
        $status;
        $message = "";
        
        if(Membership::subscribed($usermodel->stripe_active, $usermodel->trial_ends_at, $usermodel->subscription_ends_at)) {
            //$isupgrade = TRUE;
            $jobapp = JobApplications::model()->findByAttributes(array('user_id'=>$user_id,'job_id'=> $id));
            if($jobapp == null)
            {
                 if($model->video_only > 0)
		 {
                     $pmodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id));
		     $pvmodel = ProfileVideos::model()->findByAttributes(array('profile_id'=>$pmodel->id,'is_default'=>1));
                     if($pvmodel == null)
                     {
                         $status = FALSE;
                         $message = "Only Candidates with default profile video can apply to this job";
                     }else{
                         $status = TRUE;
                         $message = "Apply Now";
                     }
                 }else{
                     $status = TRUE;
                     $message = "Apply Now";
                 }
            }else{
                $status = FALSE;
                $message = "Already Applied";
            }
        }else{
            $status = FALSE;
            $message = "Upgrade membership to apply";
        }
        
        
        $result = array();
        
        $result['status'] = $status;
        $result['message'] = $message;

        if (!empty($result)) {
            $this->_sendResponse(200, array($result));
        } else
            $this->_sendResponse(500, array());
    }
	
	public function sendMessage($subject,$message,$from,$to) {
	    
	    $now = new DateTime();
	    $conmodel = new Conversations();
	    $conmodel->subject = $subject;
	    $conmodel->created_at = $now->format('Y-m-d H:i:s');
	    $conmodel->updated_at = $now->format('Y-m-d H:i:s');
	    if ($conmodel->save()) {
	        $usermodel = Users::model()->findByPk($from);
	        $conmsgmodel = new ConversationMessages();
	        $conmsgmodel->conversation_id = $conmodel->id;
	        $conmsgmodel->user_id = $from;
	        $conmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
	        $conmsgmodel->message = $message;
	        $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
	        $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
	        if ($conmsgmodel->save()) {
	            $conmemmodel_sender = new ConversationMembers();
	            $conmemmodel_sender->conversation_id = $conmodel->id;
	            $conmemmodel_sender->user_id = $from;
	            $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
	            $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
	            $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
	            $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
	            if ($conmemmodel_sender->save()) {
	                $usermodel1 = Users::model()->findByPk($to);
	                $conmemmodel_rec = new ConversationMembers();
	                $conmemmodel_rec->conversation_id = $conmodel->id;
	                $conmemmodel_rec->user_id = $to;
	                $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
	                $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
	                $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
	                if ($conmemmodel_rec->save()) {
	                    $actmodel_sender = new Activities();
	                    $actmodel_sender->user_id = $from;
	                    $actmodel_sender->type = "message";
	                    $actmodel_sender->model_id = $conmsgmodel->id;
	                    $actmodel_sender->model_type = "ConversationMessage";
	                    $actmodel_sender->subject = "You sent a message in conversation: " . $subject;
	                    $actmodel_sender->message = $message;
	                    $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
	                    $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
	                    if ($actmodel_sender->save()) {
	                        $actmodel_rec = new Activities();
	                        $actmodel_rec->user_id = $to;
	                        $actmodel_rec->type = "message";
	                        $actmodel_rec->model_id = $conmsgmodel->id;
	                        $actmodel_rec->model_type = "ConversationMessage";
	                        $actmodel_rec->subject = "You received a message in conversation: " . $subject;
	                        $actmodel_rec->message = $message;
	                        $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
	                        $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
	                        $actmodel_rec->save();
	                    }
	                }
	            }
	        }
	    }
	}
	
	
//        mail
          public function actioncheckmail() {
                $body = "register candidate registered successfully";
                Yii::import('application.extensions.phpmailer.JPhpMailer');
                $mail = new JPhpMailer;
                $mail->Host = 'cvvid.com';
                $mail->Username = 'mail@cvvid.com';
                $mail->Password = 'oBua707Aq#juCvqa';
                $mail->SMTPSecure = 'ssl';
                $mail->SetFrom('mail@cvvid.com', 'cvvid');
                $mail->Subject = 'Your are registered candidate details for cvvid.com';
                $mail->MsgHTML($body);
                $mail->AddAddress("ezhilpms@gmail.com");
                $mail->AddReplyTo('cvvid@gmail.com', 'Reply to cvvid');
                if(!$mail->send()) 
                {
                    echo "Mailer Error: " . $mail->ErrorInfo;
                } 
                else 
                {
                    echo "Message has been sent successfully";
                }
          }
	
	
	 //  get shortlisted
    //  get shortlisted
    public function actiongetallquestions() {

        $this->apikeyAuth();

        if (!isset($_GET['job_id']) || $_GET['job_id'] == 0)
            $this->_sendResponse(404, array());

        $id = $_GET['job_id'];

        $jobmodel = JobQuestions::model();

        $cri2 = new CDbCriteria();
        $cri2->alias = 'q';
        $cri2->condition = "q.job_id = " . $id;
        
        $results = $jobmodel->getCommandBuilder()->createFindCommand($jobmodel->tableSchema, $cri2)->queryAll();
       
        if (!empty($results)) {
            $this->_sendResponse(200, $results);
        } else {
            $this->_sendResponse(507, array());
        }
    }
    
     //  get shortlisted
    public function actiongetallanswers() {

        $this->apikeyAuth();

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $jobmodel = JobAnswers::model();
        
        $bucket = Yii::app()->params['AWS_BUCKET'];

        $cri2 = new CDbCriteria();
        $cri2->alias = 'a';
        $cri2->select = "a.id,CONCAT('https://s3.amazonaws.com/$bucket/',v.video_id) as video,u.forenames,u.surname,q.question";
        $cri2->join = 'LEFT OUTER join cv16_videos v on v.id = a.videoid '
                    . 'LEFT OUTER JOIN cv16_users u on u.id = a.user_id '
                    . 'LEFT OUTER JOIN cv16_job_questions q on q.id = a.question_id';
        $cri2->condition = "a.job_id = " . $id;
        
        $results = $jobmodel->getCommandBuilder()->createFindCommand($jobmodel->tableSchema, $cri2)->queryAll();
       
        if (!empty($results)) {
            $this->_sendResponse(200, $results);
        } else {
            $this->_sendResponse(507, array());
        }
    }
	
	
	  public function actionaddnewquestions() {

        $this->apikeyAuth();

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $id = $_GET['id'];

        $now = new DateTime();
        $model = Jobs::model()->findByPk($id);
        $qmodel = new JobQuestions();
        $qmodel->question = $_POST['question'];
        $qmodel->created_at = $now->format('Y-m-d H:i:s');
        $qmodel->job_id = $id;
        $qmodel->user_id = 0;
        if($qmodel->save())
        {
             $this->_sendResponse(200, sprintf("Successfully Added", ""));
        } else
            $this->_sendResponse(500, array());

    }
    

     public function actioneditquestions() {

        $this->apikeyAuth();

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $id = $_GET['id'];
        
        $now = new DateTime();
        $value = $_POST['question'];
        $qmodel = JobQuestions::model()->findByPk($id);
        $qmodel->question = $value;
        $qmodel->updated_at = $now->format('Y-m-d H:i:s');
        if($qmodel->save())
        {
             $this->_sendResponse(200, sprintf("Successfully updated", ""));
        } else
            $this->_sendResponse(500, array());

    }
    
    public function actiondeletequestions() {

        $this->apikeyAuth();

        if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(404, array());

        $id = $_GET['id'];

	$qmodel = JobQuestions::model()->deleteByPk($id);
        
        $this->_sendResponse(200, sprintf("Successfully deleted", ""));
        
        

    }
	
	public function actioncheckcandidateinterview()
    {
          $this->apikeyAuth();
          
          if (!isset($_GET['id']) || $_GET['id'] == 0)
            $this->_sendResponse(400, array());
          
          $usermodel = Users::model()->findByPk($_GET['id']);
          
          $date = date('Y-m-d H:i:s');
          
          $cri2 = new CDbCriteria();
          $cri2->alias = 'ja';
          $cri2->condition = "ja.user_id = ".$usermodel->id." and interview_date >= '$date'";
                    
          $jobapplication = JobApplications::model()->getCommandBuilder()->createFindCommand(JobApplications::model()->tableSchema, $cri2)->queryAll();
       
          if (!empty($jobapplication)) {
            $this->_sendResponse(200, $jobapplication);
          } else {
                $this->_sendResponse(507, array());
          }
    }
    
    
    public function actiongetprofileuser()
    {
        
        $id = $_GET['id'];
        
        $model = Users::model()->findByPk($id);
        
        $totarr =array();
        
        $basic = Users::model();
        $cri = new CDbCriteria();
        $cri->alias = 'u';
        $cri->select = "u.id as user_id,u.trial_ends_at as trails,p.hired,m.file_name,(SELECT count(*) FROM cv16_profile_views WHERE profile_id = p.id) as num_views,p.visibility,(SELECT count(*) FROM cv16_profile_favourites WHERE profile_id = p.id) as num_likes,CONCAT('https://cvvid.com/images/media/',p.photo_id,'/',m.file_name) as photo,CONCAT('https://cvvid.com/images/defaultprofile.jpg') as photodefault,CONCAT('https://cvvid.com/images/media/',p.cover_id,'/conversions/cover.jpg') as cover,CONCAT('https://cvvid.com/images/CoverPlaceholder.jpg') as coverdefault,u.forenames,u.surname,u.email,u.stripe_active,u.current_job,u.location,p.slug,p.photo_id,p.cover_id,p.id as profile_id,a.address,a.town,a.county,a.country,a.postcode";
        $cri->join = "inner join cv16_profiles p on p.owner_id = u.id
                      left outer join cv16_media m on m.id = p.photo_id
                      left outer join cv16_addresses a on a.model_id = u.id and a.model_type like '%student%'";
        $cri->condition = "u.id = " . $model->id . " and u.type like '%student%' and p.owner_type like '%student%'";
        $userbasic = $basic->getCommandBuilder()->createFindCommand($basic->tableSchema, $cri)->queryAll();
        
        $totarr['profile'] = $userbasic;
        
        $pmodel = Profiles::model()->findByAttributes(array('owner_id'=>$model->id,'type'=>$model->type));
        
        $pvmodel = ProfileVideos::model()->findByAttributes(array('profile_id' => $pmodel->id, 'is_default' => 1));
        
        $videoarr = array();
        
        if ($pvmodel != null) {
            $videomodel = Videos::model()->findByPk($pvmodel->video_id);
            $url = 'https://' . Yii::app()->params['AWS_BUCKET'] . '.s3.amazonaws.com/' . $videomodel->video_id;
            
            $videoarr['status'] = "1";
            $videoarr['url'] = $url;
        }  else {
            $videoarr['status'] = "0";
            $videoarr['url'] = $url;
        }
        
        $totarr['video'] = array($videoarr);
         
        
        $uemodel = UserExperiences::model();


        $cri = new CDbCriteria();
        $sql = $uemodel->find($cri);
        $startday = $sql['start_date'];
        $startmonth = $sql['start_date'];
        $startyear = $sql['start_date'];
        $endday = $sql['end_date'];
        $endmonth = $sql['end_date'];
        $endyear = $sql['end_date'];
//        $cri->select = "id, user_id, IF('$startday' = '0000-00-00', '0000-00-00', DAY('$startday')) as startdate ,  MONTH('$startmonth') as startmonth, YEAR('$startyear') as startyear,IF('$endday' = '0000-00-00', '0000-00-00', DAY('$endday')) as enddate, MONTH('$endmonth') as endmonth,YEAR('$endyear') as endyear,location, company_name, industry, job_role, position, description";
        $cri->condition = "user_id =  " . $model->id . " ";
        $usersexp = $uemodel->getCommandBuilder()->createFindCommand($uemodel->tableSchema, $cri)->queryAll();
        
        $totarr['professional'] = $usersexp;
        
        $userqmodel = UserQualifications::model();
        $cri = new CDbCriteria();
        $cri->select = "*";
        $cri->condition = "user_id =  " . $model->id . " ";
        $education = $userqmodel->getCommandBuilder()->createFindCommand($userqmodel->tableSchema, $cri)->queryAll();
        
        $totarr['education'] = $education;
//         $body['data'] = $totarr;
        
        $lmodel = UserGoal::model();
        $cri = new CDbCriteria();
        $cri->select = "id, activity, description";
        $cri->condition = "user_id =  " . $model->id . " ";
        $langugae = $lmodel->getCommandBuilder()->createFindCommand($lmodel->tableSchema, $cri)->queryAll();
         
        $totarr['goal'] = $langugae;
        
        $hobbmodel = UserHobbies::model();
        $cri = new CDbCriteria();
        $cri->select = "*";
        $cri->condition = "user_id =  " . $model->id . " ";
        $husers = $hobbmodel->getCommandBuilder()->createFindCommand($hobbmodel->tableSchema, $cri)->queryAll();
       
        $totarr['hobbies'] = $husers;
        
        $this->_sendResponse(200, $totarr);
    }

        public function actionuploadanswer(){
        
        $user_id = $_POST['user_id'];
        $job_id = $_POST['job_id'];
        $id = $_POST['joba_id'];                
        
        $jamodel = JobApplications::model()->findByPk($id);
        $umodel = Users::model()->findByPk($user_id);
        $now = new DateTime();
        $userid = $umodel->id;
        $quesid = $_POST['question_id'];
        
        $name = '';
        $tmp = '';
        $file_idx = '';
        
        $size = 0;//$_FILES['video']['size'];
        
        if (!empty($_FILES['video'])) {
            $file_idx = 'video';
            $name = $_POST['video_name'];
            $tmp = $_FILES[$file_idx]['tmp_name'];
        } 
        
         $ext = pathinfo($name, PATHINFO_EXTENSION);
         $usermodel = Users::model()->findByPk($userid);
         $pmodel = Profiles::model()->findByAttributes(array('owner_id' => $userid, 'type' => $usermodel->type));
         
         Yii::import('application.extensions.amazon.components.*');
         
         $bucket = Yii::app()->params['AWS_BUCKET'];
         $s3 = new A2S3();
         
         $vname = pathinfo($name, PATHINFO_FILENAME);
         
         $actual_image_name = $vname. "." . $ext;
         
         //        video validation
         if (strlen($name) > 0) {
                         
                 $response =  $s3->putObject(array(
                     'SourceFile' => $tmp,
                     'Bucket' => $bucket,
                     'Key' => $actual_image_name,
                     'ACL' => 'public-read',
                     'x-amz-storage-class' => 'REDUCED_REDUNDANCY'
                 ));
                 
                 $duration = 6;
                 
                 $videomodel = new Videos();
                 $videomodel->model_id = $userid;
                 $videomodel->video_id = $actual_image_name;
                 $videomodel->name = $vname;
                 $videomodel->duration = 0;
                 $videomodel->model_type = $usermodel->type;
                 $videomodel->category_id = 1;
                 $videomodel->is_processed = 1;
                 $videomodel->state = 1;
                 $videomodel->status = "active";
                 $videomodel->created_at = $now->format('Y-m-d H:i:s');
                 if($videomodel->save())
                 {
                     $ansmodel = new JobAnswers();
                     $ansmodel->user_id = $userid;
                     $ansmodel->job_id = $jamodel->job_id;
                     $ansmodel->question_id = $quesid;
                     $ansmodel->videoid = $videomodel->id;
                     $ansmodel->created_at = $now->format('Y-m-d H:i:s');
                     $ansmodel->save();
                     
                     $jamodel->status = 3;
                     $jamodel->save(false);
                 }
                 
                 $this->_sendResponse(200, spintf("Successfully uploaded !!!"));
//                 echo "success";
                 
            
         }
        
    }
    
     public static function randomBytes($length = 16) {
        if (PHP_MAJOR_VERSION >= 7 || defined('RANDOM_COMPAT_READ_BUFFER')) {
            $bytes = random_bytes($length);
        } elseif (function_exists('openssl_random_pseudo_bytes')) {
            $bytes = openssl_random_pseudo_bytes($length, $strong);

            if ($bytes === false || $strong === false) {
                throw new RuntimeException('Unable to generate random string.');
            }
        } else {
            throw new RuntimeException('OpenSSL extension or paragonie/random_compat is required for PHP 5 users.');
        }

        return $bytes;
    }
    
     public static function random($length = 16) {
        $string = '';

        while (($len = strlen($string)) < $length) {
            $size = $length - $len;

            $bytes = static::randomBytes($size);

            $string .= substr(str_replace(['/', '+', '='], '', base64_encode($bytes)), 0, $size);
        }

        return $string;
    }
    
    function timeAgo($time_ago)
    {
        $time_ago = strtotime($time_ago);
        $cur_time   = time();
        $time_elapsed   = $cur_time - $time_ago;
        $seconds    = $time_elapsed ;
        $minutes    = round($time_elapsed / 60 );
        $hours      = round($time_elapsed / 3600);
        $days       = round($time_elapsed / 86400 );
        $weeks      = round($time_elapsed / 604800);
        $months     = round($time_elapsed / 2600640 );
        $years      = round($time_elapsed / 31207680 );
        // Seconds
        if($seconds <= 60){
            return "just now";
        }
        //Minutes
        else if($minutes <=60){
            if($minutes==1){
                return "one minute ago";
            }
            else{
                return "$minutes minutes ago";
            }
        }
        //Hours
        else if($hours <=24){
            if($hours==1){
                return "an hour ago";
            }else{
                return "$hours hrs ago";
            }
        }
        //Days
        else if($days <= 7){
            if($days==1){
                return "yesterday";
            }else{
                return "$days days ago";
            }
        }
        //Weeks
        else if($weeks <= 4.3){
            if($weeks==1){
                return "a week ago";
            }else{
                return "$weeks weeks ago";
            }
        }
        //Months
        else if($months <=12){
            if($months==1){
                return "a month ago";
            }else{
                return "$months months ago";
            }
        }
        //Years
        else{
            if($years==1){
                return "one year ago";
            }else{
                return "$years years ago";
            }
        }
    }
    
    
    function getDuration($file) {

        if (file_exists($file)) {
            ## open and read video file
            $handle = fopen($file, "r");
            ## read video file size
            $contents = fread($handle, filesize($file));

            fclose($handle);
            $make_hexa = hexdec(bin2hex(substr($contents, strlen($contents) - 3)));

            if (strlen($contents) > $make_hexa) {

                $pre_duration = hexdec(bin2hex(substr($contents, strlen($contents) - $make_hexa, 3)));
                $post_duration = $pre_duration / 1000;
                $timehours = $post_duration / 3600;
                $timeminutes = ($post_duration % 3600) / 60;
                $timeseconds = ($post_duration % 3600) % 60;
                $timehours = explode(".", $timehours);
                $timeminutes = explode(".", $timeminutes);
                $timeseconds = explode(".", $timeseconds);
                $duration = $timehours[0] . ":" . $timeminutes[0] . ":" . $timeseconds[0];
            }
            return $duration;
        } else {

            return false;
        }
    }

}
?>