<?php

class AgencyStaffController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
    public $layout = '//layouts/admin';
    public $userid = 0;
    public $edit = 0;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
// 			array('deny',  // deny all users
// 				'users'=>array('*'),
// 			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new AgencyStaff;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AgencyStaff']))
		{
			$model->attributes=$_POST['AgencyStaff'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AgencyStaff']))
		{
			$model->attributes=$_POST['AgencyStaff'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('AgencyStaff');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new AgencyStaff('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['AgencyStaff']))
			$model->attributes=$_GET['AgencyStaff'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return AgencyStaff the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=AgencyStaff::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param AgencyStaff $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='agency-staff-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionEdit($id)
	{
	    $astaffmodel = AgencyStaff::model()->findByPk($id);
	    
	    $this->layout = "main";
	    $this->userid = $astaffmodel->user_id;
	    $this->edit = 1;
	    	    
	    $model = Users::model()->findByPk($astaffmodel->user_id);
	    $agencymodel = Agency::model()->findByPk($astaffmodel->agency_id);
	    $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$model->id));
	    
	    $this->render('editprofile', array(
	        'model' => $model,'agencymodel'=>$agencymodel,'profilemodel'=>$profilemodel,'astaffmodel'=>$astaffmodel
	    ));
	}
	
	public function actionaddCandidate()
	{
	    $this->layout = "main";
	    $agencystaffmodel = AgencyStaff::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
	    $model = Agency::model()->findByPk($agencystaffmodel->agency_id);
	    $acanmodel = new AgencyCandidates();
	    $usermodel = new Users();
	    $addressmodel = new Addresses();
	    
	    $this->render("candidateform",array('model'=>$model,'agencystaffmodel'=>$agencystaffmodel,'agencycanmodel'=>$acanmodel,'usermodel'=>$usermodel,'addressmodel'=>$addressmodel));
	    
	}
	
	public function actionsaveCandidate()
	{
	    $this->layout='main';
	    $agencystaffmodel = AgencyStaff::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
	    $model = Agency::model()->findByPk($agencystaffmodel->agency_id);
	    $usermodel = new Users;
	    $profilemodel = new Profiles;
	    $addressmodel = new Addresses;
	    
	    $now = new DateTime();
	    
	    if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
	        $usermodel->setScenario('confirm');
	        
	        if(isset($_POST['Users'])){
	            $usermodel->attributes=$_POST['Users'];
	        }
	        if(isset ($_POST["Addresses"])){
	            $addressmodel->attributes = $_POST["Addresses"];
	        }
	        if(isset($_POST['Users']))
	        {
	            $valid = $usermodel->validate();
	            $valid = $addressmodel->validate() && $valid;
	            
	            if($valid){
	                $usermodel->created_at = $now->format('Y-m-d H:i:s');
	                $usermodel->type = 'candidate';
	                 $usermodel->stripe_active = 1;
	                $usermodel->trial_ends_at = Carbon::now()->addYear()->toDateTimeString();
	                $usermodel->location = $_POST['Addresses']['county'].($_POST['Addresses']['county']!=''? ', '.$_POST['Addresses']['country']: '');
	                $usermodel->location_lat = $_POST['Addresses']['latitude'];
	                $usermodel->location_lng = $_POST['Addresses']['longitude'];
	                $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
	                if($usermodel->save())
	                {
	                   
	                    $agencycanmodel = new AgencyCandidates();
	                    $agencycanmodel->agency_id = $model->id;
	                    $agencycanmodel->user_id = $usermodel->id;
	                    $agencycanmodel->created_at = $now->format('Y-m-d H:i:s');
	                    if($agencycanmodel->save())
	                    {
    	                    $staffcanmodel = new AgencyStaffCandidates();
    	                    $staffcanmodel->staff_id = $agencystaffmodel->user_id;
    	                    $staffcanmodel->agency_candidate_id = $agencycanmodel->id;
    	                    $staffcanmodel->agency_id = $model->id;
    	                    $staffcanmodel->created_at = $now->format('Y-m-d H:i:s');
    	                    $staffcanmodel->save();
	                    }
	                }
	                $profilemodel->owner_id = $usermodel->id;
	                $profilemodel->owner_type = 'candidate';
	                
	                $profilemodel->type = 'candidate';
	                $profilemodel->slug = $this->getValidatedSlug($usermodel->forenames."-".$usermodel->surname);
	                $profilemodel->save();
	                //address update
	                $addressmodel->created_at = $now->format('Y-m-d H:i:s');
	                $addressmodel->model_type = 'Candidate';
	                $addressmodel->model_id = $usermodel->id;
	                if ($addressmodel->save()) {
	                    
	                    $umodel = Users::model()->findByPk($model->user_id);
	                    
	                    $this->notifycandidate($usermodel, $_POST['Users']['password']);
	                    
	                    $this->redirect($this->createUrl('agencyStaff/edit', array('id' => $agencystaffmodel->id)));
	                    
	                }
	                
	            }
	        }
	        $this->render('candidateform', array('model' => $model,'agencystaffmodel'=>$agencystaffmodel,'agencycanmodel' => new AgencyCandidates(),'profilemodel' => $profilemodel, 'addressmodel' => $addressmodel, 'usermodel' => $usermodel));
	} 
	
	public function actioneditcandidate($id)
	{
	    $this->layout='main';
	    $agencystaffmodel = AgencyStaff::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
	    $agencystaffcanmodel = AgencyStaffCandidates::model()->findByPk($id);
	    $agencycanmodel = AgencyCandidates::model()->findByPk($agencystaffcanmodel->agency_candidate_id);
	    $model = Agency::model()->findByPk($agencycanmodel->agency_id);
	    
	    $usermodel = Users::model()->findByPk($agencycanmodel->user_id);
	    $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id));
	    
	    $cri = new CDbCriteria();
	    $cri->condition = "model_id =".$usermodel->id." and model_type like '%candidate%' and deleted_at is null";
	    $addressmodel = Addresses::model()->find($cri);
	    $this->render("candidateform",array('model'=>$model,'agencystaffmodel'=>$agencystaffmodel,'agencycanmodel'=>$agencycanmodel,'usermodel'=>$usermodel,'profilemodel' => $profilemodel,'addressmodel'=>$addressmodel));
	}
	
	public function actionupdateCandidate($id)
	{
	    $this->layout='main';
	    $agencystaffmodel = AgencyStaff::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
	   
	    $agencycanmodel = AgencyCandidates::model()->findByPk($id);
	    $userid = $agencycanmodel->user_id;
	    $agencyid = $agencycanmodel->agency_id;
	    	    
	    $agencymodel = Agency::model()->findByPk($agencyid);
	    $usermodel = Users::model()->findByPk($userid);
	    $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id));
	    
	    $cri = new CDbCriteria();
	    $cri->condition = "model_id =".$userid." and model_type like '%candidate%' and deleted_at is null";
	    $addressmodel = Addresses::model()->find($cri);
	    
	    $now = new DateTime();
	    
	    if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
	        $usermodel->setScenario('confirm');
	        
	        if(isset($_POST['Users'])){
	            $usermodel->attributes=$_POST['Users'];
	        }
	        if(isset ($_POST["Addresses"])){
	            $addressmodel->attributes = $_POST["Addresses"];
	        }
	        if(isset ($_POST["Profiles"])){
	            $profilemodel->attributes = $_POST["Profiles"];
	        }
	        if(isset($_POST['Users']))
	        {
	            $valid = $usermodel->validate();
	            // 	            $valid = $model->validate() && $valid;
	            $valid = $addressmodel->validate() && $valid;
	            
	            if($valid){
	                $usermodel->updated_at = $now->format('Y-m-d H:i:s');
	                $usermodel->location = $_POST['Addresses']['county'].($_POST['Addresses']['county']!=''? ', '.$_POST['Addresses']['country']: '');
	                $usermodel->location_lat = $_POST['Addresses']['latitude'];
	                $usermodel->location_lng = $_POST['Addresses']['longitude'];
	                // 	                $usermodel->tel = $_POST['Employers']['tel'];
	                $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
	                $usermodel->save();
	                
	                $profilemodel->slug = $this->getValidatedSlug($usermodel->forenames."-".$usermodel->surname);
	                $profilemodel->save();
	                //address update
	                $addressmodel->updated_at = $now->format('Y-m-d H:i:s');
	                if ($addressmodel->save()) {
	                    
	                    $this->redirect($this->createUrl('agencyStaff/edit', array('id' => $agencystaffmodel->id)));
	                    
	                }
	                
	            }
	        }
	        
	        $this->render("candidateform",array('model'=>$agencymodel,'agencycanmodel'=>$agencycanmodel,'usermodel'=>$usermodel,'profilemodel' => $profilemodel,'addressmodel'=>$addressmodel));
	}
	
	public function actiondeleteCandidates()
	{
	    $idstring = implode(',', $_POST['ids']);
	    $ids = $_POST['ids'];
	    $now = new DateTime();
	    
	    for($i = 0; $i < count($ids); $i++)
	    {
	        $astaffcanmodel = AgencyStaffCandidates::model()->findByPk($ids[$i]);
	        $acanid = $astaffcanmodel->agency_candidate_id;
	        $acanmodel = AgencyCandidates::model()->findByPk($astaffcanmodel->agency_candidate_id);
	        $umodel = Users::model()->findByPk($acanmodel->user_id);
	        $cri = new CDbCriteria();
	        $cri->condition = "owner_id =".$umodel->id." and owner_type like '%candidate%'";
	        
	        Profiles::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')),$cri);
	        Users::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')),'id =:id',array(':id'=>$umodel->id));
	        AgencyStaffCandidates::model()->deleteByPk($astaffcanmodel->id);
	        AgencyCandidates::model()->deleteByPk($acanid);
	        
	    }
	    
	}
	
	public function actionInbox() {
	    $this->layout = "main";
	    $agencystaffmodel = AgencyStaff::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
	    $agencymodel = Agency::model()->findByPk($agencystaffmodel->agency_id);
	    $model = Users::model()->findByPk($agencystaffmodel->user_id);
	    $this->render("inbox", array('model' => $model,'agencymodel'=>$agencymodel));
	}
	
	public function actiongetInboxMessages() {
	    $this->layout = "main";
	    $convmodel = Conversations::model()->findByPk($_POST['id']);
	    $this->renderPartial('inbox_messages', array('convmodel' => $convmodel, 'to_user' => $_POST['msg_to']));
	}
	
	public function actionsendMessage() {
	    $now = new DateTime();
	    $conmodel = new Conversations();
	    $conmodel->subject = $_POST['subject'];
	    $conmodel->created_at = $now->format('Y-m-d H:i:s');
	    $conmodel->updated_at = $now->format('Y-m-d H:i:s');
	    if ($conmodel->save()) {
	        $usermodel = Users::model()->findByPk(Yii::app()->user->getState('userid'));
	        $conmsgmodel = new ConversationMessages();
	        $conmsgmodel->conversation_id = $conmodel->id;
	        $conmsgmodel->user_id = Yii::app()->user->getState('userid');
	        $conmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
	        $conmsgmodel->message = $_POST['message'];
	        $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
	        $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
	        if ($conmsgmodel->save()) {
	            $conmemmodel_sender = new ConversationMembers();
	            $conmemmodel_sender->conversation_id = $conmodel->id;
	            $conmemmodel_sender->user_id = Yii::app()->user->getState('userid');
	            $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
	            $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
	            $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
	            $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
	            if ($conmemmodel_sender->save()) {
	                $usermodel1 = Users::model()->findByPk($_POST['user_id']);
	                $conmemmodel_rec = new ConversationMembers();
	                $conmemmodel_rec->conversation_id = $conmodel->id;
	                $conmemmodel_rec->user_id = $_POST['user_id'];
	                $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
	                $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
	                $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
	                if ($conmemmodel_rec->save()) {
	                    $actmodel_sender = new Activities();
	                    $actmodel_sender->user_id = Yii::app()->user->getState('userid');
	                    $actmodel_sender->type = "message";
	                    $actmodel_sender->model_id = $conmsgmodel->id;
	                    $actmodel_sender->model_type = "ConversationMessage";
	                    $actmodel_sender->subject = "You sent a message in conversation: " . $_POST['subject'];
	                    $actmodel_sender->message = $_POST['message'];
	                    $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
	                    $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
	                    if ($actmodel_sender->save()) {
	                        $actmodel_rec = new Activities();
	                        $actmodel_rec->user_id = $_POST['user_id'];
	                        $actmodel_rec->type = "message";
	                        $actmodel_rec->model_id = $conmsgmodel->id;
	                        $actmodel_rec->model_type = "ConversationMessage";
	                        $actmodel_rec->subject = "You received a message in conversation: " . $_POST['subject'];
	                        $actmodel_rec->message = $_POST['message'];
	                        $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
	                        $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
	                        $actmodel_rec->save();
	                    }
	                }
	            }
	        }
	    }
	}
	
	public function actionsaveMessage() {
	    $this->layout = "main";
	    $conid = $_POST['id'];
	    $fromuser = Yii::app()->user->getState('userid');
	    $touser = $_POST['msg_to'];
	    $msg = $_POST['message'];
	    $now = new DateTime();
	    
	    $convmodel = Conversations::model()->findByPk($_POST['id']);
	    $usermodel = Users::model()->findByPk($fromuser);
	    
	    $convmsgmodel = new ConversationMessages();
	    $convmsgmodel->conversation_id = $conid;
	    $convmsgmodel->user_id = $fromuser;
	    $convmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
	    $convmsgmodel->message = $msg;
	    $convmsgmodel->created_at = $now->format('Y-m-d H:i:s');
	    $convmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
	    if ($convmsgmodel->save()) {
	        $conmemmodel_sender = new ConversationMembers();
	        $conmemmodel_sender->conversation_id = $convmodel->id;
	        $conmemmodel_sender->user_id = Yii::app()->user->getState('userid');
	        $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
	        $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
	        $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
	        $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
	        if ($conmemmodel_sender->save()) {
	            $usermodel1 = Users::model()->findByPk($_POST['user_id']);
	            $conmemmodel_rec = new ConversationMembers();
	            $conmemmodel_rec->conversation_id = $convmodel->id;
	            $conmemmodel_rec->user_id = $_POST['msg_to'];
	            $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
	            $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
	            $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
	            if ($conmemmodel_rec->save()) {
	                $activitymodel = new Activities();
	                $activitymodel->user_id = $fromuser;
	                $activitymodel->type = "message";
	                $activitymodel->model_id = $convmsgmodel->id;
	                $activitymodel->model_type = "App\Messaging\ConversationMessage";
	                $activitymodel->subject = "You sent a message in conversation: " . $convmodel->subject;
	                $activitymodel->message = $msg;
	                $activitymodel->created_at = $now->format('Y-m-d H:i:s');
	                $activitymodel->updated_at = $now->format('Y-m-d H:i:s');
	                $activitymodel->save();
	                
	                $activitymodel = new Activities();
	                $activitymodel->user_id = $touser;
	                $activitymodel->type = "message";
	                $activitymodel->model_id = $convmsgmodel->id;
	                $activitymodel->model_type = "App\Messaging\ConversationMessage";
	                $activitymodel->subject = "You sent a message in conversation: " . $convmodel->subject;
	                $activitymodel->message = $msg;
	                $activitymodel->created_at = $now->format('Y-m-d H:i:s');
	                $activitymodel->updated_at = $now->format('Y-m-d H:i:s');
	                $activitymodel->save();
	            }
	        }
	    }
	    Yii::app()->user->setFlash('success', 'Message sent successfully');
	    $this->renderPartial('inbox_messages', array('convmodel' => $convmodel, 'to_user' => $_POST['msg_to']));
	}
	
	/**
	 * Notify the user
	 */
	public function notifycandidate($model, $password) {
	    $body = $this->renderPartial("//emails/user-account", array('user' => $model, 'password' => $password), true);
	    // Send register notifcation to employer admin
	    Yii::import('application.extensions.phpmailer.JPhpMailer');
	    $mail = new JPhpMailer;
	    $mail->Host = 'smtp.gmail.com';
	    $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
	    $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
	    $mail->SMTPSecure = 'ssl';
	    $mail->SetFrom('mail@cvvid.com', 'CVVID');
	    $mail->Subject = 'Your account details for CVVid.com';
	    $mail->MsgHTML($body);
	    $mail->AddAddress($model->email, $model->forenames . " " . $model->surname);
	    $mail->Send();
	}
	public function getValidatedSlug($slug)
	{
	    $cri = new CDbCriteria();
	    $cri->condition = "slug ='".$slug."'";
	    $pmodel = Profiles::model()->findAll($cri);
	    
	    $cri1 = new CDbCriteria();
	    $cri1->condition = "slug like '".$slug."-%'";
	    $pmodel1 = Profiles::model()->findAll($cri1);
	    
	    if(count($pmodel1) == 0 && count($pmodel) == 1)
	    {
	        return strtolower($slug)."-1";
	    }
	    else if(count($pmodel1) > 0)
	    {
	        return (strtolower($slug)."-".(count($pmodel1)+1));
	    }
	    else if(count($pmodel) == 0)
	        return strtolower($slug);
	        else
	            return (strtolower($slug)."-".rand());
	            
	}
	
	function actionApplications($id)
	{
	    $this->layout = 'main';
	    $model = Jobs::model()->findByPk($id);
	    $jobappmodel = JobApplications::model()->findAllByAttributes(array('job_id'=>$model->id));
	    $this->render('_jobsapplication',array('model'=>$model,'jobappmodel'=>$jobappmodel));
	}
	
	function actionShortlists($id)
	{
	    $this->layout = 'main';
	    $model = Jobs::model()->findByPk($id);
	    $jobappmodel = JobApplications::model()->findAllByAttributes(array('job_id'=>$model->id));
	    $this->render('_jobshortlisted',array('model'=>$model,'jobappmodel'=>$jobappmodel));
	}
}
