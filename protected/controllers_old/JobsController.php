<?php

class JobsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
        
        /**
	 * Manages all models.
	 */
	public function actionsearchAdmin()
	{
	    $model=new Jobs('search');
	    $model->unsetAttributes();  // clear any default values
	    if(isset($_GET['Jobs']))
	    {
	        $model->attributes=$_GET['Jobs'];	        
	    } 
	    
        $this->render('admin',array(
            'model'=>$model,'issearch'=>1));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Jobs;
                $employermodel = Employers::model();
                $industrymodel = Industries::model();
                $skillcategmodel = SkillCategories::model();
                $skillsmodel = Skills::model();
                $jobskillmodel = JobSkill::model();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Jobs']))
		{
			$model->attributes=$_POST['Jobs'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,'empmodel'=>$employermodel,'industrymodel'=>$industrymodel,'skillcategorymodel'=>$skillcategmodel,'skillmodel'=>$skillsmodel,'jobskillmodel'=>$jobskillmodel
		));
	}
        
        /**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actioncreateJobs() {
// 	   echo var_dump($_POST);die();
        $model = new Jobs;
        $employermodel = Employers::model();
        $industrymodel = Industries::model();
        $skillcategmodel = SkillCategories::model();
        $skillsmodel = Skills::model();
        $jobskillmodel = JobSkill::model();

        $now = new DateTime();


        if (isset($_POST["Jobs"])) {
            $model->attributes = $_POST["Jobs"];
        }
       
        if (isset($_POST['Jobs'])) {
            if(!isset($_POST['skills']))
                $model->setScenario('skillsrequired');
    
             if(isset($_POST['competitive_salary']))
                {
                    $model->competitive_salary = 1;
                    $model->salary_min = 0;
                    $model->salary_max = 0;
                }
                
            $valid = $model->validate();

            if ($valid) {
                //  insert jobs details
                $model->attributes = $_POST["Jobs"];
                $model->owner_type = 'employer';
                $model->user_id = Yii::app()->user->getState('userid');
                if($_POST["industry_id"] > 0)
                 $model->industry_id = $_POST["industry_id"];
                $d = str_replace('/', '-', $_POST["end_date"]);
                $endformat = date('Y-m-d', strtotime($d));
                $model->video_only = isset($_POST["video_only"]) ? 1 : 0;
                $model->end_date = $endformat;
                //$model->skill_category_id = $_POST["skill_category_id"];
                //$model->type = $_POST["type"];
                //$model->description = trim($_POST["description"]);
                //$model->additional = $_POST["additional"];
               // $model->location_lat = $_POST["location_lat"];
               // $model->location_lng = $_POST["location_lng"];
                $model->created_at = $now->format('Y-m-d H:i:s');
               $model->salary_type = $_POST["salary_type"];
                if ($model->save()) {
                                if(isset($_POST["careers"]))
                       {
                           $careerjob = new Jobs();
                           $careerjob->attributes = $model->attributes;
                           $careerjob->careers = 1;
                           if($careerjob->save())
                           {
                               if(isset($_POST['skills']))
                               {
                                   $skills = $_POST['skills'];
                                   foreach ($skills as $skills_val){
                                       $jobskillmodel=new JobSkill;
                                       $jobskillmodel->job_id = $careerjob->id;
                                       $jobskillmodel->skill_id = $skills_val;
                                       if(isset($_POST[$skills_val]))
                                           $jobskillmodel->vacancies = $_POST[$skills_val];
                                           $jobskillmodel->save();
                                           
                                   }
                                }
                             }
                         }

                    $skills = $_POST['skills'];
                    foreach ($skills as $skills_val) {
                        $jobskillmodel = new JobSkill;
                        $jobskillmodel->job_id = $model->id;
                        $jobskillmodel->skill_id = $skills_val;
                        if(isset($_POST[$skills_val]))
                            $jobskillmodel->vacancies = $_POST[$skills_val];
                        $jobskillmodel->save();
                    }
                    if(isset($_FILES['jdfile']['name']) && $_FILES['jdfile']['name'] != "")
                    {
                        $mediamodel = new Media();
                        $mediamodel->model_type = "Employer";
                        $mediamodel->model_id = $model->owner_id;
                        $mediamodel->collection_name = "documents";
                        $mediamodel->name = pathinfo($_FILES['jdfile']['name'], PATHINFO_FILENAME);
                        $mediamodel->file_name = $_FILES['jdfile']['name'];
                        $mediamodel->size = $_FILES['jdfile']['size'];
                        $mediamodel->disk = "documents";
                        $mediamodel->created_at = $now->format('Y-m-d H:i:s');
                        if($mediamodel->save())
                        {
                            $mediafolder = "images/media/" . $mediamodel->id . "/conversions";
                            
                            if (!file_exists($mediafolder)) {
                                mkdir($mediafolder, 0777, true);
                            }
                            $filename = $_FILES['jdfile']['name'];
                            $fullpath = "images/media/" . $mediamodel->id . "/" . $filename;
                            if (move_uploaded_file($_FILES['jdfile']['tmp_name'], $fullpath)) {
                                
                            }
                            Jobs::model()->updateByPk($model->id,array('media_id'=>$mediamodel->id));
                        }
                    }
                    Yii::app()->user->setFlash('success', 'Successfully created job');
                    $this->redirect($this->createUrl('jobs/admin'));
                }
            }
        }

        $this->render('create', array(
            'model' => $model, 'empmodel' => $employermodel, 'industrymodel' => $industrymodel, 'skillcategorymodel' => $skillcategmodel, 'skillmodel' => $skillsmodel, 'jobskillmodel' => $jobskillmodel
        ));
    }

    public function actiondeleteJobs(){
            
            $ids = implode(',', $_POST['ids']);
                                  
            $now = new DateTime();
            
            $cri = new CDbCriteria();
            $cri->condition = "id in (".$ids.")";
            Jobs::model()->deleteAll($cri);
            
            $cri1 = new CDbCriteria();
            $cri1->condition = "job_id in (".$ids.")";
            JobSkill::model()->deleteAll($cri1);
            
           
        }


        /**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionjobsUpdate($id)
	{
	   
            $now = new DateTime();
            
            $model = Jobs::model()->findByPk($id);
            $employermodel = Employers::model()->findByPk($model->owner_id);
	        $jobskillmodel = JobSkill::model()->findAllByAttributes(array('job_id'=>$model->id));
//            $usermodel = Users::model()->findByPk($empmodel->user_id);
            
            if (isset($_POST["Jobs"])) {
               $model->attributes = $_POST["Jobs"];
            }
            
            if (isset($_POST['Jobs'])) {
                if(!isset($_POST['skills']))
                    $model->setScenario('skillsrequired');
 if(isset($_POST['competitive_salary']))
                    {
                        $model->competitive_salary = 1;
                        $model->salary_min = 0;
                        $model->salary_max = 0;
                    }

                $valid = $model->validate();

                if ($valid) {
                    
                    if($_POST["industry_id"] > 0)
                        $model->industry_id = $_POST["industry_id"];
                    $d = str_replace('/','-',$_POST["end_date"]);
                    $endformat = date('Y-m-d' , strtotime($d));
                   // $endformat = date_format(new DateTime($_POST["end_date"]),"Y-m-d");
                    $model->video_only = isset($_POST["video_only"]) ? 1 : 0;
                   $model->salary_type = $_POST["salary_type"];
	                $model->end_date = $endformat;
                    $model->updated_at = $now->format('Y-m-d H:i:s');
                    if($model->save())
                    {
                        
                        $skills = $_POST['skills'];
                        
                        $jkillmodel= JobSkill::model()->deleteAllByAttributes(array('job_id'=>$model->id));
                        
                        foreach ($skills as $skills_val){
                            $jobskillmodel=new JobSkill;
                            $jobskillmodel->job_id = $model->id;
                            $jobskillmodel->skill_id = $skills_val;
                            $jobskillmodel->save();
                        }
                        if(isset($_FILES['jdfile']['name']) && $_FILES['jdfile']['name'] != "")
                        {
                            if($model->media_id > 0)
                            {
                                $mediamodel = Media::model()->findByPk($model->media_id);
                                $mediamodel->updated_at = $now->format('Y-m-d H:i:s');
                            }
                            else
                            {
                                $mediamodel = new Media();
                                $mediamodel->created_at = $now->format('Y-m-d H:i:s');
                            }
                            $mediamodel->model_type = "Employer";
                            $mediamodel->model_id = $id;
                            $mediamodel->collection_name = "documents";
                            $mediamodel->name = pathinfo($_FILES['jdfile']['name'], PATHINFO_FILENAME);
                            $mediamodel->file_name = $_FILES['jdfile']['name'];
                            $mediamodel->size = $_FILES['jdfile']['size'];
                            $mediamodel->disk = "documents";
                            
                            if($mediamodel->save())
                            {
                                $mediafolder = "images/media/" . $mediamodel->id . "/conversions";
                                
                                if (!file_exists($mediafolder)) {
                                    mkdir($mediafolder, 0777, true);
                                }
                                $filename = $_FILES['jdfile']['name'];
                                $fullpath = "images/media/" . $mediamodel->id . "/" . $filename;
                                if (move_uploaded_file($_FILES['jdfile']['tmp_name'], $fullpath)) {
                                    
                                }
                                Jobs::model()->updateByPk($model->id,array('media_id'=>$mediamodel->id));
                            }
                        }
                        Yii::app()->user->setFlash('success', 'Successfully updated job');
                        $this->redirect($this->createUrl('jobs/admin'));
                       
                    }
                }
            }
            
            $this->render('jobsEdit',array('model'=>$model,'employermodel'=>$employermodel,'jobskillmodel'=>$jobskillmodel));
                
        }
        
        
         /**
	 * Manages Employers Edit.
	 */
        function actionupdateJobs($id)
	{
	    $model = Jobs::model()->findByPk($id);
	    $employermodel = Employers::model()->findByPk($model->owner_id);
	    $jobskillmodel = JobSkill::model()->findAllByAttributes(array('job_id'=>$model->id));
       	    $this->render('jobsEdit',array('model'=>$model,'employermodel'=>$employermodel,'jobskillmodel'=>$jobskillmodel));
	}
        
        function actionjobsView($id)
	{
	    $model = Jobs::model()->findByPk($id);
	    $employermodel = Employers::model()->findByPk($model->owner_id);
	    $jobskillmodel = JobSkill::model()->findAllByAttributes(array('job_id'=>$model->id));
       	    $this->render('jobsDetails',array('model'=>$model,'employermodel'=>$employermodel,'jobskillmodel'=>$jobskillmodel));
	}
        
        function actionApplications($id)
	{
            $this->layout = 'main';
	    $model = Jobs::model()->findByPk($id);
	    $jobappmodel = JobApplications::model()->findAllByAttributes(array('job_id'=>$model->id));
       	    $this->render('_jobsapplication',array('model'=>$model,'jobappmodel'=>$jobappmodel));
	}
        
        function actionShortlists($id)
	{
            $this->layout = 'main';
	    $model = Jobs::model()->findByPk($id);
	    $jobappmodel = JobApplications::model()->findAllByAttributes(array('job_id'=>$model->id));
       	    $this->render('_jobshortlisted',array('model'=>$model,'jobappmodel'=>$jobappmodel));
	}
        
        function actionInterviews($id)
	{
            $this->layout = 'main';
	    $model = Jobs::model()->findByPk($id);
	    $jobappmodel = JobApplications::model()->findAllByAttributes(array('job_id'=>$model->id));
       	    $this->render('_jobinterviews',array('model'=>$model,'jobappmodel'=>$jobappmodel));
	}
        
        
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Jobs']))
		{
			$model->attributes=$_POST['Jobs'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Jobs');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Jobs('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Jobs']))
			$model->attributes=$_GET['Jobs'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
        
     public function actionloadSkills()
     {
		$model = Skills::model()->findAllByAttributes(array('skill_category_id'=>$_POST['id'])); 
		  if(Yii::app()->user->getState('role') != "admin")
		  {
		      $empmodel = Employers::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid')));
            foreach ($model as $mcat) {
                $tmpmodel = TempSkills::model()->findByAttributes(array('skill_id'=>$mcat['id'],'skill_category_id'=>$mcat['skill_category_id']));
                if($tmpmodel == null || $tmpmodel['employer_id'] == $empmodel->id)
                {
                    echo "<option  value='$mcat[id]'>$mcat[name]</option>";
                }
            }
		  }
		  else {
		      foreach ($model as $mcat) {
		          echo "<option  value='$mcat[id]'>$mcat[name]</option>";		       
		      }
		  }
		  
         //   echo $this->renderPartial('loadSkills',array('model'=>$docmodel));
			
    }

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Jobs the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Jobs::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Jobs $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='jobs-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
// 	public function beforeAction($action)
// 	{
// 	    Yii::app()->user->setReturnUrl(Yii::app()->request->getUrl());
// 	    $requestUrl=Yii::app()->request->url;
// 	    if (Yii::app()->user->isGuest)
// 	        $this->redirect(Yii::app()->createUrl('site/login'));
	        
// 	        return parent::beforeAction($action);
// 	} 
	public function actionviewJob($id)
	{
	    $this->layout = '//layouts/main';
            
            $jobmodel = Jobs::model()->findByPk($id);
            $job_id = $jobmodel->id;
            $now = new DateTime();
            
            if(Yii::app()->user->getState('role')=='candidate'):
            // $this->layout = "profile";
            $type = ucfirst(Yii::app()->user->getState('role'));
            $model_id = Yii::app()->user->getState('userid');
            $cri = new CDbCriteria();
            $cri->condition = "job_id =" . $job_id . " and model_type like '%" . $type . "%' and model_id=" . $model_id;

            $jobviews = JobViews::model()->find($cri);
            if ($jobviews == null) {
                $jbmodel = new JobViews();
                $jbmodel->job_id = $job_id;
                $jbmodel->model_type = ucfirst(Yii::app()->user->getState('role'));
                $jbmodel->model_id = Yii::app()->user->getState('userid');
                $jbmodel->view_count = 1;
                $jbmodel->created_at = $now->format('Y-m-d H:i:s');
                $jbmodel->save();
                
                $pcri = new CDbCriteria();
                $pcri->select = 'sum(view_count) as view_count';
                $pcri->condition = "job_id =" . $job_id;
                $jobviews = JobViews::model()->find($pcri);

                $cri1 = new CDbCriteria();
                $cri1->condition = "id =" . $job_id;
                Jobs::model()->updateAll(array('updated_at' => $now->format('Y-m-d H:i:s'),'num_views'=>$jobviews['view_count']),$cri1);
                
                $jobmodel = Jobs::model()->findByPk($job_id);
                
                $empuserid = $jobmodel->user_id;
                
                $umodel = Users::model()->findByPk(Yii::app()->user->getState('userid'));
                $pmodel = Profiles::model()->findByAttributes(array('owner_id'=>Yii::app()->user->getState('userid'),'type'=>$umodel->type));
                $candidate_url = $this->createUrl('users/profile',array('slug'=>$pmodel->slug));
                $displayName = Yii::app()->user->getState('username'); 
                /* send message */
                $message = "Candidate <a target='_blank' href='$candidate_url'>$displayName</a> viewed your job";

               // $this->sendMessageE("A Candidate viewed your Job",$message, $umodel->id, $empuserid);
                
                //$subject,$message,$from,$to
                $subject = "A Candidate viewed your Job";
                $from = $umodel->id;
                $to = $empuserid; 
                
                $now = new DateTime();
                $conmodel = new Conversations();
                $conmodel->subject = $subject;
                $conmodel->created_at = $now->format('Y-m-d H:i:s');
                $conmodel->updated_at = $now->format('Y-m-d H:i:s');
                if ($conmodel->save()) {
                    $usermodel = Users::model()->findByPk($from);
                    $conmsgmodel = new ConversationMessages();
                    $conmsgmodel->conversation_id = $conmodel->id;
                    $conmsgmodel->user_id = $from;
                    $conmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
                    $conmsgmodel->message = $message;
                    $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
                    $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
                    if ($conmsgmodel->save()) {
                        $conmemmodel_sender = new ConversationMembers();
                        $conmemmodel_sender->conversation_id = $conmodel->id;
                        $conmemmodel_sender->user_id = $from;
                        $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
                        $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
                        $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                        $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                        if ($conmemmodel_sender->save()) {
                            $usermodel1 = Users::model()->findByPk($to);
                            $conmemmodel_rec = new ConversationMembers();
                            $conmemmodel_rec->conversation_id = $conmodel->id;
                            $conmemmodel_rec->user_id = $to;
                            $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
                            $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                            $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                            if ($conmemmodel_rec->save()) {
                                $actmodel_sender = new Activities();
                                $actmodel_sender->user_id = $from;
                                $actmodel_sender->type = "message";
                                $actmodel_sender->model_id = $conmsgmodel->id;
                                $actmodel_sender->model_type = "ConversationMessage";
                                $actmodel_sender->subject = "You sent a message in conversation: " . $subject;
                                $actmodel_sender->message = $message;
                                $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                                $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                                if ($actmodel_sender->save()) {
                                    $actmodel_rec = new Activities();
                                    $actmodel_rec->user_id = $to;
                                    $actmodel_rec->type = "message";
                                    $actmodel_rec->model_id = $conmsgmodel->id;
                                    $actmodel_rec->model_type = "ConversationMessage";
                                    $actmodel_rec->subject = "You received a message in conversation: " . $subject;
                                    $actmodel_rec->message = $message;
                                    $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                                    $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                                    $actmodel_rec->save();
                                }
                            }
                        }
                    }
                }
                
            } else {
                $pcri = new CDbCriteria();
                $pcri->select = 'sum(view_count) as view_count';
                $pcri->condition = "job_id =" . $job_id;
                $jobviews = JobViews::model()->find($pcri);

                $cri1 = new CDbCriteria();
                $cri1->condition = "id =" . $job_id;
                Jobs::model()->updateAll(array('updated_at' => $now->format('Y-m-d H:i:s'),'num_views'=>$jobviews['view_count']),$cri1);
            }
            endif;
            
            if(Yii::app()->user->isGuest):
               
                $cri = new CDbCriteria();
                $cri->condition = "job_id =" . $job_id . " and model_type like '" . Yii::app()->request->getUserHostAddress() . "' and model_id=" . 0;    
                    
                $jobviews1 = JobViews::model()->find($cri);
                
                if ($jobviews1 == null) {
                $jbmodel = new JobViews();
                $jbmodel->job_id = $job_id;
                $jbmodel->model_type = Yii::app()->request->getUserHostAddress();
                $jbmodel->model_id = 0;
                $jbmodel->view_count = 1;
                $jbmodel->created_at = $now->format('Y-m-d H:i:s');
                $jbmodel->save();

                $pcri = new CDbCriteria();
                $pcri->select = 'sum(view_count) as view_count';
                $pcri->condition = "job_id =" . $job_id;
                $jobviews = JobViews::model()->find($pcri);

                $cri1 = new CDbCriteria();
                $cri1->condition = "id =" . $job_id;
                Jobs::model()->updateAll(array('updated_at' => $now->format('Y-m-d H:i:s'), 'num_views' => $jobviews['view_count']), $cri1);
            }

        endif;
                
            
            
	    $this->render('viewJob',array(
	        'model'=>$jobmodel
	    ));
	}
	/*public function actionapply($id)
	{
	    $this->layout = '//layouts/main';
	    $now = new DateTime();
	    $userid = Yii::app()->user->getState('userid');
// 	    $summary = trim($_POST['summary']);
        $jmodel = new JobApplications();
        $jmodel->job_id = $id;
        $jmodel->user_id = $userid;
        $jmodel->status = 0;
        if(trim($_POST['summary']) != "")
            $jmodel->summary = trim($_POST['summary']);
        if(trim($_POST['reference']) != "")
            $jmodel->reference_mail = trim($_POST['reference']);
        $jmodel->created_at = $now->format('Y-m-d H:i:s');
        if($jmodel->save())
        {
			$from = $userid;
            $usermodel = Users::model()->findByPk($from);
			
            $jobmodel = Jobs::model()->findByPk($id);
            $empmodel = Employers::model()->findByPk($jobmodel->owner_id);
            //send message to employer inbox
             $subject = "APPLIED FOR THE VACANCY";
            $message = "Application from :".$usermodel->forenames." ".$usermodel->surname."<br>";
            $message += "Job Role :".$jobmodel->title."<br>";
            $message += "Message :".$jmodel->summary."<br>";
            
            //$this->sendMessage($subject,$message,$userid,$empmodel->user_id);
            
            $to = $empmodel->user_id;
            
            $now = new DateTime();
            $conmodel = new Conversations();
            $conmodel->subject = $subject;
            $conmodel->created_at = $now->format('Y-m-d H:i:s');
            $conmodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($conmodel->save()) {
               
                $conmsgmodel = new ConversationMessages();
                $conmsgmodel->conversation_id = $conmodel->id;
                $conmsgmodel->user_id = $from;
                $conmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
                $conmsgmodel->message = $message;
                $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
                $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
                if ($conmsgmodel->save()) {
                    $conmemmodel_sender = new ConversationMembers();
                    $conmemmodel_sender->conversation_id = $conmodel->id;
                    $conmemmodel_sender->user_id = $from;
                    $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
                    $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
                    $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                    $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                    if ($conmemmodel_sender->save()) {
                        $usermodel1 = Users::model()->findByPk($to);
                        $conmemmodel_rec = new ConversationMembers();
                        $conmemmodel_rec->conversation_id = $conmodel->id;
                        $conmemmodel_rec->user_id = $to;
                        $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
                        $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                        $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                        if ($conmemmodel_rec->save()) {
                            $actmodel_sender = new Activities();
                            $actmodel_sender->user_id = $from;
                            $actmodel_sender->type = "message";
                            $actmodel_sender->model_id = $conmsgmodel->id;
                            $actmodel_sender->model_type = "ConversationMessage";
                            $actmodel_sender->subject = "You sent a message in conversation: " . $subject;
                            $actmodel_sender->message = "";
                            $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                            $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                            if ($actmodel_sender->save()) {
                                $actmodel_rec = new Activities();
                                $actmodel_rec->user_id = $to;
                                $actmodel_rec->type = "message";
                                $actmodel_rec->model_id = $conmsgmodel->id;
                                $actmodel_rec->model_type = "ConversationMessage";
                                $actmodel_rec->subject = "You received a message in conversation: " . $subject;
                                $actmodel_rec->message = "Candidate ".$usermodel->forenames." ".$usermodel->surname.$message;
                                $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                                $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                                $actmodel_rec->save();
                            }
                        }
                    }
                }
            }
            
            
            //send summary message to employer inbox
            $subject1 = "JOB SUMMARY FOR THE POST - ".$jobmodel->title;
            $message1 = $jmodel->summary;
             $this->sendMessage($subject1,$message1,$userid,$empmodel->user_id); 
            
            $model = Users::model()->findByPk($userid);
            $body = $this->renderPartial("//emails/job-applied", array('model'=>$model), true);
            // Send employer viewed notifcation to candidate
            Yii::import('application.extensions.phpmailer.JPhpMailer');
            $mail = new JPhpMailer;
            $mail->Host = 'smtp.gmail.com';
            $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
            $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
            $mail->SMTPSecure = 'ssl';
            $mail->SetFrom('mail@cvvid.com', 'CVVID');
            $mail->Subject = "Applied for Job - Confirmation mail";
            $mail->MsgHTML($body);
            $mail->AddAddress($model->email, $model->forenames);
            $mail->Send();
            
            
            //mail to employer for job applied            
           
            $body = $this->renderPartial("//emails/employer-job-applied", array('model'=>$empmodel,'umodel'=>$model,'jmodel'=>$jmodel,'jobmodel'=>$jobmodel), true);
            Yii::import('application.extensions.phpmailer.JPhpMailer');
            $mail = new JPhpMailer;
            $mail->Host = 'smtp.gmail.com';
            $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
            $mail->Password = Yii::app()->params['EMAIL_PASSWORD']; 
            $mail->SMTPSecure = 'ssl';
            $mail->SetFrom('mail@cvvid.com', 'CVVID');
            $mail->Subject = "CANDIDATE HAS APPLIED FOR THE VACANCY YOU POSTED - LOG IN TO VIEW THEIR VIDEO AND PROFILE";
            $mail->MsgHTML($body);
            $mail->AddAddress($empmodel->email, $empmodel->name);
            $mail->Send();
            
            $this->redirect($this->createUrl('jobs/viewJob',array('id'=>$id,'title'=>  str_replace(' ', '-', $jobmodel->title))));
        }
	}*/
	public function actionapply($id)
	{
	    $this->layout = '//layouts/main';
	    $now = new DateTime();
	    $userid = Yii::app()->user->getState('userid');
	    
	    
// 	    $summary = trim($_POST['summary']);
        $jmodel = new JobApplications();
        $jmodel->job_id = $id;
        $jmodel->user_id = $userid;
        $jmodel->status = 0;
        if(trim($_POST['summary']) != "")
            $jmodel->summary = trim($_POST['summary']);
        if(trim($_POST['reference']) != "")
            $jmodel->reference_mail = trim($_POST['reference']);
        $jmodel->created_at = $now->format('Y-m-d H:i:s');
        if($jmodel->save())
        {
            $from = $userid;
            $usermodel = Users::model()->findByPk($from);
            
            $jobmodel = Jobs::model()->findByPk($id);
            $empmodel = Employers::model()->findByPk($jobmodel->owner_id);
			 if($empmodel == null)
                $empmodel = Agency::model()->findByPk($jobmodel->owner_id);
            //send message to employer inbox
            $subject = "APPLIED FOR THE VACANCY";
            
            //check recruitment candidate
            $agecanmodel = AgencyCandidates::model()->findByAttributes(array('user_id'=>$userid));
            if($agecanmodel != null)
            {
                $user_name = "XXXX";
                
            }
            else {
                $user_name = $usermodel->forenames." ".$usermodel->surname;
            }
            //ends
            $message = "Application from :".$user_name."<br>";
            $message .= "Job Role :".$jobmodel->title."<br>";
            $message .= "Message :".$jmodel->summary."<br>";
            
            //$this->sendMessage($subject,$message,$userid,$empmodel->user_id);
           
            $to = $empmodel->user_id;
            
            $now = new DateTime();
            $conmodel = new Conversations();
            $conmodel->subject = $subject;
            $conmodel->created_at = $now->format('Y-m-d H:i:s');
            $conmodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($conmodel->save()) {
                
                $conmsgmodel = new ConversationMessages();
                $conmsgmodel->conversation_id = $conmodel->id;
                $conmsgmodel->user_id = $from;
                $conmsgmodel->name = $user_name;
                $conmsgmodel->message = $message;
                $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
                $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
                if ($conmsgmodel->save()) {
                    $conmemmodel_sender = new ConversationMembers();
                    $conmemmodel_sender->conversation_id = $conmodel->id;
                    $conmemmodel_sender->user_id = $from;
                    $conmemmodel_sender->name = $user_name;
                    $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
                    $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                    $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                    if ($conmemmodel_sender->save()) {
                        $usermodel1 = Users::model()->findByPk($to);
                        $conmemmodel_rec = new ConversationMembers();
                        $conmemmodel_rec->conversation_id = $conmodel->id;
                        $conmemmodel_rec->user_id = $to;
                        $conmemmodel_rec->name = $user_name;
                        $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                        $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                        if ($conmemmodel_rec->save()) {
                            $actmodel_sender = new Activities();
                            $actmodel_sender->user_id = $from;
                            $actmodel_sender->type = "message";
                            $actmodel_sender->model_id = $conmsgmodel->id;
                            $actmodel_sender->model_type = "ConversationMessage";
                            $actmodel_sender->subject = "You sent a message in conversation: " . $subject;
                            $actmodel_sender->message = "";
                            $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                            $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                            if ($actmodel_sender->save()) {
                                $actmodel_rec = new Activities();
                                $actmodel_rec->user_id = $to;
                                $actmodel_rec->type = "message";
                                $actmodel_rec->model_id = $conmsgmodel->id;
                                $actmodel_rec->model_type = "ConversationMessage";
                                $actmodel_rec->subject = "You received a message in conversation: " . $subject;
                                $actmodel_rec->message = "Candidate ".$user_name.$message;
                                $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                                $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                                $actmodel_rec->save();
                            }
                        }
                    }
                }
            }
            
            
            //send summary message to employer inbox
            $subject1 = "JOB SUMMARY FOR THE POST - ".$jobmodel->title;
            $message1 = $jmodel->summary;
             $this->sendMessage($subject1,$message1,$userid,$empmodel->user_id); 
            
            $model = Users::model()->findByPk($userid);
            $body = $this->renderPartial("//emails/job-applied", array('model'=>$model), true);
            // Send employer viewed notifcation to candidate
            Yii::import('application.extensions.phpmailer.JPhpMailer');
            $mail = new JPhpMailer;
            $mail->Host = 'smtp.gmail.com';
            $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
            $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
            $mail->SMTPSecure = 'ssl';
            $mail->SetFrom('mail@cvvid.com', 'CVVID');
            $mail->Subject = "Applied for Job - Confirmation mail";
            $mail->MsgHTML($body);
            $mail->AddAddress($model->email, $model->forenames);
            $mail->Send();
            
            
            //mail to employer for job applied            
           
            $body = $this->renderPartial("//emails/employer-job-applied", array('model'=>$empmodel,'umodel'=>$model,'jmodel'=>$jmodel,'jobmodel'=>$jobmodel), true);
            Yii::import('application.extensions.phpmailer.JPhpMailer');
            $mail = new JPhpMailer;
            $mail->Host = 'smtp.gmail.com';
            $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
            $mail->Password = Yii::app()->params['EMAIL_PASSWORD']; 
            $mail->SMTPSecure = 'ssl';
            $mail->SetFrom('mail@cvvid.com', 'CVVID');
            $mail->Subject = "CANDIDATE HAS APPLIED FOR THE VACANCY YOU POSTED - LOG IN TO VIEW THEIR VIDEO AND PROFILE";
            $mail->MsgHTML($body);
            $mail->AddAddress($empmodel->email, $empmodel->name);
            $mail->Send();
            
            //$this->redirect($this->createUrl('jobs/viewJob',array('id'=>$id,'title'=>  str_replace(' ', '-', $jobmodel->title))));
        }
	}
	public function actiondownloadJobSpec($id)
	{
	    $model = Jobs::model()->findByPk($id);
	    $mediamodel = Media::model()->findByPk($model->media_id);
	    $path = Yii::app()->baseUrl."/images/media/".$mediamodel->id."/".$mediamodel->file_name;
	    Yii::app()->getRequest()->sendFile( $mediamodel->file_name , file_get_contents("images/media/".$mediamodel->id."/".$mediamodel->file_name) );
	}
	public function actionduplicateJobs($id)
	{
	    $now = new DateTime();
	    $jmodel = Jobs::model()->findByPk($id);
	    $newjobmodel = new Jobs();
	    $newjobmodel->attributes = $jmodel->attributes;
	    $newjobmodel->num_views = 0;
	    $newjobmodel->created_at = $now->format("Y-m-d H:i:s"); 
	    $newjobmodel->save();
	    $this->actionupdateJobs($newjobmodel->id);
	}
	public function actionverifyCandidate($id)
	{
	    $this->layout = '//layouts/main';
	    $jobappmodel = JobApplications::model()->findByPk($id);
	    $this->render('verifycandidate',array(
	        'model'=>$jobappmodel
	    ));
	}
	
	public function actionQuestions($id)
	{
	    $this->layout = 'main';
	    $model = Jobs::model()->findByPk($id);
	    $jobquesmodel = JobQuestions::model()->findAllByAttributes(array('job_id'=>$model->id));
	    $this->render('_jobquestions',array('model'=>$model,'jobquesmodel'=>$jobquesmodel));
	}
	
	public function actionsaveQuestion($id)
	{
       
	    $now = new DateTime();
	    $model = Jobs::model()->findByPk($id);
	    $qmodel = new JobQuestions();
	    $qmodel->question = $_POST['value'];
	    $qmodel->answer_duration = $_POST['duration'];
	    $qmodel->created_at = $now->format('Y-m-d H:i:s');
	    $qmodel->job_id = $id;
	    $qmodel->user_id = 0;
	    $qmodel->save();
	}
        
        public function actionjobstatus($id)
	{
	    $model = Jobs::model()->findByPk($id);
	    $model->status = $_POST['status'];
	    $model->save();
            Yii::app()->user->setFlash('success', 'Successfully updated job');
            $this->redirect(Yii::app()->createUrl('jobs/applications',array('id'=>$id,"title"=>  str_replace(" ", "-", $model->title))));
	}
	
	public function actiondeleteQuestion()
	{
	    $id = $_POST['id'];
	    $qmodel = JobQuestions::model()->deleteByPk($id);
	}
	
	public function actionupdateQuestion()
	{
	    $now = new DateTime();
	    $id = $_POST['id'];
	    $value = $_POST['value'];
	    $qmodel = JobQuestions::model()->findByPk($id);
	    $qmodel->question = $value;
	    $qmodel->answer_duration = $_POST['duration'];
	    $qmodel->updated_at = $now->format('Y-m-d H:i:s');
	    $qmodel->save();
	}
		public function actionAnswers($id)
	{
	    $this->layout = 'main';
	    $model = Jobs::model()->findByPk($id);
	    $jobansmodel = JobAnswers::model()->findAllByAttributes(array('job_id'=>$model->id));
	    $this->render('_jobanswers',array('model'=>$model,'jobansmodel'=>$jobansmodel));
	}
	public function actiongetAnswers($id)
	{
	  
	    
// 	    $jmodel = JobAnswers::model()->findAllByAttributes(array('job_id'=>$id));
// 	    $playlist = array();
// 	    foreach ($jmodel as $ans)
// 	    {
	        $arr = array();
// 	        $quesmodel = JobQuestions::model()->findByPk($ans['question_id']);
// 	        $videomodel = Videos::model()->findByPk($ans['videoid']);
// 	        if($videomodel != null)
// 	        {
                $userid = $_POST['uid'];
                $qmodel = JobQuestions::model()->findByPk($_POST['qid']);
                $amodel = JobAnswers::model()->findByAttributes(array('question_id'=>$qmodel->id,'user_id'=>$userid));
                $videomodel = Videos::model()->findByPk($amodel['videoid']);
    	      
                $url = 'http://' . Yii::app()->params['AWS_BUCKET'] . '.s3.amazonaws.com/' .$videomodel->video_id;
    	        $arr['image'] = "";
    	        $arr['file'] = $url;
    	        $arr['title'] = $qmodel->question;
//         	    $playlist[] = $arr;
// 	        }
// 	    }
	    
            echo json_encode($arr);
	}
	
	/*public function sendMessage($subject,$message,$from,$to) {
	    
	    $now = new DateTime();
	    $conmodel = new Conversations();
	    $conmodel->subject = $subject;
	    $conmodel->created_at = $now->format('Y-m-d H:i:s');
	    $conmodel->updated_at = $now->format('Y-m-d H:i:s');
	    if ($conmodel->save()) {
	        $usermodel = Users::model()->findByPk($from);
	        $conmsgmodel = new ConversationMessages();
	        $conmsgmodel->conversation_id = $conmodel->id;
	        $conmsgmodel->user_id = $from;
	        $conmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
	        $conmsgmodel->message = $message;
	        $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
	        $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
	        if ($conmsgmodel->save()) {
	            $conmemmodel_sender = new ConversationMembers();
	            $conmemmodel_sender->conversation_id = $conmodel->id;
	            $conmemmodel_sender->user_id = $from;
	            $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
	            $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
	            $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
	            $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
	            if ($conmemmodel_sender->save()) {
	                $usermodel1 = Users::model()->findByPk($to);
	                $conmemmodel_rec = new ConversationMembers();
	                $conmemmodel_rec->conversation_id = $conmodel->id;
	                $conmemmodel_rec->user_id = $to;
	                $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
	                $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
	                $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
	                if ($conmemmodel_rec->save()) {
	                    $actmodel_sender = new Activities();
	                    $actmodel_sender->user_id = $from;
	                    $actmodel_sender->type = "message";
	                    $actmodel_sender->model_id = $conmsgmodel->id;
	                    $actmodel_sender->model_type = "ConversationMessage";
	                    $actmodel_sender->subject = "You sent a message in conversation: " . $subject;
	                    $actmodel_sender->message = $message;
	                    $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
	                    $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
	                    if ($actmodel_sender->save()) {
	                        $actmodel_rec = new Activities();
	                        $actmodel_rec->user_id = $to;
	                        $actmodel_rec->type = "message";
	                        $actmodel_rec->model_id = $conmsgmodel->id;
	                        $actmodel_rec->model_type = "ConversationMessage";
	                        $actmodel_rec->subject = "You received a message in conversation: " . $subject;
	                        $actmodel_rec->message = $message;
	                        $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
	                        $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
	                        $actmodel_rec->save();
	                    }
	                }
	            }
	        }
	    }
	}*/
	public function sendMessage($subject,$message,$from,$to) {
	    
	    $now = new DateTime();
	    $conmodel = new Conversations();
	    $conmodel->subject = $subject;
	    $conmodel->created_at = $now->format('Y-m-d H:i:s');
	    $conmodel->updated_at = $now->format('Y-m-d H:i:s');
	    if ($conmodel->save()) {
	        $usermodel = Users::model()->findByPk($from);
	        //check recruitment candidate
	        $agecanmodel = AgencyCandidates::model()->findByAttributes(array('user_id'=>$from));
	        if($agecanmodel != null)
	        {
	            $user_name = "XXXX";
	        }
	        else {
	            
	            $user_name = $usermodel->forenames." ".$usermodel->surname;
	        }
	        //ends
	        
	        $conmsgmodel = new ConversationMessages();
	        $conmsgmodel->conversation_id = $conmodel->id;
	        $conmsgmodel->user_id = $from;
	        $conmsgmodel->name = $user_name;
	        $conmsgmodel->message = $message;
	        $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
	        $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
	        if ($conmsgmodel->save()) {
	            $conmemmodel_sender = new ConversationMembers();
	            $conmemmodel_sender->conversation_id = $conmodel->id;
	            $conmemmodel_sender->user_id = $from;
	            $conmemmodel_sender->name = $user_name;
	            $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
	            $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
	            $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
	            if ($conmemmodel_sender->save()) {
	                $usermodel1 = Users::model()->findByPk($to);
	                $conmemmodel_rec = new ConversationMembers();
	                $conmemmodel_rec->conversation_id = $conmodel->id;
	                $conmemmodel_rec->user_id = $to;
	                $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
	                $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
	                $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
	                if ($conmemmodel_rec->save()) {
	                    $actmodel_sender = new Activities();
	                    $actmodel_sender->user_id = $from;
	                    $actmodel_sender->type = "message";
	                    $actmodel_sender->model_id = $conmsgmodel->id;
	                    $actmodel_sender->model_type = "ConversationMessage";
	                    $actmodel_sender->subject = "You sent a message in conversation: " . $subject;
	                    $actmodel_sender->message = $message;
	                    $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
	                    $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
	                    if ($actmodel_sender->save()) {
	                        $actmodel_rec = new Activities();
	                        $actmodel_rec->user_id = $to;
	                        $actmodel_rec->type = "message";
	                        $actmodel_rec->model_id = $conmsgmodel->id;
	                        $actmodel_rec->model_type = "ConversationMessage";
	                        $actmodel_rec->subject = "You received a message in conversation: " . $subject;
	                        $actmodel_rec->message = $message;
	                        $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
	                        $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
	                        $actmodel_rec->save();
	                    }
	                }
	            }
	        }
	    }
	}
	public function actionappointments($id)
	{
	    $this->layout = 'main'; 
	    $model = Jobs::model()->findByPk($id);
	    $jobappmodel = JobApplications::model()->findAllByAttributes(array('job_id'=>$model->id));
	    $this->render('_jobappointments',array('model'=>$model,'jobappmodel'=>$jobappmodel));
	}
	
		public function actiondeleteAnswers($id)
	{
	    $this->layout = 'main'; 
	    $ansmodel = JobAnswers::model()->findByPk($id);
	    $model = Jobs::model()->findByPk($ansmodel->job_id);
	    $jobansmodel = JobAnswers::model()->findAllByAttributes(array('job_id'=>$model->id));
	    
	    JobAnswers::model()->deleteByPk($id);
	    
	    $this->redirect($this->createUrl("jobs/answers",array("id"=>$model->id,"title"=>  str_replace(" ", "-", $model->title))));
	  
	}
	
	public function actionreplyInterviewAnswer($id)
	{
	    $jamodel = JobAnswers::model()->findByPk($id);
	    $subject = $_POST['subject'];
	    $message = $_POST['message'];
	    $touser = $jamodel->user_id;
	    //$empusrmodel = EmployerUsers::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
	    $from = Yii::app()->user->getState('userid');
	    $this->sendMessage($subject,$message,$from,$touser);
	}
	 
//        send message to employer
        public function sendMessageE($subject,$message,$from,$to) {
        
        $now = new DateTime();
        $conmodel = new Conversations();
        $conmodel->subject = $subject;
        $conmodel->created_at = $now->format('Y-m-d H:i:s');
        $conmodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($conmodel->save()) {
            $usermodel = Users::model()->findByPk($from);
            $conmsgmodel = new ConversationMessages();
            $conmsgmodel->conversation_id = $conmodel->id;
            $conmsgmodel->user_id = $from;
            $conmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
            $conmsgmodel->message = $message;
            $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
            $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($conmsgmodel->save()) {
                $conmemmodel_sender = new ConversationMembers();
                $conmemmodel_sender->conversation_id = $conmodel->id;
                $conmemmodel_sender->user_id = $from;
                $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
                $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
                $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                if ($conmemmodel_sender->save()) {
                    $usermodel1 = Users::model()->findByPk($to);
                    $conmemmodel_rec = new ConversationMembers();
                    $conmemmodel_rec->conversation_id = $conmodel->id;
                    $conmemmodel_rec->user_id = $to;
                    $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
                    $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                    $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                    if ($conmemmodel_rec->save()) {
                        $actmodel_sender = new Activities();
                        $actmodel_sender->user_id = $from;
                        $actmodel_sender->type = "message";
                        $actmodel_sender->model_id = $conmsgmodel->id;
                        $actmodel_sender->model_type = "ConversationMessage";
                        $actmodel_sender->subject = "You sent a message in conversation: " . $subject;
                        $actmodel_sender->message = $message;
                        $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                        $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                        if ($actmodel_sender->save()) {
                            $actmodel_rec = new Activities();
                            $actmodel_rec->user_id = $to;
                            $actmodel_rec->type = "message";
                            $actmodel_rec->model_id = $conmsgmodel->id;
                            $actmodel_rec->model_type = "ConversationMessage";
                            $actmodel_rec->subject = "You received a message in conversation: " . $subject;
                            $actmodel_rec->message = $message;
                            $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                            $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                            $actmodel_rec->save();
                        }
                    }
                }
            }
        }
    }
    
    public function actionjobdelete($id)
    {
        Jobs::model()->deleteByPk($id);
        
        $empusrmodel = EmployerUsers::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $empusrmodel->employer_id, 'type' => 'employer'));
        
        $this->redirect($this->createUrl('employers/edit', array('slug' => $profilemodel->slug)));
    }
	
	
    public function actionjobsByCategory()
    {
        $this->layout = 'main';
        $model = Jobs::model();
        $this->render('jobs_by_category', array('model' => $model));
    }
    public function actionjobsByCompany()
    {
        $this->layout = 'main';
        $model = Jobs::model();
        $this->render('jobs_by_company', array('model' => $model));
    }
    public function actionjobsByLocation()
    {
        $this->layout = 'main';
        $model = Jobs::model();
        $this->render('jobs_by_location', array('model' => $model));
    }
    public function actionjobsBySkills()
    {
        $this->layout = 'main';
        $model = Jobs::model();
        $this->render('jobs_by_skills', array('model' => $model));
    }
}
