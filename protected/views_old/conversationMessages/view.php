<?php
/* @var $this ConversationMessagesController */
/* @var $model ConversationMessages */

$this->breadcrumbs=array(
	'Conversation Messages'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List ConversationMessages', 'url'=>array('index')),
	array('label'=>'Create ConversationMessages', 'url'=>array('create')),
	array('label'=>'Update ConversationMessages', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ConversationMessages', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ConversationMessages', 'url'=>array('admin')),
);
?>

<h1>View ConversationMessages #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'conversation_id',
		'user_id',
		'name',
		'message',
		'created_at',
		'updated_at',
		'deleted_at',
	),
)); ?>
