<?php
/* @var $this ConversationMessagesController */
/* @var $model ConversationMessages */

$this->breadcrumbs=array(
	'Conversation Messages'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ConversationMessages', 'url'=>array('index')),
	array('label'=>'Manage ConversationMessages', 'url'=>array('admin')),
);
?>

<h1>Create ConversationMessages</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>