<?php
/* @var $this InstitutionUsersController */
/* @var $model InstitutionUsers */

$this->breadcrumbs=array(
	'Institution Users'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List InstitutionUsers', 'url'=>array('index')),
	array('label'=>'Create InstitutionUsers', 'url'=>array('create')),
	array('label'=>'View InstitutionUsers', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage InstitutionUsers', 'url'=>array('admin')),
);
?>

<h1>Update InstitutionUsers <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>