<?php
/* @var $this UserLanguagesController */
/* @var $model UserLanguages */

$this->breadcrumbs=array(
	'User Languages'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List UserLanguages', 'url'=>array('index')),
	array('label'=>'Manage UserLanguages', 'url'=>array('admin')),
);
?>

<h1>Create UserLanguages</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>