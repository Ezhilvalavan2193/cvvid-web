<?php
/* @var $this UserLanguagesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'User Languages',
);

$this->menu=array(
	array('label'=>'Create UserLanguages', 'url'=>array('create')),
	array('label'=>'Manage UserLanguages', 'url'=>array('admin')),
);
?>

<h1>User Languages</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
