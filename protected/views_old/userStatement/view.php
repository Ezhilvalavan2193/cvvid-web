<?php
/* @var $this UserStatementController */
/* @var $model UserStatement */

$this->breadcrumbs=array(
	'User Statements'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List UserStatement', 'url'=>array('index')),
	array('label'=>'Create UserStatement', 'url'=>array('create')),
	array('label'=>'Update UserStatement', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete UserStatement', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage UserStatement', 'url'=>array('admin')),
);
?>

<h1>View UserStatement #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'description',
		'created_at',
		'updated_at',
	),
)); ?>
