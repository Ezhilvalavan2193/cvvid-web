<?php
/* @var $this UserStatementController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'User Statements',
);

$this->menu=array(
	array('label'=>'Create UserStatement', 'url'=>array('create')),
	array('label'=>'Manage UserStatement', 'url'=>array('admin')),
);
?>

<h1>User Statements</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
