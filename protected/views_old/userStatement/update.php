<?php
/* @var $this UserStatementController */
/* @var $model UserStatement */

$this->breadcrumbs=array(
	'User Statements'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List UserStatement', 'url'=>array('index')),
	array('label'=>'Create UserStatement', 'url'=>array('create')),
	array('label'=>'View UserStatement', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage UserStatement', 'url'=>array('admin')),
);
?>

<h1>Update UserStatement <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>