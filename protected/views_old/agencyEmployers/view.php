<?php
/* @var $this AgencyEmployersController */
/* @var $model AgencyEmployers */

$this->breadcrumbs=array(
	'Agency Employers'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List AgencyEmployers', 'url'=>array('index')),
	array('label'=>'Create AgencyEmployers', 'url'=>array('create')),
	array('label'=>'Update AgencyEmployers', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AgencyEmployers', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AgencyEmployers', 'url'=>array('admin')),
);
?>

<h1>View AgencyEmployers #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'agency_id',
		'employer_id',
		'role',
		'created_at',
		'updated_at',
	),
)); ?>
