<?php
/* @var $this IndustriesController */
/* @var $model Industries */

$this->breadcrumbs=array(
	'Industries'=>array('index'),
	'Manage',
);

//$this->menu=array(
//	array('label'=>'List Industries', 'url'=>array('index')),
//	array('label'=>'Create Industries', 'url'=>array('create')),
//);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#industries-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<script>
$(document).ready(function(){
	

	$('#delete-industries').click(function(e){

		
         if($("#industries-grid").find("input:checked").length > 0)
         {

            if (confirm('Are you sure you want to delete these industries?')) {

                    var ids = $.fn.yiiGridView.getChecked("industries-grid", "selectedIds");

                     $.ajax({
                        url: '<?php echo $this->createUrl('industries/deleteIndustries') ?>',
                        type: 'POST',
                        data: {ids : ids}, 	    
                        success: function(data) {
                            location.reload();
                        },
                        error: function(data) {		
                            alert('err');
                        }
                    });
            } 
        	
         }
         else
         {
            alert('Please select at least one item');
         }
	});
	
});

</script>
<h1>Sectors</h1>
<div class="Admin__content__inner">

    <div class="AdminFilters">
        <div class="form-inline">
           <?php $this->renderPartial('_search',array(
                    'model'=>$model,
            )); ?>
            <div class="pull-right">
                 <?php  
                        echo CHtml::button('Create',array("class"=>"btn btn-primary","onclick"=>"window.location='".Yii::app()->createUrl('industries/create')."'"));
                        echo CHtml::button('Delete',array("class"=>"btn btn-primary",'id'=>'delete-industries'));
                  ?>
            </div>
        </div>
    </div>

   <?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'industries-grid',
        'itemsCssClass' => 'table',
	'summaryText'=>'',
	//'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',
    'pager' => array(
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'header'=>'',
        'firstPageLabel' => '<<',
        'lastPageLabel' => '>>',
        'nextPageLabel' => '>>',
        'prevPageLabel' => '<<'),
			'ajaxUpdate'=>false,
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
			//'id',
                        array(
                            // 'name' => 'check',
                             'id' => 'selectedIds',
                             'value' => '$data->id',
                             'class' => 'CCheckBoxColumn',
                             'selectableRows'  => 2,
                            // 'checked'=>'Yii::app()->user->getState($data->id)',
                            // 'checkBoxHtmlOptions' => array('name' => 'idList[]'),

                         ),
			array(
					'header'=>'Name',
					'value'=>'CHtml::link("<i class=\"fa fa-pencil\"></i> ".$data->name,Yii::app()->createUrl("industries/industriesEdit",array("id"=>$data->id)),array("class"=>"industries-edit-button","id"=>"industries-edit"))',
			                'type'=>'raw'
			),
			
	),
)); ?>

</div>

