<?php
/* @var $this IndustriesController */
/* @var $model Industries */
/* @var $form CActiveForm */
?>

<h1>Create Sector</h1>
<div class="Admin__content__inner">
    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'industries-form',
	'action' => $this->createUrl('industries/createIndustries'),
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data')
)); ?>
     <?php echo  $form->errorSummary(array($model), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <?php echo $form->labelEx($model, 'name'); ?>
                <?php echo $form->textField($model, 'name', array('class' => 'form-control','placeholder'=>'Name')); ?>
            </div>
            <?php echo CHtml::submitButton('Save', array("class" => "btn btn-primary")); ?>
            <?php echo CHtml::button('Cancel',array("class"=>'btn btn-primary',"onClick"=>"window.location='".Yii::app()->createUrl('industries/admin',array())."'")); ?>

        </div>
    </div>
    
      <?php $this->endWidget(); ?>
</div>
