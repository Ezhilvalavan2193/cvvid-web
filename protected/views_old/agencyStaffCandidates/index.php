<?php
/* @var $this AgencyStaffCandidatesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Agency Staff Candidates',
);

$this->menu=array(
	array('label'=>'Create AgencyStaffCandidates', 'url'=>array('create')),
	array('label'=>'Manage AgencyStaffCandidates', 'url'=>array('admin')),
);
?>

<h1>Agency Staff Candidates</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
