<?php
/* @var $this AgencyStaffCandidatesController */
/* @var $model AgencyStaffCandidates */

$this->breadcrumbs=array(
	'Agency Staff Candidates'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List AgencyStaffCandidates', 'url'=>array('index')),
	array('label'=>'Create AgencyStaffCandidates', 'url'=>array('create')),
	array('label'=>'Update AgencyStaffCandidates', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AgencyStaffCandidates', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AgencyStaffCandidates', 'url'=>array('admin')),
);
?>

<h1>View AgencyStaffCandidates #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'agency_id',
		'agency_candidate_id',
		'staff_id',
		'created_at',
		'updated_at',
	),
)); ?>
