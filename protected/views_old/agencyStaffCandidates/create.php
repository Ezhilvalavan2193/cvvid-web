<?php
/* @var $this AgencyStaffCandidatesController */
/* @var $model AgencyStaffCandidates */

$this->breadcrumbs=array(
	'Agency Staff Candidates'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AgencyStaffCandidates', 'url'=>array('index')),
	array('label'=>'Manage AgencyStaffCandidates', 'url'=>array('admin')),
);
?>

<h1>Create AgencyStaffCandidates</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>