<?php
/* @var $this ProfileVideosController */
/* @var $model ProfileVideos */

$this->breadcrumbs=array(
	'Profile Videoses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProfileVideos', 'url'=>array('index')),
	array('label'=>'Create ProfileVideos', 'url'=>array('create')),
	array('label'=>'View ProfileVideos', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ProfileVideos', 'url'=>array('admin')),
);
?>

<h1>Update ProfileVideos <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>