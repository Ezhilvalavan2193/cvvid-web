<?php
/* @var $this ProfileVideosController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Profile Videoses',
);

$this->menu=array(
	array('label'=>'Create ProfileVideos', 'url'=>array('create')),
	array('label'=>'Manage ProfileVideos', 'url'=>array('admin')),
);
?>

<h1>Profile Videoses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
