<?php
/* @var $this TempIndustryController */
/* @var $model TempIndustry */

$this->breadcrumbs=array(
	'Temp Industries'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TempIndustry', 'url'=>array('index')),
	array('label'=>'Create TempIndustry', 'url'=>array('create')),
	array('label'=>'View TempIndustry', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TempIndustry', 'url'=>array('admin')),
);
?>

<h1>Update TempIndustry <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>