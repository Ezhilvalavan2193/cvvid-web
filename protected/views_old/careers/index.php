<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">
		<title>Cvvid Careers</title>
		
		<!-- css -->
		
		<!--  google fonts  -->
		<link href='http://fonts.googleapis.com/css?family=Oswald:400,300' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Merienda+One' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300' rel='stylesheet' type='text/css'>

		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl .'/css/bootstrap.min.css'?>">
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl .'/css/font-awesome.min.css'?>">
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl .'/css/animate.css'?>">
		<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl .'/css/custom.css'?>">

	</head>
	<body>
		<div class="body-wrapper">

			<!--   begin of header section   -->
			<section class="header" id="overlay-1">
				<div class="header-wrapper">
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-md-offset-2 text-center">
								<div class="theme-name">
									<img src = "<?php echo Yii::app()->theme->baseUrl.'/img/CVVIDcareers.png'?>" style="height:150px">
									<!-- <a href="#" class="title">Cvvid Careers</a> -->
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12 text-center">
								<div class="header-text">
									<!-- <p class="service-text">something new is</p> -->
									<p class="coming">coming soon</p>
								</div>
							</div>
						</div>

					</div> <!--  end of .container  -->
				</div><!-- end of .header-wrapper  -->
			</section>
			<!--   end of header section     -->

			<!--   begin of newsletter section  -->
			<section class="newsletter text-center">
				<div class="newsletter-wrapper">
					<div class="container">
						<div class="row">
							<h2 class="newsletter-text">subscribe and stay tuned with us</h2>
							<div class="col-md-8 col-md-offset-2">
								<form>
	  								<div class="form-group" style="text-align: -webkit-center;">
	    								<div class="input-group">
	      									<div class="input-group-addon"><i class="fa fa-envelope-o"></i></div>
	      									<input type="text" class="form-control" placeholder="Enter Your Email">
	    								</div>
	    								<button class="subsribe-btn">Subscribe Now</button>
	  								</div>
								</form>
								<p class="spam">
									<i class="fa fa-bookmark"></i>We'll never spam you.
								</p>
							</div>
						</div>
					</div><!--  end of .container  -->
				</div> <!--  end of .newssletter-wrapper  -->
			</section>
			<!--   end of newsletter section  -->

			<!--   begin of social-media section  -->
			<section class="social-media text-center">
				<div class="social-media-wrapper">
					<div class="container">
						<div class="row">
							<h2 class="social-media-text">get updates from social media</h2>
							<div class="col-md-8 col-md-offset-2">
								<div class="social-links">
									<a href="#" title="Facebook">
										<i class="fa fa-facebook"></i>
									</a>
									<a  href="#" title="Twitter">
										<i class="fa fa-twitter"></i>
									</a>
									<a href="#" title="Google+">
										<i class="fa fa-google-plus"></i>
									</a>
									
								</div>
							</div>
							<div class="clear-fix"></div>
						</div>
					</div>
				</div><!--  end of .social-media-wrapper  -->
			</section>
			<!--   end of social-media section   -->

			<!--   begin of footer  section  -->
			<section class="footer">
				<div class="footer-wrapper">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<p class="copy-right text-center">&copy; 2018 cvvidcareers.co.uk. All rights reserved.</p>
							</div>
							<!-- <div class="col-md-6"> -->
								<!-- <p class="develop text-center">Developed by <a href="#"></a></p> -->
							<!-- </div> -->
						</div>
					</div>
				</div><!--  end of .footer-wrapper  -->
			</section>
			<!--   end of footer section  -->
		</div>
		
		<!--  js files -->

		<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl .'/js/jquery.js'?>"></script>
		<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl .'/js/bootstrap.min.js'?>"></script>
		<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl .'/js/timer.js'?>"></script>
		<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl .'/js/script.js'?>"></script>
	
	</body>
</html>