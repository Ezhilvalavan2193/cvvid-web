<?php
/* @var $this QueuedJobsController */
/* @var $model QueuedJobs */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'queued-jobs-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'queue'); ?>
		<?php echo $form->textField($model,'queue',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'queue'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'payload'); ?>
		<?php echo $form->textArea($model,'payload',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'payload'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'attempts'); ?>
		<?php echo $form->textField($model,'attempts'); ?>
		<?php echo $form->error($model,'attempts'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'reserved'); ?>
		<?php echo $form->textField($model,'reserved'); ?>
		<?php echo $form->error($model,'reserved'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'reserved_at'); ?>
		<?php echo $form->textField($model,'reserved_at',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'reserved_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'available_at'); ?>
		<?php echo $form->textField($model,'available_at',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'available_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_at'); ?>
		<?php echo $form->textField($model,'created_at',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'created_at'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->