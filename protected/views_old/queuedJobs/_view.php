<?php
/* @var $this QueuedJobsController */
/* @var $data QueuedJobs */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('queue')); ?>:</b>
	<?php echo CHtml::encode($data->queue); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payload')); ?>:</b>
	<?php echo CHtml::encode($data->payload); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('attempts')); ?>:</b>
	<?php echo CHtml::encode($data->attempts); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reserved')); ?>:</b>
	<?php echo CHtml::encode($data->reserved); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reserved_at')); ?>:</b>
	<?php echo CHtml::encode($data->reserved_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('available_at')); ?>:</b>
	<?php echo CHtml::encode($data->available_at); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('created_at')); ?>:</b>
	<?php echo CHtml::encode($data->created_at); ?>
	<br />

	*/ ?>

</div>