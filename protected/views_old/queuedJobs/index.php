<?php
/* @var $this QueuedJobsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Queued Jobs',
);

$this->menu=array(
	array('label'=>'Create QueuedJobs', 'url'=>array('create')),
	array('label'=>'Manage QueuedJobs', 'url'=>array('admin')),
);
?>

<h1>Queued Jobs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
