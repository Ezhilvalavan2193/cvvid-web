<?php
/* @var $this ProfileLanguagesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Profile Languages',
);

$this->menu=array(
	array('label'=>'Create ProfileLanguages', 'url'=>array('create')),
	array('label'=>'Manage ProfileLanguages', 'url'=>array('admin')),
);
?>

<h1>Profile Languages</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
