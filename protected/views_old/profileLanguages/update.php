<?php
/* @var $this ProfileLanguagesController */
/* @var $model ProfileLanguages */

$this->breadcrumbs=array(
	'Profile Languages'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProfileLanguages', 'url'=>array('index')),
	array('label'=>'Create ProfileLanguages', 'url'=>array('create')),
	array('label'=>'View ProfileLanguages', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ProfileLanguages', 'url'=>array('admin')),
);
?>

<h1>Update ProfileLanguages <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>