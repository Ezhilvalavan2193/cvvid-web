<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */
/* @var $form CActiveForm */
?>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl('media/searchAdmin'),
    'method' => 'get',
        ));
?>

<?php echo $form->textField($model, 'name', array('class' => 'form-control MediaAdmin__search','placeholder'=>'Search...')); ?>


<?php $this->endWidget(); ?>
