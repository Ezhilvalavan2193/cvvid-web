<?php
/* @var $this MediaController */
/* @var $data Media */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('model_type')); ?>:</b>
	<?php echo CHtml::encode($data->model_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('model_id')); ?>:</b>
	<?php echo CHtml::encode($data->model_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('collection_name')); ?>:</b>
	<?php echo CHtml::encode($data->collection_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('file_name')); ?>:</b>
	<?php echo CHtml::encode($data->file_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('disk')); ?>:</b>
	<?php echo CHtml::encode($data->disk); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('size')); ?>:</b>
	<?php echo CHtml::encode($data->size); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('manipulations')); ?>:</b>
	<?php echo CHtml::encode($data->manipulations); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('custom_properties')); ?>:</b>
	<?php echo CHtml::encode($data->custom_properties); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('order_column')); ?>:</b>
	<?php echo CHtml::encode($data->order_column); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_at')); ?>:</b>
	<?php echo CHtml::encode($data->created_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_at')); ?>:</b>
	<?php echo CHtml::encode($data->updated_at); ?>
	<br />

	*/ ?>

</div>