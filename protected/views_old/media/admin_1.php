<?php
/* @var $this MediaController */
/* @var $model Media */

$this->breadcrumbs = array(
    'Medias' => array('index'),
    'Manage',
);

//$this->menu=array(
//	array('label'=>'List Media', 'url'=>array('index')),
//	array('label'=>'Create Media', 'url'=>array('create')),
//);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#media-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/showcase.scss">
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/successstories.scss">
<link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/slideshow.scss">
<script>
    $(document).ready(function () {
        $('body').on('click', '#media-photo-upload', function (e) {
            e.preventDefault();
            $(".media-upload").trigger('click');
        });

        $('body').on('change', '.media-upload', function (e) {

            var data = new FormData();
            jQuery.each(jQuery(this)[0].files, function (i, file) {
                data.append('media', file);
            });
            data.append('collection_name', 'sample');
            // alert(JSON.stringify(data));
            $.ajax({
                url: '<?php echo $this->createUrl('media/insertMedia') ?>',
                type: 'POST',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
//                    alert(data);
                    $(".media-photo-div").load(location.href + " .media-photo-div>*", "");
                    //$('#img-media').attr("src", data);
//                    $('#img-media').src('background-image', 'url(' + data + ')');
                },
                error: function (data) {
                    alert('err');
                }
            });

            return false;
        });

//      media_item_delete  
        $('body').on('click', '.media_item_delete', function (e) {

            if (confirm('Are you sure you want to delete this item?')) {

                var id = $(this).attr('id');

                $.ajax({
                    url: '<?php echo $this->createUrl('media/deleteMedia'); ?>',
                    type: 'POST',
                    data: {id: id},
                    success: function (data) {
                        //alert(data);
                        $(".media-photo-div").load(location.href + " .media-photo-div>*", "");
                    },
                    error: function (data) {
                        alert('err');
                    }
                });
            }

            return false;

        });


    });
</script>
<h1>Manage Media</h1>


<?php // echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<div align="right">
    <?php
    echo CHtml::button('Add Media', array("class" => "btn btn-primary", "id" => "media-photo-upload"));
    ?>

</div>

<div class="container">
    <div class="row">
        <div class="col-sm-6">

            <div class="Slide__content">
                <div class="PageSlideshow">
                    <div class="PageSlideshow__slides">

                        <div class="PageSlideshow__slide">
                            <h3>How do I make my video CV?</h3>
                            <p>The first thing is the simplest.</p>
                            <p>Practice.</p>
                            <p>Practice may not make it perfect, but it will always make it better.</p>
                            <p>&nbsp;</p>
                        </div>                        <!--.PageSlideshow__slide-->  
                        <div class="PageSlideshow__slide">
                            <h2>Get used to that camera.</h2>
                            <ul>
                                <li>Make it fun!</li>
                                <li>Shout, dance, sing, read, tell a joke, do a stand up in front of it &hellip; Until you start to relax and stop hunching your shoulders!</li>
                                <li>Now try speaking directly to camera. Start off with a couple of sentences. If there&rsquo;s something you really care passionately about, then talk about it in this test.</li>
                                <li>Okay, take a look at your recording. Don&rsquo;t think about what you&rsquo;ve said, but how you&rsquo;ve said it. Are you speaking Clearly? Confidently? With conviction? Those are the three most important things to go for.</li>
                                <li>Have another go. And another. Tell that camera you&rsquo;re in charge.</li>
                                <li>Get a friend to be your director.</li>
                                <li>And don&rsquo;t forget to smile!</li>
                                <li>Most of all, just be you. You &ndash; at your best.</li>
                            </ul>
                            <p>If cameras are a real problem for you, check out our Masterclass video: Act Natural - how to be YOU in front of a camera <a class="yellow" href="http://www.cvvid.com/how-to">here</a></p>
                        </div>                        <!--.PageSlideshow__slide-->  
                        <div class="PageSlideshow__slide">
                            <h2>So what should I say?</h2>
                            <p>It&rsquo;s really important to plan this. (Really)</p>
                            <p>You&rsquo;re showing your personality: who you are, how well you can communicate, what kind of team player you are, your skills, your experience and your ambition.</p>
                            <p>All that needs to come across right from outset.</p>
                        </div>                        <!--.PageSlideshow__slide-->  
                        <div class="PageSlideshow__slide">
                            <h2>Step 1</h2>
                            <p>Start by imagining you&rsquo;re the employer.</p>
                            <p>It&rsquo;s so important to understand that employers want genuine people, who have heart and warmth - and even vulnerability - as well as skills and drive and passion.</p>
                            <p>If you allow them to meet you at your best, you&rsquo;ll stand out from a whole production line of script-reading Cyborgs.</p>
                            <ul>
                                <li>What are you looking for in someone you want to employ? Write down the words that you come up with.</li>
                                <li>What might you miss if you-the-employer first met you-the-job-seeker?</li>
                            </ul>
                            <p>Right. So those are the things you want to try get into your CV Video.</p>
                        </div>                        <!--.PageSlideshow__slide-->  
                        <div class="PageSlideshow__slide">
                            <h2>Step 2</h2>
                            <p>Now it&rsquo;s time to build up the structure.</p>
                            <p>For this you&rsquo;ll want to create a set of questions that you&rsquo;ll then answer in your CV Video.</p>
                            <ul>
                                <li>Make sure your questions cover the things you&rsquo;ve written down in step 1.</li>
                                <li>Once you&rsquo;ve got your list, experiment with different ways to order them &ndash; until you&rsquo;ve got the best sequence.<br /> It&rsquo;s good idea to get someone to help with this too.</li>
                            </ul>
                        </div>
                        <!--.PageSlideshow__slide--> 
                        <div class="PageSlideshow__slide">
                            <h2>Questions</h2>
                            <p>Here are some examples of the sorts of questions you might ask yourself. But remember &ndash; you&rsquo;re not trying to be a flawless super-hero, or deliver a Mask of Perfection.</p>
                            <p>Make sure your questions give you the chance to be the real you. You &ndash; at your best.</p>
                            <ol>
                                <li>Tell me something surprising about yourself.</li>
                                <li>What are your strong points?</li>
                                <li>What about a failure &ndash; how did you learn from it and what did you learn?</li>
                                <li>What do you see yourself doing in five years&rsquo; time?</li>
                                <li>What motivates you?</li>
                                <li>What are the things you most want to learn?</li>
                                <li>Talk about a big event, achievement or experience &ndash; what did you learn about yourself.</li>
                                <li>Talk about something that makes you smile.</li>
                                <li>Talk about something that lets us see you at your best</li>
                            </ol>
                        </div>
                        <!--.PageSlideshow__slide--> 
                        <div class="PageSlideshow__slide">
                            <h3>FINALLY: Prepare to be your best:</h3>
                            <ul>
                                <li>
                                    <p>When do you feel at your most confident? Imagine yourself there.</p>
                                </li>
                                <li>
                                    <p>Who makes you feel most like you? &hellip; Imagine you&rsquo;re with them. Better still, ask them to help you.</p>
                                </li>
                                <li>
                                    <p>In fact it&rsquo;s always a good idea to get someone you trust to help you.</p>
                                </li>
                                <li>
                                    <p>Treat this as a project. Think it through. Walk around it. Build it up. Get some feedback from others.</p>
                                </li>
                                <li>
                                    <p>Take a look at some of our Best Practice examples&nbsp;<a class="yellow" href="http://www.cvvid.com/cv-video-showcase">here</a></p>
                                </li>
                                <li>
                                    <p>BUT always remember, you don&rsquo;t have to be like anyone else, and you shouldn&rsquo;t be. That&rsquo;s the whole point: you just have to be you. You - at your best.</p>
                                </li>
                            </ul>
                        </div>                        <!--.PageSlideshow__slide-->  

                    </div>
                    <div class="PageSlideshow__controls" style="margin-top: 20px;">
                        <a href="#" class="PageSlideshow__nav-btn PageSlideshow__prev"><i class="fa fa-chevron-left"></i></a>
                        <span class="PageSlideshow__pages">
                            <span class="PageSlideshow__current-page">1</span>/<span class="PageSlideshow__count">4</span>
                        </span>
                        <a href="#" class="PageSlideshow__nav-btn PageSlideshow__next"><i class="fa fa-chevron-right"></i></a>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<!--<div class="container">
    <div class="success-video-carousel">
        <div class="video-carousel Slideshow" data-is-full-screen="true" data-slides-to-show="5" data-autoplay="false"
             data-responsive='[{"breakpoint": 1024, "settings":{"slidesToShow": 5}},{"breakpoint": 992, "settings":{"slidesToShow": 3}},{"breakpoint": 768, "settings":{"slidesToShow": 1}}]'>
            <div class="SuccessStories__slide">
                <a class="SuccessVideo modalVideo" data-video="169239600" data-name="Alwyn David - Engineer"><div class="SuccessStories__item__image" style="background-image: url('http://localhost/cvvid_laran/public/media/98/conversions/profile.jpg');"></div></a>
                <div class="SuccessStories__item__meta clearfix">
                    <div class="SuccessStories__item__title">Alwyn David - Engineer</div>
                </div>
            </div>
            <div class="SuccessStories__slide">
                <a class="SuccessVideo modalVideo" data-video="169239725" data-name="Michael Grimmond - Gardener"><div class="SuccessStories__item__image" style="background-image: url('http://localhost/cvvid_laran/public/media/99/conversions/profile.jpg');"></div></a>
                <div class="SuccessStories__item__meta clearfix">
                    <div class="SuccessStories__item__title">Michael Grimmond - Gardener</div>
                </div>
            </div>
            <div class="SuccessStories__slide">
                <a class="SuccessVideo modalVideo" data-video="169696984" data-name="Zeynep - Film-maker"><div class="SuccessStories__item__image" style="background-image: url('http://localhost/cvvid_laran/public/media/104/conversions/profile.jpg');"></div></a>
                <div class="SuccessStories__item__meta clearfix">
                    <div class="SuccessStories__item__title">Zeynep - Film-maker</div>
                </div>
            </div>
            <div class="SuccessStories__slide">
                <a class="SuccessVideo modalVideo" data-video="169259539" data-name="Myles - Plumber"><div class="SuccessStories__item__image" style="background-image: url('http://localhost/cvvid_laran/public/media/102/conversions/profile.jpg');"></div></a>
                <div class="SuccessStories__item__meta clearfix">
                    <div class="SuccessStories__item__title">Myles - Plumber</div>
                </div>
            </div>
            <div class="SuccessStories__slide">
                <a class="SuccessVideo modalVideo" data-video="169259515" data-name="Annabel - Law Graduate"><div class="SuccessStories__item__image" style="background-image: url('http://localhost/cvvid_laran/public/media/103/conversions/profile.jpg');"></div></a>
                <div class="SuccessStories__item__meta clearfix">
                    <div class="SuccessStories__item__title">Annabel - Law Graduate</div>
                </div>
            </div>
            <div class="SuccessStories__slide">
                <a class="SuccessVideo modalVideo" data-video="169258813" data-name="Ben - Anthropologist"><div class="SuccessStories__item__image" style="background-image: url('http://localhost/cvvid_laran/public/media/101/conversions/profile.jpg');"></div></a>
                <div class="SuccessStories__item__meta clearfix">
                    <div class="SuccessStories__item__title">Ben - Anthropologist</div>
                </div>
            </div>
            <div class="SuccessStories__slide">
                <a class="SuccessVideo modalVideo" data-video="169258609" data-name="Anita - Fitness Instructor"><div class="SuccessStories__item__image" style="background-image: url('http://localhost/cvvid_laran/public/media/100/conversions/profile.jpg');"></div></a>
                <div class="SuccessStories__item__meta clearfix">
                    <div class="SuccessStories__item__title">Anita - Fitness Instructor</div>
                </div>
            </div>
            <div class="SuccessStories__slide">
                <a class="SuccessVideo modalVideo" data-video="169696886" data-name="Dr Rabeah - Virologist"><div class="SuccessStories__item__image" style="background-image: url('http://localhost/cvvid_laran/public/media/105/conversions/profile.jpg');"></div></a>
                <div class="SuccessStories__item__meta clearfix">
                    <div class="SuccessStories__item__title">Dr Rabeah - Virologist</div>
                </div>
            </div>
        </div> .video-carousel 
    </div> .success-video-carousel 
</div>-->



<!--
<div class="col-sm-12 clearfix media-photo-div">
    <input class="media-upload" type="file" style="display:none;"/>
<?php
$cri = new CDbCriteria();
$cri->order = 'order_column DESC';
$model = Media::model()->findAll($cri);
foreach ($model as $media) {
    ?>
        <div class="media_item">
            <div class="media_item_inner">
                <img id="img-media" src="<?php echo Yii::app()->baseUrl ?>/images/media/<?php echo $media['id'] ?>/conversions/thumb.jpg" alt="<?php echo $media['name'] ?>" class="img-responsive">
                <button class="media_item_delete" id="<?php echo $media['id'] ?>" type="button"><i class="fa fa-close"></i></button>
            </div>
        </div>
<?php } ?>
</div>-->


<script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/build/slideshow.js"></script>