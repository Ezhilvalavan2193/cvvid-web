<?php
/* @var $this TutorGroupsController */
/* @var $model TutorGroups */

$this->breadcrumbs=array(
	'Tutor Groups'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List TutorGroups', 'url'=>array('index')),
	array('label'=>'Create TutorGroups', 'url'=>array('create')),
	array('label'=>'Update TutorGroups', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TutorGroups', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TutorGroups', 'url'=>array('admin')),
);
?>

<h1>View TutorGroups #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'institution_id',
		'teacher_id',
		'name',
		'location',
		'location_lat',
		'location_lng',
		'graduation_date',
		'account_limit',
		'created_at',
		'updated_at',
		'deleted_at',
	),
)); ?>
