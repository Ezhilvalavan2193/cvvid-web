<?php
/* @var $this TutorGroupsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tutor Groups',
);

$this->menu=array(
	array('label'=>'Create TutorGroups', 'url'=>array('create')),
	array('label'=>'Manage TutorGroups', 'url'=>array('admin')),
);
?>

<h1>Tutor Groups</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
