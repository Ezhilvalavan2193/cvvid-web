<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */

$this->breadcrumbs=array(
    'Institutions'=>array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
    
");
?>

<?php
        $planmodel = Plans::model()->findByAttributes(array('owner_id'=>$model->id));
        $usermodel  = Users::model()->findByPk($model->admin_id);        
?>
    <h1>Edit Institution</h1>
    <div class="Admin__content__inner">
        <?php if (Yii::app()->user->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>
        <div class="InstitutionForm">
            <!-- Nav tabs -->
            <ul class="AdminTabs nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="<?php echo Yii::app()->createUrl("institutions/tabeditinstitute", array("id" => $model->id)); ?>" aria-controls="details">Details</a></li>
                <li role="presentation"><a href="<?php echo Yii::app()->createUrl("institutions/tabeditadmin", array("id" => $model->id)); ?>">Admin</a></li>
                <li role="presentation"><a href="<?php echo Yii::app()->createUrl("institutions/tabeditteachers", array("id" => $model->id)); ?>">Teachers</a></li>
                <li role="presentation"><a href="<?php echo Yii::app()->createUrl("institutions/tabedittutorgroups", array("id" => $model->id)); ?>">Tutor Groups</a></li>
                <li role="presentation"><a href="<?php echo Yii::app()->createUrl("institutions/tabeditstudents", array("id" => $model->id)); ?>">Students</a></li>
                
            </ul>
            <?php 
                    $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$model->id,'type'=>'institution'));
                    $mediamodel = Media::model()->findByPk($profilemodel->id);

                    $form = $this->beginWidget('CActiveForm', array(
                                    'id'=>'users-form',
                                    'action'=>$this->createUrl('institutions/updateInstitution',array('id'=>$model->id)),
                                    'enableAjaxValidation'=>false,
                                    'htmlOptions' => array('enctype' => 'multipart/form-data')
                            ));         

                ?>
                <div class="row">
                    <div class="col-sm-3 col-md-2">
                        <div class="form-group">
                            <label for="photo_id" class="control-label">Profile Photo</label>
                            <div class="photo-section">
                              <?php
                              if($profilemodel->photo_id > 0)
                                  $url = Yii::app()->baseUrl."/images/media/".$profilemodel->photo_id."/conversions/thumb.jpg";
                              else
                                  $url = Yii::app()->baseUrl."/images/defaultprofile.jpg";
                              ?>
                                <img src="<?php echo $url ?>" alt="" id="profile-pic" class="img-responsive">

                                        <input type="hidden" name="photo_id" id="photo_id" value="<?php echo $profilemodel->photo_id?>">
                                <button type="button" class="add-media btn btn-primary btn-xs" id="edit-photo-btn">Add Logo</button>            
                                <button type="button" class="remove-media btn btn-primary btn-xs" id="remove-photo-btn">Remove Logo</button>
                             </div>
                            
                        </div>
                    </div>
                    <div class="col-sm-9 col-md-10">
                        <div class="panel panel-default">
                            <div class="panel-heading">Subscription</div>
                            <div class="panel-body">

                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="control-label">Plan</label>
                                            <p class="form-control-static"><?php echo $planmodel->stripe_plan; ?></p>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="control-label">Card Number</label>
                                            <p class="form-control-static">****-****-****-<?php echo $model->last_four; ?></p>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label class="control-label">Card Expiry</label>
                                            <p class="form-control-static"><?php echo $model->card_expiry; ?></p>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">Institution Details</div>
                            <div class="panel-body">

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="name" class="control-label">Name</label>
                                            <?php echo $form->textField($model,'name',array('class'=>'form-control')); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="address_1" class="control-label">Address 1</label>
                                            <?php echo $form->textField($model,'address_1',array('class'=>'form-control')); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="address_2" class="control-label">Address 2</label>
                                           <?php echo $form->textField($model,'address_2',array('class'=>'form-control')); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="town" class="control-label">City / Town</label>
                                          <?php echo $form->textField($model,'town',array('class'=>'form-control')); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="post_code" class="control-label">Post Code</label>
                                         <?php echo $form->textField($model,'post_code',array('class'=>'form-control')); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Number of pupils</label>
                                           <?php echo $form->textField($model,'num_pupils',array('class'=>'form-control')); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Status</label>.
                                            <select name="status" id="status" class="form-control">
		                	<option value="active">Active</option>
		                    <option value="pending">Pending</option>		                
    		            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">Admin Account Details</div>
                            <div class="panel-body">

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="forenames" class="control-label">Forenames</label>
                                            <?php echo $form->textField($usermodel,'forenames',array('class'=>'form-control')); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="surname" class="control-label">Surname</label>
                                            <?php echo $form->textField($usermodel,'surname',array('class'=>'form-control')); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="email" class="control-label">Email</label>
                                            <?php echo $form->textField($usermodel,'email',array('class'=>'form-control')); ?>
                                        </div>
                                    </div>
                                </div><!-- .row -->

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="password" class="control-label">Password</label>
                                            <?php $usermodel->password=''; ?>
                                            <?php echo $form->passwordField($usermodel,'password',array('class'=>'form-control')); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="password_confirmation" class="control-label">Confirm Password</label>
                                            <?php echo CHtml::passwordField('Confirm Password','',array('class'=>'form-control')); ?>
                                        </div>
                                    </div>
                                </div><!-- .row -->

                            </div>
                        </div>

                    </div>
                </div>

               <?php echo CHtml::submitButton('Save',array("class"=>"btn btn-primary")); ?>       

             <?php $this->endWidget(); ?>

            <div class="row">
                <div class="col-sm-offset-2 col-sm-10">
                    <div class="panel panel-primary">
                        <div class="panel-body">     
                        		<?php echo CHtml::button('Delete Account',array('class'=>'btn btn-danger',"onclick"=>"window.location='".$this->createUrl('institutions/deleteInstitution',array('id'=>$model->id))."'"))?>                      
                               
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>


