<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
    'Institutions'=>array('index'),
    'Manage',
);
?>

<?php  
echo CHtml::button('Create',array('class'=>'btn btn-primary','onclick'=>"window.location='".Yii::app()->createUrl('institutions/createStudent',array('id'=>$model->id))."'"));
    
$instusers = InstitutionUsers::model()->findByAttributes(array('institution_id'=>$model->id,'role'=>'teacher'));

$tutorgroup = TutorGroups::model()->findByAttributes(array('institution_id'=>$model->id,'teacher_id'=>$instusers->user_id));

    $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'teacher-grid',
	'summaryText'=>'',
	'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'dataProvider'=>TutorGroupsStudents::model()->searchByInstitution($model->id), //TutorGroupsStudents::model()->searchStudents(($tutorgroup->id > 0 ? $tutorgroup->id : 0)),
        	'columns'=>array(
        	    
            			array(
            					'header'=>'Name',
            					'value'=>'CHtml::link($data->student->forenames." ".$data->student->surname,Yii::app()->createUrl("institutions/editStudent", array("id"=>$data->id)),array("class"=>"student-edit-button","id"=>"student-edit"))',
            			        'type'=>'raw'
            			),
            			array(
            					'header'=>'Email',
            			        'value'=>'$data->student->email',
            					
            			),
                	    array(
                	        'header'=>'Graduation Date',
                	        'value'=>'$data->graduation_date',
                	    ),
                	    array(
                	        'header'=>'Created At',
                	        'value'=>'$data->student->created_at',                	        
                	    ),
                	    
        	 ),
  ));
?>
 