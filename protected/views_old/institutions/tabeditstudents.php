<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
    'Institutions'=>array('index'),
    'Manage',
);
?>

    <h1>TIGHS: Students</h1>
    <div class="Admin__content__inner">
        <!-- Nav tabs -->
        <ul class="AdminTabs nav nav-tabs" role="tablist">
        <li role="presentation"><a href="<?php echo Yii::app()->createUrl("institutions/tabeditinstitute", array("id" => $model->id)); ?>" aria-controls="details">Details</a></li>
        <li role="presentation"><a href="<?php echo Yii::app()->createUrl("institutions/tabeditadmin", array("id" => $model->id)); ?>">Admin</a></li>
        <li role="presentation"><a href="<?php echo Yii::app()->createUrl("institutions/tabeditteachers", array("id" => $model->id)); ?>">Teachers</a></li>
        <li role="presentation" ><a href="<?php echo Yii::app()->createUrl("institutions/tabedittutorgroups", array("id" => $model->id)); ?>">Tutor Groups</a></li>
       <li role="presentation" class="active"><a href="<?php echo Yii::app()->createUrl("institutions/tabeditstudents", array("id" => $model->id)); ?>">Students</a></li>
    </ul>
        <?php
        echo CHtml::button('Create', array('class' => 'btn btn-primary', 'onclick' => "window.location='" . Yii::app()->createUrl('institutions/createStudent', array('id' => $model->id)) . "'"));

        $instusers = InstitutionUsers::model()->findByAttributes(array('institution_id' => $model->id, 'role' => 'teacher'));
        if($instusers != null)
            $tutorgroup = TutorGroups::model()->findByAttributes(array('institution_id' => $model->id, 'teacher_id' => $instusers->user_id));

        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'teacher-grid',
            'summaryText' => '',
             'itemsCssClass' => 'table table-striped',
           // 'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
            'dataProvider' => TutorGroupsStudents::model()->searchByInstitution($model->id), //TutorGroupsStudents::model()->searchStudents(($tutorgroup->id > 0 ? $tutorgroup->id : 0)),
            'columns' => array(
                array(
                    'header' => 'Name',
                    'value' => 'CHtml::link($data->student->forenames." ".$data->student->surname,Yii::app()->createUrl("institutions/tabeditstudent", array("id"=>$data->id)),array("class"=>"student-edit-button","id"=>"student-edit"))',
                    'type' => 'raw'
                ),
                array(
                    'header' => 'Email',
                    'value' => '$data->student->email',
                ),
                array(
                    'header' => 'Graduation Date',
                    'value' => '$data->graduation_date',
                ),
                array(
                    'header' => 'Created At',
                    'value' => '$data->student->created_at',
                ),
            ),
        ));
        ?>

    </div>

    