<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */
/* @var $form CActiveForm */
?>
<script>
$(document).ready(function(){
	var Password = {
  			 
			  _pattern : /[a-zA-Z0-9_\-\+\.]/,
			  
			  
			  _getRandomByte : function()
			  {
			    // http://caniuse.com/#feat=getrandomvalues
			    if(window.crypto && window.crypto.getRandomValues) 
			    {
			      var result = new Uint8Array(1);
			      window.crypto.getRandomValues(result);
			      return result[0];
			    }
			    else if(window.msCrypto && window.msCrypto.getRandomValues) 
			    {
			      var result = new Uint8Array(1);
			      window.msCrypto.getRandomValues(result);
			      return result[0];
			    }
			    else
			    {
			      return Math.floor(Math.random() * 256);
			    }
			  },
			  
			  generate : function(length)
			  {
			    return Array.apply(null, {'length': length})
			      .map(function()
			      {
			        var result;
			        while(true) 
			        {
			          result = String.fromCharCode(this._getRandomByte());
			          if(this._pattern.test(result))
			          {
			            return result;
			          }
			        }        
			      }, this)
			      .join('');  
			  }    
			    
			};

	$(".students-upload").change(function() {

		if($('#allocated_students').val() > 0)
		{
		
		 var file = this.files[0];

		  var reader = new FileReader();
                    reader.onload = function (progressEvent) {
                        // Entire file


                        // By lines
                        var lines = this.result.split('\n');
                        var cnt = 1;
// 		    alert(lines.length);

                       // for (var line = 0; line < lines.length; line++) {
 						for (var line = 0; line <= $('#allocated_students').val(); line++) {
                            if (cnt > 1)
                            {
                                var row = lines[line];
                                if (row != "")
                                {
                                    var array = row.split(',');

                                    var studclone = $('.student_line').find('.row').clone();
                                    $(".user-list").append(studclone);

                                    var c = 0;
                                    $(studclone).find('input').each(function () {
                                        $(this).val(array[c]);
                                        c++;
                                    });
                                }

                            }
                            cnt++;
                        }
                    };
                    reader.readAsText(file);
                } 
                else
                {
                    var $el = $('.students-upload');
                    $el.wrap('<form>').closest('form').get(0).reset();
                    $el.unwrap();
                }
        
            });

    
	$("#tutor-group-form").submit(function(event){
		//event.preventDefault();
		if($('#password').val() != "" && $('#password').val() == $('#confirmpassword').val())
			$("#tutor-group-form").submit();
		else	
		{
			$('.pwdmsg').html('Password not matched');
			return false;
		}
	});
	
	$('#inslist').change(function(){
		var id = $(this).val(); 
		if(id > 0)
		{
			 $.ajax({
		            url: '<?php echo $this->createUrl('institutions/getInsTeachers') ?>',
		            type: 'POST',
		            data: {id : id}, 	    
		            success: function(res) {
		            	$('#teacherslist').html(res);
		            	
		            	 $.ajax({
		 		            url: '<?php echo $this->createUrl('institutions/getNumPupils') ?>',
		 		            type: 'POST',
		 		            data: {id : id}, 	    
		 		            success: function(data) {
		 		            	$('#accounts_remain').html(data);
		 		            	$('#student_cnt').val(data);
		 		            },
		 				    error: function(data) {		
		 				        alert('err');
		 				    }
		 		         });
		 		         
		            },
				    error: function(data) {		
				        alert('err');
				    }
		         });
			
		}
	});	
	
	$('#teacherslist').change(function(){
		if($(this).val() > 0)
			$('.teacher-section').hide();
		else
			$('.teacher-section').show();	
	});	

	$("#add_student").click(function(){
// 		alert($('.student-section').find('.row').length -1);
		var cnt = $('#allocated_students').val() > 0 ? $('#allocated_students').val() : $("#student_cnt").val();
		if($('.user-list').find('.row').length >= cnt)
		{
			alert('Allocated Students limit reached');    		
		}
		else
		{			
    			var studclone = $('.student_line').find('.row').clone();	
        		$(".user-list").append(studclone);
			
		}
			
	});

	$(".user-list").on('click','.remove_student',function(){
		$(this).closest('.row').remove();
	});
	$(".generate-password").click(function(){

		$(".user-list").find('.row').each(function( index ) {
			if($.trim($(this).find('.col-sm-3').find(".passwordbox").val()) == "")
				$(this).find('.col-sm-3').find(".passwordbox").val(Password.generate(10));
		});
		
	});

	$("#allocated_students").blur(function(){

		
		if(parseInt($(this).val()) > parseInt($("#student_cnt").val()))
		{
			$('.max-accounts-reached').removeClass('hidden');
			$(this).val($("#student_cnt").val());
			$('.user-limit').html($(this).val());
		}
		else
		{
			$('.max-accounts-reached').addClass('hidden');
			$('.user-limit').html($(this).val());
		}

		if( $(".user-list").find('.row').length > $(this).val() )
		{
			var cnt = $(".user-list").find('.row').length - $(this).val();
			$(".user-list").find('.row').slice(-cnt).remove();
		}
	});

	$('#class-form').submit(function () {
		
		 var err = 0;
		 if($("#allocated_students").val() > 0 && $("#student_cnt").val() > 0 && $('.user-list').find('.row').length <= 0)
		 {
			alert("Please add student");
			return false;
		 }
		 else if($('.user-list').find('.row').length > 0)
		 {
			 var arr = [];
			 $(".user-list").find('.row').each(function( index ) {
				 arr.push($(this).find('input.emailbox').val());
			 });
			 
			 $.ajax({
		            url: '<?php echo $this->createUrl('institutions/checkEmails') ?>',
		            type: 'POST',
		            data: {emails : arr}, 	
		            dataType: "JSON",
		            async:false,    
		            success: function(data) {
			            
			            if(data != -1)
			            {
				            var msg = "";
	    	            	 for(var i=0;i<data.length;i++){
	    	                     msg += "<div>student "+data[i]+ " email already taken</div>";
	    	                 }
	    	            	 $('#msg_err_msg').html(msg);
	    	            	 $('#msg_err_msg').show();
	    	            	 $(window).scrollTop(0);
	    	            	 err = 1;
			            }
			            else
			            	$('#msg_err_msg').hide();
//	 		            for(i = 0 )
//	 	            	$('#msg_err_msg').html("<div>"+)
		            },
				    error: function(data) {		
				        alert('err');
				    }
		         });

	         if(err > 0)
			 	return false;
	         else
		         return true;
			
		 }
			 
	});
});
</script>

<div class="page-title text-left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Create Tutor group</h1>
            </div>
        </div>
    </div>
</div>
<div id="page-content">
    <div class="container">
     <div class="alert alert-danger" id='msg_err_msg' style="display: none">

                    </div>
                    
        <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'class-form',
                'action'=>$this->createUrl('institutions/teacherSaveClass'),
                'enableAjaxValidation'=>false
        )); ?>
        <?php echo  $form->errorSummary(array($tutormodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
            <div id="user-group-form">
                  <!-- Tutor Group -->
                <div class="form-container">
                    <h4>Tutor Group Details</h4>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="name" class="control-label">Tutor Group</label>
                                <?php echo $form->textField($tutormodel,'name',array('class'=>'form-control','placeholder'=>'Name')); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="graduation_date" class="control-label">Graduation Date</label>
                                <div data-format="DD/MM/YYYY">
                                    <input type="text" autocomplete='off' name="graduation_date" class="form-control datepicker" value="<?php echo ((empty($tutormodel->graduation_date) || $tutormodel->graduation_date == null || $tutormodel->graduation_date == "0000-00-00") ? "" : $tutormodel->graduation_date); ?>">
                                </div>
                            </div>
                        </div>
                       <!-- <div class="col-sm-4">
                            <div class="form-group">
                                <label for="location" class="control-label">Campus</label>
                                <?php //echo $form->textField($tutormodel,'location',array('class'=>'form-control','placeholder'=>'Campus')); ?>
                            </div>
                        </div>-->
                    </div>
                    <div class="form-container">
                        <h4>Allocated Students</h4>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-inline">
                                    <div class="form-group">
                                        <?php 
                                                $cri = new CDbCriteria();
                                                $cri->select = "sum(account_limit) as account_limit";
                                                $cri->condition = "institution_id =".$model->id;
                                                $tgmodel = TutorGroups::model()->find($cri);
                                              
                                                echo CHtml::textField('allocated_students','',array('id'=>'allocated_students','class'=>'form-control','placeholder'=>'Max No. of Students'))." ( ".($model->num_pupils - $tgmodel['account_limit']).' accounts remaining )';
                                           ?>
			                  			<?php echo CHtml::hiddenField('student_cnt',($model->num_pupils - $tgmodel['account_limit']),array('id'=>'student_cnt')); ?>
                                    </div>
                                    <p class="form-control-static hidden max-accounts-reached">You have reached the maximum students available.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-container">
                    <h4>Students</h4>
                    <div id="user-upload">
                        <h5>File Upload</h5>
                        <div class="user-file-upload form-inline">
                            <div class="form-group">
                                 <?php 
                                        echo CHtml::fileField('students','',array('class'=>'students-upload'));
                                  ?>
                            </div>
                            <span data-toggle="modal" data-target="#SampleDownload">
                                <a class="btn btn-default" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download a sample upload file">
                                    <i class="fa fa-info"></i>
                                </a>
                            </span>
                        </div>
                        <h5>
                            Manual Upload
                            <span class="user-limit-count pull-right">
                            
                                <span class="user-count">0</span> / <span class="user-limit"><?php echo ($model->num_pupils - $tgmodel['account_limit'])?></span>
                            </span>
                        </h5>
                        <div class="user-list"></div>
                        <div class="student_line" style="display: none">
                            <div class="row">
                                <div class="col-sm-3">
                                    <?php echo CHtml::textField('forenames[]', '', array('placeholder' => 'Forename','class'=>'form-control')); ?>
                                </div>
                                <div class="col-sm-3">
                                    <?php echo CHtml::textField('surname[]', '', array('placeholder' => 'Surname','class'=>'form-control')); ?>
                                </div>
                                <div class="col-sm-3">
                                    <?php echo CHtml::textField('email[]', '', array('placeholder' => 'Email','class'=>'form-control emailbox')); ?>
                                </div>
                                <div class="col-sm-3">
                                    <?php echo CHtml::textField('password[]', '', array('placeholder' => 'Password','class'=>'form-control passwordbox')); ?>
                                    <button type="button" class="remove_student" style="background-color: red;border-color: red"><i class="fa fa-close"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="user-actions clearfix">
                            <button type="button" class="btn btn-default add-new pull-left" id="add_student">Add Student</button>
                            <button type="button" class="btn btn-default generate-password pull-right">Generate Passwords</button>
                        </div>
                    </div>
                </div>

                <div class="form-actions clearfix">
                    <div class="pull-left">
                        <?php echo CHtml::submitButton('Save',array('class'=>'btn btn-default')); ?>
		        		<?php echo CHtml::button('Cancel',array('class'=>'btn btn-default',"onclick"=>"window.location='".$this->createUrl('institutions/insClasses',array("id"=>$model->id))."'")); ?>
                    </div>
                </div>

                <div class="modal fade" tabindex="-1" role="dialog" id="SampleDownload">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Download Sample Upload File</h4>
                            </div>
                            <div class="modal-body">
                                <p>When using the sample document, please alter the data in the columns accordingly. Please however do not add additional columns or alter the column headings.</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <a href="<?php echo $this->createUrl('institutions/sampleDownload')?>" class="btn btn-primary">Download File</a>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </div>

       <?php $this->endWidget(); ?>
    </div>
</div>


