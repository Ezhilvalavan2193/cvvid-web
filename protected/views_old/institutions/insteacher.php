<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */
/* @var $form CActiveForm */

$this->breadcrumbs = array(
    'Users' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
");
?>
<script>
    $(document).ready(function () {
    	
    });
</script>


<h1><?php echo $insmodel->isNewRecord ? "Create" : "Edit" ?> Teacher</h1>

<div id="Admin__content__inner">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'ins-teacher-form',
            'action' => ($insmodel->isNewRecord ? $this->createUrl('institutions/saveInsTeacher') : $this->createUrl('institutions/updateInsTeacher', array('id' => $model->id))),
            'enableAjaxValidation' => false,
            'htmlOptions' => array('enctype' => 'multipart/form-data')
        ));
        ?>
      <?php echo  $form->errorSummary(array($usermodel,$insmodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
        <div class="form-container">
            <!-- Your details -->
            <h4>Teacher details</h4>
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <?php echo $form->textField($usermodel, 'forenames', array('class' => 'form-control', 'placeholder' => 'Forenames')); ?>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?php echo $form->textField($usermodel, 'surname', array('class' => 'form-control', 'placeholder' => 'Surname')); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
        if (Yii::app()->user->getState("role") == "admin") {
            ?>
            <div class="form-container">
                <h4>Institution</h4>
                <!-- Your details -->
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <?php
                            $imodel = Institutions::model()->findAllByAttributes(array('deleted_at' => null));
                            $instlist = CHtml::listData($imodel, 'id', 'name');
                            echo $form->dropDownList($insmodel, 'id', $instlist, array('class' => 'form-control', 'empty' => '- Select Institution -'));
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="form-container">
            <h4>Account Details</h4>
            <div class="row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <?php
                        echo $form->emailField($usermodel, 'email', array('class' => 'form-control', 'placeholder' => 'Email'));
                        ?>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <?php
                        if ($usermodel->isNewRecord)
                            $usermodel->password = "";
                        else
                            $usermodel->password = "";
                        echo $form->passwordField($usermodel, 'password', array('class' => 'form-control', 'placeholder' => 'Password'));
                        ?>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                         <input class="form-control" placeholder="Confirm Password" type="password" value="" name="Users[confirmpassword]" id="confirmpassword">
                    </div>
                </div>
            </div>
        </div>

        <div class="form-actions clearfix">
            <div class="pull-left">
                <?php
                echo CHtml::submitButton('Save', array("class" => "btn btn-default"));
                echo CHtml::button('Cancel', array("class" => "btn btn-default","onclick"=>"window.location='".$this->createUrl('institutions/teachers')."'"));
                ?>   
            </div>
        </div>        
        <?php $this->endWidget(); ?>
       <?php if(!$usermodel->isNewRecord) { 
                 echo CHtml::button('Delete', array("class" => "btn btn-danger","onclick"=>"window.location='".$this->createUrl('institutions/deleteTeacher',array('id'=>$usermodel->id))."'"));
         
          } ?>

</div>

