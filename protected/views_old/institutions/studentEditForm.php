<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */
/* @var $form CActiveForm */

$this->breadcrumbs=array(
    'Users'=>array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
");
?>
<script>
$(document).ready(function(){
	
//	$('#slug_field').on('blur', function() {
//	    
//		 var ele = $(this);
//	 	 var value = $(this).val();
//	 	 if(value != "")
//	 	 {
//      		 $.ajax({
//                     url: '<?php echo $this->createUrl('users/checkSlug') ?>',
//                     type: 'POST',
//          	         data: {slug : value},
//                     success: function(data) {
//          				if(data == -1)
//                              $(ele).closest('.slug-field').removeClass('has-success').addClass('has-error');
//          				else
//          					$(ele).closest('.slug-field').removeClass('has-error').addClass('has-success');
//                     },
//          		     error: function(data) {		
//          		      //  alert(data);
//          		     }
//              });			
//	 	 }
//	 	 else
//	 	 {
//	 		$(ele).closest('.slug-field').removeClass('has-success').addClass('has-error');
//	 	 }
//
//	     return false;
//  });
	  
	$('#inst_list').change(function(){
		
		var id = $(this).val();
		$('.tutor_det').hide();
		
		 $.ajax({
	            url: '<?php echo $this->createUrl('institutions/getInstituteDetails') ?>',
	            type: 'POST',
		        data: {id : id},
	            success: function(data) {
		            
	            	var result = jQuery.parseJSON(data);
	            	$('#iname').html(result.name);
	            	$('#iadmin').html(result.admin);
	            	$('#icount').html(result.student_count);

	            	$.ajax({
	    	            url: '<?php echo $this->createUrl('institutions/getTutors')?>',
	    	            type: 'POST',
	    		        data: {id : id},
	    	            success: function(data) {
	    	            	
	    	            	$('#tutor_list').html(data);	    	            		    		           
	    	            },
	    			    error: function(data) {		
	    			        alert(data);
	    			    }
	    	         });
	            },
			    error: function(data) {		
			        alert(data);
			    }
	        });
	});

	$('#tutor_list').change(function(){
		if($(this).val() > 0)
		{
			
		 $.ajax({
	            url: '<?php echo $this->createUrl('institutions/getTutorDetails')?>',
	            type: 'POST',
		        data: {id : $(this).val()},
	            success: function(data) {
		            
	            	var result = jQuery.parseJSON(data);
	            	$('#tgname').html(result.name);
	            	$('#tname').html(result.teacher);
	            	$('#scount').html(result.student_count);
	            	$('.tutor_det').show();
	            },
			    error: function(data) {		
			        alert(data);
			    }
	        });
		}
		else
			$('.tutor_det').hide();
	});

	$('#delete_btn').click(function(){
		$.ajax({
            url: '<?php echo $this->createUrl('institutions/deleteStudent',array('id'=>$tgsmodel->id))?>',
            type: 'POST',
            success: function(data) {
	            window.location = "<?php echo Yii::app()->createUrl('institutions/editSubscriptionDetails',array('id'=>$model->id)); ?>";
            },
		    error: function(data) {		
		        alert(data);
		    }
        });
	});
	
});

</script>


 <h1>Edit Student</h1>
    <div class="Admin__content__inner">
        <div class="container">
             <?php 
        $mediamodel = new Media();
        
        $form = $this->beginWidget('CActiveForm', array(
                    	'id'=>'edit-student-form',
                        'action'=>$this->createUrl('institutions/updateStudent',array('id'=>$tgsmodel->id)),
                    	'enableAjaxValidation'=>false,
                        'htmlOptions' => array('enctype' => 'multipart/form-data')
                    ));         
  ?>
                <div class="row">
                    <div class="col-sm-3 col-md-2">
                        <div class="form-group">
                            <label for="photo_id" class="control-label">Profile Photo</label>
                            <div class="photo-section">
                                <img src="<?php echo Yii::app()->baseUrl?>/images/media/<?php echo $profilemodel->photo_id?>/conversions/thumb.jpg" alt="" id="profile-pic" class="img-responsive">
                                 <input type="hidden" name="photo_id" id="photo_id" value="<?php echo $profilemodel->photo_id?>">
                                <button type="button" class="add-media btn btn-primary btn-xs" id="photo-btn">Add Logo</button>            
                                <button type="button" class="remove-media btn btn-primary btn-xs" id="remove-photo-btn">Remove Logo</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-9 col-md-10">
                        <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Account Details</h3>
                                </div>
                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="forenames" class="control-label">Forenames</label>
                                                <?php echo $form->textField($usermodel,'forenames',array('class'=>'form-control')); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="surname" class="control-label">Surname</label>
                                                <?php echo $form->textField($usermodel,'surname',array('class'=>'form-control')); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="email" class="control-label">Email</label>
                                                <?php echo $form->textField($usermodel,'email',array('class'=>'form-control')); ?>
                                            </div>
                                        </div>
                                    </div><!-- .row -->

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="password" class="control-label">Password</label>
                                                <?php echo $usermodel->password = "";?>
                		               <?php echo $form->passwordField($usermodel,'password',array('class'=>'form-control')); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="password_confirmation" class="control-label">Confirm Password</label>
                                                <?php echo CHtml::passwordField('confirmpassword','',array('class'=>'form-control')); ?>
                                             <div class="errormessage"><?php echo (isset($pwderr) && $pwderr != "" ? $pwderr : "") ?></div>
                                            </div>
                                        </div>
                                    </div><!-- .row -->

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="slug" class="control-label">Link</label>
                                                <div class="input-group slug-field">
                                                    <span class="input-group-addon">http://www.cvvid.com/</span>
                                                    <?php echo $form->textField($profilemodel,'slug',array('class'=>'form-control','id'=>'slug')); ?>
                                                    <span class="input-group-addon help-block"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div><!-- .panel-body -->
                            </div><!-- .panel -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Institution Details</h3>
                            </div>
                            <div class="panel-body">

                                <div id="institution-tutor-form">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Institution</label>
                                                <?php             			
                                                        $insmodel = Institutions::model()->findAllByAttributes(array('deleted_at'=>null));
                                                        $instlist = CHtml::listData($insmodel, 'id', 'name');

                                                    echo $form->labelEx($model,'institution');            
                                                    echo $form->dropDownList($model, 'id', $instlist,array('class'=>'form-control select2','id'=>'inst_list'));
                                                ?>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Tutor Group</label>
                                                <?php             				    
                                                        $tutorgroups = TutorGroups::model()->findAllByAttributes(array('institution_id'=>$model->id));
                                                        $tutorgrouplist = CHtml::listData($tutorgroups, 'id', 'name');
                                                        echo CHtml::label('Tutor Group', '',array());
                                                        echo CHtml::dropDownList('tutor_group_id','',$tutorgrouplist,array('empty'=>'','required'=>'required','class'=>'form-control select2','id'=>'tutor_list'));             				    
                                                    ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="institution-details">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Institution Name</label>
                                                            <p class="form-control-static" id="iname"><?php echo $model->name; ?></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Admin</label>
                                                            <p class="form-control-static" id="iadmin"><?php echo $usermodel->forenames." ".$usermodel->surname; ?></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Student Count</label>
                                                            <?php 
                                                            $student_cnt = TutorGroupsStudents::model()->countByAttributes(array('institution_id'=>$model->id));
                                                            ?>
                                                            <p class="form-control-static" id="icount"><?php echo $student_cnt.' / '.$model->num_pupils; ?></p>
                                                        </div>
                                                    </div>
                                                </div></div>
                                            <div class="tutor-group-details">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Tutor Group Name</label>
                                                            <p class="form-control-static" id="tgname"><?php echo $model->name; ?></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Teacher</label>
                                                            <p class="form-control-static" id="tname"><?php echo $model->name; ?></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Student Count</label>
                                                            <p class="form-control-static" id="scount"></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                        <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Details</h3>
                                </div>
                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="dob" class="control-label">Date of Birth</label>
                                                <div class="input-group">
                                                     <input type="text" name="dob" placeholder="Date of Birth" class="dob form-control" value="<?php echo ((empty($usermodel->dob) || $usermodel->dob == null || $usermodel->dob == "0000-00-00") ? "" : date("d/m/Y", strtotime($usermodel->dob))); ?>">
                                                   
                                                </div>
                                            </div>
                                        </div><!-- .col-sm-6 -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="tel" class="control-label">Telephone</label>
                                                <?php echo $form->textField($usermodel,'tel',array('class'=>'form-control')); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="mobile" class="control-label">Mobile</label>
                                                <?php echo $form->textField($usermodel,'mobile',array('class'=>'form-control')); ?>
                                            </div>
                                        </div><!-- .col-sm-6 -->
                                    </div><!-- .row -->

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="current_job" class="control-label">Current Job</label>
                                                <?php echo $form->textField($usermodel,'current_job',array('class'=>'form-control')); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- .panel-body -->
                            </div><!-- .panel -->

                        <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Address</h3>
                                </div>
                                <div class="panel-body">
                                      <div id="location-fields">
                                            <?php $this->renderPartial('//addresses/address-form', array('form'=>$form,'addressmodel'=>$addressmodel)); ?>
                                        </div>

                                </div>
                            </div>       
                        
                        <div class="form-actions clearfix">
                            <?php echo CHtml::submitButton('Save',array("class"=>"btn btn-primary pull-right")); ?>
                        </div>
                    </div>
                </div>        
           <?php $this->endWidget(); ?>
        </div>
    </div>


<script type="text/javascript">
        <?php if(isset($profilemodel->id)){ ?>
        $(function(){
            new CandidateSlugGenerator(<?php echo $profilemodel->id; ?>);
        });
        <?php } ?>
    </script>