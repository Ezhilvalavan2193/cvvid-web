<?php 
/* @var $this InstitutionsController */
/* @var $model Institutions */

$this->breadcrumbs=array(
	'Institutions'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
    	$('.search-form').toggle();
    	return false;
    });
");
?> 
<script>

$(document).ready(function(){

	$('.photo-section').on('click','#photo-btn',function(e){
		 e.preventDefault();
		 $("#profile-photo").trigger('click');
	});

});

</script>

<?php
        $planmodel = Plans::model()->findByAttributes(array('owner_id'=>$model->id));
             
?>


    <h1>TIGHS: Admin</h1>
    <div class="Admin__content__inner">
        <!-- Nav tabs -->
        <ul class="AdminTabs nav nav-tabs" role="tablist">
            <li role="presentation"><a href="<?php echo Yii::app()->createUrl("institutions/tabeditinstitute", array("id" => $model->id)); ?>" aria-controls="details">Details</a></li>
            <li role="presentation" class="active"><a href="<?php echo Yii::app()->createUrl("institutions/tabeditadmin", array("id" => $model->id)); ?>">Admin</a></li>
            <li role="presentation"><a href="<?php echo Yii::app()->createUrl("institutions/tabeditteachers", array("id" => $model->id)); ?>">Teachers</a></li>
            <li role="presentation"><a href="<?php echo Yii::app()->createUrl("institutions/tabedittutorgroups", array("id" => $model->id)); ?>">Tutor Groups</a></li>
            <li role="presentation"><a href="<?php echo Yii::app()->createUrl("institutions/tabeditstudents", array("id" => $model->id)); ?>">Students</a></li>
        </ul>
        <div class="row">
            <div class="col-sm-6">

                <?php       
                $form = $this->beginWidget('CActiveForm', array(
                            	'id'=>'current-admin-form',
                                'action'=>$this->createUrl('institutions/updateCurrentAdmin',array('id'=>$model->id)),
                            	'enableAjaxValidation'=>false,
                                'htmlOptions' => array('enctype' => 'multipart/form-data')
                        ));              
           ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">Update Current Admin</div>
                        <div class="panel-body">


                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="forenames" class="control-label">Forenames</label>
                                        <?php echo $form->textField($usermodel,'forenames',array('class'=>'form-control')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="surname" class="control-label">Surname</label>
                                        <?php echo $form->textField($usermodel,'surname',array('class'=>'form-control')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="email" class="control-label">Email</label>
                                       <?php echo $form->textField($usermodel,'email',array('class'=>'form-control')); ?>
                                    </div>
                                </div>
                            </div><!-- .row -->

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="password" class="control-label">Password</label>
                                        <?php $usermodel->password=''; ?>
                                        <?php echo $form->passwordField($usermodel,'password',array('class'=>'form-control')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="password_confirmation" class="control-label">Confirm Password</label>
                                        <?php echo CHtml::passwordField('current_password','',array('class'=>'form-control')); ?>
                                    </div>
                                </div>
                            </div><!-- .row -->

                            <button type="submit" class="btn btn-primary">Update</button>

                        </div><!-- .panel-body -->
                    </div><!-- .panel -->
                 <?php $this->endWidget(); ?>

            </div><!-- .col-sm-6 -->

            <div class="col-sm-6">
             
               <?php  $form = $this->beginWidget('CActiveForm', array(
                    	'id'=>'new-admin-form',
                        'action'=>$this->createUrl('institutions/addNewAdmin',array('id'=>$model->id)),
                    	'enableAjaxValidation'=>false,
                        'htmlOptions' => array('enctype' => 'multipart/form-data')
                     ));  
                    echo  $form->errorSummary(array($newusermodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger'));
             
              ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">Change Admin</div>
                        <div class="panel-body">


                            <div class="new-admin-form">
                                <p>This will remove the current admin but will maintain a reference.</p>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="forenames" class="control-label">Forenames</label>
                                            <?php echo $form->textField($newusermodel,'forenames',array('class'=>'form-control')); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="surname" class="control-label">Surname</label>
                                            <?php echo $form->textField($newusermodel,'surname',array('class'=>'form-control')); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="email" class="control-label">Email</label>
                                            <?php echo $form->textField($newusermodel,'email',array('class'=>'form-control')); ?>
                                        </div>
                                    </div>
                                </div><!-- .row -->

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="password" class="control-label">Password</label>
                                            <?php echo $form->passwordField($newusermodel,'password',array('class'=>'form-control')); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="password_confirmation" class="control-label">Confirm Password</label>
                                            <?php echo CHtml::passwordField('current_password','',array('class'=>'form-control')); ?>
                                        </div>
                                    </div>
                                </div><!-- .row -->
                            </div><!-- .new-admin-form -->

                            <button type="submit" class="btn btn-primary">Save</button>
                        </div><!-- .panel-body -->
                    </div><!-- .panel -->
               <?php $this->endWidget(); ?>
            </div><!-- .col-sm-6 -->

        </div><!-- .row -->

    </div>




