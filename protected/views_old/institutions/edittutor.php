<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */
/* @var $form CActiveForm */
?>
<script>

$(document).ready(function(){

	$("#deleteTutor").click(function(){
		
		 $.ajax({
	            url: '<?php echo $this->createUrl('institutions/deleteTutor') ?>',
	            type: 'POST',
	            data: {id : <?php echo $tutormodel->id?>}, 	    
	            success: function(data) {
		            window.location = "<?php echo $this->createUrl('institutions/tabedittutorgroups',array('id'=>$model->id)); ?>"
	            },
			    error: function(data) {		
			        alert('err');
			    }
	         });  
	});
	
	$('#teacherslist').change(function(){
		if($(this).val() > 0)
			$('.teacher-section').hide();
		else
			$('.teacher-section').show();	
	});	

	$("#add_student").click(function(){
		
		if($('.student-section').find('.row').length >= $("#student_cnt").val())
		{
			alert('Student limit reached');    		
		}
		else
		{
			var studclone = $('.student_line').find('.row').clone();		
    		$(".student-section").append(studclone);
		}
			
	});

	$(".student-section").on('click','.remove_student',function(){
		$(this).closest('.row').remove();
	});
});
</script>



    <h1>Edit Tutor Group</h1>
    <div class="Admin__content__inner">
    <?php if (Yii::app()->user->hasFlash('success')): ?>
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                        <?php echo Yii::app()->user->getFlash('success'); ?>
            </div>
                    <?php endif; ?>
       <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'tutor-form',
            'action'=>$this->createUrl('institutions/updateTutor',array('id'=>$tutormodel->id)),
                'enableAjaxValidation'=>false
        )); ?>
           <?php //echo CHtml::hiddenField('user_id',($usermodel->isNewRecord ? null : $usermodel->id),array()); ?>
            <div id="user-group-form">
                <!-- Teacher -->
                <div id="user-manager">
                    <div class="form-container">
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="form-group">
                                    <label for="teacher_id" class="control-label">Teacher</label>
                                    <?php 
                                        $umodel = Users::model()->findByPk($tutormodel->teacher_id);
                                       $teachermodel = Users::model()->findAllByAttributes(array('deleted_at'=>null,'type'=>'teacher','status'=>'active'));
                                       $teacherlist = CHtml::listData($teachermodel, 'id', 'forenames');
                                       //array_unshift($teacherlist, '- New Teacher -');
                                       $teacherlist = array('0'=>'- New Teacher -') + $teacherlist;

                                    ?>

                                    <?php echo $form->dropDownList($tutormodel,'teacher_id',$teacherlist,array('class'=>'form-control select2','id'=>'teacherslist')); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="user-manager-form hidden">
                        <div class="form-container">
                            <!-- Your details -->
                            <h4>Teacher details</h4>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?php echo $form->textField($usermodel,'forenames',array('class'=>'form-control','placeholder'=>'First Name')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?php echo $form->textField($usermodel,'surname',array('class'=>'form-control','placeholder'=>'Surname')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-container">
                            <h4>Account Details</h4>
                            <div class="row">
                                <div class="col-sm-3">
                                    <?php echo $form->textField($usermodel,'email',array('class'=>'form-control','placeholder'=>'Email')); ?>
                                </div>
                                <div class="col-sm-3">
                                    <?php $usermodel->password=''; ?>
                                    <?php echo $form->textField($usermodel,'password',array('class'=>'form-control','placeholder'=>'Password')); ?>
                                </div>
                                <div class="col-sm-3">
                                    <?php echo CHtml::textField('confirmpassword','',array('class'=>'form-control','placeholder'=>'Confirm Password')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Tutor Group -->
                <div class="form-container">
                    <h4>Tutor Group Details</h4>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="name" class="control-label">Tutor Group</label>
                                <?php echo $form->textField($tutormodel,'name',array('class'=>'form-control','placeholder'=>'Name')); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="graduation_date" class="control-label">Graduation Date</label>
                                <?php 
                                $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                                    'name'=>'graduation_date',
                                    // 'model'=>$model,
                                    'value' => ((empty($tutormodel->graduation_date) || $tutormodel->graduation_date == null || $tutormodel->graduation_date == "0000-00-00") ? "" : $tutormodel->graduation_date),
                                    // additional javascript options for the date picker plugin
                                    'options'=>array(
                                        'dateFormat'=>'dd//mm/yyyy'
                                    ),
                                    'htmlOptions'=>array(
                                        'class'=>'datepicker form-control', 'autocomplete'=>'off'
                                        // 'style'=>'height:20px;'
                                    ),
                                ));

                                ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="location" class="control-label">Campus</label>
                                <?php echo $form->textField($tutormodel,'location',array('class'=>'form-control','placeholder'=>'Campus')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-container">
                        <h4>Allocated Students</h4>
                        <div class="row">
                            <div class="form-inline">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                         <?php 
                                                $tutorstudcnt = TutorGroupsStudents::model()->countByAttributes(array('institution_id'=>$model->id,'tutor_group_id'=>$tutormodel->id));
                                                $studcnt = $model->num_pupils - $tutorstudcnt;
                                                echo CHtml::textField('allocated_students',$tutorstudcnt,array('class'=>'form-control'))." ".CHtml::label('( '.$studcnt.' accounts remaining)', '',array());
                                            ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="form-actions">
                    <?php echo CHtml::submitButton('Save Class',array('class'=>'btn btn-default')); ?>
                    <?php echo CHtml::button('Cancel',array('class'=>'btn btn-default')); ?>
                </div>
            </div>

       <?php $this->endWidget(); ?>
			  <button class="btn btn-danger" id="deleteTutor">Delete</button>          
    </div>



