<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */

$this->breadcrumbs = array(
    'Institutions' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
    
");
?>
<?php
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$model->id,'type'=>$model->type));        
        $addressmodel = Addresses::model()->findByAttributes(array('model_id'=>$model->id));
        if($addressmodel == null)
            $addressmodel = Addresses::model(); 
?>
<h1>Edit Student User</h1>
<div class="Admin__content__inner">
    <div class="CandidateForm">
        <ul class="AdminTabs nav nav-tabs">
            <li role="presentation">
                <a href="<?php echo Yii::app()->createUrl("institutions/tabeditstudent", array("id" => $tgsmodel->id)); ?>">Details</a>
            </li>
            <li role="presentation">
                <a href="<?php echo Yii::app()->createUrl("institutions/tabeditprofile", array("id" => $tgsmodel->id)); ?>">Profile</a>
            </li>
            <li role="presentation">
                <a href="<?php echo Yii::app()->createUrl("institutions/tabeditdocuments", array("id" => $tgsmodel->id)); ?>">Documents</a>
            </li>
            <li role="presentation"  class="active">
                <a href="<?php echo Yii::app()->createUrl("institutions/tabeditapplications", array("id" => $tgsmodel->id)); ?>">Applications</a>
            </li>
        </ul>

        <div class="row">
            <div class="col-sm-12">
                <div class="UserExperiences">
                    <h3>Job Applications</h3>
                    <?php 
         $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'profile-job-grid',
        'itemsCssClass' => 'table',
	'summaryText'=>'',
	//'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'dataProvider'=>JobApplications::model()->searchByID($model->id),
        	'columns'=>array(
        	    
            			array(
            					'header'=>'Company Name',
            					'value'=>'$data->getCompany($data->job->owner_id)',        			        
            			),
            			array(
            					'header'=>'Job Title',
            			        'value'=>'$data->job->title',
            					
            			),
                	    array(
                	        'header'=>'Salary',
                	        'value'=>'$data->job->salary_min." to ".$data->job->salary_min." per ".$data->job->salary_type',
                	        
                	    ),
                	    array(
                	        'header'=>'Location',
                	        'value'=>'$data->job->location',
                	        
                	    ),
                	    array(
                	        'header'=>'Applied on',
                	        'value'=>'$data->job->updated_at',
                	    )
        	 ),
  ));
?>

                </div>

            </div>

        </div>


    </div>
</div>
