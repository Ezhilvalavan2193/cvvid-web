<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */
/* @var $form CActiveForm */
?>
<script>
$(document).ready(function(){

	$('#tutorslist').change(function(){
		var id = $(this).val(); 
		if(id > 0)
		{
			   $.ajax({
 		            url: '<?php echo $this->createUrl('institutions/getAllocStudents') ?>',
 		            type: 'POST',
 		            data: {id : id}, 	    
 		            success: function(data) {
                                //alert(data);return false;
 	 		            var obj = $.parseJSON(data);
 		            	$('#allocated_students').text(obj.count+' / '+obj.total);
 		            	if(parseInt(obj.count) >= parseInt(obj.total))
 		            	{
 		            		$('#msg_err_msg').html('Cannot add student,students limit reached.');
 		            		$('#msg_err_msg').show();
 	 		            	$('#save_stud').prop('disabled',true);
 		            	}
 		            	else
 		            	{
 		            		$('#msg_err_msg').html('');
 		            		$('#msg_err_msg').hide();
 		            		$('#save_stud').prop('disabled',false);
 		            	}
 	 		            	
 		            },
// 				    error: function(data) {		
// 				        alert('err');
// 				    }
 		         });  
		}
	});	


});
</script>


<div class="page-title text-left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $studmodel->isNewRecord ? "Create" : "Update"?> Student</h1>
            </div>
        </div>
    </div>
</div>
<div id="page-content">
    <div class="container">
    <div class="alert alert-danger" id='msg_err_msg' style="display: none">
			</div>
        <?php 
            $target = (isset($_GET['target']) ? $_GET['target'] : "");
            if($usermodel->isNewRecord)
                $url = $this->createUrl('institutions/saveInsStudent');
            else
                $url = $this->createUrl('institutions/updateInsStudent',array('id'=>$studmodel->id,'target'=>$target));

              $form=$this->beginWidget('CActiveForm', array(
                  'id'=>'add-stud-form',
                  'action'=>$url,
                  'enableAjaxValidation'=>false,
                  
          )); ?>
          <?php echo  $form->errorSummary(array($usermodel,$studmodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
            <div class="form-container">
                <!-- Your details -->
                <h4>Student details</h4>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <?php echo $form->textField($usermodel,'forenames',array('class'=>'form-control','placeholder'=>'First Name')); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <?php echo $form->textField($usermodel,'surname',array('class'=>'form-control','placeholder'=>'Surname')); ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-container">
                <!-- Your details -->
                <h4>Class details</h4>
                <div id="student-tutor-group">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="tutor_group_id" class="control-label">Class</label>
                                <?php 
                                $tgmodel = TutorGroups::model()->findAllByAttributes(array('deleted_at'=>null,'institution_id'=>$model->id));
                                $tglist = CHtml::listData($tgmodel, 'id', 'name');
                                     echo $form->dropDownList($tutormodel,'id',$tglist,array('class'=>'form-control','id'=>'tutorslist','empty'=>'- Select Class -'));
                                  ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Allocated Accounts</label>
                                <p class="form-control-static tutor-group-limit" id="allocated_students"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <label for="graduation_date" class="control-label">Graduation Date</label>
                            <input type="text"  autocomplete='off' name="graduation_date" class="datepicker form-control" value="<?php echo ((empty($tutormodel->graduation_date) || $tutormodel->graduation_date == null || $tutormodel->graduation_date == "0000-00-00") ? "" : $tutormodel->graduation_date)?>">
                           
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-container">
                <h4>Account Details</h4>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <?php echo $form->textField($usermodel,'email',array('class'=>'form-control','placeholder'=>'Email')); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <?php 
			  // if($usermodel->isNewRecord)
			     $usermodel->password = ""; 
			   //else
			 //    $usermodel->password = " ";
			?>
				<?php echo $form->textField($usermodel,'password',array('class'=>'form-control','id'=>'password','placeholder'=>'Password')); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <?php echo CHtml::textField('Confirm Password', $usermodel->password ,array('class'=>'form-control','id'=>'confirmpassword','placeholder'=>'Confirm Password')); ?>
			
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-actions clearfix">
                <div class="pull-left">
                    <?php echo CHtml::submitButton('Save',array('class'=>'btn btn-default','id'=>'save_stud')); ?>
		<?php 
		  if(isset($_GET['target']) && $_GET['target'] != "")
		      $returnurl = $this->createUrl($_GET['target']);
		  else
		      $returnurl = $this->createUrl('institutions/insStudents',array('id'=>$model->id));
		  
		      echo CHtml::button('Cancel',array('class'=>'btn btn-default','onclick'=>'window.location="'.$returnurl.'"')); ?>
                </div>
            </div>       
       <?php $this->endWidget(); ?>
    </div>
</div>

