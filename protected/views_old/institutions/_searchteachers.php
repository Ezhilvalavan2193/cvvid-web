<?php

/* @var $this InstitutionsController */
/* @var $model Institutions */
/* @var $form CActiveForm */
?>


<?php

$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
        ));
?>

<?php echo CHtml::textField('fullname', $fullname, array('class' => 'form-control', 'placeholder' => 'Teacher name')); ?>

<?php

$imodel = $insmodel->findAllByAttributes(array('deleted_at' => null));
$inslist = CHtml::listData($imodel, 'id', 'name');
echo $form->dropDownList($insmodel, 'id', $inslist, array('class' => 'form-control', 'empty' => '- Select Institutions -'));
?>

<?php

echo CHtml::submitButton('Search', array("class" => 'btn btn-primary'));
?>

<?php $this->endWidget(); ?>

