<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */

$this->breadcrumbs=array(
	'Institutions'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "

");
?>

<script>
$(document).ready(function(){
	setTimeout(function() {
        $('.Conversation__sent-message').fadeOut('fast');
        }, 3000);
	
});
</script>

  <h3 class="Conversation__subject"><?php echo $convmodel->subject?></h3>
   <?php if(Yii::app()->user->hasFlash('success'))            
          {
     ?>
        <div class="Conversation__sent-message alert alert-success">
            <?php echo Yii::app()->user->getFlash('success') ?> 
        </div>
    <?php } 
    
            $usermodel = Users::model()->findByPk($to_user);
            if($usermodel == null || $usermodel->deleted_at != null)
                echo "<h3 style='color:red'>This user is no longer active and cannot receive messages</h3>";
            else 
            {
        ?>
    <div id="MessageForm"><div class="NewMessageForm"><div class="clearfix">
    
         <?php 
               echo CHtml::textArea('message','',array('placeholder'=>'Message...','class'=>'form-control','id'=>'message','rows'=>"5"));                   
                ?>
        <div class="pull-right">
            <?php  echo CHtml::button('Send',array('class'=>'btn default-btn','id'=>'send_msg'));?>
        </div>
    
</div>
</div>
</div>
 <?php }?>
      <?php     
      
         
      
        $cri1 = new CDbCriteria();
        $cri1->condition = "conversation_id =".$convmodel->id;
        $cri1->order = "created_at desc";
        $convmsgmodel = ConversationMessages::model()->findAll($cri1);
        foreach ($convmsgmodel as $cmsg)
        {
            
      ?>
       
        <div id="MessageList">
             <div class="Message">
             	<div class="Message__meta clearfix">
                    <div class="Message__user pull-left">
                       <?php echo $cmsg['name']?>
                    </div>
                    <div class="Message__date pull-right">
                        Date: <?php echo $cmsg['created_at'] ?>
                    </div>
                </div>
                <div class="Message__content">
                    <?php echo $cmsg['message']?>
                </div>
              </div>
        </div>
        
 <?php } ?>
</div>




