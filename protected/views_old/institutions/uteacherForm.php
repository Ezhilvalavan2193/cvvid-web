<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */
/* @var $form CActiveForm */
?>

<div class="page-title text-left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $usermodel->isNewRecord ? "Create" : "Update" ?> Teacher</h1>
            </div>
        </div>
    </div>
</div>

<div id="page-content">
    <div class="container">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'edit-teachers-form',
            'action' => $this->createUrl('institutions/updateiTeacher', array('id' => $model->id)),
            'enableAjaxValidation' => false
        ));
        ?>
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <div class="form-container">
                    <!-- Your details -->
                    <h4>Teacher details</h4>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?php echo CHtml::hiddenField("user_id", $usermodel->id, array()); ?>
                                <?php echo $form->textField($usermodel, 'forenames', array('class' => 'form-control', 'placeholder' => 'First Name')); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?php echo $form->textField($usermodel, 'surname', array('class' => 'form-control', 'placeholder' => 'Surname')); ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-container">
                    <h4>Account Details</h4>
                    <div class="row">
                        <div class="col-sm-3">
                            <?php echo $form->textField($usermodel, 'email', array('class' => 'form-control', 'placeholder' => 'Email')); ?>
                        </div>
                        <div class="col-sm-3">
                            <?php $usermodel->password = "";
                            echo $form->passwordField($usermodel, 'password', array('class' => 'form-control', 'placeholder' => 'Password'));
                            ?>
                        </div>
                        <div class="col-sm-3">
<?php echo CHtml::passwordField('confirm_password', '', array('class' => 'form-control', 'placeholder' => 'Confirm Password')); ?>
                        </div>
                    </div>
                </div>

                <div class="form-actions">
                    <input class="btn btn-default" type="submit" value="Save">
                <?php echo CHtml::button('Cancel', array('class' => 'btn btn-default', "onclick" => "window.location='" . $this->createUrl('institutions/insTeachers', array('id' => $model->id)) . "'")); ?>
                </div>        
<?php $this->endWidget(); ?>
            </div>
        </div>

    </div>
</div>
