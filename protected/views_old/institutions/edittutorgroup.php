<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */
/* @var $form CActiveForm */
?>
<script>
$(document).ready(function(){

	$('#inslist').change(function(){
		if($(this).val() > 0)
		{
			 $.ajax({
		            url: '<?php echo $this->createUrl('institutions/getNumPupils') ?>',
		            type: 'POST',
		            data: {id : $(this).val()}, 	    
		            success: function(data) {
		            	$('#accounts_remain').html(data);
		            	$('#student_cnt').val(data);
		            },
				    error: function(data) {		
				        alert('err');
				    }
		         });
		}
	});	
	
	$('#teacherslist').change(function(){
		if($(this).val() > 0)
			$('.teacher-section').hide();
		else
			$('.teacher-section').show();	
	});	

	$("#add_student").click(function(){
// 		alert($('.student-section').find('.row').length -1);
		var cnt = $('#allocated_students').val() > 0 ? $('#allocated_students').val() : $("#student_cnt").val();
		if($('.student-section').find('.row').length >= cnt)
		{
			alert('Allocated Students limit reached');    		
		}
		else
		{			
    			var studclone = $('.student_line').find('.row').clone();	
        		$(".student-section").append(studclone);
			
		}
			
	});

	$(".student-section").on('click','.remove_student',function(){
		$(this).closest('.row').remove();
	});
});
</script>

    <h1>Edit Tutor Group</h1>
    <div class="Admin__content__inner">
   
       <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tutor-group-form',
           'action'=>$this->createUrl('institutions/updateTutorGroup',array('id'=>$tutormodel->id)),
                'enableAjaxValidation'=>false
        )); ?>
         <?php echo  $form->errorSummary(array($usermodel,$tutormodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
            <div id="user-group-form">
                <!-- Teacher -->
                <div id="user-manager">

                    <div class="form-container">
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="form-group">
                                    <label for="teacher_id" class="control-label">Teacher</label>
                                     <?php 
                                        echo CHtml::hiddenField('institution_id',$model->id,array());
                                        $cri = new CDbCriteria();
                                        $cri->alias = "iu";
                                        $cri->select = "u.id as id, CONCAT(u.forenames,' ',u.surname) as user_id";
                                        $cri->join = 'inner join cv16_institutions i on i.id = iu.institution_id
                                              inner join cv16_users u on u.id = iu.user_id';
                                        $cri->condition = "i.deleted_at is null and u.deleted_at is null and iu.role = 'teacher' and i.id =".$model->id;
                                        $teachermodel = InstitutionUsers::model()->findAll($cri);
                                        // $teachermodel = $usermodel->findAllByAttributes(array('deleted_at' => null, 'type' => 'teacher', 'status' => 'active'));
                                       
                                        $teacherlist = array();
                                        foreach ( $teachermodel as $teachers)
                                            $teacherlist[$teachers['id']] = $teachers['user_id'];
                                        //array_unshift($teacherlist, '- New Teacher -');
                                       // $teacherlist = array('0'=>'- New Teacher -') + $teacherlist;
                                       
                                       $tgeachersmodel = TutorGroupsTeachers::model()->findAllByAttributes(array('tutor_group_id'=>$tutormodel->id,'institution_id'=>$tutormodel->institution_id));
                                       $teac = array();
                                       foreach ($tgeachersmodel as $tea)
                                       {
                                           $teac[] = $tea['teacher_id'];
                                       }
                                       $gteachers = new TutorGroupsTeachers();
                                       $gteachers->teacher_id = $teac;
                                       echo $form->dropDownList($gteachers,'teacher_id',$teacherlist,array('class'=>'form-control select2','id'=>'teacherslist','multiple'=>true)); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- <div class="form-container teacher-section">
                        <div class="user-manager-form <?php echo $usermodel->isNewRecord == null ? "hidden" : "" ?>">
                            <div class="form-container">
                                <h4>Teacher details</h4>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?php echo $form->textField($usermodel,'forenames',array('class'=>'form-control','placeholder'=>'First Name')); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?php echo $form->textField($usermodel,'surname',array('class'=>'form-control','placeholder'=>'Surname')); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-container">
                                <h4>Account Details</h4>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <?php echo $form->textField($usermodel,'email',array('class'=>'form-control','placeholder'=>'Email')); ?>
                                    </div>
                                    <div class="col-sm-3">
                                        <?php $usermodel->password = ""; ?>
			                <?php echo $form->textField($usermodel,'password',array('class'=>'form-control','placeholder'=>'Password')); ?>
                                    </div>
                                    <div class="col-sm-3">
                                        <?php echo CHtml::textField('Confirm Password','',array('class'=>'form-control','placeholder'=>'Confirm Password')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>-->

                </div>

                <!-- Tutor Group -->
                <div class="form-container">
                    <h4>Tutor Group Details</h4>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="name" class="control-label">Tutor Group</label>
                                <?php echo $form->textField($tutormodel,'name',array('class'=>'form-control','placeholder'=>'Name')); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="graduation_date" class="control-label">Graduation Date</label>
                                <input type="text" name="graduation_date" autocomplete='off' class="datepicker form-control" value="<?php echo ((empty($tutormodel->graduation_date) || $tutormodel->graduation_date == null || $tutormodel->graduation_date == "0000-00-00") ? "" : $tutormodel->graduation_date)?>">
                                
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="location" class="control-label">Campus</label>
                                <?php echo $form->textField($tutormodel,'location',array('class'=>'form-control','placeholder'=>'Campus')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-container">
                        <h4>Allocated Students</h4>
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="form-group">
                                      <?php 
                $tutorstudcnt = TutorGroupsStudents::model()->countByAttributes(array('institution_id'=>$model->id,'tutor_group_id'=>$tutormodel->id));
                $studcnt = $model->num_pupils - $tutorstudcnt;
                echo CHtml::textField('allocated_students',$tutorstudcnt,array('id'=>'allocated_students','class'=>'form-control'))." ( ".CHtml::label($studcnt, '',array('id'=>'accounts_remain')).' accounts remaining )';
            ?>
			<?php echo CHtml::hiddenField('student_cnt',$studcnt,array('id'=>'student_cnt')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="form-actions">
                    <?php echo CHtml::submitButton('Save Class',array('class'=>'btn btn-default')); ?>
					<?php echo CHtml::button('Cancel',array('class'=>'btn btn-default',"onclick"=>"window.location='".$this->createUrl('institutions/tutorgroups')."'")); ?>
                </div>
            </div>

        </form>
        	<?php echo CHtml::button('Delete',array('class'=>'btn btn-danger',"onclick"=>"window.location='".$this->createUrl('institutions/deleteTutorGroup',array('id'=>$tutormodel->id))."'")); ?>
			
      <?php $this->endWidget(); ?>





