<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */
/* @var $form CActiveForm */

$this->breadcrumbs=array(
    'Users'=>array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
");
?>
<script>
$(document).ready(function(){
	
//    	$('#slug_field').on('blur', function() {
//    
//    		 var ele = $(this);
//    	 	 var value = $(this).val();
//    	 	 if(value != "")
//    	 	 {
//           	   $.ajax({
//                      url: '<?php echo $this->createUrl('users/checkSlug')  ?>',
//                      type: 'POST',
//           	       	  data: {slug : value},
//                      success: function(data) {
//           				if(data == -1)
//                            $(ele).closest('.slug-field').removeClass('has-success').addClass('has-error');
//           				else
//           					$(ele).closest('.slug-field').removeClass('has-error').addClass('has-success');
//                      },
//           		    error: function(data) {		
//           		      //  alert(data);
//           		    }
//                  });			
//    	 	 }
//    	 	 else
//    	 	 {
//    	 		$(ele).closest('.slug-field').removeClass('has-success').addClass('has-error');
//    	 	 }
//        	 return false;
//       });

//    	$('#forename,#surname').on('blur', function() {    		
//             var forename = $('#forename').val();
//             var surname = $('#surname').val();
//             $('#slug_field').val(forename+"-"+surname);
//             
//             $.ajax({
//                 url: '<?php echo $this->createUrl('users/checkSlug')  ?>',
//                 type: 'POST',
//      	       	  data: {slug :  $('#slug_field').val()},
//                 success: function(data) {
//      				if(data == -1)
//                       $('.slug-field').removeClass('has-success').addClass('has-error');
//      				else
//      				   $('.slug-field').removeClass('has-error').addClass('has-success');
//                 },
//      		    error: function(data) {		
//      		      //  alert(data);
//      		    }
//             });	             
//             
//    	});
    	
	$('#inst_list').change(function(){
		
		var id = $(this).val();
		$('.tutor_det').hide();
		if(id > 0)
		{
		 $.ajax({
	            url: '<?php echo $this->createUrl('institutions/getInstituteDetails') ?>',
	            type: 'POST',
		        data: {id : id},
	            success: function(data) {
		            
	            	var result = jQuery.parseJSON(data);
	            	$('#iname').html(result.name);
	            	$('#iadmin').html(result.admin);
	            	$('#icount').html(result.student_count);

	            	$.ajax({
	    	            url: '<?php echo $this->createUrl('institutions/getTutors')?>',
	    	            type: 'POST',
	    		        data: {id : id},
	    	            success: function(data) {
	    	            	
	    	            	$('#tutor_list').html(data);	    	            		    		           
	    	            },
	    			    error: function(data) {		
	    			        alert(data);
	    			    }
	    	         });
	            },
			    error: function(data) {		
			        alert(data);
			    }
	        });
		}
		else
		{
			$('#iname').html('');
        	$('#iadmin').html('');
        	$('#icount').html('');
        	$('#tutor_list').html('');
        	$('#tgname').html('');
        	$('#tname').html('');
        	$('#scount').html('');
		}
	});

	$('#tutor_list').change(function(){
		if($(this).val() > 0)
		{
			
		 $.ajax({
	            url: '<?php echo $this->createUrl('institutions/getTutorDetails')?>',
	            type: 'POST',
		        data: {id : $(this).val()},
	            success: function(data) {
		            
	            	var result = jQuery.parseJSON(data);
	            	$('#tgname').html(result.name);
	            	$('#tname').html(result.teacher);
	            	$('#scount').html(result.student_count);
	            	$('.tutor_det').show();
	            },
			    error: function(data) {		
			        alert(data);
			    }
	        });
		}
		else
			$('.tutor_det').hide();
	});
	
});

</script>

    <h1>Create Student</h1>
    <div class="Admin__content__inner">
      
             <?php 
        $mediamodel = new Media();
        
        $form = $this->beginWidget('CActiveForm', array(
                    	'id'=>'users-form',
                        'action'=>$this->createUrl('institutions/saveiStudent'),
                    	'enableAjaxValidation'=>false,
                        'htmlOptions' => array('enctype' => 'multipart/form-data')
                    ));         
  ?>
             <?php echo  $form->errorSummary(array($usermodel,$profilemodel,$addressmodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
                <div class="row">
                    <div class="col-sm-3 col-md-2">
                        <div class="form-group">
                            <label for="photo_id" class="control-label">Profile Photo</label>
                            <div class="photo-section">
                                 <?php
                             if ($profilemodel->photo_id > 0) {
                                    $mediamodel = Media::model()->findByPk($profilemodel->photo_id);
                                    $src = ((!empty($mediamodel->file_name) && $mediamodel->file_name != null) ? Yii::app()->baseUrl . "/images/media/" . $profilemodel->photo_id . "/" . $mediamodel->file_name : Yii::app()->baseUrl . "/images/profile.png");
                                } else {
                                    $src = Yii::app()->baseUrl . "/images/defaultprofile.jpg";
                                }
                            ?>
                                <img src="<?php echo $src ;?>" alt="" id="profile-pic" class="img-responsive">
                                 <input type="hidden" name="photo_id" id="photo_id" value="<?php echo $profilemodel->photo_id?>">
                                <button type="button" class="add-media btn btn-primary btn-xs" id="photo-btn">Add Logo</button>            
                                <button type="button" class="remove-media btn btn-primary btn-xs" id="remove-photo-btn" style="<?php  if ($profilemodel->photo_id < 1) { echo 'display:none'; } ?>"> Remove Logo</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-9 col-md-10">
                        <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Account Details</h3>
                                </div>
                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="forenames" class="control-label">Forenames</label>
                                                <?php echo $form->textField($usermodel,'forenames',array('class'=>'form-control')); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="surname" class="control-label">Surname</label>
                                                <?php echo $form->textField($usermodel,'surname',array('class'=>'form-control')); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="email" class="control-label">Email</label>
                                                <?php echo $form->textField($usermodel,'email',array('class'=>'form-control')); ?>
                                            </div>
                                        </div>
                                    </div><!-- .row -->

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="password" class="control-label">Password</label>
                                                <?php echo $usermodel->password = "";?>
                		               <?php echo $form->passwordField($usermodel,'password',array('class'=>'form-control')); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="password_confirmation" class="control-label">Confirm Password</label>
                                                <input class="form-control" placeholder="Confirm Password" type="password" value="" name="Users[confirmpassword]" id="confirmpassword">
                                            </div>
                                        </div>
                                    </div><!-- .row -->

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="slug" class="control-label">Link</label>
                                                <div class="input-group slug-field">
                                                    <span class="input-group-addon">http://www.cvvid.com/</span>
                                                    <?php echo $form->textField($profilemodel,'slug',array('class'=>'form-control','id'=>'slug')); ?>
                                                    <span class="input-group-addon help-block"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div><!-- .panel-body -->
                            </div><!-- .panel -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Institution Details</h3>
                            </div>
                            <div class="panel-body">

                                <div id="institution-tutor-form">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label class="control-label">Institution</label>
                                                <?php             			
                                                        $insmodel = Institutions::model()->findAllByAttributes(array('deleted_at'=>null));
                                                        $instlist = CHtml::listData($insmodel, 'id', 'name');

                                                    echo $form->labelEx($model,'institution');            
                                                    echo $form->dropDownList($model, 'id', $instlist,array('class'=>'form-control select2','id'=>'inst_list','empty'=>"- select Institution -"));
                                                ?>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label">Tutor Group</label>
                                                <?php             				    
                                                        $tutorgroups = TutorGroups::model()->findAllByAttributes(array('institution_id'=>$model->id));
                                                        $tutorgrouplist = CHtml::listData($tutorgroups, 'id', 'name');
                                                        echo CHtml::label('Tutor Group', '',array());
                                                        echo CHtml::dropDownList('tutor_group_id','',$tutorgrouplist,array('empty'=>'','class'=>'form-control select2','id'=>'tutor_list'));             				    
                                                    ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="institution-details">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Institution Name</label>
                                                            <p class="form-control-static" id="iname"><?php echo $model->name; ?></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Admin</label>
                                                            <p class="form-control-static" id="iadmin"></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Student Count</label>
                                                            <p class="form-control-static" id="icount"></p>
                                                        </div>
                                                    </div>
                                                </div></div>
                                            <div class="tutor-group-details">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Tutor Group Name</label>
                                                            <p class="form-control-static" id="tgname"></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Teacher</label>
                                                            <p class="form-control-static" id="tname"></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Student Count</label>
                                                            <p class="form-control-static" id="scount"></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                        <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Details</h3>
                                </div>
                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="dob" class="control-label">Date of Birth</label>
                                                <div class="input-group">
                                                    <input type="text" name="dob" class="dob form-control" value="<?php echo ((empty($usermodel->dob) || $usermodel->dob == null || $usermodel->dob == "0000-00-00") ? "" : date("d/m/Y", strtotime($usermodel->dob))); ?>">
                                                </div>
                                            </div>
                                        </div><!-- .col-sm-6 -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="tel" class="control-label">Telephone</label>
                                                <?php echo $form->textField($usermodel,'tel',array('class'=>'form-control')); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="mobile" class="control-label">Mobile</label>
                                                <?php echo $form->textField($usermodel,'mobile',array('class'=>'form-control')); ?>
                                            </div>
                                        </div><!-- .col-sm-6 -->
                                    </div><!-- .row -->

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="current_job" class="control-label">Current Job</label>
                                                <?php echo $form->textField($usermodel,'current_job',array('class'=>'form-control')); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- .panel-body -->
                            </div><!-- .panel -->

                        <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Address</h3>
                                </div>
                                <div class="panel-body">
                                      <div id="location-fields">
                                            <?php $this->renderPartial('//addresses/address-form', array('form'=>$form,'addressmodel'=>$addressmodel)); ?>
                                        </div>

                                </div>
                            </div>       
                        
                        <div class="form-actions clearfix">
                            <?php echo CHtml::submitButton('Save',array("class"=>"btn btn-primary pull-right")); ?>
                        </div>
                    </div>
                </div>        
           <?php $this->endWidget(); ?>
       
    </div>
<script type="text/javascript">
        $(function(){
            new CandidateSlugGenerator();
        });
    </script>