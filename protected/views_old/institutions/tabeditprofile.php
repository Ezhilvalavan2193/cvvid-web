<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */

$this->breadcrumbs=array(
    'Institutions'=>array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
    
");
?>
<?php
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$model->id,'type'=>$model->type));        
        $addressmodel = Addresses::model()->findByAttributes(array('model_id'=>$model->id));
        if($addressmodel == null)
            $addressmodel = Addresses::model(); 
?>
        <h1>Edit Student User</h1>
        <div class="Admin__content__inner">
            <div class="CandidateForm">
                <ul class="AdminTabs nav nav-tabs">
                    <li role="presentation">
                        <a href="<?php echo Yii::app()->createUrl("institutions/tabeditstudent",array("id"=>$tgsmodel->id)); ?>">Details</a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="<?php echo Yii::app()->createUrl("institutions/tabeditprofile",array("id"=>$tgsmodel->id)); ?>">Profile</a>
                    </li>
                    <li role="presentation">
                       <a href="<?php echo Yii::app()->createUrl("institutions/tabeditdocuments", array("id" => $tgsmodel->id)); ?>">Documents</a>
                    </li>
                    <li role="presentation">
                      <a href="<?php echo Yii::app()->createUrl("institutions/tabeditapplications", array("id" => $tgsmodel->id)); ?>">Applications</a>
                    </li>
                </ul>
               
                <div class="row">
                    <div class="col-sm-8">
                        <div class="UserExperiences">
                            <h3>Professional Experience</h3>
                            <?php 
                            $expmodel = UserExperiences::model()->findAllByAttributes(array('user_id' => $model->id));

                                $this->widget('zii.widgets.grid.CGridView', array(
                                    'id' => 'profile-exp-grid',
                                    'itemsCssClass' => 'table',
                                    'summaryText' => '',
                                    //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                                    'dataProvider' => ProfileExperiences::model()->searchByID($profilemodel->id),
                                    'columns' => array(
                                        array(
                                            'header' => 'Company',
                                            'value' => '$data->userExperience->company_name',
                                        ),
                                        array(
                                            'header' => 'Location',
                                            'value' => '$data->userExperience->location',
                                        ),
                                        array(
                                            'header' => 'Position',
                                            'value' => '$data->userExperience->position',
                                        ),
                                        array(
                                            'header' => 'Date',
                                            'value' => '$data->userExperience->start_date',
                                        ),
                                    ),
                                ));
                            ?>
                            
                        </div>
                        <div class="UserQualifications">
                            <h3>Qualifications</h3>
                            <?php 
                            $quamodel = UserQualifications::model()->findAllByAttributes(array('user_id' => $model->id));
                                $this->widget('zii.widgets.grid.CGridView', array(
                                    'id' => 'profile-qua-grid',
                                     'itemsCssClass' => 'table',
                                    'summaryText' => '',
                                   // 'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                                    'dataProvider' => ProfileQualifications::model()->searchByID($profilemodel->id),
                                    'columns' => array(
                                        array(
                                            'header' => 'Institutin',
                                            'value' => '$data->userQualification->institution',
                                        ),
                                        array(
                                            'header' => 'Location',
                                            'value' => '$data->userQualification->location',
                                        ),
                                        array(
                                            'header' => 'Field',
                                            'value' => '$data->userQualification->field',
                                        ),
                                        array(
                                            'header' => 'Level',
                                            'value' => '$data->userQualification->level',
                                        ),
                                        array(
                                            'header' => 'Completion Date',
                                            'value' => '$data->userQualification->completion_date',
                                        ),
                                        array(
                                            'header' => 'Grade',
                                            'value' => '$data->userQualification->grade',
                                        ),
                                    ),
                                ));
                            ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="UserHobbies">
                            <h3>Hobbies</h3>
                            <?php
                            $hobmodel = UserHobbies::model()->findAllByAttributes(array('user_id' => $model->id));
                                $this->widget('zii.widgets.grid.CGridView', array(
                                    'id' => 'profile-hob-grid',
                                     'itemsCssClass' => 'table',
                                    'summaryText' => '',
                                   // 'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                                    'dataProvider' => ProfileHobbies::model()->searchByID($profilemodel->id),
                                    'columns' => array(
                                        array(
                                            'header' => 'Activity',
                                            'value' => '$data->userHobby->activity',
                                        ),
                                        array(
                                            'header' => 'Description',
                                            'value' => '$data->userHobby->description',
                                        ),
                                    ),
                                ));
                            ?>
                        </div>               
                        <div class="UserLanguages">
                            <h3>Languages</h3>
                            <?php
                            $langmodel = UserLanguages::model()->findAllByAttributes(array('user_id' => $model->id));

                                $this->widget('zii.widgets.grid.CGridView', array(
                                    'id' => 'profile-lang-grid',
                                      'itemsCssClass' => 'table',
                                    'summaryText' => '',
                                    //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                                    'dataProvider' => ProfileLanguages::model()->searchByID($profilemodel->id),
                                    'columns' => array(
                                        array(
                                            'header' => 'Name',
                                            'value' => '$data->userLanguage->name',
                                        ),
                                        array(
                                            'header' => 'Proficiency',
                                            'value' => '$data->userLanguage->proficiency',
                                        ),
                                    ),
                                ));
                            ?>
                        </div>           
                    </div>
                </div>


            </div>
        </div>
    