<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
    'Candidates'=>array('index'),
    'Manage',
);
?>

<?php  
echo CHtml::button('Create',array('class'=>'btn btn-primary','onclick'=>"window.location='".Yii::app()->createUrl('institutions/createTeacher',array('id'=>$model->id))."'"));
    
    $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'teacher-grid',
	'summaryText'=>'',
	'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',
         'dataProvider'=>InstitutionUsers::model()->searchByInstitution($model->id),
        	'columns'=>array(
        	    
            			array(
            					'header'=>'Name',
            					'value'=>'CHtml::link($data->user->forenames." ".$data->user->surname,Yii::app()->createUrl("institutions/editTeacher", array("id"=>$data->id)),array("class"=>"teacher-edit-button","id"=>"teacher-edit"))',
            			        'type'=>'raw'
            			),
            			array(
            					'header'=>'Email',
            			        'value'=>'$data->user->email',
            					
            			),
                	    array(
                	        'header'=>'Created At',
                	        'value'=>'$data->user->created_at',
                	        
                	    ),
                	    
        	 ),
  ));
?>
 