<?php 
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Institutions'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
");
?> 
<script>
</script>
<div class="row">
<div class="col-sm-3 col-md-2">
  <?php 
        $meidatmodel = Media::model()->findByPk($profilemodel->id);
    
        $form=$this->beginWidget('CActiveForm', array(
                    	'id'=>'edit-stude-form',
                        'action'=>$this->createUrl('institutions/updateStudentDetails',array('id'=>$model->id)),
                    	'enableAjaxValidation'=>false,
                        'htmlOptions' => array('enctype' => 'multipart/form-data')
                    )); 
        
        
    ?>
                
    <div class="form-group">
                <div style="text-align: center;">Profile Photo</div>
              <div class="photo-section">
                <img src="<?php echo Yii::app()->baseUrl?>/images/media/<?php echo $profilemodel->photo_id?>/conversions/thumb.jpg" alt="" id="profile-pic" class="img-responsive">
            	
          		<input type="hidden" name="photo_id" id="photo_id" value="<?php echo $profilemodel->photo_id?>">
            	<button type="button" class="profile-btn add-media" id="photo-btn">Add Logo</button>            
                <button type="button" class="profile-btn" id="remove-photo-btn">Remove Logo</button>
            </div>
     </div>
</div>


<div class="col-sm-9 col-md-10">
       <div>
            
             <div class="form">
                <div class="row">
               		 <h3 class="summary" style="text-align:left;margin-bottom: 20px;">Account Details</h3>
                  
                <div class="formBlk col-xs-4">
                	<div class="formRow">
                		<?php echo $form->labelEx($model,'forenames'); ?>
                		<?php echo $form->textField($model,'forenames',array('class'=>'textBox')); ?>
                		<?php echo $form->error($model,'forenames'); ?>
                	</div>
                </div>
                <div class="formBlk col-xs-4">
                	<div class="formRow">
                		<?php echo $form->labelEx($model,'surname'); ?>
                		<?php echo $form->textField($model,'surname',array('class'=>'textBox')); ?>
                		<?php echo $form->error($model,'surname'); ?>
                	</div>
                </div>
                <div class="formBlk col-xs-4">
                	<div class="formRow">
                		<?php echo $form->labelEx($model,'email'); ?>
                		<?php echo $form->textField($model,'email',array('class'=>'textBox')); ?>
                		<?php echo $form->error($model,'email'); ?>
                	</div>
                </div>
                <div class="formBlk col-xs-4">
                	<div class="formRow">
                	    <?php echo $model->password = "";?>
                		<?php echo $form->labelEx($model,'password'); ?>
                		<?php echo $form->passwordField($model,'password',array('class'=>'textBox')); ?>
                		<?php echo $form->error($model,'password'); ?>
                	</div>
                </div>
                <div class="formBlk col-xs-4">
                	<div class="formRow">
                		<?php echo CHtml::label('Confirm Password','',array()); ?>
                		<?php echo CHtml::passwordField('confirmpassword','',array('class'=>'textBox')); ?>
                	</div>
                </div>
                </div>
                <div class="formBlk">
                	<div class="formRow">
                		<?php echo $form->labelEx($profilemodel,'slug'); ?>
                		<?php echo $form->textField($profilemodel,'slug',array('class'=>'textBox')); ?>
                		<?php echo $form->error($profilemodel,'slug'); ?>
                	</div>
                </div>
                
                <div class="row">
                <hr>                  
                 <h3 class="summary" style="text-align:left;margin-bottom: 20px;">Details</h3>
                 <div class="formBlk col-xs-4">
                	<div class="formRow">
                	    <?php echo $form->labelEx($model,'dob'); ?>
                		<?php 
        					$this->widget('zii.widgets.jui.CJuiDatePicker',array(
        						'name'=>'dob',
                               // 'model'=>$model,
        					    'value' => ((empty($model->dob) || $model->dob == null || $model->dob == "0000-00-00") ? "" : $model->dob),
        					    // additional javascript options for the date picker plugin
        					    'options'=>array(  
        					        'showAnim'=>'fold',
        							'showButtonPanel'=>true,
        							'showOn'=>'button',
                                   // 'buttonImage'=>Yii::app()->baseUrl.'/images/calendar.png',
        							'buttonImageOnly'=>true,
        							'dateFormat'=>'yyyy/mm/dd',
        						
        					    ),
        					    'htmlOptions'=>array(
        					        'class'=>'datepicker textBox',
        					       // 'style'=>'height:20px;'
        					    ),
        					));
        				?>
                	</div>
                </div>
                <div class="formBlk col-xs-4">
                	<div class="formRow">
                		<?php echo $form->labelEx($model,'tel'); ?>
                		<?php echo $form->textField($model,'tel',array('class'=>'textBox')); ?>
                		<?php echo $form->error($model,'tel'); ?>
                	</div>
                </div>
                 
                 <div class="formBlk col-xs-4">
                	<div class="formRow">
                		<?php echo $form->labelEx($model,'mobile'); ?>
                		<?php echo $form->textField($model,'mobile',array('class'=>'textBox')); ?>
                		<?php echo $form->error($model,'mobile'); ?>
                	</div>
                </div>
                
                  <div class="formBlk col-xs-4">
                	<div class="formRow">
                		<?php echo $form->labelEx($model,'current_job'); ?>
                		<?php echo $form->textField($model,'current_job',array('class'=>'textBox')); ?>
                		<?php echo $form->error($model,'current_job'); ?>
                	</div>
                </div>
                </div>
                <div class="row">
                <hr>
                <h3 class="summary" style="text-align:left;margin-bottom: 20px;">Address</h3>
                
                <div class="formBlk">
                	<div class="formRow">
                		<?php echo $form->labelEx($addressmodel,'address'); ?>
                		<?php echo $form->textArea($addressmodel,'address',array('class'=>'textArea')); ?>
                		<?php echo $form->error($addressmodel,'address'); ?>
                	</div>
                </div>
                
                <div class="formBlk col-xs-4">
                	<div class="formRow">
                		<?php echo $form->labelEx($addressmodel,'postcode'); ?>
                		<?php echo $form->textField($addressmodel,'postcode',array('class'=>'textBox')); ?>
                		<?php echo $form->error($addressmodel,'postcode'); ?>
                	</div>
                 </div>
                
                 <div class="formBlk col-xs-4">
                	<div class="formRow">
                		<?php echo $form->labelEx($addressmodel,'town'); ?>
                		<?php echo $form->textField($addressmodel,'town',array('class'=>'textBox')); ?>
                		<?php echo $form->error($addressmodel,'town'); ?>
                	</div>
                </div>
                 <div class="formBlk col-xs-4">
                	<div class="formRow">
                		<?php echo $form->labelEx($addressmodel,'county'); ?>
                		<?php echo $form->textField($addressmodel,'county',array('class'=>'textBox')); ?>
                		<?php echo $form->error($addressmodel,'county'); ?>
                	</div>
                </div>
                 <div class="formBlk col-xs-4">
                	<div class="formRow">
                		<?php echo $form->labelEx($addressmodel,'country'); ?>
                		<?php echo $form->textField($addressmodel,'country',array('class'=>'textBox')); ?>
                		<?php echo $form->error($addressmodel,'country'); ?>
                	</div>
                </div>
               </div> 
                	<div class="row buttons">
                		<?php echo CHtml::submitButton('Save',array("class"=>"btn btn-primary")); ?>
                	
                		<?php echo CHtml::submitButton('Delete',array("class"=>"btn btn-primary","style"=>"margin-left: 50px;background-color:red")); ?>
                	</div>
                <?php $this->endWidget(); ?>
                
                </div>
       </div>
       <div class="col-md-push-8 col-md-4">
        
       </div>
</div>
</div>