<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */
/* @var $form CActiveForm */
?>
<script>

$(document).ready(function(){

	var Password = {
			 
			  _pattern : /[a-zA-Z0-9_\-\+\.]/,
			  
			  
			  _getRandomByte : function()
			  {
			    // http://caniuse.com/#feat=getrandomvalues
			    if(window.crypto && window.crypto.getRandomValues) 
			    {
			      var result = new Uint8Array(1);
			      window.crypto.getRandomValues(result);
			      return result[0];
			    }
			    else if(window.msCrypto && window.msCrypto.getRandomValues) 
			    {
			      var result = new Uint8Array(1);
			      window.msCrypto.getRandomValues(result);
			      return result[0];
			    }
			    else
			    {
			      return Math.floor(Math.random() * 256);
			    }
			  },
			  
			  generate : function(length)
			  {
			    return Array.apply(null, {'length': length})
			      .map(function()
			      {
			        var result;
			        while(true) 
			        {
			          result = String.fromCharCode(this._getRandomByte());
			          if(this._pattern.test(result))
			          {
			            return result;
			          }
			        }        
			      }, this)
			      .join('');  
			  }    
			    
			};

	$(".students-upload").change(function() {

		if($('#student_cnt').val() > 0)
		{
		
		 var file = this.files[0];

		  var reader = new FileReader();
                    reader.onload = function (progressEvent) {
                        // Entire file


                        // By lines
                        var lines = this.result.split('\n');
                        var cnt = 1;
// 		    alert(lines.length);

                       // for (var line = 0; line < lines.length; line++) {
 						for (var line = 0; line <= $('#allocated_students').val(); line++) {
                            if (cnt > 1)
                            {
                                var row = lines[line];
                                if (row != "")
                                {
                                    var array = row.split(',');

                                    var studclone = $('.user-item').find('.row').clone();
                                    $(".user-list").append(studclone);

                                    var c = 0;
                                    $(studclone).find('input').each(function () {
                                        $(this).val(array[c]);
                                        c++;
                                    });
                                }

                            }
                            cnt++;
                        }
                    };
                    reader.readAsText(file);
                } else
                {
                    var $el = $('.students-upload');
                    $el.wrap('<form>').closest('form').get(0).reset();
                    $el.unwrap();
                }
        
            });

	
//	$("#tutor-group-form").submit(function(event){
//		//event.preventDefault();
//		if($('#password').val() != "" && $('#password').val() == $('#confirmpassword').val())
//			$("#tutor-group-form").submit();
//		else	
//		{
//			$('.pwdmsg').html('Password not matched');
//			return false;
//		}
//	});
	
	$('#inslist').change(function(){
		var id = $(this).val(); 
		if(id > 0)
		{
			 $.ajax({
		            url: '<?php echo $this->createUrl('institutions/getInsTeachers') ?>',
		            type: 'POST',
		            data: {id : id}, 	    
		            success: function(res) {
		            	$('#teacherslist').html(res);
		            	
		            	 $.ajax({
		 		            url: '<?php echo $this->createUrl('institutions/getNumPupils') ?>',
		 		            type: 'POST',
		 		            data: {id : id}, 	    
		 		            success: function(data) {
		 		            	$('#accounts_remain').html(data);
		 		            	$('#student_cnt').val(data);
		 		            },
		 				    error: function(data) {		
		 				        alert('err');
		 				    }
		 		         });
		 		         
		            },
				    error: function(data) {		
				        alert('err');
				    }
		         });
			
		}
		else
		{
			$('#student_cnt').val(0);
		}
	});	
	
// 	$('#teacherslist').change(function(){
// 		if($(this).val() > 0)
// 			$('.teacher-section').hide();
// 		else
// 			$('.teacher-section').show();	
// 	});	

	$("#add_student").click(function(){

		if($('#inslist').val() > 0)
		{
    		var cnt = $('#allocated_students').val() > 0 ? $('#allocated_students').val() : $("#student_cnt").val();
    		if($('.user-list').find('.row').length >= cnt)
    		{
    			alert('Allocated Students limit reached');    		
    		}
    		else
    		{			
        			var studclone = $('.user-item').find('.row').clone();	
            		$(".user-list").append(studclone);
    			
    		}
		}
	});

	$(".user-list").on('click','.remove_student',function(){
		$(this).closest('.row').remove();
	});


	$("#allocated_students").blur(function(){
		
		if(parseInt($(this).val()) > parseInt($("#student_cnt").val()))
		{
			$('.max-accounts-reached').removeClass('hidden');
			$(this).val($("#student_cnt").val());
			$('.user-limit').html($(this).val());
		}
		else
		{
			$('.max-accounts-reached').addClass('hidden');
			$('.user-limit').html($(this).val());
		}

		if( $(".user-list").find('.row').length > $(this).val() )
		{
			var cnt = $(".user-list").find('.row').length - $(this).val();
			$(".user-list").find('.row').slice(-cnt).remove();
		}
	});
	
	$(".generate-password").click(function(){

		$(".user-list").find('.row').each(function( index ) {
			if($.trim($(this).find('.col-sm-3').find('.form-group').find(".passwordbox").val()) == "")
				$(this).find('.col-sm-3').find('.form-group').find(".passwordbox").val(Password.generate(10));
		});
		
	});
});

</script>


    <h1><?php echo $usermodel->isNewRecord ? "Create" : "Update" ?> Tutor Group</h1>
    <div class="Admin__content__inner">
       <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'tutor-group-form',
            'action'=>$this->createUrl('institutions/saveTutorGroup'),
                'enableAjaxValidation'=>false
        )); ?>
         <?php echo  $form->errorSummary(array($usermodel,$tutormodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
            <div id="user-group-form">
                <!-- Teacher -->
                <div id="user-manager">

                    <div class="form-container">
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="form-group">
                                    <label for="institution" class="control-label">Institution</label>
                                     <?php 		              
                                        $insmodel = $model->findAllByAttributes(array('deleted_at'=>null));
                                        $inslist = CHtml::listData($insmodel, 'id', 'name');
                                        echo CHtml::dropDownList('institution_id','',$inslist,array('empty'=>'','class'=>'form-control select2','id'=>'inslist'));		
                                    ?>
                                </div>
                                <div class="form-group">
                                    <label for="teacher_id" class="control-label">Teacher</label>
                                    <?php 
//                                     $teachermodel = $usermodel->findAllByAttributes(array('deleted_at'=>null,'type'=>'teacher','status'=>'active'));
//                                     $teacherlist = CHtml::listData($teachermodel, 'id', 'forenames');
                                    //array_unshift($teacherlist, '- New Teacher -');
                                    //$teacherlist = array('0'=>'- New Teacher -') + $teacherlist;

                                    echo $form->dropDownList($tutormodel,'teacher_id',array(),array('class'=>'form-control select2','multiple'=>true,'id'=>'teacherslist')); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-container teacher-section" style="display: none">
                        <div class="user-manager-form">
                            <div class="form-container">
                                <!-- Your details -->
                                <h4>Teacher details</h4>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?php echo $form->textField($usermodel,'forenames',array('class'=>'form-control','placeholder'=>'First Name')); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?php echo $form->textField($usermodel,'surname',array('class'=>'form-control','placeholder'=>'Surname')); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-container">
                                <h4>Account Details</h4>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <?php echo $form->textField($usermodel,'email',array('class'=>'form-control','placeholder'=>'Email')); ?>
                                    </div>
                                    <div class="col-sm-3">
                                        <?php $usermodel->password = ""; ?>
			                <?php echo $form->textField($usermodel,'password',array('class'=>'form-control','id'=>'password','placeholder'=>'Password')); ?>
                                    </div>
                                    <div class="col-sm-3">
                                        <?php echo CHtml::textField('Confirm Password','',array('class'=>'form-control','id'=>'confirmpassword','placeholder'=>'Confirm Password')); ?>
			                <div class="errorMessage pwdmsg"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <!-- Tutor Group -->
                <div class="form-container">
                    <h4>Tutor Group Details</h4>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="name" class="control-label">Tutor Group</label>
                                <?php echo $form->textField($tutormodel,'name',array('class'=>'form-control','placeholder'=>'Name')); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="graduation_date" class="control-label">Graduation Date</label>
                                <div class="input-group">
                                    <input type="text" name="graduation_date" class="dob form-control" value="<?php echo ((empty($tutormodel->graduation_date) || $tutormodel->graduation_date == null || $tutormodel->graduation_date == "0000-00-00") ? "" : $tutormodel->graduation_date); ?>">
                 
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="location" class="control-label">Campus</label>
                                <?php echo $form->textField($tutormodel,'location',array('class'=>'form-control','placeholder'=>'Campus')); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-container">
                        <h4>Allocated Students</h4>
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <?php 
                                       echo CHtml::textField('allocated_students','',array('id'=>'allocated_students','class'=>"form-control"))." ( ".CHtml::label(0, '',array('id'=>'accounts_remain')).' accounts remaining )';
                                ?>
                                    <?php echo CHtml::hiddenField('student_cnt',0,array('id'=>'student_cnt')); ?>
                                </div>
                                <p class="form-control-static hidden max-accounts-reached">You have reached the maximum students available.</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-container">
                    <h4>Students</h4>
                    <div id="user-upload">
                        <h5>File Upload</h5>
                        <div class="user-file-upload form-inline">
                            <div class="form-group">
                                <input type="file" name="students" class="students-upload">
                            </div>
                            <a href="<?php echo $this->createUrl('institutions/sampleDownload'); ?>" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download a sample upload file">
                                <i class="fa fa-info"></i>
                            </a>
                        </div>
                        <h5>
                            Manual Upload
                            <span class="user-limit-count pull-right">
                                <span class="user-count">0</span> / <span class="user-limit">0</span>
                            </span>
                        </h5>
                        <div class="user-list"></div>
                        <div class="user-item" style="display: none">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?php echo CHtml::textField('forenames[]','',array('placeholder'=>'Forename','class'=>'form-control')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?php echo CHtml::textField('surname[]','',array('placeholder'=>'Surname','class'=>'form-control')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?php echo CHtml::textField('email[]','',array('placeholder'=>'Email Address','class'=>'form-control emailbox')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?php echo CHtml::textField('password[]','',array('placeholder'=>'Password','class'=>'form-control passwordbox')); ?>
                                        <button type="button" class="remove_student delete-user btn btn-danger"><i class="fa fa-close"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="user-actions clearfix">
                            <button type="button" id="add_student" class="btn btn-default add-new pull-left">Add Student</button>
                            <button type="button" class="btn btn-default generate-password pull-right">Generate Passwords</button>
                        </div>
                    </div>
                </div>

                <div class="form-actions">
                    <?php echo CHtml::submitButton('Save',array('class'=>'btn btn-default')); ?>
		   			<?php echo CHtml::button('Cancel',array('class'=>'btn btn-default','onClick'=>'window.location.href="'.$this->createUrl('institutions/tutorgroups').'"')); ?>
                </div>
            </div>

      <?php $this->endWidget(); ?>
    </div>

