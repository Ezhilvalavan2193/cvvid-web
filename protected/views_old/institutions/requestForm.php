<script>

$(document).ready(function () {
    $('#payment_option').change(function()
    {
    	if($(this).val() == "free")
    	{
    		$("#amount_sec").hide();
    		$("#additional_payment_sec").hide();
    		$("#total_payment_sec").hide();
    	}
    	if($(this).val() == "fixed")
    	{
    		$("#amount_sec").show();
    		$("#additional_payment_sec").hide();
    		$("#total_payment_sec").hide();
    	}
    	if($(this).val() == "specific")
    	{
    		$("#amount_sec").show();
    		$("#additional_payment_sec").show();
    		$("#total_payment_sec").show();
    	}
    });

    $('#amount').blur(function()
    {
        var amt =  parseInt($(this).val() > 0 ? $(this).val() : 0);
        var addamt = parseInt($("#additional_payment").val() > 0 ? $("#additional_payment").val() : 0);
    	$("#total_payment").val(amt + addamt);
    });
    $('#additional_payment').blur(function()
    {
    	var addamt =  parseInt($(this).val() > 0 ? $(this).val() : 0);
        var amt = parseInt($("#amount").val() > 0 ? $("#amount").val() : 0);
    	$("#total_payment").val(amt + addamt);
    });
//     $('#total_payment').blur(function()
//     {
//     	var addamt =  parseInt($("#additional_payment").val() > 0 ? $("#additional_payment").val() : 0);
//         var amt = parseInt($("#amount").val() > 0 ? $("#amount").val() : 0);
//     	$("#total_payment").val(amt + addamt);
//     });

    $("#upgrade-request-form").on("submit", function (event) {

		if($("#payment_option").val() == "fixed" && $("#amount").val() <= 0)
		{
			alert("Please enter amount.");
			return false;
		}
		else if($("#payment_option").val() == "specific")
		{
			if($("#amount").val() <= 0)
			{
				alert("Please enter amount.");
				return false;
			}
			if($("#additional_payment").val() <= 0)
			{
				alert("Please enter Additional amount.");
				return false;
			}
		}
		else
			return true;
    });
});

</script>
<div class="page-title text-left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>   
                    Request Upgrade
                </h1>
            </div>
        </div>
    </div>
</div>
<div id="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <div class="payment-form-container">
                <?php if (Yii::app()->user->hasFlash('success')): ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?php echo Yii::app()->user->getFlash('success'); ?>
                    </div>
            	<?php endif; ?>
                    <div class="payment-inner">
                        <form method="POST" id="upgrade-request-form" action="<?php echo $this->createUrl('institutions/requestUpgrade') ?>" accept-charset="UTF-8" class="payment-form">
                            <div class="row">
                                <div class="col-md-offset-1 col-md-10">
                                    <div class="payment-details-container">

                                        <div class="row">
                                            <div class="col-md-offset-2 col-md-8">

                                                <span class="payment-errors"></span>

                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                                <input type="number" required="required" name="number_students" class="form-control" id="number_students"
                                                                       placeholder="Number of Students" />
                                                               
                                                            </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <select name="payment_option" id="payment_option" class="select2 form-control">
                                                         		<option value="free"> Free </option>  
                                                         		<option value="fixed"> Fixed Payment</option>
                                                         		<option value="specific"> Specific Payment</option> 
                                                            </select>
                                                         </div>
                                                    </div>
                                                </div><!-- .row -->
												<div class="row" style="display: none;" id="amount_sec">                                                    
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                                <input type="number" name="amount" id="amount" class="form-control" 
                                                                       placeholder="Enter Amount" />                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                
                                                <div class="row" style="display: none;" id="additional_payment_sec">                                                    
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                                <input type="number"  id="additional_payment" name="additional_amount" class="form-control" 
                                                                       placeholder="Additional Payment" />                                                                
                                                            </div>
                                                        </div>
                                                </div>
												<div class="row" style="display: none;" name="total_payment" id="total_payment_sec">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                                <input type="number" readonly="readonly" name="total_amount" class="form-control" id="total_payment"
                                                                       placeholder="Total Payment" />                                                               
                                                            </div>
                                                    </div>
                                                </div><!-- .row -->
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button type="submit" name="submit" class="submit-button btn btn-block main-btn">Submit Payment</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div><!-- .row -->

                                    </div><!-- .payment-details-container -->
                                </div>
                            </div><!-- .row -->
                          
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
