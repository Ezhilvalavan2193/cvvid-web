<?php
/* @var $this SiteController */
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
	 CVPayment.init('<?php echo Yii::app()->params['PUBLIC_Key']; ?>','activation');
	$('#card_pay').click(function(){
		$('.card_details').show();		
		$('.other_details').hide();	
	});
	
	$('#other_pay').click(function(){
		$('.card_details').hide();		
		$('.other_details').show();	
	});
	
	
	
});
</script>

<?php

use Stripe\Stripe;
use Stripe\Invoice;

require Yii::app()->basePath . '/extensions/stripe/init.php';
Stripe::setApiKey(Yii::app()->params['SECRET_Key']);

$amount = 0;
if($model->payment_type != "fixed")
    $amount = (int)$model->amount;
else
    $amount = (int)$model->amount + (int)$model->additional_amount;
?>
<div class="site-content">
    <div class="page-title text-left">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1>   
                        Activate your Account
                    </h1>
                </div>
            </div>
        </div>
    </div>

    <div id="page-content">
        <div class="container">
           <div class="row">
                
           
                <div class="col-md-offset-1 col-md-12">
                    <div>
                
                        <div class="row">
                            <div class="col-md-offset-2 col-md-4">
                                <span class="payment-errors"></span>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="selected-subscription">
                                            <div class="selected-subscription-price">&#163;<?php echo $amount?></div>
                                        </div>
                                    </div>
                                </div>
                                 <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                        
                                        	<input type="radio" name="payment_method" id="card_pay" checked>Card Payment
                                        	<input type="radio" name="payment_method" id="other_pay">Other
                                        
                                        </div>
                                       </div>
                                    </div>
                          <div class="card_details">
                               <?php
                                    $form = $this->beginWidget('CActiveForm', array(
                                        'id' => 'activate-form',
                                        'action' => $this->createUrl('institutions/activateInstitution',array('id'=>$model->id)),
                                        'enableAjaxValidation' => false,
                                        'htmlOptions' => array('enctype' => 'multipart/form-data', 'class' => 'payment-form')
                                    ));
                                   ?>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="input-view">
                                            	<input type="hidden" name="amount" value="<?php echo $amount?>">
                                                <input type="tel" size="20" class="form-control cc-name"
                                                       placeholder="Cardholder Name" autocomplete="off"/>
                                                <span class="card-icon"></span>
                                                <div class="view-icon">
                                                    <i class="fa fa-pencil-square-o"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- .row -->

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <div class="input-view">
                                                <input type="tel" size="20" class="form-control cc-number" data-stripe="number"
                                                       placeholder="Card Number" autocomplete="off"/>
                                                <span class="card-icon"></span>
                                                <div class="view-icon">
                                                    <i class="fa fa-credit-card"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- .row -->

                                <div class="row">
                                    <div class="col-xs-7">  
                                      
                                        <div class="form-group form-expiration">
                                            <span>Expiry</span>
                                            <input id="cc-exp" type="tel" size="10" class="form-control cc-exp" name="card_expiry" autocomplete="cc-exp"
                                                   placeholder="•• / ••" required>
                                        </div>
                                    </div>
                                    <div class="col-xs-5">
                                        <div class="form-group">
                                            <div class="input-view cvc-input">
                                                <span>CVC</span>
                                                <input type="tel" size="4" class="form-control cc-cvc" data-stripe="cvc"
                                                       autocomplete="off"/>
                                    <!--                                                                 <div class="view-icon"> -->
                                    <!--                                                                     <i class="fa fa-lock"></i> -->
                                    <!--                                                                 </div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <button type="submit" class="submit-button btn btn-block main-btn">Submit Payment</button>
                                    </div>
                                </div>
                                 <?php $this->endWidget(); ?> 
								</div> 
								
								<div class="other_details" style="display: none;" >
								     <?php
                                        $form = $this->beginWidget('CActiveForm', array(
                                            'id' => 'activate-form',
                                            'action' => $this->createUrl('institutions/activateInstitution',array('id'=>$model->id)),
                                            'enableAjaxValidation' => false,
                                            'htmlOptions' => array('enctype' => 'multipart/form-data')
                                        ));
                                       ?>
								    <div class="row">
								     <div class="col-sm-12">
                                        <div class="form-group">
                                       
									   <p>On other payments you can send your amount via bank transfer/cheque </p>
									   </div>
									   </div>
									</div>
									<div class="row">
                                        <div class="col-md-12">
                                            <button type="submit" class="btn btn-block main-btn">Submit Payment</button>
                                        </div>
                                	</div>
                                 <?php $this->endWidget(); ?> 
								</div>
                            </div>
                        </div><!-- .row -->
						
                    </div><!-- .payment-details-container -->
                </div>
            </div><!-- .row -->
        </div>
    </div>
</div>
    <script type="text/javascript">
       
      
    </script>
