<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */

$this->breadcrumbs = array(
    'Institutions' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
		
");
?> 
<script>
    $(document).ready(function () {

        $('.form-section').on('click', '.edit-background', function () {
            $('.section-content').slideToggle('slow');
            $('.section-form').slideToggle('slow');
            return false;
        });

        $('.form-section').on('submit', '#ins-bground-form', function (e) {
            e.preventDefault();
            var formdata = new FormData(this);

            $.ajax({
                url: '<?php echo $this->createUrl('institutions/saveInsBackground', array('id' => $model->id)) ?>',
                type: 'POST',
                data: formdata,
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    $('#profile-body').html(data);
                },
                error: function (data) {
                    //alert(data);
                }
            });

            return false;
        });


        $('.ProfileEdit__operations').on('click', '.visiblity', function (e) {
            e.preventDefault();
            var status = $.trim($(this).text());

            $.ajax({
                url: '<?php echo $this->createUrl('institutions/changeVisiblity', array('id' => $model->id)) ?>',
                type: 'POST',
                async: false,
                data: {status: status},
                success: function (data) {

                    $(".ProfileEdit__operations").load(location.href + " .ProfileEdit__operations>*", "");

                },
                error: function (data) {
                    alert('err');
                }
            });
            return false;
        });

        $('.ProfileEdit__operations').on('click', '.status', function (e) {
           
            e.preventDefault();
            var status = $.trim($(this).text());

            $.ajax({
                url: '<?php echo $this->createUrl('institutions/changeStatus', array('id' => $model->id)) ?>',
                type: 'POST',
                async: false,
                data: {status: status},
                success: function (data) {
                    $(".ProfileEdit__operations").load(location.href + " .ProfileEdit__operations>*", "");

                },
                error: function (data) {
                    alert('err');
                }
            });
            return false;
        });
        
         $('body').on('click', '.open-media', function () {
            var getValue = $(this).attr("data-field");
            if(getValue == 'cover'){ $(".saveMedia").attr('id', 'save-media-cover'); }else if(getValue == 'photo'){ $(".saveMedia").attr('id', 'save-media-photo'); }
            
            //alert(getValue);return false;
            $('#mediamanager_modal').addClass('in');
            $('body').addClass('modal-open');
            $('#mediamanager_modal').show();
        });
        
       
        $('.upload-media').on('click', '.medaiamanager_add', function () {
           //e.preventDefault();
           $(".media-file").trigger('click');
            //return false;
        });

        $('.modal-content').on('change', '.media-file', function (e) {

            var fileExtension = ['jpeg', 'jpg', 'png'];
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                alert("Only '.jpeg','.jpg','.png' formats are allowed.");
            } else {
                 $('.MediaManager__loading').removeClass('hidden');
                var data = new FormData();
                jQuery.each(jQuery(this)[0].files, function (i, file) {
                    data.append('media', file);
                });
                data.append('collection_name', 'photos');
                data.append('model_type', "App\\Education\\Institution");
                data.append('model_id', <?php echo $model->id; ?>);
                // alert(JSON.stringify(data));
                $.ajax({
                    url: '<?php echo $this->createUrl('media/insertMediaManager') ?>',
                    type: 'POST',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
//                    alert(data);form-cover-photo
                        $(".MediaManager__content").load(location.href + " .MediaManager__content>*", "");
                        $('.MediaManager__loading').addClass('hidden');
                    },
                    error: function (data) {
                        alert('err');
                    }
                });
            }

            // return false;
        });
        
           //  media_item_delete  
        $('body').on('click', '.MediaManager__item__delete', function (e) {

            if (confirm('Are you sure you want to delete this item?')) {

                var id = $(this).attr('id');

                $.ajax({
                    url: '<?php echo $this->createUrl('media/deleteMedia'); ?>',
                    type: 'POST',
                    data: {id: id},
                    success: function (data) {
                        //alert(data);
                         $(".MediaManager__content").load(location.href + " .MediaManager__content>*", "");
                       // $("#result_para").load(location.href + " #result_para>*", "");
                    },
                    error: function (data) {
                        alert('err');
                    }
                });
            }

            return false;

        });

        $('.modal-content').on('click', '.MediaManager__item', function (evt) {
            $('div.selected').removeClass('selected');
            $(this).addClass('selected');
        });

        $('.modal-content').on('click', '#save-media-cover', function (evt) {

            var mediaid = $(".MediaManager__content").find(".selected").attr('id');

            $.ajax({
                url: '<?php echo $this->createUrl('profiles/updateCover') ?>',
                type: 'POST',
                data: {mediaid: mediaid, profileid: <?php echo $profilemodel->id; ?>},
                success: function (data) {
                    window.location.reload();
                    // $("#colcover").load(location.href + " #colcover>*", "");
                    //  closepopup();
                },
                error: function (data) {
                    alert('err');
                }
            });


        });
        
        
        $('.modal-content').on('click', '#save-media-photo', function (evt) {

            var mediaid = $(".MediaManager__content").find(".selected").attr('id');

            $.ajax({
                url: '<?php echo $this->createUrl('profiles/updatePhoto') ?>',
                type: 'POST',
                data: {mediaid: mediaid, profileid: <?php echo $profilemodel->id; ?>},
                success: function (data) {
                    window.location.reload();
                    // $("#colcover").load(location.href + " #colcover>*", "");
                    //  closepopup();
                },
                error: function (data) {
                    alert('err');
                }
            });


        });

        
    });

</script>
<div id="profile">
    <?php
    $employerid = isset($this->employerid) ? $this->employerid : 0;
    $userid = isset($this->userid) ? $this->userid : 0;
    $institutionid = isset($this->institutionid) ? $this->institutionid : 0;
    $edit = isset($this->edit) ? $this->edit : 0;
    $this->renderPartial('//profiles/Cover', array('userid' => $userid, 'employerid' => $employerid, 'institutionid' => $institutionid, 'edit' => $edit));
    ?>
    <div id="form-details">
        <div class="container">
            <div class="form-details-inner">
                <div class="form-user-details form-section">
                     <?php if(Yii::app()->user->hasFlash('success')):?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                       <?php echo Yii::app()->user->getFlash('success'); ?>
                    </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col-sm-4 col-md-3 col-lg-2 text-center">
                            <div class="ProfileEdit__photo">
                                <?php
                                $trail = "";
                                if ($model->trial_ends_at != null) {
                                    $trail = date('Y-m-d', strtotime($model->trial_ends_at));
                                    $expiry = date('d/m/Y', strtotime($model->trial_ends_at));
                                }
                                
                                if ($profilemodel->photo_id > 0) {
                                    $mediamodel = Media::model()->findByPk($profilemodel->photo_id);
                                    $src = ((!empty($mediamodel->file_name) && $mediamodel->file_name != null) ? Yii::app()->baseUrl . "/images/media/" . $profilemodel->photo_id . "/" . $mediamodel->file_name : Yii::app()->baseUrl . "/images/profile.png");
                                } else {
                                    $src = Yii::app()->baseUrl . "/images/defaultprofile.jpg";
                                }
                                echo CHtml::image($src, "", array("alt" => "", 'id' => 'profile-image'));
                                ?>
                                <a class="open-media profile-photo-btn" data-field="photo" data-target="#profile-image">
                                    <i class="fa fa-camera"></i>
                                    Change Photo
                                </a>
                            </div>  
                            <a class="btn btn-primary" href="<?php echo $this->createUrl('institutions/profile', array('id' => $model->id)) ?>">
                                <i class="fa fa-search"></i>
                                <span>View Profile</span>
                            </a>
                        </div>
                        <div class="col-sm-5 col-md-5 col-lg-6">
                            <div class="ProfileEdit__details">
                                <h3><?php echo $model->name ?></h3>
                                <?php 
                                $cri = new CDbCriteria();
                                $cri->condition = "model_id =".$model->id." and model_type like '%institutions%' and deleted_at is null";
                                $address = Addresses::model()->find($cri);
                                   // $address = Addresses::model()->findByAttributes(array('model_id'=>$model->id,'model_type'=>'Institutions'));
                                ?>
                                <!-- <div>Address 1: <?php echo $model->address_1 ?></div> -->
                                <div>Address: <?php echo ($address != null ? $address->address : $model->address_1); ?></div>
                                <div>Town: <?php echo ($address != null ? $address->town : ""); ?></div>
                                 <?php if (Yii::app()->user->getState('username'))  ?>
                                <div>Contact: <?php echo Yii::app()->user->getState('username'); ?></div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-4 col-lg-4">
                            <div class="ProfileEdit__operations">
                                <div>
                                    
                                   <?php if($profilemodel->published){?>
                                    <a class="Profile__active btn status btn-success" id="<?php echo $profilemodel->published ?>" href="#"><i class="fa fa-check"></i>Active</a>
                                   <?php } else { ?>
                                    <a class="Profile__active btn status btn-default" id="<?php echo $profilemodel->published ?>" href="#"><i class="fa fa-close"></i>Inactive</a>
                                   <?php } ?>

                                    <?php if($profilemodel->visibility){ ?>
                                    <a class="Profile__visibility visiblity btn btn-success" href=""><i class="fa fa-unlock"></i>Public</a>
                                    <?php } else { ?>
                                    <a class="Profile__visibility visiblity btn btn-default" href=""><i class="fa fa-lock"></i>Private</a>
                                    <?php } ?>
                                </div>
                                <div>
                                    <?php
//                                     if ($model->stripe_active)
//                                         $url = $this->createUrl('institutions/mysubscription');
//                                     else
                                        //$url = $this->createUrl('institutions/upgrade');
                                        
                                    if ($model->stripe_active > 0 || $model->payment_type == "free") {
                                    ?>
                                         <a class="btn btn-default" href="<?php echo $this->createUrl('institutions/mysubscription') ?>">
                                            <i class="fa fa-credit-card"></i>
                                            <span>Upgrade</span>
                                         </a>
                                    
                                    <?php }
                                        else { ?>
                                    <a class="btn btn-default" href="<?php echo $this->createUrl('institutions/upgrade') ?>">
                                        <i class="fa fa-credit-card"></i>
                                        <span>Account</span>
                                    </a>
                                    <?php } ?>
                                    <a class="btn default-btn upgrade" href="#">
                                        <i class="fa fa-envelope"></i>
                                        <span>Inbox</span>
                                    </a>
                                </div>

                                <div>
                                    <a class="btn default-btn" href="<?php echo $this->createUrl('institutions/userAccountInfo', array('id' => $model->id)) ?>">
                                        <i class="fa fa-pencil"></i>
                                        <span>Edit</span>
                                    </a>
                                    <a class="btn default-btn upgrade" href="<?php echo (Yii::app()->user->getState('role') == "admin" ? "#" : $this->createUrl('institutions/insTeachers', array('id' => $model->id) )) ?>">
                                        <i class="fa fa-group"></i>
                                        <span>Users</span>
                                    </a>
                                </div>
                                
                            </div>       
                        </div>
                    </div>
                </div>
                <div id="profile-body" class="form-section">
                    <?php $this->renderPartial('instbackground', array('model' => $model)); ?>
                </div>    
            </div>
        </div><!-- /.container -->
    </div>
</div>


<!--cover photo popup-->

<div class="modal fade" id="mediamanager_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
             <div class="MediaManager__loading hidden">
                <img src="<?php echo $this->createAbsoluteUrl('//'); ?>/images/loading.gif">
            </div>
            <div class="MediaManager__header">
                <form action="" method="POST" enctype="multipart/form-data" class="add-new-form">
                    <div class="row">
                        <div class="col-xs-7">
                            <h4>Media Library</h4>
                        </div>
                        <div class="col-xs-5">
                            <div class="upload-media pull-right">
                                <label class="btn main-btn medaiamanager_add"><i class="fa fa-plus"></i>Upload a File</label>
                                <input class="media-file" type="file" style="display:none;"/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            
            <div class="MediaManager__content">
                <?php
            $mediamodel = Media::model()->findAll(array(
                'condition'=>'model_id=:model_id AND model_type=:model_type AND collection_name=:collection_name',
                'params'=>array(':model_id'=>$model->id, ':model_type'=>'App\Education\Institution', ':collection_name'=>'photos'),
            ));
            if(count($mediamodel) > 0){
            ?>
            <div class="row">
                <?php foreach ($mediamodel as $media) { ?>
                    <div class="col-xs-6 col-sm-3">
                        <div class="MediaManager__item MediaManager__item--image" id="<?php echo $media['id'] ?>">
                            <img src="<?php echo Yii::app()->baseUrl ?>/images/media/<?php echo $media['id'] ?>/conversions/search.jpg" class="img-responsive">
                            <a class="MediaManager__item__delete" href="#" id="<?php echo $media['id'] ?>"><i class="fa fa-times"></i></a>
                        </div>
                    </div>
                <?php } ?>
               </div>
            <?php } ?>
                <h3>CV Vid Image Library</h3>
                <?php
                $mediamodel1 = Media::model()->findAll(array(
                    'condition'=>'model_id=:model_id AND collection_name=:collection_name',
                    'params'=>array(':model_id'=>1, ':collection_name'=>'sample'),
                ));
                if(count($mediamodel1) > 0){
                ?>
                <div class="row">
                    <?php foreach ($mediamodel1 as $media) { ?>
                        <div class="col-xs-6 col-sm-3">
                            <div class="MediaManager__item MediaManager__item--image" id="<?php echo $media['id'] ?>">
                                <img src="<?php echo Yii::app()->baseUrl ?>/images/media/<?php echo $media['id'] ?>/conversions/search.jpg" class="img-responsive">
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php } ?>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default-btn" data-dismiss="modal" onclick="closepopup()">Close</button>
                <button type="button" class="btn main-btn select-submit saveMedia">Save</button>
            </div>
            
        </div>
    </div>
</div>