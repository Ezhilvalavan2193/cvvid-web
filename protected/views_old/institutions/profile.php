<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */

$this->breadcrumbs = array(
    'Institutions' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});

");
?> 
<div id="profile">
     <?php
    $employerid = isset($this->employerid) ? $this->employerid : 0;
    $userid = isset($this->userid) ? $this->userid : 0;
    $institutionid = isset($this->institutionid) ? $this->institutionid : 0;
    $edit = isset($this->edit) ? $this->edit : 0;
    $this->renderPartial('//profiles/Cover', array('userid' => $userid, 'employerid' => $employerid, 'institutionid' => $institutionid, 'edit' => $edit));
    ?>
    <div id="form-details">
        <div class="container">
            <div class="form-details-inner">
                <div class="form-user-details form-section">
                    <div class="row">
                        <div class="col-sm-4 col-md-3 col-lg-2 text-center">
                            <div class="ProfileEdit__photo">
                                <?php
                                if ($profilemodel->photo_id > 0) {
                                    $mediamodel = Media::model()->findByPk($profilemodel->photo_id);
                                    $src = ((!empty($mediamodel->file_name) && $mediamodel->file_name != null) ? Yii::app()->baseUrl . "/images/media/" . $profilemodel->photo_id . "/" . $mediamodel->file_name : Yii::app()->baseUrl . "/images/profile.png");
                                } else {
                                    $src = Yii::app()->baseUrl . "/images/defaultprofile.jpg";
                                }
                                echo CHtml::image($src, "", array("id" => "profile-image","alt"=>""));
                                ?>
                            </div>                                       
                            <a class="btn btn-primary" href="<?php echo $this->createUrl('institutions/edit', array('id' => $model->id)) ?>">
                                <i class="fa fa-pencil"></i>
                                <span>Edit Profile</span>
                            </a>
                        </div>
                        <div class="col-sm-5 col-md-6 col-lg-7">
                            <div class="ProfileEdit__details">
                                <h3><?php echo $model->name ?></h3>
                                <div>Address 1: <?php echo $model->address_1 ?></div>
                                <div>Address 2: <?php echo $model->address_2 ?></div>
                                <div>Town: <?php echo $model->town ?></div>
                                <?php if(Yii::app()->user->getState('username'))?>
                                <div>Contact: <?php echo Yii::app()->user->getState('username'); ?></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-section">
                    <div class="form-section-inner">
                        <div class="section-header">
                            <h4>Information</h4>
                        </div>
                        <div class="section-content">
                            <div class="EmployerProfile__body"><?php echo $model->body; ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container -->
    </div>
</div>



