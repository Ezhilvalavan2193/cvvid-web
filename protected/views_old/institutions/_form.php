<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'institutions-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'admin_id'); ?>
		<?php echo $form->textField($model,'admin_id',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'admin_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'body'); ?>
		<?php echo $form->textArea($model,'body',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'body'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address_1'); ?>
		<?php echo $form->textField($model,'address_1',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'address_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address_2'); ?>
		<?php echo $form->textField($model,'address_2',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'address_2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'town'); ?>
		<?php echo $form->textField($model,'town',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'town'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'post_code'); ?>
		<?php echo $form->textField($model,'post_code',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'post_code'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'num_pupils'); ?>
		<?php echo $form->textField($model,'num_pupils'); ?>
		<?php echo $form->error($model,'num_pupils'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stripe_active'); ?>
		<?php echo $form->textField($model,'stripe_active'); ?>
		<?php echo $form->error($model,'stripe_active'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stripe_id'); ?>
		<?php echo $form->textField($model,'stripe_id',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'stripe_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stripe_subscription'); ?>
		<?php echo $form->textField($model,'stripe_subscription',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'stripe_subscription'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stripe_plan'); ?>
		<?php echo $form->textField($model,'stripe_plan',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'stripe_plan'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'last_four'); ?>
		<?php echo $form->textField($model,'last_four',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'last_four'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'card_expiry'); ?>
		<?php echo $form->textField($model,'card_expiry'); ?>
		<?php echo $form->error($model,'card_expiry'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'card_expiry_sent'); ?>
		<?php echo $form->textField($model,'card_expiry_sent'); ?>
		<?php echo $form->error($model,'card_expiry_sent'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'trial_ends_at'); ?>
		<?php echo $form->textField($model,'trial_ends_at'); ?>
		<?php echo $form->error($model,'trial_ends_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'subscription_ends_at'); ?>
		<?php echo $form->textField($model,'subscription_ends_at'); ?>
		<?php echo $form->error($model,'subscription_ends_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_at'); ?>
		<?php echo $form->textField($model,'created_at'); ?>
		<?php echo $form->error($model,'created_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_at'); ?>
		<?php echo $form->textField($model,'updated_at'); ?>
		<?php echo $form->error($model,'updated_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'deleted_at'); ?>
		<?php echo $form->textField($model,'deleted_at'); ?>
		<?php echo $form->error($model,'deleted_at'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->