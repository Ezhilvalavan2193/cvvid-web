<?php
/* @var $this InstitutionsController */
/* @var $data Institutions */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('admin_id')); ?>:</b>
	<?php echo CHtml::encode($data->admin_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('body')); ?>:</b>
	<?php echo CHtml::encode($data->body); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address_1')); ?>:</b>
	<?php echo CHtml::encode($data->address_1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address_2')); ?>:</b>
	<?php echo CHtml::encode($data->address_2); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('town')); ?>:</b>
	<?php echo CHtml::encode($data->town); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('post_code')); ?>:</b>
	<?php echo CHtml::encode($data->post_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('num_pupils')); ?>:</b>
	<?php echo CHtml::encode($data->num_pupils); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stripe_active')); ?>:</b>
	<?php echo CHtml::encode($data->stripe_active); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stripe_id')); ?>:</b>
	<?php echo CHtml::encode($data->stripe_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stripe_subscription')); ?>:</b>
	<?php echo CHtml::encode($data->stripe_subscription); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stripe_plan')); ?>:</b>
	<?php echo CHtml::encode($data->stripe_plan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_four')); ?>:</b>
	<?php echo CHtml::encode($data->last_four); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('card_expiry')); ?>:</b>
	<?php echo CHtml::encode($data->card_expiry); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('card_expiry_sent')); ?>:</b>
	<?php echo CHtml::encode($data->card_expiry_sent); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trial_ends_at')); ?>:</b>
	<?php echo CHtml::encode($data->trial_ends_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subscription_ends_at')); ?>:</b>
	<?php echo CHtml::encode($data->subscription_ends_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_at')); ?>:</b>
	<?php echo CHtml::encode($data->created_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_at')); ?>:</b>
	<?php echo CHtml::encode($data->updated_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deleted_at')); ?>:</b>
	<?php echo CHtml::encode($data->deleted_at); ?>
	<br />

	*/ ?>

</div>