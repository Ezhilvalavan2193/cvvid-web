<div class="employer-wrapper">
    <section class="section" id="why-cvvid">
        <div class="container">
            <h3 class="text-center">WHY CVVID?</h3>
            <div class="divider divider-center divider-dark"></div>
            <div class="col-xs-6">
                <figure class="about-image" style="background-image: url('../images/slideshow/slide10.jpg');">

                </figure>
            </div>
            <div class="col-xs-6">
                <div class="Slide__content">
                        <h2>CVVID FOR EMPLOYERS.</h2>
                        <p>CVVID is proving to be an enormous asset to employers, making recruitment more effective
                            and more efficient. It can work for you too, saving you valuable time and money.</p>
                        <p>How?</p>
                        <p>Paper CVs are fine for listing academic achievements, but they cannot give an adequate
                            picture of a candidate’s personality - so crucial to the smooth running of your
                            business.</p>
                        <p>Now you don’t have to wade through hundreds of paper CVs. Instead, you can quickly
                            assess a candidate’s suitability based on a far more personal introduction – Video CV.
                            You can get a much clearer idea of what each candidate is about before calling them for
                            interview. It’s the next best thing to a face-to-face – and far quicker.</p>
                         <a href="#" class="btn btn-primary faqs-btn">FAQs</a>
                    </div>
            </div>
    </section>
    <?php
     $model = Slideshows::model()->findByAttributes(array('slug' => 'faqs'));
     $slidemodel = SlideshowSlides::model()->findAllByAttributes(array('slideshow_id' => $model->id, 'deleted_at' => null));
    ?>
    <section class="section background-grey" id="faqs">
        <div class="container">
            <h3 class="text-center">FAQs</h3>
            <div class="divider divider-center divider-dark"></div>
            <?php 
              if($model->background_id > 0)
              {
                  $mediamodel = Media::model()->findByPk($model->background_id);
                  if($mediamodel != null && $mediamodel->file_name != null)
                  {
                      $src = "https://cvvid.com/images/slideshow/slide11.jpg"; ?>
            <div class="col-xs-6">
                  <h2>The Key Questions</h2>
                <div class="swiper-container">
<div class="swiper-wrapper">
      <?php
foreach ($slidemodel as $slide) {
                                    ?>
 
<div class="swiper-slide">
   <h2><?php echo $slide['title']; ?></h2>
 <?php echo $slide['body']; ?></div>
  <?php } ?>

</div>
<div class="swiper-pagination"></div>
<div class="btn-wrapper">
    <a class="btn btn-primary employer-reg-btn" href="#">Register Now</a>
</div>
</div>
                
            </div>
            <div class="col-xs-6">
                <figure class="about-image-worry" style="background-image: url('<?php echo $src; ?>');">

                </figure>
            </div>
              <?php } } else { ?>
             <?php
        if (isset($slidemodel)){
        $cnt = 0;
        foreach ($slidemodel as $slide) { 
           $mediamodel = Media::model()->findByPk($slide['media_id']);  
        ?>
            <div class="col-xs-6">
                <div class="Slide__inner">
                    <div class="Slide__caption">
                       <?php echo $slide['body']; ?>
                        <a href="<?php echo Yii::app()->createUrl('site/about'); ?>" class="btn btn-primary">Learn More</a>
                        <a href="#" class="view-showcase btn btn-primary">View CV Video Showcase</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <figure class="about-image-worry" style="background-image: url('<?php echo Yii::app()->baseUrl . "/images/media/" . $mediamodel->id . "/" . $mediamodel->file_name; ?>'); background-position: <?php echo $slide['background_x']; ?> <?php echo $slide['background_y']; ?>;">

                </figure>
            </div>
            <?php } ?>
        <?php } ?>
        <?php } ?>
    </section>
    
    <?php
    $model = VideoCategories::model()->findByAttributes(array('slug' => 'success-stories'));
    $videomodel = Videos::model()->findAllByAttributes(array('category_id' => $model->id, 'deleted_at' => null));
    ?>
    <section class="section" id="video-showcase">
        <div class="container">
            <h3 class="text-center">CV VIDEO SHOWCASE</h3>
            <div class="divider divider-center divider-dark"></div>
            <p class="text-center">Positive proof that you can realise your career ambitions with a great video CV.</p>
            <div class="col-xs-12">
                <ul class="listing">
                     <?php
                foreach ($videomodel as $video) {

                    $cri = new CDbCriteria();
                    $cri->condition = "model_type like '%Video%' and model_id =" . $video['id'];
                    $mediamodel = Media::model()->find($cri);
                    ?>   
                    <li id="<?php echo $video['video_id'] ?>">
                        <div>
                            <img src="<?php echo Yii::app()->baseUrl . '/images/media/' . $mediamodel->id . '/conversions/profile.jpg' ?>" alt="<?php echo $video['name']; ?>">
                            <div class="bottom-wrap"><span class="SuccessStories__item__title"><?php echo $video['name']; ?></span> (<?php echo gmdate('i:s', $video['duration']); ?>)</div>
                        </div>
                    </li>
                    <?php } ?>
                    
                </ul>
            </div>
    </section>
    <section id="employer-membership" class="section background-membeship">
        <h3 class="text-center">CHOOSE YOUR MEMBERSHIP</h3>
        <div class="divider divider-center divider-dark"></div>
        <div class="price-tables-container clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-2 col-xs-4">
                        <div class="subscription-plan">
                            <div class="subscription-plan-header">
                                <h2 class="plan-title">Basic</h2>
                                <div class="plan-price">FREE</div>
                            </div>
                            <div class="subscription-plan-content">
                                <div class="subscription-plan-inner" style="min-height: 260px;">
                                    <div class="plan-description">
                                        <div class="plan-description">
                                            Enjoy the basic features of the CVVid platform.
                                        </div>
                                        <div class="plan-features">
                                            <ul class="fa-ul">
                                            <li><i class="fa fa-check"></i>Create an Employer profile</li>
                                            <li><i class="fa fa-check"></i>Search for suitable candidates by qualification or geography</li>
                                            <li><i class="fa fa-check"></i>View which potential candidates have visited your company profile</li>
                                        </ul>
                                        </div>
                                    </div>
                                    <div class="plan-button" style="bottom: -21px;">
                                        <a href="<?php echo $this->createUrl('site/basicregisteremployer'); ?>" class="btn hollow-btn">Join Free</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="col-xs-4">
                        <div class="subscription-plan premium">
                            <div class="subscription-plan-header">
                                <h2 class="plan-title">Premium</h2>
                                <div class="plan-price">
                                    £450.00
                                </div>
                            </div>
                            <div class="subscription-plan-content">
                                <div class="subscription-plan-inner" style="min-height: 260px;">
                                    <div class="plan-description">
                                        Enjoy the full benefits of the CVVid platform.
                                    </div>
                                    <div class="plan-features">
                                        <div>All Basic features plus</div>
                                        <ul class="fa-ul">
                                        <li><i class="fa fa-check"></i>Search and contact suitable candidates </li>
                                        <li><i class="fa fa-check"></i>Upload unlimited number of job vacancies</li>
                                        <li><i class="fa fa-check"></i>Carry out online interviews with potential candidates</li>
                                    </ul>
                                    </div>

                                    <div class="free-trial plan-button">
                                        <a href="<?php echo $this->createUrl('site/premiumregisteremployer'); ?>" class="btn btn-danger">
                                            JOIN <span>NOW </span>
                                        </a>
                                        <br>
                                        <!--<small>for a limited time only</small>-->
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<?php
$model = Slideshows::model()->findByAttributes(array('slug' => 'about-us'));
$slidemodel = SlideshowSlides::model()->findAllByAttributes(array('slideshow_id' => $model->id, 'deleted_at' => null));
?>
<div class='candidate-wrapper'>
    <section class="section why-cvvid" id="why-cvvid">
        <div class="container">
            <h3 class="text-center">WHY CVVID?</h3>
            <div class="divider divider-center divider-dark"></div>
            <?php
            if ($model->background_id > 0) {
                $mediamodel = Media::model()->findByPk($model->background_id);
                if ($mediamodel != null && $mediamodel->file_name != null) {
                    $src = Yii::app()->baseUrl . '/images/media/' . $mediamodel->id . '/' . $mediamodel->file_name;
                    ?>
                    <div class="col-xs-6">

                        <figure class="about-image" style="background-image: url('<?php echo $src; ?>')">

                        </figure>
                    </div>
                    <div class="col-xs-6">
                        <div class="Slide__content">
                            <div class="PageSlideshow">
                                <div class="PageSlideshow__slides">
                                    <?php
                                    foreach ($slidemodel as $slide) {
                                        ?>
                                        <div class="PageSlideshow__slide">
                                            <?php echo $slide['body']; ?>
                                        </div>
                                    <?php } ?>

                                </div>
                                <div class="PageSlideshow__controls" style="margin-top: 20px;">
                                    <a href="#" class="PageSlideshow__nav-btn PageSlideshow__prev"><i class="fa fa-chevron-left"></i></a>
                                    <span class="PageSlideshow__pages">
                                        <span class="PageSlideshow__current-page">1</span>/<span class="PageSlideshow__count">4</span>
                                    </span>
                                    <a href="#" class="PageSlideshow__nav-btn PageSlideshow__next"><i class="fa fa-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }
            } else { ?>
                <div class="col-xs-6">
                    <?php
                    if (isset($slidemodel)) {
                        $cnt = 0;
                        foreach ($slidemodel as $slide) {
                            $mediamodel = Media::model()->findByPk($slide['media_id']);
                            ?>
                            <figure class="about-image" style="background-image: url('<?php echo Yii::app()->baseUrl . "/images/media/" . $mediamodel->id . "/" . $mediamodel->file_name; ?>'); background-position: <?php echo $slide['background_x']; ?> <?php echo $slide['background_y']; ?>;">

                            </figure>
                        </div>
                        <div class="col-xs-6">
                            <div class="Slide__inner">
                                <div class="Slide__caption">
                                    <?php echo $slide['body']; ?>
                                    <a href="<?php echo Yii::app()->createUrl('site/about'); ?>" class="btn btn-primary">Learn More</a>
                                    <a href="#" class="view-showcase btn btn-primary">View CV Video Showcase</a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
            <?php } ?>

    </section>
    <?php 
    $model = Slideshows::model()->findByAttributes(array('slug' => 'top-tips'));
    $slidemodel = SlideshowSlides::model()->findAllByAttributes(array('slideshow_id' => $model->id, 'deleted_at' => null));
    ?>
    <section class="section background-grey" id="top-tips">
        <div class="container">
            <h3 class="text-center">Top Tips</h3>
            <div class="divider divider-center divider-dark"></div>
            <?php 
            if($model->background_id > 0)
            {
                $mediamodel = Media::model()->findByPk($model->background_id);
                if($mediamodel != null && $mediamodel->file_name != null)
                {
                 $src = Yii::app()->baseUrl.'/images/media/'.$mediamodel->id.'/'.$mediamodel->file_name; ?>
            <div class="col-xs-6">
                <div class="Slide__content">
                        <div class="PageSlideshow">
                            <div class="PageSlideshow__slides">
                                <?php
                                foreach ($slidemodel as $slide) {
                                    ?>
                                    <div class="PageSlideshow__slide">
                                        <?php echo $slide['body']; ?>
                                    </div><!-- .PageSlideshow__slide -->
                                <?php } ?>

                            </div>
                            <div class="PageSlideshow__controls" style="margin-top: 20px;">
                                <a href="#" class="PageSlideshow__nav-btn PageSlideshow__prev"><i class="fa fa-chevron-left"></i></a>
                            <span class="PageSlideshow__pages">
                                <span class="PageSlideshow__current-page">1</span>/<span class="PageSlideshow__count">4</span>
                            </span>
                                <a href="#" class="PageSlideshow__nav-btn PageSlideshow__next"><i class="fa fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="col-xs-6">
                <figure class="about-image-top" style="background-image: url('<?php echo $src; ?>')">

                </figure>
            </div>
              <?php } } else { ?>
             <?php
        if (isset($slidemodel)){
        $cnt = 0;
        foreach ($slidemodel as $slide) { 
           $mediamodel = Media::model()->findByPk($slide['media_id']);  
        ?>
            <div class="col-xs-6">
                <div class="Slide__inner">
                    <div class="Slide__caption">
                       <?php echo $slide['body']; ?>
                        <a href="<?php echo Yii::app()->createUrl('site/about'); ?>" class="btn btn-primary">Learn More</a>
                        <a href="#" class="view-showcase btn btn-primary">View CV Video Showcase</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <figure class="about-image-top" style="background-image: url('<?php echo Yii::app()->baseUrl . "/images/media/" . $mediamodel->id . "/" . $mediamodel->file_name; ?>'); background-position: <?php echo $slide['background_x']; ?> <?php echo $slide['background_y']; ?>;">

                </figure>
            </div>
             <?php } ?>
        <?php } ?>
             <?php }  ?>
    </section>
    <?php
    $model = VideoCategories::model()->findByAttributes(array('slug' => 'how-to'));
    $videomodel = Videos::model()->findAllByAttributes(array('category_id' => $model->id, 'deleted_at' => null));
    ?>
    <section class="section" id="how-to">
        <div class="container">
            <h3 class="text-center">HOW TO - THE PATH TO SUCCESS</h3>
            <div class="divider divider-center divider-dark"></div>
            <div class="col-xs-12">
                    <ul class="listing">
                        <?php
                        $cnt = 1;
                        foreach ($videomodel as $video) {
                        $cri = new CDbCriteria();
                        $cri->condition = "model_type like '%Video%' and model_id =" . $video['id'];
                        $mediamodel = Media::model()->find($cri);
                        
                        ?>
                        <?php 
                            $bucket = "cvvid-new";
                            
                           $videomodel = Videos::model()->findByPk($video['video_id']);
                            $url = 'https://' . Yii::app()->params['AWS_BUCKET'] . '.s3.amazonaws.com/how-to/'.$video['video_id'];
                        ?>
                        <li>
                            <div id="my-video-<?php echo $cnt; ?>">
                                <script type="text/javascript">
                                jwplayer("my-video-<?php echo $cnt; ?>").setup({
                                 'file': '<?php echo $url; ?>',
                                 'image': '<?php echo $mediamodel != null ? Yii::app()->baseUrl."/images/media/".$mediamodel->id."/".$mediamodel->file_name : ""?>',		 
                                });
                               	
                                </script>
                          
                            </div>
                                <div class="bottom-wrap"><?php echo $video['name']; ?> (<?php echo gmdate('i:s', $video['duration']); ?>)</div>
                        </li>
                        <?php
         $cnt++;
                }
            
            ?>
                        
                    </ul>
                </div>
            </div>
    </section>
    <?php
     $model = Slideshows::model()->findByAttributes(array('slug' => 'worry-list'));
     $slidemodel = SlideshowSlides::model()->findAllByAttributes(array('slideshow_id' => $model->id, 'deleted_at' => null));
    ?>
    <section class="section background-grey" id="worry-list">
        <div class="container">
            <h3 class="text-center">Worry List</h3>
            <div class="divider divider-center divider-dark"></div>
            <?php 
              if($model->background_id > 0)
              {
                  $mediamodel = Media::model()->findByPk($model->background_id);
                  if($mediamodel != null && $mediamodel->file_name != null)
                  {
                      $src = Yii::app()->baseUrl.'/images/media/'.$mediamodel->id.'/'.$mediamodel->file_name; ?>
            <div class="col-xs-6">
                <div class="Slide__content">
                        <div class="PageSlideshow">
                            <div class="PageSlideshow__slides">
                                <?php
                                foreach ($slidemodel as $slide) {
                                    ?>
                                    <div class="PageSlideshow__slide">
                                        <?php echo $slide['body']; ?>
                                    </div><!-- .PageSlideshow__slide -->
                                <?php } ?>

                            </div>
                            <div class="PageSlideshow__controls" style="margin-top: 20px;">
                                <a href="#" class="PageSlideshow__nav-btn PageSlideshow__prev"><i class="fa fa-chevron-left"></i></a>
                            <span class="PageSlideshow__pages">
                                <span class="PageSlideshow__current-page">1</span>/<span class="PageSlideshow__count">4</span>
                            </span>
                                <a href="#" class="PageSlideshow__nav-btn PageSlideshow__next"><i class="fa fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="col-xs-6">
                <figure class="about-image-worry" style="background-image: url('<?php echo $src; ?>');">

                </figure>
            </div>
              <?php } } else { ?>
             <?php
        if (isset($slidemodel)){
        $cnt = 0;
        foreach ($slidemodel as $slide) { 
           $mediamodel = Media::model()->findByPk($slide['media_id']);  
        ?>
            <div class="col-xs-6">
                <div class="Slide__inner">
                    <div class="Slide__caption">
                       <?php echo $slide['body']; ?>
                        <a href="<?php echo Yii::app()->createUrl('site/about'); ?>" class="btn btn-primary">Learn More</a>
                        <a href="#" class="view-showcase btn btn-primary">View CV Video Showcase</a>
                    </div>
                </div>
            </div>
            <div class="col-xs-6">
                <figure class="about-image-worry" style="background-image: url('<?php echo Yii::app()->baseUrl . "/images/media/" . $mediamodel->id . "/" . $mediamodel->file_name; ?>'); background-position: <?php echo $slide['background_x']; ?> <?php echo $slide['background_y']; ?>;">

                </figure>
            </div>
            <?php } ?>
        <?php } ?>
        <?php } ?>
    </section>
    <?php
    $model = VideoCategories::model()->findByAttributes(array('slug' => 'success-stories'));
    $videomodel = Videos::model()->findAllByAttributes(array('category_id' => $model->id, 'deleted_at' => null));
    ?>
    <section class="section" id="video-showcase">
        <div class="container">
            <h3 class="text-center">CV VIDEO SHOWCASE</h3>
            <div class="divider divider-center divider-dark"></div>
            <p class="text-center">Positive proof that you can realise your career ambitions with a great video CV.</p>
            <div class="col-xs-12">
                <ul class="listing">
                     <?php
                foreach ($videomodel as $video) {

                    $cri = new CDbCriteria();
                    $cri->condition = "model_type like '%Video%' and model_id =" . $video['id'];
                    $mediamodel = Media::model()->find($cri);
                    ?>   
                    <li id="<?php echo $video['video_id'] ?>">
                        <div>
                            <img src="<?php echo Yii::app()->baseUrl . '/images/media/' . $mediamodel->id . '/conversions/profile.jpg' ?>" alt="<?php echo $video['name']; ?>">
                            <div class="bottom-wrap"><span class="SuccessStories__item__title"><?php echo $video['name']; ?></span> (<?php echo gmdate('i:s', $video['duration']); ?>)</div>
                        </div>
                    </li>
                    <?php } ?>
                    
                </ul>
            </div>
    </section>
    <section id="candidate-membership" class="section background-membeship">
        <h3 class="text-center">CHOOSE YOUR MEMBERSHIP</h3>
        <div class="divider divider-center divider-dark"></div>
        <div class="price-tables-container clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-2 col-xs-4">
                        <div class="subscription-plan">
                            <div class="subscription-plan-header">
                                <h2 class="plan-title">Basic</h2>
                                <div class="plan-price">FREE</div>
                            </div>
                            <div class="subscription-plan-content">
                                <div class="subscription-plan-inner">
                                    <div class="plan-description">
                                        <div class="plan-description">
                                            Enjoy the basic features of the CVVid platform.
                                        </div>
                                        <div class="plan-features">
                                            <ul class="fa-ul">
                                                <li><i class="fa fa-check"></i>Create your own CVVid profile</li>
                                                <li><i class="fa fa-check"></i>Upload your portfolio</li>
                                                <li><i class="fa fa-check"></i>Browse all vacancies</li>
                                                <li><i class="fa fa-check"></i>Create your CVVid profile PDF</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="plan-button">
                                        <a href="<?php echo $this->createUrl('site/basicregistercandidate'); ?>" class="btn hollow-btn">Basic Account</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> 
                    <div class="col-xs-4">
                        <div class="subscription-plan premium">
                            <div class="subscription-plan-header">
                                <h2 class="plan-title">Premium</h2>
                                
                                <div class="plan-price" >
                                <p style="padding-bottom:0px;"><strike style="text-decoration-color: red;">£ 252.00</strike>  Free</p>
                                </div>
                            </div>
                            <div class="subscription-plan-content">
                                <div class="subscription-plan-inner">
                                    <div class="plan-description">
                                        Enjoy the full benefits of the CVVid platform.
                                    </div>
                                    <div class="plan-features">
                                        <div>All Basic features plus</div>
                                        <ul class="fa-ul">
                                            <li><i class="fa fa-check"></i>Browse and apply for jobs</li>
                                            <li><i class="fa fa-check"></i>Unique real-time messaging platform</li>
                                            <li><i class="fa fa-check"></i>See who has visited your profile</li>
                                            <li><i class="fa fa-check"></i>See who has liked your profile</li>
                                        </ul>
                                    </div>

                                    <div class="free-trial plan-button">
                                        <a href="<?php echo $this->createUrl('site/premiumregistercandidate'); ?>" class="btn btn-danger">
                                            JOIN FOR <span>FREE</span>
                                        </a>
                                        <br>
                                        <small>for a limited time only</small>
                                    </div>
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

            <style>
                                                                                                                                                                                                                                                                                                                                                                                                                            
                                #my-video {
font-size: 0.9em;
height: 0;
overflow: hidden;
padding-bottom: 53%;
padding-top: 20px;
position: relative;
width:100%!important;
}
#my-video embed, #my-video object, #my-video iframe {
max-height: 100%;
max-width: 100%;
height: 100%;
left: 0pt;
position: absolute;
top: 0pt;
width: 100%;
}
.jwplayer {
    width: 100%!important;}
 </style>
