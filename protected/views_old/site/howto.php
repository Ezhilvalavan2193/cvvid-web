<?php
/* @var $this SiteController */
?>
 <style>
                                                                                                                                                                                                                                                                                                                                                                                                                            
                                #my-video {
font-size: 0.9em;
height: 0;
overflow: hidden;
padding-bottom: 53%;
padding-top: 20px;
position: relative;
width:100%!important;
}
#my-video embed, #my-video object, #my-video iframe {
max-height: 100%;
max-width: 100%;
height: 100%;
left: 0pt;
position: absolute;
top: 0pt;
width: 100%;
}
.jwplayer {
    width: 100%!important;}
                            </style>
<div id="page-content">
    <div class="HowTo__header" style="background-image: <?php echo Yii::app()->theme->baseUrl."/images/success-stories-header.jpg"; ?>">
        <div class="container">
            <div class="row">
                <div class="col-sm-7">
                    <h1>How To - The Path To Success</h1>
                    <h4>You never get a second chance to make a first impression.</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="HowTo">
        <div class="container">
             <?php
        $cnt = 0;
        foreach ($videomodel as $video) {
            $cnt++;
            if ($cnt == 1) {
                $cri = new CDbCriteria();
                $cri->condition = "model_type like '%Video%' and model_id =" . $video['id'];
                $mediamodel = Media::model()->find($cri);
                ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="VideoItem__video">
                       <!--   <iframe src="//player.vimeo.com/video/<?php //echo $video['video_id']?>" width="100%" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>-->
                        <?php 
                            $bucket = "cvvid-new";
                            
                           $videomodel = Videos::model()->findByPk($video['video_id']);
                            $url = 'http://' . Yii::app()->params['AWS_BUCKET'] . '.s3.amazonaws.com/how-to/'.$video['video_id'];
                        ?>
                        		<div id="my-video"></div>
                                <script type="text/javascript">
                                jwplayer("my-video").setup({
                                 'file': '<?php echo $url; ?>',
                                 'image': '<?php echo $mediamodel != null ? Yii::app()->baseUrl."/images/media/".$mediamodel->id."/".$mediamodel->file_name : ""?>',		 
//                                 width: "100%",
//                                 height: "100%",
                                // 'primary': 'flash',
                                 //'autostart': 'true'
                                });
                               	
                                </script>
                    </div>
                </div>
            </div>
             <?php
            } else {

                if ($cnt == 2) {
                    ?> 
            <h4>Some great tips on how to create your video CV</h4>
            <div class="HowTo__other-videos">
                <?php
                    }
                    $cri = new CDbCriteria();
                    $cri->condition = "model_type like '%Video%' and model_id =" . $video['id'];
                    $mediamodel = Media::model()->find($cri);
                    ?>
                <div class="VideoItem" style="width: 350px;height:auto;">
                    <div class="ModalVideo" data-video="<?php echo $video['id']; ?>" data-name="<?php echo $video['name']; ?>">
                        <div class="VideoItem__image">
                         <?php 
                            $bucket = "cvvid-new";
                           $videomodel = Videos::model()->findByPk($video['video_id']);
                            $url = 'http://' . Yii::app()->params['AWS_BUCKET'] . '.s3.amazonaws.com/how-to/'.$video['video_id'];
                        ?>
                        		<div id="my-video<?php echo $cnt?>" ></div>
                                <script type="text/javascript">
                                jwplayer("my-video<?php echo $cnt ?>").setup({
                                 'file': '<?php echo $url; ?>',//'https://s3.us-east-2.amazonaws.com/cvvid/small.mp4',
                                 'image': '<?php echo $mediamodel != null ? Yii::app()->baseUrl."/images/media/".$mediamodel->id."/".$mediamodel->file_name : ""?>',
//                                 width: "100%",
//                                 height: "100%",
                                // 'primary': 'flash',
                                 //'autostart': 'true'
                                });
                               	
                                </script>
                           <!--  <img src="<?php //echo Yii::app()->baseUrl . '/images/media/' . $mediamodel->id . '/conversions/profile.jpg' ?>" class="img-responsive"> -->
                        </div>
                    </div>
                    <div class="VideoItem__meta clearfix" style="margin:0">
                        <div class="VideoItem__title"><?php echo $video['name']; ?></div>
                        <div class="VideoItem__duration"><?php echo gmdate('i:s', $video['duration']); ?></div>
                    </div>
                </div>
                <?php
                    if (count($videomodel) == ($cnt - 1))
                        echo "</div>";
                }
            }
            ?>
            </div><!-- .SuccessStory__other-videos -->
        </div>
    </div>
</div>
