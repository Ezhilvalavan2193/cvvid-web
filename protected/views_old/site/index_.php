<?php
/* @var $this SiteController */
?>

<div id="colcover">
    <?php
    if ($model->background_id > 0) {
        $mediamodel = Media::model()->findByPk($model->background_id);
        if ($mediamodel != null && $mediamodel->file_name != null) {
            $src = Yii::app()->baseUrl . '/images/media/' . $mediamodel->id . '/' . $mediamodel->file_name;

            echo "<section class='cover-photo' id='cover-sec'>";

            echo "<div class='cover-photo' id='img-cover' style='background-image: url(" . $src . ");'></div>";

            echo '</section>';
        }
    } else {
        ?>            
        <div id="home-slide" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
        <?php
        $cnt = 0;
        foreach ($slidemodel as $slide) {
            if ($slide['media_id'] > 0) {
                $cnt++;
                if ($cnt == 1)
                    $class = "item active";
                else
                    $class = "item";
                $mediamodel = Media::model()->findByPk($slide['media_id']);
                echo '<div class="' . $class . '">';
                echo '<div class="banner-text">' . $slide['body'];
                echo '<a href="' . Yii::app()->createUrl('site/about') . '" class="btn btn-primary" style="padding:13px">Learn More</a>&nbsp;';
                echo '<a href="#" class="video-showcase btn btn-primary" style="padding:13px">View CV Video Showcase</a>';
                echo '</div>';
                echo '<img src="' . Yii::app()->baseUrl . "/images/media/" . $mediamodel->id . "/" . $mediamodel->file_name . '" alt="' . $mediamodel->name . '">';
                echo '</div>';
            }
        }
        ?>
                <!-- Left and right controls -->
                <a class="left carousel-control" href="#home-slide" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#home-slide" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>

<?php } ?>

    <div class="success-video-carousel">
        <div class="video-carousel Slideshow" data-is-full-screen="true" data-slides-to-show="5" data-autoplay="false"
             data-responsive='[{"breakpoint": 1024, "settings":{"slidesToShow": 5}},{"breakpoint": 992, "settings":{"slidesToShow": 3}},{"breakpoint": 768, "settings":{"slidesToShow": 1}}]'>
            <div class="SuccessStories__slide">
                <a class="SuccessVideo modalVideo" data-video="169239600" data-name="Alwyn David - Engineer"><div class="SuccessStories__item__image" style="background-image: url('http://localhost/cvvid_laran/public/media/98/conversions/profile.jpg');"></div></a>
                <div class="SuccessStories__item__meta clearfix">
                    <div class="SuccessStories__item__title">Alwyn David - Engineer</div>
                </div>
            </div>
            <div class="SuccessStories__slide">
                <a class="SuccessVideo modalVideo" data-video="169239725" data-name="Michael Grimmond - Gardener"><div class="SuccessStories__item__image" style="background-image: url('http://localhost/cvvid_laran/public/media/99/conversions/profile.jpg');"></div></a>
                <div class="SuccessStories__item__meta clearfix">
                    <div class="SuccessStories__item__title">Michael Grimmond - Gardener</div>
                </div>
            </div>
            <div class="SuccessStories__slide">
                <a class="SuccessVideo modalVideo" data-video="169696984" data-name="Zeynep - Film-maker"><div class="SuccessStories__item__image" style="background-image: url('http://localhost/cvvid_laran/public/media/104/conversions/profile.jpg');"></div></a>
                <div class="SuccessStories__item__meta clearfix">
                    <div class="SuccessStories__item__title">Zeynep - Film-maker</div>
                </div>
            </div>
            <div class="SuccessStories__slide">
                <a class="SuccessVideo modalVideo" data-video="169259539" data-name="Myles - Plumber"><div class="SuccessStories__item__image" style="background-image: url('http://localhost/cvvid_laran/public/media/102/conversions/profile.jpg');"></div></a>
                <div class="SuccessStories__item__meta clearfix">
                    <div class="SuccessStories__item__title">Myles - Plumber</div>
                </div>
            </div>
            <div class="SuccessStories__slide">
                <a class="SuccessVideo modalVideo" data-video="169259515" data-name="Annabel - Law Graduate"><div class="SuccessStories__item__image" style="background-image: url('http://localhost/cvvid_laran/public/media/103/conversions/profile.jpg');"></div></a>
                <div class="SuccessStories__item__meta clearfix">
                    <div class="SuccessStories__item__title">Annabel - Law Graduate</div>
                </div>
            </div>
            <div class="SuccessStories__slide">
                <a class="SuccessVideo modalVideo" data-video="169258813" data-name="Ben - Anthropologist"><div class="SuccessStories__item__image" style="background-image: url('http://localhost/cvvid_laran/public/media/101/conversions/profile.jpg');"></div></a>
                <div class="SuccessStories__item__meta clearfix">
                    <div class="SuccessStories__item__title">Ben - Anthropologist</div>
                </div>
            </div>
            <div class="SuccessStories__slide">
                <a class="SuccessVideo modalVideo" data-video="169258609" data-name="Anita - Fitness Instructor"><div class="SuccessStories__item__image" style="background-image: url('http://localhost/cvvid_laran/public/media/100/conversions/profile.jpg');"></div></a>
                <div class="SuccessStories__item__meta clearfix">
                    <div class="SuccessStories__item__title">Anita - Fitness Instructor</div>
                </div>
            </div>
            <div class="SuccessStories__slide">
                <a class="SuccessVideo modalVideo" data-video="169696886" data-name="Dr Rabeah - Virologist"><div class="SuccessStories__item__image" style="background-image: url('http://localhost/cvvid_laran/public/media/105/conversions/profile.jpg');"></div></a>
                <div class="SuccessStories__item__meta clearfix">
                    <div class="SuccessStories__item__title">Dr Rabeah - Virologist</div>
                </div>
            </div>
        </div><!-- .video-carousel -->
    </div><!-- .success-video-carousel -->

    <!-- Indicators -->
    <!--  <ol class="carousel-indicators">
       <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
       <li data-target="#myCarousel" data-slide-to="1"></li>
       <li data-target="#myCarousel" data-slide-to="2"></li>
     </ol> -->

    <!-- Wrapper for slides -->
</div>
<script>
$(document).ready(function (){
    $('.video-showcase').click(function (){
        $('.success-video-carousel').toggleClass('open');
    });
});

</script>


