<?php
/* @var $this SiteController */
?>
 <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAd4jLN8LdoltMtZJfGS3C8ZTgPL7LDtdw&region=UK&sensor=false&type=school"></script>
<script>
function initialize() {
	  var input = document.getElementById('searchInstitute');
	  var autocompleteSchools = new google.maps.places.Autocomplete(input, {
		    types: ['establishment'], 
		    componentRestrictions: {country: "uk"}
		});
	}

	google.maps.event.addDomListener(window, 'load', initialize);
</script>
<div class="site-content">
    <div class="page-title text-left">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1>   
                        REGISTER YOUR INTEREST AS A SCHOOL
                    </h1>
                </div>
            </div>
        </div>
    </div>
    
    
    
    <div id="page-content">
        <div class="container">

           <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'employer-form',
                'action' => $this->createUrl('site/createInstitutions'),
                'enableAjaxValidation' => false,
                'htmlOptions' => array('enctype' => 'multipart/form-data')
            ));
            ?>

               
           <?php echo  $form->errorSummary(array($model,$usermodel,$addressmodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>

                <div class="form-container">
                    <h4>Your Details</h4>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                 <?php echo $form->textField($usermodel, 'forenames', array('class' => 'form-control','placeholder'=>'Forenames')); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?php echo $form->textField($usermodel, 'surname', array('class' => 'form-control','placeholder'=>'Surname')); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                 <?php echo $form->textField($usermodel, 'email', array('class' => 'form-control','placeholder'=>'Email Address')); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?php echo $form->textField($usermodel, 'tel', array('class' => 'form-control','placeholder'=>'Contact Number')); ?>
                            </div>
                        </div>
                        
                    </div>
                </div>

                <div class="form-container">
                    <h4>School / Institution Details</h4>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                  <?php echo $form->textField($model, 'name', array('class' => 'form-control','placeholder'=>'Name','id'=>'searchInstitute')); ?>
                            </div>
                        </div>
                    </div>

                    <h4>Head Office Address</h4>
                    <div id="location-fields">
                        <div class="form-group">
                            <label class="control-label">Street Address</label>
                            <?php echo $form->textField($addressmodel, 'address', array('class' => 'form-control', 'placeholder' => 'Address','id'=>'route')); ?>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label">Post Code</label>
                            <?php echo $form->textField($addressmodel, 'postcode', array('class' => 'form-control location-search', 'placeholder' => 'Post Code','id'=>'postal_code')); ?>
                             <input id="location_lat" name="Addresses[latitude]" type="hidden" value="<?php echo $addressmodel->latitude ?>">
                            <input id="location_lng" name="Addresses[longitude]" type="hidden" value="<?php echo $addressmodel->longitude ?>">
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">City</label>
                                    <?php echo $form->textField($addressmodel, 'town', array('class' => 'form-control', 'placeholder' => 'City','id'=>'postal_town')); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">County</label>
                                    <?php echo $form->textField($addressmodel, 'county', array('class' => 'form-control', 'placeholder' => 'County','id'=>'administrative_area_level_2')); ?>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Country</label>
                                    <?php echo $form->textField($addressmodel, 'country', array('class' => 'form-control', 'placeholder' => 'Country','id'=>'country')); ?>
                                </div>
                            </div>
                        </div>
                    </div>

					<!-- <h4>Address</h4>
                    <div id="location-fields">
                        <div class="form-group">
                            <label class="control-label">Street Address</label>
                            <?php echo $form->textField($addressmodel, 'address', array('class' => 'form-control', 'placeholder' => 'Address')); ?>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label">Post Code</label>
                            <?php echo $form->textField($addressmodel, 'postcode', array('class' => 'form-control location-search', 'placeholder' => 'Post Code','id'=>'postal_code')); ?>
                             <input id="location_lat" name="Addresses[latitude]" type="hidden" value="<?php echo $addressmodel->latitude ?>">
                            <input id="location_lng" name="Addresses[longitude]" type="hidden" value="<?php echo $addressmodel->longitude ?>">
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">City</label>
                                    <?php echo $form->textField($addressmodel, 'town', array('class' => 'form-control', 'placeholder' => 'City','id'=>'locality')); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">County</label>
                                    <?php echo $form->textField($addressmodel, 'county', array('class' => 'form-control', 'placeholder' => 'County','id'=>'administrative_area_level_2')); ?>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Country</label>
                                    <?php echo $form->textField($addressmodel, 'country', array('class' => 'form-control', 'placeholder' => 'Country','id'=>'country')); ?>
                                </div>
                            </div>
                        </div>
                    </div>-->
                    
                    <h4>Students</h4>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group form-inline">
                                <label>Number of pupils</label>
                                <input class="form-control" name="Institutions[num_pupils]" type="text">
                            </div>
                        </div>
                    </div>
                </div>
<!-- Privacy -->
                    <div class="form-container">
                        <!-- Account -->
                        <h4>Account Settings</h4>
                        <div class="row">
                            <div class="col-sm-3">
                                <?php echo $usermodel->password = ""; ?>
                                <?php echo $form->passwordField($usermodel, 'password', array('class' => 'form-control', 'placeholder' => 'Password')); ?>
                            </div>
                            <div class="col-sm-3">
                                <input class="form-control" placeholder="Confirm Password" name="Users[confirmpassword]" id="Users_confirmpassword" type="password" value="">
                            </div>
                        </div>
                    </div><!-- .form-container -->
                    
                <script src='https://www.google.com/recaptcha/api.js'></script>
                <div class="g-recaptcha" data-sitekey="6Le5Vz4UAAAAAG8IXqMe7onsjTZxNXkLj70i72CG"></div>

                <div class="form-actions">
                    <?php echo CHtml::submitButton('Submit', array("class" => "default-btn",'style'=>'')); ?>
                </div>
           <?php $this->endWidget(); ?>
        </div>
    </div>
    
    
    