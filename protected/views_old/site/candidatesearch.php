<?php
/* @var $this SiteController */
?>


<script>
$(document).ready(function()
{
	 
	$("#skill_categ").change(function()
	{
		var id = $(this).val();
		if(id > 0 )
		{
        	 $.ajax({
                 url: '<?php echo $this->createUrl('site/getSkills') ?>',
                 type: 'POST',
                 data: {id : id},
                 success: function (data) {  
                		$('#skills').html('');                
 						$('#skills').append(data);
                        $('#skills').trigger('liszt:updated');
                 },
                 error: function (data) {
                     alert('err');
                 }
             });
		}
	});
});
</script>
<?php
//    $this->widget('ext.EChosen.EChosen', array(
//        'target' => '.select2',
//        'useJQuery' => true,
//        'debug' => true,
//    ));
?>
<div class="site-content">
    <div class="page-title text-left">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1>   
                       Candidates Search
                    </h1>
                </div>
            </div>
        </div>
    </div>
    
    
    <div id="page-content">
        <div class="CandidateSearch" style="background: url(<?php echo Yii::app()->theme->baseUrl.'/images/candidate-search.jpg'?>) no-repeat;background-size: cover;">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">

                        <div class="CandidateSearch__content">
                            <div class="CandidateSearch__title">
                                <h1>Hire the best</h1>
                                <h4>Find the ideal candidates to secure your company's future success.</h4>
                            </div>
                            <div class="CandidateSearch__form">
                                 <?php        
                                    $form = $this->beginWidget('CActiveForm', array(
                                                      'id'=>'candidate-search-form',
                                                      'method'=>'get',
                                                      'action'=>$this->createUrl('site/searchCandidates'),
                                                      'enableAjaxValidation'=>false
                                     ));         
                                   ?>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                 <?php echo CHtml::hiddenField('view_style','grid',array('id'=>'view_style'));?>
                                                                        
                                                <?php 
                                                   $cri = new CDbCriteria();
                                                   $cri->order = "name";
                                                   $skillcategmodel = SkillCategories::model()->findAll($cri);
                                                   $skillcateglist = CHtml::listData($skillcategmodel, 'id', 'name');
                                                   echo CHtml::dropDownList('skill_category','',$skillcateglist,array('empty'=>'Search by Category','id'=>"skill_categ",'class'=>'form-control')); ?>
                                    
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <select name="skills[]" id="skills" class="select2 form-control"  multiple="" placeholder="Career paths required">
                                                
                                            </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <?php 
                                    		  echo CHtml::textField('location',ip_info("Visitor", "Country"),array('class'=>'location-search form-control','placeholder'=>'Search by Location')); 
                                    		  echo CHtml::hiddenField('latitude','',array('id'=>'location_lat'));
                                    		  echo CHtml::hiddenField('longitude','',array('id'=>'location_lng'));
                                    		  
                                    		  ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                               <?php $this->endWidget(); ?>
                            </div>
                        </div><!-- .CandidateSearch__content -->

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php 
function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
    $output = NULL;
    if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
        $ip = $_SERVER["REMOTE_ADDR"];
        if ($deep_detect) {
            if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
    }
    $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
    $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
    $continents = array(
        "AF" => "Africa",
        "AN" => "Antarctica",
        "AS" => "Asia",
        "EU" => "Europe",
        "OC" => "Australia (Oceania)",
        "NA" => "North America",
        "SA" => "South America"
    );
    if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
        $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
        if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
            switch ($purpose) {
                case "location":
                    $output = array(
                    "city"           => @$ipdat->geoplugin_city,
                    "state"          => @$ipdat->geoplugin_regionName,
                    "country"        => @$ipdat->geoplugin_countryName,
                    "country_code"   => @$ipdat->geoplugin_countryCode,
                    "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                    "continent_code" => @$ipdat->geoplugin_continentCode
                    );
                    break;
                case "address":
                    $address = array($ipdat->geoplugin_countryName);
                    if (@strlen($ipdat->geoplugin_regionName) >= 1)
                        $address[] = $ipdat->geoplugin_regionName;
                        if (@strlen($ipdat->geoplugin_city) >= 1)
                            $address[] = $ipdat->geoplugin_city;
                            $output = implode(", ", array_reverse($address));
                            break;
                case "city":
                    $output = @$ipdat->geoplugin_city;
                    break;
                case "state":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "region":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "country":
                    $output = @$ipdat->geoplugin_countryName;
                    break;
                case "countrycode":
                    $output = @$ipdat->geoplugin_countryCode;
                    break;
            }
        }
    }
    return $output;
}
?>
        
    
    
    
