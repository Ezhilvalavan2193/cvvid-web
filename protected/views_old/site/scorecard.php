<?php 
/* @var $this FixtureController */
/* @var $model Fixture */
?>

<?php echo CHtml::label('Matches List','',array('style'=>'font-size:24px')); ?>
</br></br>
  <?php 
  	$fixture = Fixture::model()->findAll();
  	$leagueid = 0;
    foreach ($fixture as $fix)
    {
    	$leagueid = $fix['league_id'];
    	if($leagueid == 0 || $leagueid == $fix['league_id'])
    	{
    		echo "<br><br>";
    		$league = League::model()->findByPk($fix['league_id']);
    		echo CHtml::label($league->name,"",array('style'=>'font-size:20px;font-weight:italic;'));
    		echo "<br><br>";
    	}
	    	$team1 = Team::model()->findByPk($fix['team1']);
	    	$team2 = Team::model()->findByPk($fix['team2']);
	    	echo CHtml::link($team1->name." Vs ".$team2->name,$this->createUrl('fixture/selectPlayers',array('id'=>$fix['fixture_id'])),array('style'=>'font-weight:bold;font-size:18px'));
    }
  ?>