<?php
/* @var $this SiteController /
  / @var $model LoginForm /
  / @var $form CActiveForm */
?>

<div class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Unauthorized</h1>
            </div>
        </div>
    </div>
</div>
<div id="page-content">
    <div class="container">
        <div class="login-container">
            You don't have the proper credentials to access this page.
        </div><!-- .login-container -->
    </div><!-- .container -->
</div>