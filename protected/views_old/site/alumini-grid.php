<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Candidates'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#users-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});

$('#export-button').on('click',function() {
    $.fn.yiiGridView.export();
});

$.fn.yiiGridView.export = function() {
    $.fn.yiiGridView.update('alumini-grid',{ 
    	success: function() {
    		$('#alumini-grid').removeClass('grid-view-loading');
    		window.location = '". $this->createUrl('exportFile')  . "';
    	},
    	data: $('.form-inline form').serialize() + '&export=true'
    });
}

");
?>
<script>
$(document).ready(function(){
	
});
</script>
<?php 

/* $skillcategid = isset($_GET['skill_category']) ? $_GET['skill_category'] : "";
if($skillcategid > 0)
{
    
}
$gender = isset($_GET['gender']) ? $_GET['gender'] : "";
$skillarr = isset($_GET['skills']) ? $_GET['skills'] : array();
$location = isset($_GET['location']) ? $_GET['location'] : "";
$lat = isset($_GET['latitude']) ? $_GET['latitude'] : "";
$long = isset($_GET['longitude']) ? $_GET['longitude'] : "";
$from_age = isset($_GET['from_age']) ? $_GET['from_age'] : "";
$to_age = isset($_GET['to_age']) ? $_GET['to_age'] : "";
$grade = isset($_GET['grade']) ? $_GET['grade'] : "";

$from_job_cnt = isset($_GET['from_job_cnt']) ? $_GET['from_job_cnt'] : "";
$to_job_cnt = isset($_GET['to_job_cnt']) ? $_GET['to_job_cnt'] : "";

$from_job_view_cnt = isset($_GET['from_job_view_cnt']) ? $_GET['from_job_view_cnt'] : "";
$to_job_view_cnt = isset($_GET['to_job_view_cnt']) ? $_GET['to_job_view_cnt'] : "";

$from_profile_view_cnt = isset($_GET['from_profile_view_cnt']) ? $_GET['from_profile_view_cnt'] : "";
$to_profile_view_cnt = isset($_GET['to_profile_view_cnt']) ? $_GET['to_profile_view_cnt'] : ""; */

?>
<h2 style="text-align: center;">Alumni Report - <?php echo date("Y"); ?></h2>
<?php 
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'alumini-grid',
   //  'itemsCssClass' => 'table',
//     'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',
	'summaryText'=>'',
    'dataProvider'=>$model->searchAlumini(),
	'columns'=>array(
			//'id',
//     	    array(
//     	       // 'name' => 'check',
//     	        'id' => 'selectedIds',
//     	        'value' => '$data->id',
//     	        'class' => 'CCheckBoxColumn',
//     	        'selectableRows'  => 2,
//     	       // 'checked'=>'Yii::app()->user->getState($data->id)',
//     	       // 'checkBoxHtmlOptions' => array('name' => 'idList[]'),
    	        
//     	    ),
// 			array(
// 					'header'=>'Name',
// 					'value'=>'CHtml::link($data->forenames." ".$data->surname,Yii::app()->createUrl("users/tabedit", array("id"=>$data->id)),array("class"=>"profile-edit-button","id"=>"profile-edit"))',
// 			        'type'=>'raw'
// 			),
	       array(
					'header'=>'Age',
					'value'=>'strlen($data->dob) > 2 ? "" : $data->dob',	    			       
	    		),
    	    array(
    	        'header'=>'Gender',
    	        'value'=>'$data->gender == "F" ? "Female" : "Male"',
    	    ),
    	    array(
    	        'header'=>'Grade',
    	        'value'=>'$data->loggeddatetime',
    	    ),
			
			array(
					'header'=>'Status',
					'value'=>'$data->status',
			),
// 			array(
// 					'header'=>'Date Joined',
// 					'value'=>'$data->created_at',
// 			),
// 			array(
// 					'header'=>'Last Active',
// 					'value'=>'$data->loggeddatetime',
//                    // 'visible'=>$basic,
// 			),
			
    	    array
	        (
    	        'header'=>'Job count',
    	        'value'=>'$data->created_at != "" ? $data->created_at : 0',
    	       // 'visible'=>$paid,
    	    ),

    	    array
    	    (
    	        'header'=>'Job View count',
    	        'value'=>'$data->updated_at != "" ? $data->updated_at : 0',
    	        // 'visible'=>$paid,
    	    ),
    	    array
    	    (
    	        'header'=>'Profile Viewed count',
    	        'value'=>'$data->display_gender != "" ? $data->display_gender : 0',
    	        // 'visible'=>$paid,
    	    ),
	),
)); ?>


