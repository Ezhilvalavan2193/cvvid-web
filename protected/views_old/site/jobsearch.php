<?php
/* @var $this SiteController */
?>
<?php
$this->_pageTitle = "Job Search";
//$this->widget('ext.EChosen.EChosen', array(
//    'target' => '.select2',
//    'useJQuery' => true,
//    'debug' => true,
//));
?>
<script>
    $(document).ready(function ()
    {

    	$( ".search-btnn" ).click(function( event ) {  
    		event.preventDefault();
    		var id =  $("#skill_categ").val();
    		var location =  $("#location-box").val();
//     		if(id > 0)
//     		{
        		var url ="";
        		//alert('asd');
        		$.ajax({
                    url: '<?php echo Yii::app()->createUrl('site/getSkillCategory') ?>',
                    type: 'POST',
                    data: {id: id,loc : location},
                    async: false,
                    success: function (data) {                        
                       $( "#job-search-form" ).attr('action', data).submit();
                    },
                    error: function (data) {
                        alert('err');
                    }
                });
//     		}
//     		else
//     		{
// 				alert("Category is mandatory!");
//     		}
            	return false;
    	});
        
        $("#skill_categ").change(function ()
        {
            var id = $(this).val();
            $('#skills').html('');
            $('#skills').attr("disabled",true);
            if (id > 0)
            {
                $.ajax({
                    url: '<?php echo Yii::app()->createUrl('site/getSkills') ?>',
                    type: 'POST',
                    data: {id: id},
                    success: function (data) {
                    	$('#skills').attr("disabled",false);
                        $('#skills').html('');
                        $('#skills').append(data);
                        $('#skills').trigger('liszt:updated');
                    },
                    error: function (data) {
                        alert('err');
                    }
                });
            }
            else
            {
                $('#skills').html('');
                $('#skills').trigger('liszt:updated');
            }
        });
    });
</script>
<div id="page-content">
    <div class="JobSearch">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <div class="JobSearch__content">
                        <div class="JobSearch__title">
                            <h1>You; at your best</h1>
                            <h4>Find the job that maximises your potential for greatness</h4>
                        </div>
                        <div class="JobSearch__form">
                        <?php
                                 $form = $this->beginWidget('CActiveForm', array(
                                     'id' => 'job-search-form',
                                    'method'=>'post',
                                    // 'action' => $this->createUrl('site/searchJobs'),
                                     'enableAjaxValidation' => false
                                 ));
                                 ?>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                             <?php //echo CHtml::hiddenField('view_style','grid',array('id'=>'view_style'));?>
                                             <?php
                                                $cri = new CDbCriteria();
                                                $cri->order = "name";
                                                $skillcategmodel = SkillCategories::model()->findAll($cri);
                                                $skillcateglist = CHtml::listData($skillcategmodel, 'id', 'name');
                                                
                                                $scselected = "";
                                                $skselected = "";
                                                if(Yii::app()->user->getState('role') == "candidate")
                                                {
//                                                  //skill category
                                                    $pmodel = Profiles::model()->findByAttributes(array("owner_id"=>Yii::app()->user->getState('userid')));
                                                    $psmodel = ProfileSkills::model()->findByAttributes(array("profile_id"=>$pmodel['id']));
                                                    $skmodel = Skills::model()->findByPk($psmodel['skill_id']);
                                                    $scmodel = SkillCategories::model()->findByPk($skmodel['skill_category_id']);
                                                    if($scmodel != null)
                                                        $scselected = $scmodel->id;
                                                    
                                                    
                                                    //skills
                                                    $psallmodel = ProfileSkills::model()->findAllByAttributes(array("profile_id"=>$pmodel['id']));
                                                    foreach ( $psallmodel as $ps)
                                                    {
                                                        $skmodel = Skills::model()->findByPk($ps['skill_id']);
                                                        $pid = $skmodel->id;
                                                        $pname = $skmodel->name;
                                                        $skselected .= "<option  value='$pid' selected >$pname</option>";
                                                    }
                                                    
                                                }
                                                echo CHtml::dropDownList('skill_category', '', $skillcateglist, array('empty' => 'Search by Category', 'id' => "skill_categ", 'class' => 'form-control','required'=>true));
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                             <select name="skills[]" id="skills" class="select2 form-control" multiple="" disabled="disabled"  data-placeholder="Career paths required">
													
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-inline">
                                            <div class="form-group">
                                                <?php
                                                echo CHtml::textField('location', ip_info("Visitor", "Country"), array('id'=>'location-box','class' => 'location-search form-control', 'placeholder' => 'Search by Location'));
                                                echo CHtml::hiddenField('latitude', '', array('id' => 'location_lat'));
                                                echo CHtml::hiddenField('longitude', '', array('id' => 'location_lng'));
                                                ?>
                                            </div>
                                            <div class="form-group">
                                                <select name="distance" id="distance" class="form-control" >
                                                        <option value="" selected="selected">- Distance -</option>
                                                        <option value="5">Within 5 miles</option>
                                                        <option value="10">Within 10 miles</option>
                                                        <option value="20">Within 20 miles</option>
                                                        <option value="30">Within 30 miles</option>
                                                        <option value="40">Within 40 miles</option>
                                                        <option value="50">Within 50 miles</option>
                                                         <option value="60">Over 50 miles</option>
                                                    </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-1">
                                        <div class="form-group">
                                            <button type="submit" class="btn main-btn search-btnn"><i class="fa fa-search"></i></button>
                                        </div>
                                    </div>
                                </div>
                          <?php $this->endWidget(); ?>
                        </div>
                    </div><!-- .JobSearch__content -->

                </div>
            </div>
        </div>
    </div>
</div>
<?php 
function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
    $output = NULL;
    if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
        $ip = $_SERVER["REMOTE_ADDR"];
        if ($deep_detect) {
            if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
    }
    $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
    $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
    $continents = array(
        "AF" => "Africa",
        "AN" => "Antarctica",
        "AS" => "Asia",
        "EU" => "Europe",
        "OC" => "Australia (Oceania)",
        "NA" => "North America",
        "SA" => "South America"
    );
    if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
        $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
        if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
            switch ($purpose) {
                case "location":
                    $output = array(
                    "city"           => @$ipdat->geoplugin_city,
                    "state"          => @$ipdat->geoplugin_regionName,
                    "country"        => @$ipdat->geoplugin_countryName,
                    "country_code"   => @$ipdat->geoplugin_countryCode,
                    "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                    "continent_code" => @$ipdat->geoplugin_continentCode
                    );
                    break;
                case "address":
                    $address = array($ipdat->geoplugin_countryName);
                    if (@strlen($ipdat->geoplugin_regionName) >= 1)
                        $address[] = $ipdat->geoplugin_regionName;
                        if (@strlen($ipdat->geoplugin_city) >= 1)
                            $address[] = $ipdat->geoplugin_city;
                            $output = implode(", ", array_reverse($address));
                            break;
                case "city":
                    $output = @$ipdat->geoplugin_city;
                    break;
                case "state":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "region":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "country":
                    $output = @$ipdat->geoplugin_countryName;
                    break;
                case "countrycode":
                    $output = @$ipdat->geoplugin_countryCode;
                    break;
            }
        }
    }
    return $output;
}
?>
