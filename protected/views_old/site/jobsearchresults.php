<?php
/* @var $this SiteController */
?>

<script>
    function message(id)
    {
        $('#msgdialog').addClass('in');
        $('body').addClass('modal-open');
        $('#msgdialog').show();
        $("#user_id").val(id);

    }
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };
    
    function removeURLParameter(url, parameter) {
        //prefer to use l.search if you have a location/link object
        var urlparts= url.split('?');   
        if (urlparts.length>=2) {

            var prefix= encodeURIComponent(parameter)+'=';
            var pars= urlparts[1].split(/[&;]/g);

            //reverse iteration as may be destructive
            for (var i= pars.length; i-- > 0;) {    
                //idiom for string.startsWith
                if (pars[i].lastIndexOf(prefix, 0) !== -1) {  
                    pars.splice(i, 1);
                }
            }
            url= urlparts[0]+'?'+pars.join('&');
            return url;
        } else {
            return url;
        }
    }
    function favourite(id)
    {
        if (id > 0)
        {
            $.ajax({
                url: '<?php echo $this->createUrl('site/addfavourite') ?>',
                type: 'POST',
                data: {fid: id},
                success: function (data) {

                },
                error: function (data) {
                    alert('err');
                }
            });
        }
    }

    $(document).ready(function ()
    {
    	
//     	   $(".sort-job").click(function()
// 	        {           
// 	            var sort = $('.sorting').val();
// 	            if(!getUrlParameter('sort') || getUrlParameter('sort') == null)
// 	                window.location.search += '&sort=1';
// 	            else
// 	            {
// 	                var t =  window.location.href;
// 	                t = t.substr(0, t.lastIndexOf("&"));
// 	                t += '&sort='+sort;
// 	                window.location.href = t;
// 	            }
// 	        });
//     	   $(".sort-upload").click(function()
//     		        {           
    		   		
//     		            var time = $('.date_uploaded').val();
//     		            if(!getUrlParameter('time') || getUrlParameter('time') == null)
//     		                window.location.search += '&time=1';
//     		            else
//     		            {
//     		                var t =  window.location.href;
//     		                t = t.substr(0, t.lastIndexOf("&"));
//     		                t += '&time='+time;
//     		                window.location.href = t;
//     		            }
//     		        });

// 			$("#sorting").change(function()
// 	        {         
// 				alert($(this).val());
// 	            var sort = $(this).val();
// 	            var url =  window.location.href;
// 	           alert("sort="+sort);
// 	           alert(getUrlParameter(sort));
// 	            if(!getUrlParameter(sort) || getUrlParameter(sort) == null || getUrlParameter(sort) === "undefined")
// 	            {
// // 	            	alert('nn=='+getUrlParameter(sort));

// 	            	url += '&'+sort+'=1';
// 	                window.location.href = url;
// 	            }
// 	            else
// 	            {
	                //var t =  window.location.href;
// 	                url = url.substr(0, url.lastIndexOf("&"));
	               // alert(url);
// 	                var arr = ['a_z','z_a','recent','old'];
	                
// 	                var url =  window.location.href;
// 	                for(var i = 0; i <= arr.length; i++)
// 	                {
// 	                	url = removeURLParameter(url,arr[i]);
// 	                }
					
// 	                url += '&'+sort+'=1';
// 	                window.location.href = url;
// 	      //     }
// 	        });

        $("#skill_categ").change(function ()
        {
            var id = $(this).val();
            $('#skills').html('');
            $('#skills').attr("disabled",true);
            if (id > 0)
            {
                $.ajax({
                    url: '<?php echo $this->createUrl('site/getSkills') ?>',
                    type: 'POST',
                    data: {id: id},
                    success: function (data) {
                    	$('#skills').attr("disabled",false);
                        $('#skills').html('');
                        $('#skills').append(data);
                        $('#skills').trigger('liszt:updated');
                    },
                    error: function (data) {
                        alert('err');
                    }
                });
            } else
            {
                $('#skills').html('');
                $('#skills').trigger('liszt:updated');
            }
        });

        $("#sorting").change(function()
                { 
                	event.preventDefault();
                	var id =  $("#skill_categ").val();
                	var location =  $("#location").val();
                	var url ="";
                	//alert('asd');
                	$.ajax({
                        url: '<?php echo Yii::app()->createUrl('site/getSkillCategory') ?>',
                        type: 'POST',
                        data: {id: id,loc : location},
                        success: function (data) {                            
                            
                           $( "#job-search-form1" ).attr('action', data).submit();
                        },
                        error: function (data) {
                            alert('err');
                        }
                    });
                
                    	return false;

                });
        
        $("#sendmsg_btn").click(function ()
        {
            $.ajax({
                url: '<?php echo $this->createUrl('site/sendMessage') ?>',
                type: 'POST',
                data: {user_id: $('#user_id').val(), subject: $('#subject').val(), message: $('#message').val()},
                success: function (data) {
                    closepopup();
                },
                error: function (data) {
                    alert('err');
                }
            });
        });

        $("#msg_cancel").click(function ()
        {
            closepopup();
        });		 


       
        $( ".filter-btnn" ).click(function( event ) {  
        	event.preventDefault();
        	var id =  $("#skill_categ").val();
        	var location =  $("#location").val();
        	var url ="";
        	$.ajax({
                url: '<?php echo Yii::app()->createUrl('site/getSkillCategory') ?>',
                type: 'POST',
                data: {id: id,loc : location},
                success: function (data) {
                	 $( "#job-search-form1" ).attr('action', data).submit();
                },
                error: function (data) {
                    alert('err');
                }
            });

            	return false;
        });

    	
        
    });

$(document).on("blur",".location-search",function() {
    		if($(this).val() == "")
    		{
    		 	$("#location_lat").val("");
    		 	$("#location_lng").val("");
    		}
        });


        

</script>
<style>
.showellipse
{
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    height: 25px;
}
</style>
<?php
//$this->widget('ext.EChosen.EChosen', array(
//    'target' => '.select2',
//    'useJQuery' => true,
//    'debug' => true,
//));
?>
<?php 
$disptext = count($model)." job openings";
if($job_skill_category > 0)
{
    $scategmodel = SkillCategories::model()->findByPk($job_skill_category);
    $disptext .= " for ".$scategmodel->name;
}
if($job_location != "")
{
    $disptext .= " in ".$job_location;
}
?>
<div class="page-title text-left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $disptext;?> </h1>
            </div>
        </div>
    </div>
</div>
<div id="page-content">
    <div class="JobSearchResults">
        <?php //$view = $_GET['view_style']; ?>

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'job-search-form1',
            'method' => 'post',
           // 'action' => $this->createUrl('site/searchJobs'),
        ));
        ?>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-lg-3">
                    <div class="JobListFilters">
                    	 <?php
                             $form = $this->beginWidget('CActiveForm', array(
                                 'id' => 'job-filter-form',
                                 'method'=>'post',
                                // 'action' => $this->createUrl('site/searchJobs'),
                                 'enableAjaxValidation' => false
                             ));
                         ?>
                        <h4>Your Search</h4>
                        
                         <div class="form-group">
                            <label for="name" class="control-label">Job Title</label>
                            <?php
                            echo CHtml::textField('name',(isset($job_name) ? $job_name : ""), array('class' => 'form-control'));
                            ?>
                        </div>
                        
                        <div class="form-group">
                            <label for="category_id" class="control-label">Search  Category</label>
                            <?php
                             $cri = new CDbCriteria();
                                                $cri->order = "name";
                                                $skillcategmodel = SkillCategories::model()->findAll($cri);
                            $skillcateglist = CHtml::listData($skillcategmodel, 'id', 'name');
                         //   echo CHtml::label('Search Category', '', array());
                            echo CHtml::dropDownList('skill_category', ($job_skill_category > 0 ? $job_skill_category : ''), $skillcateglist, array('empty' => 'Search by Category', 'id' => "skill_categ", 'class' => 'form-control', 'required' => ""));
                            ?>
                        </div>
                        <div class="form-group">
                            <label for="skills" class="control-label">Required Skills</label>
                            <?php
                            $skilllist = array();
                            if (isset($job_skill_category) && $job_skill_category > 0) {
                                $skillmodel = Skills::model()->findAllByAttributes(array('skill_category_id' => $job_skill_category));
                                $skilllist = CHtml::listData($skillmodel, 'id', 'name');
                            }

                            // echo CHtml::dropDownList('skills[]','',$skilllist,array('empty'=>'Career paths required','id'=>"skills",'class'=>'select2','required'=>"",'multiple'=>'','style'=>'width: 100%'));
                            ?>
                            <select name="skills[]" id="skills" class="form-control select2" multiple=""  data-placeholder="Career paths required">	
                                <?php
                                if (isset($job_skills)) {
                                    foreach ($skilllist as $skill => $skillname) {
                                        ?>
                                        <option value="<?php echo $skill ?>" <?php echo in_array($skill, $job_skills) ? 'selected' : '' ?>><?php echo $skillname ?></option>
                                        <?php
                                    }
                                }
                                ?>	
                            </select> 
                        </div>
                        <div class="form-group">
                            <label for="location" class="control-label">Location</label>
                            <?php
                            echo CHtml::textField('location', $job_location, array('class' => 'form-control location-search','placeholder'=>'Search by Location','id'=>'location'));
                            echo CHtml::hiddenField('latitude', $job_latitude, array('id' => 'location_lat'));
                            echo CHtml::hiddenField('longitude', $job_longitude, array('id' => 'location_lng'));
                            ?>
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="distance" onchange='checkdistancevalidation(this.value)'>
                                <option value="" selected="selected">- Select Distance -</option>
                                <option value="5" <?php echo (isset($job_distance) && $job_distance == 5) ? "selected" : "" ?>>Within 5 miles</option>
                                <option value="10" <?php echo (isset($job_distance) && $job_distance == 10) ? "selected" : "" ?>>Within 10 miles</option>
                                <option value="20" <?php echo (isset($job_distance) && $job_distance == 20) ? "selected" : "" ?>>Within 20 miles</option>
                                <option value="30" <?php echo (isset($job_distance) && $job_distance == 30) ? "selected" : "" ?>>Within 30 miles</option>
                                <option value="40" <?php echo (isset($job_distance) && $job_distance == 40) ? "selected" : "" ?>>Within 40 miles</option>
                                <option value="50" <?php echo (isset($job_distance) && $job_distance == 50) ? "selected" : "" ?>>Within 50 miles</option>
                                <option value="60" <?php echo (isset($job_distance) && $job_distance == 60) ? "selected" : "" ?>>Over 50 miles</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="salary" class="control-label">Salary</label>
                            <ul class="FilterRadio list-unstyled">
                                <li>
                                    <label for="salary0">
                                        <input id="salary0" name="salary" type="radio" value="10000" <?php echo (isset($job_salary) && $job_salary == "10000") ? "checked" : "" ?>>
                                        at least &pound;10,000
                                    </label>
                                </li>
                                <li>
                                    <label for="salary1">
                                        <input id="salary1" name="salary" type="radio" value="20000" <?php echo (isset($job_salary) && $job_salary == "20000") ? "checked" : "" ?>>
                                        at least &pound;20,000
                                    </label>
                                </li>
                                <li>
                                    <label for="salary2">
                                        <input id="salary2" name="salary" type="radio" value="30000" <?php echo (isset($job_salary) && $job_salary == "30000") ? "checked" : "" ?>>
                                        at least &pound;30,000
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="form-group">
                            <label for="type" class="control-label">Job Type</label>
                            <ul class="FilterRadio list-unstyled">
                                <li>
                                    <label for="jobtype0">
                                        <input id="jobtype0" name="jobtype" type="radio" value="Permanent" <?php echo ($job_jobtype == "Permanent" ? "checked" : "") ?>> Permanent
                                    </label>
                                </li>
                                <li>
                                    <label for="jobtype2">
                                        <input id="jobtype2" name="jobtype" type="radio" value="Temporary" <?php echo ($job_jobtype == "Temporary" ? "checked" : "") ?>> Temporary
                                    </label>
                                </li>
                                <li>
                                    <label for="jobtype3">
                                        <input id="jobtype3" name="jobtype" type="radio" value="Contract" <?php echo ($job_jobtype == "Contract" ? "checked" : "") ?>> Contract
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn main-btn btn-block filter-btnn">Filter</button>
                        </div>
                          <?php $this->endWidget(); ?>          
                    </div>      
                   
                </div>
                <div class="col-md-8 col-lg-9">
                    <div class="JobSearchMeta clearfix">
                        <div class="JobSearchMeta__page-info pull-left">
                            <h4><?php echo count($model)?> Jobs</h4>
                        </div>
                    </div>
                    
 					<!-- <div class="clearfix">
                      <div class="pull-right" style="margin-top: 10px">
                                <input type="hidden" name="sorting" class="sorting" value="<?php //echo isset($_GET['sort']) ? ($_GET['sort'] == 1 ? 0 : 1) : 1 ?>">
                                <p><a href="#" class="sort-job"><?php //echo (isset($_GET['sort']) ? ($_GET['sort'] == 1 ? "A-Z <i class='fa fa-long-arrow-down'></i>" : "Z-A <i class='fa fa-long-arrow-up'></i>") : "A-Z"); ?></a></p>
                        </div>
                 
                      <div class="pull-right" style="margin: 10px">
                                <input type="hidden" name="date_uploaded" class="date_uploaded" value="<?php //echo isset($_GET['time']) ? ($_GET['time'] == 1 ? 0 : 1) : 1 ?>">
                                <p><a href="#" class="sort-upload"><?php //echo (isset($_GET['time']) ? ($_GET['time'] == 1 ? "Old" : "Recent") : "Recent"); ?></a></p>
                        </div>
                     </div> -->
                     <div class="clearfix">
                          <div class="pull-right" style="margin-top: 10px">
                          		<select name="sorting" id="sorting" class="form-control">
                          			
                          			<option value="recent" <?php echo ($job_recent == 1 ? "selected" : "selected" ) ?>>Recent Jobs</option>
                          			<option value="old" <?php echo ($job_old == 1 ? "selected" : "" ) ?>>Old Jobs</option>
                          			<option value="a_z" <?php echo ($job_a_z == 1 ? "selected" : "" ) ?>>Job (A-Z)</option>
                          			<option value="z_a" <?php echo ($job_z_a == 1 ? "selected" : "" ) ?>>Job (Z-A)</option>
                          		</select>
                          </div>
                     </div>
                    <div class="JobList">
                      <?php  
                            foreach ($model as $job){
                                
                               if($job['owner_type'] == "agency")
                                {
                                    $empmodel = Agency::model()->findByPk($job['owner_id']);
                                    $type = "agency";
                                }
                                else
                                {
                                    $empmodel = Employers::model()->findByPk($job['owner_id']);
                                    $type = "employer";
                                }
                                $cri = new CDbCriteria();
                              //  $cri->condition = "owner_id =".$empmodel->id." and type ='employer'";
                                $cri->condition = "owner_id =".$empmodel->id." and type like '%".$type."%'";
                                $profilemodel = Profiles::model()->find($cri); 
                                $src = "";
                                if($profilemodel['photo_id'] > 0)
                                {
                                    $mediamodel = Media::model()->findByPk($profilemodel['photo_id']);
                                    $src = Yii::app()->baseUrl."/images/media/".$profilemodel['photo_id']."/".$mediamodel->file_name;
                                }
								 else 
                                    $src = Yii::app()->baseUrl."/images/no-image-".$empmodel->id.".png";
                          ?>
                            <div class="JobItem JobItem--list clearfix">
                                <div class="row">
                                    <div class="col-sm-3 col-sm-push-9">
                                        <img src="<?php echo $src ?>" class="img-responsive" style="height: 100%;width: 100%;max-height: 140px;max-width: 140px;">
                                    </div>
                                    <div class="col-sm-9 col-sm-pull-3">
                                        <div class="JobItem__title">
                                            <a href="<?php echo urldecode($this->createUrl('jobs/viewJob',array('id'=>$job['id'],'title'=>  str_replace('/','-',str_replace(' ', '-', $job['title']))))) ?>"><?php echo $job['title'] ?></a>
                                        </div>
                                        <ul class="JobItem__details">
                                          <li class="JobItem__salary">
                                             <?php if($job['salary_min'] <= 0 && $job['salary_max'] > 0) { 
                                             echo Yii::app()->numberFormatter->formatCurrency($job['salary_max'], 'GBP')." ".($job['salary_type'] == "annual" ? "annum" : "hour");
                                             }else if($job['salary_max'] <= 0 && $job['salary_min'] > 0) {
                                             echo Yii::app()->numberFormatter->formatCurrency($job['salary_min'], 'GBP')." ".($job['salary_type'] == "annual" ? "annum" : "hour");
                                             }
                                             else if($job['competitive_salary'] > 0) {
                                                 echo "Competitive Salary";
                                             } 
                                             else if($job['salary_max'] <= 0 && $job['salary_min'] <= 0) {
                                                echo "Not mentioned";
                                             }                                              
                                             else {
                                             echo Yii::app()->numberFormatter->formatCurrency($job['salary_min'], 'GBP')." to ".Yii::app()->numberFormatter->formatCurrency($job['salary_max'], 'GBP')." per ".($job['salary_type'] == "annual" ? "annum" : "hour")?>
                            	            <?php } ?>
                                          </li>
                                          <li class="JobItem__location"><?php echo $job['location'] ?></li>                                
                                            
                                          <li class="JobItem__posted-date"><?php echo timeAgo($job['created_at']) ?> </li>
                                            
                                        </ul>
                                    </div>
                                </div>
                                <div class="row">
                                  <div class="col-sm-12 showellipse">
									
											<?php 
											  echo $job['description'];
											?>
											
                                        
                                    </div>
                                </div>
                            </div>
                          <?php  } ?>
                    </div>

                    <div class="CandidatePager">

                    </div>
                </div>
            </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>



<?php
$this->beginWidget(
        'booster.widgets.TbModal', array('id' => 'msgdialog')
);
?>
<div class="modal-header">
    <a class="close" data-dismiss="modal" onclick='closepopup()'>&times;</a>
    <h4 class="msg-title">Send Message</h4>
</div>

<div class="modal-body clearfix">
    <div class="col-sm-12">
        <div class="formBlk">
            <div class="formRow">
                <?php
                echo CHtml::hiddenField('user_id', '', array('id' => 'user_id'));
                echo CHtml::label('Subject', '', array());
                echo CHtml::textField('subject', '', array('class' => 'textBox', 'id' => 'subject', 'required' => 'required'));
                ?>
            </div>
        </div>
        <div class="formBlk">
            <div class="formRow">
                <?php
                echo CHtml::label('Message', '', array());
                echo CHtml::textArea('message', '', array('class' => 'textArea', 'id' => 'message', 'required' => 'required'));
                ?> 
            </div>
        </div>

    </div>
</div>

<div class="modal-footer">
    <?php
    echo CHtml::button('Send', array("class" => "btn btn-default", "id" => "sendmsg_btn"));
    echo CHtml::button('Cancel', array("class" => "btn btn-default", "id" => "msg_cancel"));
    ?>
</div>

<script>
function checkdistancevalidation(val){
    
    var loc = $('#location').val();
    
//    alert(val);
    
    if(val != '')
    {
        if(loc === '')
            $('#location').css("border", "1px solid red");
        else
            $('#location').css("border", "1px solid #ccc");
    }
    else{
        $('#location').css("border", "1px solid #ccc");
    }
    
    
}</script>

<?php $this->endWidget(); ?>

<?php 

function timeAgo($time_ago)
{
    $time_ago = strtotime($time_ago);
    $cur_time   = time();
    $time_elapsed   = $cur_time - $time_ago;
    $seconds    = $time_elapsed ;
    $minutes    = round($time_elapsed / 60 );
    $hours      = round($time_elapsed / 3600);
    $days       = round($time_elapsed / 86400 );
    $weeks      = round($time_elapsed / 604800);
    $months     = round($time_elapsed / 2600640 );
    $years      = round($time_elapsed / 31207680 );
    // Seconds
    if($seconds <= 60){
        return "just now";
    }
    //Minutes
    else if($minutes <=60){
        if($minutes==1){
            return "one minute ago";
        }
        else{
            return "$minutes minutes ago";
        }
    }
    //Hours
    else if($hours <=24){
        if($hours==1){
            return "an hour ago";
        }else{
            return "$hours hrs ago";
        }
    }
    //Days
    else if($days <= 7){
        if($days==1){
            return "yesterday";
        }else{
            return "$days days ago";
        }
    }
    //Weeks
    else if($weeks <= 4.3){
        if($weeks==1){
            return "a week ago";
        }else{
            return "$weeks weeks ago";
        }
    }
    //Months
    else if($months <=12){
        if($months==1){
            return "a month ago";
        }else{
            return "$months months ago";
        }
    }
    //Years
    else{
        if($years==1){
            return "one year ago";
        }else{
            return "$years years ago";
        }
    }
}

/* $this->beginWidget('zii.widgets.jui.CJuiDialog',array(
  'id'=>'sendmsgdialog',
  // additional javascript options for the dialog plugin
  'options'=>array(
  'title'=>'Send Message',
  'autoOpen'=>false
  ),
  ));?>

  <div class="formBlk">
  <div class="formRow">
  <?php
  echo CHtml::hiddenField('user_id','',array('id'=>'user_id'));
  echo CHtml::label('Subject', '',array());
  echo CHtml::textField('subject','',array('class'=>'textBox','id'=>'subject','required'=>'required'));
  ?>
  </div>
  </div>
  <div class="formBlk">
  <div class="formRow">
  <?php
  echo CHtml::label('Message', '',array());
  echo CHtml::textArea('message','',array('class'=>'textArea','id'=>'message','required'=>'required'));
  ?>
  </div>
  </div>
  <div class="formBlk">
  <?php
  echo CHtml::button('Send',array("class"=>"btn-search","id"=>"sendmsg_btn"));
  echo CHtml::button('Cancel',array("class"=>"btn-search","id"=>"msg_cancel"));
  ?>
  </div>
  <?php $this->endWidget('zii.widgets.jui.CJuiDialog'); */ ?>