<?php
/* @var $this SiteController */
?>
<script type="text/javascript">
$(document).ready(function(){
   
        var link = "<?php echo Yii::app()->baseUrl.'/site/top-tips'; ?>";
        $('a.btn').attr('href',link);
    
});
</script>

<div id="page-content">
<?php 
  if($model->background_id > 0)
  {
      $mediamodel = Media::model()->findByPk($model->background_id);
      if($mediamodel != null && $mediamodel->file_name != null)
      {
          $src = Yii::app()->baseUrl.'/images/media/'.$mediamodel->id.'/'.$mediamodel->file_name; ?>
    <div class="fullscreen" style="background-image: url('<?php echo $src; ?>'); background-position: <?php echo $model->background_x; ?> <?php echo $model->background_x; ?>;">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">

                    <div class="Slide__content">
                        <div class="PageSlideshow">
                            <div class="PageSlideshow__slides">
                                <?php
                                foreach ($slidemodel as $slide) {
                                    ?>
                                    <div class="PageSlideshow__slide">
                                        <?php echo $slide['body']; ?>
                                    </div>
                                <?php } ?>

                            </div>
                            <div class="PageSlideshow__controls" style="margin-top: 20px;">
                                <a href="#" class="PageSlideshow__nav-btn PageSlideshow__prev"><i class="fa fa-chevron-left"></i></a>
                            <span class="PageSlideshow__pages">
                                <span class="PageSlideshow__current-page">1</span>/<span class="PageSlideshow__count">4</span>
                            </span>
                                <a href="#" class="PageSlideshow__nav-btn PageSlideshow__next"><i class="fa fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
  <?php } } else { ?>
    
    <div class="Slideshow FullscreenSlideshow" data-autoplay="true" data-autoplay-speed="4000" data-dots="false" data-arrows="false">
        <?php
        if (isset($slidemodel)){
        $cnt = 0;
        foreach ($slidemodel as $slide) { 
           $mediamodel = Media::model()->findByPk($slide['media_id']);  
        ?>
        <div class="Slide" style="background-image: url('<?php echo Yii::app()->baseUrl . "/images/media/" . $mediamodel->id . "/" . $mediamodel->file_name; ?>'); background-position: <?php echo $slide['background_x']; ?> <?php echo $slide['background_y']; ?>;">
            <div class="container">
                <div class="Slide__inner">
                    <div class="Slide__caption">
                       <?php echo $slide['body']; ?>
                        <a href="<?php echo Yii::app()->createUrl('site/about'); ?>" class="btn btn-primary">Learn More</a>
                        <a href="#" class="view-showcase btn btn-primary">View CV Video Showcase</a>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php } ?>
    </div>
      <?php }  ?>
</div>


