<?php
/* @var $this SiteController */
?>
<div class="site-content">
    <div class="page-title text-left">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1>   
                        Choose your Employer membership
                    </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content">
        
        <div class="price-tables-container">
        <div class="container">
            <div class="row">
                <div class="col-sm-offset-1 col-sm-5 col-md-offset-2 col-md-4">
                    <div class="subscription-plan">
                        <div class="subscription-plan-header">
                            <h2 class="plan-title">Basic</h2>
                            <div class="plan-price">FREE</div>
                        </div>
                        <div class="subscription-plan-content">
                            <div class="subscription-plan-inner">
                                <div class="plan-description">
                                    <div class="plan-description">
                                        Enjoy the basic features of the CVVid platform.
                                    </div>
                                    <div class="plan-features">
                                        <ul class="fa-ul">
                                            <li><i class="fa fa-check"></i>Create an Employer profile</li>
                                            <li><i class="fa fa-check"></i>Search for suitable candidates by qualification or geography</li>
                                            <li><i class="fa fa-check"></i>View which potential candidates have visited your company profile</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="plan-button">
                                    <a href="<?php echo $this->createUrl('site/basicregisteremployer'); ?>" class="btn hollow-btn">Basic Account</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- .col-sm-4 -->
                <div class="col-sm-5 col-md-4">
                    <div class="subscription-plan premium">
                        <div class="subscription-plan-header">
                            <h2 class="plan-title">Premium</h2>
                            <div class="plan-price">
                              <?php if (Membership::freeTrailEnabled()){ ?>
                                    £450.00
                                <?php }else{ ?>
                                    <small>From</small> £50.00
                               <?php } ?>
                            </div>
                        </div>
                        <div class="subscription-plan-content">
                            <div class="subscription-plan-inner">
                                <div class="plan-description">
                                    Enjoy the full benefits of the CVVid platform.
                                </div>
                                <div class="plan-features">
                                    <div>All Basic features plus</div>
                                    <ul class="fa-ul">
                                        <li><i class="fa fa-check"></i>Search and contact suitable candidates </li>
                                        <li><i class="fa fa-check"></i>Upload unlimited number of job vacancies</li>
                                        <li><i class="fa fa-check"></i>Benchmark salary levels across different industries</li>
                                    </ul>
                                </div>

                               <?php if (Membership::freeTrailEnabled()){ ?>
                                    <div class="free-trial plan-button">
                                        <a href="<?php echo $this->createUrl('site/premiumregisteremployer'); ?>" class="btn btn-danger">
                                            Premium Account
                                        </a>
                                        <br/>
                                        <small>for a limited time only</small>
                                    </div>
                               <?php }else{ ?>
                                    <div class="plan-button">
                                        <a href="<?php echo $this->createUrl('site/premiumregisteremployer'); ?>" class="btn default-btn">Premium Account</a>
                                    </div>
                                <?php } ?>

                            </div>
                        </div>
                    </div>
                </div><!-- .col-sm-4 -->
            </div>
        </div><!-- .container -->
    </div><!-- .price-tables -->
    </div>
</div>