<?php
/* @var $this SiteController */
?>
<div class="site-content">
    <div class="page-title text-left">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1>   
                        REGISTER AS PREMIUM JOB SEEKER
                    </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content">
        
        <div class="container">
        <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'employer-form',
                'action' => $this->createUrl('site/createCandidatePremium'),
                'enableAjaxValidation' => false,
                'htmlOptions' => array('enctype' => 'multipart/form-data')
            ));
            ?>
            
<?php echo  $form->errorSummary(array($model,$profilemodel,$addressmodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
        <div class="row">
            <div class="col-md-offset-1 col-md-10">

                <div class="RegisterAccordion" id="registerAccordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#registerAccordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <span>Your details</span> <i class="fa fa-chevron-right"></i>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                               <?php $this->renderPartial('_candidatedetails', array('model' => $model,'addressmodel'=>$addressmodel,'profilemodel'=>$profilemodel,'form' => $form));  ?>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#registerAccordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                    <span>Your subscription</span> <i class="fa fa-chevron-right"></i>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                <?php if (Membership::freeTrailEnabled()) { ?>
                                    <div class="row">
                                        <div class="col-md-offset-1 col-md-10">
                                            <div class="row">
                                              <?php if (Membership::freeTrailEnabled()) { ?>
                                                <div class="col-md-12">
                                                    <div class="free-option subscription-option selected">
                                                        <div class="option-title">Annual Subscription</div>
                                                        <div class="option-price">£0.00</div>
                                                        <div class="option-description"><span class="save-100">Save 100%</span></div>
                                                        <input type="radio" name="subscription_plan" value="0" checked/>
                                                         <?php echo CHtml::submitButton('CONFIRM', array("class" => "btn btn-success",'style'=>'')); ?>
                                                    </div>
                                                </div>
                                              <?php }  ?>

                                            </div>
                                        </div>
                                    </div><!-- .row -->
                                <?php } else { ?>
                                  <div class="row">
                                      <div class="col-md-offset-1 col-md-10">
                                              <div class="payment-details-container">

                                                  <div class="row">
                                                      <div class="col-md-offset-2 col-md-8">

                                                          <span class="payment-errors"></span>

                                                          <div class="row">
                                                              <div class="col-sm-12">
                                                                  <div class="selected-subscription">
                                                                      <div class="selected-subscription-price"></div>
                                                                  </div>
                                                              </div>
                                                          </div>

                                                          <div class="row">
                                                              <div class="col-sm-12">
                                                                  <div class="form-group">
                                                                      <div class="input-view">
                                                                          <input type="tel" size="20" class="form-control cc-name"
                                                                                 placeholder="Cardholder Name" autocomplete="off"/>
                                                                          <span class="card-icon"></span>
                                                                          <div class="view-icon">
                                                                              <i class="fa fa-pencil-square-o"></i>
                                                                          </div>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div><!-- .row -->

                                                          <div class="row">
                                                              <div class="col-sm-12">
                                                                  <div class="form-group">
                                                                      <div class="input-view">
                                                                          <input type="tel" size="20" class="form-control cc-number" data-stripe="number"
                                                                                 placeholder="Card Number" autocomplete="off"/>
                                                                          <span class="card-icon"></span>
                                                                          <div class="view-icon">
                                                                              <i class="fa fa-credit-card"></i>
                                                                          </div>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div><!-- .row -->

                                                          <div class="row">
                                                              <div class="col-xs-7">
                                                                  <div class="form-group form-expiration">
                                                                      <span>Expiry</span>
                                                                      <input id="cc-exp" type="tel" size="10" class="form-control cc-exp" name="card_expiry" autocomplete="cc-exp"
                                                                             placeholder="•• / ••" required>
                                                                  </div>
                                                              </div>
                                                              <div class="col-xs-5">
                                                                  <div class="form-group">
                                                                      <div class="input-view  cvc-input">
                                                                          <input type="tel" size="4" class="form-control cc-cvc" data-stripe="cvc"
                                                                                autocomplete="off"/>
<!--                                                                           <div class="view-icon"> -->
<!--                                                                               <i class="fa fa-lock"></i> -->
<!--                                                                           </div> -->
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                          </div>

                                                          <div class="row">
                                                              <div class="col-md-12">
                                                                  <button type="submit" class="submit-button btn btn-block main-btn">Submit Payment</button>
                                                              </div>
                                                          </div>

                                                      </div>
                                                  </div><!-- .row -->

                                              </div><!-- .payment-details-container -->
                                          </div>
                                      </div><!-- .row -->
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
       <?php $this->endWidget(); ?>
    </div>
        
     <script type="text/javascript">
       $(function(){
            new CandidateSlugGenerator();
        });
    </script>   