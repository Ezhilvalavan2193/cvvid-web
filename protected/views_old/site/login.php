<?php
/* @var $this SiteController /
  / @var $model LoginForm /
  / @var $form CActiveForm */
?>
<script>
$(document).ready(function()
{
	$(".register").click(function(){
		$('#RegisterModal').addClass('in');
        $('body').addClass('modal-open');
        $('#RegisterModal').show();
	});
	
	$(".register_apply").click(function(){
		
		if($('#register_sel').val() == "candidate")
			window.location = '<?php echo Yii::app()->createUrl('site/candidatememberships')?>';
		else if($('#register_sel').val() == "employer")
			window.location = '<?php echo Yii::app()->createUrl('site/employermemberships')?>';
		else
			window.location = '<?php echo Yii::app()->createUrl('site/basicagency')?>';
	});
});	
</script>
<div class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
				<h1>Log in</h1>
            </div>
        </div>
    </div>
</div>
<div id="page-content">
    <div class="container">

        <div class="login-container">
            <?php if (Yii::app()->user->hasFlash('success')): ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php echo Yii::app()->user->getFlash('success'); ?>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-sm-6">
                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'login-form',
                        'enableClientValidation' => true,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                        ),
                    ));
                    ?>
                    <?php echo $form->errorSummary(array($model), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>', '', array('class' => 'alert alert-danger')); ?>
                    <h5>Login</h5>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <?php echo $form->textField($model, 'username', array('class' => 'form-control', 'id' => 'email', 'placeholder' => 'E-mail Address')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <?php echo $form->passwordField($model, 'password', array('class' => 'form-control', 'id' => 'password', 'placeholder' => 'Password')); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <?php echo CHtml::submitButton('Login', array('class' => 'main-btn btn-block')); ?>
                    </div>

                    <div class="login-links">
                        <a href="<?php echo Yii::app()->createUrl('site/password') ?>">Forgot your password?</a>
                        <a href="#" class="register">Register</a>
<!--                         <a href="#">Need Help?</a> -->
                    </div>

                    <?php $this->endWidget(); ?>
                </div>
                <div class="col-sm-6">

                </div>
            </div>
        </div><!-- .login-container -->
    </div><!-- .container -->
</div>

<div id="RegisterModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closepopup()"><span aria-hidden="true">Ã—</span></button>
                    <h4 class="modal-title">Register For</h4>
                </div>
                <div class="modal-body">
                    <form class="MessageForm">
                    	<div class="form-group">              
                    		<select class="form-control" name="register" id="register_sel">
                                <option value="candidate">Candidate</option>
                                <option value="employer">Employer</option>
                                <option value="agency">Agency</option>
                            </select>
                        </div>
                                              
                    </form>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default-btn" data-dismiss="modal" onclick="closepopup()">Cancel</button>
                    <button type="button" class="SendMessageBtn btn main-btn register_apply">Ok</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
</div>