<?php
/* @var $this SiteController */
?>
<?php

use Stripe\Stripe;
use Stripe\Invoice;

require Yii::app()->basePath . '/extensions/stripe/init.php';
Stripe::setApiKey(Yii::app()->params['SECRET_Key']);
?>
<div class="site-content">
    <div class="page-title text-left">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1>   
                        REGISTER AS PREMIUM EMPLOYER
                    </h1>
                </div>
            </div>
        </div>
    </div>


    <div id="page-content">
        <div class="container">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'employer-form',
                'action' => $this->createUrl('site/createEmployerPremium'),
                'enableAjaxValidation' => false,
                'htmlOptions' => array('enctype' => 'multipart/form-data', 'class' => 'payment-form')
            ));
            ?>
            <?php echo $form->errorSummary(array($model, $usermodel, $addressmodel, $profilemodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
            <div class="row">
                <div class="col-md-offset-1 col-md-10">

                    <div class="RegisterAccordion" id="registerAccordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#registerAccordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <span>Company details</span> <i class="fa fa-chevron-right"></i>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <?php $this->renderPartial('_employerdetails', array('model' => $model, 'usermodel' => $usermodel, 'addressmodel' => $addressmodel, 'profilemodel' => $profilemodel, 'form' => $form)); ?>


                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#registerAccordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                        <span>Your subscription</span> <i class="fa fa-chevron-right"></i>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="payment-form-container">
                                                <div class="payment-inner">

                                                    <div class="row">
                                                        <div class="col-md-offset-1 col-md-12">
                                                            <div class="row">
                                                                <?php
                                                                if(strtolower(ip_visitor_country()) == "gb")
                                                                {
                                                                    $monplans[] = Membership::plan('Employer Per Vacancy Subscription', 'employer_vacancy')
                                                                            ->user_type('employer')
                                                                            ->price(45)
                                                                            // ->price_suffix('+ VAT')
                                                                            ->description('+ VAT 20%')
                                                                            //->monthly()
                                                                            ->permissions([
                                                                        'post_jobs' => 'Post Jobs',
                                                                        'send_messages' => 'Send messages',
                                                                    ]);
                                                                }
                                                                else {
                                                                    $monplans[] = Membership::plan('Employer Per Vacancy Subscription', 'employer_vacancy_others')
                                                                    ->user_type('employer')
                                                                    ->price(54)
                                                                    // ->price_suffix('+ VAT')
                                                                    //->description('+ VAT 20%')
                                                                    //->monthly()
                                                                    ->permissions([
                                                                        'post_jobs' => 'Post Jobs',
                                                                        'send_messages' => 'Send messages',
                                                                    ]);
                                                                }
                                                                // echo var_dump($plans);
                                                                foreach ($monplans as $plan) {
                                                                    ?>
                                                                    <div class="col-md-6">
                                                                        <div class="subscription-option <?php echo ($plan->id == 'employer_vacancy' || $plan->id == 'employer_vacancy_others' ? 'selected' : ''); ?>">
                                                                            <div class="option-title"><?php echo $plan->name ?></div>
                                                                            <div class="option-price">£ <?php echo $plan->price ?><small><?php echo $plan->price_suffix ?></small></div>
                                                                            <div class="option-description"><?php echo $plan->description ?></div>
                                                                            <input type="radio" name="subscription_plan" value="<?php echo $plan->id ?>"
                                                                            <?php echo ($plan->id == 'employer_vacancy' || $plan->id == "employer_vacancy_others" ? 'checked' : ''); ?>/>
                                                                        </div>
                                                                    </div>
                                                                <?php } ?>

                                                                <?php
                                                                if(strtolower(ip_visitor_country()) == "gb")
                                                                {
                                                                    $annplans[] = Membership::plan('Employer Annual Subscription', 'employer_annual')
                                                                            ->user_type('employer')
                                                                            ->price(450)
                                                                            // ->price_suffix('per year')
                                                                            ->description('+ VAT 20%')
                                                                            ->permissions([
                                                                        'post_jobs' => 'Post Jobs',
                                                                        'send_messages' => 'Send messages',
                                                                    ]);
                                                                }
                                                                else {
                                                                    $annplans[] = Membership::plan('Employer Annual Subscription', 'employer_annual_others')
                                                                    ->user_type('employer')
                                                                    ->price(540)
                                                                    // ->price_suffix('per year')
                                                                   // ->description('+ VAT 20%')
                                                                    ->permissions([
                                                                        'post_jobs' => 'Post Jobs',
                                                                        'send_messages' => 'Send messages',
                                                                    ]);
                                                                }
                                                                // echo var_dump($plans);
                                                                foreach ($annplans as $plan) {
                                                                    ?>
                                                                    <div class="col-md-6">
                                                                        <div class="subscription-option">
                                                                            <div class="option-title"><?php echo $plan->name ?></div>
                                                                            <div class="option-price">£ <?php echo $plan->price ?><small><?php echo $plan->price_suffix ?></small></div>
                                                                            <div class="option-description"><?php echo $plan->description ?></div>
                                                                            <input type="radio" name="subscription_plan" value="<?php echo $plan->id ?>"/>
                                                                        </div>
                                                                    </div>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div><!-- .row -->
                                                    <div class="row" style="padding-bottom: 20px;width: 83%;margin: auto;">
                                                        <input type="hidden" name="amount" id="amount" value="">
                                                        <div class="no_vcancy">
                                                            <label class="control-label">Select vacancy</label>
                                                            <select id="vacancy_count" class="form-control" name="vacancy_count" style="width: 80%" onchange="onchangeVacancy(this.value)">
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                            </select>
                                                        </div>
                                                        <div class="coupon_wrap">
                                                            <label class="control-label"> Coupon Code: </label>
                                                            <input class="form-control" style="display: inline-block;width: 75%;margin-bottom:5px" type=text size="6" id="coupon" placeholder="Enter a Coupon" name="coupon_id" />
                                                            <button class="btn main-btn" type="button" id="coupon_check">Apply</button>
                                                            <input type="hidden" id="validcoupon" name="validcoupon" value="0">
                                                            <input type="hidden" id="percent_off" name="percent_off" value="0">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-offset-1 col-md-12">
                                                            <div class="payment-details-container">
                                                                <div class="row">
                                                                    <div class="col-md-offset-2 col-md-8">
                                                                        <span class="payment-errors"></span>
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <div class="selected-subscription">
                                                                                    <div class="selected-subscription-price"></div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <div class="form-group">
                                                                                    <div class="input-view">
                                                                                        <input type="tel" size="20" class="form-control cc-name"
                                                                                               placeholder="Cardholder Name" autocomplete="off"/>
                                                                                        <span class="card-icon"></span>
                                                                                        <div class="view-icon">
                                                                                            <i class="fa fa-pencil-square-o"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div><!-- .row -->

                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <div class="form-group">
                                                                                    <div class="input-view">
                                                                                        <input type="tel" size="20" class="form-control cc-number" data-stripe="number"
                                                                                               placeholder="Card Number" autocomplete="off"/>
                                                                                        <span class="card-icon"></span>
                                                                                        <div class="view-icon">
                                                                                            <i class="fa fa-credit-card"></i>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div><!-- .row -->

                                                                        <div class="row">
                                                                            <div class="col-xs-7">
                                                                                <div class="form-group form-expiration">
                                                                                    <span>Expiry</span>
                                                                                    <input id="cc-exp" type="tel" size="10" class="form-control cc-exp" name="card_expiry" autocomplete="cc-exp"
                                                                                           placeholder="•• / ••" required>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-5">
                                                                                <div class="form-group">
                                                                                    <div class="input-view cvc-input">
                                                                                        <span>CVC</span>
                                                                                        <input type="tel" size="4" class="form-control cc-cvc" data-stripe="cvc"
                                                                                               autocomplete="off"/>
                                                                                        <!--                                                                 <div class="view-icon"> -->
                                                                                        <!--                                                                     <i class="fa fa-lock"></i> -->
                                                                                        <!--                                                                 </div> -->
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <button type="submit" class="submit-button btn btn-block main-btn">Submit Payment</button>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div><!-- .row -->

                                                            </div><!-- .payment-details-container -->
                                                        </div>
                                                    </div><!-- .row -->


                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
<?php 
function ip_visitor_country()
{
    
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];
    $country  = "Unknown";
    
    if(filter_var($client, FILTER_VALIDATE_IP))
    {
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP))
    {
        $ip = $forward;
    }
    else
    {
        $ip = $remote;
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "http://www.geoplugin.net/json.gp?ip=".$ip);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    $ip_data_in = curl_exec($ch); // string
    curl_close($ch);
    
    $ip_data = json_decode($ip_data_in,true);
    $ip_data = str_replace('&quot;', '"', $ip_data);
    
    if($ip_data && $ip_data['geoplugin_countryCode'] != null) {
        $country = $ip_data['geoplugin_countryCode'];
    }
    
    return $country;
}

?>

    <script type="text/javascript">
        CVPayment.init('<?php echo Yii::app()->params['PUBLIC_Key']; ?>');
        $(function () {
            new EmployerSlugGenerator();
        });

        $(document).ready(function () {
            $('#coupon_check').click(function () {
                 requestData = "coupon_id=" + $('#coupon').val()+"&amount="+$("#amount").val();
                $.ajax({
                    type: "GET",
                    url: "<?php echo $this->createUrl('site/validatecoupon'); ?>",
                    data: requestData,
                    success: function (response) {
                       var obj = $.parseJSON(response);
                        if (obj['status']) {
                            $('#validcoupon').val("1");
                            $("#coupon_check").html("<i class='fa fa-check'></i>");
                            $("#coupon_check").css("background-color","green");
                            $("#coupon_check").css("color","white");
                            $("#coupon_check").attr("disabled", "true");

                            var $selectedOption = $('.subscription-option.selected');
                             
                            var pr = $selectedOption.find('.option-price').html().trim();
    
                            var st = pr.split(" ");
                            
                            var val = $('#vacancy_count').val();
                    
                            var price = parseInt(st[1])* val;
                            
                            var cal = price * (obj['discount'] / 100);
                                                         
                            var offr = price - cal;
                             
                            var totalpr = (offr + (offr*(20/100)));
                             
                            $(".selected-subscription-price").html("&#163;"+(obj['amount']));
                            
                            $("#percent_off").val(obj['discount']);
                             $("#coupon").css("border","1px solid green");
                                                         
                        } else {
                            $('#validcoupon').val("0");
                            $("#coupon_check").html("<i class='fa fa-times' aria-hidden='true'></i>");
                            $("#coupon_check").css("background-color","gray");
                            $("#coupon_check").css("color","white");
                            $("#coupon_check").attr("disabled", "true");
                            $("#coupon").css("border","1px solid red");
                        }
                    }
                });
            });
        });
    </script>
