<?php

/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>
<script>
$(document).ready(function(){
	
	$("#skill_categ").change(function ()
	{
		            var id = $(this).val();
		            if (id > 0)
		            {
		                $.ajax({
		                    url: '<?php echo $this->createUrl('site/getSkills') ?>',
		                    type: 'POST',
		                    data: {id: id},
		                    success: function (data) {
		                        $('#skills').html('');
		                        $('#skills').append(data);
		                        $('#skills').trigger('liszt:updated');
		                    },
		                    error: function (data) {
		                        alert('err');
		                    }
		                });
		            } else
		            {
		                $('#skills').html('');
		                $('#skills').trigger('liszt:updated');
		            }
	 });

});
</script>
<style>
.select2
{
    width: 263px!important;
}
</style>
<?php

$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl('site/alumini'), //Yii::app()->createUrl($this->route),
    'method' => 'get',
        ));
?>
<div class="row">
<div class="form-group">
<?php 
   // echo CHtml::textField('name',(isset($_GET['name']) ? $_GET['name'] : ""), array('class' => 'form-control', 'placeholder' => 'Enter Name'));        
?>
</div>

<div class="form-group">
<?php 
    echo CHtml::dropDownList('gender', (isset($_GET['gender']) ? $_GET['gender'] : ""),array('M'=>"Male",'F'=>"Female"),array('class' => 'form-control', 'empty' => '- Select Gender -'));
?>
</div>

<div class="form-group">
<?php
    echo CHtml::textField('from_age', (isset($_GET['from_age']) ? $_GET['from_age'] : ""), array('class' => 'form-control', 'placeholder' => 'From age'));
    echo " To ";
    echo CHtml::textField('to_age', (isset($_GET['to_age']) ? $_GET['to_age'] : ""), array('class' => 'form-control', 'placeholder' => 'To age'));
?>
</div>
</div>

<div class="row" style="padding-top: 10px">
	<div class="form-group">
<!--             <label for="category_id" class="control-label">Category</label> -->
            <?php
            $skillcategmodel = SkillCategories::model()->findAll();
            $skillcateglist = CHtml::listData($skillcategmodel, 'id', 'name');
          //  echo CHtml::label('Search Category', '', array());
            echo CHtml::dropDownList('skill_category', (isset($_GET['skill_category']) ? $_GET['skill_category'] : ''), $skillcateglist, array('empty' => 'Search by Category', 'id' => "skill_categ", 'class' => 'form-control'));
            ?>
        </div>
   <div class="form-group">
<!--             <label for="skills" class="control-label">Required Career paths</label> -->
            <?php
            $skilllist = array();
            if (isset($_GET['skill_category']) && $_GET['skill_category'] > 0) {
                $skillmodel = Skills::model()->findAllByAttributes(array('skill_category_id' => $_GET['skill_category']));
                $skilllist = CHtml::listData($skillmodel, 'id', 'name');
            }
            ?>

            <select name="skills[]" id="skills" class="form-control select2" multiple="" data-placeholder="Career paths required">	
                <?php
                if (isset($_GET['skills'])) {
                    foreach ($skilllist as $skill => $skillname) {
                        ?>
                        <option value="<?php echo $skill ?>" <?php echo in_array($skill, $_GET['skills']) ? 'selected' : '' ?>><?php echo $skillname ?></option>
                        <?php
                    }
                } 
                
                ?>	
            </select> 
</div>
</div>
<div class="row" style="padding-top: 10px">
<div class="form-group">
<?php 
        $cri = new CDbCriteria();
        $cri->select = "distinct grade";
        $cri->condition = "grade <> null OR grade <> ''";
        $grademodel = UserQualifications::model()->findAll($cri);
       
        $gradelist = CHtml::listData($grademodel, 'grade', 'grade');
        //  echo CHtml::label('Search Category', '', array());
        echo CHtml::dropDownList('grade', (isset($_GET['grade']) ? $_GET['grade'] : ''), $gradelist, array('empty' => 'Search by Grade', 'id' => "grade", 'class' => 'form-control'));

    //echo CHtml::textField('grade',(isset($_GET['grade']) ? $_GET['grade'] : ""), array('class' => 'form-control', 'placeholder' => 'Enter Grade'));        
?>
</div>

<div class="form-group">
<?php
    echo CHtml::textField('from_job_cnt', (isset($_GET['from_job_cnt']) ? $_GET['from_job_cnt'] : ""), array('class' => 'form-control', 'placeholder' => 'Min Job Count'));
    echo " To ";
    echo CHtml::textField('to_job_cnt', (isset($_GET['to_job_cnt']) ? $_GET['to_job_cnt'] : ""), array('class' => 'form-control', 'placeholder' => 'Max Job Count'));
?>
</div>

<div class="form-group">    
    <?php
        echo CHtml::textField('location', (isset($_GET['location']) ? $_GET['location'] : ""), array('class' => 'form-control location-search','placeholder'=>'Location'));
        echo CHtml::hiddenField('latitude', (isset($_GET['latitude']) ? $_GET['latitude'] : ""), array('id' => 'location_lat'));
        echo CHtml::hiddenField('longitude', (isset($_GET['longitude']) ? $_GET['longitude'] : ""), array('id' => 'location_lng'));
    ?>
</div>
</div>

<div class="row" style="padding-top: 10px">
<div class="form-group">
<?php
    echo CHtml::textField('from_job_view_cnt', (isset($_GET['from_job_view_cnt']) ? $_GET['from_job_view_cnt'] : ""), array('class' => 'form-control', 'placeholder' => 'Min Job view Count'));
    echo " To ";
    echo CHtml::textField('to_job_view_cnt', (isset($_GET['to_job_view_cnt']) ? $_GET['to_job_view_cnt'] : ""), array('class' => 'form-control', 'placeholder' => 'Max Job view Count'));
?>
</div>
<div class="form-group">
<?php
    echo CHtml::textField('from_profile_view_cnt', (isset($_GET['from_profile_view_cnt']) ? $_GET['from_profile_view_cnt'] : ""), array('class' => 'form-control', 'placeholder' => 'Min Profile view Count'));
    echo " To ";
    echo CHtml::textField('to_profile_view_cnt', (isset($_GET['to_profile_view_cnt']) ? $_GET['to_profile_view_cnt'] : ""), array('class' => 'form-control', 'placeholder' => 'Max Profile view Count'));
?>
</div>
</div>
<div class="row" style="padding-top: 10px">
<?php
echo CHtml::submitButton('Search', array("class" => "btn btn-primary"));
?>
</div>
<?php $this->endWidget(); ?>

