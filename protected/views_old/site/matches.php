<?php 
/* @var $this FixtureController */
/* @var $model Fixture */
Yii::app()->clientScript->registerScript('save-leagueplayers',"

$('.fixture_link').click(function(e){
		var id = $(this).attr('id');
		if(id > 0)
		{
			 $.ajax
			    ({ 
			        type: 'POST',
			        url: '".$this->createUrl("fixture/checkScoreSheetExist")."',
			        data: {id: id},
			        success: function(data)
			        {
			        	if(data == -1)
			         		alert('Scoresheet not exists!!!');
			         	else
			         		window.location = '".$this->createUrl('fixture/selectPlayers')."/'+id;
			        },
			        error: function(result)
			        {
						
			        }
			    });
		
		}
});

");
?>

<div id="sub-wrapper">
		
		<section id="hero-banner">
			<img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/hero-banner-2.jpg" alt="">
			<div class="banner-title">
                            <h1><?php echo CHtml::label('Matches List','',array('style'=>'')); ?></h1>
				
			</div>
		</section>
    <section id="content-section">
			<div class="content-row">
				<div class="pull-top">
                                    
<div class="middle-wrap col-xs-12">
   <?php 
  $cri = new CDbCriteria();
  $cri->order = 'league_id asc';
 
	if(Yii::app()->user->getState('team_id'))
		 $cri->addCondition('team1 ='.Yii::app()->user->getState('team_id').' or team2 ='.Yii::app()->user->getState('team_id'));
		 
  	$fixture = Fixture::model()->findAll($cri);
  	$leaguearr = array();$leagueids = array();
    foreach ($fixture as $fix)
    {
    	$leagueids[$fix['league_id']] = $fix['league_id'];
    }
    
   foreach ($leagueids as $league)
    {
    	
    	$lea = League::model()->findByPk($league);
    	echo '<div class="summary" style="margin-bottom:20px;font-weight:bold">'.CHtml::label($lea->name,"",array()).'</div>';
    	
    	$crit = new CDbCriteria();
    	$crit->condition = "league_id =".$league;
    	
    	if(Yii::app()->user->getState('team_id'))
		 $crit->addCondition('team1 ='.Yii::app()->user->getState('team_id').' or team2 ='.Yii::app()->user->getState('team_id'));
		 
    	$fixt = Fixture::model()->findAll($crit);
    	echo '<ul class="leg-list list-col">';
    	foreach ($fixt as $f)
    	{
	    	$team1 = Team::model()->findByPk($f['team1']);
	    	$team2 = Team::model()->findByPk($f['team2']);
	    	if(Yii::app()->user->getState('team_id'))
	    		echo CHtml::link($team1->name." Vs ".$team2->name,'#',array('class'=>'fixture_link','id'=>$f['fixture_id'],'style'=>'font-weight:bold;font-size:15px;'));
	    	else
	    	{
	    		
	    		echo '<li style="height: 298px;">';
	    			echo CHtml::link((CHtml::image(Yii::app()->baseUrl.'/images/'.$team1->team_logo,$team1->name,array("style"=>"height:100px;width:100px")). "<span> Vs </span>".CHtml::image(Yii::app()->baseUrl.'/images/'.$team2->team_logo,$team2->name,array("style"=>"height:100px;width:100px"))),$this->createUrl('fixture/selectPlayers',array('id'=>$f['fixture_id'])),array('style'=>'font-weight:bold;font-size:15px;'));
	    		echo '</li>';
	    		
	    	}
	    		//echo CHtml::link((CHtml::image(Yii::app()->baseUrl.'/images/'.$team1->team_logo). " Vs ".CHtml::image(Yii::app()->baseUrl.'/images/'.$team2->team_logo)),$this->createUrl('fixture/selectPlayers',array('id'=>$f['fixture_id'])),array('style'=>'font-weight:bold;font-size:15px;'));
	    		//echo CHtml::link($team1->name." Vs ".$team2->name,$this->createUrl('fixture/selectPlayers',array('id'=>$f['fixture_id'])),array('style'=>'font-weight:bold;font-size:15px;'));	    	
	    	//echo "<br><br>";
    	}
    	echo '</ul>';
    	//echo "<br><br>";
    }
  ?>
   </table>
	</div>

</div></section>		</div>