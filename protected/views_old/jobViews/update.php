<?php
/* @var $this JobViewsController */
/* @var $model JobViews */

$this->breadcrumbs=array(
	'Job Views'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List JobViews', 'url'=>array('index')),
	array('label'=>'Create JobViews', 'url'=>array('create')),
	array('label'=>'View JobViews', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage JobViews', 'url'=>array('admin')),
);
?>

<h1>Update JobViews <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>