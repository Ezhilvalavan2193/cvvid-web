<?php
/* @var $this JobViewsController */
/* @var $model JobViews */

$this->breadcrumbs=array(
	'Job Views'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List JobViews', 'url'=>array('index')),
	array('label'=>'Manage JobViews', 'url'=>array('admin')),
);
?>

<h1>Create JobViews</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>