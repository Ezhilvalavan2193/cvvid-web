<?php
/* @var $this JobViewsController */
/* @var $model JobViews */

$this->breadcrumbs=array(
	'Job Views'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List JobViews', 'url'=>array('index')),
	array('label'=>'Create JobViews', 'url'=>array('create')),
	array('label'=>'Update JobViews', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete JobViews', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage JobViews', 'url'=>array('admin')),
);
?>

<h1>View JobViews #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'job_id',
		'model_type',
		'model_id',
		'view_count',
		'created_at',
		'updated_at',
	),
)); ?>
