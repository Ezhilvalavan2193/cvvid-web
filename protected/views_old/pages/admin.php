<?php
/* @var $this PagesController */
/* @var $model Pages */

$this->breadcrumbs = array(
    'Pages' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#pages-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?><script>
    $(document).ready(function () {

        $('#delete-page').click(function (e) {


            if ($("#pages-grid").find("input:checked").length > 0)
            {

                if (confirm('Are you sure you want to delete these items?')) {

                    var ids = $.fn.yiiGridView.getChecked("pages-grid", "selectedIds");

                    $.ajax({
                        url: '<?php echo $this->createUrl('pages/deletePage') ?>',
                        type: 'POST',
                        data: {ids: ids},
                        success: function (data) {
                            $.fn.yiiGridView.update('pages-grid');
                        },
                        error: function (data) {
                            alert('err');
                        }
                    });
                }

            } else
            {
                alert('Please select at least one item');
            }
        });

    });

</script>
<h1>Manage Pages</h1>
<div class="Admin__content__inner">

    <div class="AdminFilters">
        <div class="form-inline">
            <?php
            $this->renderPartial('_search', array(
                'model' => $model,
            ));
            ?>
            <div class="pull-right">
                <?php
                echo CHtml::button('Create', array("class" => "btn btn-primary", "style" => "", "onclick" => "window.location='" . Yii::app()->createUrl('pages/create', array()) . "'"));
                echo CHtml::button('Delete', array("class" => "btn btn-primary", 'id' => 'delete-page'));
                ?>
            </div>
        </div>
    </div>

    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'pages-grid',
        'itemsCssClass' => 'table',
        'summaryText' => '',
        // 'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'enableSorting' => true,
        'ajaxUpdate' => true,
    'pager' => array(
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'header'=>'',
        'firstPageLabel' => '<<',
        'lastPageLabel' => '>>',
        'nextPageLabel' => '>>',
        'prevPageLabel' => '<<'),
        'ajaxUpdate' => false,
        'dataProvider' => $model->search(),
        //'filter'=>$model,
        'columns' => array(
            array(
                // 'name' => 'check',
                'id' => 'selectedIds',
                'value' => '$data->id',
                'class' => 'CCheckBoxColumn',
                'selectableRows' => 2,
            // 'checked'=>'Yii::app()->user->getState($data->id)',
            // 'checkBoxHtmlOptions' => array('name' => 'idList[]'),
            ),
            array(
                'header' => 'Title',
                'value' => 'CHtml::link("<i class=\"fa fa-pencil\"></i> ".$data->title,Yii::app()->createUrl("pages/update", array("id"=>$data->id)),array("class"=>"page-edit-button","id"=>"page-edit"))',
                'type' => 'raw'
            ),
            'slug',
            'created_at'
        ),
    ));
    ?>
</div>

