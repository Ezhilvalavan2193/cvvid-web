<?php $ownerid = 0;
if($employerid > 0)
{
  $model = Employers::model()->findByPk($employerid);
  if($model->user_id > 0)
  {
      $usermodel = Users::model()->findByPk($model->user_id);
      $ownerid = $employerid;
      $type = $usermodel->type;
  }
}
else if($userid > 0)
{
  $model = Users::model()->findByPk($userid);
  $ownerid = $userid;
  $type = $model->type;
}
else if($institutionid > 0)
{
    $ownerid = $institutionid;
    $type = "institution";
}
else if(isset($agencyid) && $agencyid > 0)
{
    $ownerid = $agencyid;
    $type = "agency";
}
$coverimage = Yii::app()->theme->baseUrl.'/images/CoverPlaceholder.jpg';
if($ownerid > 0)
{
    $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$ownerid,'type'=>$type));
    
    $mediamodel = Media::model()->findByPk($profilemodel->cover_id);
    
    if($profilemodel->cover_id != NULL)
        $coverimage = Yii::app()->baseUrl.'/images/media/'.$profilemodel->cover_id.'/conversions/cover.jpg';
}
?>
<div id="form-cover-photo" style="background-image: url('<?php echo $coverimage; ?>');">
    <?php if (isset($this->edit) && $this->edit) {
        ?>
        <div class="cover-photo-edit">
            <a class="open-media main-btn" data-field="cover" data-size="cover" data-target="#form-cover-photo"><i class="fa fa-camera"></i>Change Cover Photo</a>
        </div>
    <?php } ?>
</div>