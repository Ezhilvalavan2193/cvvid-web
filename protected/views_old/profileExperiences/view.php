<?php
/* @var $this ProfileExperiencesController */
/* @var $model ProfileExperiences */

$this->breadcrumbs=array(
	'Profile Experiences'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ProfileExperiences', 'url'=>array('index')),
	array('label'=>'Create ProfileExperiences', 'url'=>array('create')),
	array('label'=>'Update ProfileExperiences', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ProfileExperiences', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProfileExperiences', 'url'=>array('admin')),
);
?>

<h1>View ProfileExperiences #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'profile_id',
		'user_experience_id',
		'created_at',
		'updated_at',
	),
)); ?>
