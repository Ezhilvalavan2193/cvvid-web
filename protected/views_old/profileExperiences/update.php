<?php
/* @var $this ProfileExperiencesController */
/* @var $model ProfileExperiences */

$this->breadcrumbs=array(
	'Profile Experiences'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProfileExperiences', 'url'=>array('index')),
	array('label'=>'Create ProfileExperiences', 'url'=>array('create')),
	array('label'=>'View ProfileExperiences', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ProfileExperiences', 'url'=>array('admin')),
);
?>

<h1>Update ProfileExperiences <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>