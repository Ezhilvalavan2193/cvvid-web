<?php
/* @var $this ProfileHobbiesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Profile Hobbies',
);

$this->menu=array(
	array('label'=>'Create ProfileHobbies', 'url'=>array('create')),
	array('label'=>'Manage ProfileHobbies', 'url'=>array('admin')),
);
?>

<h1>Profile Hobbies</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
