<?php
/* @var $this ProfileHobbiesController */
/* @var $model ProfileHobbies */

$this->breadcrumbs=array(
	'Profile Hobbies'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ProfileHobbies', 'url'=>array('index')),
	array('label'=>'Create ProfileHobbies', 'url'=>array('create')),
	array('label'=>'Update ProfileHobbies', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ProfileHobbies', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProfileHobbies', 'url'=>array('admin')),
);
?>

<h1>View ProfileHobbies #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'profile_id',
		'user_hobby_id',
		'created_at',
		'updated_at',
	),
)); ?>
