<?php
/* @var $this AgencyStaffController */
/* @var $model AgencyStaff */

$this->breadcrumbs=array(
	'Agency Staff'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AgencyStaff', 'url'=>array('index')),
	array('label'=>'Manage AgencyStaff', 'url'=>array('admin')),
);
?>

<h1>Create AgencyStaff</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>