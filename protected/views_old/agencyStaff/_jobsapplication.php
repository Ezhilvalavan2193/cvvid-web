<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs = array(
    'APPLICATIONS' => array('index'),
    'Manage',
);
?>
<script>

function changeStatus(val, appilication_id){
    
     var value = val;
     var application_id = appilication_id;
      
     if(value == 'reject')
     {
            $('#RejectModal').addClass('in');
            $('body').addClass('modal-open');
            $('#RejectModal').show();

        } else if (value == 'invite') {


            $('#InterviewModal').addClass('in');
            $('body').addClass('modal-open');
            $('#InterviewModal').show();



        } else if (value == 'accept') {

            $('#AcceptModal').addClass('in');
            $('body').addClass('modal-open');
            $('#AcceptModal').show();


        } else {

            $.ajax({
                url: '<?php echo $this->createUrl('jobApplications/updateStatus') ?>',
                type: 'POST',
                data: {application_id: application_id, value: value},
                success: function (response) {
                    //alert(response);
                    location.reload();
                },
//                error: function (data) {
//                    alert('err');
//                }
            });
        }


        return false;

    }



    $(document).ready(function () {
    	
        $('body').on('click', '#save-reject-application', function (e) {
           
           var msg = $('#message-reject').val();
           var application_id = $('#job-reject-application-id').val();
           
           $.ajax({
            url: '<?php echo $this->createUrl('jobApplications/updateReject') ?>',
            type: 'POST',
            data: {application_id: application_id, msg: msg},
            success: function (data) {
                location.reload();
            },
//            error: function (data) {
//                alert('err');
//            }
          });

        });
        
         $('body').on('click', '#save-accept-application', function (e) {
           
           var msg = $('#message-accept').val();
           var application_id = $('#job-accept-application-id').val();
           
           $.ajax({
            url: '<?php echo $this->createUrl('jobApplications/updateAccept') ?>',
            type: 'POST',
            data: {application_id: application_id, msg: msg},
            success: function (data) {
               // alert(data);
                location.reload();
            },
//            error: function (data) {
//                alert('err');
//            }
          });

        });
        
        
        $('body').on('click', '#save-invite-application', function (e) {
           
           var interview_date = $('#interview_date').val();
           var msg = $('#message-invite').val();
           var application_id = $('#job-invite-application-id').val();
           
           $.ajax({
            url: '<?php echo $this->createUrl('jobApplications/updateInvite') ?>',
            type: 'POST',
            data: {application_id: application_id, msg: msg, interview_date: interview_date},
            success: function (data) {
                location.reload();
            },
//            error: function (data) {
//                alert('err');
//            }
          });

        });


 	   $('body').on('click', '.check_ref', function (e) {

 	 	   var id = $(this).attr('id');
 		   if (confirm('You want to send verification mail to referrer ?')) {
 	 		  
 			 $.ajax({
                     url: '<?php echo $this->createUrl('jobApplications/sendReferenceMail') ?>',
                     type: 'POST',
                     data: {job_application_id: id},
                     success: function (data) {
	// alert(data);
                       location.reload();
                     },
                    error: function (data) {
                        //alert('err');
                    }
                 });
 		    }
              
 	   });

    });

</script>

<div class="page-title text-left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Applications</h1>
            </div>
        </div>
    </div>
</div>
<div id="page-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
<?php if (Yii::app()->user->hasFlash('success')): ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?php echo Yii::app()->user->getFlash('success'); ?>
                    </div>
                <?php endif; ?>
                <div class="JobApplications__job">

                    <div class="Job">
                        <div class="Job__header clearfix">
                            <div class="pull-left">
                                <?php echo $model->title; ?><br/>
                                <div class="Job__deadline">Deadline: <?php echo $model->end_date; ?></div>
                            </div>
                            <div class="pull-right">
                                <form method="POST" action="<?php echo $this->createUrl('jobs/jobstatus',array('id'=>$model->id)); ?>" accept-charset="UTF-8" class="JobStatus__form">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <?php 
                                                 if($model->status=='active')
                                                    echo "Active";
                                                 else
                                                    echo "On Hold";
                                            ?> <span class="caret"></span>
                                            <input name="status" type="hidden">
                                        </button>
                                        <?php 
                                        $cri = new CDbCriteria();
                                        $cri->condition = "owner_id =".$model->owner_id." and type like '%agency%'";
                                        $profilemodel = Profiles::model()->find($cri);
                                        ?>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" class="JobBtn__active">Active</a></li>
                                            <li><a href="#" class="JobBtn__hold">On Hold</a></li>
                                            <li><a href="<?php echo $this->createUrl('employers/jobsedit',array('slug'=>$profilemodel->slug,'id'=>$model->id)); ?>">Extend</a></li>
                                        </ul>
                                    </div><!-- .btn-group -->
                                </form>                            
                            </div>
                        </div>
                        <div class="Job__meta">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="Job__details">
                                        <ul class="row list-unstyled">
                                             <?php if($model->salary_min > 0){ ?>
                                            <li class="col-sm-4"><?php echo Yii::app()->numberFormatter->formatCurrency($model->salary_min, 'GBP')." to ".Yii::app()->numberFormatter->formatCurrency($model->salary_max, 'GBP')." per ".($model->salary_type == "annual" ? "annum" : "hour")?></li>
                                           <?php } ?>
                                            <li class="col-sm-4"><?php echo $model->type ?></li>
                                             <?php 
                                                $date1 = new DateTime();
                                                $date2 = new DateTime($model->created_at);
                                                
                                                $interval= $date1->diff($date2);
                                                $hourdiff = ($interval->days * 24) + $interval->h;
                                                if($hourdiff <= 23)
                                                    $postedtime = $hourdiff." hours ago";
                                                else 
                                                {
                                                    $daysdiff = $date2->diff($date1)->format("%a");
                                                    if($daysdiff <= 28)
                                                        $postedtime = $daysdiff . " days ago";
                                                    else             
                                                    {
                                                        
                                                        $postedtime = ($daysdiff / 30). " months ago";
                                                    }
                                                }
                                                
                                                
            //                                     $postedtime = $diff;
                                               // echo "diff=".$diff;
                                                
                                            ?>
                                            <li class="col-sm-4">Posted: <?php echo timeAgo($model->created_at); ?></li>
                                            <li class="col-sm-4"><?php echo $model->location ?></li>
                                            <?php if($model->vacancies > 0){ ?>
                                            	<li class="col-sm-4"><?php echo $model->vacancies." vacancies" ?></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-sm-12">
                 <?php if (Yii::app()->user->hasFlash('success')): ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?php echo Yii::app()->user->getFlash('success'); ?>
                    </div>
                <?php endif; ?>
                <div class="JobApplications__applications">
                      <?php
                        $cri3 = new CDbCriteria();
                        $cri3->condition = "status < 1 and job_id = ".$model->id;
                        $count2 = count(JobApplications::model()->findAll($cri3));
                        ?>
                    <ul class="nav nav-tabs">
                        <li role="presentation"  class="active">
                            <a href="<?php echo Yii::app()->createUrl("agencyStaff/applications",array("id"=>$model->id,"title"=>  str_replace(" ", "-", $model->title))); ?>"><i class="fa fa-inbox"></i>Applications (<?php echo $count2; ?>)</a>
                        </li>
                        <li role="presentation">
                            <a href="<?php echo Yii::app()->createUrl("agencyStaff/shortlists",array("id"=>$model->id,"title"=>  str_replace(" ", "-", $model->title))); ?>"><i class="fa fa-star"></i>Shortlisted (<?php echo JobApplications::model()->getShortCount($model->id); ?>)</a>
                        </li>
                        
                    </ul>                   
<!--                     <form method="GET" action="#" accept-charset="UTF-8"> -->
<!--                         <div class="JobApplications__filter form-inline"> -->
<!--                             <div class="form-group"> -->
<!--                                 <div class="input-group"> -->
<!--                                     <input class="form-control" placeholder="Search..." name="search" type="text" value=""> -->
<!--                                     <span class="input-group-btn"> -->
<!--                                         <button class="btn btn-primary"><i class="fa fa-search"></i></button> -->
<!--                                     </span> -->
<!--                                 </div> -->
<!--                             </div> -->
<!--                         </div> -->
<!--                     </form> -->
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>Applied Date</th>
                                <th>Check Reference</th>
                                <th>Actions</th>
                            </tr>
                        </tbody>
                        <tbody>
                            <?php
                            $cri2 = new CDbCriteria();
                            $cri2->condition = "status < 1 and job_id = " . $model->id;
                            $count1 = count(JobApplications::model()->findAll($cri2));
                            if ($count1 > 0) {
                                $jobnewapp = JobApplications::model()->findAll($cri2);
                                foreach ($jobnewapp as $job) {
                                    $usermodel = Users::model()->findByPk($job['user_id']);
                                    $cri1 = new CDbCriteria();
                                    $cri1->condition = "owner_id =".$usermodel->id." and type like '%candidate%'";
                                    $candprofile = Profiles::model()->find($cri1);
                                    
                                    //check recruitment candidate
                                    $agecanmodel = AgencyCandidates::model()->findByAttributes(array('user_id'=>$job['user_id']));
                                    if($agecanmodel != null)
                                    {
                                        $agencymodel = Agency::model()->findByPk($agecanmodel['agency_id']);
                                        $user_name = "XXXX";
                                        $user_email = $agencymodel->email;
                                    }
                                    else {
                                        $user_name = $usermodel->forenames." ".$usermodel->surname;
                                        $user_email = $usermodel->email;
                                    }
                                    //ends
                                    
                                    ?>
                                    <tr>
                                        <td>
                                            <a href="<?php echo $this->createUrl('users/profile',array('slug'=>$candprofile['slug'])) ?>" target="_blank">
                                                <?php echo $user_name; ?>
                                            </a>
                                        </td>
                                        <td><?php echo $user_email; ?></td>
                                        <td><?php
                                            if ($job['status'] == 0) {
                                                echo "New Application";
                                            } else if ($job['status'] == 1) {
                                                echo "Shortlist";
                                            } else if ($job['status'] == 2) {
                                                echo "Invited for Interview";
                                            } else if ($job['status'] == -1) {
                                                echo "Rejected";
                                            }
                                            ?></td>
                                        <td> <?php echo $job['created_at']; ?></td>
                                        <td> <a href="#" class="check_ref" id="<?php echo $job['id']?>"><?php echo $job['reference_mail']; ?></a></td>
                                        <td width="25%">
                                                <input name="application_id" id="application_id" type="hidden" value="<?php echo $job['id']; ?>">
                                                <div class="form-group">
                                                    <select class="form-control application-status" name="status" onchange="changeStatus(this.value, <?php echo $job['id']; ?>)">
                                                        <?php if ($job['status'] != -1) { ?><option value="pending" <?php
                                                            if ($job['status'] == 0) {
                                                                echo "selected";
                                                            }
                                                            ?>>New Application</option> <?php } else { ?>
                                                            <option value="reject" <?php
                                                            if ($job['status'] == -1) {
                                                                echo "selected";
                                                            }
                                                            ?>>Rejected</option>
                                                                <?php } ?>
                                                        <option value="shortlist" <?php
                                                        if ($job['status'] == 1) {
                                                            echo "selected";
                                                        }
                                                        ?>>Shortlist</option>
                                                                <?php if ($job['status'] != -1) { ?>
                                                            <option value="reject" <?php
                                                            if ($job['status'] == -1) {
                                                                echo "selected";
                                                            }
                                                            ?>>Reject</option>
                                                                <?php } ?>
                                                    </select>
                                                </div>
                                                
                                                 <div class="modal fade" id="InterviewModal">
                                                        <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <input type="hidden" id="job-invite-application-id" value="<?php echo $job['id']; ?>">
                                                                <button type="button" class="close" data-dismiss="modal" onclick='closepopup()' aria-label="Close">
                                                                <span aria-hidden="true">×</span></button>
                                                                <h4 class="modal-title" id="myModalLabel">Invite for Interview</h4>
                                                            </div>
                                                            <div class="ModalApplicationStatus__body">

                                                                <div class="ModalApplicationStatus__form">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Interview Date and Time</label>
                                                                        <input type="text" autocomplete='off' name="interview_date" class="datepicker form-control" value="">
                                                    
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label class="control-label">Message</label>
                                                                        <textarea id="message-invite" name="message" class="form-control" rows="10"></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="ModalApplicationStatus__loader" style="display:none;">
                                                                    <div class="ModalApplicationStatus__loader-inner">
                                                                        <div>
                                                                            <img src="<?php echo Yii::app()->baseUrl?>/images/loading.gif" alt="">
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" onclick='closepopup()' data-dismiss="modal">Close</button>
                                                                <button type="button" id="save-invite-application" class="btn btn-primary SubmitBtn">Submit</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                    
                                                <!--reject popup-->
                                                <div class="modal fade" id="RejectModal">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick='closepopup()'><span aria-hidden="true">×</span></button>
                                                                <h4 class="modal-title" id="myModalLabel">Reject the Application</h4>
                                                            </div>
                                                            <div class="ModalApplicationStatus__body">

                                                                <div class="ModalApplicationStatus__form">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Message</label>
                                                                        <input type="hidden" id="job-reject-application-id" value="<?php echo $job['id']; ?>">
                                                                        <textarea name="message" id="message-reject" class="form-control" rows="10"></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="ModalApplicationStatus__loader" style="display:none;">
                                                                    <div class="ModalApplicationStatus__loader-inner">
                                                                        <div>
                                                                            <img src="<?php echo Yii::app()->baseUrl?>/images/loading.gif" alt="">
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal" onclick='closepopup()'>Close</button>
                                                                <button class="btn btn-primary SubmitBtn" id="save-reject-application" type="button">Submit</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <!--accept popup-->
                                                <div class="modal fade" id="AcceptModal">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal" onclick='closepopup()' aria-label="Close"><span aria-hidden="true">×</span></button>
                                                                <h4 class="modal-title" id="myModalLabel">Accept the Application</h4>
                                                            </div>
                                                            <div class="ModalApplicationStatus__body">

                                                                <div class="ModalApplicationStatus__form">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Message</label>
                                                                        <input type="hidden" id="job-accept-application-id" value="<?php echo $job['id']; ?>">
                                                                        <textarea name="message" id="message-accept" class="form-control" rows="10"></textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="ModalApplicationStatus__loader" style="display:none;">
                                                                    <div class="ModalApplicationStatus__loader-inner">
                                                                        <div>
                                                                            <img src="<?php echo Yii::app()->baseUrl?>/images/loading.gif" alt="">
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal" onclick='closepopup()'>Close</button>
                                                                <button class="btn btn-primary SubmitBtn" type="button" id="save-accept-application" onclick='closepopup()'>Submit</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        </td>
                                    </tr>

                                    <?php
                                }
                            } else {
                                ?>
                                <tr>
                                    <td colspan="5">There are currently no applications matching this criteria</td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div><!-- .JobApplications__applications -->

            </div>
        </div>
    </div>
</div>

 <?php
    $this->beginWidget(
            'booster.widgets.TbModal', array('id' => 'InterviewModal')
    );
    ?>

        <div class="modal-header">
            <a class="close" data-dismiss="modal" onclick='closepopup()'>&times;</a>
            <h4>Invite for Interview</h4>
        </div>

        <div class="modal-body" id="load-interview-div">
            
        </div>

        <div class="modal-footer">
            <?php
            $this->widget(
                    'booster.widgets.TbButton', array(
                'context' => 'primary',
                'label' => 'Save changes',
                'url' => '#',
                'htmlOptions' => array('data-dismiss' => 'modal','id'=>'save-invite-application'),
                    )
            );
            ?>
            <?php
            $this->widget(
                    'booster.widgets.TbButton', array(
                'label' => 'Close',
                'url' => '#',
                'htmlOptions' => array('data-dismiss' => 'modal','onclick'=>'closepopup()'),
                    )
            );
            ?>
        </div>

<?php $this->endWidget(); ?>
        
        <?php
    $this->beginWidget(
            'booster.widgets.TbModal', array('id' => 'RejectModal')
    );
    ?>

        <div class="modal-header">
            <a class="close" data-dismiss="modal" onclick='closepopup()'>&times;</a>
            <h4>Reject the Application</h4>
        </div>

        <div class="modal-body" id="load-reject-div">
            
        </div>

        <div class="modal-footer">
            <?php
            $this->widget(
                'booster.widgets.TbButton', array(
                'context' => 'primary',
                'label' => 'Save changes',
                'url' => '#',
                'htmlOptions' => array('data-dismiss' => 'modal','id'=>'save-reject-application'),
                    )
            );
            ?>
            <?php
            $this->widget(
                'booster.widgets.TbButton', array(
                'label' => 'Close',
                'url' => '#',
                'htmlOptions' => array('data-dismiss' => 'modal','onclick'=>'closepopup()'),
                    )
            );
            ?>
        </div>

<?php $this->endWidget(); ?>


 <?php
    $this->beginWidget(
            'booster.widgets.TbModal', array('id' => 'AcceptModal')
    );
    ?>

        <div class="modal-header">
            <a class="close" data-dismiss="modal" onclick='closepopup()'>&times;</a>
            <h4>Accept the Application</h4>
        </div>

        <div class="modal-body clearfix" id="load-accept-div">
            
        </div>

        <div class="modal-footer">
            <?php
            $this->widget(
                'booster.widgets.TbButton', array(
                'context' => 'primary',
                'label' => 'Save changes',
                'url' => '#',
                'htmlOptions' => array('data-dismiss' => 'modal','id'=>'save-accept-application'),
                    )
            );
            ?>
            <?php
            $this->widget(
                'booster.widgets.TbButton', array(
                'label' => 'Close',
                'url' => '#',
                'htmlOptions' => array('data-dismiss' => 'modal','onclick'=>'closepopup()'),
                    )
            );
            ?>
        </div>

<?php $this->endWidget(); ?>

 
 <?php 
    function timeAgo($time_ago)
    {
        $time_ago = strtotime($time_ago);
        $cur_time   = time();
        $time_elapsed   = $cur_time - $time_ago;
        $seconds    = $time_elapsed ;
        $minutes    = round($time_elapsed / 60 );
        $hours      = round($time_elapsed / 3600);
        $days       = round($time_elapsed / 86400 );
        $weeks      = round($time_elapsed / 604800);
        $months     = round($time_elapsed / 2600640 );
        $years      = round($time_elapsed / 31207680 );
        // Seconds
        if($seconds <= 60){
            return "just now";
        }
        //Minutes
        else if($minutes <=60){
            if($minutes==1){
                return "one minute ago";
            }
            else{
                return "$minutes minutes ago";
            }
        }
        //Hours
        else if($hours <=24){
            if($hours==1){
                return "an hour ago";
            }else{
                return "$hours hrs ago";
            }
        }
        //Days
        else if($days <= 7){
            if($days==1){
                return "yesterday";
            }else{
                return "$days days ago";
            }
        }
        //Weeks
        else if($weeks <= 4.3){
            if($weeks==1){
                return "a week ago";
            }else{
                return "$weeks weeks ago";
            }
        }
        //Months
        else if($months <=12){
            if($months==1){
                return "a month ago";
            }else{
                return "$months months ago";
            }
        }
        //Years
        else{
            if($years==1){
                return "one year ago";
            }else{
                return "$years years ago";
            }
        }
    }
 ?>




