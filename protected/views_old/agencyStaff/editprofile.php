<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs = array(
    'Users' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});");
?> 
<script>
$(document).ready(function(){
	$('body').on('click', '#delete-candidates', function () {
           
            if($("#staff-can-grid").find("input:checked").length > 0)
            { 			
           	 	if (confirm('Are you sure you want to delete these items?')) {

            		 var ids = $.fn.yiiGridView.getChecked("staff-can-grid", "selectedIds");
          			  $.ajax({
          		            url: '<?php echo $this->createUrl('agencyStaff/deleteCandidates') ?>',
          		            type: 'POST',
          		            data: {ids : ids}, 	    
          		            success: function(data) {
          		            	location.reload();
          		            },
          				    error: function(data) {		
          				        alert('err');
          				    }
          		         });
           	 		} 
           	
            }
            else
            {
               alert('Please select at least one item');
            }
        });


    $('#NewConversationModal').on('click', '.SendMessageBtn', function () {
        var errors = false;
        var errors_mgs = '';
        var $subject = $('input[id="subject"]');
        var subject_val = $subject.val();
        var $target_id = $('select[id="target_id"]');
        var target_val = $target_id.val();
        var $message = $('textarea[id="message"]')
        var message_val = $message.val();
        // Validate Subject
        if (target_val == '') {
            errors_mgs += '<div>The target field is required.</div>';
            errors = true;
        } else {
            errors = false;
        }

        // Validate Subject
        if (subject_val == '') {
            errors_mgs += '<div>The subject is required</div>';
            errors = true;
        } else {
            errors = false;
        }

        // Validate Message
        if (message_val == '') {
            errors_mgs += '<div>A message is required</div>';
            errors = true;
        } else {
            errors = false;
        }
        
        if (!errors) {
            $('.modal-loader').show();
            $.ajax({
                url: '<?php echo $this->createUrl('agencyStaff/sendMessage') ?>',
                type: 'POST',
                data: {message: message_val, subject: subject_val, user_id: target_val},
                success: function (data) {
                    $('#msg_scc_msg').show();
                    $('.modal-loader').hide();
                    setTimeout(function () {
                        closepopup();
                    }, 3000);
                },
            });
        } else {
            $('#msg_err_msg').html(errors_mgs);
            $('#msg_err_msg').show();
        }


        return false;
    });

   
     

        $('body').on('click', '.open-media', function () {
            var getValue = $(this).attr("data-field");
            if (getValue == 'cover') {
                $(".saveMedia").attr('id', 'save-media-cover');
            } else if (getValue == 'photo') {
                $(".saveMedia").attr('id', 'save-media-photo');
            }

            //alert(getValue);return false;
            $('#mediamanager_modal').addClass('in');
            $('body').addClass('modal-open');
            $('#mediamanager_modal').show();
        });

        $('body').on('click', '.create-message', function () {
            //alert(getValue);return false;
            $('#NewConversationModal').addClass('in');
            $('body').addClass('modal-open');
            $('#NewConversationModal').show();
        });

        $('.upload-media').on('click', '.medaiamanager_add', function () {
            //e.preventDefault();
            $(".media-file").trigger('click');
            //return false;
        });

        $('.modal-content').on('change', '.media-file', function (e) {

            var fileExtension = ['jpeg', 'jpg', 'png'];
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                alert("Only '.jpeg','.jpg','.png' formats are allowed.");
            } else {
                $('.MediaManager__loading').removeClass('hidden');
                var data = new FormData();
                jQuery.each(jQuery(this)[0].files, function (i, file) {
                    data.append('media', file);
                });
                data.append('collection_name', 'photos');
                data.append('model_type', "App\\Employer");
                data.append('model_id', <?php echo $model->id; ?>);
                // alert(JSON.stringify(data));
                $.ajax({
                    url: '<?php echo $this->createUrl('media/insertMediaManager') ?>',
                    type: 'POST',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $(".MediaManager__content").load(location.href + " .MediaManager__content>*", "");
                        $('.MediaManager__loading').addClass('hidden');
                    },
                    error: function (data) {
                        alert('err');
                    }
                });
            }

            // return false;
        });


        //  media_item_delete  
        $('body').on('click', '.MediaManager__item__delete', function (e) {

            if (confirm('Are you sure you want to delete this item?')) {

                var id = $(this).attr('id');

                $.ajax({
                    url: '<?php echo $this->createUrl('media/deleteMedia'); ?>',
                    type: 'POST',
                    data: {id: id},
                    success: function (data) {
                        //alert(data);
                        $(".MediaManager__content").load(location.href + " .MediaManager__content>*", "");
                        // $("#result_para").load(location.href + " #result_para>*", "");
                    },
                    error: function (data) {
                        alert('err');
                    }
                });
            }

            return false;

        });


        $('.modal-content').on('click', '.MediaManager__item', function (evt) {
            $('div.selected').removeClass('selected');
            $(this).addClass('selected');
        });

        $('.modal-content').on('click', '#save-media-cover', function (evt) {

            var mediaid = $(".MediaManager__content").find(".selected").attr('id');

            $.ajax({
                url: '<?php echo $this->createUrl('profiles/updateCover') ?>',
                type: 'POST',
                data: {mediaid: mediaid, profileid: <?php echo $profilemodel->id; ?>},
                success: function (data) {
                    window.location.reload();
                    // $("#colcover").load(location.href + " #colcover>*", "");
                    //  closepopup();
                },
                error: function (data) {
                    alert('err');
                }
            });


        });


        $('.modal-content').on('click', '#save-media-photo', function (evt) {

            var mediaid = $(".MediaManager__content").find(".selected").attr('id');

            $.ajax({
                url: '<?php echo $this->createUrl('profiles/updatePhoto') ?>',
                type: 'POST',
                data: {mediaid: mediaid, profileid: <?php echo $profilemodel->id; ?>},
                success: function (data) {
                    window.location.reload();
                },
                error: function (data) {
                    alert('err');
                }
            });


        });
}); 


</script>


<div id="profile">
    <?php
    $employerid = isset($this->employerid) ? $this->employerid : 0;
    $userid = isset($this->userid) ? $this->userid : 0;
    $institutionid = isset($this->institutionid) ? $this->institutionid : 0;
    $edit = isset($this->edit) ? $this->edit : 0;
    $this->renderPartial('//profiles/Cover', array('userid' => $userid, 'employerid' => $employerid, 'institutionid' => $institutionid, 'edit' => $edit));
    ?>
    <div id="form-details">
        <div class="container">
            <div class="form-details-inner">
                <div class="form-user-details form-section">
                  <?php if (Yii::app()->user->hasFlash('subscription-ends')): ?>
                		<div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                             <?php echo Yii::app()->user->getFlash('subscription-ends'); ?>
                        </div>
                     <?php endif; ?>
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="text-center col-sm-4 col-md-3 col-lg-2">
                            <div class="ProfileEdit__photo">
                                <?php
                                if ($profilemodel->photo_id > 0) {
                                    $mediamodel = Media::model()->findByPk($profilemodel->photo_id);
                                    $src = ((!empty($mediamodel->file_name) && $mediamodel->file_name != null) ? Yii::app()->baseUrl . "/images/media/" . $profilemodel->photo_id . "/" . $mediamodel->file_name : Yii::app()->baseUrl . "/images/profile.png");
                                } else {
                                    $src = Yii::app()->baseUrl . "/images/defaultprofile.jpg";
                                }
                                echo CHtml::image($src, "", array("id" => "profile-image", 'alt' => ''));
                                ?>
                                <?php if (isset($this->edit) && $this->edit) { ?>
                                    <a class="open-media profile-photo-btn" data-field="photo" data-target="#profile-image">
                                        <i class="fa fa-camera"></i>
                                        Change Photo
                                    </a>
                                <?php } ?>
                            </div>
                            <a class="btn btn-primary" href="<?php echo $this->createUrl('agencyStaff/profile', array('id' => $model->id)) ?>">
                                <i class="fa fa-search"></i>
                                <span>View Profile</span>
                            </a>
                        </div>
                        <div class="col-sm-5 col-md-5 col-lg-6">
                            <div class="ProfileEdit__details">
                                <h3>
                                    <?php echo $model->forenames ?> <?php echo $model->surname ?>
                                    <?php
                                    $trail = "";
                                    if ($model->trial_ends_at != null) {
                                        $trail = date('Y-m-d', strtotime($model->trial_ends_at));
                                        $expiry = date('d/m/Y', strtotime($model->trial_ends_at));
                                    }

                                    /* if (($model->trial_ends_at != null && Membership::onTrail($trail)) || $model->stripe_active > 0) {
                                        ?>
                                        - <small class="subscription-marker subscription-marker--premium">Premium Account</small>
                                    <?php } else { ?>
                                        - <small class="subscription-marker subscription-marker--basic">Basic Account</small>
                                    <?php } */?>
                                </h3>
                                <div><?php echo $model->current_job ?></div>
                                <div><?php echo $model->location ?></div>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-4 col-lg-4">
                            <div class="ProfileEdit__operations">
                                
                                <div>
                                    <?php /* if (($model->trial_ends_at == null && !$model->stripe_active)){ ?>
                                        <a class="btn btn-default btn-upgrade" href="<?php echo $this->createUrl('agencystaff/upgrade'); ?>">
                                            <i class="fa fa-user"></i>
                                            <span>Basic</span>
                                        </a>
                                    <?php } else { 
                                         if (Membership::onTrail($trail) || $model->stripe_active) {
                                            ?>
                                            <div class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Expires on <?php echo $expiry; ?>">
                                                <i class="fa fa-credit-card"></i>
                                                <span>Premium</span>
                                            </div>
                                        <?php } else { ?>
                                            <a class="btn btn-default" href="<?php echo $this->createUrl('agencystaff/mysubscription') ?>">
                                                <i class="fa fa-credit-card"></i>
                                                <span>Account</span>
                                            </a>
                                        <?php
                                        }
                                     }*/ ?>

                                    <!-- <a class="btn btn-default" href="<?php //echo $this->createUrl('agencystaff/profile', array('id' => $model->id)) ?>">
                                        <i class="fa fa-user"></i>
                                        <span>View</span>
                                    </a> -->
                                </div>
                                 
                                   <!-- <div>
                                        <a class="btn default-btn" href="<?php //echo $this->createUrl('agencystaff/userAccountInfo') ?>">
                                            <i class="fa fa-pencil"></i>
                                            <span>Edit</span>
                                        </a>
                                         <a class="btn default-btn" href="<?php //echo $this->createUrl('agencystaff/candidates', array('id' => $model->id)) ?>">
                                            <i class="fa fa-group"></i>
                                            <span>Candidates</span>
                                        </a> 
                                       
                                    </div>-->
                                
                                    <div style="display: <?php echo $astaffmodel->is_employer <= 0 ? "" : "none"?>">
                                        <?php
                                        $convmem = ConversationMembers::model()->findAllByAttributes(array('user_id' => $profilemodel->owner_id, 'last_read' => null));
                                        ?>
                                        <a class="btn default-btn" href="<?php echo $this->createUrl('agencyStaff/inbox') ?>">
                                            <i class="fa fa-envelope"></i>
                                            <span>Inbox (<?php echo count($convmem) ?>)</span>
                                        </a>

                                        <a class="btn btn-default create-message" href="#">
                                            <i class="fa fa-envelope-o"></i>
                                            <span>Message</span>
                                        </a>
                                    </div>
                                 
                            </div>               
                        </div>
                    </div>
                </div>
				
             <div id="profile-jobs" class="form-section">
              <?php if($astaffmodel->is_employer <= 0) { ?>
                    <div class="form-section-inner">
                        <div class="section-header">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4>Candidates</h4>
                                </div>
                            </div>
                        </div>
                        <div class="section-content">
                            <div class="section-description" style="overflow-x: scroll;">
                                <?php
                               $this->widget('zii.widgets.grid.CGridView', array(
                                    'id' => 'staff-can-grid',
                                    'itemsCssClass' => 'table table-striped',
                                    'summaryText' => '',
                                    //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                                    'dataProvider' => AgencyStaffCandidates::model()->searchByStaff(Yii::app()->user->getState('userid')),
                                    'columns' => array(
                                        array(
                                            'id' => 'selectedIds',
                                            'value' => '$data->staff_id',
                                            'class' => 'CCheckBoxColumn',
                                            'selectableRows' => 2
                                        ),
                                        array(
                                            'header' => 'Name',
                                            'value' => 'CHtml::link("<i class=\"fa fa-pencil\"></i> ".$data->getName(),Yii::app()->createUrl("agencyStaff/editCandidate",array("id"=>$data->id)),array("class"=>"can-edit-button","id"=>"can-edit"))',
                                            'type' => 'raw'
                                        ),
                                        array(
                                            'header' => 'Email',
                                            'value'=>'$data->getEmail()'
                                        ),
                                        array(
                                            'header' => 'Current Job',
                                            'value'=>'$data->getCurrentJob()'
                                        ),
                                    ),
                                ));
                                ?>
                           
                                
                                    <a class="main-btn" href="<?php echo $this->createUrl('agencyStaff/addCandidate') ?>">Add Candidate</a>
                                   
                               		<button type="button" class="main-btn" id="delete-candidates"> Delete</button>
                            </div>
                        </div>
                    </div>
                    
                    
                      <div class="form-section-inner">
                        <div class="section-header">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4>Recruitment Jobs</h4>
                                </div>
                            </div>
                        </div>
                        <div class="section-content">
                            <div class="section-description" style="overflow-x: scroll;">
                                <?php
                               $this->widget('zii.widgets.grid.CGridView', array(
                                    'id' => 'staff-can-grid',
                                    'itemsCssClass' => 'table table-striped',
                                    'summaryText' => '',
                                    //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                                    'dataProvider' => AgencyVacancies::model()->search($agencymodel->id),
                                    'columns' => array(
                                        array(
                                            'header' => 'Job Title',
                                            'value' => '$data->job->title',
                                            'type' => 'raw',
                                        ),
                                        array(
                                            'header' => 'Salary',
                                            'value' => '$data->job->getSalary()',
                                        ),
                                        array(
                                            'header' => 'Type',
                                            'value' => '$data->job->type',
                                        ),
                                        array(
                                            'header' => 'Location',
                                            'value' => '$data->job->location',
                                        ),
                                        array(
                                            'header' => 'Date Added',
                                            'value' => '$data->job->created_at',
                                        ),
                                        array(
                                            'header' => 'Date Ending',
                                            'value' => '$data->job->end_date',
                                        ),
                                        array(
                                            'header' => 'Views',
                                            'value' => '$data->job->num_views',
                                        ),
                                        array(
                                            'header' => 'Applications',
                                            'value' => 'CHtml::link("View Applicants (".$data->job->getCount().")",urldecode(Yii::app()->createUrl("agencyStaff/applications",array("id"=>$data->job->id,"title"=>str_replace("/","",str_replace(" ", "-", $data->job->title))))),array("class"=>"job-applications-button","id"=>"job-applications"))',
                                            'type' => 'raw'
                                        ),
                                    ),
                                ));
                                ?>
                           
                            </div>
                        </div>
                    </div>
                    
                    <?php  } 
                    else { ?>
                    
                     <div class="form-section-inner">
                        <div class="section-header">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4>Jobs</h4>
                                </div>
                            </div>
                        </div>
                        <div class="section-content">
                            <div class="section-description" style="overflow-x: scroll;">
                                <?php
                               $this->widget('zii.widgets.grid.CGridView', array(
                                    'id' => 'staff-can-grid',
                                    'itemsCssClass' => 'table table-striped',
                                    'summaryText' => '',
                                    //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                                   'dataProvider' => AgencyVacancies::model()->search($agencymodel->id),
                                    'columns' => array(
                                      
                                        array(
                                            'header' => 'Job',
                                            'value' => 'CHtml::link($data->job->title,Yii::app()->createUrl("agency/editVacancy",array("id"=>$data->id)),array("class"=>"can-edit-button","id"=>"can-edit"))',
                                            'type' => 'raw'
                                        ), 
                                        array(
                                            'header' => 'Salary',
                                            'value' => '$data->job->getSalary()',
                                        ),
                                        array(
                                            'header' => 'Type',
                                            'value' => '$data->job->type',
                                        ),
                                        array(
                                            'header' => 'Location',
                                            'value' => '$data->job->location',
                                        ),
                                        array(
                                            'header' => 'Date Ending',
                                            'value' => '$data->job->end_date',
                                        ),
//                                         array(
//                                             'header' => 'Applications',
//                                             'value' => 'CHtml::link("View Applicants (".$data->job->getCount().")",urldecode(Yii::app()->createUrl("jobs/applications",array("id"=>$data->job->id,"title"=>str_replace("/","",str_replace(" ", "-", $data->job->title))))),array("class"=>"job-applications-button","id"=>"job-applications"))',
//                                             'type' => 'raw'
//                                         )
                                    ),
                                ));
                                ?>
                           
                                
                            </div>
                        </div>
                    </div>
                    
                    <?php } ?>
                </div>


            </div>
        </div><!-- /.container -->
    </div>
</div>

<!--cover photo popup-->

<div class="modal fade" id="mediamanager_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="MediaManager__loading hidden">
                <img src="<?php echo $this->createAbsoluteUrl('//'); ?>/images/loading.gif">
            </div>
            <div class="MediaManager__header">
                <form action="" method="POST" enctype="multipart/form-data" class="add-new-form">
                    <div class="row">
                        <div class="col-xs-7">
                            <h4>Media Library</h4>
                        </div>
                        <div class="col-xs-5">
                            <div class="upload-media pull-right">
                                <label class="btn main-btn medaiamanager_add"><i class="fa fa-plus"></i>Upload a File</label>
                                <input class="media-file" type="file" style="display:none;"/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="MediaManager__content">
                <?php
                $mediamodel = Media::model()->findAll(array(
                    'condition' => 'model_id=:model_id AND model_type=:model_type AND collection_name=:collection_name',
                    'params' => array(':model_id' => $model->id, ':model_type' => 'App\Employer', ':collection_name' => 'photos'),
                ));
                if (count($mediamodel) > 0) {
                    ?>
                    <div class="row">
                        <?php foreach ($mediamodel as $media) { ?>
                            <div class="col-xs-6 col-sm-3">
                                <div class="MediaManager__item MediaManager__item--image" id="<?php echo $media['id'] ?>">
                                    <img src="<?php echo Yii::app()->baseUrl ?>/images/media/<?php echo $media['id'] ?>/conversions/search.jpg" class="img-responsive">
                                    <a class="MediaManager__item__delete" href="#" id="<?php echo $media['id'] ?>"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
                <h3>CV Vid Image Library</h3>
                <?php
                $mediamodel1 = Media::model()->findAll(array(
                    'condition' => 'model_id=:model_id AND collection_name=:collection_name',
                    'params' => array(':model_id' => 1, ':collection_name' => 'sample'),
                ));
                if (count($mediamodel1) > 0) {
                    ?>
                    <div class="row">
                        <?php foreach ($mediamodel1 as $media) { ?>
                            <div class="col-xs-6 col-sm-3">
                                <div class="MediaManager__item MediaManager__item--image" id="<?php echo $media['id'] ?>">
                                    <img src="<?php echo Yii::app()->baseUrl ?>/images/media/<?php echo $media['id'] ?>/conversions/search.jpg" class="img-responsive">
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn default-btn" data-dismiss="modal" onclick="closepopup()">Close</button>
                <button type="button" class="btn main-btn select-submit saveMedia">Save</button>
            </div>

        </div>
    </div>
</div>

<!--conver messgae-->
<div id="NewConversationModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closepopup()"><span aria-hidden="true">X</span></button>
                <h4 class="modal-title">Send a Message</h4>
            </div>
            <div class="modal-body">
                <form class="MessageForm">
                    <div class="alert alert-success" id='msg_scc_msg' style="display:none;">
                        Your message has been successfully sent
                    </div>
                    <div class="alert alert-danger" id='msg_err_msg' style="display: none">

                    </div>
                    <div class="form-group">
                        <label>Contact</label>
                         <select name="target_id" id="target_id" class="form-control">
                                <option value="">- Select Contact -</option>
                                <?php              
                                $agencystaffmodel = AgencyStaffCandidates::model()->findAllByAttributes(array('staff_id'=>Yii::app()->user->getState('userid')));
                                foreach ($agencystaffmodel as $agencystaff) {
                                    $agencycanmodel = AgencyCandidates::model()->findByPk($agencystaff['agency_candidate_id']);  
                                    if($agencycanmodel != null)
                                        $userarr = Users::model()->findByPk($agencycanmodel->user_id);
                                    ?>                  
                                    <option value="<?php echo $userarr['id'] ?>"> <?php echo $userarr['forenames'] . " " . $userarr['surname']; ?></option>
                                <?php } ?>
                            </select>
                    </div>
                    <div class="form-group">
                        <label for="subject">Subject</label>
                        <input name="subject" type="text" id="subject" class="form-control" value="">
                    </div>
                    <div class="form-group">
                        <label for="message">Message</label>
                        <textarea name="message" id="message" class="form-control" rows="10"></textarea>
                    </div>
                </form>
                <div class="modal-loader">
                    <img src="<?php echo Yii::app()->baseUrl?>/images/loading.gif">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default-btn" data-dismiss="modal" onclick="closepopup()">Close</button>
                <button type="button" class="SendMessageBtn btn main-btn">Send Message</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>