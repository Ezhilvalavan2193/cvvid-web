<?php
/* @var $this ProfileQualificationsController */
/* @var $model ProfileQualifications */

$this->breadcrumbs=array(
	'Profile Qualifications'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProfileQualifications', 'url'=>array('index')),
	array('label'=>'Create ProfileQualifications', 'url'=>array('create')),
	array('label'=>'View ProfileQualifications', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ProfileQualifications', 'url'=>array('admin')),
);
?>

<h1>Update ProfileQualifications <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>