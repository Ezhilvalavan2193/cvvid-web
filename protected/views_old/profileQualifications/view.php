<?php
/* @var $this ProfileQualificationsController */
/* @var $model ProfileQualifications */

$this->breadcrumbs=array(
	'Profile Qualifications'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ProfileQualifications', 'url'=>array('index')),
	array('label'=>'Create ProfileQualifications', 'url'=>array('create')),
	array('label'=>'Update ProfileQualifications', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ProfileQualifications', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProfileQualifications', 'url'=>array('admin')),
);
?>

<h1>View ProfileQualifications #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'profile_id',
		'user_qualification_id',
		'created_at',
		'updated_at',
	),
)); ?>
