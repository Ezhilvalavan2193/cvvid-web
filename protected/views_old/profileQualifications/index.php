<?php
/* @var $this ProfileQualificationsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Profile Qualifications',
);

$this->menu=array(
	array('label'=>'Create ProfileQualifications', 'url'=>array('create')),
	array('label'=>'Manage ProfileQualifications', 'url'=>array('admin')),
);
?>

<h1>Profile Qualifications</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
