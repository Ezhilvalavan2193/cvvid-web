<?php
/* @var $this JobsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    'Jobs',
);

?>

<div id="page-content">
<div class="container">
 <h1 align="center">Jobs by Company</h1>
 <hr>
<ul class="job-by-list clearfix">
<?php 
   $cri = new CDbCriteria();
    $cri->condition = "deleted_at is null";
    $empmodel = Employers::model()->findAll($cri);
    foreach ($empmodel as $emp):
    $string = str_replace(' ', '-', trim($emp['name'])); // Replaces all spaces with hyphens.
    $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    $compname = strtolower($string);
    
?> 
       <li class="jobs-li"><a href="<?php echo $this->createUrl('site/searchCompany',array('company'=>$compname)) ?>"> <?php echo $emp['name']." Jobs" ?></a> </li>
       
<?php endforeach; ?>   
 </ul>
 </div>
 </div>
    