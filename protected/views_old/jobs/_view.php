<?php
/* @var $this JobsController */
/* @var $data Jobs */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('owner_id')); ?>:</b>
	<?php echo CHtml::encode($data->owner_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('owner_type')); ?>:</b>
	<?php echo CHtml::encode($data->owner_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_id')); ?>:</b>
	<?php echo CHtml::encode($data->user_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('industry_id')); ?>:</b>
	<?php echo CHtml::encode($data->industry_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('company_name')); ?>:</b>
	<?php echo CHtml::encode($data->company_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('title')); ?>:</b>
	<?php echo CHtml::encode($data->title); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('salary_type')); ?>:</b>
	<?php echo CHtml::encode($data->salary_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('salary_min')); ?>:</b>
	<?php echo CHtml::encode($data->salary_min); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('salary_max')); ?>:</b>
	<?php echo CHtml::encode($data->salary_max); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('end_date')); ?>:</b>
	<?php echo CHtml::encode($data->end_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('skill_category_id')); ?>:</b>
	<?php echo CHtml::encode($data->skill_category_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('location')); ?>:</b>
	<?php echo CHtml::encode($data->location); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('location_lat')); ?>:</b>
	<?php echo CHtml::encode($data->location_lat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('location_lng')); ?>:</b>
	<?php echo CHtml::encode($data->location_lng); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
	<?php echo CHtml::encode($data->type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('additional')); ?>:</b>
	<?php echo CHtml::encode($data->additional); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_at')); ?>:</b>
	<?php echo CHtml::encode($data->created_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_at')); ?>:</b>
	<?php echo CHtml::encode($data->updated_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('closed_date')); ?>:</b>
	<?php echo CHtml::encode($data->closed_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('successful_applicant_id')); ?>:</b>
	<?php echo CHtml::encode($data->successful_applicant_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('num_views')); ?>:</b>
	<?php echo CHtml::encode($data->num_views); ?>
	<br />

	*/ ?>

</div>