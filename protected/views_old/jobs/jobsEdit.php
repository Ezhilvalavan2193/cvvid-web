<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs = array(
    'Employers' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});

");
?> 
<script>
$(document).ready(function(){
$("#competitive_salary").click(function() {
		
		if($(this).is(":checked"))
		{
			$('#min_salary').attr('disabled',true);
			$('#max_salary').attr('disabled',true);
		}
		else
		{
			$('#min_salary').attr('disabled',false);
			$('#max_salary').attr('disabled',false);
		}

		
	});	
// 	 $('#skills').on('select2:selecting', function(e) {
// 		 var id = e.params.args.data.id;
// 		 var text = e.params.args.data.text;
// 		   // console.log('Selecting: ' , e.params.args.data);
// 		 $('#skills-block').append('<div class="skill-selected" id='+id+'><div>'+text+'</div><input name='+id+' class="form-control" style="width:10%;display:inline" type="text"> <div style="display:inline">(No. of vacancies)</div></div>');
		 
// 	});

// 	 $('#skills').on('select2:unselecting', function(e) {
// 		    var id = e.params.args.data.id;
// 		    $('#skills-block').find('#'+id).remove();
// 	});

});
</script>
 <h1>Edit Job</h1>
 <div class="Admin__content__inner">
     
     <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'jobs-form',
        'action' => $this->createUrl('jobs/jobsUpdate', array('id' => $model->id)),
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data','class' => 'form-horizontal'),
    ));
    ?>
    <?php
//    $this->widget('ext.EChosen.EChosen', array(
//        'target' => '.select2',
//        'useJQuery' => true,
//        'debug' => true,
//    ));
    ?>
   <?php echo $form->errorSummary($model); ?>

     <div class="form-container">

         <div class="row">
             <div class="col-sm-6">                
                 <div class="form-group">
                  <h4>Employer</h4>
                <select name="Jobs[owner_id]"  class="form-control select2">
                    <option selected="selected" disabled>Search Employer</option>
                    <?php
                    $emodel = Employers::model()->findAll();
                    foreach ($emodel as $emp) {
                        ?>                  
                        <option value="<?php echo $emp['id'] ?>" <?php echo ($employermodel->id ==  $emp['id'] ? "selected" : "");?>> <?php echo $emp['name'] ?> </option>
                     <?php } ?>
                </select>
                </div>
             </div>
         </div>

         
         <div class="row">
             <div class="col-sm-6">
                 <div class="row">
                     <div class="col-sm-12">
                         <div class="form-group">
                         <h4>Job Specifics</h4>
                            <?php echo $form->textField($model, 'title', array('class' => 'form-control','placeholder'=>"Title*")); ?>
                         </div>
                     </div>
                 </div><!-- .row -->
                 <div class="row">
                     <div class="col-sm-12">
                         <div class="form-group">
                             <?php echo $form->textField($model, 'location', array('class' => 'form-control location-search', 'placeholder'=>"Location*")); ?>
                            <input id="location_lat" name="Jobs[location_lat]" type="hidden" value="<?php echo $model->location_lat; ?>">
                            <input id="location_lng" name="Jobs[location_lng]" type="hidden" value="<?php echo $model->location_lng; ?>">
                         </div>
                     </div>
                 </div><!-- .row -->
                 <div class="row">
                     <div class="col-sm-6">
                         <div class="form-group">
                             <select name="Jobs[type]" class="form-control">
                                    <option value="">Select</option>
                                    <option value="Permanent" <?php echo ($model->type ==  'Permanent' ? "selected" : "");?>>Permanent</option>
                                    <option value="Temporary" <?php echo ($model->type ==  'Temporary' ? "selected" : "");?>>Temporary</option>
                                    <option value="Fixed Term Contract" <?php echo ($model->type ==  'Fixed Term Contract' ? "selected" : "");?>>Fixed Term Contract</option>
                                    <option value="Work Experience" <?php echo ($model->type ==  'Work Experience' ? "selected" : "");?>>Work Experience</option>
                                    <option value="Apprenticeships" <?php echo ($model->type ==  'Apprenticeships' ? "selected" : "");?>>Apprenticeships</option>
                                
                                </select>
                         </div>
                     </div>
                     <div class="col-sm-6">
                         <div class="form-group">
                                <select name="industry_id" class="form-control select2">
                                   <option selected="selected" disabled>Select Industry</option>
                                     <?php
                                    $industrymodel = Industries::model()->findAll();
                                    foreach ($industrymodel as $ind) {
                                        ?>                  
                                        <option value="<?php echo $ind['id'] ?>" <?php echo ($model->industry_id==$ind['id']?"selected":"") ?>> <?php echo $ind['name'] ?> </option>
<?php } ?>
                                </select>
                         </div>
                     </div>
                 </div><!-- .row -->
                 <div class="row">
                     <div class="col-sm-6">
                         <div class="form-group">
                             <input type="text" name="end_date" autocomplete='off' class="datepicker form-control" value="<?php echo $model->end_date; ?>">
                                                    
                             
                         </div>
                     </div><!-- .col-sm-6 -->
                 </div><!-- .row -->

                 <div class="form-salary">
                     <div class="row">
                         <div class="col-sm-4">
                             
                             <div class="form-group">
                             <h4>Salary Type</h4>
                                 <div class="radio-inline">
                                     <label>
                                         <input name="salary_type" type="radio" value="annual" <?php echo ($model->salary_type=='annual' ? "checked" : "") ?>>
                                         Annual
                                     </label>
                                 </div>
                                 <div class="radio-inline">
                                     <label>
                                         <input name="salary_type" type="radio" value="hourly" <?php echo ($model->salary_type=='hourly' ? "checked" : "") ?>>
                                         Hourly
                                     </label>
                                 </div>
                             </div>
                         </div>
                     </div>
                      <div class="row">
                         <div class="col-sm-4">                             
                             <div class="form-group">
                             			<label>
                                            <input name="competitive_salary" type="checkbox" id="competitive_salary" <?php echo ($model->competitive_salary == 1 ? "checked" : "") ?>>
                                            Competitive Salary
                                        </label>
                             </div>
                         </div>
                     </div>
                     
                     <div class="row">
                         <div class="col-sm-4">
                             <div class="form-group">
                                 <div class="input-group">
                                     <span class="input-group-addon">&pound;</span>
                                     <?php echo $form->textField($model, 'salary_min', array('class' => 'form-control', 'placeholder' => 'Min Salary','id'=>'min_salary','disabled'=>($model->competitive_salary == 1 ? true : false))); ?>
                                     <?php echo $form->error($model, 'salary_min'); ?>
                                 </div>
                             </div>
                         </div>
                         <div class="col-sm-4">
                             <div class="form-group">
                                 <div class="input-group">
                                     <span class="input-group-addon">&pound;</span>
                                     <?php echo $form->textField($model, 'salary_max', array('class' => 'form-control','placeholder' => 'Max Salary','id'=>'max_salary','disabled'=>($model->competitive_salary == 1 ? true : false))); ?>
                                     <?php echo $form->error($model, 'salary_max'); ?>
                                 </div>
                             </div>
                         </div>
                     </div><!-- .row -->
                 </div><!-- .form-salary -->

             </div>
         </div>
     </div><!-- .form-container -->

     <div class="form-container">
        
         <div class="row">
             <div class="col-sm-6">
                 <div class="form-group">
                  <h4>Required Industry Experience</h4>
                     <select name="Jobs[skill_category_id]" id="skill_category_id" class="form-control">
                                <option value="">Select</option>
                                 <?php
                                $skillcategmodel = SkillCategories::model()->findAll();
                                foreach ($skillcategmodel as $skillcat) {
                                    ?>                  
                                    <option value="<?php echo $skillcat['id'] ?>" <?php echo ($model->skill_category_id==$skillcat['id']?"selected":"") ?>> <?php echo $skillcat['name'] ?> </option>
<?php } ?>
                            </select>
                 </div>
             </div>
         </div>
         <div class="row">
             <div class="col-sm-6">
                 <div class="form-group">
                     <span>Please select a maximum of 5 career choices from the dropdown menu</span>
                     <?php 
                            $skillmodel = Skills::model()->findAllByAttributes(array('skill_category_id'=>$model->skill_category_id)); 
                            $seletedskills = CHtml::listData($jobskillmodel, 'skill_id', 'skill_id');
                            
                            ?>
                            <select name="skills[]" id="skills" class="form-control select2" multiple="">
                                <option value="">Select</option>
                                <?php 
                            foreach ($skillmodel as $mcat) { ?>
                            <option  value='<?php echo $mcat['id']; ?>' <?php echo (in_array($mcat['id'], $seletedskills) ? 'selected' : ''); ?>><?php echo $mcat['name']; ?></option>
                           <?php }
                                ?>
                    </select>
                    <!-- <div id="skills-block">
                    	<?php 
                    	   foreach ($jobskillmodel as $js)
                    	   {
                    	       $skil = Skills::model()->findByPk($js['skill_id']);
                    	?>
                            	<div class="skill-selected" id='<?php echo $js['skill_id'] ?>'>
                            		 <div><?php echo $skil->name ?></div>
                            		 <input name='<?php echo $js['skill_id'] ?>' value="<?php echo ($js['vacancies'] > 0 ? $js['vacancies'] : '') ?>" class="form-control" style="width:10%;display:inline" type="text">
                            		 <div style="display:inline">(No. of vacancies)</div>
                                </div>
                       <?php } ?>
                    </div>-->
                 </div>
             </div>
         </div>
     </div>
        <div class="form-container">
        
          <div class="row">
             <div class="col-sm-3">
                 <div class="form-group">
                  <h4>No. of vacancies</h4>
                     <?php echo $form->textField($model,'vacancies', array('empty' => 'No. of vacancies','class' => 'form-control','id' => 'vacancies')); ?>
                 </div>
        	 </div>
         </div>
     </div>
     <div class="row">
            <div class="col-sm-4">
            
                <div class="form-group">
                <h4>Upload Job Description file</h4>
                	 <label for="choose_jd_file" class="main-btn"><i class="fa fa-file"></i> Choose File</label>
            		 <input id="choose_jd_file" name="jdfile" type="file" accept=".doc,.docx,.pdf,.txt" style="display: none">
             	</div>
            </div>
         </div>
       <div class="form-container">
        
          <div class="row">
             <div class="col-sm-3">
                 <div class="form-group">
                  <h4>Candidates with videos only</h4>
                     <?php echo CHtml::checkBox('video_only',($model->video_only == 1 ? true : false),array()); ?>
                 </div>
        	 </div>
         </div>
     </div>
     <div class="form-container">
         
         <!-- Description form field -->
         <div class="form-group">
         <h4>Job Description</h4>
             <?php echo $form->textArea($model,'description',  array('empty' => 'Click ball to enter/edit comments',  'id' => 'comment_box')); ?>
         </div>
     </div>

     <div class="form-container">
        
         
         <div class="form-group">
          <h4>Required & Desired</h4>
          <span>For example, number of years experience, qualifications, languages etc.</span>
             <?php echo $form->textArea($model,'additional',array('empty' => 'Click ball to enter/edit comments', 'id' => 'comment_box')); ?>
         </div>
     </div>

     <div class="form-actions">
         <?php 
         echo CHtml::submitButton('Save', array("class" => "btn btn-primary")); 
         echo CHtml::button('Back',array("class"=>"btn btn-default","onclick"=>"window.location='".Yii::app()->createUrl('jobs/admin')."'"));
         ?>
     </div> 

     <?php $this->endWidget(); ?>
     
     
 </div>

    <script>
        $(document).ready(function(){
        $('body').on('change','#skill_category_id',function(e){	
                var id = $(this).val();
                e.preventDefault();
                $.ajax({
                   url: '<?php echo $this->createUrl('jobs/loadSkills') ?>',
                   type: 'POST',
                   data: {id : id},
                   success: function(data) {
//                       console.log(data);
                           $('#skills').empty(); //remove all child nodes
                           $('#skills').append(data);
                           $('#skills').trigger('liszt:updated');
                   },
                    error: function(data) {		
                        alert('data');
                    }
               });
        });
        });
    </script>
     
  				
