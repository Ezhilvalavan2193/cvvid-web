<?php
/* @var $this JobsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Jobs',
);

?>

<div id="page-content">
<div class="container">
 <h1 align="center">Jobs by Category</h1>
 <hr>
<ul class="job-by-list clearfix">
<?php 
    $catmodel = SkillCategories::model()->findAll();
    foreach ($catmodel as $cat):
    $catname = str_replace("/", "-", str_replace(" ", "-", trim($cat['name'])));
?> 
       <li class="jobs-li"> <a href="<?php echo $this->createUrl('site/searchCategory',array('category'=>$catname)) ?>"><?php echo $cat['name']." Jobs" ?> </a></li>
       
<?php endforeach; ?>   
 </ul>
 </div></div>
    