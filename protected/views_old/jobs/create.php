<?php
/* @var $this JobsController */
/* @var $model Jobs */

$this->menu=array(
//	array('label'=>'List Jobs', 'url'=>array('index')),
	array('label'=>'Manage Jobs', 'url'=>array('admin')),
);
?>
 <script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
  <!-- <script type="text/javascript">
        tinymce.init({
            selector: 'textarea',
            plugins: 'pagebreak code link',
            pagebreak_split_block: true,
            min_height: 300,
            link_list: [
                {title: 'Home', value: 'http://www.cvvid.com'},
                {title: 'Login', value: 'http://www.cvvid.com/auth/login'},
                {title: 'About Us', value: 'http://www.cvvid.com/about-us'},
                {title: 'Worry List', value: 'http://www.cvvid.com/worry-list'},
                {title: 'How To', value: 'http://www.cvvid.com/how-to'},
                {title: 'Employers', value: 'http://www.cvvid.com/employers'},
                {title: 'Top Tips', value: 'http:///www.cvvid.com/top-tips'},
                {title: 'Success Stories', value: 'http://www.cvvid.com/success-stories'},
                {title: 'Contact Us', value: 'http:///www.cvvid.com/contact-us'},
                {title: 'Candidate Register', value: 'http://www.cvvid.com/candidate/register'},
                {title: 'Employer Register', value: 'http://www.cvvid.com/employer/register'}
            ],
            link_class_list: [
                {title: 'None', value: ''},
                {title: 'Yellow Text', value: 'yellow'},
                {title: 'Default', value: 'btn btn-default'},
                {title: 'Primary', value: 'btn btn-primary'}
            ]
        });
    </script> -->
<?php

$this->renderPartial('_form', array('model'=>$model,/* 'empmodel'=>$employermodel, */'industrymodel'=>$industrymodel,/* 'skillcategorymodel'=>$skillcategmodel,'skillmodel'=>$skillsmodel, */'jobskillmodel'=>$jobskillmodel)); 
?>