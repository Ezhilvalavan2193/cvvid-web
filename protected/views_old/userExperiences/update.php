<?php
/* @var $this UserExperiencesController */
/* @var $model UserExperiences */

$this->breadcrumbs=array(
	'User Experiences'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List UserExperiences', 'url'=>array('index')),
	array('label'=>'Create UserExperiences', 'url'=>array('create')),
	array('label'=>'View UserExperiences', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage UserExperiences', 'url'=>array('admin')),
);
?>

<h1>Update UserExperiences <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>