<?php
/* @var $this UserExperiencesController */
/* @var $model UserExperiences */

$this->breadcrumbs=array(
	'User Experiences'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List UserExperiences', 'url'=>array('index')),
	array('label'=>'Create UserExperiences', 'url'=>array('create')),
	array('label'=>'Update UserExperiences', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete UserExperiences', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage UserExperiences', 'url'=>array('admin')),
);
?>

<h1>View UserExperiences #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'start_date',
		'end_date',
		'location',
		'company_name',
		'industry',
		'job_role',
		'position',
		'description',
		'created_at',
		'updated_at',
		'deleted_at',
	),
)); ?>
