<?php
/* @var $this UserExperiencesController */
/* @var $model UserExperiences */

$this->breadcrumbs=array(
	'User Experiences'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List UserExperiences', 'url'=>array('index')),
	array('label'=>'Manage UserExperiences', 'url'=>array('admin')),
);
?>

<h1>Create UserExperiences</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>