<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>
<div class="page-title text-left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>    Edit Profile Details
                </h1>
            </div>
        </div>
    </div>
</div>
<div id="page-content">
    <div class="container">
        <?php $form=$this->beginWidget('CActiveForm', array(
                'id'=>'users-acc-form',
                'action'=>$this->createUrl('users/updateAccountInfo',array('id'=>$model->id)),
                'enableAjaxValidation'=>false,
        )); ?>
        <?php echo  $form->errorSummary(array($model,$pmodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
            <div class="form-container">
                <!-- Your details -->
                <h4>Your details</h4>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input class="form-control" <?php echo ($model->type == 'student' ? 'readonly' : '') ?> placeholder="First Name" name="Users[forenames]" id="Users_forenames" type="text" maxlength="255" value="<?php echo $model->forenames; ?>" />
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                             <input class="form-control" <?php echo ($model->type == 'student' ? 'readonly' : '') ?> placeholder="Surname" name="Users[surname]" id="Users_surname" type="text" maxlength="255" value="<?php echo $model->surname; ?>" /> 
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <div class="form-group">
                            <input type="text" name="dob" <?php echo ($model->type == 'student' ? 'disabled' : '') ?> autocomplete='off' class="datepicker form-control" placeholder="DOB" value="<?php echo ((empty($model->dob) || $model->dob == null || $model->dob == "0000-00-00") ? "" : date("d/m/Y", strtotime($model->dob))); ?>">
                 
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input class="form-control location-search" <?php echo ($model->type == 'student' ? 'readonly' : '') ?> placeholder="Address" name="Users[location]" id="Users_location" type="text" maxlength="255" value="<?php echo $model->location; ?>" /> 
                            <input id="location_lat" name="location_lat" type="hidden" value="">
                            <input id="location_lng" name="location_lng" type="hidden" value="">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <input class="form-control" <?php echo ($model->type == 'student' ? 'readonly' : '') ?> placeholder="E-mail Address" name="Users[email]" id="Users_email" type="text" maxlength="255" value="<?php echo $model->email; ?>" />  
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <div class="form-group">
                                <?php echo $form->textField($model,'current_job',array('class'=>'form-control','placeholder'=>'Current Job Title')); ?>
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-3">
                        <div class="form-group">
                            <div class="form-group">
                                <?php echo $form->textField($model,'tel',array('class'=>'form-control','placeholder'=>'Contact Number')); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="form-container">
                <!-- Custom url -->
                <h4>Custom URL</h4>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-inline slug-field">
                            <div class="form-group"><?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'; ?></div>
                            <div class="form-group">
                                <?php echo $form->textField($pmodel,'slug',array('class'=>'form-control','readonly'=>true)); ?>
                            </div>
                            <div class="form-group">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="form-container">
                <!-- Privacy -->
                <h4>Privacy Settings</h4>
                <div class="row">
                    <div class="col-sm-6">
                        <label for="visibility">
                            <?php echo CHtml::radioButton('status',true,array('value'=>'0','class'=>'')); ?>
                            Private
                        </label>
                        <p><?php echo "Only those who know your profile can find your account."; ?></p>
                    </div>
                    <!-- <div class="col-sm-6">
                        <label for="visibility">
                            <?php //echo CHtml::radioButton('status',($pmodel->visibility == 1),array('value'=>'1','class'=>'')); ?>
                            Public
                        </label>
                        <p><?php echo "Your profile can be found by employer searches."; ?></p>
                    </div> -->
                </div>
            </div>

            <div class="form-container">
                <!-- Update password -->
                <h4>Update Password</h4>
                <div class="row">
                    <!-- <div class="col-sm-4">                   
                        <input class="form-control" placeholder="Current Password" name="current_password" type="password" value="">
                    </div> -->
                    <div class="col-sm-4">
                      <?php 
                      $model->password = "";
                      $model->confirmpassword = "";
                      echo $form->passwordField($model,'password',array('class'=>'form-control','placeholder'=>"New Password")); ?>
<!--                         <input class="form-control" placeholder="New Password" name="password" type="password" value=""> -->
                    </div>
                    <div class="col-sm-4">
                      <?php echo $form->passwordField($model,'confirmpassword',array('class'=>'form-control','placeholder'=>"Confirm New Password")); ?>
<!--                         <input class="form-control" placeholder="Confirm New Password" name="password_confirmation" type="password" value=""> -->
                    </div>
                </div>
            </div>

            <div class="form-actions">
                <?php echo CHtml::submitButton('Update',array("class"=>"btn main-btn")); ?>
		<a href="<?php echo $this->createUrl('users/edit',array('slug'=>$pmodel->slug))?>" class="btn default-btn">Cancel</a>
            </div>

       <?php $this->endWidget(); ?>
    </div>
</div>

