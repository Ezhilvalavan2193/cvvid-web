<?php $docmodel = UserDocuments::model()->findAllByAttributes(array('user_id' => $model->id)); ?>
<div class="profile-section-header">
    <div class="row">
        <div class="col-sm-8">
            <h4>Accreditations and Portfolio</h4>
        </div>
        <div class="col-sm-4">
            <div class="pull-right">
                <button class="add-new main-btn add-new-doc">Add</button>
            </div>
        </div>
    </div>
</div>
<div class="profile-section-content">
    <div class="profile-section-info" <?php if(count($docmodel) > 0) { ?>style="display: none;" <?php } ?>>
        <p>What's your educational background?</p>
        <p>Tell employers more about your education; remember, be clear and concise.</p>
    </div>
    <div class="form-profile new-document-form" style="display: none;">
        <div class="document-form">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'new-doc-form',
                'enableAjaxValidation' => false,
                'htmlOptions' => array('enctype' => 'multipart/form-data','class' => 'form-horizontal'),
            ));
            ?>
            <!-- Name -->
            <div class="form-group">
                <label class="col-sm-3">Name</label>
                <div class="col-sm-9">
                    <input type="hidden" name="user_id" value="<?php echo $model->id ?>">
                    <input type="text" class="form-control" name="doc_name" value="" placeholder="Name" required="required">
                </div>
            </div>
            <!-- Category -->
            <div class="form-group">
                <label class="col-sm-3">Category</label>
                <div class="col-sm-9">
                    <select name="category_id" id="document_category" class="form-control" required="required">

                        <option value="1">Certificates</option>

                        <option value="2">Qualifications</option>

                        <option value="3">Training skills/courses</option>

                        <option value="4">References</option>

                    </select>
                </div>
            </div>
            <!-- Published -->
            <div class="form-group">
                <label class="col-sm-3">Published</label>
                <div class="col-sm-9">
                    <select name="published" id="Published" class="form-control">
                        <option value="0">Unpublished</option>
                        <option value="1">Published</option>
                    </select>
                </div>
            </div>
            <!-- File -->
            <div class="form-group">
                <label class="col-sm-3">File</label>
                <div class="col-sm-9">
                    <label for="choose_file" class="main-btn"><i class="fa fa-file"></i>Choose File</label>
                    <input id="choose_file" name="doc_file" type="file" style="display: none">
                    <p style="display: inline-block;" class="file-name">Please select a file.</p>
                     <script>
                    $(document).ready(function(){
                        $('input[type="file"]').change(function() {
                          if ($(this).val()) {
                               var filename = $(this).val();
                               $('.file-name').html(filename);
                          }
                        });
                      });
                    </script>
                </div>
            </div>
            <div class="form-actions">
                <input type="submit" class="main-btn" id="new_doc_save" value="Save">
                <input type="button" class="cancel-btn new-doc-cancel" value="Cancel">
            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
    <div class="document-list">
        <?php

        foreach ($docmodel as $doc) {
            $categmodel = MediaCategories::model()->findByPk($doc['category_id']);
            ?>


            <div class="document-item">
                <div class="document-item-inner" style="display: block;">
                    <div class="row">
                        <div class="col-sm-5">
                            <label class="document-label">Title:</label>
                            <span class="document-title"><?php echo $doc['name'] ?></span>
                        </div>
                        <div class="col-sm-5">
                            <label class="document-label">Category:</label>
                            <span class="document-category"><?php echo $categmodel->name ?></span>
                        </div>
                        <div class="col-sm-2">
                            <div class="document-options">
                                <ul class="list-inline">
                                    <li><a href="#" class="edit-item edit-doc" id="<?php echo $doc['id'] ?>">Edit</a></li>
                                    <li><a href="#" class="delete-item delete-doc" id="<?php echo $doc['id'] ?>"> Delete</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="edit-document-form-<?php echo $doc['id'] ?>" style="display: none;">
                    <div class="document-form">
                        <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'edit-doc-form',
                            'enableAjaxValidation' => false,
                            'htmlOptions' => array( 'class' => 'form-horizontal','enctype' => 'multipart/form-data'),
                        ));
                        ?>
                        <!-- Name -->
                        <div class="form-group">
                            <label class="col-sm-3">Name</label>
                            <div class="col-sm-9">
                                <?php echo CHtml::hiddenField('id', $doc['id'], array()); ?>
                                <input type="text" class="form-control" name="name" value="<?php echo $doc['name'] ?>">
                            </div>
                        </div>
                        <!-- Category -->
                        <div class="form-group">
                            <label class="col-sm-3">Category</label>
                            <div class="col-sm-9">
                                <select name="category_id" id="document_category" class="form-control">

                                    <?php
                                    $mediacatmodel = MediaCategories::model()->findAll();
                                    foreach ($mediacatmodel as $mcat) {
                                        ?>                  
                                        <option <?php echo ($mcat['id'] == $doc['category_id'] ? "selected" : "") ?> value="<?php echo $mcat['id'] ?>"> <?php echo $mcat['name'] ?> </option>

                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <!-- Published -->
                        <div class="form-group">
                            <label class="col-sm-3">Published</label>
                            <div class="col-sm-9">
                                <select name="published" id="Published" class="form-control">
                                    <option value="0" <?php echo ($doc['published'] == 0 ? "selected" : "") ?>>Unpublished</option>
                                    <option value="1" <?php echo ($doc['published'] == 1 ? "selected" : "") ?>>Published</option>
                                </select>
                            </div>
                        </div>
                          <!-- File -->
                        <div class="form-group">
                            <label class="col-sm-3">File</label>
                            <div class="col-sm-9">
                                <?php $mediadocmodel = Media::model()->findByPk($doc['media_id']); ?>
                                    
<!--                                     <label for="edit_choose_file" class="main-btn"><i class="fa fa-file"></i>Choose File</label> -->
                                    <input id="edit_choose_file" name="doc_file" type="file">
                             <!--       <p style="display: inline-block;" class="file-name"><?php //echo $mediadocmodel->file_name ?></p>-->
                                    
                                <?php echo CHtml::hiddenField('media_id', $doc['media_id'], array()); ?>
                                     <script>
//                                     $(document).ready(function(){
//                                         $('input[type="file"]').change(function() {
//                                           if ($(this).val()) {
//                                                var filename = $(this).val();
//                                                $('.file-name').html(filename);
//                                           }
//                                         });
//                                       });
                                    </script>
                              
                               
                            </div>
                        </div>
                        <div class="form-actions">
                            <input type="submit" class="main-btn doc_edit_save" id="<?php echo $doc['id'] ?>"  value="Save">
                            <button type="button" class="cancel-btn doc_edit_cancel" id="<?php echo $doc['id'] ?>">Cancel</button>
                        </div>
                        <?php $this->endWidget(); ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>




