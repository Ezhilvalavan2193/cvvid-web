<?php  $langmodel = UserLanguages::model()->findAllByAttributes(array('user_id' => $model->id)); ?>
<div class="profile-section-header">
    <div class="row">
        <div class="col-sm-8">
            <h4>Language</h4>
        </div>
        <div class="col-sm-4">
            <div class="pull-right">
                <button class="add-new main-btn add-new-lang">Add</button>
            </div>
        </div>
    </div>
</div>
<div class="profile-section-content">
    <div class="profile-section-info" <?php if(count($langmodel) > 0) { ?>style="display: none;" <?php } ?>>
        <p>Do you speak more than one language?</p>
        <p>For some jobs fluency in one or more foreign languages is a plus, so add your language skills to get better results.</p>
    </div>
    <div class="new-language-form form-profile" style="display: none;">
        <div>
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'new-lang-form',
                'enableAjaxValidation' => false,
                'htmlOptions' => array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'),
            ));
            ?>
            <div class="form-group">
                <label class="col-md-3">Language</label>
                <div class="col-md-9">
                    <input type="hidden" name="user_id" value="<?php echo $model->id ?>">
                    <input type="text" class="form-control" name="name" value="">
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3">Proficiency</label>
                <div class="col-md-5">
                    <select name="proficiency" id="proficiency" class="form-control">
                        <option value="Basic">Basic</option>
                        <option value="Intermediate">Intermediate</option>
                        <option value="Expert">Expert</option>
                    </select>
                </div>
            </div>
            <div class="form-actions">
                <button type="submit" class="btn btn-primary">Save</button>
                <button type="button" class="cancel-btn new-lang-cancel">Cancel</button>

            </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
    <div class="languages-list">
        <?php
       
        foreach ($langmodel as $lang) {
            ?>
            <div class="language-item profile-item">
                <div class="language-item-inner-<?php echo $lang['id'] ?>" style="display: block;margin-top:20px">

                    <div class="profile-item-title">
                        <?php echo $lang['name'] ?> <a class="edit-lang edit-item" href="#" id="<?php echo $lang['id'] ?>"><i class="fa fa-pencil"></i>Edit</a>
                    </div>
                    <div>
                        <?php echo $lang['proficiency']; ?>
                    </div>
                </div>
                <div class="edit-language-form-<?php echo $lang['id'] ?>" style="display: none;">
                    <div>
                        <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'edit-lang-form',
                            'enableAjaxValidation' => false,
                            'htmlOptions' => array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data'),
                        ));
                        ?>
                                <div class="form-group">
                                    <label class="col-md-3">Language</label>
                                    <div class="col-md-9">
                                         <?php echo CHtml::hiddenField('id', $lang['id'], array()); ?>
                                        <input type="text" class="form-control" name="name" value="<?php echo $lang['name'] ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3">Proficiency</label>
                                    <div class="col-md-5">
                                        <select name="proficiency" id="proficiency" class="form-control">
                                            <option value="Basic" <?php echo $lang['proficiency'] == "Basic" ? "selected" : "" ?> >Basic</option>
                                                <option value="Intermediate" <?php echo $lang['proficiency'] == "Intermediate" ? "selected" : "" ?>>Intermediate</option>
                                                <option value="Expert" <?php echo $lang['proficiency'] == "Expert" ? "selected" : "" ?>>Expert</option>
                                            </select>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                    <button type="button" class="cancel-btn lang_edit_cancel" id="<?php echo $lang['id'] ?>">Cancel</button>

                                    <button type="button" class="delete-btn lang-delete" id="<?php echo $lang['id'] ?>">Delete</button>

                                </div>
                        <?php $this->endWidget(); ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

