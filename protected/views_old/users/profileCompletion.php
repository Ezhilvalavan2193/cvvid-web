<div id="ProfileCompletion">
            <div class="ProfileCompletion__label">Profile Completion</div>            
            <div class="ProfileCompletion__bar progress">
                <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $percentage; ?>%;">
                    <div class="progress-score"><?php echo $percentage; ?>%</div>
                </div>
            </div>
        </div>    