<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs = array(
    'Users' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
		
");
?> 
<?php $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $model->id, 'type' => $model->type)); ?>
<script>
    function getpercentage()
    {
        $.ajax({
            url: '<?php echo $this->createUrl('users/getPercentage', array('id' => $profilemodel->id)) ?>',
            type: 'POST',
            async: false,
            success: function (data) {
                $('#profile-completion').html(data);
            },
            error: function (data) {
                //  alert(data);
            }
        });
    }
    function isVideo(filename) {
        var ext = getExtension(filename);
        switch (ext.toLowerCase()) {
            case 'm4v':
            case 'avi':
            case 'mpg':
            case 'mp4':
            case 'webm':
            case 'mov':
                return true;
        }
        return false;
    }
    function getExtension(filename) {
        var parts = filename.split('.');
        return parts[parts.length - 1];
    }
    $(document).ready(function () {
    	$(document).on('click', '.add-new-doc,.new-doc-cancel', function () {
            $('.new-document-form').slideToggle("slow");
            return false;
        });

    	$(document).on('click', '.edit-doc', function () {
            var id = $(this).attr('id');
            var name = 'edit-document-form-' + id;
            $('.' + name).toggle();
            return false;
        });

    	$(document).on('submit', '#new-doc-form', function (e) {
            e.preventDefault();
            var formdata = new FormData(this);
            $.ajax({
                url: '<?php echo $this->createUrl('users/saveUserDoc') ?>',
                type: 'POST',
                data: formdata,
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    $('#profile-documents').html(data);
                    getpercentage();
                },
                error: function (data) {
                    //  alert(data);
                }
            });

            return false;
        });

    	$(document).on('submit', '#edit-doc-form', function (e) {
            e.preventDefault();
            var formdata = new FormData(this);
            $.ajax({
                url: '<?php echo $this->createUrl('users/updateUserDoc') ?>',
                type: 'POST',
                data: formdata,
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    $('#profile-documents').html(data);
                },
                error: function (data) {
                    alert(data);
                }
            });

            return false;
        });

    	$(document).on('click', '.doc_edit_cancel', function (e) {
            var id = $(this).attr('id');
            $('.edit-document-form-' + id).toggle();
        });

    	$(document).on('click', '.delete-doc', function (e) {
            var id = $(this).attr('id');
            e.preventDefault();
            $.ajax({
                url: '<?php echo $this->createUrl('users/deleteUserDoc') ?>',
                type: 'POST',
                data: {id: id},
                success: function (data) {
                    $('#profile-documents').html(data);
                    getpercentage();
                },
                error: function (data) {
                    alert(data);
                }
            });
        });


    	$(document).on('click', '.add-new-exp,.new_exp_cancel', function () {
            $('.new-experience-form').slideToggle('slow');
            return false;
        });

    	$(document).on('click', '.edit-exp,.exp_edit_cancel', function () {
            var id = $(this).attr('id');
            var name = 'experience-form-' + id;
            $('.' + name).slideToggle('slow');
            $('.experience-inner-' + id).slideToggle('slow');
            return false;
        });

    	$(document).on('submit', '#new-exp-form', function (e) {
            if ($('#start_year').val() <= $('#end_year').val() || $('#is_current').is(':checked'))
            {
                if ($('#start_year').val() == $('#end_year').val() && $('#start_month').val() > $('#end_month').val() && !$('#is_current').is(':checked'))
                {
                    alert('End month should be greater than start month');
                } 
                else
                {
                    e.preventDefault();

                    var formdata = new FormData(this);
                    $.ajax({
                        url: '<?php echo $this->createUrl('users/saveUserExp') ?>',
                        type: 'POST',
                        data: formdata,
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            $('#professional-experience').html(data);
                            getpercentage();
                        },
                        error: function (data) {
                            alert(data);
                        }
                    });
                }
            } else
            {
                alert('End year should be greater than start year');
            }
            return false;
        });

    	$(document).on('submit', '#edit-exp-form', function (e) {
            if ($('#start_year').val() <= $('#end_year').val() || $('#is_current').is(':checked'))
            {
                if ($('#start_year').val() == $('#end_year').val() && $('#start_month').val() > $('#end_month').val() && !$('#is_current').is(':checked'))
                {
                    alert('End month should be greater than start month');
                } 
                else
                {
                    e.preventDefault();
                    var formdata = new FormData(this);
                    $.ajax({
                        url: '<?php echo $this->createUrl('users/updateUserExp') ?>',
                        type: 'POST',
                        data: formdata,
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (data) {
                            $('#professional-experience').html(data);
                        },
                        error: function (data) {
                            alert(data);
                        }
                    });
                }
            } else
            {
                alert('End year should be greater than start year');
            }


            return false;

        });

    	$(document).on('click', '.exp_edit_cancel', function (e) {
            var id = $(this).attr('id');
            $('.edit-exp-form-' + id).toggle();
        });

    	$(document).on('click', '.exp-delete', function (e) {
            var id = $(this).attr('id');
            e.preventDefault();
            $.ajax({
                url: '<?php echo $this->createUrl('users/deleteUserExp') ?>',
                type: 'POST',
                data: {id: id},
                success: function (data) {
                    $('#professional-experience').html(data);
                    getpercentage();
                },
                error: function (data) {
                    alert(data);
                }
            });
        });


    	$(document).on('click', '.add-new-edu,.new-edu-cancel', function () {
            $('.new-education-form').slideToggle('slow');
            return false;
        });

    	$(document).on('click', '.edit-edu', function () {

            var id = $(this).attr('id');
            var name = 'edit-education-form-' + id;
            $('.' + name).slideToggle('slow');
            $('.education-item-inner-' + id).slideToggle('slow');
            return false;
        });

    	$(document).on('submit', '#new-edu-form', function (e) {

            e.preventDefault();

            var formdata = new FormData(this);
            $.ajax({
                url: '<?php echo $this->createUrl('users/saveUserEducation') ?>',
                type: 'POST',
                data: formdata,
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    $('#profile-education').html(data);
                    getpercentage();
                },
                error: function (data) {
                    alert(data);
                }
            });

            return false;
        });

    	$(document).on('submit', '#edit-edu-form', function (e) {
            e.preventDefault();
            var formdata = new FormData(this);
            $.ajax({
                url: '<?php echo $this->createUrl('users/updateUserEducation') ?>',
                type: 'POST',
                data: formdata,
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    $('#profile-education').html(data);
                },
                error: function (data) {
                    alert(data);
                }
            });

            return false;
        });

    	$(document).on('click', '.edu_edit_cancel', function (e) {
            var id = $(this).attr('id');
            $('.edit-education-form-' + id).slideToggle('slow');
            $('.education-item-inner-' + id).slideToggle('slow');
        });

    	$(document).on('click', '.edu-delete', function (e) {
            var id = $(this).attr('id');
            e.preventDefault();
            $.ajax({
                url: '<?php echo $this->createUrl('users/deleteUserEducation') ?>',
                type: 'POST',
                data: {id: id},
                success: function (data) {
                    $('#profile-education').html(data);
                    getpercentage();
                },
                error: function (data) {
                    alert(data);
                }
            });
        });


    	$(document).on('click', '.add-new-hobby,.new-hobby-cancel', function () {
            $('.new-hobbies-form').slideToggle('slow');
            return false;
        });

    	$(document).on('click', '.edit-hobby', function () {

            var id = $(this).attr('id');
            $('.hobby-item-inner-' + id).slideToggle('slow');
            $('.edit-hobby-form-' + id).slideToggle('slow');
            return false;
        });

    	$(document).on('submit', '#new-hobby-form', function (e) {

            e.preventDefault();

            var formdata = new FormData(this);
            $.ajax({
                url: '<?php echo $this->createUrl('users/saveUserHobbies') ?>',
                type: 'POST',
                data: formdata,
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    $('#profile-hobbies').html(data);
                    getpercentage();
                },
                error: function (data) {
                    alert(data);
                }
            });

            return false;
        });

    	$(document).on('submit', '#edit-hobby-form', function (e) {
            e.preventDefault();
            var formdata = new FormData(this);
            $.ajax({
                url: '<?php echo $this->createUrl('users/updateUserHobbies') ?>',
                type: 'POST',
                data: formdata,
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    $('#profile-hobbies').html(data);
                },
                error: function (data) {
                    alert(data);
                }
            });

            return false;
        });

    	$(document).on('click', '.hobby_edit_cancel', function (e) {
            var id = $(this).attr('id');

            $('.edit-hobby-form-' + id).slideToggle('slow');
            $('.hobby-item-inner-' + id).slideToggle('slow');
            return false;
        });

    	$(document).on('click', '.hobby-delete', function (e) {
            var id = $(this).attr('id');
            e.preventDefault();
            $.ajax({
                url: '<?php echo $this->createUrl('users/deleteUserHobbies') ?>',
                type: 'POST',
                data: {id: id},
                success: function (data) {
                    $('#profile-hobbies').html(data);
                    getpercentage();
                },
                error: function (data) {
                    alert(data);
                }
            });
        });

    	$(document).on('click', '.add-new-lang,.new-lang-cancel', function () {
            $('.new-language-form').slideToggle('slow');
            return false;
        });

    	$(document).on('click', '.edit-lang,.lang_edit_cancel', function () {
            var id = $(this).attr('id');
            $('.edit-language-form-' + id).slideToggle('slow');
            $('.language-item-inner-' + id).slideToggle('slow');

            return false;
        });

    	$(document).on('submit', '#new-lang-form', function (e) {

            e.preventDefault();

            var formdata = new FormData(this);
            $.ajax({
                url: '<?php echo $this->createUrl('users/saveUserLanguages') ?>',
                type: 'POST',
                data: formdata,
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    $('#profile-languages').html(data);
                    getpercentage();
                },
                error: function (data) {
                    alert(data);
                }
            });

            return false;
        });

    	$(document).on('submit', '#edit-lang-form', function (e) {
            e.preventDefault();
            var formdata = new FormData(this);
            $.ajax({
                url: '<?php echo $this->createUrl('users/updateUserLanguages') ?>',
                type: 'POST',
                data: formdata,
                contentType: false,
                cache: false,
                processData: false,
                success: function (data) {
                    $('#profile-languages').html(data);
                    getpercentage();
                },
                error: function (data) {
                    alert(data);
                }
            });

            return false;
        });

    	$(document).on('click', '.lang_edit_cancel', function (e) {
            var id = $(this).attr('id');
            $('.edit-lang-form-' + id).toggle();
        });

    	$(document).on('click', '.lang-delete', function (e) {
            var id = $(this).attr('id');
            e.preventDefault();
            $.ajax({
                url: '<?php echo $this->createUrl('users/deleteUserLanguages') ?>',
                type: 'POST',
                data: {id: id},
                success: function (data) {
                    $('#profile-languages').html(data);
                    getpercentage();
                },
                error: function (data) {
                    alert(data);
                }
            });
        });

    	$(document).on('click', '.add-new-skill,.new-skill-cancel', function () {
            $('#skills-form').slideToggle('slow');

            if ($(this).attr('class') == 'add-new main-btn add-new-skill')
            {
                $('.add-new-skill').hide();
                $('.save-new-skill').show();

            }
            return false;
        });

    	$(document).on('click', '.edit-skill', function () {
            var id = $(this).attr('id');
            var name = 'edit-skill-form-' + id;
            $('.' + name).toggle();
            return false;
        });

    	$(document).on('click', '.skill-category', function () {
            var id = $(this).attr('id');
            var name = 'skill-category-inner-' + id;
            $('.save-new-skill').attr('id', id);
            $('.' + name).slideToggle('slow');
            $('#skills-form').slideToggle('slow');
            return false;
            //$('.active-category-inner').slideToggle('slow');

            //return false;		
        });

    	$(document).on('click', '.skill-change', function () {

            var curr = $(this);
            var id = $(this).attr('id');
            var userid = $('#user_id').val();
            var action = "";

            var currclass = $.trim(curr.attr('class'));
            if (currclass == 'skill skill-change') {
                action = "add";
            } else if (currclass == 'skill skill-change selected') {
                action = "remove";
            }

            $.ajax({
                url: '<?php echo $this->createUrl('users/changeUserSkills') ?>',
                type: 'POST',
                data: {skill_id: id, user_id: userid, action: action},
                success: function (data) {
                    $(curr).toggleClass('selected');
                },
                error: function (data) {
                    alert('err');
                }
            });

            return false;
        });

    	$(document).on('click', '.save-new-skill,.CategoryEditFinish', function () {
            var id = $(this).attr('id');
            var userid = $('#user_id').val();

            $.ajax({
                url: '<?php echo $this->createUrl('users/renderUserSkills') ?>',
                type: 'POST',
                data: {user_id: userid},
                success: function (data) {
                    $('#profile-skills').html(data);
                    getpercentage();
                },
                error: function (data) {
                    alert('err');
                }
            });


            return false;
        });

    	$(document).on('click', '.edit-skill-categ', function () {

            var id = $(this).attr('id');

            $('.active-category#' + id + ' > .active-category-inner').hide();
            $('.active-category-form-' + id).slideToggle('slow');
            $('.CategoryEditFinish').attr('id', id);
            return false;
        });


    	$(document).on('click', '.visiblity', function (e) {
            e.preventDefault();
            var userid = $('#user_id').val();
            var status = $.trim($(this).text());

            $.ajax({
                url: '<?php echo $this->createUrl('users/changeUserVisiblity') ?>',
                type: 'POST',
                async: false,
                data: {userid: userid, status: status},
                success: function (data) {
                    // alert(data);
                    $(".ProfileEdit__operations").load(location.href + " .ProfileEdit__operations>*", "");
//	       		 if(status == "Public")			
//	                {            	        
//	                	$('.visiblity').text('');    	
//	            		$('.visiblity').append( '<i class="fa fa-lock"></i> Private' );
//	                }
//	                else
//	                {		       
//	                	$('.visiblity').text('');    	     	
//	                	$('.visiblity').append( '<i class="fa fa-unlock"></i> Public' );
//	                }

                },
                error: function (data) {
                    alert('err');
                }
            });
            return false;
        });

    	$(document).on('click', '.status', function (e) {
            e.preventDefault();
            var userid = $('#user_id').val();
            var status = $.trim($(this).text());

            $.ajax({
                url: '<?php echo $this->createUrl('users/changeUserStatus') ?>',
                type: 'POST',
                async: false,
                data: {userid: userid, status: status},
                success: function (data) {
                    $(".ProfileEdit__operations").load(location.href + " .ProfileEdit__operations>*", "");
//	       		 	if(status.toLowerCase() == "active")			
//	                {            	        
//	                	$('.status').text('');    	
//	            		$('.status').append( '<i class="fa fa-times"></i> inactive' );
//	                }
//	                else
//	                {		       
//	                	$('.status').text('');    	     	
//	                	$('.status').append( '<i class="fa fa-check"></i> Active' );
//	                }

                },
                error: function (data) {
                    alert('err');
                }
            });
            return false;
        });

        //configuration
        //var allowed_file_types = ['video/mp4', 'video/flv']; //allowed file types
        var result_output = '#output'; //ID of an element for response output
        var my_form_id = '#users-pvideo-form'; //ID of an element for response output
        var progress_bar_id = '.progress'; //ID of an element for response output

        //on form submit
//         $(document).ready(function () {

            //$(my_form_id).on("submit", function (event) {
            $(document).on('submit', my_form_id, function (e) {
                if ($('#NewVideo').val() == "")
                {
                    alert("Please select video.")
                    return false;
                } else
                {
                    var file = $('#NewVideo');
                    if (!isVideo(file.val())) {
                        alert('Please select a valid video file.');
                        return false;
                    }
                }
                event.preventDefault();
                var proceed = true; //set proceed flag
                var error = [];	//errors
                var total_files_size = 0;

                //reset progressbar
                $(progress_bar_id + " .progress-bar").css("width", "0%");
                $(progress_bar_id + " .status").text("0%");

                if (!window.File && window.FileReader && window.FileList && window.Blob) { //if browser doesn't supports File API
                    error.push("Your browser does not support new File API! Please upgrade."); //push error text
                } else {
                    //iterate files in file input field
                    proceed = true;
//                    $(this.elements['files'].files).each(function (i, ifile) {
//                        
//                        if (ifile.value !== "") { //continue only if file(s) are selected
//                           
//                            if (allowed_file_types.indexOf(ifile.type) === -1) { //check unsupported file
//                                alert("<b>" + ifile.name + "</b> is unsupported file type!"); //push error text
//                                proceed = false; //set proceed flag to false
//                            }
//
//                            total_files_size = total_files_size + ifile.size; //add file size to total size
//                        }
//                    });


                    var submit_btn = $(this).find("input[type=submit]"); //form submit button	

                    //if everything looks good, proceed with jQuery Ajax
                    if (proceed) {

                        submit_btn.val("Please Wait...").prop("disabled", true); //disable submit button
                        var form_data = new FormData(this); //Creates new FormData object
                        var post_url = $(this).attr("action"); //get action URL of form

                        //jQuery Ajax to Post form data
                        $.ajax({
                            url: post_url,
                            type: "POST",
                            data: form_data,
                            contentType: false,
                            cache: false,
                            processData: false,
                            xhr: function () {
                                //upload Progress
                                var xhr = $.ajaxSettings.xhr();
                                if (xhr.upload) {
                                    xhr.upload.addEventListener('progress', function (event) {
                                        var percent = 0;
                                        var position = event.loaded || event.position;
                                        var total = event.total;
                                        if (event.lengthComputable) {
                                            percent = Math.ceil(position / total * 100);
                                        }
                                        $(progress_bar_id).show();
                                        //update progressbar
                                        $(progress_bar_id + " .progress-bar").css("width", +percent + "%");
                                        $(progress_bar_id + " .status").text(percent + "%");

                                    }, true);
                                }
                                return xhr;
                            },
                            mimeType: "multipart/form-data"
                        }).done(function (res) { 
                            $(my_form_id)[0].reset(); //reset form
                            $(result_output).html(res); //output response from server
                            submit_btn.val("Upload Video").prop("disabled", false); //enable submit button once ajax is done
                            window.setTimeout(function () {
                                location.reload()
                            }, 2000);
                        });

                    }
                }

                $(result_output).html(""); //reset output 
                $(error).each(function (i) { //output any error to output element
                    $(result_output).append('<div class="error">' + error[i] + "</div>");
                });

            });
//         });


        $(document).on('change', '#NewVideo', function (e) {
            $('#video-name').val(e.target.files[0].name);
        });

        $(document).on('click', '.ProfileVideo__edit', function (e) {
            e.preventDefault();
            var id = $(this).attr('id');
            $('#ProfileVideo-' + id).slideToggle('slow');
//          $('#'+id+'.video-name-label').hide();
        });

        $(document).on('click', '.video-edit-close', function (e) {
            e.preventDefault();
            var id = $(this).attr('id');
            $('#ProfileVideo-' + id).slideToggle('slow');
        });

        $(document).on('click', '.video-edit-save', function (e) {

            var id = $(this).attr('id');
            var name = $('#' + id + '.evideo-name').val();

            $.ajax({
                url: '<?php echo $this->createUrl('users/updateProfileVideoName') ?>',
                type: 'POST',
                data: {id: id, name: name},
                success: function (data) {
                   $('.profile-videos-section').html(data);
                },
//                error: function (data) {
//                    alert('err');
//                }
            });

        });

        $(document).on('click', '.make_default', function (e) {

            e.preventDefault();
            var id = $(this).attr('id');

            $.ajax({
                url: '<?php echo $this->createUrl('users/profileVideoDefault') ?>',
                type: 'POST',
                data: {id: id},
                success: function (data) {
                  $('.profile-videos-section').html(data);
                },
//                error: function (data) {
//                    alert('err');
//                }
            });

        });

        $(document).on('click', '.video_delete', function (e) {

            e.preventDefault();
            var id = $(this).attr('id');

            $.ajax({
                url: '<?php echo $this->createUrl('users/profileVideoDelete') ?>',
                type: 'POST',
                data: {id: id},
                success: function (data) {
                     $('.profile-videos-section').html(data);
                    getpercentage();
                },
//                error: function (data) {
//                    alert('err');
//                };
            });

        });

        $(document).on('click', '.video_publish', function (e) {

            e.preventDefault();
            var id = $(this).attr('id');
            var status = $('#' + id + '.video_publish').text();

            $.ajax({
                url: '<?php echo $this->createUrl('users/profileVideoPublish') ?>',
                type: 'POST',
                data: {id: id, status: status},
                success: function (data) {
                    //location.reload();
                    $('.profile-videos-section').html(data);
                },
                
            });

        });
        $('body').on('click', '.open-media', function () {
            var getValue = $(this).attr("data-field");
            if (getValue == 'cover') {
                $(".saveMedia").attr('id', 'save-media-cover');
            } else if (getValue == 'photo') {
                $(".saveMedia").attr('id', 'save-media-photo');
            }

            //alert(getValue);return false;
            $('#mediamanager_modal').addClass('in');
            $('body').addClass('modal-open');
            $('#mediamanager_modal').show();
        });

        //  media_item_delete  
        $('body').on('click', '.MediaManager__item__delete', function (e) {

            if (confirm('Are you sure you want to delete this item?')) {

                var id = $(this).attr('id');

                $.ajax({
                    url: '<?php echo $this->createUrl('media/deleteMedia'); ?>',
                    type: 'POST',
                    data: {id: id},
                    success: function (data) {
                        //alert(data);
                        $(".MediaManager__content").load(location.href + " .MediaManager__content>*", "");
                        // $("#result_para").load(location.href + " #result_para>*", "");
                    },
//                    error: function (data) {
//                        alert('err');
//                    }
                });
            }

            return false;

        });



        $(document).on('click', '.medaiamanager_add', function () {
            //e.preventDefault();
            $(".media-file").trigger('click');
            //return false;
        });

        $('.modal-content').on('change', '.media-file', function (e) {

            var fileExtension = ['jpeg', 'jpg', 'png'];
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                alert("Only '.jpeg','.jpg','.png' formats are allowed.");
            } else {
                $('.MediaManager__loading').removeClass('hidden');
                var data = new FormData();
                jQuery.each(jQuery(this)[0].files, function (i, file) {
                    data.append('media', file);
                });
                data.append('collection_name', 'photos');
                data.append('model_type', "App\\Users\\Candidate");
                data.append('model_id', <?php echo $model->id; ?>);
                // alert(JSON.stringify(data));
                $.ajax({
                    url: '<?php echo $this->createUrl('media/insertMediaManager') ?>',
                    type: 'POST',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $(".MediaManager__content").load(location.href + " .MediaManager__content>*", "");
                        $('.MediaManager__loading').addClass('hidden');
                    },
//                    error: function (data) {
//                        alert('err');
//                    }
                });
            }

            // return false;
        });

        $('.modal-content').on('click', '.MediaManager__item', function (evt) {
            $('div.selected').removeClass('selected');
            $(this).addClass('selected');
        });

        $('.modal-content').on('click', '#save-media-cover', function (evt) {

            var mediaid = $(".MediaManager__content").find(".selected").attr('id');

            $.ajax({
                url: '<?php echo $this->createUrl('profiles/updateCover') ?>',
                type: 'POST',
                data: {mediaid: mediaid, profileid: <?php echo $profilemodel->id; ?>},
                success: function (data) {
                    window.location.reload();
                    // $("#colcover").load(location.href + " #colcover>*", "");
                    //  closepopup();
                },
//                error: function (data) {
//                    alert('err');
//                }
            });


        });


        $('.modal-content').on('click', '#save-media-photo', function (evt) {

            var mediaid = $(".MediaManager__content").find(".selected").attr('id');

            $.ajax({
                url: '<?php echo $this->createUrl('profiles/updatePhoto') ?>',
                type: 'POST',
                data: {mediaid: mediaid, profileid: <?php echo $profilemodel->id; ?>},
                success: function (data) {
                    window.location.reload();
                    // $("#colcover").load(location.href + " #colcover>*", "");
                    //  closepopup();
                },
//                error: function (data) {
//                    alert('err');
//                }
            });


        });

        $("#is_current").click(function () {

            if ($(this).is(':checked'))
            {
                $('#end_year').prop('disabled', true);
                $('#end_month').prop('disabled', true);
            } else
            {
                $('#end_year').prop('disabled', false);
                $('#end_month').prop('disabled', false);
            }
        });

        $(document).on('click', '#edit_is_current', function (evt) {
            // $("#edit_is_current").click(function(){
            //   alert('in');
            if ($(this).is(':checked'))
            {
                $('.end_year').prop('disabled', true);
                $('.end_month').prop('disabled', true);
            } else
            {
                $('.end_year').prop('disabled', false);
                $('.end_month').prop('disabled', false);
            }
        });

        $(document).on('click', '.hired', function (e) {
            e.preventDefault();
            var userid = $('#user_id').val();
            var status = $.trim($(this).text());
            $.ajax({
                url: '<?php echo $this->createUrl('users/changeHiredStatus') ?>',
                type: 'POST',
                async: false,
                data: {userid: userid, status: status},
                success: function (data) {
                    $(".ProfileEdit__operations").load(location.href + " .ProfileEdit__operations>*", "");

                },
                error: function (data) {
                    alert('err');
                }
            });
            return false;
        });
        
    });

</script>


<div id="profile">
    <?php
    $employerid = isset($this->employerid) ? $this->employerid : 0;
    $userid = isset($this->userid) ? $this->userid : 0;
    $institutionid = isset($this->institutionid) ? $this->institutionid : 0;
    $edit = isset($this->edit) ? $this->edit : 0;
    $this->renderPartial('//profiles/Cover', array('userid' => $userid, 'employerid' => $employerid, 'institutionid' => $institutionid, 'edit' => $edit));
    ?>
    <div id="form-details">
        <div class="container">
            <?php
            echo CHtml::hiddenField('user_id', $model->id, array('id' => 'user_id'));
            ?>
            <div class="form-details-inner">
                <div class="form-user-details form-section">
                  
                    <?php if (Yii::app()->user->hasFlash('subscription-ends')): ?>
                		<div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                             <?php echo Yii::app()->user->getFlash('subscription-ends'); ?>
                        </div>
                     <?php endif; ?>
                        
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="text-center col-sm-4 col-md-3 col-lg-2">
                            <div class="ProfileEdit__photo">
                                <?php
                                if ($profilemodel->photo_id > 0) {
                                    $mediamodel = Media::model()->findByPk($profilemodel->photo_id);
                                    $src = ((!empty($mediamodel->file_name) && $mediamodel->file_name != null) ? Yii::app()->baseUrl . "/images/media/" . $profilemodel->photo_id . "/" . $mediamodel->file_name : Yii::app()->baseUrl . "/images/profile.png");
                                } else {
                                    $src = Yii::app()->baseUrl . "/images/defaultprofile.jpg";
                                }
                                echo CHtml::image($src, "", array("id" => "profile-image", 'alt' => ''));
                                ?>
                                <a class="open-media profile-photo-btn" data-field="photo" data-target="#profile-image">
                                    <i class="fa fa-camera"></i>
                                    Change Photo
                                </a>
                            </div>                    
                            <a class="btn btn-primary" href="<?php echo $this->createUrl('users/profile', array('id' => $model->id)) ?>">
                                <i class="fa fa-search"></i>
                                <span>View Profile</span>
                            </a>
                        </div>
                        <div class="col-sm-5 col-md-5 col-lg-6">
                            <div class="ProfileEdit__details">
                                <h3>
                                    <?php echo $model->forenames ?> <?php echo $model->surname ?>
                                    <?php
                                    $trail = "";
                                    if ($model->trial_ends_at != null) {
                                        $trail = date('Y-m-d', strtotime($model->trial_ends_at));
                                        $expiry = date('d/m/Y', strtotime($model->trial_ends_at));
                                    }

                                    if (($model->trial_ends_at != null && Membership::onTrail($trail)) || $model->stripe_active > 0) {
                                        ?>
                                        - <small class="subscription-marker subscription-marker--premium">Premium Account</small>
                                    <?php } else { ?>
                                        - <small class="subscription-marker subscription-marker--basic">Basic Account</small>
                                    <?php } ?>
                                </h3>
                                <div><?php echo $model->current_job ?></div>
                                <div><?php echo $model->location ?></div>

                                <span class="profile-link">URL: <?php echo "http://" . $_SERVER['SERVER_NAME'] . '/' . $profilemodel->slug ?></span>

                                <ul class="ProfileEdit__details__meta list-inline">
                                    <?php
                                    $pcri = new CDbCriteria();
                                    $pcri->alias = 'pv';
                                    $pcri->join = 'inner join cv16_users u on u.id = pv.model_id';
                                    $pcri->condition = "pv.profile_id = ".$profilemodel->id;
                                    $promodel = ProfileViews::model()->findAll($pcri);
                                    
                                    $fcri = new CDbCriteria();
                                    $fcri->alias = 'pv';
                                    $fcri->join = 'inner join cv16_users u on u.id = pv.favourite_id';
                                    $fcri->condition = "pv.profile_id = ".$profilemodel->id;
                                    $pfmodel = ProfileFavourites::model()->findAll($fcri);
                                    if (Membership::onTrail($trail) || $model->stripe_active) {                                        
                                        ?>
                                        <li><i class="fa fa-eye"></i><a href="<?php echo $this->createUrl('users/profileViews', array('id' => $profilemodel->id)); ?>"><?php echo count($promodel) ?> Views</a></li>
                                        <li><i class="fa fa-heart"></i><a href="<?php echo $this->createUrl('users/favourites', array('id' => $profilemodel->id)); ?>"><?php echo count($pfmodel)  ?> Favourites</a></li>
                                    <?php } else {
                                        ?>
                                        <li><i class="fa fa-eye"></i><?php echo count($promodel)  ?> Views</li>
                                        <li><i class="fa fa-heart"></i><?php echo count($pfmodel)  ?> Favourites</li>
                                    <?php } ?>

                                </ul><!-- .ProfileEdit__details__meta -->
                            </div><!-- .ProfileEdit__details -->               
                        </div>
                        <div class="col-sm-3 col-md-4 col-lg-4">
                            <div class="ProfileEdit__operations">
                                <div>
                                    <?php
                                    $target = Yii::app()->controller->getRoute() . "/" . $model->id;
                                    if (($model->trial_ends_at == null && !$model->stripe_active)) {
                                        ?>
                                        <a class="btn btn-default btn-upgrade upgrade" href="<?php echo $this->createUrl('users/upgrade', array('target' => $target)) ?>">
                                            <i class="fa fa-user"></i>
                                            <span>Basic</span>
                                        </a>
                                        <?php
                                    } else {
                                        if (Membership::onTrail($trail) || $model->stripe_active) {
                                            ?>
                                            <div class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Expires on <?php echo $expiry; ?>">
                                                <i class="fa fa-credit-card"></i>
                                                <span>Premium</span>
                                            </div>
                                        <?php } else { ?>
                                            <a class="btn btn-default" href="<?php echo $this->createUrl('users/mysubscription') ?>">
                                                <i class="fa fa-credit-card"></i>
                                                <span>Account</span>
                                            </a>
                                        <?php
                                        }
                                    }
                                    ?>

                                    <?php if ($profilemodel->published) { ?>
                                        <a class="Profile__active btn status  btn-success" id="<?php echo $profilemodel->published ?>" href=""><i class="fa fa-check"></i>Active</a>
                                    <?php } else { ?>
                                        <a class="Profile__active btn status btn-default" id="<?php echo $profilemodel->published ?>" href=""><i class="fa fa-close"></i>Inactive</a>
<?php } ?>
                                </div>
                                <div>
                                    <?php if ($profilemodel->visibility) { ?>
                                        <a class="Profile__visibility btn visiblity  btn-success" id="<?php echo $profilemodel->visibility ?>" href="#"><i class="fa fa-unlock"></i>Public</a>
                                    <?php } else { ?>
                                        <a class="Profile__visibility btn visiblity btn-default" id="<?php echo $profilemodel->visibility ?>" href="#"><i class="fa fa-lock"></i>Private</a>
                                    <?php } ?>
                                    <?php // echo $model->stripe_active; ?>
                                    <?php
                                    if (($model->trial_ends_at != null && Membership::onTrail($trail)) || $model->stripe_active) {

                                        $convmem = ConversationMembers::model()->findAllByAttributes(array('user_id' => $profilemodel->owner_id, 'last_read' => null));
                                        ?>
                                        <a class="btn btn-default" href="<?php echo $this->createUrl('users/inbox', array('id' => $model->id)) ?>">
                                            <i class="fa fa-envelope"></i>
                                            <span>Inbox (<?php echo count($convmem) ?>)</span>
                                        </a>
<?php } ?>
								
                                </div>



                                <div>
                                    <a class="btn btn-default" href="<?php echo $this->createUrl('users/Profile', array('id' => $model->id)) ?>">
                                        <i class="fa fa-user"></i>
                                        <span>View</span>
                                    </a>
                                    <a class="btn btn-default" href="<?php echo $this->createUrl('users/userAccountInfo', array('id' => $model->id)) ?>">
                                        <i class="fa fa-pencil"></i>
                                        <span>Account</span>
                                    </a>
                                </div>
                                <div>
                                    <?php if ($profilemodel->hired) { ?>					
    									 <a class="btn btn-default hired btn-success" href="#"><i class="fa fa-check"></i><span>Hired</span></a>
    								<?php } else { ?>	 
    									 <a class="btn btn-default hired" href="#"><i class="fa fa-close"></i><span>Not Hired</span></a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-push-8 col-md-4">
                        <div class="form-completion form-module" id="profile-completion">
<?php 
$percentage = 0;
$profiledocs = ProfileDocuments::model()->findAllByAttributes(array('profile_id' => $profilemodel->id));
if (count($profiledocs) > 0)
    $percentage += '15';
    
    $profileexp = ProfileExperiences::model()->findAllByAttributes(array('profile_id' => $profilemodel->id));
    if (count($profileexp) > 0)
        $percentage += '15';
        
        $profilehob = ProfileHobbies::model()->findAllByAttributes(array('profile_id' => $profilemodel->id));
        if (count($profilehob) > 0)
            $percentage += '15';
            
            $profilelang = ProfileLanguages::model()->findAllByAttributes(array('profile_id' => $profilemodel->id));
            if (count($profilelang) > 0)
                $percentage += '15';
                
                $profilequal = ProfileQualifications::model()->findAllByAttributes(array('profile_id' => $profilemodel->id));
                if (count($profilequal) > 0)
                    $percentage += '15';
                    
                    $profileskill = ProfileSkills::model()->findAllByAttributes(array('profile_id' => $profilemodel->id));
                    if (count($profileskill) > 0)
                        $percentage += '15';
                        
                        $profilevid = ProfileVideos::model()->findAllByAttributes(array('profile_id' => $profilemodel->id));
                        if (count($profilevid) > 0)
                            $percentage += '10';
                        
                            $this->renderPartial('profileCompletion', array('profilemodel' => $profilemodel,'percentage'=>$percentage)); ?>                                        
                        </div>
                        <div class="form-profile-business-card form-module">
                            <h5>My Business Card</h5>
                            <div class="text-center">
<?php $this->renderPartial('businesscard', array('model' => $model, 'print' => 0)); ?>
                                <a class="bcard-btn main-btn" target="_blank" href="<?php echo $this->createUrl('users/viewBCard', array('id' => $model->id)) ?>">View</a>
                                <a class="bcard-btn main-btn" target="_blank" href="<?php echo $this->createUrl('users/downloadBCard', array('id' => $model->id)) ?>">Save as PDF</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-pull-4 col-md-8">
                        <div id="edit-profile">
                            <div id="profile-videos" class="form-profile-videos form-section">
                                <div class="profile-section-header">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <h4>CV Video</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="profile-section-content profile-videos-section">
                                   <?php $this->renderPartial('userVideos', array('model' => $model, 'profilemodel' => $profilemodel)); ?>
                                </div>
                            </div>                    
                            <div id="profile-documents" class="form-profile-documents form-section">
<?php $this->renderPartial('userDocuments', array('model' => $model)); ?>
                            </div>                 
                            <!-- Education -->
                            <div id="profile-skills" class="form-skills form-section">
<?php $this->renderPartial('userCareer', array('model' => $model, 'profilemodel' => $profilemodel)); ?>
                            </div>
                            <!-- Professional Experience -->
                            <div id="professional-experience" class="form-professional-experienceerience form-section">
<?php $this->renderPartial('userExp', array('model' => $model)); ?>
                            </div>
                            <!-- Education -->
                            <div id="profile-education" class="form-education form-section">
<?php $this->renderPartial('userEducation', array('model' => $model)); ?>
                            </div>
                            <!-- Hobbies -->
                            <div id="profile-hobbies" class="form-hobbies form-section">
<?php $this->renderPartial('userHobbies', array('model' => $model)); ?>
                            </div>
                            <!-- Hobbies -->
                            <div id="profile-languages" class="form-languages form-section">
<?php $this->renderPartial('userLanguage', array('model' => $model)); ?>
                            </div>
                        </div>

                        <div class="text-center">
                            <a class="main-btn download-cv" href="<?php echo $this->createUrl('users/resume', array('id' => $model->id)) ?>" target="_blank">Download my CVVid Profile</a>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container -->
    </div>
</div>



<!--cover photo popup-->

<div class="modal fade" id="mediamanager_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="MediaManager__loading hidden">
                <img src="<?php echo $this->createAbsoluteUrl('//'); ?>/images/loading.gif">
            </div>
            <div class="MediaManager__header">
                <form action="" method="POST" enctype="multipart/form-data" class="add-new-form">
                    <div class="row">
                        <div class="col-xs-7">
                            <h4>Media Library</h4>
                        </div>
                        <div class="col-xs-5">
                            <div class="upload-media pull-right">
                                <label class="btn main-btn medaiamanager_add"><i class="fa fa-plus"></i>Upload a File</label>
                                <input class="media-file" type="file" style="display:none;"/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="MediaManager__content">
                <?php
                $mediamodel = Media::model()->findAll(array(
                    'condition' => 'model_id=:model_id AND model_type=:model_type AND collection_name=:collection_name',
                    'params' => array(':model_id' => $model->id, ':model_type' => 'App\\Users\\Candidate', ':collection_name' => 'photos'),
                ));
                if (count($mediamodel) > 0) {
                    ?>
                    <div class="row">
    <?php foreach ($mediamodel as $media) { ?>
                            <div class="col-xs-6 col-sm-3">
                                <div class="MediaManager__item MediaManager__item--image" id="<?php echo $media['id'] ?>">
                                    <img src="<?php echo Yii::app()->baseUrl ?>/images/media/<?php echo $media['id'] ?>/conversions/search.jpg" class="img-responsive">
                                    <a class="MediaManager__item__delete" href="#" id="<?php echo $media['id'] ?>"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                    <?php } ?>
                    </div>
                <?php } ?>
                <h3>CV Vid Image Library</h3>
                <?php
                $mediamodel1 = Media::model()->findAll(array(
                    'condition' => 'model_id=:model_id AND collection_name=:collection_name',
                    'params' => array(':model_id' => 1, ':collection_name' => 'sample'),
                ));
                if (count($mediamodel1) > 0) {
                    ?>
                    <div class="row">
    <?php foreach ($mediamodel1 as $media) { ?>
                            <div class="col-xs-6 col-sm-3">
                                <div class="MediaManager__item MediaManager__item--image" id="<?php echo $media['id'] ?>">
                                    <img src="<?php echo Yii::app()->baseUrl ?>/images/media/<?php echo $media['id'] ?>/conversions/search.jpg" class="img-responsive">
                                </div>
                            </div>
                    <?php } ?>
                    </div>
<?php } ?>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn default-btn" data-dismiss="modal" onclick="closepopup()">Close</button>
                <button type="button" class="btn main-btn select-submit saveMedia">Save</button>
            </div>

        </div>
    </div>
</div>