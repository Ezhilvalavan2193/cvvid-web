<?php   $statmodel = UserStatement::model()->findAllByAttributes(array('user_id'=>$model->id)); ?>
<div class="profile-section-header">
    <div class="row">
        <div class="col-sm-8">
            <h4>Personal Statement</h4>
        </div>
        <?php if(count($statmodel) <= 0) { ?>
        <div class="col-sm-4">
            <div class="pull-right">
                <button class="add-new main-btn add-new-statement">Add</button>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
<div class="profile-section-content">
    <div class="profile-section-info" <?php if(count($statmodel) > 0) { ?>style="display: none;" <?php } ?>>
        <p>Share about yourself.</p>
    </div>
    <div class="new-statement-form form-profile" style="display: none;">
        <div>
              <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'new-stat-form',
                        'enableAjaxValidation'=>false,
            'htmlOptions' => array( 'class' => 'form-horizontal','enctype' => 'multipart/form-data'),
                )); ?>
                <div class="form-group">
                    <label class="col-lg-3">Personal Statement</label>
                    <div class="col-lg-9">
                     <input type="hidden" name="user_id" value="<?php echo $model->id ?>">
                        <textarea name="description" class="form-control" id="description" maxlength="500" cols="30" rows="10" required></textarea>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="button" class="cancel-btn new-hobby-cancel">Cancel</button>

                </div>
             <?php $this->endWidget(); ?>
        </div>
    </div>
    <div class="statement-list">
          <?php 
          
          foreach ($statmodel as $stat)
            {
          ?>
        <div class="statement-item profile-item">
            <div class="statement-item-inner-<?php echo $stat['id']?>" style="display: block;">
               
                <div class="profile-item-description statement-description">
                    <?php echo nl2br($stat['description']); ?> <a class="edit-item edit-statement" href="#" id="<?php echo $stat['id'] ?>"><i class="fa fa-pencil"></i>Edit</a>
                </div>
            </div>
            <div class="edit-statement-form-<?php echo $stat['id'] ?>" style="display: none;">
                <div>
                     <?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'edit-stat-form',
					'enableAjaxValidation'=>false,
	      		   'htmlOptions' => array('class'=>'form-horizontal','enctype' => 'multipart/form-data'),
				)); ?>
                        <div class="form-group">
                            <label class="col-lg-3">Personal Statement</label>
                           
                            <div class="col-md-9"> <?php echo CHtml::hiddenField('id',$stat['id'],array()); ?>
                                <textarea name="description" maxlength="500" class="form-control" id="description" cols="30" rows="10"><?php echo str_replace("<br />", null, $stat['description']); ?></textarea>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="cancel-btn statement_edit_cancel" id="<?php echo $stat['id'] ?>">Cancel</button>

                            <button type="button" class="delete-btn statement-delete" id="<?php echo $stat['id'] ?>">Delete</button>

                        </div>
                   <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
          <?php } ?>
    </div>
</div>

