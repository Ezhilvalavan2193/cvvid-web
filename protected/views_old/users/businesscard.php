<?php
/* @var $this UsersController */
/* @var $model Users */
?> 
<div class="profile-card">
    <div class="row">
        <div class="col-sm-12">
            <div class="business-card-preview">
            <?php $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $model->id, 'type' => $model->type)); ?>
             <img src="<?php echo $this->createAbsoluteUrl('//'); ?>/themes/Dev/images/logo.png" alt="CV Vid">
             <div class="business-card-name"><?php echo $model->forenames." ".$model->surname; ?></div>
            <div class="business-card-phone"><?php echo $model->tel; ?></div>
            <div class="business-card-email"><?php echo $model->email; ?></div>
            <div class="business-card-url"><?php echo "https://".$_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']).$profilemodel->slug; ?></div>
            </div>
        </div>
    </div>    
    
</div>

