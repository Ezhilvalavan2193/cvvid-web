<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs = array(
    'Users' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
");
?> 
<script>
//    $(document).ready(function ()
//    {
//        $("#manage-admins-form").submit(function (event) {
//            //event.preventDefault();
//            if ($('#password').val() != "" && $('#password').val() == $('#confirmpassword').val())
//                $("#manage-admins-form").submit();
//            else
//            {
//                $('.pwdmsg').html('Password not matched');
//                return false;
//            }
//        });
//
//    });
</script>
<h1><?php echo ($model->isNewRecord ? "Create" : "Update") ?> Admin User</h1>
<div class="Admin__content__inner">
    
    <?php
    if ($model->isNewRecord)
        $url = $this->createUrl('users/saveAdmin');
    else
        $url = $this->createUrl('users/updateAdmin', array('id' => $model->id));

    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'manage-admins-form',
        'action' => $url,
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data')
    ));
    ?>
    
    <?php echo  $form->errorSummary(array($model,$addressmodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>

    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Account Details</h3>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'forenames'); ?>
                        <?php echo $form->textField($model, 'forenames', array('class' => 'form-control', 'placeholder' => "Forenames")); ?>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'surname'); ?>
                        <?php echo $form->textField($model, 'surname', array('class' => 'form-control', 'placeholder' => "Surname")); ?>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'email'); ?>
                        <?php echo $form->emailField($model, 'email', array('class' => 'form-control', 'placeholder' => "Email")); ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <?php echo $model->password = ""; ?>
                        <?php echo $form->labelEx($model, 'password'); ?>
                        <?php echo $form->passwordField($model, 'password', array('class' => 'form-control', 'placeholder' => "Password", 'id' => 'password')); ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <?php echo CHtml::label('Confirm Password', '', array()); ?>
                        <input class="form-control" placeholder="Confirm Password" type="password" value="" name="Users[confirmpassword]" id="confirmpassword">
                    </div>
                </div>
            </div><!-- .row -->

            <div class="row">

            </div>

        </div><!-- .panel-body -->
    </div><!-- .panel -->
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Details</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label for="dob" class="control-label">Date of Birth</label>
                        <div class="input-group"><input type="text" name="dob" class="dob form-control" value="<?php echo ((empty($model->dob) || $model->dob == null || $model->dob == "0000-00-00") ? "" : date("d/m/Y", strtotime($model->dob))); ?>"></div>
                 
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'tel'); ?>
                        <?php echo $form->textField($model, 'tel', array('class' => 'form-control','placeholder'=>"Telephone")); ?>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'mobile'); ?>
                        <?php echo $form->textField($model, 'mobile', array('class' => 'form-control','placeholder'=>"Mobile")); ?>
                    </div>
                </div><!-- .col-sm-6 -->
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'current_job'); ?>
                        <?php echo $form->textField($model, 'current_job', array('class' => 'form-control','placeholder'=>"Current Job")); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Address</h3>
        </div>
        <div class="panel-body">
             <div id="location-fields">
                <?php $this->renderPartial('//addresses/address-form', array('form'=>$form,'addressmodel'=>$addressmodel)); ?>
             </div>
        </div>
        <div class="form-actions clearfix">
            <?php echo CHtml::submitButton('Save', array("class" => "btn btn-primary pull-right")); ?>

        </div> 

        <?php $this->endWidget(); ?>
        <form method="POST" action="#" accept-charset="UTF-8">
           <?php
            if (!$model->isNewRecord)
                echo CHtml::submitButton('Delete', array("class" => "btn btn-danger", "style" => ""));
            ?>
        </form>

           
    </div>
    </div>


