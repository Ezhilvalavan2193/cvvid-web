<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Candidates'=>array('index'),
	'Manage',
);

// $this->menu=array(
// // 	array('label'=>'List Users', 'url'=>array('index')),
// 	array('label'=>'Create Candidates', 'url'=>array('create')),
// );

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#users-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});

");
?>
<script>
$(document).ready(function(){
	
	$('#export-candidate').on('click',function() {
	    window.location = '<?php echo $this->createUrl('users/exportCSV',array('trial'=>$trial,'paid'=>$paid,'basic'=>$basic)); ?>'
	});

	$('#activate-candidate').click(function(e){
		
		 if($("#users-grid").find("input:checked").length > 0)
         {

        	 if (confirm('Are you sure you want to activate these candidates ?')) {

         		 var ids = $.fn.yiiGridView.getChecked("users-grid", "selectedIds");
             	
       			  $.ajax({
       		            url: '<?php echo $this->createUrl('users/activateUsers') ?>',
       		            type: 'POST',
       		            data: {ids : ids}, 	    
       		            success: function(data) {
       		            	$.fn.yiiGridView.update('users-grid');
       			           // alert(data);
       		            },
       				    error: function(data) {		
       				        alert('err');
       				    }
       		         });
        	 } 
        	
         }
         else
         {
            alert('Please select at least one item');
         }
	});

	$('#block-candidate').click(function(e){
		
		 if($("#users-grid").find("input:checked").length > 0)
         {

        	 if (confirm('Are you sure you want to block these candidates ?')) {

         		 var ids = $.fn.yiiGridView.getChecked("users-grid", "selectedIds");
             	
       			  $.ajax({
       		            url: '<?php echo $this->createUrl('users/blockUsers') ?>',
       		            type: 'POST',
       		            data: {ids : ids}, 	    
       		            success: function(data) {
       			           // alert(data);
       		            	$.fn.yiiGridView.update('users-grid');
       		            },
       				    error: function(data) {		
       				        alert('err');
       				    }
       		         });
        	 } 
        	
         }
         else
         {
            alert('Please select at least one item');
         }
	});
	
	$('#delete-candidate').click(function(e){

		
         if($("#users-grid").find("input:checked").length > 0)
         {

        	 if (confirm('Are you sure you want to delete these items?')) {

         		 var ids = $.fn.yiiGridView.getChecked("users-grid", "selectedIds");
             	
       			  $.ajax({
       		            url: '<?php echo $this->createUrl('users/deleteUsers') ?>',
       		            type: 'POST',
       		            data: {ids : ids}, 	    
       		            success: function(data) {
       		            	$.fn.yiiGridView.update('users-grid');
       			            //alert(data);
       		            },
       				    error: function(data) {		
       				        alert('err');
       				    }
       		         });
        	 } 
        	
         }
         else
         {
            alert('Please select at least one item');
         }
	});
	
});

</script>
<h1><?php echo $trial == 1 ? "On Trial Candidates" : ($paid == 1 ? "Candidates": "Basic Memberships") ?></h1>
<?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
<div class="Admin__content__inner">
    <div class="AdminFilters">
        <div class="form-inline">
          <?php $this->renderPartial('_search',array(
                'model'=>$model,'trial'=>$trial,'paid'=>$paid,'basic'=>$basic,'fullname'=>$fullname
            )); ?>
            <div class="pull-right">
                  <?php  
                    echo CHtml::button('Bulk Create',array("class"=>"btn btn-primary","style"=>"","onclick"=>"window.location='".Yii::app()->createUrl('users/bulkCreate',array('trial'=>$trial,'paid'=>$paid,'basic'=>$basic))."'"));
                    echo CHtml::button('Create',array("class"=>"btn btn-primary","style"=>"","onclick"=>"window.location='".Yii::app()->createUrl('users/createCandidate',array('trial'=>$trial,'paid'=>$paid,'basic'=>$basic))."'"));
                    echo CHtml::button('Activate',array("class"=>"btn btn-primary",'id'=>'activate-candidate'));
                    echo CHtml::button('Block',array("class"=>"btn btn-primary",'id'=>'block-candidate'));
                    echo CHtml::button('Delete',array("class"=>"btn btn-primary","style"=>"",'id'=>'delete-candidate'));
                    echo CHtml::button('Export',array("class"=>"btn btn-primary","style"=>"",'id'=>'export-candidate'));
                  ?>
            </div>
        </div>
    </div>
     
    <?php 
  
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-grid',
     'itemsCssClass' => 'table',
	'summaryText'=>'',
	//'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',    
    'enableSorting'=>true,
    'ajaxUpdate'=>true,
    
    'pager' => array(
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'header'=>'',
        'firstPageLabel' => '<<',
        'lastPageLabel' => '>>',
        'nextPageLabel' => '>>',
        'prevPageLabel' => '<<'),
			'ajaxUpdate'=>false,
    'dataProvider'=>($namesearch > 0 ? $model->searchByName($namearray) : $model->search($trial,$paid,$basic)),
	//'filter'=>$model,
	'columns'=>array(
			//'id',
    	    array(
    	       // 'name' => 'check',
    	        'id' => 'selectedIds',
    	        'value' => '$data->id',
    	        'class' => 'CCheckBoxColumn',
    	        'selectableRows'  => 2,
    	       // 'checked'=>'Yii::app()->user->getState($data->id)',
    	       // 'checkBoxHtmlOptions' => array('name' => 'idList[]'),
    	        
    	    ),
			array(
					'header'=>'Name',
					'value'=>'CHtml::link($data->forenames." ".$data->surname,Yii::app()->createUrl("users/tabedit", array("id"=>$data->id)),array("class"=>"profile-edit-button","id"=>"profile-edit"))',
			        'type'=>'raw'
			),
			array(
					'header'=>'Profile',
					'value'=>'CHtml::link("View",$data->getSlugURL(),array("class"=>"profile-view-button","id"=>"profile-view"))',
					'type'=>'raw'
			),
			array(
					'header'=>'Status',
					'value'=>'$data->status',
			),
			array(
					'header'=>'Date Joined',
					'value'=>'$data->created_at',
			),
			array(
					'header'=>'Last Active',
					'value'=>'$data->loggeddatetime',
                    'visible'=>$basic,
			),
			array(
					'header'=>'Trail ends at',
					'value'=>'$data->trial_ends_at',
			         'visible'=>$trial,			    
			),
            array(
					'header'=>'Subscription ends at',
					'value'=>'$data->subscription_ends_at',
			         'visible'=>$paid,			    
			),
    	    array
	        (
    	        'header'=>'Plan',
    	        'value'=>'$data->stripe_plan',
                'visible'=> $paid,
    	    )
// 		array(
// 			'class'=>'CButtonColumn',
// 			'template'=>'{del}',
			
// 			'buttons'=>array
// 			(
// 					'del' => array
// 					(
// 						'label'=>'<i class="fa fa-trash" aria-hidden="true"></i>',
// 						'url'=>'Yii::app()->createUrl("users/delete", array("id"=>$data->id))',
// 					),
// 			),
// 		),
	),
)); ?>
</div>

