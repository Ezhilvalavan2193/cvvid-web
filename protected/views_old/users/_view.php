<?php
/* @var $this UsersController */
/* @var $data Users */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
	<?php echo CHtml::encode($data->type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('forenames')); ?>:</b>
	<?php echo CHtml::encode($data->forenames); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('surname')); ?>:</b>
	<?php echo CHtml::encode($data->surname); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('password')); ?>:</b>
	<?php echo CHtml::encode($data->password); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dob')); ?>:</b>
	<?php echo CHtml::encode($data->dob); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_premium')); ?>:</b>
	<?php echo CHtml::encode($data->is_premium); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tel')); ?>:</b>
	<?php echo CHtml::encode($data->tel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mobile')); ?>:</b>
	<?php echo CHtml::encode($data->mobile); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('current_job')); ?>:</b>
	<?php echo CHtml::encode($data->current_job); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('location')); ?>:</b>
	<?php echo CHtml::encode($data->location); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('location_lat')); ?>:</b>
	<?php echo CHtml::encode($data->location_lat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('location_lng')); ?>:</b>
	<?php echo CHtml::encode($data->location_lng); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('remember_token')); ?>:</b>
	<?php echo CHtml::encode($data->remember_token); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_at')); ?>:</b>
	<?php echo CHtml::encode($data->created_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_at')); ?>:</b>
	<?php echo CHtml::encode($data->updated_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deleted_at')); ?>:</b>
	<?php echo CHtml::encode($data->deleted_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stripe_active')); ?>:</b>
	<?php echo CHtml::encode($data->stripe_active); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stripe_id')); ?>:</b>
	<?php echo CHtml::encode($data->stripe_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stripe_subscription')); ?>:</b>
	<?php echo CHtml::encode($data->stripe_subscription); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stripe_plan')); ?>:</b>
	<?php echo CHtml::encode($data->stripe_plan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_four')); ?>:</b>
	<?php echo CHtml::encode($data->last_four); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('card_expiry')); ?>:</b>
	<?php echo CHtml::encode($data->card_expiry); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('card_expiry_sent')); ?>:</b>
	<?php echo CHtml::encode($data->card_expiry_sent); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('trial_ends_at')); ?>:</b>
	<?php echo CHtml::encode($data->trial_ends_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subscription_ends_at')); ?>:</b>
	<?php echo CHtml::encode($data->subscription_ends_at); ?>
	<br />

	*/ ?>

</div>