<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Users', 'url'=>array('index')),
	array('label'=>'Create Users', 'url'=>array('create')),
	array('label'=>'Update Users', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Users', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Users', 'url'=>array('admin')),
);
?>

<h1>View Users #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'type',
		'forenames',
		'surname',
		'email',
		'password',
		'dob',
		'status',
		'is_premium',
		'tel',
		'mobile',
		'current_job',
		'location',
		'location_lat',
		'location_lng',
		'remember_token',
		'created_at',
		'updated_at',
		'deleted_at',
		'stripe_active',
		'stripe_id',
		'stripe_subscription',
		'stripe_plan',
		'last_four',
		'card_expiry',
		'card_expiry_sent',
		'trial_ends_at',
		'subscription_ends_at',
	),
)); ?>
