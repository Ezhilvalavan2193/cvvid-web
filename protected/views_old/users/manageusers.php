<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs = array(
    'Candidates' => array('index'),
    'Manage',
);

// $this->menu=array(
// // 	array('label'=>'List Users', 'url'=>array('index')),
// 	array('label'=>'Create Candidates', 'url'=>array('create')),
// );

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#manage-users-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});

");
?>
<script>
    $(document).ready(function () {

        $('#delete-admin').click(function (e) {


            if ($("#manage-users-grid").find("input:checked").length > 0)
            {

                if (confirm('Are you sure you want to delete these items?')) {

                    var ids = $.fn.yiiGridView.getChecked("manage-users-grid", "selectedIds");

                    $.ajax({
                        url: '<?php echo $this->createUrl('users/deleteAdmin') ?>',
                        type: 'POST',
                        data: {ids: ids},
                        success: function (data) {
                            $.fn.yiiGridView.update('manage-users-grid');
                            //alert(data);
                        },
//                        error: function (data) {
//                            alert('err');
//                        }
                    });
                }

            } else
            {
                alert('Please select at least one item');
            }
        });

    });

</script>
<h1>Administrators</h1>
<div class="Admin__content__inner">
<?php if (Yii::app()->user->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>
        <div class="AdminFilters">
            <div class="form-inline">
                <?php
                $this->renderPartial('_searchadmins', array(
                    'model' => $model, 'fullname' => $fullname
                ));
                ?>


                <div class="pull-right">
                    <?php
                    echo CHtml::button('Create', array("class" => "btn btn-primary", "style" => "", "onclick" => "window.location='" . Yii::app()->createUrl('users/createAdmin', array()) . "'"));
                    echo CHtml::button('Block', array("class" => "btn btn-primary", 'id' => 'block-admin'));
                    echo CHtml::button('Delete', array("class" => "btn btn-primary", 'id' => 'delete-admin'));
                    ?>
                </div>
            </div>
        </div>

        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'manage-users-grid',
            'itemsCssClass' => 'table',
            'summaryText' => '',
            // 'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
            'enableSorting' => true,
            'ajaxUpdate' => true,
    'pager' => array(
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'header'=>'',
        'firstPageLabel' => '<<',
        'lastPageLabel' => '>>',
        'nextPageLabel' => '>>',
        'prevPageLabel' => '<<'),
            'ajaxUpdate' => false,
            'dataProvider' => ($namesearch > 0 ? $model->searchByName($namearray) : $model->searchAdmins()),
            'columns' => array(
                //'id',
                array(
                    // 'name' => 'check',
                    'id' => 'selectedIds',
                    'value' => '$data->id',
                    'class' => 'CCheckBoxColumn',
                    'selectableRows' => 2,
                // 'checked'=>'Yii::app()->user->getState($data->id)',
                // 'checkBoxHtmlOptions' => array('name' => 'idList[]'),
                ),
                array(
                    'header' => 'Name',
                    'value' => 'CHtml::link("<i class=\"fa fa-pencil\"></i> ".$data->forenames." ".$data->surname,Yii::app()->createUrl("users/editManageUser", array("id"=>$data->id)),array("class"=>"profile-edit-button","id"=>"profile-edit"))',
                    'type' => 'raw'
                )
            ),
        ));
        ?>

</div>
