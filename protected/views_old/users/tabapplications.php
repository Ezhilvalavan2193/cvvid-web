<?php 
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
");
?> 
<script>
$(document).ready(function(){
	
	$('#slug_field').on('blur', function() {

		 var ele = $(this);
	 	 var value = $(this).val();
	 	 if(value != "")
	 	 {
        	 $.ajax({
                   url: '<?php echo $this->createUrl('users/checkSlug')?>',
                   type: 'POST',
        	       data: {slug : value},
                   success: function(data) {
        				if(data == -1)
                            $(ele).closest('.slug-field').removeClass('has-success').addClass('has-error');
        				else
        					$(ele).closest('.slug-field').removeClass('has-error').addClass('has-success');
                   },
        		    error: function(data) {		
        		      //  alert(data);
        		    }
               });			
	 	 }
	 	 else
	 	 {
	 		$(ele).closest('.slug-field').removeClass('has-success').addClass('has-error');
	 	 }
     	 return false;
    });
});
</script>

<h1><?php echo $model->forenames." ".$model->surname; ?>: Job Applications</h1>
<div class="Admin__content__inner">
    <div class="UserInformation">
        <ul class="AdminTabs nav nav-tabs">
            <li role="presentation">
                <a href="<?php echo Yii::app()->createUrl("users/tabedit", array("id"=>$model->id)); ?>">Details</a>
            </li>
            <li role="presentation">
                <a href="<?php echo Yii::app()->createUrl("users/tabprofile", array("id"=>$model->id)); ?>">Profile</a>
            </li>
            <li role="presentation"  >
                <a href="<?php echo Yii::app()->createUrl("users/tabdocuments", array("id"=>$model->id)); ?>">Documents</a>
            </li>
            <li role="presentation" class=active>
                <a href="<?php echo Yii::app()->createUrl("users/tabapplications", array("id"=>$model->id)); ?>">Applications</a>
            </li>
            <li role="upgrade" >
                <a href="<?php echo Yii::app()->createUrl("users/tabupgrade", array("id"=>$model->id)); ?>">Upgrade</a>
            </li>
        </ul>
        <?php 
    $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'profile-job-grid',
         'itemsCssClass' => 'table',
            'summaryText' => '',
           // 'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
            'dataProvider' => JobApplications::model()->searchByID($model->id),
            'columns' => array(
                array(
                    'header' => 'Company Name',
                    'value' => '$data->getCompany($data->job->owner_id)',
                ),
                array(
                    'header' => 'Job Title',
                    'value' => '$data->job->title',
                ),
                array(
                    'header' => 'Salary',
                    'value' => '$data->job->salary_min." to ".$data->job->salary_min." per ".$data->job->salary_type',
                ),
                array(
                    'header' => 'Location',
                    'value' => '$data->job->location',
                ),
                array(
                    'header' => 'Applied on',
                    'value' => '$data->job->updated_at',
                )
            ),
        ));
        ?>
    </div>
</div>
