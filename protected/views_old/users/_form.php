<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
    'Candidates'=>array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
");
?>
<script>
$(document).ready(function(){

//	$('.photo-section').on('click','#photo-btn',function(e){
//		 e.preventDefault();
//		 $("#profile-photo").trigger('click');
//	});
//
//	$('.photo-section').on('change','#profile-photo',function(evt){
//
//          var files = evt.target.files;
//          for(var i =0,f; f= files[i];i++){
//              
//	        var reader = new FileReader();
//	        reader.onload = (function (thefile){
//               return function(e){
//               	 $('#profile-pic').attr('src',e.target.result);
//               };
//	        })(f);
//	        reader.readAsDataURL(f);
//          }
//
//          $('#remove-photo-btn').show();
//  });
//
//	$('.photo-section').on('click','#remove-photo-btn',function(evt){
//		$('#profile-pic').attr('src','');			
//		$(this).hide();	
//	});
//
//});

</script>

<h1>Create Candidate User</h1>
<div class="Admin__content__inner">
      <?php 
       
        $form = $this->beginWidget('CActiveForm', array(
                    	'id'=>'create-candidate-form',
                        'action'=>$this->createUrl('users/create'),
                    	'enableAjaxValidation'=>false,
                        'htmlOptions' => array('enctype' => 'multipart/form-data')
                ));         
        
    ?>
    <div class="row">
        <div class="col-sm-3 col-md-2">
            <div class="form-group">
                <label for="photo_id" class="control-label">Profile Photo</label>
                <div class="photo-section">
                    <img src="" alt="" id="profile-pic" class="img-responsive">
                    <!--<input id="profile-photo" name="photo" type="file" style="display:none;"/>-->
                    <input type="hidden" name="photo_id" id="photo_id" value="">
                    <button type="button" class="add-media btn btn-primary btn-xs" id="photo-btn">Add Logo</button>            
                    <button type="button" class="remove-media btn btn-primary btn-xs" id="remove-photo-btn">Remove Logo</button>
                </div>
            </div>
        </div>
        <div class="col-sm-9 col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Account Details</h3>
                </div>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="forenames" class="control-label">Forenames</label>
                                <?php echo $form->textField($model,'forenames',array('class'=>'form-control','placeholder'=>"Forenames")); ?>
                        	<?php echo $form->error($model,'forenames'); ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="surname" class="control-label">Surname</label>
                                <?php echo $form->textField($model,'surname',array('class'=>'form-control','placeholder'=>'')); ?>
                        	<?php echo $form->error($model,'surname'); ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="email" class="control-label">Email</label>
                                <?php echo $form->textField($model,'email',array('class'=>'form-control','placeholder'=>'Email')); ?>
                        	<?php echo $form->error($model,'email'); ?>
                            </div>
                        </div>
                    </div><!-- .row -->

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="password" class="control-label">Password</label>
                                <?php echo $model->password = "";?>
                                <?php echo $form->passwordField($model,'password',array('class'=>'form-control','placeholder'=>'Password')); ?>
                                <?php echo $form->error($model,'password'); ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="password_confirmation" class="control-label">Confirm Password</label>
                                <?php echo CHtml::passwordField('confirmpassword','',array('class'=>'form-control','placeholder'=>'Confirm Password')); ?>
                                <input class="form-control" id="password" placeholder="Confirm Password" name="password_confirmation" type="password" value="">
                            </div>
                        </div>
                    </div><!-- .row -->

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="profile[slug]" class="control-label">Link</label>
                                <div class="input-group slug-field <?php echo isset($slugerr) && $slugerr != "" ? "has-error" : "" ?>">
                                    <span class="input-group-addon"><?php echo "http://" . $_SERVER['SERVER_NAME'] . "/" ?></span>
                                    <?php echo $form->textField($profilemodel, 'slug', array('class' => 'form-control', 'id' => 'slug', 'placeholder' => 'Slug', 'required' => 'required')); ?>
                                    <span class="input-group-addon help-block"></span>
                                    <?php echo $form->error($profilemodel, 'slug'); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                </div><!-- .panel-body -->
            </div>
            
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Details</h3>
                </div>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="dob" class="control-label">Date of Birth</label>
                                <div class="input-group">
                                    <input type="text" name="dob" class="dob form-control" value="<?php echo ((empty($model->dob) || $model->dob == null || $model->dob == "0000-00-00") ? "" : date("d/m/Y", strtotime($model->dob))); ?>">
                 
                                </div>
                            </div>
                        </div><!-- .col-sm-6 -->
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="tel" class="control-label">Telephone</label>
                                <?php echo $form->textField($model,'tel',array('class'=>'form-control','placeholder'=>'Telephone')); ?>
                    		<?php echo $form->error($model,'tel'); ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="mobile" class="control-label">Mobile</label>
                                <?php echo $form->textField($model,'mobile',array('class'=>'form-control','placeholder'=>'Mobile')); ?>
                    		<?php echo $form->error($model,'mobile'); ?>
                            </div>
                        </div><!-- .col-sm-6 -->
                    </div><!-- .row -->

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="current_job" class="control-label">Current Job</label>
                                <?php echo $form->textField($model,'current_job',array('class'=>'form-control','placeholder'=>'Current Job')); ?>
                    		<?php echo $form->error($model,'current_job'); ?>
                            </div>
                        </div>
                    </div>
                </div><!-- .panel-body -->
            </div>
            
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Address</h3>
                </div>
                <div class="panel-body">
                    <div id="location-fields">
                       <?php $this->renderPartial('//addresses/address-form', array('form'=>$form,'addressmodel'=>$addressmodel)); ?>
                    </div>

                </div>
            </div>
            
            <div class="form-actions clearfix">
                <?php echo CHtml::submitButton('Save',array("class"=>"btn btn-primary pull-right")); ?>                	
            </div>
            
        </div>
    </div>
    
      <?php $this->endWidget(); ?>
</div>



