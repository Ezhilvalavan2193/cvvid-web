<?php
/* @var $this InstitutionOfficesController */
/* @var $model InstitutionOffices */

$this->breadcrumbs=array(
	'Employer Offices'=>array('index'),
	'Create',
);

$this->menu=array(
	
);
?>

<h1>Create EmployerOffices</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>