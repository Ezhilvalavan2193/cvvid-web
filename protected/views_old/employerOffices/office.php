<?php
/* @var $this InstitutionOfficesController */
/* @var $model InstitutionOffices */
/* @var $form CActiveForm */
?>

<div class="Admin__content__inner">
    <div class="OfficeForm">
<?php
if($model->isNewRecord)
    $url = $this->createUrl('employerOffices/create',array('id'=>$insmodel->id));
else 
    $url = $this->createUrl('employerOffices/edit',array('id'=>$model->id));

   $form=$this->beginWidget('CActiveForm', array(
	   'id'=>'empp-offices-form',
       'action'=>$url,
    //'enableAjaxValidation'=>false
)); ?>
<?php echo  $form->errorSummary(array($model,$addressmodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
 <div class="form-container">	
 <h3> <?php echo $model->isNewRecord ? "Add" : "Update"?> Office Location </h3>
 <hr>
         <div class="row">
            <div id="location-fields">
                   <?php $this->renderPartial('//addresses/address-form', array('form'=>$form,'addressmodel'=>$addressmodel)); ?>
			</div>	
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label">Head Office</label>
                        <?php 
                            $addressmodel->head_office = 0;
                            echo $form->checkBox($addressmodel, 'head_office', array()); 
                        ?>
                    </div>
                </div>
            </div>
         </div>
     <div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-primary')); ?>
        <?php echo CHtml::button('Cancel', array("class" => "btn btn-primary", "onclick" => "window.location='" . Yii::app()->createUrl('employerOffices/admin',array('id'=>$insmodel->id)) . "'")); ?>
		
	</div>
	
</div>
<?php $this->endWidget(); ?>
</div>
</div><!-- form -->