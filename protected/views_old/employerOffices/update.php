<?php
/* @var $this InstitutionOfficesController */
/* @var $model InstitutionOffices */

$this->breadcrumbs=array(
	'Employer Offices'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	
);
?>

<h1>Update EmployerOffices <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>