<?php
/* @var $this IndustriesController */
/* @var $model Industries */
/* @var $form CActiveForm */
?>
<h1>Edit Career Path</h1>
<div class="Admin__content__inner">
       <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'skills-form',
        'action' => $this->createUrl('skills/updateSkills',array('id'=>$model->id)),
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data')
    ));
    ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <?php echo $form->labelEx($model, 'name'); ?>
                <?php echo $form->textField($model, 'name', array('class' => 'form-control', 'placeholder' => 'Name')); ?>
                <?php echo $form->error($model, 'name'); ?>
            </div>
            <div class="form-group">
                <?php 
                    // $empmodel = new Employers;
                     $skmodel = SkillCategories::model()->findAll();
                     $skclist = CHtml::listData($skmodel, 'id', 'name');
                     echo $form->dropDownList($skillcmodel,'id',$skclist,array('class'=>'form-control'));
                ?>
            </div>
            <?php echo CHtml::submitButton('Save', array("class" => "btn btn-primary")); ?>
            <?php echo CHtml::button('Cancel',array("class"=>'btn btn-primary',"onClick"=>"window.location='".Yii::app()->createUrl('skills/admin',array())."'")); ?>

        </div>
    </div>
    
    
    <?php $this->endWidget(); ?>
</div>

