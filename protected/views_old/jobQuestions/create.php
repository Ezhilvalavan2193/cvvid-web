<?php
/* @var $this JobQuestionsController */
/* @var $model JobQuestions */

$this->breadcrumbs=array(
	'Job Questions'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List JobQuestions', 'url'=>array('index')),
	array('label'=>'Manage JobQuestions', 'url'=>array('admin')),
);
?>

<h1>Create JobQuestions</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>