<div class="formBlk col-md-12">
    <div class="formRow">
        <label class="control-label">Interview Date and Time*</label>
        <?php
        $this->widget('zii.widgets.jui.CJuiDatePicker', array(
            'name' => 'interview_date',
            'value' => '',
            'id' => 'interview_date'.uniqid(),
            // additional javascript options for the date picker plugin
            'options' => array(
                'showAnim' => 'fold',
                'showButtonPanel' => true,
                'showOn' => 'button',
                'buttonImageOnly' => true,
                'dateFormat' => 'yyyy/mm/dd',
            ),
            'htmlOptions' => array(
                'class' => 'datepicker textBox',
            ),
        ));
        ?>
    </div>
</div>
<div class="formBlk col-md-12">
    <div class="formRow border-none">
        <label class="control-label">Message*</label>

        <input type="hidden" id="job-invite-application-id" value="<?php echo $application_id; ?>">
        <textarea class="form-control" id="message-invite" name="message-reject" cols="50" rows="5" spellcheck="false" style="width: 100%; overflow: hidden; padding: 20px; z-index: auto; position: relative; line-height: 20px; font-size: 14px; transition: none; background: transparent !important;" ></textarea>
    </div>
</div>
