<html>
    <head>
    
<link rel="Stylesheet" type="text/css" href="http://code.jquery.com/ui/1.9.0/themes/base/jquery-ui.css" />
<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>
<script type="text/javascript" src="http://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>

<script type="text/javascript">
$(function(){

	$('a').hover(function(e){
	    $(this).attr('title', '');
	});

});
</script>
        <style>
            @page {
                margin: 20px;
            }

            body {
                margin: 0;
            }

            .business-card {
                border: 1px solid #E2E2E2;
                color: #888;
                height: 158px;
                padding: 30px 20px;
                text-align: center;
                width: 100%;
            }
            
        </style>
    </head>
    <body>
    <div class="business-card">
        <div class="business-card-details">
            <?php $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $model->id, 'type' => $model->type)); ?>
             <img  src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo.jpg" alt="CV Vid"> 
             <div class="business-card-name"><?php echo $model->forenames." ".$model->surname; ?></div>
            <div class="business-card-phone"><?php echo $model->tel; ?></div>
            <div class="business-card-email"><?php echo $model->email; ?></div>
            <div class="business-card-url"><?php echo "https://cvvidcareers.co.uk/".$profilemodel->slug ?></div>
        </div>
    </div>
    </body>
</html>