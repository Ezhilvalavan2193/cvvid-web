<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="./../images/favicon.ico">

        <title>CVVid</title>

        <!-- Custom styles for this template -->
        <style>
            /*--------------------------------------------------------------
    # Typography
    --------------------------------------------------------------*/
            /*--------------------------------------------------------------
            # Bootstrap
            --------------------------------------------------------------*/
            /*--------------------------------------------------------------
            # Profile
            --------------------------------------------------------------*/
            @font-face {
                font-family: 'Arimo';
                src: url("../fonts/arimo/arimo-regular-webfont.ttf") format("truetype"); }

            .page-break {
                page-break-after: always; }

            body {
                font-family: 'Arimo';
                font-size: 15px; }

            h2 {
                font-family: 'Arimo';
                color: #888;
                font-size: 21px;
                margin-bottom: 0;
                padding: 0 40px; }

            h3 {
                font-family: 'Arimo';
                color: #000;
                font-size: 18px;
                margin-top: 0;
                margin-bottom: 10px; }

            .tagline {
                text-transform: uppercase;
                color: #7D7D7D; }
            .tagline span {
                color: #F9DF6E; }

            .cv-section {
                border: 1px solid #EBEBEB;
                margin: 15px 0px;
                padding-bottom: 15px; }
            .cv-section h2 {
                margin-top: 0;
                background: #000;
                color: #FFF;
                padding: 10px 40px 15px; }

            h2, h3, h4 {
                font-family: "Arimo", Helvetica, sans-serif; }

            h2 {
                margin-bottom: 20px;
                font-weight: normal; }

            .profile-header {
                background: #000;
                padding: 20px 0;
                text-align: center; }

            .profile-details {
                margin-bottom: 15px;
                padding: 40px 80px;
                background: #F9F9F9; }

            .profile-item-title {
                margin-bottom: 0; }

            .profile-item-description {
                color: #888;
                margin-top: 10px; }

            .profile-details table {
                width: 100%; }
            .profile-details table .profile-image {
                width: 18%; }
            .profile-details table .profile-info {
                padding-left: 20px; }
            .profile-details table .profile-info .details-name {
                font-size: 20px;
                /*line-height: 24px;*/
                margin: 10px 10px 10px; }

            .profile-items > div {
                padding: 15px 40px; }
            .profile-items > div:last-child {
                border-bottom: 0; }

            .profile-language small {
                color: #888;
                font-weight: normal; }

            .qualification-title small {
                margin-left: 30px; }
            .profile-items{
                margin-bottom: 30px;
            }
            .profile-item-title, profile-item-subtitle{
                color: black;
                margin-bottom: 10px;
                margin-top: 10px;
            }
            .details-name{
                margin-bottom: 20px;
                margin-top: 20px;
            }
            .details-current-job, .details-location, .details-url{
                margin-bottom: 5px;
                margin-top: 5px;
            }
           .profile-image{
                display: inline-block;
                width: 30%;
                float: left;
            }
            .profile-info{
                display: inline-block;
                width: 70%;
                float: left;
            }
           
            /*# sourceMappingURL=cv.css.map */

        </style>
    </head>

    <body>
        <div id="profile-pdf">
            <div class="profile-header">
                <img width="50" height="35" src="<?php echo Yii::app()->theme->baseUrl; ?>/images/cvvid-logo-white.jpg" alt="CV Vid"/>
            </div>

            <div class="profile-details">
<!--                <table>
                    <tbody>
                        <tr>-->
                            <div class="profile-image">
                                <?php
                                $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $model->id, 'type' => $model->type));
                                $addressmodel = Addresses::model()->findByAttributes(array('model_id' => $model->id));
                                $mediamodel = Media::model()->findByPk($profilemodel->photo_id);
                                ?>
                                <img width="120px" height="140px" src="<?php echo ((!empty($mediamodel->file_name) && $mediamodel->file_name != null) ? Yii::app()->baseUrl . "/images/media/" . $profilemodel->photo_id . "/conversions/thumb.jpg"  : Yii::app()->baseUrl . "/images/defaultprofile.jpg"); ?>" alt="" id="profile-image"/>
                            </div>
                            <div class="profile-info">
                                <h4 class="details-name"><?php echo $model->forenames . " " . $model->surname ?></h4>
                                <div class="details-current-job"><?php echo $model->current_job ?></div>
                                <div class="details-location"><?php echo $addressmodel->county . "," .$addressmodel->country?></div>
                                <div class="details-url">
                                    <a href="#"><?php echo "https://cvvidcareers.co.uk/". $profilemodel->slug ?></a>
                                </div>
                            </div>
<!--                        </tr>
                    </tbody>
                </table>-->
            </div>

            <?php
            $expmodel = UserExperiences::model()->findAllByAttributes(array('user_id' => $model->id));
            if (count($expmodel) > 0) {
                ?>    
                <!-- Experiences -->
                <div class="profile-experiences cv-section">
                    <h2>Professional Experience</h2>
                    <?php
                    $expmodel = UserExperiences::model()->findAllByAttributes(array('user_id' => $model->id));
                    foreach ($expmodel as $exp) {
                        ?>
                        <div class="profile-items">
                            <div class="profile-item profile-experience">
                                <h3 class="profile-item-title experience-title"><?php echo ($exp['company_name'] . ($exp['location'] != null ? "(" . $exp['location'] . ")" : "")) ?></h3>
                                <div class="profile-item-position"><?php echo $exp['position'] ?></div>
                                <div class="profile-item-dates experience-dates"> <?php echo $exp['start_date'] ?> to    <?php echo $exp['end_date'] == null ? "Present" : $exp['end_date'] ?></div>
                                <div class="profile-item-description experience-details">
                                    <?php echo $exp['description'] ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php
            }
            $qualmodel = UserQualifications::model()->findAllByAttributes(array('user_id' => $model->id));
            if (count($qualmodel) > 0) {
                ?>
                <!-- Education -->
                <div class="profile-qualifications cv-section">
                    <h2>Qualifications</h2>
                    <?php
                    $qualmodel = UserQualifications::model()->findAllByAttributes(array('user_id' => $model->id));
                    foreach ($qualmodel as $qual) {
                        ?>
                        <div class="profile-items">
                            <div class="profile-item profile-qualification">
                                <h3 class="profile-item-title"><?php echo $qual['institution'] ?></h3>
                                <div class="profile-item-subtitle qualification-title"><?php echo $qual['field'] ?><small><?php echo $qual['level'] ?></small></div>
                                <div class="profile-item-description qualification-details">
                                    <?php echo $qual['description'] ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php
            }
            $langmodel = UserLanguages::model()->findAllByAttributes(array('user_id' => $model->id));
            if (count($langmodel) > 0) {
                ?>
                <!-- Languages -->
                <div class="profile-languages cv-section">
                    <h2>Languages</h2>
                    <?php
                    foreach ($langmodel as $lang) {
                        ?>
                        <div class="profile-items">
                            <div class="profile-language">
                                <h3> <?php echo $lang['name'] ?>  <small>  <?php echo $lang['proficiency']; ?></small></h3>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php
            }
            $hobmodel = UserHobbies::model()->findAllByAttributes(array('user_id' => $model->id));
            if (count($hobmodel) > 0) {
                ?>
                <!-- Hobbies -->
                <div class="profile-hobbies cv-section">
                    <h2>Hobbies</h2>
                    <?php
                    $hobmodel = UserHobbies::model()->findAllByAttributes(array('user_id' => $model->id));
                    foreach ($hobmodel as $hob) {
                        ?>
                        <div class="profile-items">
                            <div class="profile-item profile-hobby">
                                <h3 class="profile-item-title">  <?php echo $hob['activity'] ?></h3>
                                <div class="hobby-details profile-item-description">
                                    <?php echo $hob['description'] ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </body>
</html>