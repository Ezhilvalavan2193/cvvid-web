<?php
/* @var $this InstitutionUpgradeController */
/* @var $model InstitutionUpgrade */

$this->breadcrumbs=array(
	'Institution Upgrades'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List InstitutionUpgrade', 'url'=>array('index')),
	array('label'=>'Create InstitutionUpgrade', 'url'=>array('create')),
	array('label'=>'View InstitutionUpgrade', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage InstitutionUpgrade', 'url'=>array('admin')),
);
?>

<h1>Update InstitutionUpgrade <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>