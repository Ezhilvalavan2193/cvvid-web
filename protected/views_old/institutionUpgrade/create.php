<?php
/* @var $this InstitutionUpgradeController */
/* @var $model InstitutionUpgrade */

$this->breadcrumbs=array(
	'Institution Upgrades'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List InstitutionUpgrade', 'url'=>array('index')),
	array('label'=>'Manage InstitutionUpgrade', 'url'=>array('admin')),
);
?>

<h1>Create InstitutionUpgrade</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>