<?php
/* @var $this InstitutionUpgradeController */
/* @var $data InstitutionUpgrade */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('institution_id')); ?>:</b>
	<?php echo CHtml::encode($data->institution_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('num_pupils')); ?>:</b>
	<?php echo CHtml::encode($data->num_pupils); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />


</div>