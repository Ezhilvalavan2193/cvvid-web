<?php
/* @var $this InstitutionUpgradeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Institution Upgrades',
);

$this->menu=array(
	array('label'=>'Create InstitutionUpgrade', 'url'=>array('create')),
	array('label'=>'Manage InstitutionUpgrade', 'url'=>array('admin')),
);
?>

<h1>Institution Upgrades</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
