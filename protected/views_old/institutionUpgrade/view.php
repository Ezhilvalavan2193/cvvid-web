<?php
/* @var $this InstitutionUpgradeController */
/* @var $model InstitutionUpgrade */

$this->breadcrumbs=array(
	'Institution Upgrades'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List InstitutionUpgrade', 'url'=>array('index')),
	array('label'=>'Create InstitutionUpgrade', 'url'=>array('create')),
	array('label'=>'Update InstitutionUpgrade', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete InstitutionUpgrade', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage InstitutionUpgrade', 'url'=>array('admin')),
);
?>

<h1>View InstitutionUpgrade #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'institution_id',
		'num_pupils',
		'status',
	),
)); ?>
