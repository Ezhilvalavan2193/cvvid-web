<?php
/* @var $this ProfileDocumentsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Profile Documents',
);

$this->menu=array(
	array('label'=>'Create ProfileDocuments', 'url'=>array('create')),
	array('label'=>'Manage ProfileDocuments', 'url'=>array('admin')),
);
?>

<h1>Profile Documents</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
