<?php
/* @var $this ProfileDocumentsController */
/* @var $model ProfileDocuments */

$this->breadcrumbs=array(
	'Profile Documents'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ProfileDocuments', 'url'=>array('index')),
	array('label'=>'Create ProfileDocuments', 'url'=>array('create')),
	array('label'=>'Update ProfileDocuments', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ProfileDocuments', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProfileDocuments', 'url'=>array('admin')),
);
?>

<h1>View ProfileDocuments #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'profile_id',
		'user_document_id',
		'created_at',
		'updated_at',
	),
)); ?>
