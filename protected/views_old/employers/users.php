<?php
/* @var $this EmployersController */
/* @var $model Employers */

$this->breadcrumbs = array(
    'Users' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#employers-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<script>
    $(document).ready(function () {


        $('.EmployerUser__filters').on('click', '.ActivateBtn', function () {


            if ($("#employers-users-grid").find("input:checked").length > 0)
            {

                var ids = []; // initialize empty array 
                $("input[name='selectedIds[]']:checkbox:checked").each(function () {
                    ids.push($(this).val());
                });

                $.ajax({
                    url: '<?php echo $this->createUrl('employers/usersActivate') ?>',
                    type: 'POST',
                    data: {ids: ids},
                    success: function (data) {
                         location.reload();
                       // $("#site-content").load(location.href + " #site-content>*", "");
                    },
                    error: function (data) {
                        alert('err');
                    }
                });

            } else
            {
                alert('Please select at least one item');
            }
        });

        $('.EmployerUser__filters').on('click', '.BlockBtn', function () {


            if ($("#employers-users-grid").find("input:checked").length > 0)
            {

                var ids = []; // initialize empty array 
                $("input[name='selectedIds[]']:checkbox:checked").each(function () {
                    ids.push($(this).val());
                });

                $.ajax({
                    url: '<?php echo $this->createUrl('employers/usersBlock') ?>',
                    type: 'POST',
                    data: {ids: ids},
                    success: function (data) {
                        location.reload();
                       // $("#site-content").load(location.href + " #site-content>*", "");
                    },
                    error: function (data) {
                        alert('err');
                    }
                });

            } else
            {
                alert('Please select at least one item');
            }
        });

		  $('.EmployerUser__filters').on('click', '.DeleteBtn', function () {


            if ($("#employers-users-grid").find("input:checked").length > 0)
            {

                var ids = []; // initialize empty array 
                $("input[name='selectedIds[]']:checkbox:checked").each(function () {
                    ids.push($(this).val());
                });

                $.ajax({
                    url: '<?php echo $this->createUrl('employers/usersDelete') ?>',
                    type: 'POST',
                    data: {ids: ids},
                    success: function (data) {
                        location.reload();
                       // $("#site-content").load(location.href + " #site-content>*", "");
                    },
                    error: function (data) {
                        alert('err');
                    }
                });

            } else
            {
                alert('Please select at least one item');
            }
        });
		
    });

</script>

<div class="page-title text-left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Manage Users</h1>
            </div>
        </div>
    </div>
</div>
<div id="page-content">
    <div class="container">
        <div class="row">
            <?php if (Yii::app()->user->hasFlash('success')): ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php echo Yii::app()->user->getFlash('success'); ?>
                </div>
            <?php endif; ?>
            <?php echo CHtml::hiddenField('employer_id', $model->id, array('id' => 'employer_id')); ?>
            <div class="EmployerUser__filters">
                <div class="row">
<!--                    <div class="col-sm-6">
                        <form method="GET" action="<?php echo Yii::app()->createUrl('employerUsers/userssearch'); ?>" accept-charset="UTF-8">
                            <div class="input-group">
                                <input class="form-control" placeholder="Search" name="search" type="text" value="">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </form>
                    </div>-->
                    <div class="col-sm-12 text-right">
                        <a class="btn btn-primary" href="<?php echo Yii::app()->createUrl('employers/userCreate', array('id' => $model->id)); ?>"><i class="fa fa-plus"></i> Add User</a>
                        <button type="button" class="ActivateBtn btn btn-default"><i class="fa fa-check"></i> Activate</button>
                        <button type="button" class="BlockBtn btn btn-default"><i class="fa fa-ban"></i> Block</button>
                        <button type="button" class="DeleteBtn btn btn-default"><i class="fa fa-close"></i> Delete</button>
                    </div>
                </div>
            </div>
            <?php
            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'employers-users-grid',
                'summaryText' => '',
                'itemsCssClass' => 'table table-striped',
                //'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                'pager' => array(
                    //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                    'header' => '',
                    'firstPageLabel' => '<<',
                    'lastPageLabel' => '>>',
                    'nextPageLabel' => '>>',
                    'prevPageLabel' => '<<'),
                'ajaxUpdate' => false,
                'dataProvider' => EmployerUsers::model()->searchByID($model->id),
                //'filter'=>$model,
                'columns' => array(
                    //'id',
                    array(
                        // 'name' => 'check',
                        'id' => 'selectedIds',
                        'value' => '$data->user_id',
                        'class' => 'CCheckBoxColumn',
                        'selectableRows' => 2,
                    // 'checked'=>'Yii::app()->user->getState($data->id)',
                    // 'checkBoxHtmlOptions' => array('name' => 'idList[]'),
                    ),
                    array(
                        'header' => 'Name',
                        'value' => 'CHtml::link("<i class=\"fa fa-pencil\"></i> ".$data->user->forenames." ".$data->user->surname,Yii::app()->createUrl("employers/userEdit",array("id"=>$data->user->id)),array("class"=>"user-edit-button","id"=>"user-edit"))',
                        'type' => 'raw'
                    ),
                    array(
                        'header' => 'Email',
                        'value' => '$data->user->email',
                        'type' => 'raw'
                    ),
                    array(
                        'header' => 'Posted Jobs',
                        'value' => '$data->getCount()',
                        'type' => 'raw'
                    ),
                    array(
                        'header' => 'Status',
                        'value' => '$data->user->status',
                        'type' => 'raw'
                    ),
                    array(
                        'header' => 'Created On',
                        'value' => '$data->created_at',
                    ),
                ),
            ));
            ?>

        </div>
    </div>
</div>





