<?php
/* @var $this EmployersController */
/* @var $model Employers */

$this->breadcrumbs=array(
	'Employers'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Employers', 'url'=>array('index')),
	array('label'=>'Create Employers', 'url'=>array('create')),
	array('label'=>'Update Employers', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Employers', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Employers', 'url'=>array('admin')),
);
?>

<h1>View Employers #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'name',
		'email',
		'body',
		'location',
		'location_lat',
		'location_lng',
		'website',
		'tel',
		'stripe_active',
		'stripe_id',
		'stripe_subscription',
		'stripe_plan',
		'last_four',
		'card_expiry',
		'card_expiry_sent',
		'trial_ends_at',
		'subscription_ends_at',
		'published',
		'created_at',
		'updated_at',
		'deleted_at',
	),
)); ?>
