<?php
/* @var $this EmployersController */
/* @var $model Employers */

$this->breadcrumbs=array(
	'Employers'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#employers-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<script>
$(document).ready(function(){
	

	$('.industry-action').click(function(e){

		var action = $(this).attr('id');
		
         if($("#industry-approval-grid").find("input:checked").length > 0)
         {

        	 if (confirm('Are you sure you want to perform this action?')) {

         		 var ids = $.fn.yiiGridView.getChecked("industry-approval-grid", "industryIds");
             	
       			  $.ajax({
       		            url: '<?php echo $this->createUrl('employers/industryAction') ?>',
       		            type: 'POST',
       		            data: {ids : ids ,action : action}, 	    
       		            success: function(data) {
                                $.fn.yiiGridView.update('industry-approval-grid');
       			            //alert(data);
       		            },
                            error: function(data) {		
                                alert('err');
                            }
       		         });
        	 } 
        	
         }
         else
         {
            alert('Please select at least one item');
         }
	});
	
	$('.skillcategory-action').click(function(e){

        if($("#sc-approval-grid").find("input:checked").length > 0)
        {

       	 if (confirm('Are you sure you want to perform this action?')) {

       		var action = $(this).attr('id');
        		 var ids = $.fn.yiiGridView.getChecked("sc-approval-grid", "skillcatIds");
            	
      			  $.ajax({
      		            url: '<?php echo $this->createUrl('employers/skillcategoryAction') ?>',
      		            type: 'POST',
      		            data: {ids : ids, action: action}, 	    
      		            success: function(data) {
                               $.fn.yiiGridView.update('sc-approval-grid');
      			            //alert(data);
      		            },
                           error: function(data) {		
                               alert('err');
                           }
      		         });
       	 } 
       	
        }
        else
        {
           alert('Please select at least one item');
        }
	});
	
	$('.skills-action').click(function(e){

        if($("#skill-approval-grid").find("input:checked").length > 0)
        {

       	 if (confirm('Are you sure you want to perform this action?')) {

        		 var ids = $.fn.yiiGridView.getChecked("skill-approval-grid", "skillIds");
        		 var action = $(this).attr('id');
      			  $.ajax({
      		            url: '<?php echo $this->createUrl('employers/skillsAction') ?>',
      		            type: 'POST',
      		            data: {ids : ids, action : action}, 	    
      		            success: function(data) {
                               $.fn.yiiGridView.update('skill-approval-grid');
      			            //alert(data);
      		            },
                           error: function(data) {		
                               alert('err');
                           }
      		         });
       	 } 
       	
        }
        else
        {
           alert('Please select at least one item');
        }
	});
	
});

</script>
<h3>Industry Approval Section</h3>
<?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
 <div class="pull-right">
        <?php  
        echo CHtml::button('Approve',array("class"=>"btn btn-primary industry-action","id"=>"approve"));
        echo CHtml::button('Ignore',array("class"=>"btn btn-primary industry-action","id"=>"ignore"));
        ?>
 </div>
<div class="Admin__content__inner">
  
  <?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'industry-approval-grid',
    'itemsCssClass' => 'table',
	'summaryText'=>'',
	//'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',
    'pager' => array(
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'header'=>'',
        'firstPageLabel' => '<<',
        'lastPageLabel' => '>>',
        'nextPageLabel' => '>>',
        'prevPageLabel' => '<<'),
			'ajaxUpdate'=>false,
	'dataProvider'=>$imodel->search(),
	//'filter'=>$model,
	'columns'=>array(
			//'id',
                        array(
                            // 'name' => 'check',
                             'id' => 'industryIds',
                             'value' => '$data->id',
                             'class' => 'CCheckBoxColumn',
                             'selectableRows'  => 2,
                            // 'checked'=>'Yii::app()->user->getState($data->id)',
                            // 'checkBoxHtmlOptions' => array('name' => 'idList[]'),

                         ),
		
			array(
					'header'=>'Employer',
			        'value'=>'$data->employer_id > 0 ? $data->employers->name : ""',
			),
			array(
					'header'=>'Industry',
					'value'=>'$data->industry->name',
			),
	),
)); ?>

</div>

<h3>Skill Category Approval Section</h3>
 <div class="pull-right">
        <?php  
           echo CHtml::button('Approve',array("class"=>"btn btn-primary skillcategory-action","id"=>"approve"));
           echo CHtml::button('Ignore',array("class"=>"btn btn-primary skillcategory-action","id"=>"ignore"));
        ?>
 </div>
<div class="Admin__content__inner">
   
  <?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'sc-approval-grid',
    'itemsCssClass' => 'table',
	'summaryText'=>'',
	//'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',
    'pager' => array(
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'header'=>'',
        'firstPageLabel' => '<<',
        'lastPageLabel' => '>>',
        'nextPageLabel' => '>>',
        'prevPageLabel' => '<<'),
			'ajaxUpdate'=>false,
	'dataProvider'=>$scmodel->search(),
	//'filter'=>$model,
	'columns'=>array(
			//'id',
                        array(
                            // 'name' => 'check',
                             'id' => 'skillcatIds',
                             'value' => '$data->id',
                             'class' => 'CCheckBoxColumn',
                             'selectableRows'  => 2,
                            // 'checked'=>'Yii::app()->user->getState($data->id)',
                            // 'checkBoxHtmlOptions' => array('name' => 'idList[]'),

                         ),
            	    array(
            	        'header'=>'Employer',
            	        'value'=>'$data->employer_id > 0 ? $data->employers->name : ""',
            	    ),
            	    array(
            	        'header'=>'Skill Category',
            	        'value'=>'$data->skillcategory->name',
            	    ),
	),
)); ?>

</div>


<h3>Skills Approval Section</h3>
 <div class="pull-right">
        <?php  
        echo CHtml::button('Approve',array("class"=>"btn btn-primary skills-action","id"=>"approve"));
        echo CHtml::button('Ignore',array("class"=>"btn btn-primary skills-action","id"=>"ignore"));
        ?>
 </div>
<div class="Admin__content__inner">
   
  <?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'skill-approval-grid',
    'itemsCssClass' => 'table',
	'summaryText'=>'',
	//'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',
    'pager' => array(
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'header'=>'',
        'firstPageLabel' => '<<',
        'lastPageLabel' => '>>',
        'nextPageLabel' => '>>',
        'prevPageLabel' => '<<'),
			'ajaxUpdate'=>false,
	'dataProvider'=>$smodel->search(),
	//'filter'=>$model,
	'columns'=>array(
			//'id',
                        array(
                            // 'name' => 'check',
                             'id' => 'skillIds',
                             'value' => '$data->id',
                             'class' => 'CCheckBoxColumn',
                             'selectableRows'  => 2,
                            // 'checked'=>'Yii::app()->user->getState($data->id)',
                            // 'checkBoxHtmlOptions' => array('name' => 'idList[]'),

                         ),
        	    array(
        	        'header'=>'Employer',
        	        'value'=>'$data->employer_id > 0 ? $data->employers->name : ""',
        	    ),
        	    array(
        	        'header'=>'Skill Category',
        	        'value'=>'$data->skillcategory->name',
        	    ),
        	    array(
        	        'header'=>'Skill',
        	        'value'=>'$data->skill->name',
        	    )
	),
)); ?>

</div>

