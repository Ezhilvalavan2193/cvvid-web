<?php
/* @var $this EmployersController */
/* @var $model Employers */

$this->breadcrumbs=array(
	'Employers'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Employers', 'url'=>array('index')),
	array('label'=>'Create Employers', 'url'=>array('create')),
	array('label'=>'View Employers', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Employers', 'url'=>array('admin')),
);
?>

<h1>Update Employers <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>