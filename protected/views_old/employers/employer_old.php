<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs = array(
    'Employers' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});

");
?> 
<script>
$(document).ready(function () {
	
$('.ProfileEdit__operations').on('click', '.visiblity', function (e) {
    e.preventDefault();

    var status = $(this).attr('id');
    var employerid = $('#employer_id').val();

    $.ajax({
        url: '<?php echo $this->createUrl('employers/changeEmployerVisiblity') ?>',
        type: 'POST',
        async: false,
        data: {status: status, employerid: employerid},
        success: function (data) {
            $(".ProfileEdit__operations").load(location.href + " .ProfileEdit__operations>*", "");
        },
        error: function (data) {
            alert('err');
        }
    });
    return false;
});

$('.ProfileEdit__operations').on('click', '.emp-status', function (e) {
    e.preventDefault();

    var status = $(this).attr('id');
    var employerid = $('#employer_id').val();

    $.ajax({
        url: '<?php echo $this->createUrl('employers/adminChangeStatus') ?>',
        type: 'POST',
        async: false,
        data: {status: status, employerid: employerid},
        success: function (data) {
            $(".ProfileEdit__operations").load(location.href + " .ProfileEdit__operations>*", "");
        },
        error: function (data) {
            alert('err');
        }
    });
    return false;
});

});
</script>

<div id="profile">
    <?php //echo "sd"; die();
    $employerid = isset($this->employerid) ? $this->employerid : 0;
    $userid = isset($this->userid) ? $this->userid : 0;
    $institutionid = isset($this->institutionid) ? $this->institutionid : 0;
    $edit = isset($this->edit) ? $this->edit : 0;
    $this->renderPartial('//profiles/Cover', array('userid' => $userid, 'employerid' => $employerid, 'institutionid' => $institutionid, 'edit' => $edit));
    ?>
    <?php
        $usermodel = new Users();
        $profilemodel = new Profiles();
        if($model->user_id > 0)
        {
            $usermodel = Users::model()->findByPk($model->user_id);
            $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $model->id, 'type' => $usermodel->type));
        }
    ?>
    <div id="form-details">
        <div class="container">
            <div class="form-details-inner">
                <div class="form-user-details form-section">
                    <div class="row">
                        <div class="text-center col-sm-4 col-md-3 col-lg-2">
                            <div class="ProfileEdit__photo">
                                <?php 									     
                                if($profilemodel->photo_id > 0)
                                 {
                                     $mediamodel = Media::model()->findByPk($profilemodel->photo_id);
                                     $src = ((!empty($mediamodel->file_name) && $mediamodel->file_name != null) ? Yii::app()->baseUrl."/images/media/".$profilemodel->photo_id."/".$mediamodel->file_name : Yii::app()->baseUrl."/images/profile.png");
                                 } 
                                    else
                                    {
                                        $src = Yii::app()->baseUrl."/images/defaultprofile.jpg";
                                    }
                                    echo CHtml::image($src,"",array("id" => "profile-image",'alt'=>''));
                                   ?>
                                <?php if (isset($this->edit) && $this->edit) { ?>
                                    <a class="open-media profile-photo-btn" data-field="photo" data-target="#profile-image">
                                        <i class="fa fa-camera"></i>
                                        Change Photo
                                    </a>
                                <?php } ?>
                            </div>
                            <?php if(Yii::app()->user->getState('role') == "employer") { 
                                $cri = new CDbCriteria();
                                $cri->condition = "owner_id =".$model->id." and type like '%employer%'";
                                $profilemodel = Profiles::model()->find($cri);
                                ?>
                            <a class="btn btn-primary" href="<?php echo $this->createUrl('employers/edit', array('slug' => $profilemodel->slug)) ?>">
                                <i class="fa fa-pencil"></i>
                                <span>Edit Profile</span>
                            </a>
                            <?php } ?>
                        </div>
                        <div class="col-sm-5 col-md-6 col-lg-6">
                            <div class="ProfileEdit__details">
                                <h3>
                                    <?php echo $model->name; ?>
                                  <?php if (Yii::app()->user->getState('role') == "employer") { ?>
                                                <?php if($usermodel->is_premium){ ?>
                                                - <small class="subscription-marker subscription-marker--premium">Premium Account</small>
                                                <?php } else { ?>
                                                - <small class="subscription-marker subscription-marker--basic">Basic Account</small>
                                               <?php } ?>
                                        <?php } ?>
                                </h3>
                                <?php 
                                    $cri = new CDbCriteria();
                                    $cri->condition = "model_id =".$model->id." and model_type like '%employer%' and deleted_at is null";
                                    $addressmodel = Addresses::model()->find($cri);
                                    if($addressmodel != null)
                                    {
                                        $address = "";
                                        if($addressmodel->address != "")
                                            $address = $addressmodel->address.",";
                                        if($addressmodel->town != "")
                                            $address .= $addressmodel->town.",";
                                        if($addressmodel->postcode != "")
                                            $address .= $addressmodel->postcode.",";
                                        if($addressmodel->country != "")
                                            $address .= $addressmodel->country;
                                    }
                                ?>
                                <div>Location: <?php echo ($addressmodel == null ? $model->location : $address) ?></div>
                                <div>Phone: <?php echo $model->tel ?></div>
                                <div>Website: <?php echo $model->website ?></div>
                                <?php //if(Yii::app()->user->getState('role') == "admin")?>
                                <div>Contact: <?php echo $usermodel->forenames." ".$usermodel->surname;//Yii::app()->user->getState('username'); ?></div>
                            </div>
                        </div>

 <?php if(Yii::app()->user->getState('role') == "admin"){ ?>
                         <div class="col-sm-3 col-md-4 col-lg-4">
                            <div class="ProfileEdit__operations">
                                
                                    <div>
                                    <?php 
                                         $empusermodel = EmployerUsers::model()->findByAttributes(array('employer_id'=>$model->id));
                                         $umodel = Users::model()->findByPk($empusermodel->user_id);
                                    ?>
                                	<?php if ($umodel->status == "active") { ?>
                                        <a class="Profile__visibility btn emp-status btn-danger" id="<?php echo $umodel->status ?>" href="javascript::void(0)" title=""  aria-describedby="tooltippublic"><i class="fa fa-lock"></i>Block</a>
                                        
                                    <?php } else { ?>
                                        <a class="Profile__visibility btn emp-status btn-primary" id="<?php echo $umodel->status ?>" href="javascript::void(0)"  title="" aria-describedby="tooltipprivate"><i class="fa fa-unlock"></i>Unblock</a>
                                        
                                    <?php } ?>
                                	</div>
                                	<div>
                                     <?php if ($profilemodel->visibility) { ?>
                                        <a class="Profile__visibility btn visiblity btn-success" id="<?php echo $profilemodel->visibility ?>" href="javascript::void(0)" data-original-title="Currently Private, only those who know your profile can find your account" title=""  aria-describedby="tooltippublic"><i class="fa fa-unlock"></i>Public</a>
                                        <div class="tooltip fade top" role="tooltip" id="tooltippublic" style="top: -50px; left: 146.766px; display: block;"><div class="tooltip-arrow" style="left: 50%;"></div><div class="tooltip-inner">Currently Public, your profile can be found by employer searches</div></div>
                                    <?php } else { ?>
                                        <a class="Profile__visibility btn visiblity btn-default" id="<?php echo $profilemodel->visibility ?>" href="javascript::void(0)"  data-original-title="Currently Public, your profile can be found by employer searches" title="" aria-describedby="tooltipprivate"><i class="fa fa-lock"></i>Private</a>
                                        <div class="tooltip fade top" role="tooltip" id="tooltipprivate" style="top: -67px; left: 146.766px; display: block;"><div class="tooltip-arrow" style="left: 50%;"></div><div class="tooltip-inner">Currently Private, only those who know your profile can find your account</div></div>
                                    <?php } ?>
                                   
                                     <?php 
                                    if(!Membership::subscribed($model->stripe_active, $model->trial_ends_at, $model->subscription_ends_at))
                                    {
                                    ?>
                                    <a class="btn btn-default btn-upgrade upgrade" href="<?php echo $this->createUrl('employers/adminupgrade',array('id'=>$model->id)) ?>">
                                        <i class="fa fa-user"></i>
                                        <span>Basic</span>
                                    </a>
                                    <?php } 
                                   else {
                                    // if(Membership::onTrail($model->subscription_ends_at)){ ?>
                                        <div class="btn btn-default" data-toggle="tooltip" data-placement="top" >
                                           <i class="fa fa-credit-card"></i>
                                           <span>Premium</span>
                                       </div>
                                      <?php //} 
                                      /*else { ?>
                                         <a class="btn btn-default" href="<?php echo $this->createUrl('employers/mysubscription') ?>">
                                            <i class="fa fa-credit-card"></i>
                                            <span>Account</span>
                                        </a>
                                    <?php } */
                                    }  ?>
                                
                                </div>
                        </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
				<?php if($model->video_id > 0) { ?>
     				<div class="form-section">
                        <div class="form-section-inner">
                            <div class="section-header">
                                <h4>Company Video Profile</h4>
                            </div>
                            <?php $videomodel = Videos::model()->findByPk($model->video_id); ?>
                            <div data-type="vimeo" data-video-id="<?php echo $videomodel->video_id ?>"></div>
                        </div>
                    </div>
                <?php }?>
                <div class="form-section">
                    <div class="form-section-inner">
                        <div class="section-header">
                            <h4>Information</h4>
                        </div>
                        <div class="section-content">
                            <div class="EmployerProfile__body"><?php echo $model->body; ?></div>
                        </div>
                    </div>
                </div>

                <div class="form-section">
                    <div class="form-section-inner">
                        <div class="section-header">
                            <h4>Advertised Jobs</h4>
                        </div>
                        <div class="section-content">
                            <div class="EmployerProfile__jobs">
                                <?php 
                                $jobsmodel = Jobs::model()->findAllByAttributes(array('owner_id' => $model->id));
                                if (count($jobsmodel) > 0) {
                                ?>
                                <?php foreach ($jobsmodel as $job) { ?>
                                <div class="EmployerProfile__job">
                                     <h4><a href="<?php echo $this->createUrl('jobs/viewJob',array('id'=>$job['id'],'title'=>  str_replace(' ', '-', $job['title']))) ?>"><?php echo $job['title'] ?></a></h4>
                                    <ul>
                                        <li><?php echo "£".$job['salary_min']; ?> - <?php echo "£".$job['salary_max']; ?> per <?php echo $job['salary_type']; ?></li>
                                        <li><?php echo $job['location']; ?></li>
                                        <li>{<?php
                                $time_elapsed = timeAgo($job['created_at']);
                                echo $time_elapsed; ?></li>
                                    </ul>
                                    <div class="EmployerProfile__job__description"><?php echo $job['description'] ?></div>
                                </div>
                                <?php } ?>
                                <?php } else { ?>
                                <div class="EmployerProfile__job-empty">
                                    There are currently no jobs posted
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div><!-- /.container -->
    </div>
</div>

   

<?php 
//Function definition

function timeAgo($time_ago)
{
    $time_ago = strtotime($time_ago);
    $cur_time   = time();
    $time_elapsed   = $cur_time - $time_ago;
    $seconds    = $time_elapsed ;
    $minutes    = round($time_elapsed / 60 );
    $hours      = round($time_elapsed / 3600);
    $days       = round($time_elapsed / 86400 );
    $weeks      = round($time_elapsed / 604800);
    $months     = round($time_elapsed / 2600640 );
    $years      = round($time_elapsed / 31207680 );
    // Seconds
    if($seconds <= 60){
        return "just now";
    }
    //Minutes
    else if($minutes <=60){
        if($minutes==1){
            return "one minute ago";
        }
        else{
            return "$minutes minutes ago";
        }
    }
    //Hours
    else if($hours <=24){
        if($hours==1){
            return "an hour ago";
        }else{
            return "$hours hrs ago";
        }
    }
    //Days
    else if($days <= 7){
        if($days==1){
            return "yesterday";
        }else{
            return "$days days ago";
        }
    }
    //Weeks
    else if($weeks <= 4.3){
        if($weeks==1){
            return "a week ago";
        }else{
            return "$weeks weeks ago";
        }
    }
    //Months
    else if($months <=12){
        if($months==1){
            return "a month ago";
        }else{
            return "$months months ago";
        }
    }
    //Years
    else{
        if($years==1){
            return "one year ago";
        }else{
            return "$years years ago";
        }
    }
}
?>