<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs = array(
    'Create User' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
");
?> 
<!--<script>
    $(document).ready(function ()
    {
        $("#employer-users-form").submit(function (event) {
            //event.preventDefault();
            if ($('#password').val() != "" && $('#password').val() == $('#confirmpassword').val())
                $("#employer-users-form").submit();
            else
            {
                $('.pwdmsg').html('Password not matched');
                return false;
            }
        });
    });
</script>-->
<h1>Create User: <?php echo $model->name; ?></h1>
<?php


$form = $this->beginWidget('CActiveForm', array(
    'id' => 'employer-users-form',
    'action' => $this->createUrl('employers/createUsers', array('id' => $model->id)),
    'enableAjaxValidation' => false,
    'htmlOptions' => array('enctype' => 'multipart/form-data')
        ));
?>
 <?php echo  $form->errorSummary(array($usermodel,$addressmodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Account Details</h3>
    </div>
    <div class="panel-body">

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <?php echo $form->labelEx($usermodel, 'forenames'); ?>
                    <?php echo $form->textField($usermodel, 'forenames', array('class' => 'form-control', 'placeholder' => "Forenames")); ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <?php echo $form->labelEx($usermodel, 'surname'); ?>
                    <?php echo $form->textField($usermodel, 'surname', array('class' => 'form-control', 'placeholder' => "Surname")); ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <?php echo $form->labelEx($usermodel, 'email'); ?>
                    <?php echo $form->textField($usermodel, 'email', array('class' => 'form-control', 'placeholder' => "Email")); ?>
                </div>
            </div>
        </div><!-- .row -->

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <?php
                    $usermodel->password = "";
                    echo $form->labelEx($usermodel, 'password'); ?>
                    <?php echo $form->passwordField($usermodel, 'password', array('class' => 'form-control', 'id' => 'password','placeholder'=>"Password")); ?>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <?php echo CHtml::label('Confirm Password', '', array()); ?>
                    <input class="form-control" id="confirmpassword"  name="Users[confirmpassword]" type="password" value="" placeholder="Confirm Password">
                </div>
            </div>
        </div><!-- .row -->

        <div class="row">
        </div>

    </div><!-- .panel-body -->
</div><!-- .panel --><div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Details</h3>
    </div>
    <div class="panel-body">

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <?php echo CHtml::label('Date of Birth', '', array('class' => 'clearfix')); ?>
                    <div class="input-group"><input type="text" name="dob" placeholder="Date of Birth" class="dob form-control" value="<?php echo ((empty($usermodel->dob) || $usermodel->dob == null || $usermodel->dob == "0000-00-00") ? "" : date("d/m/Y", strtotime($usermodel->dob))); ?>"></div>
                </div>
            </div><!-- .col-sm-6 -->
            <div class="col-sm-4">
                <div class="form-group">
                    <?php echo $form->labelEx($usermodel, 'tel'); ?>
                    <?php echo $form->textField($usermodel, 'tel', array('class' => 'form-control','placeholder'=>'Telephone')); ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <?php echo $form->labelEx($usermodel, 'mobile'); ?>
                    <?php echo $form->textField($usermodel, 'mobile', array('class' => 'form-control','placeholder'=>"Mobile")); ?>
                </div>
            </div><!-- .col-sm-6 -->
        </div><!-- .row -->

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <?php echo $form->labelEx($usermodel, 'current_job'); ?>
                    <?php echo $form->textField($usermodel, 'current_job', array('class' => 'form-control','placeholder'=>'Current Job')); ?>
                </div>
            </div>
        </div>
    </div><!-- .panel-body -->
</div><!-- .panel -->

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Address</h3>
    </div>
    <div class="panel-body">
        <div id="location-fields">
            <?php $this->renderPartial('//addresses/address-form', array('form'=>$form,'addressmodel'=>$addressmodel)); ?>
        </div>

    </div>
</div>
<div class="form-actions clearfix">
    <?php echo CHtml::button('Back to Employer', array("class" => "btn btn-primary", "onclick" => "window.location='" . Yii::app()->createUrl('employers/tabusers', array('id' => $model->id)) . "'")); ?>
    <?php echo CHtml::submitButton('Save', array("class" => "btn btn-primary pull-right")); ?>
</div> 

<?php $this->endWidget(); ?>


