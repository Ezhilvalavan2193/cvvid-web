<?php
/* @var $this EmployersController */
/* @var $model Employers */
/* @var $form CActiveForm */
?>
<script>

$(document).ready(function(){

	var Password = {
			 
			  _pattern : /[a-zA-Z0-9_\-\+\.]/,
			  
			  
			  _getRandomByte : function()
			  {
			    // http://caniuse.com/#feat=getrandomvalues
			    if(window.crypto && window.crypto.getRandomValues) 
			    {
			      var result = new Uint8Array(1);
			      window.crypto.getRandomValues(result);
			      return result[0];
			    }
			    else if(window.msCrypto && window.msCrypto.getRandomValues) 
			    {
			      var result = new Uint8Array(1);
			      window.msCrypto.getRandomValues(result);
			      return result[0];
			    }
			    else
			    {
			      return Math.floor(Math.random() * 256);
			    }
			  },
			  
			  generate : function(length)
			  {
			    return Array.apply(null, {'length': length})
			      .map(function()
			      {
			        var result;
			        while(true) 
			        {
			          result = String.fromCharCode(this._getRandomByte());
			          if(this._pattern.test(result))
			          {
			            return result;
			          }
			        }        
			      }, this)
			      .join('');  
			  }    
			    
			};

	$(".students-upload").change(function() {

		 var file = this.files[0];

		  var reader = new FileReader();
                    reader.onload = function (progressEvent) {
                        
                        var lines = this.result.split('\n');
                        var cnt = 1;
 						for (var line = 0; line < lines.length; line++) {
                            if (cnt > 1)
                            {
                                var row = lines[line];
                                if (row != "")
                                {
                                    var array = row.split(',');

                                    var studclone = $('.user-item').find('.row').clone();
                                    $(".user-list").append(studclone);

                                    var c = 0;
                                    $(studclone).find('input').each(function () {
                                        $(this).val(array[c]);
                                        c++;
                                    });
                                }

                            }
                            cnt++;
                        }
                    };
                    reader.readAsText(file);               
        
            });

	$(".user-list").on('click','.remove_student',function(){
		$(this).closest('.row').remove();
	});
	$(".generate-password").click(function(){

		$(".user-list").find('.row').each(function( index ) {
			if($.trim($(this).find('.col-sm-3').find('.form-group').find(".passwordbox").val()) == "")
				$(this).find('.col-sm-3').find('.form-group').find(".passwordbox").val(Password.generate(10));
		});
		
	});

	$('#bulk-emp-form').submit(function () {
		
		 var err = 0;
		 if($('.user-list').find('.row').length > 0)
		 {
			 var arr = [];
			 $(".user-list").find('.row').each(function( index ) {
				 arr.push($(this).find('input.emailbox').val());
			 });
			 
			 $.ajax({
		            url: '<?php echo $this->createUrl('employers/checkEmails') ?>',
		            type: 'POST',
		            data: {emails : arr}, 	
		            dataType: "JSON",
		            async:false,    
		            success: function(data) {
// 			            alert(data);1
// 			            alert(arr);0
			            if(data != -1)
			            {
				            var msg = "";
	    	            	 for(var i=0;i<data.length;i++){
	    	                     msg += "<div>Email: "+arr[data[i]-1]+ " already taken</div>";
	    	                 }
	    	            	 $('#msg_err_msg').html(msg);
	    	            	 $('#msg_err_msg').show();
	    	            	 $(window).scrollTop(0);
	    	            	 err = 1;
			            }
			            else
			            	$('#msg_err_msg').hide();
//	 		            for(i = 0 )
//	 	            	$('#msg_err_msg').html("<div>"+)
		            },
				    error: function(data) {		
				        alert('err');
				    }
		         });

	         if(err > 0)
			 	return false;
	         else
		         return true;
			
		 }
			 
	});
	
});

</script>
<h1>Create Bulk Employers</h1>
  <div class="form-container">
     <div class="alert alert-danger" id='msg_err_msg' style="display: none">
                    </div>
           <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'bulk-emp-form',
                     'action'=>$this->createUrl('employers/createBulkEmployers'),
                    'enableAjaxValidation'=>false
            )); ?>
             <?php echo  $form->errorSummary(array($model), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
                    <div id="user-upload">
                        <h5>File Upload</h5>
                        <div class="user-file-upload form-inline">
                            <div class="form-group">
                                <input type="file" name="students" class="students-upload">
                            </div>
                            <a href="<?php echo $this->createUrl('institutions/sampleDownload'); ?>" class="btn btn-default" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download a sample upload file">
                                <i class="fa fa-info"></i>
                            </a>
                        </div>
                        <div class="user-list"></div>
                        <div class="user-item" style="display: none">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?php echo CHtml::textField('forenames[]','',array('placeholder'=>'Forename','class'=>'form-control')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?php echo CHtml::textField('surname[]','',array('placeholder'=>'Surname','class'=>'form-control')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?php echo CHtml::textField('email[]','',array('placeholder'=>'Email Address','class'=>'form-control emailbox')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?php echo CHtml::textField('password[]','',array('placeholder'=>'Password','class'=>'form-control passwordbox')); ?>
                                        <button type="button" class="remove_student delete-user btn btn-danger"><i class="fa fa-close"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="user-actions clearfix">
                         	<?php echo CHtml::submitButton('Save',array('class'=>'btn btn-default')); ?>
                            <button type="button" class="btn btn-default generate-password pull-right">Generate Passwords</button>
                        </div>
                    </div>
           <?php $this->endWidget(); ?>
  </div>
