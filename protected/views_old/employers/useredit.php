<?php
/* @var $this EmployersController */
/* @var $model Employers */
/* @var $form CActiveForm */
?>

<div class="page-title text-left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Edit User</h1>
            </div>
        </div>
    </div>
</div>
<div id="page-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <?php
                    $form = $this->beginWidget('CActiveForm', array(
                       'id' => 'employer-form',
                       'action' => $this->createUrl('employers/editUsers', array('id'=>$usermodel->id)),
                       'enableAjaxValidation' => false,
                       'htmlOptions' => array('enctype' => 'multipart/form-data')
                   ));
                   ?>
                   <?php echo  $form->errorSummary(array($usermodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Account Details</h3>
                        </div>
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="forenames" class="control-label">Forenames</label>
                                         <?php echo $form->textField($usermodel, 'forenames', array('class' => 'form-control','placeholder'=>'Forenames')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="surname" class="control-label">Surname</label>
                                        <?php echo $form->textField($usermodel, 'surname', array('class' => 'form-control','placeholder'=>'Surname')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="email" class="control-label">Email</label>
                                        <?php echo $form->textField($usermodel, 'email', array('class' => 'form-control','placeholder'=>'Email')); ?>
                                    </div>
                                </div>
                            </div><!-- .row -->

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="password" class="control-label">Password</label>
                                        <?php echo $usermodel->password = ""; ?>
                                        <?php echo $form->passwordField($usermodel, 'password', array('class' => 'form-control','placeholder'=>'Password')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="password_confirmation" class="control-label">Confirm Password</label>
                                        <input class="form-control" id="confirmpassword" placeholder="Confirm Password" name="Users[confirmpassword]" type="password" value="">
                                    </div>
                                </div>
                            </div><!-- .row -->

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label>
                                                <input name="inform_user" type="checkbox" value="1">
                                                Send credentials to the user
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div><!-- .panel-body -->
                    </div><!-- .panel -->

                    <div class="form-group">
                        <button type="submit" class="btn btn-default">Save User</button>
                    </div>                
             <?php $this->endWidget(); ?>
            </div>
        </div>
    </div>
</div>



        
