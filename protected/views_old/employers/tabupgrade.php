<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs = array(
    'Employers' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
    
");
?>


<h1><?php echo $model->name . ": Users"; ?></h1>
<?php if(Yii::app()->user->hasFlash('success')):?>
<div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
   <?php echo Yii::app()->user->getFlash('success'); ?>
</div>
<?php endif; ?>
<div class="EmployerUsers">
    <!-- Nav tabs -->
    <ul class="AdminTabs nav nav-tabs" role="tablist">
        <li role="presentation"><a href="<?php echo Yii::app()->createUrl("employers/tabedit", array("id" => $model->id)); ?>" aria-controls="details">Details</a></li>
        <li role="presentation" ><a href="<?php echo Yii::app()->createUrl("employers/tabusers", array("id" => $model->id)); ?>" aria-controls="users">Users</a></li>
        <li role="presentation"><a href="<?php echo Yii::app()->createUrl("employers/tabjobs", array("id" => $model->id)); ?>" aria-controls="jobs">Jobs</a></li>
        <li role="presentation"><a href="<?php echo Yii::app()->createUrl("employerOffices/admin", array("id" => $model->id)); ?>" aria-controls="offices">Offices</a></li>
        <li role="presentation" class="active"><a href="<?php echo Yii::app()->createUrl("employers/tabupgrade", array("id" => $model->id)); ?>" aria-controls="upgrade">Upgrade</a></li>
    </ul>
   
      <?php
     $form = $this->beginWidget('CActiveForm', array(
         'id' => 'upgrade-form',
         'action' => $this->createUrl('employers/premiumUpgrade',array('id'=>$model->id)),
         // Please note: When you enable ajax validation, make sure the corresponding
         // controller action is handling ajax validation correctly.
         // There is a call to performAjaxValidation() commented in generated controller code.
         // See class documentation of CActiveForm for details on this.
         'enableAjaxValidation' => false,
         'htmlOptions' => array('enctype' => 'multipart/form-data','class' => 'form-horizontal'),
     ));
     ?>
     <div class="form-container">
     			<?php if (Yii::app()->user->hasFlash('success')): ?>
                		<div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                             <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                     <?php endif; ?>
        <div class="row">
                     <div class="col-sm-3">
                         <div class="form-group">
                             <input type="text" name="end_date"  autocomplete='off' class="datepicker form-control" value="" Placeholder="End Date">
                             
                         </div>
                     </div>
          </div><!-- .row -->
         <div class="form-actions">         
        		<input type="submit" class="btn btn-default" value="Upgrade" >
        </div>
    </div>
     <?php $this->endWidget(); ?>
     
</div>


