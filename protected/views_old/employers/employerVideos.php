<div class="profile-section-info">
 <h4>Company Profile</h4>
    <div class="VideoList">
        <?php
             $mediamodel = Media::model()->findByAttributes(array('model_id' => $model->video_id,'model_type'=>'Employer','collection_name'=>'video'));
             if($mediamodel != null)
             {
               $videomodel = Videos::model()->findByPk($model->video_id);
               // if($videomodel->duration <= 0)
               // {
               //     $videoid = $videomodel->video_id;
               //     $json_url = 'http://vimeo.com/api/v2/video/'.$videoid.'.xml';
               //     $ch = curl_init($json_url);
               //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
               //     curl_setopt($ch, CURLOPT_HEADER, 0);
               //     $data = curl_exec($ch);
               //     curl_close($ch);
               //     $data = new SimpleXmlElement($data, LIBXML_NOCDATA);
               //     $duration = isset($data->video->duration) ? $data->video->duration : 0;
               //     $imgpath = $data->video->thumbnail_large;
                   
               //     //$iname = pathinfo($name, PATHINFO_FILENAME);//substr($imgpath, $pos + strlen($del), strlen($imgpath) - 1);
               //     $img_name = $mediamodel->file_name;
                   
               //     $mediamodel->size = (int)$duration;
               //     if ($mediamodel->save()) {
               //         $videomodel->duration = (int)$duration;
               //         $videomodel->save();
                       
               //         $filename = $img_name;
               //         $mediafolder = "images/media/" . $mediamodel->id . "/conversions";
                       
               //         $imgprefix = "images/media/" . $mediamodel->id;
               //         $fullpath = "images/media/" . $mediamodel->id . "/" . $img_name;
                       
               //         if (!file_exists($mediafolder)) {
               //             mkdir($mediafolder, 0777, true);
               //         }
                       
               //         if (copy($imgpath, $fullpath)) {
                           
               //             //create joblisting
               //             $jlwidth = 350;
               //             $jlheight = 190;
               //             $jldestpath = $imgprefix . "/conversions/profile.jpg";
                           
               //             //create search
               //             $searchwidth = 167;
               //             $searchheight = 167;
               //             $searchdestpath = $imgprefix . "/conversions/search.jpg";
                           
               //             //create thumb
               //             $thumbwidth = 126;
               //             $thumbheight = 126;
               //             $thumbdestpath = $imgprefix . "/conversions/thumb.jpg";
                           
                           
               //             $image = imagecreatefromjpeg($fullpath);
                           
               //             //$this->resize_image($image, $image_type, $fullpath, $coverdestpath, $coverwidth, $coverheight); //create cover
               //             //$this->resize_image($image, $image_type, $fullpath, $icondestpath, $iconwidth, $iconheight); //create icon
               //             resize_image($image, "image/jpg", $fullpath, $jldestpath, $jlwidth, $jlheight); //create joblisting
               //             resize_image($image, "image/jpg", $fullpath, $searchdestpath, $searchwidth, $searchheight); //create search
               //             resize_image($image, "image/jpg", $fullpath, $thumbdestpath, $thumbwidth, $thumbheight); //create thumb
                           
               //         }
               //     }
               // }
           ?>       
            <!--  <div data-type="vimeo" data-video-id="<?php echo $videomodel->video_id ?>"></div>-->
     
           <div class="ProfileVideo">
                    <div class="ProfileVideo__inner">
                        <div class="row">
                           <div class="col-sm-4">
                            <div class="ProfileVideo__thumb">
                                <img src="<?php echo Yii::app()->baseUrl . "/images/media/" . $mediamodel->id . "/" . $mediamodel->file_name ?>" class="img-responsive">
							 </div>
							 	
                          </div>
                           <div class="ProfileVideo__buttons">                                   
                                    <a class="ProfileVideo__delete emp_video_delete main-btn" href="<?php echo $this->createUrl('employers/deleteCompanyProfile',array('id'=>$model->id))?>" id="<?php echo $model->id ?>">Delete</a>                                   
                                </div>
                        </div>
                    </div>
                </div>
          <?php 
               }
          ?>
                
    </div>
    <div class="VideoUpload" style="display: <?php echo ($model->video_id > 0 ? "none" : "") ?>">
        <div>
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'emp-video-form',
                'action' => $this->createUrl('employers/addProfileVideo'), 
                'enableAjaxValidation' => true,
                'htmlOptions' => array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data')
            ));
            ?>
            <div class="progress" style="display:none;">
                <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                    <div class="status">0%</div>
                </div>
            </div>
            <div class="UploadMessage alert alert-success" role="alert" style="display:none;">
                Thanks for uploading your video. Our clever algorithms will optimise the file before it is available to view.
            </div>
            <input type="hidden" name="employerid" value="<?php echo $model->id ?>">
            <input type="text" class="form-control col-sm-3 VideoUpload__name" id="video-name" name="video_name" placeholder="Video Name">
            <label class="btn main-btn add-new" for="NewVideo">
                Choose Video
                <input type="file" id="NewVideo" name="video" accept="video/*">
            </label>
            <input type="submit" class="btn btn-primary" value="Upload Video">
            <!--<button type="submit" class="btn btn-primary">Upload Video</button>-->
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>
<?php 
function resize_image($image, $image_type, $fullpath, $destpath, $width, $height) {
    $size_arr = getimagesize($fullpath);
    
    list($width_orig, $height_orig, $img_type) = $size_arr;
    $ratio_orig = $width_orig / $height_orig;
    
    $tempimg = imagecreatetruecolor($width, $height);
    imagecopyresampled($tempimg, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
    
    if ($image_type == "image/jpg" || $image_type == "image/jpeg")
        imagejpeg($tempimg, $destpath);
        else if ($image_type == "image/png")
            imagepng($tempimg, $destpath);
            else if ($image_type == "image/gif")
                imagegif($tempimg, $destpath);
}
?>