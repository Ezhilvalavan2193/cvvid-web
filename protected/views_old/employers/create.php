<?php
/* @var $this EmployersController */
/* @var $model Employers */

$this->breadcrumbs=array(
	'Employers'=>array('index'),
	'Create',
);

$this->menu=array(
//	array('label'=>'List Employers', 'url'=>array('index')),
	array('label'=>'Manage Employers', 'url'=>array('admin')),
);
?>

<?php

$this->renderPartial('_form', array('model'=>$model,'usermodel'=>$usermodel,'addressmodel'=>$addressmodel,'profilemodel'=>$profilemodel)); 
?>