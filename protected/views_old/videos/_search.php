<?php

/* @var $this VideosController */
/* @var $model Videos */
/* @var $form CActiveForm */
?>


<?php

$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
        ));
?>


<?php echo $form->textField($model, 'name', array('class' => 'form-control', 'placeholder' => 'Enter Name')); ?>

<?php

$vcategmodel = VideoCategories::model()->findAll();
$vcateglist = CHtml::listData($vcategmodel, 'id', 'name');
?>
<?php echo $form->dropDownList($model, 'category_id', $vcateglist, array('class' => 'form-control', 'empty' => '- Please Select -')); ?>

<?php echo $form->dropDownList($model, 'status', array('active' => 'active', 'flagged' => 'flagged'), array('class' => 'form-control', 'empty' => '- Filter Status -')); ?>

<?php echo CHtml::submitButton('Search', array('class' => 'btn btn-primary')); ?>

<?php $this->endWidget(); ?>
