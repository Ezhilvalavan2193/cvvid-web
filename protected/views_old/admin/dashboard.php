<h1>Dashboard</h1>
<div class="Admin__content__inner">
    <div class="row">
        <div class="col-sm-6">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Statistics</h3>
                </div>
                <div class="panel-body">
                   <?php 
                        $usermodel = Users::model()->countByAttributes(array('deleted_at'=>null));   
                        $empmodel = Employers::model()->countByAttributes(array('deleted_at'=>null));   
                        $jobmodel = Jobs::model()->count();   
                        $jobappmodel = JobApplications::model()->countByAttributes(array('deleted_at'=>null));   
                        $videomodel = Videos::model()->countByAttributes(array('deleted_at'=>null));   
                    ?>
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>Users</td>
                                <td><?php echo $usermodel ?></td>
                            </tr>
                            <tr>
                                <td>Employers</td>
                                <td><?php echo $empmodel ?></td>
                            </tr>
                            <tr>
                                <td>Active Jobs</td>
                                <td><?php echo $jobmodel ?></td>
                            </tr>
                            <tr>
                                <td>Active Applications</td>
                                <td><?php echo $jobappmodel ?></td>
                            </tr>
                            <tr>
                                <td>Video</td>
                                <td><?php echo $videomodel ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>