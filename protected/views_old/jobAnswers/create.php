<?php
/* @var $this JobAnswersController */
/* @var $model JobAnswers */

$this->breadcrumbs=array(
	'Job Answers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List JobAnswers', 'url'=>array('index')),
	array('label'=>'Manage JobAnswers', 'url'=>array('admin')),
);
?>

<h1>Create JobAnswers</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>