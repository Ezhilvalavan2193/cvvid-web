<?php
/* @var $this JobAnswersController */
/* @var $model JobAnswers */

$this->breadcrumbs=array(
	'Job Answers'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List JobAnswers', 'url'=>array('index')),
	array('label'=>'Create JobAnswers', 'url'=>array('create')),
	array('label'=>'Update JobAnswers', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete JobAnswers', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage JobAnswers', 'url'=>array('admin')),
);
?>

<h1>View JobAnswers #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'job_id',
		'user_id',
		'question_id',
		'videoid',
		'status',
		'created_at',
		'updated_at',
	),
)); ?>
