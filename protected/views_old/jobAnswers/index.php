<?php
/* @var $this JobAnswersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Job Answers',
);

$this->menu=array(
	array('label'=>'Create JobAnswers', 'url'=>array('create')),
	array('label'=>'Manage JobAnswers', 'url'=>array('admin')),
);
?>

<h1>Job Answers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
