<?php
/* @var $this TempSkillCategoryController */
/* @var $model TempSkillCategory */

$this->breadcrumbs=array(
	'Temp Skill Categories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TempSkillCategory', 'url'=>array('index')),
	array('label'=>'Manage TempSkillCategory', 'url'=>array('admin')),
);
?>

<h1>Create TempSkillCategory</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>