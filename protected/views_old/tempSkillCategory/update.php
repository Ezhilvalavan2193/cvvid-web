<?php
/* @var $this TempSkillCategoryController */
/* @var $model TempSkillCategory */

$this->breadcrumbs=array(
	'Temp Skill Categories'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TempSkillCategory', 'url'=>array('index')),
	array('label'=>'Create TempSkillCategory', 'url'=>array('create')),
	array('label'=>'View TempSkillCategory', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TempSkillCategory', 'url'=>array('admin')),
);
?>

<h1>Update TempSkillCategory <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>