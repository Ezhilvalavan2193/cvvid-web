<?php
/* @var $this UserQualificationsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'User Qualifications',
);

$this->menu=array(
	array('label'=>'Create UserQualifications', 'url'=>array('create')),
	array('label'=>'Manage UserQualifications', 'url'=>array('admin')),
);
?>

<h1>User Qualifications</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
