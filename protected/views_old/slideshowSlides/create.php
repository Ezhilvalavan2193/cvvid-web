<?php
/* @var $this SlideshowSlidesController */
/* @var $model SlideshowSlides */

$this->breadcrumbs=array(
	'Slideshow Slides'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List SlideshowSlides', 'url'=>array('index')),
	array('label'=>'Manage SlideshowSlides', 'url'=>array('admin')),
);
?>

<h1>Create SlideshowSlides</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>