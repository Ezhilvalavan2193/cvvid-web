<?php
/* @var $this SlideshowSlidesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Slideshow Slides',
);

$this->menu=array(
	array('label'=>'Create SlideshowSlides', 'url'=>array('create')),
	array('label'=>'Manage SlideshowSlides', 'url'=>array('admin')),
);
?>

<h1>Slideshow Slides</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
