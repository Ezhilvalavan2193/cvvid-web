<?php
/* @var $this SlideshowSlidesController */
/* @var $model SlideshowSlides */

$this->breadcrumbs=array(
	'Slideshow Slides'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List SlideshowSlides', 'url'=>array('index')),
	array('label'=>'Create SlideshowSlides', 'url'=>array('create')),
	array('label'=>'Update SlideshowSlides', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete SlideshowSlides', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SlideshowSlides', 'url'=>array('admin')),
);
?>

<h1>View SlideshowSlides #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'slideshow_id',
		'user_id',
		'title',
		'body',
		'media_id',
		'created_at',
		'updated_at',
		'deleted_at',
		'background_x',
		'background_y',
	),
)); ?>
