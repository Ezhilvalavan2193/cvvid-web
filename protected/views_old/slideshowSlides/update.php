<?php
/* @var $this SlideshowSlidesController */
/* @var $model SlideshowSlides */

$this->breadcrumbs=array(
	'Slideshow Slides'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SlideshowSlides', 'url'=>array('index')),
	array('label'=>'Create SlideshowSlides', 'url'=>array('create')),
	array('label'=>'View SlideshowSlides', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SlideshowSlides', 'url'=>array('admin')),
);
?>

<h1>Update SlideshowSlides <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>