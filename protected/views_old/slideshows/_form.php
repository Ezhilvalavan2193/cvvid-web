<?php
/* @var $this SlideshowsController */
/* @var $model Slideshows */
/* @var $form CActiveForm */
?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'slideshows-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
        ));
?>
<div class="form-group">
    <?php echo $form->textField($model, 'title', array('class' => 'form-control','placeholder'=>"Title")); ?>
    <?php echo $form->error($model, 'title'); ?>
</div>
<div class="form-group">
    <?php echo $form->textField($model, 'slug', array('class' => 'form-control','placeholder'=>"Alias")); ?>
    <?php echo $form->error($model, 'slug'); ?>
</div>
<?php echo CHtml::submitButton('Save', array('class' => 'btn btn-primary')); ?>

<?php $this->endWidget(); ?>

