<?php
/* @var $this VideosController */
/* @var $model Videos */
/* @var $form CActiveForm */
?>
<script>
$(document).ready(function(){

    $('#mark_video').click(function()
    {
    	$('#videodialog').dialog('open');	
    });
    
    $('#cancel_msg').click(function()
    {
    	$('#videodialog').dialog('close');
    });

    $('.photo-section').on('click','#photo-btn',function(e){
		 e.preventDefault();
		 $("#photo").trigger('click');
	});

	$('.photo-section').on('change','#photo',function(evt){

          var files = evt.target.files;
          for(var i =0,f; f= files[i];i++){
              
	        var reader = new FileReader();
	        reader.onload = (function (thefile){
               return function(e){
               	 $('#profile-pic').attr('src',e.target.result);
               };
	        })(f);
	        reader.readAsDataURL(f);
          }

          $('#remove-photo-btn').show();
  });

	$('.photo-section').on('click','#remove-photo-btn',function(evt){
		$('#profile-pic').attr('src','');			
		$(this).hide();	
	});
});
</script>
<h1>Create Slideshow</h1>
<div class="Admin__content__inner">
    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'slideshow-edit-form',
    'action'=>Yii::app()->createUrl('slideshows/update',array('id'=>$model->id)),
	'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data')
)); ?>
    <div class="row">
        <div class="col-sm-6">

            <div class="form-group">
		<?php echo $form->textField($model,'title',array('class'=>'form-control','required'=>'required')); ?>
		<?php echo $form->error($model,'title'); ?>
            </div>

            <div class="form-group">
		<?php echo $form->textField($model,'slug',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'slug'); ?>
            </div>

            <div class="form-group">
                <label class="control-label">Image Horizontal</label>
		<?php echo $form->textField($model,'background_x',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'background_x'); ?>
                <span class="help-block">The horizontal position of the image, can be (left, center, right or percentage value)</span>
            </div>

            <div class="form-group">
                <label class="control-label">Image Vertical</label>
		<?php echo $form->textField($model,'background_y',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'background_y'); ?>
                <span class="help-block">The vertical position of the image, can be (top, center, bottom or percentage value)</span>
            </div>

        </div>
        <div class="col-sm-6">

            <div class="form-group">
                <div class="MediaField" data-thumbnail_src="" data-value="" data-type="photos" data-field="background_id" data-multiple="false">
                    <?php 	
									   
                        if($model->background_id > 0)
                        {
                            $mediamodel = Media::model()->findByPk($model->background_id);
                            $src = ((!empty($mediamodel->file_name) && $mediamodel->file_name != null) ? Yii::app()->baseUrl."/images/media/".$mediamodel->id."/conversions/thumb.jpg" : Yii::app()->baseUrl."/images/profile.png");
                            $display = 1;
                        } 
                       else
                       {
                           $src = "";
                           $display = 0;
                       }

                     ?>
                    <img src="<?php echo $src ?>" alt="" id='profile-pic' class="img-responsive" style="display: <?php echo ($display == 1 ? 'block' : 'none') ?>">
                    <button type="button" class="add-media btn btn-primary btn-xs">Add Logo</button>
                    <button type="button" class="remove-media btn btn-primary btn-xs" id="remove-photo-btn" style="display: <?php echo ($display == 1 ? 'block' : 'none') ?>">Remove Logo</button>
                    <input type="hidden" name="background_id" id='background_id' value="">
                </div>
            </div>

        </div>
    </div>
    <div class="AdminSlideshow">
        <div class="pull-right">
            <?php echo CHtml::button('Add Slide',array("class"=>"btn btn-primary btn-xs","onclick"=>"window.location='".Yii::app()->createUrl('slideshows/createSlide',array('id'=>$model->id))."'"));?>
        </div>
        
        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'slides-grid',
              'itemsCssClass' => 'table AdminSlideshow__slides',
            'summaryText' => '',
            //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
            'enableSorting' => true,
            'ajaxUpdate' => true,
    'pager' => array(
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'header'=>'',
        'firstPageLabel' => '<<',
        'lastPageLabel' => '>>',
        'nextPageLabel' => '>>',
        'prevPageLabel' => '<<'),
            'ajaxUpdate' => false,
            'dataProvider' => SlideshowSlides::model()->searchByID($model->id),
            'columns' => array(
// 	    array(
// 	        'header'=>'Slide',
// 	        'value'=>'',	        
// 	    ),
                array(
                    'header' => 'Title',
                    'value' => '$data->title'
                ),
                array(
                    'header' => 'Action',
                    'value' => 'CHtml::link("<i class=\"fa fa-pencil\"></i>Edit",Yii::app()->createUrl("slideshows/editSlide", array("id"=>$data->id)),array())',
                    'type' => 'raw'
                )
            ),
        ));
        ?>
    </div>
    <?php echo CHtml::submitButton('Save',array('class'=>'btn btn-primary')); ?>
    
    
   
    <?php $this->endWidget(); ?>
    
</div>

