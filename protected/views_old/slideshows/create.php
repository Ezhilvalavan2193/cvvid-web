<?php
/* @var $this SlideshowsController */
/* @var $model Slideshows */

$this->breadcrumbs=array(
	'Slideshows'=>array('index'),
	'Create',
);

?>

<h1>Create Slideshow</h1>
<div class="Admin__content__inner">
<?php $this->renderPartial('_form', array('model'=>$model)); ?>
</div>