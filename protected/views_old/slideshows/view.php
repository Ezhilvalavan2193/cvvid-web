<?php
/* @var $this SlideshowsController */
/* @var $model Slideshows */

$this->breadcrumbs=array(
	'Slideshows'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Slideshows', 'url'=>array('index')),
	array('label'=>'Create Slideshows', 'url'=>array('create')),
	array('label'=>'Update Slideshows', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Slideshows', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Slideshows', 'url'=>array('admin')),
);
?>

<h1>View Slideshows #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'title',
		'slug',
		'type',
		'body',
		'status',
		'background_id',
		'created_at',
		'updated_at',
		'deleted_at',
		'background_x',
		'background_y',
	),
)); ?>
