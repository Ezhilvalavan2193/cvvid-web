<?php
/* @var $this SlideshowsController */
/* @var $model Slideshows */

$this->breadcrumbs=array(
	'Slideshows'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Slideshows', 'url'=>array('index')),
	array('label'=>'Create Slideshows', 'url'=>array('create')),
	array('label'=>'View Slideshows', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Slideshows', 'url'=>array('admin')),
);
?>

<h1>Update Slideshows <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>