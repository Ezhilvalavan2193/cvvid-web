<?php
/* @var $this SlideshowsController */
/* @var $model Slideshows */

$this->breadcrumbs=array(
	'Slideshows'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#slideshows-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<script>
    $(document).ready(function () {

        $('#delete-slideshow').click(function (e) {


            if ($("#slideshows-grid").find("input:checked").length > 0)
            {

                if (confirm('Are you sure you want to delete these items?')) {

                    var ids = $.fn.yiiGridView.getChecked("slideshows-grid", "selectedIds");

                    $.ajax({
                        url: '<?php echo $this->createUrl('slideshows/deleteSlideshows') ?>',
                        type: 'POST',
                        data: {ids: ids},
                        success: function (data) {
                            $.fn.yiiGridView.update('slideshows-grid');
                        },
                        error: function (data) {
                            alert('err');
                        }
                    });
                }

            } else
            {
                alert('Please select at least one item');
            }
        });

    });

</script>
<h1>Slideshows</h1>
<div class="Admin__content__inner">
<?php if (Yii::app()->user->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>
    <div class="AdminFilters">
        <div class="form-inline">
           <?php $this->renderPartial('_search',array(
                    'model'=>$model,
            )); ?>
            <div class="pull-right">
                <?php  
                    echo CHtml::button('Create',array("class"=>"btn btn-primary","style"=>"","onclick"=>"window.location='".Yii::app()->createUrl('slideshows/create')."'"));
                     echo CHtml::button('Delete',array("class"=>"btn btn-primary","style"=>"",'id'=>'delete-slideshow'));
                  ?>
            </div>
        </div>
    </div>

   <?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'slideshows-grid',
         'itemsCssClass' => 'table',
    'summaryText'=>'',
   // 'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',
    'enableSorting'=>true,
    'ajaxUpdate'=>true,
    
    'pager' => array(
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'header'=>'',
        'firstPageLabel' => '<<',
        'lastPageLabel' => '>>',
        'nextPageLabel' => '>>',
        'prevPageLabel' => '<<'),
    'ajaxUpdate'=>false,
	'dataProvider'=>$model->search(),
	'columns'=>array(
             array(
                // 'name' => 'check',
                'id' => 'selectedIds',
                'value' => '$data->id',
                'class' => 'CCheckBoxColumn',
                'selectableRows' => 2,
            // 'checked'=>'Yii::app()->user->getState($data->id)',
            // 'checkBoxHtmlOptions' => array('name' => 'idList[]'),
            ),
	    array(
	        'header'=>'Name',
	        'value'=>'CHtml::link("<i class=\"fa fa-pencil\"></i> ".$data->title,Yii::app()->createUrl("slideshows/edit", array("id"=>$data->id)),array())',
	        'type'=>'raw'
	    ),
	    array(
	        'header'=>'Num Slides',
	        'value'=>'$data->getNumSlides()'
	    ),		
	    'created_at'
	),
)); ?>

</div>

