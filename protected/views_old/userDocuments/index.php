<?php
/* @var $this UserDocumentsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'User Documents',
);

$this->menu=array(
	array('label'=>'Create UserDocuments', 'url'=>array('create')),
	array('label'=>'Manage UserDocuments', 'url'=>array('admin')),
);
?>

<h1>User Documents</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
