<?php
/* @var $this UserDocumentsController */
/* @var $model UserDocuments */

$this->breadcrumbs=array(
	'User Documents'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List UserDocuments', 'url'=>array('index')),
	array('label'=>'Create UserDocuments', 'url'=>array('create')),
	array('label'=>'View UserDocuments', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage UserDocuments', 'url'=>array('admin')),
);
?>

<h1>Update UserDocuments <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>