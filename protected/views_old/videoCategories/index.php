<?php
/* @var $this VideoCategoriesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Video Categories',
);

$this->menu=array(
	array('label'=>'Create VideoCategories', 'url'=>array('create')),
	array('label'=>'Manage VideoCategories', 'url'=>array('admin')),
);
?>

<h1>Video Categories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
