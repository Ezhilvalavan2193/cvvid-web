<?php
/* @var $this VideoCategoriesController */
/* @var $model VideoCategories */

$this->breadcrumbs=array(
	'Video Categories'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List VideoCategories', 'url'=>array('index')),
	array('label'=>'Create VideoCategories', 'url'=>array('create')),
	array('label'=>'View VideoCategories', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage VideoCategories', 'url'=>array('admin')),
);
?>

<h1>Update VideoCategories <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>