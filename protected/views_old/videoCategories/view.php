<?php
/* @var $this VideoCategoriesController */
/* @var $model VideoCategories */

$this->breadcrumbs=array(
	'Video Categories'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List VideoCategories', 'url'=>array('index')),
	array('label'=>'Create VideoCategories', 'url'=>array('create')),
	array('label'=>'Update VideoCategories', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete VideoCategories', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage VideoCategories', 'url'=>array('admin')),
);
?>

<h1>View VideoCategories #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'slug',
		'created_at',
		'updated_at',
	),
)); ?>
