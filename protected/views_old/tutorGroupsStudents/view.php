<?php
/* @var $this TutorGroupsStudentsController */
/* @var $model TutorGroupsStudents */

$this->breadcrumbs=array(
	'Tutor Groups Students'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TutorGroupsStudents', 'url'=>array('index')),
	array('label'=>'Create TutorGroupsStudents', 'url'=>array('create')),
	array('label'=>'Update TutorGroupsStudents', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TutorGroupsStudents', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TutorGroupsStudents', 'url'=>array('admin')),
);
?>

<h1>View TutorGroupsStudents #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'institution_id',
		'tutor_group_id',
		'student_id',
		'graduation_date',
		'created_at',
		'updated_at',
	),
)); ?>
