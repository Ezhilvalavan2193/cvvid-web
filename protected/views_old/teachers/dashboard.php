<script>
$(document).ready(function(){
	

	$('.DeleteBtn').click(function(e){
         if($("#inst-studs-grid").find("input:checked").length > 0)
         {

        	 if (confirm('Are you sure you want to delete these items?')) {

         		 var ids = $.fn.yiiGridView.getChecked("inst-studs-grid", "selectedIds");
             	
       			  $.ajax({
       		            url: '<?php echo $this->createUrl('institutions/deleteStudents') ?>',
       		            type: 'POST',
       		            data: {ids : ids}, 	    
       		            success: function(data) {
                               // $.fn.yiiGridView.update('inst-studs-grid');
                               location.reload();
       			            //alert(data);
       		            },
                            error: function(data) {		
                                alert('err');
                            }
       		         });
        	 } 
        	
         }
         else
         {
            alert('Please select at least one item');
         }
	});
	
});

</script>
<div class="page-title text-left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $model->name; ?> - <?php echo $usermodel->forenames . " " . $usermodel->surname; ?></h1>
            </div>
        </div>
    </div>
</div>
<div id="page-content">
    <div class="container">
      <?php 
      $tgtmodel = TutorGroupsTeachers::model()->findByAttributes(array('teacher_id'=>Yii::app()->user->getState('userid'),'institution_id'=>$model->id));
      
      if($tgtmodel == null){ 
      
          echo "<p>You currently are not managing a tutor group.</p>";
          echo '<a href="'.Yii::app()->createUrl('institutions/teacherClass',array('id'=>$model->id)).'" class="btn btn-primary">Create Tutor Group</a>';
      }
      else
      {
          
          if (Yii::app()->user->hasFlash('success')): 
            ?>
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?php echo Yii::app()->user->getFlash('success'); ?>
            </div>
        <?php endif; ?>
        <?php
        $cri = new CDbCriteria();
        $cri->alias = "tg";
        $cri->select = "sum(tg.account_limit) as account_limit,tg.id";       
        $cri->join = "inner join cv16_tutor_groups_teachers tgt on tgt.tutor_group_id = tg.id";
         $cri->condition = "tg.institution_id =".$model->id." AND tgt.teacher_id=".$usermodel->id;
        $tgmodel = TutorGroups::model()->find($cri);
        $student_cnt = TutorGroupsStudents::model()->countByAttributes(array('institution_id'=>$model->id,'tutor_group_id'=>$tgmodel['id']));
        $total = $model->num_pupils;
        
        $target = Yii::app()->controller->getRoute()."/".Yii::app()->user->getState('userid');
        Yii::app()->user->setState('instid',$target);
       
        if ($tgtmodel == null) {
            ?>
            <p>You currently are not managing a tutor group.</p>
            <a href="<?php echo "dd"; ?>" class="btn btn-primary">Create Tutor Group</a>
             <?php } else { ?>
            <div class="form-container clearfix">
                <div class="pull-left">
                    <p>Account Limit: <?php echo $student_cnt; ?> / <?php echo $tgmodel['account_limit']; ?></p>
                </div>
                <div class="pull-right">
                 
                   	<a href="<?php echo Yii::app()->createUrl('institutions/bulkStudent') ?>" style="display: <?php echo ($student_cnt < $tgmodel['account_limit'] ? "" : "none")?>" class="btn btn-default"><i class="fa fa-plus"></i> Bulk Add Student</a>
                    <a href="<?php echo Yii::app()->createUrl('institutions/addStudent') ?>" style="display: <?php echo ($student_cnt < $tgmodel['account_limit'] ? "" : "none")?>"class="btn btn-default"><i class="fa fa-plus"></i> Add Student</a>
                    <a href="<?php echo $this->createUrl('institutions/inbox') ?>" class="btn btn-default"><i class="fa fa-envelope-o"></i> Inbox</a>
                    <button type="button" class="btn btn-danger DeleteBtn"><i class="fa fa-close"></i> Delete</button>
                </div>
            </div>
            <form method="POST" id="adminTable" action="">
                <div class="dashboard-table">
                    <?php
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'inst-studs-grid',
                        'summaryText' => '',
                        'itemsCssClass' => 'table',
                        //  'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                        'enableSorting' => true,
                        'ajaxUpdate' => true,
                        'pager' => array(
                            //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                            'header' => '',
                            'firstPageLabel' => '<<',
                            'lastPageLabel' => '>>',
                            'nextPageLabel' => '>>',
                            'prevPageLabel' => '<<'),
                        'ajaxUpdate' => false,
                        'dataProvider' => (($tgtmodel != null && $tgtmodel->tutor_group_id > 0) ? TutorGroupsStudents::model()->searchStudents($tgtmodel->tutor_group_id) : TutorGroupsStudents::model()->searchStudents(0)),
                        //'filter'=>$model,
                        'columns' => array(
                            array(
                                // 'name' => 'check',
                                'id' => 'selectedIds',
                                'value' => '$data->id',
                                'class' => 'CCheckBoxColumn',
                                'selectableRows' => 2,
                            // 'checked'=>'Yii::app()->user->getState($data->id)',
                            // 'checkBoxHtmlOptions' => array('name' => 'idList[]'),
                            ),
                            array(
                                'header' => 'Students Name',
                                'value' => 'CHtml::link($data->student->forenames." ".$data->student->surname,Yii::app()->createUrl("institutions/modifyStudent",array("id"=>$data->id,"target"=>$data->getInsid())),array("class"=>"edit-button"))',
                                'type' => 'raw'
                            ),
                            array(
                                'header' => 'View Profile',
                                'value' => 'CHtml::link("Profile",Yii::app()->createUrl("users/edit",array("id"=>$data->student_id)),array("class"=>"profile-button"))',
                                'type' => 'raw'
                            ),
                            array(
                                'header' => 'Graduation Date',
                                'value' => '$data->graduation_date'
                            ),
                            array(
                                'header' => 'Views',
                                'value' => '$data->created_at'
                            )
                        ),
                    ));
                    ?>

                </div>
            </form>
<?php } 
    } ?>
    </div>
</div>