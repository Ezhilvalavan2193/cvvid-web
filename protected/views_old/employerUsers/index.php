<?php
/* @var $this EmployerUsersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Employer Users',
);

$this->menu=array(
	array('label'=>'Create EmployerUsers', 'url'=>array('create')),
	array('label'=>'Manage EmployerUsers', 'url'=>array('admin')),
);
?>

<h1>Employer Users</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
