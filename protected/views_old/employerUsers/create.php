<?php
/* @var $this EmployerUsersController */
/* @var $model EmployerUsers */

$this->breadcrumbs=array(
	'Employer Users'=>array('index'),
	'Create',
);

$this->menu=array(
//	array('label'=>'List EmployerUsers', 'url'=>array('index')),
	array('label'=>'Manage EmployerUsers', 'url'=>array('admin')),
);
?>

<?php $this->renderPartial('_form', array('model'=>$model,'usermodel'=>$usermodel)); ?>