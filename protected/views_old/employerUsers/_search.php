<?php

/* @var $this InstitutionsController */
/* @var $model Institutions */
/* @var $form CActiveForm */
?>

<?php

$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
        ));
?>

<?php echo CHtml::textField('fullname', $fullname, array('class' => 'form-control')); ?>

<?php

// $empmodel = new Employers;
$emodel = $empmodel->findAllByAttributes(array('deleted_at' => null));
$emplist = CHtml::listData($emodel, 'id', 'name');
echo $form->dropDownList($empmodel, 'id', $emplist, array('class' => 'form-control', 'empty' => '- Select Employer -'));
?>

<?php

echo CHtml::submitButton('Search', array("class" => 'btn btn-primary'));
?>

<?php $this->endWidget(); ?>

