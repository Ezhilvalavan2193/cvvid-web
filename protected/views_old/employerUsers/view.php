<?php
/* @var $this EmployerUsersController */
/* @var $model EmployerUsers */

$this->breadcrumbs=array(
	'Employer Users'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List EmployerUsers', 'url'=>array('index')),
	array('label'=>'Create EmployerUsers', 'url'=>array('create')),
	array('label'=>'Update EmployerUsers', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete EmployerUsers', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage EmployerUsers', 'url'=>array('admin')),
);
?>

<h1>View EmployerUsers #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'employer_id',
		'user_id',
		'role',
		'created_at',
		'updated_at',
	),
)); ?>
