<?php
/* @var $this EmployersController */
/* @var $model Employers */
/* @var $form CActiveForm */
?>
<script>
//$(document).ready(function()
//{
//	$("#employerusers-form").submit(function(event){
//		//event.preventDefault();
//		if($('#password').val() != "" && $('#password').val() == $('#confirmpassword').val())
//			$("#employerusers-form").submit();
//		else	
//		{
//			$('.pwdmsg').html('Password not matched');
//			return false;
//		}
//	});
//});
</script>

 <h1>Create Employer User</h1>
 <div class="Admin__content__inner">
     <?php
         
         $form = $this->beginWidget('CActiveForm', array(
            'id' => 'employerusers-form',
            'action' => $this->createUrl('employerUsers/createEmployerUsers'),
            'enableAjaxValidation' => false,
            'htmlOptions' => array('enctype' => 'multipart/form-data')
        ));
        ?>
     <?php echo  $form->errorSummary(array($model,$usermodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
     <div class="form-container">
         <!-- Your details -->
         <h4>User details</h4>
         <div class="row">
             <div class="col-sm-3">
                 <div class="form-group">
                    <?php echo $form->textField($usermodel, 'forenames', array('class' => 'form-control','placeholder'=>"First Name")); ?>
                 </div>
             </div>
             <div class="col-sm-3">
                 <div class="form-group">
                    <?php echo $form->textField($usermodel, 'surname', array('class' => 'form-control','placeholder'=>"Surname")); ?>
                 </div>
             </div>
         </div>
     </div>

     <div class="form-container">
         <div class="row">
             <div class="col-sm-3">
                 <div class="form-group">
                     <label class="control-label">Employer</label>
                            <select name="EmployerUsers[employer_id]" class="form-control">
                                <option selected="selected" disabled>Search Employer</option>
                                <?php
                                $employermodel = Employers::model()->findAll();
                                foreach ($employermodel as $emp) {
                                    ?>  
                                   <option value="<?php echo $emp['id'] ?>" <?php echo (isset($_POST['EmployerUsers']['employer_id']) && $_POST['EmployerUsers']['employer_id']  == $emp['id'] ? 'selected' : '') ?>> <?php echo $emp['name'] ?> </option>
                                 <?php } ?>
                            </select>
                 </div>
             </div>
         </div>
     </div>

     <div class="form-container">
         <h4>Account Details</h4>
         <div class="row">
             <div class="col-sm-3">
                <?php echo $form->textField($usermodel, 'email', array('class' => 'form-control','placeholder'=>"Email")); ?>
             </div>
             <div class="col-sm-3">
                <?php echo $usermodel->password = ""; ?>
                <?php echo $form->passwordField($usermodel, 'password', array('class' => 'form-control','id'=>'password','placeholder'=>"Password")); ?>
             </div>
             <div class="col-sm-3">
               <input class="form-control" placeholder="Confirm Password" type="password" value="" name="Users[confirmpassword]" id="confirmpassword">
             </div>
         </div>
     </div>
     <br>
     <div class="form-actions">
          <?php echo CHtml::submitButton('Save', array("class" => "btn btn-default"));
          echo CHtml::button('Cancel',array("class"=>"btn btn-default","onclick"=>"window.location='".Yii::app()->createUrl('employerUsers/admin')."'"));
          ?>
         
     </div>  

     
      <?php $this->endWidget(); ?>
 </div>

