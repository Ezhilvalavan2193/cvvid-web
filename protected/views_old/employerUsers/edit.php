<?php
/* @var $this EmployersController */
/* @var $model Employers */
/* @var $form CActiveForm */
?>
<script>
$(document).ready(function()
{
//	$("#employerusers-edit-form").submit(function(event){
//		//event.preventDefault();
//		if($('#password').val() == $('#confirmpassword').val())
//			$("#employerusers-edit-form").submit();
//		else	
//		{
//			$('.pwdmsg').html('Password not matched');
//			return false;
//		}
//	});
});
</script>


<div class="Admin__content__inner">
     <?php
         
         $form = $this->beginWidget('CActiveForm', array(
            'id' => 'employerusers-edit-form',
            'action' => $this->createUrl('employerUsers/editUsers', array('id'=>$usermodel->id)),
            'enableAjaxValidation' => false,
            'htmlOptions' => array('enctype' => 'multipart/form-data')
        ));
        ?>
     <?php echo  $form->errorSummary(array($usermodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
     <div class="form-container">
         <!-- Your details -->
         <h4>User details</h4>
         <div class="row">
             <div class="col-sm-3">
                 <div class="form-group">
                    <?php echo $form->textField($usermodel, 'forenames', array('class' => 'form-control','placeholder'=>"First Name")); ?>
                 </div>
             </div>
             <div class="col-sm-3">
                 <div class="form-group">
                    <?php echo $form->textField($usermodel, 'surname', array('class' => 'form-control','placeholder'=>"Surname")); ?>
                 </div>
             </div>
         </div>
     </div>


     <div class="form-container">
         <h4>Account Details</h4>
         <div class="row">
             <div class="col-sm-3">
                <?php echo $form->textField($usermodel, 'email', array('class' => 'form-control','placeholder'=>"Email")); ?>
             </div>
             <div class="col-sm-3">
                <?php echo $usermodel->password = ""; ?>
                <?php echo $form->passwordField($usermodel, 'password', array('class' => 'form-control','id'=>'password','placeholder'=>"Password")); ?>
             </div>
             <div class="col-sm-3">
               <input class="form-control" placeholder="Confirm Password" type="password" value="" name="Users[confirmpassword]" id="confirmpassword">
             </div>
         </div>
     </div>
     <br>
     <div class="form-actions">
          <?php echo CHtml::submitButton('Save', array("class" => "btn btn-default"));
          echo CHtml::button('Cancel',array("class"=>"btn btn-primary","onclick"=>"window.location='".Yii::app()->createUrl('employerUsers/admin')."'"));
          ?>
         
     </div>  

     
      <?php $this->endWidget(); ?>
 </div>

