<?php
/* @var $this ConversationMembersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Conversation Members',
);

$this->menu=array(
	array('label'=>'Create ConversationMembers', 'url'=>array('create')),
	array('label'=>'Manage ConversationMembers', 'url'=>array('admin')),
);
?>

<h1>Conversation Members</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
