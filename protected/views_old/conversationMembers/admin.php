<?php
/* @var $this ConversationMembersController */
/* @var $model ConversationMembers */

$this->breadcrumbs=array(
	'Conversation Members'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List ConversationMembers', 'url'=>array('index')),
	array('label'=>'Create ConversationMembers', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#conversation-members-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Conversation Members</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'conversation-members-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'conversation_id',
		'user_id',
		'name',
		'last_read',
		'created_at',
		/*
		'updated_at',
		'deleted_at',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
