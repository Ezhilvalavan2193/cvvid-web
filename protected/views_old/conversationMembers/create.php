<?php
/* @var $this ConversationMembersController */
/* @var $model ConversationMembers */

$this->breadcrumbs=array(
	'Conversation Members'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ConversationMembers', 'url'=>array('index')),
	array('label'=>'Manage ConversationMembers', 'url'=>array('admin')),
);
?>

<h1>Create ConversationMembers</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>