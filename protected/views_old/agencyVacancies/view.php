<?php
/* @var $this AgencyVacanciesController */
/* @var $model AgencyVacancies */

$this->breadcrumbs=array(
	'Agency Vacancies'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List AgencyVacancies', 'url'=>array('index')),
	array('label'=>'Create AgencyVacancies', 'url'=>array('create')),
	array('label'=>'Update AgencyVacancies', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AgencyVacancies', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AgencyVacancies', 'url'=>array('admin')),
);
?>

<h1>View AgencyVacancies #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'agency_id',
		'job_id',
		'created_at',
		'updated_at',
	),
)); ?>
