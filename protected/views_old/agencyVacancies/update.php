<?php
/* @var $this AgencyVacanciesController */
/* @var $model AgencyVacancies */

$this->breadcrumbs=array(
	'Agency Vacancies'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AgencyVacancies', 'url'=>array('index')),
	array('label'=>'Create AgencyVacancies', 'url'=>array('create')),
	array('label'=>'View AgencyVacancies', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AgencyVacancies', 'url'=>array('admin')),
);
?>

<h1>Update AgencyVacancies <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>