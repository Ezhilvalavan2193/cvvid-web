<?php
/* @var $this AgencyVacanciesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Agency Vacancies',
);

$this->menu=array(
	array('label'=>'Create AgencyVacancies', 'url'=>array('create')),
	array('label'=>'Manage AgencyVacancies', 'url'=>array('admin')),
);
?>

<h1>Agency Vacancies</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
