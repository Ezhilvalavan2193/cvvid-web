<?php
/* @var $this IndustriesController */
/* @var $model Industries */
/* @var $form CActiveForm */
?>
<h1>Edit Industry</h1>
<div class="Admin__content__inner">
      <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'skill-categories-form',
        'action' => $this->createUrl('skillCategories/updateSkillCategories', array('id'=>$model->id)),
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data')
    ));
    ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
               <?php echo $form->labelEx($model, 'name'); ?>
                <?php echo $form->textField($model, 'name', array('class' => 'form-control', 'placeholder' => 'Name')); ?>
                <?php echo $form->error($model, 'name'); ?> 
            </div>
            <?php echo CHtml::submitButton('Save', array("class" => "btn btn-primary")); ?>
            <?php echo CHtml::button('Cancel',array("class"=>'btn btn-primary',"onClick"=>"window.location='".Yii::app()->createUrl('skillCategories/admin',array())."'")); ?>
        </div>
    </div>
    
     <?php $this->endWidget(); ?>
</div>

