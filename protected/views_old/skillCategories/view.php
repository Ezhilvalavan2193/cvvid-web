<?php
/* @var $this SkillCategoriesController */
/* @var $model SkillCategories */

$this->breadcrumbs=array(
	'Skill Categories'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List SkillCategories', 'url'=>array('index')),
	array('label'=>'Create SkillCategories', 'url'=>array('create')),
	array('label'=>'Update SkillCategories', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete SkillCategories', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage SkillCategories', 'url'=>array('admin')),
);
?>

<h1>View SkillCategories #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'created_at',
		'updated_at',
		'ordering',
	),
)); ?>
