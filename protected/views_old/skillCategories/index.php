<?php
/* @var $this SkillCategoriesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Skill Categories',
);

$this->menu=array(
	array('label'=>'Create SkillCategories', 'url'=>array('create')),
	array('label'=>'Manage SkillCategories', 'url'=>array('admin')),
);
?>

<h1>Skill Categories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
