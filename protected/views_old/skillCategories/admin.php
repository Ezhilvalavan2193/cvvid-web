<?php
/* @var $this SkillCategoriesController */
/* @var $model SkillCategories */

$this->breadcrumbs = array(
    'Skill Categories' => array('index'),
    'Manage',
);
//
//$this->menu=array(
//	array('label'=>'List SkillCategories', 'url'=>array('index')),
//	array('label'=>'Create SkillCategories', 'url'=>array('create')),
//);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#skill-categories-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<script>
    $(document).ready(function () {


        $('#delete-skillcategories').click(function (e) {


            if ($("#skill-categories-grid").find("input:checked").length > 0)
            {

                if (confirm('Do you want to delete these skillCategories ? respected skills will also be deleted.')) {

                    var ids = $.fn.yiiGridView.getChecked("skill-categories-grid", "selectedIds");

                    $.ajax({
                        url: '<?php echo $this->createUrl('skillCategories/deleteSkillCategories') ?>',
                        type: 'POST',
                        data: {ids: ids},
                        success: function (data) {
                            location.reload();
                        },
                        error: function (data) {
                            alert('err');
                        }
                    });
                }

            } else
            {
                alert('Please select at least one item');
            }
        });

    });

</script>
<h1>Industries</h1>
<div class='Admin__content__inner'>
    <div class='AdminFilters'>
        <?php
        $this->renderPartial('_search', array(
            'model' => $model,
        ));
        ?>
        <div class="pull-right">
             <?php
                echo CHtml::button('Create', array("class" => "btn btn-primary", "onclick" => "window.location='" . Yii::app()->createUrl('skillCategories/create') . "'"));
                echo CHtml::button('Delete', array("class" => "btn btn-primary", 'id' => 'delete-skillcategories'));
                ?>
        </div>
    </div>
    
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'itemsCssClass'  => 'table',
    'id' => 'skill-categories-grid',
    'summaryText' => '',
    //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
    'pager' => array(
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'header'=>'',
        'firstPageLabel' => '<<',
        'lastPageLabel' => '>>',
        'nextPageLabel' => '>>',
        'prevPageLabel' => '<<'),
    'ajaxUpdate' => false,
    'dataProvider' => $model->search(),
    'rowHtmlOptionsExpression' => 'array("id"=>"category-".$data->id)',
    'columns' => array(
        
        //'id',
        array(
            // 'name' => 'check',
            'id' => 'selectedIds',
            'value' => '$data->id',
            'class' => 'CCheckBoxColumn',
            'selectableRows' => 2,
        // 'checked'=>'Yii::app()->user->getState($data->id)',
        // 'checkBoxHtmlOptions' => array('name' => 'idList[]'),
        ),
        array(
            'header' => 'Name',
            'value' => 'CHtml::link("<i class=\"fa fa-pencil\"></i> ".$data->name,Yii::app()->createUrl("skillCategories/skillCategoriesEdit",array("id"=>$data->id)),array("class"=>"skill-categories-edit-button","id"=>"skill-categories-edit"))',
            'type' => 'raw'
        ),
    ),
));
?>

</div>

<script type="text/javascript">
   // $('tbody').sortable();
    $('table').attr('id','AdminSortable'); 
    
      $(function () {

        var fixHelper = function fixHelper(e, ui) {
            ui.children().each(function () {
                $(this).width($(this).width());
            });
            return ui;
        };
        //alert(fixHelper);
        $('#AdminSortable tbody').sortable({
            helper: fixHelper,
            stop: function stop(event, ui) {
                var newOrders = $('#AdminSortable tbody').sortable('toArray');
//                alert(newOrders);
                $.ajax({
                    type: 'POST',
                    url: '<?php echo $this->createUrl('skillCategories/reorder') ?>',
                    data: {
                        orders: newOrders
                    }
                });
            }
        });
        });
</script>