<?php
/* @var $this ProfileStatementController */
/* @var $model ProfileStatement */

$this->breadcrumbs=array(
	'Profile Statements'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ProfileStatement', 'url'=>array('index')),
	array('label'=>'Manage ProfileStatement', 'url'=>array('admin')),
);
?>

<h1>Create ProfileStatement</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>