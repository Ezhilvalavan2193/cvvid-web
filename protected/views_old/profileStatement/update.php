<?php
/* @var $this ProfileStatementController */
/* @var $model ProfileStatement */

$this->breadcrumbs=array(
	'Profile Statements'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProfileStatement', 'url'=>array('index')),
	array('label'=>'Create ProfileStatement', 'url'=>array('create')),
	array('label'=>'View ProfileStatement', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ProfileStatement', 'url'=>array('admin')),
);
?>

<h1>Update ProfileStatement <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>