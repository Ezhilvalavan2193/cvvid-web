<?php
/* @var $this TempSkillsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Temp Skills',
);

$this->menu=array(
	array('label'=>'Create TempSkills', 'url'=>array('create')),
	array('label'=>'Manage TempSkills', 'url'=>array('admin')),
);
?>

<h1>Temp Skills</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
