<?php
/* @var $this ProfileViewsController */
/* @var $model ProfileViews */

$this->breadcrumbs=array(
	'Profile Views'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProfileViews', 'url'=>array('index')),
	array('label'=>'Create ProfileViews', 'url'=>array('create')),
	array('label'=>'View ProfileViews', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ProfileViews', 'url'=>array('admin')),
);
?>

<h1>Update ProfileViews <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>