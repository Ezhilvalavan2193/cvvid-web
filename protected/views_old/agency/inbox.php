<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */

$this->breadcrumbs = array(
    'Institutions' => array('index'),
    'Manage',
);

// $this->menu=array(
// 	array('label'=>'List Institutions', 'url'=>array('index')),
// 	array('label'=>'Create Institutions', 'url'=>array('create')),
// );

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#inst-admin-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<script>
    $(document).ready(function () {
        $('.ConversationListItem').click(function ()
        {
            var conv_id = $(this).find('#conversation_id').val();
            var userid = $(this).find('#user_id').val();
            $('.ConversationListItem').removeClass('active');
            $(this).addClass('active');
            $.ajax({
                url: '<?php echo $this->createUrl('agency/getInboxMessages') ?>',
                type: 'POST',
                data: {id: conv_id, msg_to: userid},
                success: function (data) {
                    // alert(data);
                    $('.ConversationInner').html(data);
                },
                error: function (data) {
                    alert('err');
                }
            });
        });

        $('#Conversation').on('click', '#send_msg', function (e)
        {
            var userid = $('#ConversationList').find('.active').find('#user_id').val();
            var conv_id = $('#ConversationList').find('.active').find('#conversation_id').val();
            var msg = $('#message').val();
            $('.ConversationLoader').show();
            $.ajax({
                url: '<?php echo $this->createUrl('employers/saveMessage') ?>',
                type: 'POST',
                data: {id: conv_id, msg_to: userid, message: msg},
                success: function (data) {
                    $('.ConversationInner').html(data);
                    $('.ConversationLoader').hide();
                },
                error: function (data) {
                    alert('err');
                }
            });
        });

        $('body').on('click', '.create-message', function () {
            //alert(getValue);return false;
            $('#NewConversationModal').addClass('in');
            $('body').addClass('modal-open');
            $('#NewConversationModal').show();
        });

        $('#NewConversationModal').on('click', '.SendMessageBtn', function () {

            var errors = false;
            var errors_mgs = '';
            var $subject = $('input[id="subject_msg"]');
            var subject_val = $subject.val();
            var $target_id = $('select[id="target_id"]');
            var target_val = $target_id.val();
            var $message = $('textarea[id="message_msg"]')
            var message_val = $message.val();
            // Validate Subject
            if (target_val == '') {
                errors_mgs += '<div>The target field is required.</div>';
                errors = true;
            } else {
                errors = false;
            }

            // Validate Subject
            if (subject_val == '') {
                errors_mgs += '<div>The subject is required</div>';
                errors = true;
            } else {
                errors = false;
            }
//alert(message_val);
            // Validate Message
            if (message_val == '') {
                errors_mgs += '<div>A message is required</div>';
                errors = true;
            } else {
                errors = false;
            }
            if (!errors) {
                $('.modal-loader').show();
                $.ajax({
                    url: '<?php echo $this->createUrl('agency/sendMessage') ?>',
                    type: 'POST',
                    data: {message: message_val, subject: subject_val, user_id: target_val},
                    success: function (data) {
                        $('#msg_scc_msg').show();
                        $('.modal-loader').hide();
                        setTimeout(function () {
                            location.reload();
                        }, 3000);
                    },
                });
            } else {
                $('#msg_err_msg').html(errors_mgs);
                $('#msg_err_msg').show();
            }



            return false;
        });

    });

</script>
<div class="page-title text-left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Messages</h1>
            </div>
        </div>
    </div>
</div>
<div id="page-content">

    <div class="container">
        <div id="Mailbox"> 
            <div class = "row">


                <?php
                $cri = new CDbCriteria();
                $cri->condition = "user_id =" . $model->id;
                $cri->group = "conversation_id";
                $cri->order = "created_at desc";
                $convmembers = ConversationMembers::model()->findAll($cri);
                ?>
                <div class="col-sm-4 col-md-4 col-lg-4">
                  <?php
                  $trail = "";
                   ?>
                    <button class="btn btn-primary create-message">Create Message</button>
                  
                    <div id="ConversationList">
                        <?php
                        $conversationid = 0;
                        $msg_to = 0;
                        foreach ($convmembers as $mem) {
                            $crit = new CDbCriteria();
                            $crit->condition = "conversation_id = " . $mem['conversation_id'] . " and user_id not in (" . $model->id . ")";
                            $convmem = ConversationMembers::model()->find($crit);
                            $created = date_format(new DateTime($convmem['created_at']), "Y-m-d");

                            $conv = Conversations::model()->findByPk($convmem['conversation_id']);

                            $msgcri = new CDbCriteria();
                            $msgcri->condition = "conversation_id = " . $mem['conversation_id'] . " and created_at ='" . $mem['created_at'] . "'";
                            $convmsg = ConversationMessages::model()->find($msgcri);

                            if ($conversationid == 0) {
                                $conversationid = $convmsg['conversation_id'];
                                $msg_to = $convmem['user_id'];
                                $class = "active";
                            } else {
                                $class = "";
                            }

                            echo '<div class="ConversationListItem ' . $class . '">';
                            echo CHtml::hiddenField('conversation_id', $convmsg['conversation_id'], array('id' => 'conversation_id'));
                            echo CHtml::hiddenField('user_id', $convmem['user_id'], array('id' => 'user_id'));
                            echo '<div class="ConversationItem__meta clearfix">';

                            echo '<div class="ConversationItem__user pull-left">' . $convmem['name'] . '</div>';

                            echo '<div class="ConversationItem__date pull-right">' . $created . '</div>';

                            echo '</div>';

                            echo '<div class="ConversationItem__subject"><label>Subject:</label> ' . $conv->subject . '</div>';

                            echo '<div class="ConversationItem__message">' . $convmsg['message'] . '</div>';

                            echo '</div>';
                        }
                        ?>
                    </div>
                </div>
                <div class="col-dm-8 col-md-8 col-lg-8">


                    <div id="Conversation">

                     <?php  if ($conversationid > 0) { ?>
                        <div class="ConversationInner">
                            <?php
                           
                                $convmodel = Conversations::model()->findByPk($conversationid);
                                $this->renderPartial('inbox_messages', array('convmodel' => $convmodel, 'to_user' => $msg_to));
                           
                            ?>
                        </div>
                      <?php } else { ?>
                      <div class="alert alert-warning">You currently don't have any messages in your inbox.</div>      
                   <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!--conver messgae-->
    <div id="NewConversationModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closepopup()"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Send a Message</h4>
                </div>
                <div class="modal-body">
                    <form class="MessageForm">
                        <div class="alert alert-success" id='msg_scc_msg' style="display:none;">
                            Your message has been successfully sent
                        </div>
                        <div class="alert alert-danger" id='msg_err_msg' style="display: none">

                        </div>
                        <div class="form-group">
                            <label>Contact</label>
                               <select name="target_id" id="target_id" class="form-control">
                                <option value="">- Select Contact -</option>
                                <?php
                                $cri = new CDbCriteria();
                                $cri->alias = "ac";
                                $cri->join = "inner join cv16_users u on u.id = ac.user_id";
                                $cri->condition = "ac.agency_id =".$agencymodel->id;
                                $agencymodel = AgencyCandidates::model()->findAll($cri);
                                $cri = new CDbCriteria();
                                foreach ($agencymodel as $usr) {
                                    $userarr = Users::model()->findByPk($usr['user_id']);
                                    ?>                  
                                    <option value="<?php echo $userarr['id'] ?>"> <?php echo $userarr['forenames'] . " " . $userarr['surname']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="subject">Subject</label>
                            <input name="subject" type="text" id="subject_msg" class="form-control" value="">
                        </div>
                        <div class="form-group">
                            <label for="message">Message</label>
                            <textarea name="message" id="message_msg" class="form-control" rows="10"></textarea>
                        </div>
                    </form>
                    <div class="modal-loader">
                        <img src="<?php echo Yii::app()->baseUrl?>/images/loading.gif">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default-btn" data-dismiss="modal" onclick="closepopup()">Close</button>
                    <button type="button" class="SendMessageBtn btn main-btn">Send Message</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>