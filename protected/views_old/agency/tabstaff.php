<?php
/* @var $this AgencyController */
/* @var $model AgencyStaff */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#inst-admin-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<script>
$(document).ready(function(){
	
	$('#export-candidate').on('click',function() {
	    window.location = '<?php echo $this->createUrl('users/exportCSV') ?>'
	});

});

</script>
 <div class="Admin__content__inner">         
	<div class="pull-right">       
		<?php 
		  $asmodelcnt = AgencyStaff::model()->countByAttributes(array('agency_id'=>$agencymodel->id));
		  echo CHtml::button("Add Staff", array("class" => "btn btn-primary","disabled"=>($asmodelcnt >= $agencymodel->num_staff ? true : false), "onclick" => "window.location='" . Yii::app()->createUrl('agency/createstaff',array("id"=>$agencymodel->id)) . "'")) ?>
    </div>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'agency-staff-grid',
     'itemsCssClass' => 'table',
    'summaryText' => '',
   // 'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
    'enableSorting' => true,
    'ajaxUpdate' => true,
    'pager' => array(
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'header'=>'',
        'firstPageLabel' => '<<',
        'lastPageLabel' => '>>',
        'nextPageLabel' => '>>',
        'prevPageLabel' => '<<'),
                        'ajaxUpdate' => false,
                        'dataProvider' => $model->search($agencymodel->id),
                        //'filter'=>$model,
                        'columns' => array(
                            array(
                                'header' => 'Name',
                                'value' => 'CHtml::link("<i class=\"fa fa-pencil\"></i> ".$data->candidate->forenames." ".$data->candidate->surname,Yii::app()->createUrl("agency/editStaff",array("id"=>$data->id)),array("class"=>"staff-edit-button","id"=>"staff-edit"))',
                                'type' => 'raw'
                            ),                            
                            array(
                                'header' => 'Email',
                                'value'=>'$data->candidate->email'
                            ),
                            array(
                                'header' => 'Candidates',                                
                                'value'=>'CHtml::link("<i class=\"fa fa-users\"></i> ".$data->getStudentcount(),Yii::app()->createUrl("agency/viewcandidates",array("id"=>$data->id)),array("class"=>"staff-edit-button","id"=>"staff-edit"))',
                                'type' => 'raw'
                            )
                        ),
                    ));
                    ?>
                   
  </div>
       

