<?php
/* @var $this AgencyController */
/* @var $model AgencyStaff */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#inst-admin-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<script>
$(document).ready(function(){

	  $('body').on('click', '#delete-staff', function () {

          if ($("#agency-staff-grid").find("input:checked").length > 0)
          {
              if (confirm('Are you sure you want to delete these items?')) {

                  var ids = $.fn.yiiGridView.getChecked("agency-staff-grid", "selectedIds");
                  $.ajax({
                      url: '<?php echo $this->createUrl('agency/deleteStaff') ?>',
                      type: 'POST',
                      data: {ids: ids},
                      success: function (data) {
                          location.reload();
                      }
                  });
              }
          }else{
             alert('Please select at least one item');
          }

      });
      
	$('#export-candidate').on('click',function() {
	    window.location = '<?php echo $this->createUrl('users/exportCSV') ?>'
	});

	$('.add_staff').click(function(){

    		$.ajax({
                url: '<?php echo $this->createUrl('agency/checkPackage') ?>',
                type: 'POST',
                data: {user : "user"},
                success: function (data) {
                    if(data > 0)
                        window.location = '<?php echo Yii::app()->createUrl('agency/createstaff',array("id"=>$agencymodel->id))?>';
                    else
                    	window.location = '<?php echo Yii::app()->createUrl('agency/userpackage',array('type'=>"staff"))?>';        
                }
            });
            
	});
});

</script>

<div class="page-title text-left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Staff</h1>
            </div>
        </div>
    </div>
</div>
<div id="page-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-md-2 dashboard-header-section">
                <ul class="box dashboard-links list-unstyled">
                   <!-- <li><a href="<?php //echo $this->createUrl('agency/employers',array('id'=>$agencymodel->id))?>">Employers</a></li> --> 
                    <li><a href="<?php echo $this->createUrl('agency/staff',array('id'=>$agencymodel->id))?>">Staff</a></li>
                    <li><a href="<?php echo $this->createUrl('agency/candidates',array('id'=>$agencymodel->id))?>">Candidates</a></li>
                    
                    <li><a href="<?php echo $this->createUrl('agency/vacancies',array('id'=>$agencymodel->id))?>">Vacancies</a></li>
                </ul>
            </div>
            <div class="col-sm-9 col-md-10">
           		 <div class="pull-right">
                    <button type="button" class="main-btn" id="delete-staff"> Delete</button>
                </div>
                <div class="dashboard-table">
                    <?php
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'agency-staff-grid',
                         'itemsCssClass' => 'table',
                        'summaryText' => '',
                       // 'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                        'enableSorting' => true,
                        'ajaxUpdate' => true,
    'pager' => array(
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'header'=>'',
        'firstPageLabel' => '<<',
        'lastPageLabel' => '>>',
        'nextPageLabel' => '>>',
        'prevPageLabel' => '<<'),
                        'ajaxUpdate' => false,
                        'dataProvider' => $model->search($agencymodel->id),
                        //'filter'=>$model,
                        'columns' => array(
                            array(
                                'id' => 'selectedIds',
                                'value' => '$data->id',
                                'class' => 'CCheckBoxColumn',
                                'selectableRows' => 2
                            ),
                            array(
                                'header' => 'Name',
                                'value' => 'CHtml::link("<i class=\"fa fa-pencil\"></i> ".$data->candidate->forenames." ".$data->candidate->surname,Yii::app()->createUrl("agency/editStaff",array("id"=>$data->id)),array("class"=>"staff-edit-button","id"=>"staff-edit"))',
                                'type' => 'raw'
                            ),                            
                            array(
                                'header' => 'Email',
                                'value'=>'$data->candidate->email'
                            ),
                            array(
                                'header' => 'Candidates',                                
                                'value'=>'CHtml::link("<i class=\"fa fa-users\"></i> ".$data->getStudentcount(),Yii::app()->createUrl("agency/viewcandidates",array("id"=>$data->id)),array("class"=>"staff-edit-button","id"=>"staff-edit"))',
                                'type' => 'raw'
                            )
                            
                        )
                    ));
                    ?>
                   <?php 
                       $asmodelcnt = AgencyStaff::model()->countByAttributes(array('agency_id'=>$agencymodel->id));
                       echo CHtml::button("Add Staff", array("class" => "btn btn-default add_staff","disabled"=>($asmodelcnt >= $agencymodel->num_staff ? true : false))); 
                       //echo CHtml::button("Add Staff", array("class" => "btn btn-default","disabled"=>($asmodelcnt >= $agencymodel->num_staff ? true : false), "onclick" => "window.location='" . Yii::app()->createUrl('agency/createstaff',array("id"=>$agencymodel->id)) . "'")) ?>
                </div>
            </div>
        </div>

    </div>
</div>

