<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */
/* @var $form CActiveForm */
?>
<script>
    $(document).ready(function () {

    });
</script>



<h1><?php echo $usermodel->isNewRecord ? "Create" : "Update" ?> Recruitment Agency</h1>
<div class="Admin__content__inner">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'agency-admin-form',
        'action' => $this->createUrl('agency/saveAgency', array('id' => $model->id)),
        'enableAjaxValidation' => false
    ));
    ?>
      <h4>Account Details</h4>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                               <?php echo $form->textField($usermodel, 'forenames', array('class' => 'form-control','placeholder'=>'First Name')); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <?php echo $form->textField($usermodel, 'surname', array('class' => 'form-control','placeholder'=>'Surname')); ?>
                        </div>
                    </div>
                     <div class="col-sm-3">
                        <div class="form-group">
                            <?php echo $form->textField($usermodel, 'email', array('class' => 'form-control','placeholder'=>'Email Address')); ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                   
                    <div class="col-sm-3">
                        <div class="form-group">
                             <?php echo $usermodel->password = ""; ?>
                            <?php echo $form->passwordField($usermodel, 'password', array('class' => 'form-control','placeholder'=>'Password')); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                             <input class="form-control" placeholder="Confirm Password" name="Users[confirmpassword]" id="Users_confirmpassword" type="password" value="">
                        </div>
                    </div>
                </div>
                <h4>Agency details</h4>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                              <?php echo $form->textField($model, 'name', array('class' => 'form-control','placeholder'=>'Agency Name')); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                             <?php echo $form->textField($model, 'num_staff', array('class' => 'form-control','placeholder'=>'Number of Staff')); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <select name="industry_id" class="form-control select2">
<!--                                 <option selected="selected" disabled>Search Industry</option> -->
                                <?php
                                $industrymodel = Industries::model()->findAll();
                                echo "<option value=''>Select Industry</option>";
                                foreach ($industrymodel as $ind) {
                                    ?>                  
                                    <option value="<?php echo $ind['id'] ?>"> <?php echo $ind['name'] ?> </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                 
                </div>

                <div class="form-container">
                    <h4>Address</h4>
                     <div id="location-fields">
                        <?php $this->renderPartial('//addresses/address-form', array('form' => $form, 'addressmodel' => $addressmodel)); ?>
                    </div>

                </div>
				<!-- <div class="form-container">
                    <h4>Additional Offices (Optional)</h4>
                     <div id="location-fields">
                        <?php $this->renderPartial('//addresses/address-form', array('form' => $form, 'addressmodel' => $addressmodel)); ?>
                    </div>
                </div> -->
                <div class="form-container">
                    <!-- Custom url -->
                    <h4>Custom URL</h4>
                    <div class="row">
                        <div class="col-sm-12">
                          <div class="form-inline slug-field">
                                 <div class="form-group">http://<?php echo $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF'])."/"?></div>
                                <div class="form-group">
                                    <?php echo $form->textField($profilemodel, 'slug', array('class' => 'form-control','placeholder'=>'company-name','id'=>'slug')); ?>
                                </div>
                                <div class="form-group">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- .form-container -->
                
    <div class="form-actions">
        <?php echo CHtml::submitButton('Save', array('class' => 'btn btn-default')); ?>
        <?php echo CHtml::button('Cancel', array('class' => 'btn btn-default')); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>

   <script type="text/javascript">
       $(function(){
            new AgencySlugGenerator();
        });
    </script>