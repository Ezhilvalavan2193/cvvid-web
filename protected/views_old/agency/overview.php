<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */

$this->breadcrumbs=array(
	'Institutions'=>array('index'),
	'Manage',
);

// $this->menu=array(
// 	array('label'=>'List Institutions', 'url'=>array('index')),
// 	array('label'=>'Create Institutions', 'url'=>array('create')),
// );

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#inst-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<script>
$(document).ready(function(){
	
	$('#export-candidate').on('click',function() {
	    window.location = '<?php echo $this->createUrl('users/exportCSV') ?>'
	});

	$('#delete-candidate').click(function(e){

		
         if($("#users-grid").find("input:checked").length > 0)
         {

        	 if (confirm('Are you sure you want to delete these items?')) {

         		 var ids = $.fn.yiiGridView.getChecked("users-grid", "selectedIds");
             	
       			  $.ajax({
       		            url: '<?php echo $this->createUrl('users/deleteUsers') ?>',
       		            type: 'POST',
       		            data: {ids : ids}, 	    
       		            success: function(data) {
       			            alert(data);
       		            },
       				    error: function(data) {		
       				        alert('err');
       				    }
       		         });
        	 } 
        	
         }
         else
         {
            alert('Please select at least one item');
         }
	});
	
});

</script>
<h1>Recruitment Agency</h1>
<div class="Admin__content__inner">
 <?php if (Yii::app()->user->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>
    <div class="AdminFilters">
        <div class="form-inline">
            <?php 
//             $this->renderPartial('_search',array(
//                 'model'=>$model
//             ));
            ?>
            <div class="pull-right">
                <?php       
                    echo CHtml::button('Create',array("style"=>"","class"=>"btn btn-primary","onclick"=>"window.location='".Yii::app()->createUrl('agency/createAgency')."'"));
                    echo CHtml::button('Delete',array("class"=>"btn btn-primary",'id'=>'delete-candidate'));
              ?>
            </div>
        </div>
    </div>

    <?php 
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'inst-grid',
      'itemsCssClass' => 'table',
	'summaryText'=>'',
	//'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',    
    'enableSorting'=>true,
    'ajaxUpdate'=>true,
    
    'pager' => array(
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'header'=>'',
        'firstPageLabel' => '<<',
        'lastPageLabel' => '>>',
        'nextPageLabel' => '>>',
        'prevPageLabel' => '<<'),
			'ajaxUpdate'=>false,
    'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
    	    array(
    	        'id' => 'selectedIds',
    	        'value' => '$data->id',
    	        'class' => 'CCheckBoxColumn',
    	        'selectableRows' => 2    	        
    	    ),
			array(
					'header'=>'Name',
					'value'=>'CHtml::link("<i class=\"fa fa-pencil\"></i> ".$data->name,Yii::app()->createUrl("agency/tabedit", array("id"=>$data->id)),array("class"=>"profile-edit-button","id"=>"profile-edit"))',
			        'type'=>'raw'
			),
			array(
					'header'=>'Profile',
					'value'=>'CHtml::link("View",Yii::app()->createUrl("agency/profile", array("id"=>$data->id)),array("class"=>"profile-view-button","id"=>"profile-view"))',
					'type'=>'raw'
			),
//     	    array(
//         	        'header'=>'Admin',
//         	        'value'=>'$data->getAdmin()',
//     	    ),
// 			array(
// 					'header'=>'Status',
// 					'value'=>'$data->status',
// 			)
	),
)); ?>

</div>

