<?php
/* @var $this AgencyController */
/* @var $model Agency */
/* @var $form CActiveForm */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
");
?>
<script>
    $(document).ready(function () {

    });
</script>

<div class="page-title text-left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $agencystaffmodel->isNewRecord ? "Create" : "Edit" ?> Staff</h1>
            </div>
        </div>
    </div>
</div>
<div id="page-content">
    <div class="container">
        <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'agency-staff-form',
                'action' => ($agencystaffmodel->isNewRecord ? $this->createUrl('agency/saveStaff',array('id'=>$model->id)) : $this->createUrl('agency/updateStaff', array('id' => $agencystaffmodel->id))),
                'enableAjaxValidation' => false,
                'htmlOptions' => array('enctype' => 'multipart/form-data')
            ));
        ?>
        <?php echo $form->errorSummary(array($model,$usermodel,$addressmodel),'<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
          
              
                    <div class="form-container">
                        <!-- Your details -->
                        <h4>Your details</h4>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?php echo $form->textField($usermodel, 'forenames', array('class' => 'form-control', 'placeholder' => 'First Name')); ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?php echo $form->textField($usermodel, 'surname', array('class' => 'form-control', 'placeholder' => 'Surname')); ?>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <input type="text" name="dob" placeholder="Date of Birth" class="form-control dob" value="<?php echo ((empty($model->dob) || $model->dob == null || $model->dob == "0000-00-00") ? "" : date("d/m/Y", strtotime($model->dob))); ?>">
                 
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?php echo $form->textField($usermodel, 'email', array('class' => 'form-control', 'placeholder' => 'E-mail Address','readonly'=>(!$agencystaffmodel->isNewRecord ? true : false ))); ?>
                                </div>
                            </div>
                            
                        </div>
                    </div><!-- .form-container -->
					<div class="form-container">
                        <h4>Gender</h4>
                        <div id="gender-fields">
                            <?php $usermodel->gender = 'M';
                                $accountStatus = array('M'=>'Male', 'F'=>'Female');
                                echo $form->radioButtonList($usermodel, 'gender',$accountStatus,array('separator'=>' ')); ?>
                        </div>

                    </div>
                    <div class="form-container">
                        <h4>Address</h4>
                        <div id="location-fields">
                            <?php $this->renderPartial('//addresses/address-form', array('form' => $form, 'addressmodel' => $addressmodel)); ?>
                        </div>

                    </div>

                    <div class="form-container">
                        <!-- Account -->
                        <h4>Account Settings</h4>
                        <div class="row">
                            <div class="col-sm-3">
                                <?php echo $usermodel->password = ""; ?>
                                <?php echo $form->passwordField($usermodel, 'password', array('class' => 'form-control', 'placeholder' => 'Password')); ?>
                            </div>
                            <div class="col-sm-3">
                                <input class="form-control" placeholder="Confirm Password" name="Users[confirmpassword]" id="Users_confirmpassword" type="password" value="">
                            </div>
                        </div>
                    </div>

            <div class="form-actions clearfix">
                <div class="pull-left">
                     <?php
                        echo CHtml::submitButton('Save', array("class" => "btn btn-default",'style'=>'margin-right:10px'));
                        echo CHtml::button('Cancel', array("class" => "btn btn-default", "onclick" => "window.location='" . Yii::app()->createUrl("agency/candidates",array('id'=>$model->id)). "'"));
                     ?> 
                    
                </div>
            </div>       
      <?php $this->endWidget(); ?>
    </div>
</div>