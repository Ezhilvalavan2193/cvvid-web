<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs = array(
    'Employers' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
    
");
?>


<div id="profile">
    <?php
    $employerid = isset($this->employerid) ? $this->employerid : 0;
    $userid = isset($this->userid) ? $this->userid : 0;
    $institutionid = isset($this->institutionid) ? $this->institutionid : 0;
    $agencyid = isset($this->agencyid) ? $this->agencyid : 0;
    $edit = isset($this->edit) ? $this->edit : 0;
    $this->renderPartial('//profiles/Cover', array('userid' => $userid, 'employerid' => $employerid, 'institutionid' => $institutionid,'agencyid' => $agencyid, 'edit' => $edit));
    ?>
    <?php
        $usermodel = new Users();
        $profilemodel = new Profiles();
        if($model->user_id > 0)
        {
            $usermodel = Users::model()->findByPk($model->user_id);
            $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $model->id, 'type' => $usermodel->type));
        }
    ?>
    <div id="form-details">
        <div class="container">
            <div class="form-details-inner">
                <div class="form-user-details form-section">
                    <div class="row">
                        <div class="text-center col-sm-4 col-md-3 col-lg-2">
                            <div class="ProfileEdit__photo">
                                <?php 									     
                                if($profilemodel->photo_id > 0)
                                 {
                                     $mediamodel = Media::model()->findByPk($profilemodel->photo_id);
                                     $src = ((!empty($mediamodel->file_name) && $mediamodel->file_name != null) ? Yii::app()->baseUrl."/images/media/".$profilemodel->photo_id."/".$mediamodel->file_name : Yii::app()->baseUrl."/images/profile.png");
                                 } 
                                    else
                                    {
                                        $src = Yii::app()->baseUrl."/images/defaultprofile.jpg";
                                    }
                                    echo CHtml::image($src,"",array("id" => "profile-image",'alt'=>''));
                                   ?>
                                <?php if (isset($this->edit) && $this->edit) { ?>
                                    <a class="open-media profile-photo-btn" data-field="photo" data-target="#profile-image">
                                        <i class="fa fa-camera"></i>
                                        Change Photo
                                    </a>
                                <?php } ?>
                            </div>
                     
                            <a class="btn btn-primary" href="<?php echo $this->createUrl('agency/edit', array('id' => $model->id)) ?>">
                                <i class="fa fa-pencil"></i>
                                <span>Edit Profile</span>
                            </a>
                           
                        </div>
                        <div class="col-sm-5 col-md-6 col-lg-7">
                            <div class="ProfileEdit__details">
                                
                                
                                 <h3><?php echo $model->name ?></h3>
                                <?php 
                                $cri = new CDbCriteria();
                                $cri->condition = "model_id = ".$model->id." and model_type like '%agency%' and deleted_at is null";
                                $address = Addresses::model()->find($cri);
                                ?>
                                <!-- <div>Address 1: <?php //echo $model->address_1 ?></div> -->
                                <div>Address: <?php echo ($address != null ? $address->address : ""); ?></div>
                                <div>Town: <?php echo ($address != null ? $address->town : ""); ?></div>
                                 <?php if (Yii::app()->user->getState('username'))  ?>
                                <div>Contact: <?php echo Yii::app()->user->getState('username'); ?></div>
                                
                            </div>
                        </div>
                    </div>
              
            </div>
        </div><!-- /.container -->
    </div>
</div>
