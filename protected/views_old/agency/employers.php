<?php
/* @var $this AgencyController */
/* @var $model AgencyEmployers */

$this->breadcrumbs = array(
    'Agency' => array('index'),
    'Manage',
);

// $this->menu=array(
// 	array('label'=>'List Institutions', 'url'=>array('index')),
// 	array('label'=>'Create Institutions', 'url'=>array('create')),
// );

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#inst-admin-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<script>
    $(document).ready(function () {
        $('body').on('click', '#delete-employers', function () {

            if ($("#agency-emp-grid").find("input:checked").length > 0)
            {
                if (confirm('Are you sure you want to delete these items?')) {

                    var ids = $.fn.yiiGridView.getChecked("agency-emp-grid", "selectedIds");
                    $.ajax({
                        url: '<?php echo $this->createUrl('agency/deleteEmployers') ?>',
                        type: 'POST',
                        data: {ids: ids},
                        success: function (data) {
                            location.reload();
                        }
                    });
                }
            }else{
               alert('Please select at least one item');
            }

        });

        $('#export-candidate').on('click', function () {
            window.location = '<?php echo $this->createUrl('users/exportCSV') ?>'
        });

    });

</script>

<div class="page-title text-left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Employers</h1>
            </div>
        </div>
    </div>
</div>
<div id="page-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 col-md-2 dashboard-header-section">
                <ul class="box dashboard-links list-unstyled">
                    <li><a href="<?php echo $this->createUrl('agency/employers', array('id' => $agencymodel->id)) ?>">Employers</a></li>
                   <li><a href="<?php echo $this->createUrl('agency/staff', array('id' => $agencymodel->id)) ?>">Staffs</a></li>
                    <li><a href="<?php echo $this->createUrl('agency/candidates', array('id' => $agencymodel->id)) ?>">Candidates</a></li>
                    
                    <li><a href="<?php echo $this->createUrl('agency/vacancies', array('id' => $agencymodel->id)) ?>">Vacancies</a></li>
                </ul>
            </div>
            <div class="col-sm-9 col-md-10">
                <div class="pull-right">
                    <button type="button" class="main-btn" id="delete-employers"> Delete</button>
                </div>
                <div class="dashboard-table">

                    <?php
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'agency-emp-grid',
                        'itemsCssClass' => 'table',
                        'summaryText' => '',
                        // 'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                        'enableSorting' => true,
                        'ajaxUpdate' => true,
                        'pager' => array(
                            //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                            'header' => '',
                            'firstPageLabel' => '<<',
                            'lastPageLabel' => '>>',
                            'nextPageLabel' => '>>',
                            'prevPageLabel' => '<<'),
                        'ajaxUpdate' => false,
                        'dataProvider' => $model->search($agencymodel->id),
                        //'filter'=>$model,
                        'columns' => array(
                            array(
                                'id' => 'selectedIds',
                                'value' => '$data->id',
                                'class' => 'CCheckBoxColumn',
                                'selectableRows' => 2
                            ),
                            array(
                                'header' => 'Name',
                                'value' => 'CHtml::link("<i class=\"fa fa-pencil\"></i> ".$data->employer->name,Yii::app()->createUrl("agency/editEmployer",array("id"=>$data->id)),array("class"=>"emp-edit-button","id"=>"emp-edit"))',
                                'type' => 'raw'
                            ),
                            array(
                                'header' => 'Email',
                                'value' => '$data->employer->email'
                            ),
                            array(
                                'header' => 'Website',
                                'value' => '$data->employer->website'
                            )
                        ),
                    ));
                    ?>
                    <?php echo CHtml::button("Add Employer", array("class" => "btn btn-default", "onclick" => "window.location='" . Yii::app()->createUrl('agency/createemployer', array("id" => $agencymodel->id)) . "'")) ?>
                </div>
            </div>
        </div>

    </div>
</div>

