<?php
/* @var $this AgencyController */
/* @var $model AgencyVacancies */

$this->breadcrumbs=array(
    'Agency'=>array('index'),
    'Manage',
);

// $this->menu=array(
// 	array('label'=>'List Institutions', 'url'=>array('index')),
// 	array('label'=>'Create Institutions', 'url'=>array('create')),
// );

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#inst-admin-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<script>
$(document).ready(function(){
	
	$('#delete-vacancies').click(function(e){

		
        if($("#agency-vacancies-grid").find("input:checked").length > 0)
        {			
       	 if (confirm('Are you sure you want to delete these items?')) {

        		 var ids = $.fn.yiiGridView.getChecked("agency-vacancies-grid", "selectedIds");
      			  $.ajax({
      		            url: '<?php echo $this->createUrl('agency/deleteVacancies') ?>',
      		            type: 'POST',
      		            data: {ids : ids}, 	    
      		            success: function(data) {
      		            	$.fn.yiiGridView.update('agency-vacancies-grid');
      		            },
      				    error: function(data) {		
      				        alert('err');
      				    }
      		         });
       	 } 
       	
        }
        else
        {
           alert('Please select at least one item');
        }
	});
	
});

</script>
<div class="Admin__content__inner">         
	<div class="pull-right">       
		<?php echo CHtml::button("Add vacancy", array("class" => "btn btn-primary","onclick" => "window.location='" . Yii::app()->createUrl('agency/createvacancy',array("id"=>$agencymodel->id)) . "'")) ?>
    </div>
                  <?php if (Yii::app()->user->hasFlash('vacancy')): ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php echo Yii::app()->user->getFlash('vacancy'); ?>
                </div>
            		<?php endif; ?>
                    <?php
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'agency-vacancies-grid',
                         'itemsCssClass' => 'table',
                        'summaryText' => '',
                       // 'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                        'enableSorting' => true,
                        'ajaxUpdate' => true,
    'pager' => array(
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'header'=>'',
        'firstPageLabel' => '<<',
        'lastPageLabel' => '>>',
        'nextPageLabel' => '>>',
        'prevPageLabel' => '<<'),
                        'ajaxUpdate' => false,
                        'dataProvider' => $model->search($agencymodel->id),
                        //'filter'=>$model,
                        'columns' => array(
                            array(
                                'id' => 'selectedIds',
                                'value' => '$data->job_id',
                                'class' => 'CCheckBoxColumn',
                                'selectableRows' => 2
                            ),
                            array(
                                'header' => 'Job',
                                'value' => 'CHtml::link($data->job->title,Yii::app()->createUrl("agency/editVacancy",array("id"=>$data->id)),array("class"=>"can-edit-button","id"=>"can-edit"))',
                                'type' => 'raw'
                            ),     
                            array(
                                'header' => 'Job type',
                                'value'=>'$data->job->type'
                            ),
                            array(
                                'header' => 'vacancies',
                                'value'=>'$data->job->vacancies'
                            )
                          
                        ),
                    ));
                    ?>
                
  </div>
      
<!-- Assign candidate dialog -->

<div class="modal fade" id="assign_candidate_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">            
            <div class="MediaManager__header">
                <h3>Assign candidates to staff</h3>
            </div>            
            <div class="MediaManager__content">
             	<div class="col-md-4">
             	 <label>Select Staff</label>
               		 <select name="staff-select" class="form-control" id="staff-select">
                    	<?php 
                    	   $agencystaffmodel = AgencyStaff::model()->findAll();
                    	   foreach ($agencystaffmodel as $staff)
                    	   {
                    	       $umodel = Users::model()->findByPk($staff['user_id']);
                    	?>
        					<option value="<?php echo $umodel->id ?>"><?php echo $umodel->forenames." ".$umodel->surname ?></option>
        				
        		  		<?php } ?>
        		    </select>    
    		    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default-btn" data-dismiss="modal" onclick="closepopup()">Close</button>
                <button type="button" class="btn main-btn select-submit savebtn">Save</button>
            </div>

        </div>
    </div>
</div>