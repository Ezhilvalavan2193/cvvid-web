<?php

class Plan implements JsonSerializable {
    
     /**
     * @var
     */
    public $name;

    /**
     * @var
     */
    public $id;

    /**
     * @var
     */
    public $user_type = 'candidate';

    /**
     * @var int
     */
    public $price = 0;

    /**
     * @var string
     */
    public $interval = 'yearly';

    /**
     * @var array
     */
    public $permissions = [];

    /**
     * @var bool
     */
    public $active = true;

    /**
     * @var string
     */
    public $price_suffix = '';

    /**
     * @var string
     */
    public $description = '';

    
    /**
     * @param $name
     * @param $id
     */
    public function __construct($name, $id)
    {
        $this->name = $name;
        $this->id = $id;
    }
    
    /**
     * The type of user the membership is for
     *
     * @param $type
     * @return $this
     */
    public function user_type($type)
    {
        $this->user_type = $type;

        return $this;
    }

    /**
     * Set the price of the plan
     *
     * @param $price
     * @return $this
     */
    public function price($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get the price suffix
     *
     * @param $suffix
     * @return $this
     */
    public function price_suffix($suffix)
    {
        $this->price_suffix = $suffix;

        return $this;
    }

    /**
     * Set the description of the plan
     *
     * @param $description
     * @return $this
     */
    public function description($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Specify that the plan is monthly
     *
     * @return $this
     */
    public function monthly()
    {
        $this->interval = 'monthly';

        return $this;
    }

    /**
     * Specify that the plan is archived
     *
     * @return $this
     */
    public function archived()
    {
        $this->active = false;

        return $this;
    }

    /**
     * Specify the permissions
     *
     * @param array $permissions
     * @return $this
     */
    public function permissions(array $permissions)
    {
        $this->permissions = $permissions;

        return $this;
    }
    
    /**
     * Specify data which should be serialized to JSON
     * which is a value of any type other than a resource.
     */
    function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'user_type' => $this->user_type,
            'name' => $this->name,
            'price' => $this->price,
            'price_suffix' => $this->price_suffix,
            'description' => $this->description,
            'interval' => $this->interval,
            'active' => $this->active,
            'permissions' => $this->permissions,
        ];
    }
    
}