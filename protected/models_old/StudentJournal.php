<?php

/**
 * This is the model class for table "cv16_student_journal".
 *
 * The followings are the available columns in table 'cv16_student_journal':
 * @property integer $id
 * @property integer $student_id
 * @property string $institution
 * @property string $education
 * @property string $work_experience
 * @property string $volunteering
 * @property string $curriculum_activities
 * @property string $goals
 * @property string $hobbies
 */
class StudentJournal extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cv16_student_journal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('student_id, volunteering, curriculum_activities, goals, hobbies'),
			array('student_id', 'numerical', 'integerOnly'=>true),
			array('institution', 'length', 'max'=>255),
			array('education, work_experience,student_id, volunteering, curriculum_activities, goals, hobbies', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, student_id, institution, education, work_experience, volunteering, curriculum_activities, goals, hobbies', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'student_id' => 'Student',
			'institution' => 'Institution',
			'education' => 'Education',
			'work_experience' => 'Work Experience',
			'volunteering' => 'Volunteering',
			'curriculum_activities' => 'Curriculum Activities',
			'goals' => 'Goals',
			'hobbies' => 'Hobbies',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('student_id',$this->student_id);
		$criteria->compare('institution',$this->institution,true);
		$criteria->compare('education',$this->education,true);
		$criteria->compare('work_experience',$this->work_experience,true);
		$criteria->compare('volunteering',$this->volunteering,true);
		$criteria->compare('curriculum_activities',$this->curriculum_activities,true);
		$criteria->compare('goals',$this->goals,true);
		$criteria->compare('hobbies',$this->hobbies,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StudentJournal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
