<?php

class Membership {

    protected $cardUpFront = false;
    /**
     * @var array
     */
    public static $plans = [];

    /**
     * @return bool
     */
    public static function freeTrailEnabled()
    {
        return true;
    }
    
    public static function plan($name, $id)
    {
        static::$plans[] = $plan = new Plan($name, $id);

        return $plan;
    }
  
    
    public static function onTrail($trail)
    {
        if (!is_null($trail)) {
            return Carbon::today()->lt(Carbon::instance(new DateTime($trail)));
        } else {
            return false;
        }
    }
        
//    stripe active
    public static function stripeIsActive($stripe = true)
    {
        return $stripe;
    }
    
//    subscription ends
    public static function onGracePeriod($endsAt)
    {
        if (!is_null($endsAt)) {
            return Carbon::now()->lt(Carbon::instance(new DateTime($endsAt)));
        } else {
            return false;
        }
    }
    
//    subscribed
     public static function subscribed($status, $trails, $subs)
    {
        if (Membership::requiresCardUpFront()) {
            return Membership::stripeIsActive($status) || Membership::onGracePeriod($subs);
        } else {
            return Membership::stripeIsActive($status) || Membership::onTrail($trails) || Membership::onGracePeriod($subs);
        }
    }
    
    
    
    public static function requiresCardUpFront()
    {
        if(isset(Membership::$cardUpFront)) {
            return Membership::$cardUpFront;
        }
        return false;
    }
    
}