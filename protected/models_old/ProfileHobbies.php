<?php

/**
 * This is the model class for table "cv16_profile_hobbies".
 *
 * The followings are the available columns in table 'cv16_profile_hobbies':
 * @property string $id
 * @property string $profile_id
 * @property string $user_hobby_id
 * @property string $created_at
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property Profiles $profile
 * @property UserHobbies $userHobby
 */
class ProfileHobbies extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cv16_profile_hobbies';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('profile_id, user_hobby_id', 'required'),
			array('profile_id, user_hobby_id', 'length', 'max'=>10),
			array('created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, profile_id, user_hobby_id, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'profile' => array(self::BELONGS_TO, 'Profiles', 'profile_id'),
			'userHobby' => array(self::BELONGS_TO, 'UserHobbies', 'user_hobby_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'profile_id' => 'Profile',
			'user_hobby_id' => 'User Hobby',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('profile_id',$this->profile_id,true);
		$criteria->compare('user_hobby_id',$this->user_hobby_id,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function searchByID($id)
	{
	    // @todo Please modify the following code to remove attributes that should not be searched.
	    
	    $criteria=new CDbCriteria;
	    
	    $criteria->compare('id',$this->id,true);
	    $criteria->compare('profile_id',$this->profile_id,true);
	    $criteria->compare('user_hobby_id',$this->user_hobby_id,true);
	    $criteria->compare('created_at',$this->created_at,true);
	    $criteria->compare('updated_at',$this->updated_at,true);
	    
	    $criteria->addCondition('profile_id ='.$id);
	    
	    return new CActiveDataProvider($this, array(
	        'criteria'=>$criteria,
	    ));
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProfileHobbies the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
