<?php

/**
 * This is the model class for table "cv16_profiles".
 *
 * The followings are the available columns in table 'cv16_profiles':
 * @property string $id
 * @property string $owner_id
 * @property string $owner_type
 * @property string $type
 * @property string $slug
 * @property string $industry_id
 * @property string $photo_id
 * @property string $cover_id
 * @property integer $visibility
 * @property integer $published
 * @property string $num_likes
 * @property string $num_views
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 *
 * The followings are the available model relations:
 * @property ProfileDocuments[] $profileDocuments
 * @property ProfileExperiences[] $profileExperiences
 * @property ProfileFavourites[] $profileFavourites
 * @property ProfileFavourites[] $profileFavourites1
 * @property ProfileHobbies[] $profileHobbies
 * @property ProfileLanguages[] $profileLanguages
 * @property ProfileQualifications[] $profileQualifications
 * @property ProfileSkills[] $profileSkills
 * @property ProfileVideos[] $profileVideoses
 * @property ProfileViews[] $profileViews
 * @property Media $cover
 * @property Industries $industry
 * @property Media $photo
 * @property ProfileLikes[] $profileLikes
 */
class Profiles extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cv16_profiles';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('slug', 'required','message'=>'A custom url is required for your profile.'),
		    array('slug', 'unique','attributeName' => 'slug','message'=>'This Slug is already in use'),
			array('visibility, published', 'numerical', 'integerOnly'=>true),
			array('owner_id, industry_id, photo_id, cover_id, num_likes, num_views', 'length', 'max'=>10),
			array('owner_type, type, slug', 'length', 'max'=>255),
			array('created_at, updated_at, deleted_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, owner_id, owner_type, type, slug, industry_id, photo_id, cover_id, visibility, published, num_likes, num_views, created_at, updated_at, deleted_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'profileDocuments' => array(self::HAS_MANY, 'ProfileDocuments', 'profile_id'),
			'profileExperiences' => array(self::HAS_MANY, 'ProfileExperiences', 'profile_id'),
			'profileFavourites' => array(self::HAS_MANY, 'ProfileFavourites', 'favourite_id'),
			'profileFavourites1' => array(self::HAS_MANY, 'ProfileFavourites', 'profile_id'),
			'profileHobbies' => array(self::HAS_MANY, 'ProfileHobbies', 'profile_id'),
			'profileLanguages' => array(self::HAS_MANY, 'ProfileLanguages', 'profile_id'),
			'profileQualifications' => array(self::HAS_MANY, 'ProfileQualifications', 'profile_id'),
			'profileSkills' => array(self::HAS_MANY, 'ProfileSkills', 'profile_id'),
			'profileVideoses' => array(self::HAS_MANY, 'ProfileVideos', 'profile_id'),
			'profileViews' => array(self::HAS_MANY, 'ProfileViews', 'profile_id'),
			'cover' => array(self::BELONGS_TO, 'Media', 'cover_id'),
			'industry' => array(self::BELONGS_TO, 'Industries', 'industry_id'),
			'photo' => array(self::BELONGS_TO, 'Media', 'photo_id'),
		    'profileLikes' => array(self::HAS_MANY, 'ProfileLikes', 'like_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'owner_id' => 'Owner',
			'owner_type' => 'Owner Type',
			'type' => 'Type',
			'slug' => 'Slug',
			'industry_id' => 'Industry',
			'photo_id' => 'Photo',
			'cover_id' => 'Cover',
			'visibility' => 'Visibility',
			'published' => 'Published',
			'num_likes' => 'Num Likes',
			'num_views' => 'Num Views',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'deleted_at' => 'Deleted At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('owner_id',$this->owner_id,true);
		$criteria->compare('owner_type',$this->owner_type,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('industry_id',$this->industry_id,true);
		$criteria->compare('photo_id',$this->photo_id,true);
		$criteria->compare('cover_id',$this->cover_id,true);
		$criteria->compare('visibility',$this->visibility);
		$criteria->compare('published',$this->published);
		$criteria->compare('num_likes',$this->num_likes,true);
		$criteria->compare('num_views',$this->num_views,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('deleted_at',$this->deleted_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Profiles the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
