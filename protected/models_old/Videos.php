<?php

/**
 * This is the model class for table "cv16_videos".
 *
 * The followings are the available columns in table 'cv16_videos':
 * @property string $id
 * @property string $model_type
 * @property integer $model_id
 * @property string $video_id
 * @property string $name
 * @property integer $duration
 * @property string $category_id
 * @property integer $is_processed
 * @property integer $state
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $ordering
 *
 * The followings are the available model relations:
 * @property ProfileVideos[] $profileVideoses
 * @property VideoCategories $category
 */
class Videos extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cv16_videos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('model_id, duration, is_processed, state', 'numerical', 'integerOnly'=>true),
			array('model_type, video_id, name', 'length', 'max'=>255),
			array('category_id', 'length', 'max'=>10),
			array('status', 'length', 'max'=>100),
			array('created_at, updated_at, deleted_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('model_type, model_id, video_id, name, duration, category_id, is_processed, state, status, created_at, updated_at, deleted_at, ordering', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'profileVideoses' => array(self::HAS_MANY, 'ProfileVideos', 'video_id'),
			'category' => array(self::BELONGS_TO, 'VideoCategories', 'category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'model_type' => 'Model Type',
			'model_id' => 'Model',
			'video_id' => 'Video ID',
			'name' => 'Name',
			'duration' => 'Duration',
			'category_id' => 'Category',
			'is_processed' => 'Is Processed',
			'state' => 'State',
			'status' => 'Status',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'deleted_at' => 'Deleted At',
			'ordering' => 'Ordering',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('model_type',$this->model_type,true);
		$criteria->compare('model_id',$this->model_id);
		$criteria->compare('video_id',$this->video_id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('duration',$this->duration);
		$criteria->compare('category_id',$this->category_id,true);
		$criteria->compare('is_processed',$this->is_processed);
		$criteria->compare('state',$this->state);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('deleted_at',$this->deleted_at,true);
		$criteria->compare('ordering',$this->ordering);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Videos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function getUploader()
	{
	    $umodel = Users::model()->findByPk($this->model_id);
            if($umodel != null)
	        return $umodel->forenames." ".$umodel->surname;
            else
                return "";
	}
	public function getLength()
	{
	    $length = gmdate("i:s", $this->duration);
	    return $length;
	}
	public function getVisible()
	{
	    if($this->category_id == 1)
	       return 0;
            else
               return 1;
	}
	public function getImageURL()
	{
	    $mediamodel = Media::model()->findByAttributes(array('model_id'=>$this->id,'collection_name'=>'video'));
            if($mediamodel != null)
	        return Yii::app()->baseUrl."/images/media/".$mediamodel->id."/conversions/profile.jpg";
            else
                return "";
	}
	public function getName()
	{
	    if($this->status == "active")
	       return $this->name;
	    else    
	       return '<i class="fa fa-flag"></i>'.$this->name;
	}
}
