<?php

class EmployerOfficesController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
    public $layout = '//layouts/admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
// 			array('deny',  // deny all users
// 				'users'=>array('*'),
// 			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
	    $ids = implode(',', $_POST['ids']);
	    $offcids = explode(",", $ids);
	    $now = new DateTime();
	    
	    $empmodel = Employers::model()->findByPk($id);	    	    	    
	    
	    $cri = new CDbCriteria();
	    $cri->condition = "id in (".$ids.")";
	    $empofc = EmployerOffices::model()->findAll($cri);
	    
	    foreach ($empofc as $ofc)
	    {
	        $addressmodel = Addresses::model()->findByPk($ofc['id']);
	        $addressmodel->deleted_at = $now->format('Y-m-d H:i:s');
	        $addressmodel->save();
	    }
	    $cri = new CDbCriteria();
	    $cri->condition = "id in (".$ids.")";
	    EmployerOffices::model()->updateAll(array('deleted_at' => $now->format('Y-m-d H:i:s')),$cri);
	    
	    $this->redirect($this->createUrl('employerOffices/admin',array('id'=>$id)));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('EmployerOffices');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin($id)
	{
	    if(Yii::app()->user->getState('role') != 'admin')
	        $this->layout='main';
	    $imodel=Employers::model()->findByPk($id);
	    $model=new EmployerOffices('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['EmployerOffices']))
			$model->attributes=$_GET['EmployerOffices'];

		if(Yii::app()->user->getState('role') != 'admin')
    		$this->render('officelist',array(
    		    'model'=>$model,'insmodel'=>$imodel,'addressmodel'=>new Addresses()
    		));
        else 
            $this->render('admin',array(
                'model'=>$model,'insmodel'=>$imodel,'addressmodel'=>new Addresses()
            ));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return InstitutionOffices the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
	    $model=EmployerOffices::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param InstitutionOffices $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='institution-offices-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function beforeAction($action) {
	    Yii::app()->user->setReturnUrl(Yii::app()->request->getUrl());
	    $requestUrl = Yii::app()->request->url;
	    if (Yii::app()->user->isGuest && !strpos(Yii::app()->request->getUrl(), 'employers/profile'))
	        $this->redirect(Yii::app()->createUrl('site/login'));
	        
	        return parent::beforeAction($action);
	}
	
	public function actioncreate($id)
	{
	    if(Yii::app()->user->getState('role') != 'admin')
	        $this->layout='main';
	    $model = new EmployerOffices;
	    $imodel = Employers::model()->findByPk($id);
	    $addressmodel = new Addresses();
	    $now = new DateTime();
	    // Uncomment the following line if AJAX validation is needed
	    // $this->performAjaxValidation($model);
	    if(isset($_POST['Addresses']))
	    {	     
	    
	        $addressmodel->attributes = $_POST['Addresses'];
	        $addressmodel->model_type = 'Office';
	        $addressmodel->model_id = $imodel->id;	        
	        $addressmodel->created_at = $now->format('Y-m-d H:i:s');
	        $addressmodel->head_office = $_POST['Addresses']['head_office'];
	        
	        if($addressmodel->save())
	        {
	            $model->employer_id = $id;
	            $model->address_id = $addressmodel->id;
	            $model->created_at = $now->format('Y-m-d H:i:s');
	            if($model->save())
	               $this->redirect($this->createUrl('employerOffices/admin',array('id'=>$id)));
	        }
	    }
	    
	    if(Yii::app()->user->getState('role') != 'admin')
    	    $this->render('createoffice',array(
    	        'model'=>$model,'insmodel'=>$imodel,'addressmodel'=>$addressmodel
    	    ));
        else
            $this->render('office',array(
                'model'=>$model,'insmodel'=>$imodel,'addressmodel'=>$addressmodel
            ));
	}
	
	public function actionedit($id)
	{
	    if(Yii::app()->user->getState('role') != 'admin')
	        $this->layout='main';
	    $model = EmployerOffices::model()->findByPk($id);
	    $imodel = Employers::model()->findByPk($model->employer_id);
	    $addressmodel = Addresses::model()->findByPk($model->address_id);
	    $now = new DateTime();
	    // Uncomment the following line if AJAX validation is needed
	    // $this->performAjaxValidation($model);
	    if(isset($_POST['Addresses']))
	    {
	        $addressmodel->attributes = $_POST['Addresses'];
	        $addressmodel->model_type = 'Office';
	        $addressmodel->model_id = $imodel->id;
	        $addressmodel->updated_at = $now->format('Y-m-d H:i:s');
	        $addressmodel->head_office = $_POST['Addresses']['head_office'];
	        if($addressmodel->save())
	        {
				 $model->address_id = $addressmodel->id;
	            $model->updated_at = $now->format('Y-m-d H:i:s');
	            if($model->save())
	                $this->redirect($this->createUrl('employerOffices/admin',array('id'=>$model->employer_id)));
	        }
	    }
	    if(Yii::app()->user->getState('role') != 'admin')
	        $this->render('createoffice',array(
	            'model'=>$model,'insmodel'=>$imodel,'addressmodel'=>$addressmodel
	        ));
	        else
	    $this->render('office',array(
	        'model'=>$model,'insmodel'=>$imodel,'addressmodel'=>$addressmodel
	    ));
	}
	
}
