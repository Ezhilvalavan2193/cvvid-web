 <?php
use Stripe\Charge;
use Stripe\Invoice;
use Stripe\InvoiceItem;
use Stripe\Stripe;
use Stripe\Token;
use Stripe\Customer;
use Stripe\Plan;
use Stripe\Subscription;
use Stripe\Coupon;
use Stripe\InvalidRequestError;
class SiteController extends Controller {

    /**
     * Declares class-based actions.
     */
    public $layout = '//layouts/main';
    public $pagename = '';
    //  public $is_premiun = 0;

    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        $this->_pageTitle = "Job Search | Cvvid.com";
        $this->_pageDescription = "Connect with employers and candidates. upload your video resume of job opportunities across top companies, Apply online. Post  video CV today";
        $this->_metaKeywords = "Job seeker | search jobs | search jobs by keywords";
        $this->actionHome();
      
        //      $this->actionLogin();
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the login page
     */
    public function actionLogin($target = "") {
        $this->_pageTitle = "Login | Cvvid.com";
        $this->_pageDescription = "Log in now and get access to a video resume at cvvid.com anywhere.";
        $this->_metaKeywords = "login to cvvid | emolyer login | candidate login | Institution login";
        $model = new LoginForm;
        $this->layout = "main";
        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()) {
                
                $now = new DateTime();
                
                $urs = Users::model()->findByPk(Yii::app()->user->getState('userid'));
                $urs->loggeddatetime = $now->format('Y-m-d H:i:s');
                $urs->save(FALSE);
                
                
                if ($target != "" && Yii::app()->user->getState('role') == "candidate") {
                    $this->redirect($this->createUrl($target));
                } else
                if (Yii::app()->user->getState('role') == "admin")
                    $this->redirect($this->createUrl('admin/dashboard'));
                else if (Yii::app()->user->getState('role') == "candidate")
                {
                    
                    
                    $pmodel = Profiles::model()->findByAttributes(array('owner_id'=>$urs->id));
                    if(Yii::app()->user->returnUrl != "/")
                        $this->redirect(Yii::app()->user->returnUrl);
                    else if(isset($_GET['redirect_url']))
                        $this->redirect($_GET['redirect_url']);
                    else    
                        $this->redirect($this->createUrl('users/edit', array('slug'=>$pmodel['slug'])));
                }
                else if (Yii::app()->user->getState('role') == "employer") {
                  
                    $empusrmodel = EmployerUsers::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
              if(Yii::app()->user->returnUrl != "/")
                        $this->redirect(Yii::app()->user->returnUrl);
//                     else if(isset($_GET['redirect_url']))
//                         $this->redirect($_GET['redirect_url']);
                    else   
                    {                        
                        $aemployermodel = AgencyEmployers::model()->findByAttributes(array('employer_id'=>$empusrmodel->employer_id));                        
                        if($aemployermodel != null)
                        {
                            $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $empusrmodel->employer_id, 'type' => 'employer'));
                            $this->redirect($this->createUrl('agencyEmployers/edit', array('slug' => $profilemodel->slug)));
                        }
                        else
                        {
                            $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $empusrmodel->employer_id, 'type' => 'employer'));
                            $this->redirect($this->createUrl('employers/edit', array('slug' => $profilemodel->slug)));
                        }
                   }
                } else if (Yii::app()->user->getState('role') == "institution_admin") {
                    // $this->redirect($this->createUrl('institutions/edit',array('id'=>Yii::app()->user->getState('userid'))));
                    $insmodel = Institutions::model()->findByAttributes(array('admin_id' => Yii::app()->user->getState('userid')));
                  //  if(isset($_GET['redirect_url']))
                  //      $this->redirect($_GET['redirect_url']);
                  //  else 
                        $this->redirect($this->createUrl('institutions/edit', array('id' => $insmodel->id)));
                } else if (Yii::app()->user->getState('role') == "teacher") {
                    // $this->redirect($this->createUrl('institutions/edit',array('id'=>Yii::app()->user->getState('userid'))));
                    // $insusrmodel = InstitutionUsers::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid')));
                    $this->redirect($this->createUrl('institutions/teacherdashboard', array('id' => Yii::app()->user->getState('userid'))));
                }
                else if (Yii::app()->user->getState('role') == "agency") {
                    $agencymodel = Agency::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
                  //  if(isset($_GET['redirect_url']))
                   //     $this->redirect($_GET['redirect_url']);
                  //  else 
                        $this->redirect($this->createUrl('agency/edit', array('id' => $agencymodel->id)));
                }
                else if (Yii::app()->user->getState('role') == "staff") {
                    $agencystaffmodel = AgencyStaff::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
                    $this->redirect($this->createUrl('agencyStaff/edit', array('id' => $agencystaffmodel->id)));
                }
            }
            //$this->redirect(Yii::app()->user->returnUrl);
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionPassword() {
        $this->_pageTitle = "Forgot password | Cvvid.com";
        $this->_pageDescription = "If you forget your password we will help to reset your password, we can help by sending you a link to reset it.";
        $this->_metaKeywords = "change password | forgot password | Reset password";
        
        $model = new ForgotPassword();
        $this->layout = "main";
        
        // collect user input data
        if (isset($_POST['ForgotPassword'])) {
            
            $email = $_POST['ForgotPassword']['email'];
            
            $count = Users::model()->countByAttributes(array('email' => $email));
            
            if ($count > 0) {
                
                $now = new DateTime();
                $token = $this->random(50);
                $passmodel = new PasswordResets();
                $passmodel->attributes = $_POST['ForgotPassword'];
                $passmodel->token = $token;
                $passmodel->created_at = $now->format('Y-m-d H:i:s');
                if ($passmodel->save()) {
                    
                    $this->notifyresetpassword($email, $token);
                    
                    Yii::app()->user->setFlash('success', 'Reset link sent to your mail.');
                    $this->redirect($this->createUrl('site/password'));
                }
            } else {
                Yii::app()->user->setFlash('error', "We can't find a user with that e-mail address.");
                $this->redirect($this->createUrl('site/password'));
            }
        }
        $this->render('password', array('model' => $model));
    }
    
    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        $this->layout = "user";
        Yii::app()->user->logout();
        $this->redirect(yii::app()->createurl("site/index"));
        //$this->redirect(Yii::app()->homeUrl);
    }

  public function actionHome() {
  
        Yii::app()->theme = 'home';
        $this->_pageTitle = "Job Search | Cvvid.com";
        $this->_pageDescription = "Connect with employers and candidates. upload your video resume of job opportunities across top companies, Apply online. Post  video CV today";
        $this->_metaKeywords = "Job seeker | search jobs | search jobs by keywords";
        $model = Slideshows::model()->findByAttributes(array('slug' => 'home'));
     
        $slidemodel = SlideshowSlides::model()->findAllByAttributes(array('slideshow_id' => $model->id, 'deleted_at' => null));
        
        $this->render('index', array('model' => $model, 'slidemodel' => $slidemodel));
 
        
    }

    public function actionabout() {
       
        $model = Slideshows::model()->findByAttributes(array('slug' => 'about-us'));
       $this->_pageTitle = "About cvvid | cvvid.com";
        $this->_pageDescription = "Your online CV profile. Make your CV stand out with a CVVID profile. Provide employers with an insight into your character and personality as well as ability with a CV video.We aim to provide all job seekers with the tools they need to source employment. Stand out! Simply- You, at your best";
        // $this->_pageTitle = $model->meta_title;
        // $this->_pageDescription = $model->meta_description;
        // $this->_metaKeywords = $model->meta_keywords;
        $slidemodel = SlideshowSlides::model()->findAllByAttributes(array('slideshow_id' => $model->id, 'deleted_at' => null));
        $this->render('about', array('model' => $model, 'slidemodel' => $slidemodel));
    }

    public function actiontoptips() {
       
        $model = Slideshows::model()->findByAttributes(array('slug' => 'top-tips'));
        $this->_pageTitle = "Career Guide | Cvvid.com";
        $this->_pageDescription = "Find the information about creating a video resume at cvvid.com. Select your profession and create your unique video CV and upload to showcase your profile.";
        $this->_metaKeywords = "career guide | video upload | profession video | showcase profile | career guide cvvid";
        // $this->_pageTitle = $model->meta_title;
        // $this->_pageDescription = $model->meta_description;
        // $this->_metaKeywords = $model->meta_keywords;
        $slidemodel = SlideshowSlides::model()->findAllByAttributes(array('slideshow_id' => $model->id, 'deleted_at' => null));
        $this->render('toptips', array('model' => $model, 'slidemodel' => $slidemodel));
    }

    public function actionhowto() {
      
        $model = VideoCategories::model()->findByAttributes(array('slug' => 'how-to'));
        $this->_pageTitle = "Create your own video CV | Cvvid.com";
        $this->_pageDescription = "Create, record and upload your video resume with your profession. cvvid.com gives some great tips on creating your video resume.";
        $this->_metaKeywords = "Create video cv | Profession video | create your video resume";
        $videomodel = Videos::model()->findAllByAttributes(array('category_id' => $model->id, 'deleted_at' => null));
        $this->render('howto', array('model' => $model, 'videomodel' => $videomodel));
    }

    public function actionworrylist() {
        
        $model = Slideshows::model()->findByAttributes(array('slug' => 'worry-list'));
        $this->_pageTitle = "Frequently Asked Questions | cvvid.com";
        $this->_pageDescription = "Registering for a CVVID account is completely free, even a Premium Account is free, but only for a limited time. This will allow you to search and apply for jobs and communicate directly with employers.";
        $this->_metaKeywords = "Faq | questions video resume";
        $slidemodel = SlideshowSlides::model()->findAllByAttributes(array('slideshow_id' => $model->id, 'deleted_at' => null));
        $this->render('worrylist', array('model' => $model, 'slidemodel' => $slidemodel));
    }

    public function actionvideoshowcase() {
        $model = VideoCategories::model()->findByAttributes(array('slug' => 'success-stories'));
         $this->_pageTitle = "UK's No.1 video cv and job search | cvvid.com";
        $this->_pageDescription = "Connect with employers and candidates. upload your video resume of job opportunities across top companies, Apply online. Post  video CV today";
        $this->_metaKeywords = "Showcase Your Profession | video resume | create video";
        $videomodel = Videos::model()->findAllByAttributes(array('category_id' => $model->id, 'deleted_at' => null));
        $this->render('videoshowcase', array('model' => $model, 'videomodel' => $videomodel));
    }

    public function actionjobsearch() {
        $this->_pageTitle = "Job Search | Cvvid.com";
        $this->_pageDescription = "search  jobs with your  career keywords ";
        $this->_metaKeywords = "Job search | search jobs | search jobs by keywords ";
        $model = Jobs::model();
        $this->render('jobsearch', array('model' => $model));
    }

    public function actiongetSkills() {
        $skillmodel = Skills::model()->findAllByAttributes(array('skill_category_id' => $_POST['id']));
        foreach ($skillmodel as $skill) {
            echo "<option  value='$skill[id]'>$skill[name]</option>";
        }
    }

    public function actioncontact() {
      
        $model = new Contact;
        if (isset($_POST['Contact'])) {
            $model->attributes = $_POST['Contact'];
            if (isset($_POST['g-recaptcha-response']) && $_POST['g-recaptcha-response'] == "")
                $model->setScenario('captcha');

            if ($model->validate()) {

               if($_POST['Contact']['topic'] == 1)
            		$topic = "Institution";
            	else if($_POST['Contact']['topic'] == 2)
            		$topic = "Employer";
        		else if($_POST['Contact']['topic'] == 3)
        		    $topic = "Candidate";
    		    else if($_POST['Contact']['topic'] == 4)
    		        $topic = "Recruitment Agency";
            	else 
            		$topic = "Others";
                
                Yii::import('application.extensions.phpmailer.JPhpMailer');
                $mail = new JPhpMailer;
                // $mail->IsSMTP();
                $mail->Host = 'mail.cvvid.com';
                //$mail->SMTPAuth = true;
                $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
                $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
                $mail->SMTPSecure = 'ssl';
                $mail->SetFrom('mail@cvvid.com', 'cvvid');
                $mail->Subject = 'Enquiry Details';
                $mail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
                
                $msg = "<h1><b>Enquiry Details</b></h1>";
                $msg .="<table>";
                $msg .="<tr><td>Forenames: </td><td>".$_POST['Contact']['forenames']."</td></tr>";
                $msg .="<tr><td>Surname: </td><td>".$_POST['Contact']['surname']."</td></tr>";
                $msg .="<tr><td>Email: </td><td>".$_POST['Contact']['email']."</td></tr>";
                $msg .="<tr><td>About: </td><td>".$topic."</td></tr>";
                $msg .="<tr><td>Message: </td><td>".$_POST['Contact']['message']."</td></tr>";
                $msg .="</table>";
                
                $mail->MsgHTML($msg);
                $mail->AddAddress('mail@cvvid.com', 'cvvid');
                if ($mail->Send()) {
                    
                    $rmail = new JPhpMailer;
                    // $mail->IsSMTP();
                    $rmail->Host = 'mail.cvvid.com';
                    //$mail->SMTPAuth = true;
                    $rmail->Username = Yii::app()->params['EMAIL_USERNAME'];
                    $rmail->Password = Yii::app()->params['EMAIL_PASSWORD'];
                    $rmail->SMTPSecure = 'ssl';
                    $rmail->SetFrom('mail@cvvid.com', 'cvvid');
                    $rmail->Subject = 'Thank you for contacting us';
                    $rmail->AltBody = 'To view the message, please use an HTML compatible email viewer!';
                    
                    $msg = "<h1><b>Thank you for contacting us, we will contact you shortly</b></h1>";
                    
                    $rmail->MsgHTML($msg);
                    $rmail->AddAddress($_POST['Contact']['email'], $_POST['Contact']['forenames']);                 
                    $rmail->Send();
                    
                    Yii::app()->user->setFlash('success', 'Thank you for your message. We will be in contact with you shortly.');
                    $this->redirect($this->createUrl('site/contact'));
                }
            }
        }
        $this->_pageTitle = "Contact Us | Cvvid.com";
        $this->_pageDescription = "contact us for any queries cvvid.com";
        $this->_metaKeywords = "contact us | contact cvvid | support cvvid";
        $this->render('contact', array('model' => $model));
    }
    
    public function actionemployers() {
        $this->_pageTitle = "CVVID FOR EMPLOYERS | cvvid.com";
        $this->_pageDescription = "CVVID is proving to be an enormous asset to employers, making recruitment more effective and more efficient. It can work for you too, saving you valuable time and money";
        $this->render('employers');
    }

     public function actionemployerfaqs() {
        $model = Slideshows::model()->findByAttributes(array('slug' => 'faqs'));
          $this->_pageTitle = "Employers FAQ | Cvvid.com";
        $this->_pageDescription = "Frequently asked questions about Employee premium membership on cvvid.com";
        $this->_metaKeywords = "Premium Faq | Premium employer | frequently asked questions";
        $slidemodel = SlideshowSlides::model()->findAllByAttributes(array('slideshow_id' => $model->id, 'deleted_at' => null));
        $this->render('employer-faqs', array('model' => $model, 'slidemodel' => $slidemodel));
//        $this->render('employer-faqs');
    }

    public function actioncandidatesearch() {
        $model = Jobs::model();
        $this->render('candidatesearch', array('model' => $model));
    }

 
    public function actionsearchCandidates() {
        
        $name = isset($_GET['name']) ? $_GET['name'] : "";
        $skillcategid = $_GET['skill_category'];
        $skillarr = isset($_GET['skills']) ? $_GET['skills'] : array();
        $location = isset($_GET['location']) ? $_GET['location'] : "";
        $lat = isset($_GET['latitude']) ? $_GET['latitude'] : "";
        $long = isset($_GET['longitude']) ? $_GET['longitude'] : "";
        $radius = isset($_GET['radius']) ? $_GET['radius'] : 0;
        $hasvideo = isset($_GET['video']) ? 1 : 0;

        $skillids = implode(',', $skillarr);

        $join = "inner join cv16_users u on u.id = p.owner_id
                      left outer join cv16_profile_skills ps on ps.profile_id = p.id
                      left outer join cv16_skills s on s.id = ps.skill_id
                      left outer join cv16_skill_categories sc on sc.id = s.skill_category_id
                      left outer join cv16_profile_videos pv on pv.profile_id = p.id";
        $cri = new CDbCriteria();
        $cri->alias = "p";
        $cri->distinct = true;
//         $cri->join = "inner join cv16_users u on u.id = p.owner_id
//                       left outer join cv16_profile_skills ps on ps.profile_id = p.id
//                       left outer join cv16_skills s on s.id = ps.skill_id
//                       left outer join cv16_skill_categories sc on sc.id = s.skill_category_id
//                       left outer join cv16_profile_videos pv on pv.profile_id = p.id";

        
        
       
            $empusrmodel = EmployerUsers::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid')));
            $aemployermodel = AgencyEmployers::model()->findByAttributes(array('employer_id'=>$empusrmodel->employer_id));
            if($aemployermodel != null)
            {     
                $join .= " inner join cv16_agency_candidates ac on ac.user_id = u.id";
            }
        $cri->join = $join;
        
        if ($skillcategid > 0)
            $cri->addCondition("s.skill_category_id =" . $skillcategid);
        if ($skillids != "")
            $cri->addCondition('ps.skill_id in (' . $skillids . ')', 'AND');
        if ($location != "" && $lat != "" ) {
            if ($radius > 0) {
                $cri->select = "*,(3959 * acos( cos( radians(".$lat.") ) * cos( radians(location_lat) ) *
                                cos(radians(location_lng) - radians(".$long.")) + sin(radians(".$lat.")) *
                                sin(radians(location_lat)) )) as distance";
                //                 $cri->addCondition("HAVING distance < " . $radius);
                $cri->having = "distance < " . $radius;
            } 
//              else {
// //                $cri->addCondition("");
//                 $cri->addCondition("u.location_lat =".$lat." and u.location_lng=".$long);
//              }
        }

        if($hasvideo > 0)
            $cri->addCondition("pv.video_id > 0");
            
        $cri->addCondition("p.visibility = 1 and p.type like '%candidate%' and u.deleted_at is null");
        if(trim($name) != "")
            $cri->addCondition("u.forenames like '%".$name."%' or u.surname like '%".$name."%'");
      
            $cri->addCondition("(DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(u.dob, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(u.dob, '00-%m-%d'))) > 16");
        $cri->group = "p.id";
        $order = "";
//          if(isset($_GET['sort']))
//         {
//             if($_GET['sort'] > 0)
//                 $cri->order = "u.forenames asc";
//             else 
//                 $cri->order = "u.forenames desc";
//         }
//         if(isset($_GET['has_video']))
//         {
//             if($_GET['has_video'] > 0)
//                 $order = $order.($order != "" ? ",pv.video_id desc" : "pv.video_id desc");
//             else
//                 $order = $order.($order != "" ? ",pv.video_id asc" : "pv.video_id asc");
//         }

            $cri->order = "u.created_at desc";
            if(isset($_GET['a_z']))
            {
                if($_GET['a_z'] == 1)
                    $order = "u.forenames asc";
            }
            else if(isset($_GET['z_a']))
            {
                
                if($_GET['z_a'] == 1)
                    $order = "u.forenames desc";
                    
            }
            else if(isset($_GET['recent']))
            {
                if($_GET['recent'] == 1)
                    $order = "u.created_at desc";
                    
            }
            else if(isset($_GET['old']))
            {
                if($_GET['old'] == 1)
                    $order = "u.created_at asc";
            }
            else if(isset($_GET['video']))
            {
                if($_GET['video'] == 1)
                    $order = "pv.video_id desc";
            }
            else if(isset($_GET['novideo']))
            {
                if($_GET['novideo'] == 1)
                    $order = "pv.video_id asc";
            }
            
            

        if($order != "")
        {
            $cri->order = $order;
        }
        
        
        if($location != "")
        {
            $loc = urldecode($location);
            $locarr = explode(",", $loc);
            
            //             $cri1 = new CDbCriteria();
            //             $cri1->condition = "location = '".urldecode($loc)."'";
            //             $joblocmodel = Jobs::model()->find($cri1);
           // $location = $loc;//$joblocmodel['location'];
            //             $firstloc = explode(",", urldecode($loc))[0];
            if($loc == "United-Kingdom" || $loc == "UK")
                $cri->addCondition("TRIM(substring_index(u.location, ',', -1)) = 'UK' OR TRIM(substring_index(u.location, ',', -1)) like '%United Kingdom%'");
            else if($loc == "United-States" || $loc == "US")
                $cri->addCondition("TRIM(substring_index(u.location, ',', -1)) = 'US' OR TRIM(substring_index(u.location, ',', -1)) like '%United States%'");
            else
                $cri->addCondition("u.location like '%".$locarr[0]."%'");
                              
                   
                        //  $cri->addCondition("substring_index(j.location, ',', -1) like '%".$visitor_country."%' OR substring_index(j.location, ',', -1) = '".$visitor_country_code."' OR  substring_index(j.location, ',', 1) like '%.$visitor_region.%' OR  substring_index(j.location, ',', 1) like '%.$visitor_city.%'");
                       
                        //             if($location != "")
                            //                 $cri->addCondition("LOWER(replace(LEFT(location,LOCATE(' ',j.location) - 2), ' ','')) like '%".$location."%'");
                            //                 else
                                //                     $cri->addCondition("substring_index(j.location, ',', -1) like '%".$visitor_country."%'");
                            
           //  $pmodel = Profiles::model()->findAll($cri);
                            
        }
        else
        {
            $visitor_country = $this->ip_info("Visitor", "Country");
            $visitor_country_code = $this->ip_info("Visitor", "Countrycode");
            $visitor_region = $this->ip_info("Visitor", "Region");
            $visitor_city = $this->ip_info("Visitor", "City");
            
            $cri->addCondition("TRIM(substring_index(u.location, ',', -1)) like '%".$visitor_country."%' OR TRIM(substring_index(u.location, ',', -1)) = '".$visitor_country_code."' OR  TRIM(substring_index(u.location, ',', 1)) like '%.$visitor_region.%' OR  TRIM(substring_index(u.location, ',', 1)) like '%.$visitor_city.%'");
//             $visitor_country = $this->ip_info("Visitor", "Country");            
//             $cri->addCondition("substring_index(u.location, ',', -1) like '%".$visitor_country."%'");
            
        }
        
     //  echo var_dump($cri);
    //  die();
        $pmodel = Profiles::model()->findAll($cri);
        
        if(count($pmodel) <= 0)
            $pmodel = array();
        
        $this->render('candidatesearchresults', array('model' => $pmodel));
    }

    public function actionaddfavourite() {
        $empusrmodel = EmployerUsers::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
        $pmodel = Profiles::model()->findByAttributes(array('owner_id' => $empusrmodel->employer_id,'type'=>'employer'));
        $pfmodel = ProfileFavourites::model()->findByAttributes(array('profile_id' => $pmodel->id, 'favourite_id' => $_POST['fid']));
        if ($pfmodel == null) {
            $pfmodel = new ProfileFavourites();
            $pfmodel->profile_id = $pmodel->id;
            $pfmodel->favourite_id = $_POST['fid'];
            
            $canprofmodel = Profiles::model()->findByPk($_POST['fid']);
            
            if($pfmodel->save())
            {
                $empmodel = Employers::model()->findByPk($empusrmodel->employer_id);
                
                $subject = "An employer favourited your profile";
                $msg = "Your profile has been favourited by employer <a href=".$this->createUrl('employers/employer',array('slug'=>$pmodel->slug))."> ".$empmodel->name." </a>";
                
                    $now = new DateTime();
                    $conmodel = new Conversations();
                    $conmodel->subject = $subject;
                    $conmodel->created_at = $now->format('Y-m-d H:i:s');
                    $conmodel->updated_at = $now->format('Y-m-d H:i:s');
                    if ($conmodel->save()) {
                        $usermodel = Users::model()->findByPk(Yii::app()->user->getState('userid'));
                        $conmsgmodel = new ConversationMessages();
                        $conmsgmodel->conversation_id = $conmodel->id;
                        $conmsgmodel->user_id = Yii::app()->user->getState('userid');
                        $conmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
                        $conmsgmodel->message = $msg;
                        $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
                        $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
                        if ($conmsgmodel->save()) {
                            $conmemmodel_sender = new ConversationMembers();
                            $conmemmodel_sender->conversation_id = $conmodel->id;
                            $conmemmodel_sender->user_id = Yii::app()->user->getState('userid');
                            $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
                            $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
                            $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                            $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                            if ($conmemmodel_sender->save()) {
                                $usermodel1 = Users::model()->findByPk($canprofmodel->owner_id);
                                $conmemmodel_rec = new ConversationMembers();
                                $conmemmodel_rec->conversation_id = $conmodel->id;
                                $conmemmodel_rec->user_id = $canprofmodel->owner_id;
                                $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
                                $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                                $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                                if ($conmemmodel_rec->save()) {
                                    $actmodel_sender = new Activities();
                                    $actmodel_sender->user_id = Yii::app()->user->getState('userid');
                                    $actmodel_sender->type = "message";
                                    $actmodel_sender->model_id = $conmsgmodel->id;
                                    $actmodel_sender->model_type = "ConversationMessage";
                                    $actmodel_sender->subject = "You sent a message in conversation: " . $subject;
                                    $actmodel_sender->message = $msg;
                                    $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                                    $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                                    if ($actmodel_sender->save()) {
                                        $actmodel_rec = new Activities();
                                        $actmodel_rec->user_id = $canprofmodel->owner_id;
                                        $actmodel_rec->type = "message";
                                        $actmodel_rec->model_id = $conmsgmodel->id;
                                        $actmodel_rec->model_type = "ConversationMessage";
                                        $actmodel_rec->subject = "You received a message in conversation: " . $subject;
                                        $actmodel_rec->message = $msg;
                                        $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                                        $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                                        $actmodel_rec->save();
                                    }
                                }
                            }
                        }
                    }
            }
            
        }else{
            ProfileFavourites::model()->deleteAllByAttributes(array('profile_id' => $pmodel->id, 'favourite_id' => $_POST['fid']));
        }
    }

    public function actionsendMessage() {
        $now = new DateTime();
        $conmodel = new Conversations();
        $conmodel->subject = $_POST['subject'];
        $conmodel->created_at = $now->format('Y-m-d H:i:s');
        $conmodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($conmodel->save()) {
            $usermodel = Users::model()->findByPk(Yii::app()->user->getState('userid'));
            $conmsgmodel = new ConversationMessages();
            $conmsgmodel->conversation_id = $conmodel->id;
            $conmsgmodel->user_id = Yii::app()->user->getState('userid');
            $conmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
            $conmsgmodel->message = $_POST['message'];
            $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
            $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($conmsgmodel->save()) {
                $conmemmodel_sender = new ConversationMembers();
                $conmemmodel_sender->conversation_id = $conmodel->id;
                $conmemmodel_sender->user_id = Yii::app()->user->getState('userid');
                $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
                $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
                $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                if ($conmemmodel_sender->save()) {
                    $usermodel1 = Users::model()->findByPk($_POST['user_id']);
                    $conmemmodel_rec = new ConversationMembers();
                    $conmemmodel_rec->conversation_id = $conmodel->id;
                    $conmemmodel_rec->user_id = $_POST['user_id'];
                    $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
                    $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                    $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                    if ($conmemmodel_rec->save()) {
                        $actmodel_sender = new Activities();
                        $actmodel_sender->user_id = Yii::app()->user->getState('userid');
                        $actmodel_sender->type = "message";
                        $actmodel_sender->model_id = $conmsgmodel->id;
                        $actmodel_sender->model_type = "ConversationMessage";
                        $actmodel_sender->subject = "You sent a message in conversation: " . $_POST['subject'];
                        $actmodel_sender->message = $_POST['message'];
                        $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                        $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                        if ($actmodel_sender->save()) {
                            $actmodel_rec = new Activities();
                            $actmodel_rec->user_id = $_POST['user_id'];
                            $actmodel_rec->type = "message";
                            $actmodel_rec->model_id = $conmsgmodel->id;
                            $actmodel_rec->model_type = "ConversationMessage";
                            $actmodel_rec->subject = "You received a message in conversation: " . $_POST['subject'];
                            $actmodel_rec->message = $_POST['message'];
                            $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                            $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                            $actmodel_rec->save();
                        }
                    }
                }
            }
        }
    }

    //        /ezhil coding
    public function actioncandidatememberships() {
        $this->_pageTitle = "Candidate Membership | Cvvid.com";
        $this->_pageDescription = "Cvvid provides stunnings job search options for every candidate. Search your jobs with basic and premium registration.";
        $this->_metaKeywords = "candidate membership | basic membership | premium membership | premium candidate";
        $model = new Users();
        $this->render('candidatememberships', array('model' => $model));
    }

    public function actionemployermemberships() {
        $this->_pageTitle = "Employer Membership | Cvvid.com";
        $this->_pageDescription = "Cvvid provides stunning recruiting options for every employer. Post your jobs with basic and premium registration.";
        $this->_metaKeywords = "Employer membership , basic membership, premium membership, Premium employer";
        $model = new Employers();
        $this->render('employermemberships', array('model' => $model));
    }

    public function actionbasicregisteremployer() {
        $model = new Employers();
        $addressmodel = new Addresses();
        $usermodel = new Users();
        $profilemodel = new Profiles();
        // $this->is_premiun = 0;
        $this->_pageTitle = "Employer Registration | Cvvid.com";
        $this->_pageDescription = "New employer registration on cvvid.com to get the video resumes for the recruitment process using our cvvid.com.";
        $this->_metaKeywords = "Employer registration | basic membership | basic employee registration";
        $this->render('registeremployerbasic', array('model' => $model, 'addressmodel' => $addressmodel, 'usermodel' => $usermodel,'profilemodel'=>$profilemodel));
        
    }

    public function actionpremiumregisteremployer() {
        $model = new Employers();
        $addressmodel = new Addresses();
        $usermodel = new Users();
        $profilemodel = new Profiles();
        // $this->is_premiun = 1;
        $this->_pageTitle = "Premium Employer | Cvvid.com";
        $this->_pageDescription = "Premium employer registration on cvvid.com to get the highlighted video resumes and access unlimited candidate profiles.";
        $this->_metaKeywords = "Premium Employer registration | Premium registration | premium employee registration";
        $this->render('registeremployerpremium', array('model' => $model, 'addressmodel' => $addressmodel, 'usermodel' => $usermodel,'profilemodel'=>$profilemodel));
    }

    public function actionbasicregistercandidate() {
        $model = new Users();
        $addressmodel = new Addresses();
        $profilemodel = new Profiles();
        //  $this->is_premiun = 0;
        $this->_pageTitle = "Job Seeker | Registration";
        $this->_pageDescription = "New job seeker registration on cvvid.com to upload the video resumes for searching jobs using our cvvid.com.";
        $this->_metaKeywords = "Job seeker | search jobs | search jobs by keywords";
        $this->render('registercandidatebasic', array('model' => $model, 'addressmodel' => $addressmodel, 'profilemodel' => $profilemodel));
    }

    public function actionpremiumregistercandidate() {
        $model = new Users();
        $addressmodel = new Addresses();
        $profilemodel = new Profiles();
        // $this->is_premiun = 1;
        $this->_pageTitle = "Premium Job Seeker | Cvvid.com";
        $this->_pageDescription = "Premium job seeker registration on cvvid.com to get the change their career in a top concern.";
        $this->_metaKeywords = "Premium job seeker | unlimited companies search";
        $this->render('registercandidatepremium', array('model' => $model, 'addressmodel' => $addressmodel, 'profilemodel' => $profilemodel));
    }

    public function actionregisterinstitution() {
        $model = new Institutions();
        $addressmodel = new Addresses();
        $usermodel = new Users();
        $this->render('registerinstitution', array('model' => $model, 'addressmodel' => $addressmodel, 'usermodel' => $usermodel));
    }
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actioncreateEmployerDetails() {
        
        $model = new Employers;
        $empusermodel = new EmployerUsers;
        $usermodel = new Users;
        $profilemodel = new Profiles;
        $addressmodel = new Addresses;
        
        $now = new DateTime();
        if (isset($_POST['g-recaptcha-response']) && $_POST['g-recaptcha-response'] == "")
            $model->setScenario('captcha');
            
            if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
                $usermodel->setScenario('confirm');
                
                if(isset($_POST['Users'])){
                    $usermodel->attributes=$_POST['Users'];
                }
                if(isset($_POST['Employers'])){
                    $model->attributes=$_POST['Employers'];
                }
                if(isset ($_POST["Addresses"])){
                    $addressmodel->attributes = $_POST["Addresses"];
                }
                if(isset ($_POST["Profiles"])){
                    $profilemodel->attributes = $_POST["Profiles"];
                }
                if(isset($_POST['Users']))
                {
                    $valid = $usermodel->validate();
                    $valid = $model->validate() && $valid;
                    $valid = $addressmodel->validate() && $valid;
                    $valid = $profilemodel->validate() && $valid;
                    
                    if($valid){
                        $usermodel->created_at = $now->format('Y-m-d H:i:s');
                        $usermodel->type = 'employer';
                        $usermodel->location = $_POST['Addresses']['county'].($_POST['Addresses']['county']!=''? ', '.$_POST['Addresses']['country']: '');
                        $usermodel->location_lat = $_POST['Addresses']['latitude'];
                        $usermodel->location_lng = $_POST['Addresses']['longitude'];
                        $usermodel->tel = $_POST['Employers']['tel'];
                        $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
                        $usermodel->api_key = md5(uniqid(rand(), true));
                        if($usermodel->save())
                        {
                        // insert employer details
                        $model->email = $usermodel->email;
                        
                        $model->basic_user_count = 1;
                        
                        $model->created_at = $now->format('Y-m-d H:i:s');
                        $model->user_id = $usermodel->id;
                        if($model->save())
                        {
                            //employer user update
                            $empusermodel->employer_id = $model->id;
                            $empusermodel->user_id = $usermodel->id;
                            $empusermodel->role = 'admin';
                            $empusermodel->created_at = $now->format('Y-m-d H:i:s');
                            $empusermodel->save();
                        }
                        $profilemodel->owner_id = $model->id;
                        $profilemodel->owner_type = 'employer';
                        if($_POST['industry_id'] > 0)
                            $profilemodel->industry_id = $_POST['industry_id'];
                            $profilemodel->type = 'employer';
                            $profilemodel->slug = $_POST["Profiles"]['slug'];
                            $profilemodel->save();
                            //address update
                            $addressmodel->created_at = $now->format('Y-m-d H:i:s');
                            $addressmodel->model_type = 'employer';
                            $addressmodel->model_id = $model->id;
                            if ($addressmodel->save()) {
                                
                                $umodel = Users::model()->findByPk($model->user_id);
                                
                                $this->notifyemployer($model, $planname = "");
                                
                                $logmodel = new LoginForm;
                                
                                $logmodel->username = $umodel->email;
                                $logmodel->password = $_POST['Users']['password'];
                                
                                if ($logmodel->validate() && $logmodel->login()) {
                                    
                                    $this->redirect($this->createUrl('employers/edit', array('slug' => $profilemodel->slug)));
                                }
                            }
                        }  
                    }
                }
                
                //   $this->is_premiun = 0;
                $this->render('registeremployerbasic', array('model' => $model, 'addressmodel' => $addressmodel, 'usermodel' => $usermodel,'profilemodel'=>$profilemodel));
                // $this->render('registeremployerbasic', array('model' => $model, 'addressmodel' => $addressmodel, 'usermodel' => $usermodel));
                
    }
    
    public function actionvalidatecoupon()
    {
        $answer = FALSE;
        $coupon_id  = $_GET['coupon_id'];
        $amount = $_GET['amount'];
      
        
        require Yii::app()->basePath . '/extensions/stripe/init.php';
        Stripe::setApiKey(Yii::app()->params['SECRET_Key']);
        
        
        // needs if coupon_id is not blank
        try {
        $coupon = Coupon::retrieve($coupon_id);
        
        if ($coupon->valid) {
            if($coupon->percent_off == null || $coupon->percent_off == "")
            {
                $deduct_amount = $coupon->amount_off / 100;
                $amount = $amount - $deduct_amount;
            }
            else
            {
                $deduct_amount = $amount * ($coupon->percent_off / 100);
                $amount = $amount - $deduct_amount;
            }
            echo json_encode(array("status" => TRUE ,"discount"=>($coupon->percent_off),"amount"=>$amount));
        }else{
            echo json_encode(array("status" => FALSE ,"discount"=> 0,"amount"=>$amount));
        }
        //$couponid = $coupon->id;
        } catch (\Stripe\Error\InvalidRequest $e) {
            // $answer is already set to false
            echo json_encode((array("status" => FALSE ,"discount" => 0)));
            // echo $answer = FALSE;
        }
        
        
        
      /*   try {
            $coupon = Coupon::retrieve($coupon_id);

        
            if ($coupon->valid) {
                echo json_encode(array("status" => TRUE ,"discount"=>($coupon->percent_off)));
            }else{
                echo json_encode(array("status" => FALSE ,"discount"=> 0));
            }
            //$couponid = $coupon->id;
        } catch (\Stripe\Error\InvalidRequest $e) {
            // $answer is already set to false
            echo json_encode((array("status" => FALSE ,"discount" => 0)));
            // echo $answer = FALSE;
        } */
        
        //            if ($coupon->valid) {
        //                $answer = "11";
        //            }
        //            echo $answer;  //this sends 0 or 1
    }
   /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
   /* public function actioncreateEmployerPremium() {
        
        $model = new Employers;
        $empusermodel = new EmployerUsers;
        $usermodel = new Users;
        $profilemodel = new Profiles;
        $addressmodel = new Addresses;
        
        $now = new DateTime();
        if (isset($_POST['g-recaptcha-response']) && $_POST['g-recaptcha-response'] == "")
            $model->setScenario('captcha');
            
            if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
                $usermodel->setScenario('confirm');
                
                if(isset($_POST['Users'])){
                    $usermodel->attributes=$_POST['Users'];
                }
                if(isset($_POST['Employers'])){
                    $model->attributes=$_POST['Employers'];
                }
                if(isset ($_POST["Addresses"])){
                    $addressmodel->attributes = $_POST["Addresses"];
                }
                if(isset ($_POST["Profiles"])){
                    $profilemodel->attributes = $_POST["Profiles"];
                }
             
                    $valid = $usermodel->validate();
                    $valid = $model->validate() && $valid;
                    $valid = $addressmodel->validate() && $valid;
                    $valid = $profilemodel->validate() && $valid;
                    
                    if($valid){
                        $usermodel->created_at = $now->format('Y-m-d H:i:s');
                        $usermodel->type = 'employer';
                        $usermodel->location = $_POST['Addresses']['county'].($_POST['Addresses']['county']!=''? ', '.$_POST['Addresses']['country']: '');
                        $usermodel->location_lat = $_POST['Addresses']['latitude'];
                        $usermodel->location_lng = $_POST['Addresses']['longitude'];
                        $usermodel->tel = $_POST['Employers']['tel'];
                        $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
                        $usermodel->api_key = md5(uniqid(rand(), true));
                        $usermodel->save();
                      
                        $model->email = $usermodel->email;
                        
                        $model->premium_user_count = 2;
                        
                        // Check if using free trial then set membership trial date
                        $model->subscription_ends_at = Carbon::now()->addDay()->toDateTimeString();
                        $model->created_at = $now->format('Y-m-d H:i:s');
                        $model->user_id = $usermodel->id;
                        if($model->save()){
                            //employer user update
                            $empusermodel->employer_id = $model->id;
                            $empusermodel->user_id = $usermodel->id;
                            $empusermodel->role = 'admin';
                            $empusermodel->created_at = $now->format('Y-m-d H:i:s');
                            $empusermodel->save();
                        }
                        $profilemodel->owner_id = $model->id;
                        $profilemodel->owner_type = 'employer';
                        $profilemodel->type = 'employer';
                        if($_POST['industry_id'] > 0)
                            $profilemodel->industry_id = $_POST['industry_id'];
                            $profilemodel->slug = $_POST["Profiles"]['slug'];
                            $profilemodel->save();
                            
                            //address update
                            $addressmodel->attributes = $_POST["Addresses"];
                            $addressmodel->created_at = $now->format('Y-m-d H:i:s');
                            $addressmodel->model_type = 'employer';
                            $addressmodel->model_id = $model->id;
                            if ($addressmodel->save()) {
                                //subscription code
                                $now = new DateTime();
                                // $empusrmodel = EmployerUsers::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
                                $model = Employers::model()->findByPk($empusermodel->employer_id);
                                $usermodel = Users::model()->findByPk($empusermodel->user_id);
                                
                                require Yii::app()->basePath . '/extensions/stripe/init.php';
                                Stripe::setApiKey(Yii::app()->params['SECRET_Key']);
                                
                                $plan = $_POST['subscription_plan'];
                                $token = $_POST['stripeToken'];
                                $amount = $_POST['amount'];
                                // $trial_ends = Carbon::now()->addYear()->toDateTimeString();
                                
                                $planarr = Plan::retrieve($plan);
                                
//                                $amount = $planarr->amount;
                                $planname = $planarr->name;
                                $currency = $planarr->currency;
                                                               
                         
                                //        create subscriber
                                $customer = Customer::create(array(
                                    'source'   => $token,
                                    'email'    => $model->email,
                                    'description' => $model->name,
                                ));
                                
//                                $charge = Charge::create(array(
//                                    "amount" => $amount * 100,
//                                    "currency" => $currency,
//                                    "customer" => $customer->id,
//                                    "receipt_email" => $model->email,
//                                ));
                                
                                $coupon = $_POST['coupon_id'];
                                $validcoupon = $_POST['validcoupon'];
                                
                                if($validcoupon == "1")
                                {
                                    try {
                                        $coupon = Coupon::retrieve($coupon);
                                        $couponid = $coupon->id;
                                        
                                         $subscription = \Stripe\Subscription::create([
                                            "customer" => $customer->id,
                                            "items" => array(
                                                array(
                                                        "plan" => $plan,
                                                        'quantity' => $_POST['vacancy_count'],
                                                ),
                                            ),
                                            'tax_percent' => Yii::app()->params['TAX_PERCENT'],
                                            'coupon' => $couponid,
                                        ]);
                                         
                                    } catch (\Stripe\Error\InvalidRequest $e) {
                                        
                                    }
                                }else{
                                    $subscription = \Stripe\Subscription::create([
                                            "customer" => $customer->id,
                                            "items" => array(
                                                array(
                                                        "plan" => $plan,
                                                        'quantity' => $_POST['vacancy_count'],
                                                ),
                                            ),
                                           'tax_percent' => Yii::app()->params['TAX_PERCENT'],
                                      ]);
                                }
                                
                               
                                $model->stripe_active = 1;
                                if(isset($_POST['vacancy_count']) && $_POST['vacancy_count'] > 0)   
                                {
                                   $model->vacancy_count = $_POST['vacancy_count'];
                                   $model->current_vacancy_count = $_POST['vacancy_count'];
                                }
                                $model->stripe_id = $customer->id;
                                $model->stripe_subscription = $subscription->id;
                                $model->stripe_plan = $plan;
                                $model->last_four = $_POST['cvc'];
                                //$model->trial_ends_at = $trial_ends;
                                $model->updated_at = $now->format('Y-m-d H:i:s');
                                $model->save();
                                
                                //subscription code
                                $umodel = Users::model()->findByPk($model->user_id);
                                
                                $logmodel = new LoginForm;
                                
                                $this->notifyemployer($model, $planname = "");
                                
                                $logmodel->username = $umodel->email;
                                $logmodel->password = $_POST['Users']['password'];
                                                                
//                                invoice send
                                $this->notifyemployerupgrade($model, $customer->id);
                                
                                $subscriber = Employers::model()->findByPk($model->id);
                                
                                $invoiceitemlist = Invoice::all(array("customer" => $subscriber->stripe_id));
                                foreach ($invoiceitemlist->autoPagingIterator() as $invoice) {
                                     $link = $invoice['invoice_pdf'];
                                }
                                
                                $message = "Successfully updated your membership <a href='$link'>Click here to download your invoice</a>";
                                
                               $this->sendMessageE("Successfully updated your membership",$message, 57, $model->user_id);
                                                                
                                if ($logmodel->validate() && $logmodel->login()) {
                                    $cri = new CDbCriteria();
                                    $cri->condition = "owner_id =".$model->id." and type like '%employer%'";
                                    $profilemodel = Profiles::model()->find($cri);
                                   
                                    $this->redirect($this->createUrl('employers/edit', array('slug' => $profilemodel->slug)));
                                }
                            }
                    }
                    $this->render('registeremployerpremium', array('model' => $model, 'addressmodel' => $addressmodel, 'usermodel' => $usermodel,'profilemodel'=>$profilemodel));
                
                //$this->render('registeremployerpremium', array('model' => $model, 'addressmodel' => $addressmodel, 'usermodel' => $usermodel));
                
    }*/
 public function actioncreateEmployerPremium() {
        
        $model = new Employers;
        $empusermodel = new EmployerUsers;
        $usermodel = new Users;
        $profilemodel = new Profiles;
        $addressmodel = new Addresses;
        
        $now = new DateTime();
        if (isset($_POST['g-recaptcha-response']) && $_POST['g-recaptcha-response'] == "")
            $model->setScenario('captcha');
            
            if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
                $usermodel->setScenario('confirm');
                
                if(isset($_POST['Users'])){
                    $usermodel->attributes=$_POST['Users'];
                }
                if(isset($_POST['Employers'])){
                    $model->attributes=$_POST['Employers'];
                }
                if(isset ($_POST["Addresses"])){
                    $addressmodel->attributes = $_POST["Addresses"];
                }
                if(isset ($_POST["Profiles"])){
                    $profilemodel->attributes = $_POST["Profiles"];
                }
             
                    $valid = $usermodel->validate();
                    $valid = $model->validate() && $valid;
                    $valid = $addressmodel->validate() && $valid;
                    $valid = $profilemodel->validate() && $valid;
                    
                    if($valid){
                        $usermodel->created_at = $now->format('Y-m-d H:i:s');
                        $usermodel->type = 'employer';
                        $usermodel->location = $_POST['Addresses']['county'].($_POST['Addresses']['county']!=''? ', '.$_POST['Addresses']['country']: '');
                        $usermodel->location_lat = $_POST['Addresses']['latitude'];
                        $usermodel->location_lng = $_POST['Addresses']['longitude'];
                        $usermodel->tel = $_POST['Employers']['tel'];
                        $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
                        $usermodel->api_key = md5(uniqid(rand(), true));
                        $usermodel->save();
                      
                        $model->email = $usermodel->email;
                        
                        $model->premium_user_count = 2;
                        
                        // Check if using free trial then set membership trial date
                        $model->subscription_ends_at = Carbon::now()->addDay()->toDateTimeString();
                        $model->created_at = $now->format('Y-m-d H:i:s');
                        $model->user_id = $usermodel->id;
                        if($model->save()){
                            //employer user update
                            $empusermodel->employer_id = $model->id;
                            $empusermodel->user_id = $usermodel->id;
                            $empusermodel->role = 'admin';
                            $empusermodel->created_at = $now->format('Y-m-d H:i:s');
                            $empusermodel->save();
                        }
                        $profilemodel->owner_id = $model->id;
                        $profilemodel->owner_type = 'employer';
                        $profilemodel->type = 'employer';
                        if($_POST['industry_id'] > 0)
                            $profilemodel->industry_id = $_POST['industry_id'];
                            $profilemodel->slug = $_POST["Profiles"]['slug'];
                            $profilemodel->save();
                            
                            //address update
                            $addressmodel->attributes = $_POST["Addresses"];
                            $addressmodel->created_at = $now->format('Y-m-d H:i:s');
                            $addressmodel->model_type = 'employer';
                            $addressmodel->model_id = $model->id;
                            if ($addressmodel->save()) {
                                //subscription code
                                $now = new DateTime();
                                // $empusrmodel = EmployerUsers::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
                                $model = Employers::model()->findByPk($empusermodel->employer_id);
                                $usermodel = Users::model()->findByPk($empusermodel->user_id);
                                
                                require Yii::app()->basePath . '/extensions/stripe/init.php';
                                Stripe::setApiKey(Yii::app()->params['SECRET_Key']);
                                
                                $plan = $_POST['subscription_plan'];
                                $token = $_POST['stripeToken'];
                                $amount = $_POST['amount'];
                                // $trial_ends = Carbon::now()->addYear()->toDateTimeString();
                                
                                $planarr = Plan::retrieve($plan);
                                
//                                $amount = $planarr->amount;
                                $planname = $planarr->name;
                                $currency = $planarr->currency;
                                                               
                           //        create subscriber
                                $customer = Customer::create(array(
                                    'source'   => $token,
                                    'email'    => $model->email,
                                    'description' => $model->name,
                                ));
                                
//                                $charge = Charge::create(array(
//                                    "amount" => $amount * 100,
//                                    "currency" => $currency,
//                                    "customer" => $customer->id,
//                                    "receipt_email" => $model->email,
//                                ));
                                
                                $coupon = $_POST['coupon_id'];
                                $validcoupon = $_POST['validcoupon'];
                                
//                                 $charge = Charge::create(array(
                                    
//                                     "amount" => $amount*100 , // amount in cents, again
//                                     "currency" => "gbp",
//                                     "customer" => $customer->id,
//                                     "description" => "vacancy count",
//                                     'receipt_email' => $model->email,
//                                 ));
                                
                               // if( $planarr->id == "employer_annual" || $planarr->id == "employer_annual_others")
                               // {
                                  if($validcoupon == "1")
                                  {
                                    
                                    
                                    
                                     try {
                                        $coupon = Coupon::retrieve($coupon);
                                        $couponid = $coupon->id;
                                        
                                         $subscription = \Stripe\Subscription::create([
                                            "customer" => $customer->id,
                                            "items" => array(
                                                array(
                                                        "plan" => $plan,
                                                        'quantity' => $_POST['vacancy_count'],
                                                ),
                                            ),
                                            'tax_percent' => Yii::app()->params['TAX_PERCENT'],
                                            'coupon' => $couponid,
                                        ]);
                                         
                                    } catch (\Stripe\Error\InvalidRequest $e) {
                                        
                                    } 
                                }else{
                                    $subscription = \Stripe\Subscription::create([
                                            "customer" => $customer->id,
                                            "items" => array(
                                                array(
                                                        "plan" => $plan,
                                                        'quantity' => $_POST['vacancy_count'],
                                                ),
                                            ),
                                           'tax_percent' => Yii::app()->params['TAX_PERCENT'],
                                      ]);
                                }
                                
//                                 $model->stripe_subscription = $subscription->id;
//                                 $model->stripe_plan = $plan;
//                                 $model->last_four = $_POST['cvc'];
                                
                           //  }
                                
                                $model->stripe_active = 1;                                
                              //  $model->stripe_id = $customer->id;
                              
                                //$model->trial_ends_at = $trial_ends;
                                $link = "";
                                if($plan != "employer_annual" && isset($_POST['vacancy_count']) && $_POST['vacancy_count'] > 0)
                                {
                                    $model->vacancy_count = $_POST['vacancy_count'];
                                    $model->current_vacancy_count = $_POST['vacancy_count'];
                                    $model->stripe_plan = $plan;
                                    
                                    $subs = Carbon::now()->addMonths($model->vacancy_count)->toDateTimeString();
                                    
                                    $charge = Charge::create(array(
                                        
                                        "amount" => $amount , // amount in cents, again
                                        "currency" => "gbp",
                                        "customer" => $customer->id,
                                        "description" => "vacancy count",
                                        'receipt_email' => $model->email
                                    ));
                                }
                                else if($plan == "employer_annual")
                                {
                                    $subs = Carbon::now()->addYear()->toDateTimeString();
                                    $model->stripe_subscription = $subscription->id;
                                    $model->stripe_plan = $plan;
                                    $model->last_four = $_POST['cvc'];
                                    
                                    $invoiceitemlist = Invoice::all(array("customer" => $subscriber->stripe_id));
                                    foreach ($invoiceitemlist->autoPagingIterator() as $invoice) {
                                        $link = $invoice['invoice_pdf'];
                                    }
                                    
                                    
                                }
                                
                                $model->subscription_ends_at = $subs;
                                
                                if(isset($_POST['vacancy_count']) && $_POST['vacancy_count'] > 0)
                                {
                                    $model->vacancy_count = $_POST['vacancy_count'];
                                    $model->current_vacancy_count = $_POST['vacancy_count'];
                                }
                                
                                $model->stripe_id = $customer->id;
                                
                                //$model->trial_ends_at = $trial_ends;
                                $model->updated_at = $now->format('Y-m-d H:i:s');
                                
                                $model->premium_user_count = 2;
                                
                                $model->updated_at = $now->format('Y-m-d H:i:s');
                                $model->save();
                                if($plan == "employer_annual")
                                    $this->notifyemployerupgrade($model, $customer->id);
                                
                                    
                                if($link != ""){
                                    $message = "Successfully updated your membership <a href='$link'>Click here to download your invoice</a>";
                                    $this->sendMessageE("Successfully updated your membership",$message, 57, $model->user_id);
                                }
                                
                                //subscription code
                                $umodel = Users::model()->findByPk($model->user_id);
                                
                                $logmodel = new LoginForm;
                                
                              $this->notifyemployer($model, $planname = "");
                                
                                $logmodel->username = $umodel->email;
                                $logmodel->password = $_POST['Users']['password'];
                                         
                                
                              /*  if( $planarr->id == "employer_annual" || $planarr->id == "employer_annual_others")
                                {
    //                                invoice send
                                    $this->notifyemployerupgrade($model, $customer->id);
                                    
                                    $subscriber = Employers::model()->findByPk($model->id);
                                    
                                    $invoiceitemlist = Invoice::all(array("customer" => $subscriber->stripe_id));
                                    foreach ($invoiceitemlist->autoPagingIterator() as $invoice) {
                                         $link = $invoice['invoice_pdf'];
                                    }
                                    
                                    $message = "Successfully updated your membership <a href='$link'>Click here to download your invoice</a>";
                                    
                                   $this->sendMessageE("Successfully updated your membership",$message, 57, $model->user_id);
                                }     */
                                
                                if ($logmodel->validate() && $logmodel->login()) {
                                    $cri = new CDbCriteria();
                                    $cri->condition = "owner_id =".$model->id." and type like '%employer%'";
                                    $profilemodel = Profiles::model()->find($cri);
                                   
                                    $this->redirect($this->createUrl('employers/edit', array('slug' => $profilemodel->slug)));
                                }
                            }
                    }
                    $this->render('registeremployerpremium', array('model' => $model, 'addressmodel' => $addressmodel, 'usermodel' => $usermodel,'profilemodel'=>$profilemodel));
                
                //$this->render('registeremployerpremium', array('model' => $model, 'addressmodel' => $addressmodel, 'usermodel' => $usermodel));
                
    }
    
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actioncreateCandidateDetails($target = "") {
       
        $model = new Users();
        $profilemodel = new Profiles();
        $addressmodel = new Addresses();
        $now = new DateTime();
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if (isset($_POST['g-recaptcha-response']) && $_POST['g-recaptcha-response'] == "")
            $model->setScenario('captcha');
        
        if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
            $model->setScenario('confirm');
        if($_POST['Users']['dob'] == "" || $_POST['Users']['dob']  == null)
            $model->setScenario('doberror');
        if(isset($_POST['Users'])){
           $model->attributes=$_POST['Users'];
        }
        if(isset ($_POST["Addresses"])){
            $addressmodel->attributes = $_POST["Addresses"]; 
        }
        if(isset ($_POST["Profiles"])){
            $profilemodel->attributes = $_POST["Profiles"];
        }     
        if(isset($_POST['Users']))
        {
            $valid = $model->validate();
            $valid = $profilemodel->validate() && $valid;
            $valid = $addressmodel->validate() && $valid;
             
            if($valid){
            $d = str_replace('/', '-', $_POST['Users']['dob']);
            $dobformat = date('Y-m-d', strtotime($d));
            
            $model->display_gender = ($_POST['Users']['display_gender'] == 0 ? 1 : 0);
            $model->dob = $dobformat;
            $model->location = $_POST['Addresses']['county'].($_POST['Addresses']['county']!=''? ', '.$_POST['Addresses']['country']: '');
            $model->location_lat = $_POST['Addresses']['latitude'];
            $model->location_lng = $_POST['Addresses']['longitude'];
            $model->type = 'candidate';
            $model->created_at = $now->format('Y-m-d H:i:s');
            $model->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
            $model->api_key = md5(uniqid(rand(), true));
            $model->save();
            //profile insert
            
            $from = new DateTime($dobformat);
            $to   = new DateTime('today');
            $age = $from->diff($to)->y;
            if($age <= 16)
                $profilemodel->visibility = 0;
            $profilemodel->owner_type = "Candidate";
            $profilemodel->owner_id = $model->id;
            $profilemodel->created_at = $model->created_at;
            $profilemodel->type = $model->type;
            $profilemodel->save();
            //address insert
            $addressmodel->model_id = $model->id;
            $addressmodel->model_type = "Candidate";
            $addressmodel->created_at = $now->format('Y-m-d H:i:s');
            if($addressmodel->save()){
                $umodel = Users::model()->findByPk($model->id);

                $this->notifycandidate($model, $_POST['Users']['password']);

                $logmodel = new LoginForm;

                $logmodel->username = $umodel->email;
                $logmodel->password = $_POST['Users']['password'];

                if ($logmodel->validate() && $logmodel->login()) {
                    if ($target != "") {
                        $this->redirect($this->createUrl($target));
                    }
                    $this->redirect($this->createUrl('users/edit', array('slug' => $profilemodel->slug)));
                }
                
            }
        }
        }
        // $this->is_premiun = 0;
        $this->render('registercandidatebasic', array('model' => $model, 'addressmodel' => $addressmodel, 'profilemodel' => $profilemodel));

    }
    
    
     /**
     * Notify the user
     */
    public function notifyemployerupgrade($model, $customerid) {
        $body = $this->renderPartial("//emails/invoice", array('customerid' => $customerid), true);
        // Send register notifcation to employer admin
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->Host = 'mail.cvvid.com';
        $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
        $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
        $mail->SMTPSecure = 'ssl';
        $mail->SetFrom('mail@cvvid.com', 'CVVID');
        $mail->Subject = 'Invoice details for CVVid.com';
        $mail->MsgHTML($body);
        $mail->AddAddress($model->email, $model->name);
        $mail->Send();
    }

    /**
     * Notify the user
     */
    public function notifyemployer($model, $password) {
        //echo "test";
        //die();
        $body = $this->renderPartial("//emails/registered-employer", array('model' => $model, 'planname' => ''), true);
        // Send register notifcation to employer admin
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->Host = 'mail.cvvid.com';
        $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
        $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
        $mail->SMTPSecure = 'ssl';
        $mail->SetFrom('mail@cvvid.com', 'CVVID');
        $mail->Subject = 'Your account details for CVVid.com';
        $mail->MsgHTML($body);
        $mail->AddAddress($model->email, $model->name);
        $mail->Send();
    }

    /**
     * Notify the user
     */
    public function notifycandidate($model, $password) {
        $body = $this->renderPartial("//emails/user-account", array('user' => $model, 'password' => $password), true);
        // Send register notifcation to employer admin
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->Host = 'mail.cvvid.com';
        //$mail->Port = "465";
       // $mail->SMTPSecure = 'ssl';
       // $mail->SMTPAuth = true;
        $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
        $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];     
        $mail->SMTPSecure = 'ssl';
       // $mail->SMTPDebug = 2;
       // $mail->CharSet = 'UTF-8';
        $mail->SetFrom('mail@cvvid.com', 'CVVID');
        $mail->Subject = 'Your account details for CVVid.com';
        $mail->MsgHTML($body);
        $mail->AddAddress($model->email, $model->forenames . " " . $model->surname);
        $mail->Send();
    }

    /**
     * Notify the user
     */
    public function notifyinterest($model) {
        $body = $this->renderPartial("//emails/register-institution-tankyou", array('institution' => $model), true);
        // Send register notifcation to employer admin
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->Host = 'mail.cvvid.com';
        $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
        $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
        $mail->SMTPSecure = 'ssl';
        $mail->SetFrom('mail@cvvid.com', 'CVVID');
        $mail->Subject = 'Thank you for registering your interest with CVVid';
        $mail->MsgHTML($body);
        $mail->AddAddress($model->email, $model->name);
        $mail->Send();
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actioncreateCandidatePremium($target = "") {

        $model = new Users();
        $profilemodel = new Profiles();
        $addressmodel = new Addresses();
        $now = new DateTime();
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        if (isset($_POST['g-recaptcha-response']) && $_POST['g-recaptcha-response'] == "")
            $model->setScenario('captcha');
        
        if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
            $model->setScenario('confirm');
        if($_POST['Users']['dob'] == "" || $_POST['Users']['dob']  == null)
            $model->setScenario('doberror');
        if(isset($_POST['Users'])){
           $model->attributes=$_POST['Users'];
        }
        if(isset ($_POST["Addresses"])){
            $addressmodel->attributes = $_POST["Addresses"]; 
        }
        if(isset ($_POST["Profiles"])){
            $profilemodel->attributes = $_POST["Profiles"];
        }     
        if(isset($_POST['Users']))
        {
            $valid = $model->validate();
            $valid = $addressmodel->validate() && $valid;
            $valid = $profilemodel->validate() && $valid;
             
            if($valid){
            $d = str_replace('/', '-', $_POST['Users']["dob"]);
            $dobformat = date('Y-m-d', strtotime($d));
            
            $model->display_gender = ($_POST['Users']['display_gender'] == 0 ? 1 : 0);
            $model->dob = $dobformat;
            // Check if using free trial then set membership trial date
            $model->trial_ends_at = Carbon::now()->addYear()->toDateTimeString();
            $model->location = $_POST['Addresses']['county'].($_POST['Addresses']['county']!=''? ', '.$_POST['Addresses']['country']: '');
            $model->location_lat = $_POST['Addresses']['latitude'];
            $model->location_lng = $_POST['Addresses']['longitude'];
            $model->type = 'candidate';
            $model->created_at = $now->format('Y-m-d H:i:s');
            $model->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
            $model->api_key = md5(uniqid(rand(), true));
            $model->save();
            //profile insert
            $profilemodel->attributes = $_POST['Profiles'];
            $profilemodel->owner_type = "Candidate";
            $profilemodel->owner_id = $model->id;
            $profilemodel->created_at = $model->created_at;
            $profilemodel->type = 'candidate';
            $profilemodel->slug = $_POST['Profiles']['slug'];
            $profilemodel->save();
            // address insert
            $addressmodel->model_id = $model->id;
            $addressmodel->model_type = "Candidate";
            $addressmodel->created_at = $now->format('Y-m-d H:i:s');
            if($addressmodel->save()){
                $umodel = Users::model()->findByPk($model->id);

                $logmodel = new LoginForm;

                $this->notifycandidate($model, $_POST['Users']['password']);

                $logmodel->username = $umodel->email;
                $logmodel->password = $_POST['Users']['password'];

                if ($logmodel->validate() && $logmodel->login()) {
                    if ($target != "") {
                        $this->redirect($this->createUrl($target));
                    }
                    $this->redirect($this->createUrl('users/edit', array('slug' => $profilemodel->slug)));
                }
            }
        }
        }
        //   $this->is_premiun = 1;
        $this->render('registercandidatepremium', array('model' => $model, 'addressmodel' => $addressmodel, 'profilemodel' => $profilemodel));
    }

    public function actioncreateInstitutions() {

        $now = new DateTime();
        $model = new Institutions();
        $iusermodel = new InstitutionUsers();
        $usermodel = new Users();
        $profilemodel = new Profiles();
        $addressmodel = new Addresses();

        if (isset($_POST['g-recaptcha-response']) && $_POST['g-recaptcha-response'] == "")
            $usermodel->setScenario('captcha');
        
        if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
            $usermodel->setScenario('confirm');

        if (isset($_POST['Users'])) {
            $usermodel->attributes = $_POST['Users'];
        }
        if (isset($_POST['Institutions'])) {
            $model->attributes = $_POST['Institutions'];
        }
        if (isset($_POST["Addresses"])) {
            $addressmodel->attributes = $_POST["Addresses"];
        }
        if (isset($_POST["Profiles"])) {
            $profilemodel->attributes = $_POST["Profiles"];
        }
        if (isset($_POST['Users'])) {

            $valid = $usermodel->validate();
           
            $valid = $model->validate() && $valid;
           
            $valid = $addressmodel->validate() && $valid;
           
         //   $valid = $profilemodel->validate() && $valid;
    
            if ($valid) {

                $usermodel->status = 'pending';
                $usermodel->location = $_POST['Addresses']['county'] . ($_POST['Addresses']['county'] != '' ? ', ' . $_POST['Addresses']['country'] : '');
                $usermodel->location_lat = $_POST['Addresses']['latitude'];
                $usermodel->location_lng = $_POST['Addresses']['longitude'];
                $usermodel->type = 'institution_admin';
                $usermodel->created_at = $now->format('Y-m-d H:i:s');
                $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
                $usermodel->api_key = md5(uniqid(rand(), true));
                $usermodel->save();
//            instituion insert
                $model->admin_id = $usermodel->id;
                $model->email = $usermodel->email;
                $model->status = 'pending';
                $model->address_1 = $_POST['Addresses']['address'];
                $model->town = $_POST['Addresses']['town'];
                $model->post_code = $_POST['Addresses']['postcode'];
                $model->created_at = $now->format('Y-m-d H:i:s');
                if ($model->save()) {
                    $iusermodel->institution_id = $model->id;
                    $iusermodel->user_id = $usermodel->id;
                    $iusermodel->role = "admin";
                    $iusermodel->created_at = $now->format('Y-m-d H:i:s');
                    $iusermodel->save();
                }
                //profile insert
                $slug = str_replace(" ", "-", $model->name);
                $profilemodel->owner_type = "Institution";
                $profilemodel->owner_id = $model->id;
                $profilemodel->created_at = $usermodel->created_at;
                $profilemodel->type = "institution";
                $profilemodel->slug = lcfirst($slug);
                $profilemodel->save();
                //address insert
                $addressmodel->attributes = $_POST['Addresses'];
                $addressmodel->model_id = $model->id;
                $addressmodel->model_type = "Institutions";
                $addressmodel->created_at = $now->format('Y-m-d H:i:s');
                if ($addressmodel->save()) {

                    $this->notifyinterest($model);

                    Yii::app()->user->setFlash('success', 'Successfully registered institution, we will be in touch shortly.');
                    $this->redirect($this->createUrl('site/login'));
                }
            }
        }
        $this->render('registerinstitution', array('model' => $model, 'addressmodel' => $addressmodel, 'usermodel' => $usermodel));
    }

    public function actionloadVideo() {
        
        $videoid = $_POST['videoid'];
         $url = 'https://' . Yii::app()->params['AWS_BUCKET'] . '.s3.amazonaws.com/success-stories/'.$videoid;
        echo '<iframe src="'.$url.'" class="iframe-video" width="100%" height="500px" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>';
        //echo '<div data-type="vimeo" data-video-id="//player.vimeo.com/video/' . $videoid . '"></div>';
        // echo '<iframe src="//player.vimeo.com/video/' . $videoid . '" width="100%" height="500px" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
    }
    
     public function actionloadVideohowto() {
        
        $videoid = $_POST['videoid'];
         $url = 'https://' . Yii::app()->params['AWS_BUCKET'] . '.s3.amazonaws.com/how-to/'.$videoid;
        echo '<iframe src="'.$url.'" class="iframe-video" width="100%" height="500px" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>';
        //echo '<div data-type="vimeo" data-video-id="//player.vimeo.com/video/' . $videoid . '"></div>';
        // echo '<iframe src="//player.vimeo.com/video/' . $videoid . '" width="100%" height="500px" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
    }
    
     public function actionloadVideowhycvvid() {
        
        $videoid = $_POST['videoid'];
         $url = 'https://' . Yii::app()->params['AWS_BUCKET'] . '.s3.amazonaws.com/why-cvvid/'.$videoid;
        echo '<iframe src="'.$url.'" class="iframe-video" width="100%" height="500px" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>';
        //echo '<div data-type="vimeo" data-video-id="//player.vimeo.com/video/' . $videoid . '"></div>';
        // echo '<iframe src="//player.vimeo.com/video/' . $videoid . '" width="100%" height="500px" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
    }
    
     public function actionloadVideotoptips() {
        
        $videoid = $_POST['videoid'];
         $url = 'https://' . Yii::app()->params['AWS_BUCKET'] . '.s3.amazonaws.com/top-tips/'.$videoid;
        echo '<iframe src="'.$url.'" class="iframe-video" width="100%" height="500px" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>';
        //echo '<div data-type="vimeo" data-video-id="//player.vimeo.com/video/' . $videoid . '"></div>';
        // echo '<iframe src="//player.vimeo.com/video/' . $videoid . '" width="100%" height="500px" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
    }

     /* public function actionsearchJobs($title = "",$loc = "all") {
       
     
         $name = isset($_POST['name']) ? $_POST['name'] : "";
         $skillcategid = isset($_POST['skill_category']) ? $_POST['skill_category'] : 0;
         $skillarr = isset($_POST['skills']) ? $_POST['skills'] : array();
         $location = isset($_POST['location']) ? $_POST['location'] : "";
         $lat = isset($_POST['latitude']) ? $_POST['latitude'] : "";
         $long = isset($_POST['longitude']) ? $_POST['longitude'] : "";
         $jobtype = isset($_POST['jobtype']) ? $_POST['jobtype'] : "";
         $salary = isset($_POST['salary']) ? $_POST['salary'] : "";
         $radius = isset($_POST['distance']) ? $_POST['distance'] : 0;
        
         if($title != "all" && $title != "")
         {
             //$title = str_replace("~", "/", str_replace("-", " ", urldecode($title)));
             $catname = str_replace("-", "", urldecode($title));
            
             $cri1 = new CDbCriteria();
             $cri1->condition = "LOWER(replace(name, ' ','')) LIKE '%".$catname."%' OR LOWER(replace(name, '/','')) LIKE '%".$catname."%' OR LOWER(replace(LOWER(replace(name, '/','')), ' ','')) LIKE '%".$catname."%' OR LOWER(replace(name, '/','')) LIKE '%".$catname."%' OR LOWER(replace(name, ',','')) LIKE '%".$catname."%' OR
        LOWER(replace(name, '\&','')) LIKE '%".$catname."%' OR LOWER(replace(name, '&','')) LIKE '%".$catname."%'";
             $catmodel = SkillCategories::model()->find($cri1);
             
//              $skillcategmodel = SkillCategories::model()->findByAttributes(array('name'=>str_replace("~", "/", str_replace("-", " ", urldecode($title)))));
             $skillcategid = $catmodel['id'];
             
         }
         
         
         if($skillcategid > 0)
         {
             $scmodel = SkillCategories::model()->findByPk($skillcategid);            
         }
         
         
         if($title == "all" && $loc != "all")
         {
             $names = Yii::app()->db->createCommand(
                 'SELECT name FROM cv16_skill_categories'
                 )->queryAll();
                 
                 $def_keys = array("Video cv","Cv builder","Online interview platform","Blackburn jobs","Job search","Upload cv");
                 $names_arr = array_column( $names, 'name' ) ;
                 $this->_metaKeywords = implode(",", array_merge($def_keys,$names_arr));
         }         
         if($skillcategid > 0 && $loc != "all")
         {
             $def_keys = array("Video cv","Cv builder","Online interview platform","Blackburn jobs","Job search","Upload cv");
             
             $def_keys[] = "Jobs in ".$location." ".$scmodel->name." jobs";
             
             
             $cri1 = new CDbCriteria();
             $cri1->select = "skill_category_id";
             $cri1->distinct = true;
             $cri1->condition = "location = '".urldecode($loc)."'";   
             $jmodel = Jobs::model()->findAll($cri1);
             foreach ($jmodel as $jm)
             {
                 $scateg = SkillCategories::model()->findByPk($jm['skill_category_id']);
                 $def_keys[] = $scateg->name." jobs in ".$location;                 
             }
             
             $cri2 = new CDbCriteria();
             $cri2->select = "location";
             $cri2->distinct = true;
             $cri2->condition = "skill_category_id =".$skillcategid;
             $jmodel1 = Jobs::model()->findAll($cri2);
             foreach ($jmodel1 as $jm1)
             {
                 $scateg1 = SkillCategories::model()->findByPk($jm1['skill_category_id']);
                 $def_keys[] = $scmodel->name." jobs in ".$jm1['location'];
             }
             
             $this->_metaKeywords = implode(",", $def_keys);
         }
         
         else if($skillcategid > 0)
         {
             $def_keys = array("Video cv","Cv builder","Online interview platform","Blackburn jobs","Job search","Upload cv");
             $def_keys[] = $scmodel->name." jobs";
             $cri1 = new CDbCriteria();
             $cri1->select = "location";
             $cri1->distinct = true;
             $cri1->condition = "skill_category_id =".$skillcategid;
             $jmodel = Jobs::model()->findAll($cri1);
             foreach ($jmodel as $jm)
                 $def_keys[] = $scmodel->name." jobs in ".$jm['location'];
                 
             $this->_metaKeywords = implode(",", $def_keys);
         }
         else if($loc != "all")
         {
             $def_keys = array("Video cv","Cv builder","Online interview platform","Blackburn jobs","Job search","Upload cv");
             $def_keys[] = "Jobs in ".$location;
             $cri1 = new CDbCriteria();
             $cri1->select = "skill_category_id";
             $cri1->distinct = true;
             $cri1->condition = "location = '".urldecode($loc)."'";   
             
             $jmodel = Jobs::model()->findAll($cri1);
             foreach ($jmodel as $jm)
             {
                 $scateg1 = SkillCategories::model()->findByPk($jm['skill_category_id']);
                 if($scateg1 != null)
                     $def_keys[] = $scateg1->name." jobs in ".$location;
             }
                 $this->_metaKeywords = implode(",", $def_keys);
         }
         
       
         
        $skillids = implode(',', $skillarr);

        $cri = new CDbCriteria();
        $cri->alias = "j";
        $cri->join = "left outer join cv16_job_skill js on js.job_id = j.id
                      left outer join cv16_skills s on s.id = js.skill_id
                      left outer join cv16_skill_categories sc on sc.id = j.skill_category_id";
        if ($skillcategid > 0)
            $cri->addCondition("j.skill_category_id =" . $skillcategid);
        if ($skillids != "")
            $cri->addCondition('js.skill_id in (' . $skillids . ')', 'AND');
        if ($location != "" && $lat != "" && $loc == "all") {
            if ($radius > 0) {
               $cri->select = "*,(3959 * acos( cos( radians(".$lat.") ) * cos( radians(location_lat) ) *
                                cos(radians(location_lng) - radians(".$long.")) + sin(radians(".$lat.")) *
                                sin(radians(location_lat)) )) as distance";   
//                 $cri->addCondition("HAVING distance < " . $radius);
                 if($radius <= 50)
                   $cri->having = "distance < " . $radius;
            }
            else {
                 $cri->addCondition("j.location_lat =".$lat." and j.location_lng=".$long);
                
                //$cri->addCondition("j.location like '%" . $location . "%'");
               // $cri->addCondition("j.location_lat =".$lat." and j.location_lng=".$long);
            }
        }
        
        if($loc != "all")
        {
          //  $cri->addCondition("j.location ='".urldecode($loc)."'");
            $cri->addCondition("LOWER(replace(LEFT(location,LOCATE(' ',j.location) - 2), ' ','')) like '%".urldecode($loc)."%'");
        }
        
          $cri->addCondition("j.status ='active'");
        if ($salary > 0) {
            $cri->addCondition("j.salary_min >= " . $salary);
        }
        if ($jobtype != "") {
            $cri->addCondition("j.type like '%" . $jobtype . "%'");
        }
        if(trim($name) != "")
        {
            $cri->addCondition("j.title like '%" . $name . "%'");
        }
        $cri->addCondition("j.end_date >= '".date('Y-m-d')."'");
        $cri->addCondition("j.careers <= 0");
        $cri->group = "j.id";
        
        $order = "";
        $cri->order = "j.created_at desc";
        
        $job_a_z = "";
        $job_z_a = "";
        $job_recent = "";
        $job_old = "";
        if(isset($_POST['sorting']) && $_POST['sorting'] == 'a_z')
        {
            $job_a_z = 1;
                $order = "j.title asc";
        }
        else if(isset($_POST['sorting']) && $_POST['sorting'] == 'z_a')
        {
            $job_z_a = 1;
                $order = "j.title desc";
            
        }
        else if(isset($_POST['sorting']) && $_POST['sorting'] == 'recent')
        {
            
            $job_recent = 1;
                $order = "j.created_at desc";
                
        }
        else if(isset($_POST['sorting']) && $_POST['sorting'] == 'old')
        {
            $job_old = 1;
                $order = "j.created_at asc";
        }
       
        if($order != "")
        {
            $cri->order = $order;
        }
        
       
       
     //   if($loc != "all")
      //  {
            
//             $cri1 = new CDbCriteria();
//             $cri1->condition = "location = '".urldecode($loc)."'";            
//             $joblocmodel = Jobs::model()->find($cri1);
            $location = $loc;//$joblocmodel['location'];
//             $firstloc = explode(",", urldecode($loc))[0];
            $cri->addCondition("LOWER(replace(LEFT(location,LOCATE(' ',j.location) - 2), ' ','')) like '%".$loc."%'");
            
//             if($location != "")
//                 $cri->addCondition("LOWER(replace(LEFT(location,LOCATE(' ',j.location) - 2), ' ','')) like '%".$location."%'");
//                 else
//                     $cri->addCondition("substring_index(j.location, ',', -1) like '%".$visitor_country."%'");
            
            $jobmodel = Jobs::model()->findAll($cri);
            if(count($jobmodel) > 0)
                $this->_pageTitle = count($jobmodel)." openings for ".$title." jobs in ".str_replace('-'," ", $loc)." - Best Online interview platform in ".str_replace('-'," ", $loc)."  - cvvid.com";
            else 
                $this->_pageTitle = "Jobs in ".str_replace('-'," ", $loc)." - ".Yii::app()->name;
            $this->_pageDescription = "Apply to ".count($jobmodel)." ".$title." jobs in ".str_replace('-'," ", $loc)."- Best Online interview platform in ".str_replace('-'," ", $loc).".Explore latest Jobs in Location across top companies Now!";
      /*      
        }
        else 
        {
            $visitor_country = $this->ip_info("Visitor", "Country");
          
            $cri->addCondition("substring_index(j.location, ',', -1) like '%".$visitor_country."%'");

//             $geocode_stats = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyAd4jLN8LdoltMtZJfGS3C8ZTgPL7LDtdw&address=".$visitor_country."&sensor=false");            
//             $output_deals = json_decode($geocode_stats);          
//             $latLng = $output_deals->results[0]->geometry->location;           
//             $lat = $latLng->lat;
//             $lng = $latLng->lng;             
           // $cri->addCondition("j.location_lat like '%".$lat."%' and j.location_lng like '%".$lng."%'");
            $jobmodel = Jobs::model()->findAll($cri);
            if(count($jobmodel) > 0)
                $this->_pageTitle = count($jobmodel)." openings for ".$title." jobs on ".Yii::app()->name;  
            else 
                $this->_pageTitle = $title." jobs on ".Yii::app()->name;  
            
            $this->_pageDescription = "Apply to ".count($jobmodel)." ".$title." jobs on ".Yii::app()->name;
        }*/
        
       
        
       /* if(count($jobmodel) <= 0)
            $jobmodel = array();
        
            $this->render('jobsearchresults', array('model' => $jobmodel,'job_name'=>$name,'job_skill_category'=>$skillcategid,
                'job_skills'=>$skillarr,'job_location'=>$location,'job_latitude'=>$lat,
                'job_longitude'=>$long,'job_jobtype'=>$jobtype,'job_salary'=>$salary,'job_distance'=>$radius,
                'job_a_z'=>$job_a_z,'job_z_a'=>$job_z_a,"job_recent"=>$job_recent,"job_old"=>$job_old
                
            ));
    } */
    
   
 public function actionsearchJobs($title = "",$loc = "all") {
       
     
         $name = isset($_POST['name']) ? $_POST['name'] : "";
         $skillcategid = isset($_POST['skill_category']) ? $_POST['skill_category'] : 0;
         $skillarr = isset($_POST['skills']) ? $_POST['skills'] : array();
         $location = isset($_POST['location']) ? $_POST['location'] : "";
         $lat = isset($_POST['latitude']) ? $_POST['latitude'] : "";
         $long = isset($_POST['longitude']) ? $_POST['longitude'] : "";
         $jobtype = isset($_POST['jobtype']) ? $_POST['jobtype'] : "";
         $salary = isset($_POST['salary']) ? $_POST['salary'] : "";
         $radius = isset($_POST['distance']) ? $_POST['distance'] : 0;
        
         if($title != "all" && $title != "")
         {
             //$title = str_replace("~", "/", str_replace("-", " ", urldecode($title)));
             $catname = str_replace("-", "", urldecode($title));
            
             $cri1 = new CDbCriteria();
             $cri1->condition = "LOWER(replace(name, ' ','')) LIKE '%".$catname."%' OR LOWER(replace(name, '/','')) LIKE '%".$catname."%' OR LOWER(replace(LOWER(replace(name, '/','')), ' ','')) LIKE '%".$catname."%' OR LOWER(replace(name, '/','')) LIKE '%".$catname."%' OR LOWER(replace(name, ',','')) LIKE '%".$catname."%' OR
  		LOWER(replace(name, '\&','')) LIKE '%".$catname."%' OR LOWER(replace(name, '&','')) LIKE '%".$catname."%'";
             $catmodel = SkillCategories::model()->find($cri1);
             
//              $skillcategmodel = SkillCategories::model()->findByAttributes(array('name'=>str_replace("~", "/", str_replace("-", " ", urldecode($title)))));
             $skillcategid = $catmodel['id'];
             
         }
         
         
         if($skillcategid > 0)
         {
             $scmodel = SkillCategories::model()->findByPk($skillcategid);            
         }
         
         
         if($title == "all" && $loc != "all")
         {
             $names = Yii::app()->db->createCommand(
                 'SELECT name FROM cv16_skill_categories'
                 )->queryAll();
                 
                 $def_keys = array("Video cv","Cv builder","Online interview platform","Blackburn jobs","Job search","Upload cv");
                 $names_arr = array_column( $names, 'name' ) ;
                 $this->_metaKeywords = implode(",", array_merge($def_keys,$names_arr));
         }         
         if($skillcategid > 0 && $loc != "all")
         {
             $def_keys = array("Video cv","Cv builder","Online interview platform","Blackburn jobs","Job search","Upload cv");
             
             $def_keys[] = "Jobs in ".$location." ".$scmodel->name." jobs";
             
             
             $cri1 = new CDbCriteria();
             $cri1->select = "skill_category_id";
             $cri1->distinct = true;
             $cri1->condition = "location = '".urldecode($loc)."'";   
             $jmodel = Jobs::model()->findAll($cri1);
             foreach ($jmodel as $jm)
             {
                 $scateg = SkillCategories::model()->findByPk($jm['skill_category_id']);
                 $def_keys[] = $scateg->name." jobs in ".$location;                 
             }
             
             $cri2 = new CDbCriteria();
             $cri2->select = "location";
             $cri2->distinct = true;
             $cri2->condition = "skill_category_id =".$skillcategid;
             $jmodel1 = Jobs::model()->findAll($cri2);
             foreach ($jmodel1 as $jm1)
             {
                 $scateg1 = SkillCategories::model()->findByPk($jm1['skill_category_id']);
                 $def_keys[] = $scmodel->name." jobs in ".$jm1['location'];
             }
             
             $this->_metaKeywords = implode(",", $def_keys);
         }
         
         else if($skillcategid > 0)
         {
             $def_keys = array("Video cv","Cv builder","Online interview platform","Blackburn jobs","Job search","Upload cv");
             $def_keys[] = $scmodel->name." jobs";
             $cri1 = new CDbCriteria();
             $cri1->select = "location";
             $cri1->distinct = true;
             $cri1->condition = "skill_category_id =".$skillcategid;
             $jmodel = Jobs::model()->findAll($cri1);
             foreach ($jmodel as $jm)
                 $def_keys[] = $scmodel->name." jobs in ".$jm['location'];
                 
             $this->_metaKeywords = implode(",", $def_keys);
         }
         else if($loc != "all")
         {
             $def_keys = array("Video cv","Cv builder","Online interview platform","Blackburn jobs","Job search","Upload cv");
             $def_keys[] = "Jobs in ".$location;
             $cri1 = new CDbCriteria();
             $cri1->select = "skill_category_id";
             $cri1->distinct = true;
             $cri1->condition = "location = '".urldecode($loc)."'";   
             
             $jmodel = Jobs::model()->findAll($cri1);
             foreach ($jmodel as $jm)
             {
                 $scateg1 = SkillCategories::model()->findByPk($jm['skill_category_id']);
                 if($scateg1 != null)
                     $def_keys[] = $scateg1->name." jobs in ".$location;
             }
                 $this->_metaKeywords = implode(",", $def_keys);
         }
         
       
         
        $skillids = implode(',', $skillarr);

        $cri = new CDbCriteria();
        $cri->alias = "j";
        $join = "left outer join cv16_job_skill js on js.job_id = j.id
                      left outer join cv16_skills s on s.id = js.skill_id
                      left outer join cv16_skill_categories sc on sc.id = j.skill_category_id";
        if ($skillcategid > 0)
            $cri->addCondition("j.skill_category_id =" . $skillcategid);
        if ($skillids != "")
            $cri->addCondition('js.skill_id in (' . $skillids . ')', 'AND');
      /*  if ($location != "" && $lat != "" && $loc == "all") {
            if ($radius > 0) {
               $cri->select = "*,(3959 * acos( cos( radians(".$lat.") ) * cos( radians(location_lat) ) *
                                cos(radians(location_lng) - radians(".$long.")) + sin(radians(".$lat.")) *
                                sin(radians(location_lat)) )) as distance";   
//                 $cri->addCondition("HAVING distance < " . $radius);
                 if($radius <= 50)
                   $cri->having = "distance < " . $radius;
            }
            else {
                 $cri->addCondition("j.location_lat =".$lat." and j.location_lng=".$long);
                
                //$cri->addCondition("j.location like '%" . $location . "%'");
               // $cri->addCondition("j.location_lat =".$lat." and j.location_lng=".$long);
            }
        }*/
        
//         if($loc != "all")
//         {
//           //  $cri->addCondition("j.location ='".urldecode($loc)."'");
//             $cri->addCondition("LOWER(replace(LEFT(location,LOCATE(' ',j.location) - 2), ' ','')) like '%".urldecode($loc)."%'");
//         }
        
          $cri->addCondition("j.status ='active'");
        if ($salary > 0) {
            $cri->addCondition("j.salary_min >= " . $salary);
        }
        if ($jobtype != "") {
            $cri->addCondition("j.type like '%" . $jobtype . "%'");
        }
        if(trim($name) != "")
        {
            $cri->addCondition("j.title like '%" . $name . "%'");
        }
        $cri->addCondition("j.end_date >= '".date('Y-m-d')."'");
        $cri->addCondition("j.careers <= 0");
        $cri->group = "j.id";
        
        $order = "";
        $cri->order = "j.created_at desc";
        
        $job_a_z = "";
        $job_z_a = "";
        $job_recent = "";
        $job_old = "";
        if(isset($_POST['sorting']) && $_POST['sorting'] == 'a_z')
        {
            $job_a_z = 1;
                $order = "j.title asc";
        }
        else if(isset($_POST['sorting']) && $_POST['sorting'] == 'z_a')
        {
            $job_z_a = 1;
                $order = "j.title desc";
            
        }
        else if(isset($_POST['sorting']) && $_POST['sorting'] == 'recent')
        {
            
            $job_recent = 1;
                $order = "j.created_at desc";
                
        }
        else if(isset($_POST['sorting']) && $_POST['sorting'] == 'old')
        {
            $job_old = 1;
                $order = "j.created_at asc";
        }
       
        if($order != "")
        {
            $cri->order = $order;
        }
        
//         if($loc == "all")
//             $loc = "";

        //Rec Agency candidate search
        if(Yii::app()->user->getState('role') == "candidate")
        {
            $acmodel = AgencyCandidates::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid')));  
            if($acmodel != null)
            {
                $join .= " left outer join cv16_agency_vacancies av on av.job_id = j.id";
                $cri->order = "av.id desc";
            }

        }
        $cri->join = $join;
        //Rec Agency candidate search
        
       if($loc != "all")
       {
            
//             $cri1 = new CDbCriteria();
//             $cri1->condition = "location = '".urldecode($loc)."'";            
//             $joblocmodel = Jobs::model()->find($cri1);
            $location = $loc;//$joblocmodel['location'];
//             $firstloc = explode(",", urldecode($loc))[0];

            if ($lat != "" && $radius > 0) {
                $cri->select = "*,(3959 * acos( cos( radians(".$lat.") ) * cos( radians(location_lat) ) *
                                cos(radians(location_lng) - radians(".$long.")) + sin(radians(".$lat.")) *
                                sin(radians(location_lat)) )) as distance";
                //                 $cri->addCondition("HAVING distance < " . $radius);
                if($radius <= 50)
                    $cri->having = "distance < " . $radius;
            }
            else {
               if($loc == "United-Kingdom" || $loc == "UK")
                   $cri->addCondition("TRIM(substring_index(j.location, ',', -1)) = 'UK' OR TRIM(substring_index(j.location, ',', -1)) like '%United Kingdom%'");
               else if($loc == "United-States" || $loc == "US")
                       $cri->addCondition("TRIM(substring_index(j.location, ',', -1)) = 'US' OR TRIM(substring_index(j.location, ',', -1)) like '%United States%'");
               else 
                   $cri->addCondition("replace(TRIM(substring_index(j.location, ',', -1)), ' ', '-') like '%".$loc."%' OR replace(TRIM(substring_index(j.location, ',', 1)), ' ', '-') like '%".$loc."%' OR (j.location_lat ='".$lat."' AND j.location_lng ='".$long."')");
            
            }
          //  $cri->addCondition("substring_index(j.location, ',', -1) like '%".$visitor_country."%' OR substring_index(j.location, ',', -1) = '".$visitor_country_code."' OR  substring_index(j.location, ',', 1) like '%.$visitor_region.%' OR  substring_index(j.location, ',', 1) like '%.$visitor_city.%'");
//             echo var_dump($cri);
//             die();
//             if($location != "") 
//                 $cri->addCondition("LOWER(replace(LEFT(location,LOCATE(' ',j.location) - 2), ' ','')) like '%".$location."%'");
//                 else
//                     $cri->addCondition("substring_index(j.location, ',', -1) like '%".$visitor_country."%'");
            
            $jobmodel = Jobs::model()->findAll($cri);
            if(count($jobmodel) > 0)
                $this->_pageTitle = count($jobmodel)." openings for ".$title." jobs in ".str_replace('-'," ", $loc)." - Best Online interview platform in ".str_replace('-'," ", $loc)."  - cvvid.com";
            else 
                $this->_pageTitle = "Jobs in ".str_replace('-'," ", $loc)." - ".Yii::app()->name;
            $this->_pageDescription = "Apply to ".count($jobmodel)." ".$title." jobs in ".str_replace('-'," ", $loc)."- Best Online interview platform in ".str_replace('-'," ", $loc).".Explore latest Jobs in Location across top companies Now!";
            
       }
        else 
        {
           
            
            $visitor_country = $this->ip_info("Visitor", "Country");
            $visitor_country_code = $this->ip_info("Visitor", "Countrycode");
            $visitor_region = $this->ip_info("Visitor", "Region");
            $visitor_city = $this->ip_info("Visitor", "City");
            
            $cri->addCondition("TRIM(substring_index(j.location, ',', -1)) like '%".$visitor_country."%' OR TRIM(substring_index(j.location, ',', -1)) = '".$visitor_country_code."' OR  TRIM(substring_index(j.location, ',', 1)) like '%.$visitor_region.%' OR  TRIM(substring_index(j.location, ',', 1)) like '%.$visitor_city.%'");
           
//             $geocode_stats = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyAd4jLN8LdoltMtZJfGS3C8ZTgPL7LDtdw&address=".$visitor_country."&sensor=false");            
//             $output_deals = json_decode($geocode_stats);          
//             $latLng = $output_deals->results[0]->geometry->location;            
//             $lat = $latLng->lat;
//             $lng = $latLng->lng;	            
           // $cri->addCondition("j.location_lat like '%".$lat."%' and j.location_lng like '%".$lng."%'");
            $jobmodel = Jobs::model()->findAll($cri);
            if(count($jobmodel) > 0)
                $this->_pageTitle = count($jobmodel)." openings for ".$title." jobs on ".Yii::app()->name;  
            else 
                $this->_pageTitle = $title." jobs on ".Yii::app()->name;  
            
            $this->_pageDescription = "Apply to ".count($jobmodel)." ".$title." jobs on ".Yii::app()->name;
        }
        
       
        
        if(count($jobmodel) <= 0)
            $jobmodel = array();
        
            $this->render('jobsearchresults', array('model' => $jobmodel,'job_name'=>$name,'job_skill_category'=>$skillcategid,
                'job_skills'=>$skillarr,'job_location'=>$location,'job_latitude'=>$lat,
                'job_longitude'=>$long,'job_jobtype'=>$jobtype,'job_salary'=>$salary,'job_distance'=>$radius,
                'job_a_z'=>$job_a_z,'job_z_a'=>$job_z_a,"job_recent"=>$job_recent,"job_old"=>$job_old
                
            ));
    }

    public static function randomBytes($length = 16) {
        if (PHP_MAJOR_VERSION >= 7 || defined('RANDOM_COMPAT_READ_BUFFER')) {
            $bytes = random_bytes($length);
        } elseif (function_exists('openssl_random_pseudo_bytes')) {
            $bytes = openssl_random_pseudo_bytes($length, $strong);

            if ($bytes === false || $strong === false) {
                throw new RuntimeException('Unable to generate random string.');
            }
        } else {
            throw new RuntimeException('OpenSSL extension or paragonie/random_compat is required for PHP 5 users.');
        }

        return $bytes;
    }

    public static function random($length = 16) {
        $string = '';

        while (($len = strlen($string)) < $length) {
            $size = $length - $len;

            $bytes = static::randomBytes($size);

            $string .= substr(str_replace(['/', '+', '='], '', base64_encode($bytes)), 0, $size);
        }

        return $string;
    }

    public function actionresetPassword($token) {
              
        
        $tkncount = PasswordResets::model()->countByAttributes(array('token'=>$token));
 
        
        if($tkncount > 0)
        {
            $passreset = PasswordResets::model()->findByAttributes(array('token'=>$token));
            $usermodel = Users::model()->findByAttributes(array('email'=>$passreset->email));
            
            
            $this->render('change-password', array('model' => $usermodel));
        }
        else
        {
              $this->render('unauth-page');
        }
        
        
    }
    
    public function actionsavePassword($id) {
        
        $usermodel = Users::model()->findByPk($id);
           
        if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
        {   
            Yii::app()->user->setFlash('error', 'The password confirmation does not match.');
            $this->render('change-password', array('model' => $usermodel));
        }
        else
        {   
            $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
            $usermodel->save();
            
            Yii::app()->user->setFlash('success', 'Successfully password changed');
            $this->redirect($this->createUrl('site/login'));
        }
       
    }

   
    /**
     * Notify the user
     */
    public function notifyresetpassword($email, $token) {
        $body = $this->renderPartial("//emails/reset-password", array('email' => $email, 'token' => $token), true);
        // Send register notifcation to employer admin
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->Host = 'mail.cvvid.com';
        $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
        $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
        $mail->SMTPSecure = 'ssl';
        $mail->SetFrom('mail@cvvid.com', 'CVVID');
        $mail->Subject = 'Your account details for CVVid.com';
        $mail->MsgHTML($body);
        $mail->AddAddress($email);
        $mail->Send();
    }
    
    public function actionbasicagency() {
        
        $model = new Agency;
        $usermodel = new Users;
        $profilemodel = new Profiles;
        $addressmodel = new Addresses;
        //  $this->is_premiun = 0;
        //  $this->_pageTitle = $model-> "Agency Registration | Cvvid";
        $this->_pageTitle = "Agency Registration | Cvvid.com";
        $this->_pageDescription = "New Recruitment agency registration on cvvid.com to get the video resumes for the recruitment process using our cvvid.com.";
        $this->_metaKeywords = "Agency registration | Registration form | agency";
        $this->render('registeragency', array('model' => $model, 'addressmodel' => $addressmodel, 'usermodel' => $usermodel,'profilemodel'=>$profilemodel));
      
    }
    
    public function actioncreateAgency($target = "") {
        $model = new Agency;
        $usermodel = new Users;
        $profilemodel = new Profiles;
        $addressmodel = new Addresses;
        
        $now = new DateTime();
        if (isset($_POST['g-recaptcha-response']) && $_POST['g-recaptcha-response'] == "")
            $model->setScenario('captcha');
            
            if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
                $usermodel->setScenario('confirm');
                
                if(isset($_POST['Users'])){
                    $usermodel->attributes=$_POST['Users'];
                }
                if(isset($_POST['Agency'])){
                    $model->attributes=$_POST['Agency'];
                }
                if(isset ($_POST["Addresses"])){
                    $addressmodel->attributes = $_POST["Addresses"];
                }
                if(isset ($_POST["Profiles"])){
                    $profilemodel->attributes = $_POST["Profiles"];
                }
                if(isset($_POST['Users']))
                {
                    $valid = $usermodel->validate();
                    $valid = $model->validate() && $valid;
                    $valid = $addressmodel->validate() && $valid;
                    $valid = $profilemodel->validate() && $valid;
                    
                    if($valid){
                        $usermodel->created_at = $now->format('Y-m-d H:i:s');
                        $usermodel->type = 'agency';
                        $usermodel->location = $_POST['Addresses']['county'].($_POST['Addresses']['county']!=''? ', '.$_POST['Addresses']['country']: '');
                        $usermodel->location_lat = $_POST['Addresses']['latitude'];
                        $usermodel->location_lng = $_POST['Addresses']['longitude'];
                        $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
                        $usermodel->save();
                        
                        $model->email = $usermodel->email;
                        // Check if using free trial then set membership trial date
                        $model->trial_ends_at = Carbon::now()->addYear()->toDateTimeString();
                        $model->created_at = $now->format('Y-m-d H:i:s');
                        $model->user_id = $usermodel->id;
                        $model->industry_id = $_POST['industry_id'];
                        $model->location = $_POST['Addresses']['county'].($_POST['Addresses']['county']!=''? ', '.$_POST['Addresses']['country']: '');
                        $model->location_lat = $_POST['Addresses']['latitude'];
                        $model->location_lng = $_POST['Addresses']['longitude'];
                        $model->save();
                            
                        $profilemodel->owner_id = $model->id;
                        $profilemodel->owner_type = 'Agency';
                        $profilemodel->type = 'agency';
                            $profilemodel->slug = $_POST["Profiles"]['slug'];
                            $profilemodel->save();
                            
                            //address update
                            $addressmodel->attributes = $_POST["Addresses"];
                            $addressmodel->created_at = $now->format('Y-m-d H:i:s');
                            $addressmodel->model_type = 'Agency';
                            $addressmodel->model_id = $model->id;
                            if ($addressmodel->save()) {
                                
                                $umodel = Users::model()->findByPk($model->user_id);
                                
                                $logmodel = new LoginForm;
                                
                                $this->notifyemployer($model, $planname = "");
                                
                                $logmodel->username = $umodel->email;
                                $logmodel->password = $_POST['Users']['password'];
                                
                                if ($logmodel->validate() && $logmodel->login()) {
                                    $this->redirect($this->createUrl('agency/edit', array('id' => $model->id)));
                                }
                            }
                    }
                    
                }
                
                $this->render('registeragency', array('model' => $model, 'addressmodel' => $addressmodel, 'usermodel' => $usermodel,'profilemodel'=>$profilemodel));
                              
    }
    public function actioncareers()
    {
        $this->redirect(Yii::app()->getBaseUrl(true) . '/cvvidcareers');
    }
    
    public function actioncareer() {
      
      
        $this->render('careers');
    }

    public function actioninvoicepdf()
    {
        $this->render('invoice');
    }
    
    public function sendMessageE($subject,$message,$from,$to) {
        
        $now = new DateTime();
        $conmodel = new Conversations();
        $conmodel->subject = $subject;
        $conmodel->created_at = $now->format('Y-m-d H:i:s');
        $conmodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($conmodel->save()) {
            $usermodel = Users::model()->findByPk($from);
            $conmsgmodel = new ConversationMessages();
            $conmsgmodel->conversation_id = $conmodel->id;
            $conmsgmodel->user_id = $from;
            $conmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
            $conmsgmodel->message = $message;
            $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
            $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($conmsgmodel->save()) {
                $conmemmodel_sender = new ConversationMembers();
                $conmemmodel_sender->conversation_id = $conmodel->id;
                $conmemmodel_sender->user_id = $from;
                $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
                $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
                $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                if ($conmemmodel_sender->save()) {
                    $usermodel1 = Users::model()->findByPk($to);
                    $conmemmodel_rec = new ConversationMembers();
                    $conmemmodel_rec->conversation_id = $conmodel->id;
                    $conmemmodel_rec->user_id = $to;
                    $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
                    $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                    $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                    if ($conmemmodel_rec->save()) {
                        $actmodel_sender = new Activities();
                        $actmodel_sender->user_id = $from;
                        $actmodel_sender->type = "message";
                        $actmodel_sender->model_id = $conmsgmodel->id;
                        $actmodel_sender->model_type = "ConversationMessage";
                        $actmodel_sender->subject = "You sent a message in conversation: " . $subject;
                        $actmodel_sender->message = $message;
                        $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                        $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                        if ($actmodel_sender->save()) {
                            $actmodel_rec = new Activities();
                            $actmodel_rec->user_id = $to;
                            $actmodel_rec->type = "message";
                            $actmodel_rec->model_id = $conmsgmodel->id;
                            $actmodel_rec->model_type = "ConversationMessage";
                            $actmodel_rec->subject = "You received a message in conversation: " . $subject;
                            $actmodel_rec->message = $message;
                            $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                            $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                            $actmodel_rec->save();
                        }
                    }
                }
            }
        }
    }
public function actionalumini()
    {
        $this->layout ='admin';        
        $model = new Users('search');
        if(Yii::app()->request->getParam('export')) {
            $this->actionExport();
            Yii::app()->end();
        }
        $this->render('alumini',array('model'=>$model));
    }
    
   /* public function actionexportAlumini()
    {
        $model = new Users('search');
        
        $mPDF1 = Yii::app()->ePdf->mpdf();
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
        $stylesheet = file_get_contents(Yii::app()->getBaseUrl(true) . '/themes/Dev/css/style.css');
        $mPDF1->WriteHTML($stylesheet, 1);
        $mPDF1->WriteHTML($this->renderPartial('alumini-grid', array('model' => $model), true));
        $mPDF1->Output('alumini.pdf', EYiiPdf::OUTPUT_TO_DOWNLOAD);
    }*/
    
       public function actionexportAlumini()
    {
        $model = new Users('search');
        
        $mPDF1 = Yii::app()->ePdf->mpdf();
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
       // $stylesheet = file_get_contents(Yii::app()->getBaseUrl(true) . '/themes/Dev/css/style.css');
        $curl_handle=curl_init();
curl_setopt($curl_handle, CURLOPT_URL,Yii::app()->getBaseUrl(true) . '/themes/Dev/css/style.css');
curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl_handle, CURLOPT_USERAGENT, 'cvvid');
$stylesheet = curl_exec($curl_handle);
curl_close($curl_handle);

        $mPDF1->WriteHTML($stylesheet, 1);
        $mPDF1->WriteHTML($this->renderPartial('alumini-grid', array('model' => $model), true));
        $mPDF1->Output('alumini.pdf', EYiiPdf::OUTPUT_TO_DOWNLOAD);
    }
    
    public function actionExport()
    {
        $fp = fopen('php://temp', 'w');
        
        /*
         * Write a header of csv file
         */
        $headers = array(
            'd_date',
            'client.clientFirstName',
            'client.clientLastName',
            'd_time',
        );
        $row = array();
        foreach($headers as $header) {
            $row[] = Users::model()->getAttributeLabel($header);
        }
        fputcsv($fp,$row);
        
        /*
         * Init dataProvider for first page
         */
        $model=new Users('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Users'])) {
            $model->attributes=$_GET['MODEL'];
        }
        $dp = $model->searchAlumini();
        
        /*
         * Get models, write to a file, then change page and re-init DataProvider
         * with next page and repeat writing again
         */
        while($models = $dp->getData()) {
            foreach($models as $model) {
                $row = array();
                foreach($headers as $head) {
                    $row[] = CHtml::value($model,$head);
                }
                fputcsv($fp,$row);
            }
            
            unset($model,$dp,$pg);
            $model=new Users('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['Users']))
                $model->attributes=$_GET['Users'];
                
                $dp = $model->search();
                $nextPage = $dp->getPagination()->getCurrentPage()+1;
                $dp->getPagination()->setCurrentPage($nextPage);
        }
        
        /*
         * save csv content to a Session
         */
        rewind($fp);
        Yii::app()->user->setState('export',stream_get_contents($fp));
        fclose($fp);
    }
    
    public function actionGetExportFile()
    {
        Yii::app()->request->sendFile('export.csv',Yii::app()->user->getState('export'));
        Yii::app()->user->clearState('export');
    }
    
  public function beforeAction($action)
    {
        //Yii::app()->user->setReturnUrl(Yii::app()->request->getUrl());
        $requestUrl=Yii::app()->request->url;
       if(isset($_SERVER['HTTPS'])){
            $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
        }
        else{
            $protocol = 'http';
        }
        $canurl = $protocol."://".$_SERVER['SERVER_NAME'].$requestUrl;
      
        $this->_canonicalUrl = $canurl;
        
        return parent::beforeAction($action);
    } 

    public function actiongetSkillCategory() {
        $loc = "";
        if(isset($_POST['loc']) && $_POST['loc'] != "")
        {
//             $arr = explode(",", $_POST['loc']);
//             $arr1 = explode(" ", implode("-", array_map('trim',$arr)));
//             $trim_arr = array_map('trim',$arr1);

            $loc = explode("/", $_POST['loc'])[0];
            $first = explode(",", $loc)[0];
            $loc = str_replace(" ", "-", $first);
            // $loc = $_POST['loc'];//implode("-", $trim_arr);
        }
        if($_POST['id'] <= 0)
        {
            if($loc != "")
                echo Yii::app()->createUrl('site/searchJobs',array('title'=>"all",'loc'=>$loc));
            else
                echo Yii::app()->createUrl('site/searchJobs',array('title'=>"all",'loc'=>"all"));
        }
        else{
            
           
            $scmodel = SkillCategories::model()->findByPk($_POST['id']);
            $str = str_replace("/","-",str_replace(' ', "-", $scmodel->name));
            
            if($loc != "")
                echo Yii::app()->createUrl('site/searchJobs',array('title'=>$str,'loc'=>$loc));
            else
                echo Yii::app()->createUrl('site/searchJobs',array('title'=>$str,'loc'=>"all"));
                
//             echo Yii::app()->createUrl('site/searchJobs',array('title'=>$str,'loc'=>$loc));
        }
        
        
    }
   public function actioncronsitemap()
    {
        
        include Yii::app()->basePath."/components/SitemapGenerator.php";
        
        $sitemap = new SitemapGenerator(Yii::app()->params['DOMAIN_NAME']);        
        $sitemap->sitemapFileName = "category-sitemap.xml";
        $sitemap->sitemapIndexFileName = "category-sitemap-index.xml";
        
        // add urls
        $scmodel = SkillCategories::model()->findAll();
        $i = 1;
        $scname = 0;
        $j = 0;
        foreach ($scmodel as $sc)
        {
            
            if($i % 2000 == 0)
            {
                $sitemap->createSitemap();
                $sitemap->writeSitemap();
                
                $scname++;
//                 $j = $i;
                $sitemap = new SitemapGenerator(Yii::app()->params['DOMAIN_NAME']);        
                $sitemap->sitemapFileName = "category-sitemap-".$scname.".xml";
                $sitemap->sitemapIndexFileName = "category-sitemap-index-".$scname.".xml";
            }
           
            $str = str_replace("/","-",str_replace(' ', "-", $sc['name']));        
            $sitemap->addUrl(Yii::app()->params['DOMAIN_NAME']."/joblist/jobs-for-".$str."-in-all",   date('c'),  'daily',    '0.8');
            
//             if($i == ($j + 2000))
//             {
//                 $sitemap->createSitemap();
//                 $sitemap->writeSitemap();
//             }
            
            $i++;
        }
        
        // create sitemap
//         $sitemap->createSitemap();
        
        // write sitemap as file
//         $sitemap->writeSitemap();
        
        
        
        $jsitemap = new SitemapGenerator(Yii::app()->params['DOMAIN_NAME']);
        
        $jsitemap->sitemapFileName = "search-sitemap.xml";
        $jsitemap->sitemapIndexFileName = "search-sitemap-index.xml";
        // add urls
        
        
        $cri = new CDbCriteria();
        $cri->condition = "status = 'active' and end_date >= '".date('Y-m-d')."' and careers <= 0";
        
        $i = 1;
        $scname = 0;
        
        $jobmodel = Jobs::model()->findAll($cri);
        foreach ($jobmodel as $job)
        {
            if($i % 2000 == 0)
            {
                $jsitemap->createSitemap();
                $jsitemap->writeSitemap();
                
                $scname++;
                //                 $j = $i;
                $jsitemap = new SitemapGenerator(Yii::app()->params['DOMAIN_NAME']);
                $jsitemap->sitemapFileName = "search-sitemap-".$scname.".xml";
                $jsitemap->sitemapIndexFileName = "search-sitemap-index-".$scname.".xml";
                
            }
            
            $str1 = str_replace('/','-',str_replace(' ', '-', $job['title']))."-".$job['id'];
            $jsitemap->addUrl(Yii::app()->params['DOMAIN_NAME']."/job/".$str1,   date('c'),  'daily',    '0.8');
            $i++;
        }
        
        // create sitemap
       // $jsitemap->createSitemap();
        
        // write sitemap as file
      //  $jsitemap->writeSitemap();
        
        
        
        //all jobs location wise
        
        $jsitemap = new SitemapGenerator(Yii::app()->params['DOMAIN_NAME']);
        
        $jsitemap->sitemapFileName = "search-location-sitemap.xml";
        $jsitemap->sitemapIndexFileName = "search-location-sitemap-index.xml";
                
        $cri = new CDbCriteria();
        $cri->condition = "status = 'active' and end_date >= '".date('Y-m-d')."' and careers <= 0";
        
        $locjobmodel = Jobs::model()->findAll($cri);
        $locarr = array();
        foreach ($locjobmodel as $locjob)
        {
            $locarr[] = $locjob['location'];
        }
        
        $i = 1;
        $scname = 0;
        
        foreach ($jobmodel as $job)
        {
            foreach ($locarr as $loc)
            {
                if($job['location'] != $loc)
                {
                    
                    if($i % 2000 == 0)
                    {
                        $jsitemap->createSitemap();
                        $jsitemap->writeSitemap();
                        
                        $scname++;
                        //                 $j = $i;
                        $jsitemap = new SitemapGenerator(Yii::app()->params['DOMAIN_NAME']);
                        $jsitemap->sitemapFileName = "search-location-sitemap-".$scname.".xml";
                        $jsitemap->sitemapIndexFileName = "search-location-sitemap-index-".$scname.".xml";
                        
                    }
                    
                    $loc = explode("/",$loc)[0];
                    $first = explode(",", $loc)[0];
                    $loc = str_replace(" ", "-", $first);
                    
                    $jsitemap->addUrl(Yii::app()->params['DOMAIN_NAME']."/joblist/jobs-for-all-in-".$loc,   date('c'),  'daily',    '0.8');
                    $i++;
                }
                
            }
        }
        
//         $jsitemap->createSitemap();        
//         $jsitemap->writeSitemap();
        
        
        
        //per job all location wise
        
//         $jsitemap = new SitemapGenerator(Yii::app()->params['DOMAIN_NAME']);
        
//         $jsitemap->sitemapFileName = "search-location-sitemap.xml";
//         $jsitemap->sitemapIndexFileName = "search-location-sitemap-index.xml";
        
//         $cri = new CDbCriteria();
//         $cri->condition = "status = 'active' and end_date >= '".date('Y-m-d')."' and careers <= 0";
        
//         $locjobmodel = Jobs::model()->findAll($cri);
//         $locarr = array();
//         foreach ($locjobmodel as $locjob)
//         {
//             $locarr[] = $locjob['location'];
//         }
        
        
//         foreach ($jobmodel as $job)
//         {
//             foreach ($locarr as $loc)
//             {
//                 if($job['location'] != $loc)
//                     $jsitemap->addUrl(Yii::app()->params['DOMAIN_NAME']."/joblist/all?loc=".$loc,   date('c'),  'daily',    '0.8');
//             }
//         }
        
//         $jsitemap->createSitemap();
//         $jsitemap->writeSitemap();
        
        
        //per category all location
        
        $jsitemap = new SitemapGenerator(Yii::app()->params['DOMAIN_NAME']);
        
        $jsitemap->sitemapFileName = "per-category-all-location-sitemap.xml";
        $jsitemap->sitemapIndexFileName = "per-category-all-location-sitemap-index.xml";
        
        $i = 1;
        $scname = 0;
        
        $scmodel = SkillCategories::model()->findAll();
        foreach ($scmodel as $sc)
        {
            
            $categ = str_replace("/","-",str_replace(' ', "-", $sc['name']));
            foreach ($locarr as $loc)
            {
                if($i % 2000 == 0)
                {
                    $jsitemap->createSitemap();
                    $jsitemap->writeSitemap();
                    
                    $scname++;
                    //                 $j = $i;
                    $jsitemap = new SitemapGenerator(Yii::app()->params['DOMAIN_NAME']);
                    $jsitemap->sitemapFileName = "per-category-all-location-sitemap-".$scname.".xml";
                    $jsitemap->sitemapIndexFileName = "per-category-all-location-sitemap-index-".$scname.".xml";
                }
                
                $loc = explode("/",$loc)[0];
                $first = explode(",", $loc)[0];
                $loc = str_replace(" ", "-", $first);
                
                $jsitemap->addUrl(Yii::app()->params['DOMAIN_NAME']."/joblist/jobs-for-".$categ."-in-".$loc,   date('c'),  'daily',    '0.8');
               $i++;
            }
        }
//         $jsitemap->createSitemap();
//         $jsitemap->writeSitemap();
        
        
        //company sitemap
        
        $jsitemap = new SitemapGenerator(Yii::app()->params['DOMAIN_NAME']);
        
        $jsitemap->sitemapFileName = "company-sitemap.xml";
        $jsitemap->sitemapIndexFileName = "company-sitemap-index.xml";
        
        $empmodel = Employers::model()->findAll();
        
        $i = 1;
        $scname = 0;
        foreach ($empmodel as $emp)
        {
            if($i % 2000 == 0)
            {
                $jsitemap->createSitemap();
                $jsitemap->writeSitemap();
                
                $scname++;
                //                 $j = $i;
                $jsitemap = new SitemapGenerator(Yii::app()->params['DOMAIN_NAME']);
                $jsitemap->sitemapFileName = "company-sitemap-".$scname.".xml";
                $jsitemap->sitemapIndexFileName = "company-sitemap-index-".$scname.".xml";
                
            }
            $string = str_replace(' ', '-', trim($emp['name']));
            $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); 
            $compname = strtolower($string);
                
            $jsitemap->addUrl(Yii::app()->params['DOMAIN_NAME']."/site/jobs-by-".$compname,   date('c'),  'daily',    '0.8');
            $i++;
        }
//         $jsitemap->createSitemap();
//         $jsitemap->writeSitemap();
        
        //skills sitemap
        
        $jsitemap = new SitemapGenerator(Yii::app()->params['DOMAIN_NAME']);
        
        $jsitemap->sitemapFileName = "skills-sitemap.xml";
        $jsitemap->sitemapIndexFileName = "skills-sitemap-index.xml";
        
        $i = 1;
        $scname = 0;
        $skillmodel = Skills::model()->findAll();
        foreach ($skillmodel as $skill){
            
            if($i % 2000 == 0)
            {
                $jsitemap->createSitemap();
                $jsitemap->writeSitemap();
                
                $scname++;
                //                 $j = $i;
                $jsitemap = new SitemapGenerator(Yii::app()->params['DOMAIN_NAME']);
                $jsitemap->sitemapFileName = "skills-sitemap-".$scname.".xml";
                $jsitemap->sitemapIndexFileName = "skills-sitemap-index-".$scname.".xml";
                
            }
            
            $smodel = Skills::model()->findByPk($skill['id']);
            
            $string = str_replace(' ', '-', trim($smodel->name)); 
            $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); 
            $skilname = strtolower($string);
            
            $jsitemap->addUrl(Yii::app()->params['DOMAIN_NAME']."/site/job-for-".$skilname,   date('c'),  'daily',    '0.8');
            $i++;
        }
        
//         $jsitemap->createSitemap();
//         $jsitemap->writeSitemap();
    }
  /*   public function actioncronsitemap()
    {
        
        include Yii::app()->basePath."/components/SitemapGenerator.php";
        
        $sitemap = new SitemapGenerator(Yii::app()->params['DOMAIN_NAME']);
        
        $sitemap->sitemapFileName = "category-sitemap.xml";
        $sitemap->sitemapIndexFileName = "category-sitemap-index.xml";
        // add urls
        $scmodel = SkillCategories::model()->findAll();
        foreach ($scmodel as $sc)
        {
            $str = str_replace("/","-",str_replace(' ', "-", $sc['name']));        
            $sitemap->addUrl(Yii::app()->params['DOMAIN_NAME']."/joblist/jobs-for-".$str."-in-all",   date('c'),  'daily',    '0.8');
        }
        
        // create sitemap
        $sitemap->createSitemap();
        
        // write sitemap as file
        $sitemap->writeSitemap();
        
        
        
        $jsitemap = new SitemapGenerator(Yii::app()->params['DOMAIN_NAME']);
        
        $jsitemap->sitemapFileName = "search-sitemap.xml";
        $jsitemap->sitemapIndexFileName = "search-sitemap-index.xml";
        // add urls
        
        
        $cri = new CDbCriteria();
        $cri->condition = "status = 'active' and end_date >= '".date('Y-m-d')."' and careers <= 0";
        
        $jobmodel = Jobs::model()->findAll($cri);
        foreach ($jobmodel as $job)
        {
            $str1 = str_replace('/','-',str_replace(' ', '-', $job['title']))."-".$job['id'];
            $jsitemap->addUrl(Yii::app()->params['DOMAIN_NAME']."/job/".$str1,   date('c'),  'daily',    '0.8');
        }
        
        // create sitemap
        $jsitemap->createSitemap();
        
        // write sitemap as file
        $jsitemap->writeSitemap();
        
        
        
        //all jobs location wise
        
        $jsitemap = new SitemapGenerator(Yii::app()->params['DOMAIN_NAME']);
        
        $jsitemap->sitemapFileName = "search-location-sitemap.xml";
        $jsitemap->sitemapIndexFileName = "search-location-sitemap-index.xml";
                
        $cri = new CDbCriteria();
        $cri->condition = "status = 'active' and end_date >= '".date('Y-m-d')."' and careers <= 0";
        
        $locjobmodel = Jobs::model()->findAll($cri);
        $locarr = array();
        foreach ($locjobmodel as $locjob)
        {
            $locarr[] = $locjob['location'];
        }
        
        
        foreach ($jobmodel as $job)
        {
            foreach ($locarr as $loc)
            {
                if($job['location'] != $loc)
                {
                    $loc = explode("/",$loc)[0];
                    $first = explode(",", $loc)[0];
                    $loc = str_replace(" ", "-", $first);
                    
                    $jsitemap->addUrl(Yii::app()->params['DOMAIN_NAME']."/joblist/jobs-for-all-in-".$loc,   date('c'),  'daily',    '0.8');
                }
            }
        }
        
        $jsitemap->createSitemap();        
        $jsitemap->writeSitemap();
        
        
        
        //per job all location wise
        
//         $jsitemap = new SitemapGenerator(Yii::app()->params['DOMAIN_NAME']);
        
//         $jsitemap->sitemapFileName = "search-location-sitemap.xml";
//         $jsitemap->sitemapIndexFileName = "search-location-sitemap-index.xml";
        
//         $cri = new CDbCriteria();
//         $cri->condition = "status = 'active' and end_date >= '".date('Y-m-d')."' and careers <= 0";
        
//         $locjobmodel = Jobs::model()->findAll($cri);
//         $locarr = array();
//         foreach ($locjobmodel as $locjob)
//         {
//             $locarr[] = $locjob['location'];
//         }
        
        
//         foreach ($jobmodel as $job)
//         {
//             foreach ($locarr as $loc)
//             {
//                 if($job['location'] != $loc)
//                     $jsitemap->addUrl(Yii::app()->params['DOMAIN_NAME']."/joblist/all?loc=".$loc,   date('c'),  'daily',    '0.8');
//             }
//         }
        
//         $jsitemap->createSitemap();
//         $jsitemap->writeSitemap();
        
        
        //per category all location
        
        $jsitemap = new SitemapGenerator(Yii::app()->params['DOMAIN_NAME']);
        
        $jsitemap->sitemapFileName = "per-category-all-location-sitemap.xml";
        $jsitemap->sitemapIndexFileName = "per-category-all-location-sitemap-index.xml";
        
        $scmodel = SkillCategories::model()->findAll();
        foreach ($scmodel as $sc)
        {
            $categ = str_replace("/","-",str_replace(' ', "-", $sc['name']));
            foreach ($locarr as $loc)
            {
                $loc = explode("/",$loc)[0];
                $first = explode(",", $loc)[0];
                $loc = str_replace(" ", "-", $first);
                
                $jsitemap->addUrl(Yii::app()->params['DOMAIN_NAME']."/joblist/jobs-for-".$categ."-in-".$loc,   date('c'),  'daily',    '0.8');
            }
        }
        $jsitemap->createSitemap();
        $jsitemap->writeSitemap();
        
        
        //company sitemap
        
        $jsitemap = new SitemapGenerator(Yii::app()->params['DOMAIN_NAME']);
        
        $jsitemap->sitemapFileName = "company-sitemap.xml";
        $jsitemap->sitemapIndexFileName = "company-sitemap-index.xml";
        
        $empmodel = Employers::model()->findAll();
        
        foreach ($empmodel as $emp)
        {
            $string = str_replace(' ', '-', trim($emp['name']));
            $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); 
            $compname = strtolower($string);
                
            $jsitemap->addUrl(Yii::app()->params['DOMAIN_NAME']."/site/jobs-by-".$compname,   date('c'),  'daily',    '0.8');
            
        }
        $jsitemap->createSitemap();
        $jsitemap->writeSitemap();
        
        //skills sitemap
        
        $jsitemap = new SitemapGenerator(Yii::app()->params['DOMAIN_NAME']);
        
        $jsitemap->sitemapFileName = "skills-sitemap.xml";
        $jsitemap->sitemapIndexFileName = "skills-sitemap-index.xml";
        
        $skillmodel = Skills::model()->findAll();
        foreach ($skillmodel as $skill){
            $smodel = Skills::model()->findByPk($skill['id']);
            
            $string = str_replace(' ', '-', trim($smodel->name)); 
            $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); 
            $skilname = strtolower($string);
            
            $jsitemap->addUrl(Yii::app()->params['DOMAIN_NAME']."/site/job-for-".$skilname,   date('c'),  'daily',    '0.8');
        }
        
        $jsitemap->createSitemap();
        $jsitemap->writeSitemap();
    }
    */
    
    
 public function actionviewVideo($video)
    {
        
//         $cri = new CDbCriteria();
//         $cri->condition = "model_type like '%Video%' and model_id =" . $video['id'];
//         $mediamodel = Media::model()->find($cri);
        
        $video = str_replace("-", "", $video);
        $cri1 = new CDbCriteria();
        $cri1->condition = "LOWER(replace(name, ' ','')) LIKE '%".$video."%' OR LOWER(replace(name, '/','')) LIKE '%".$video."%' OR LOWER(replace(LOWER(replace(name, '/','')), ' ','')) LIKE '%".$video."%' OR LOWER(replace(name, ',','')) LIKE '%".$video."%' OR
        LOWER(replace(name, '\&','')) LIKE '%".$video."%' OR LOWER(replace(name, '&','')) LIKE '%".$video."%' OR LOWER(replace(replace(replace(name, '-',''),' ',''), ' - ','')) LIKE '%".$video."%' OR LOWER(replace(name, '-','')) LIKE '%".$video."%'";
                
        $vmodel = Videos::model()->find($cri1);

        $this->_pageTitle = "best video cv for ".$vmodel['name']." | cvvid.com";
        $this->_pageDescription = "CVVID is sharing this video as this is a successful ".$vmodel['name']." video CV, an opportunity for you to get ideas on how to deliver your video CV. The idea is to present yourself in the best way possible. Check out our other videos for more examples of successful videos and hints and tips on our youtube channel and on our website cvvid.com";
        
//         $cri = new CDbCriteria();
//         $cri->condition = "model_type like '%Video%' and model_id =" . $vmodel['id'];
//         $mediamodel = Media::model()->find($cri);
        
        $this->render('videoshowcase_view', array('videoid' => $vmodel['video_id'],'vname'=>$vmodel['name']));
    }
    
     public function actionterms()
    {
        $this->_pageTitle = "Terms & Conditions |  cvvid.com";
        $this->_pageDescription = "This terms of use policy (together with the documents referred to in it) tells you the terms of use on which you may make use of Our site, whether as a guest or a registered user. Use of Our site includes accessing, browsing, or registering to use Our site.";
        $this->render('terms', array());
    }
    public function actionprivacy()
    {
        $this->_pageTitle = "Privacy Policy | cvvid.com";
        $this->_pageDescription = "Know how we collect information from you, how we protect it and how we use it, and share all personal information which you provide when you use cvvid.com";
        $this->render('privacy', array());
    }
    public function actioncookie()
    {
        $this->_pageTitle = "Why Is COOKIE POLICY ? | cvvid.com";
        $this->_pageDescription = "A cookie is a small file of letters and numbers that We store on your browser or the hard drive of your computer if you agree. Cookies contain information that is transferred to your computer's hard drive.";
        $this->render('cookie', array());
    }
    
    public function actionsearchSkills($skills) {
     
        $this->_pageTitle = "Jobs Search  by Skills  | cvvid.com";
        $this->_pageDescription = "Jobs Search  by Skills on cvvid.com. Register  to apply online for Job Vacancies on basis of your skill";
        
        
        $skillname = str_replace("-", "", $skills);
        $cri1 = new CDbCriteria();
        $cri1->condition = "LOWER(replace(name, ' ','')) LIKE '%".$skillname."%' OR LOWER(replace(name, '/','')) LIKE '%".$skillname."%' OR LOWER(replace(name, ',','')) LIKE '%".$skillname."%' OR
        LOWER(replace(name, '\&','')) LIKE '%".$skillname."%' OR LOWER(replace(name, '&','')) LIKE '%".$skillname."%'";
        $smodel = Skills::model()->find($cri1);
      
        $skillmodel = Skills::model()->findByPk($smodel['id']);
        $scategmodel = SkillCategories::model()->findByPk($skillmodel->skill_category_id);
        $skillcategid = $scategmodel->id;
        $skillids = $smodel['id'];
        
        $cri = new CDbCriteria();
        $cri->alias = "j";
        $cri->join = "left outer join cv16_job_skill js on js.job_id = j.id
                      left outer join cv16_skills s on s.id = js.skill_id
                      left outer join cv16_skill_categories sc on sc.id = j.skill_category_id";
        
            if ($skillids != "")
                $cri->addCondition('js.skill_id in (' . $skillids . ')', 'AND');
              
                
                $cri->addCondition("j.status ='active'");               
                $cri->addCondition("j.end_date >= '".date('Y-m-d')."'");
                $cri->addCondition("j.careers <= 0");
                $cri->group = "j.id";
                
                $order = "";
                $cri->order = "j.created_at desc";
                
              
                $visitor_country = $this->ip_info("Visitor", "Country");
                
                $cri->addCondition("TRIM(substring_index(j.location, ',', -1)) like '%".$visitor_country."%'");
                  
                
                $jobmodel = Jobs::model()->findAll($cri);
                
                if(count($jobmodel) <= 0)
                    $jobmodel = array();
                    
                    $this->render('jobsearchresults', array('model' => $jobmodel,'job_name'=>'','job_skill_category'=>$skillcategid,
                        'job_skills'=>array($skillids),'job_location'=>'','job_latitude'=>'',
                        'job_longitude'=>'','job_jobtype'=>'','job_salary'=>'','job_distance'=>'',
                        'job_a_z'=>'','job_z_a'=>'',"job_recent"=>'',"job_old"=>''
                        
                    ));
    }
    
    public function actionsearchCategory($category) {
        
        $this->_pageTitle = "Jobs Search  by category | cvvid.com ";
        $this->_pageDescription = "Jobs Search  by category on cvvid.com. Register  to apply online for Job Vacancies on basis of your skill";
        
        $catname = str_replace("-", "", $category);
        $cri1 = new CDbCriteria();
        $cri1->condition = "LOWER(replace(name, ' ','')) LIKE '%".$catname."%' OR LOWER(replace(name, '/','')) LIKE '%".$catname."%' OR LOWER(replace(name, ',','')) LIKE '%".$catname."%' OR 
        LOWER(replace(name, '\&','')) LIKE '%".$catname."%' OR LOWER(replace(name, '&','')) LIKE '%".$catname."%'";
        $catmodel = SkillCategories::model()->find($cri1);
        $scategmodel = SkillCategories::model()->findByPk($catmodel['id']);
        $skillcategid = $scategmodel->id;
        $skillids = array();
        
        $cri = new CDbCriteria();
        $cri->alias = "j";
        $cri->join = "left outer join cv16_job_skill js on js.job_id = j.id
                      left outer join cv16_skills s on s.id = js.skill_id
                      left outer join cv16_skill_categories sc on sc.id = j.skill_category_id";
        
        if ($skillcategid > 0)
            $cri->addCondition("j.skill_category_id =" . $skillcategid);
            
            
            $cri->addCondition("j.status ='active'");
            $cri->addCondition("j.end_date >= '".date('Y-m-d')."'");
            $cri->addCondition("j.careers <= 0");
            $cri->group = "j.id";
            
            $order = "";
            $cri->order = "j.created_at desc";
            
            
            $visitor_country = $this->ip_info("Visitor", "Country");
            
            $cri->addCondition("TRIM(substring_index(j.location, ',', -1)) like '%".$visitor_country."%'");
            
            
            $jobmodel = Jobs::model()->findAll($cri);
            
            if(count($jobmodel) <= 0)
                $jobmodel = array();
                
                $this->render('jobsearchresults', array('model' => $jobmodel,'job_name'=>'','job_skill_category'=>$skillcategid,
                    'job_skills'=>array(),'job_location'=>'','job_latitude'=>'',
                    'job_longitude'=>'','job_jobtype'=>'','job_salary'=>'','job_distance'=>'',
                    'job_a_z'=>'','job_z_a'=>'',"job_recent"=>'',"job_old"=>''
                    
                ));
    }
    
    public function actionsearchLocation($location) {
        
        $this->_pageTitle = "Best Online interview platform in UK | cvvid.com";
        $this->_pageDescription = "Search  jobs with best online interview platform in UK - cvvid.com";
        
        $skillids = array();
        
        $cri = new CDbCriteria();
        $cri->alias = "j";
        $cri->join = "left outer join cv16_job_skill js on js.job_id = j.id
                      left outer join cv16_skills s on s.id = js.skill_id
                      left outer join cv16_skill_categories sc on sc.id = j.skill_category_id";
        
       
            
            
            $cri->addCondition("j.status ='active'");
            $cri->addCondition("j.end_date >= '".date('Y-m-d')."'");
            $cri->addCondition("j.careers <= 0");
            $cri->group = "j.id";
            
            $order = "";
            $cri->order = "j.created_at desc";
            
            
            $visitor_country = $this->ip_info("Visitor", "Country");
            
           // $firstloc = explode(",", urldecode($location))[0];
          //  $firstloc = str_replace("-", $replace, $subject)
            
           
            if($location != "")
                $cri->addCondition("LOWER(replace(LEFT(location,LOCATE(' ',j.location) - 2), ' ','')) like '%".$location."%'");
            else
                $cri->addCondition("TRIM(substring_index(j.location, ',', -1)) like '%".$visitor_country."%'");
           
            
            
            $jobmodel = Jobs::model()->findAll($cri);
            
            if(count($jobmodel) <= 0)
                $jobmodel = array();
                
                $this->render('jobsearchresults', array('model' => $jobmodel,'job_name'=>'','job_skill_category'=>'',
                    'job_skills'=>array(),'job_location'=>$location,'job_latitude'=>'',
                    'job_longitude'=>'','job_jobtype'=>'','job_salary'=>'','job_distance'=>'',
                    'job_a_z'=>'','job_z_a'=>'',"job_recent"=>'',"job_old"=>''
                    
                ));
    }
    
    public function actionsearchCompany($company) {
        
        $this->_pageTitle = "Jobs Search  by Company - Jobs in Top Companies | cvvid.com";
        $this->_pageDescription = "Jobs Search  by Top  Companies on cvvid.com. Register  to apply online for Job Vacancies across Top Companies in UK";
        
        $skillids = array();
        $compname = str_replace("-", "", $company);
        
        $ecri = new CDbCriteria();
        $ecri->condition = "LOWER(replace(name, ' ','')) LIKE '%".$compname."%' OR LOWER(replace(name, '/','')) LIKE '%".$compname."%' OR LOWER(replace(name, ',','')) LIKE '%".$compname."%' OR
        LOWER(replace(name, '\&','')) LIKE '%".$compname."%' OR LOWER(replace(name, '&','')) LIKE '%".$compname."%'";
        $emodel = Employers::model()->find($ecri);
        
        $id = $emodel['id'];
        
       
        $cri = new CDbCriteria();
        $cri->alias = "j";
        $cri->join = "left outer join cv16_job_skill js on js.job_id = j.id
                      left outer join cv16_skills s on s.id = js.skill_id
                      left outer join cv16_skill_categories sc on sc.id = j.skill_category_id";
        
        
       $cri->addCondition("j.owner_id =".$id);
        
        $cri->addCondition("j.status ='active'");
        $cri->addCondition("j.end_date >= '".date('Y-m-d')."'");
        $cri->addCondition("j.careers <= 0");
        $cri->group = "j.id";
        
        $order = "";
        $cri->order = "j.created_at desc";
        
        
        $visitor_country = $this->ip_info("Visitor", "Country");
        
        $cri->addCondition("TRIM(substring_index(j.location, ',', -1)) like '%".$visitor_country."%'");
        
        
        $jobmodel = Jobs::model()->findAll($cri);
        
        if(count($jobmodel) <= 0)
            $jobmodel = array();
            
            $this->render('jobsearchresults', array('model' => $jobmodel,'job_name'=>'','job_skill_category'=>'',
                'job_skills'=>array(),'job_location'=>'','job_latitude'=>'',
                'job_longitude'=>'','job_jobtype'=>'','job_salary'=>'','job_distance'=>'',
                'job_a_z'=>'','job_z_a'=>'',"job_recent"=>'',"job_old"=>''
                
            ));
    }
     function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
        $output = NULL;
        if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
            $ip = $_SERVER["REMOTE_ADDR"];
            if ($deep_detect) {
                if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                    if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                        $ip = $_SERVER['HTTP_CLIENT_IP'];
            }
        }
        $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
        $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
        $continents = array(
            "AF" => "Africa",
            "AN" => "Antarctica",
            "AS" => "Asia",
            "EU" => "Europe",
            "OC" => "Australia (Oceania)",
            "NA" => "North America",
            "SA" => "South America"
        );
        if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
            $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
            if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
                switch ($purpose) {
                    case "location":
                        $output = array(
                        "city"           => @$ipdat->geoplugin_city,
                        "state"          => @$ipdat->geoplugin_regionName,
                        "country"        => @$ipdat->geoplugin_countryName,
                        "country_code"   => @$ipdat->geoplugin_countryCode,
                        "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                        "continent_code" => @$ipdat->geoplugin_continentCode
                        );
                        break;
                    case "address":
                        $address = array($ipdat->geoplugin_countryName);
                        if (@strlen($ipdat->geoplugin_regionName) >= 1)
                            $address[] = $ipdat->geoplugin_regionName;
                            if (@strlen($ipdat->geoplugin_city) >= 1)
                                $address[] = $ipdat->geoplugin_city;
                                $output = implode(", ", array_reverse($address));
                                break;
                    case "city":
                        $output = @$ipdat->geoplugin_city;
                        break;
                    case "state":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "region":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "country":
                        $output = @$ipdat->geoplugin_countryName;
                        break;
                    case "countrycode":
                        $output = @$ipdat->geoplugin_countryCode;
                        break;
                }
            }
        }
        return $output;
    }
    
    public function actiondigitalpassport()
    {
        $this->layout ='admin';
        $model = new Users('search');
        if(Yii::app()->request->getParam('export')) {
            $this->actionExport();
            Yii::app()->end();
        }
        $this->render('digitalpassport', array('model'=>$model));
    }
    
    public function actionexportPassport()
    {
        $model = new Users('search');
        
        $mPDF1 = Yii::app()->ePdf->mpdf();
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
        // $stylesheet = file_get_contents(Yii::app()->getBaseUrl(true) . '/themes/Dev/css/style.css');
        
        $curl_handle=curl_init();
        curl_setopt($curl_handle, CURLOPT_URL,Yii::app()->getBaseUrl(true) . '/themes/Dev/css/style.css');
        curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, 2);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl_handle, CURLOPT_USERAGENT, 'cvvid');
        $stylesheet = curl_exec($curl_handle);
        curl_close($curl_handle);
        
        $mPDF1->WriteHTML($stylesheet, 1);
        $mPDF1->WriteHTML($this->renderPartial('digital-passport-grid', array('model' => $model), true));
        $mPDF1->Output('digital_passport.pdf', EYiiPdf::OUTPUT_TO_DOWNLOAD);
    }
    
    public function actionExportDigitalPassport()
    {
        $fp = fopen('php://temp', 'w');
        
        /*
         * Write a header of csv file
         */
        $headers = array(
            'd_date',
            'client.clientFirstName',
            'client.clientLastName',
            'd_time',
        );
        $row = array();
        foreach($headers as $header) {
            $row[] = Users::model()->getAttributeLabel($header);
        }
        fputcsv($fp,$row);
        
        /*
         * Init dataProvider for first page
         */
        $model=new Users('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Users'])) {
            $model->attributes=$_GET['MODEL'];
        }
        $dp = $model->searchPassport();
        
        /*
         * Get models, write to a file, then change page and re-init DataProvider
         * with next page and repeat writing again
         */
        while($models = $dp->getData()) {
            foreach($models as $model) {
                $row = array();
                foreach($headers as $head) {
                    $row[] = CHtml::value($model,$head);
                }
                fputcsv($fp,$row);
            }
            
            unset($model,$dp,$pg);
            $model=new Users('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['Users']))
                $model->attributes=$_GET['Users'];
                
                $dp = $model->search();
                $nextPage = $dp->getPagination()->getCurrentPage()+1;
                $dp->getPagination()->setCurrentPage($nextPage);
        }
        
        /*
         * save csv content to a Session
         */
        rewind($fp);
        Yii::app()->user->setState('digital_passport',stream_get_contents($fp));
        fclose($fp);
    }
    
    public function actionGetPassportExportFile()
    {
        Yii::app()->request->sendFile('digital_passport.csv',Yii::app()->user->getState('digital_passport'));
        Yii::app()->user->clearState('digital_passport');
    }
}
