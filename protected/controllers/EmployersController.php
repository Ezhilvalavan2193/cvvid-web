<?php
use Stripe\Charge;
use Stripe\Invoice;
use Stripe\InvoiceItem;
use Stripe\Stripe;
use Stripe\Token;
use Stripe\Customer;
use Stripe\Plan;
use Stripe\Subscription;
use Stripe\Coupon;

class EmployersController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/admin';
    public $employerid = 0;
    public $edit = 0;

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
//          array('deny',  // deny all users
//              'users'=>array('*'),
//          ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionupgrade() {
        $this->layout = 'main';
        $this->render('upgrade');
    }
    
     /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionmysubscription() {
        $this->layout = 'main';
        $this->render('subscription');
    }

    public function actiondeleteEmployer($id) {

        $now = new DateTime();

        $empmodel = Employers::model()->findByPk($id);

        $empmodel->deleted_at = $now->format('Y-m-d H:i:s');
        $empmodel->save();

        $usermodel = Users::model()->findByPk($empmodel->user_id);
        $usermodel->deleted_at = $now->format('Y-m-d H:i:s');
        $usermodel->save();

        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $id, 'type' => 'employer'));
        $profilemodel->deleted_at = $now->format('Y-m-d H:i:s');
        $profilemodel->save();

        $cri = new CDbCriteria();
        $cri->condition = "model_id = ".$id." and model_type like '%employer%' and deleted_at is null";
        
        $addressmodel = Addresses::model()->find($cri);
        
        $addressmodel->deleted_at = $now->format('Y-m-d H:i:s');
        $addressmodel->save();

        $this->redirect($this->createUrl('employers/admin'));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Employers;
        $usermodel = Users::model();
        $addressmodel = Addresses::model();
        $profilemodel = Profiles::model();
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Employers'])) {
            $now = new DateTime();
            $model->attributes = $_POST['Employers'];
            $model->save();
        }

        $this->render('create', array('model' => $model, 'usermodel' => $usermodel, 'addressmodel' => $addressmodel, 'profilemodel' => $profilemodel));
    }

    /**
     * Manages all models.
     */
    public function actionsearchAdmin() {
        $model = new Employers('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Employers'])) {
            $model->attributes = $_GET['Employers'];
        }

        $this->render('admin', array(
            'model' => $model, 'issearch' => 1));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actioncreateEmployerDetails() {

        $model = new Employers;
        $empusermodel = new EmployerUsers;
        $usermodel = new Users;
        $profilemodel = new Profiles;
        $addressmodel = new Addresses;
        $now = new DateTime();
        
        if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
           $usermodel->setScenario('confirm');
        
        if(isset($_POST['Users'])){
           $usermodel->attributes=$_POST['Users'];
        }
        if(isset($_POST['Employers'])){
           $model->attributes=$_POST['Employers'];
        }
        if(isset ($_POST["Addresses"])){
            $addressmodel->attributes = $_POST["Addresses"]; 
        }
        if(isset ($_POST["Profiles"])){
            $profilemodel->attributes = $_POST["Profiles"];
        } 
        
        if(isset($_POST['Employers']))
        {
            
             $valid = $usermodel->validate();
             $valid = $model->validate() && $valid;
             $valid = $addressmodel->validate() && $valid;
             $valid = $profilemodel->validate() && $valid;
             
             if($valid){
                $usermodel->created_at = $now->format('Y-m-d H:i:s');
                $usermodel->type = 'employer';
                $usermodel->location = $_POST['Addresses']['county'].($_POST['Addresses']['county']!=''? ', '.$_POST['Addresses']['country']: '');
                $usermodel->location_lat = $_POST['Addresses']['latitude'];
                $usermodel->location_lng = $_POST['Addresses']['longitude'];
                $usermodel->tel = $_POST['Employers']['tel'];
                $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
                $usermodel->save();
//                insert employer details
                $model->email = $usermodel->email;
                $model->created_at = $now->format('Y-m-d H:i:s');
                $model->user_id = $usermodel->id;
                $model->body = $_POST['body'];
                if($model->save())
                {
                    //employer user update
                    $empusermodel->employer_id = $model->id;
                    $empusermodel->user_id = $usermodel->id;
                    $empusermodel->role = Yii::app()->user->getState('role');
                    $empusermodel->created_at = $now->format('Y-m-d H:i:s');
                    $empusermodel->save();
                }
//                profile model
                $profilemodel->owner_id = $model->id;
                $profilemodel->owner_type = 'employer';
                $profilemodel->type = 'employer';
                $profilemodel->slug = $_POST["Profiles"]['slug'];
                if ($_POST['photo_id'] > 0)
                    $profilemodel->photo_id = $_POST['photo_id'];
                $profilemodel->save();
                
                //address update
                $addressmodel->attributes = $_POST["Addresses"];
                $addressmodel->created_at = $now->format('Y-m-d H:i:s');
                $addressmodel->model_type = 'employer';
                $addressmodel->model_id = $model->id;
                if ($addressmodel->save()) {
                    Yii::app()->user->setFlash('success', 'Successfully created employer');
                    $this->redirect($this->createUrl('employers/tabedit', array('id' => $model->id)));
                }
             }
        }
        
        $this->render('create', array('model' => $model, 'usermodel' => $usermodel, 'addressmodel' => $addressmodel, 'profilemodel' => $profilemodel));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actioncreateEmployerUsers() {
           
           $this->layout='main';
           $model = Employers::model()->findByPk($_POST['employer_id']);
        
            $empusermodel = new EmployerUsers;
            $usermodel = new Users;
            $now = new DateTime();

            if(isset($_POST['Users'])){
               $usermodel->attributes=$_POST['Users'];
            }   
           if(isset($_POST['Users']))
           {
               if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
                   $usermodel->setScenario('confirm');
                $valid = $usermodel->validate();
                if($valid){
                     // insert user details
                    $usermodel->created_at = $now->format('Y-m-d H:i:s');
                    $usermodel->type = 'employer';
                    $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
                    if(isset($_POST['Addresses']['latitude']))
                    {
                        $usermodel->location_lat = $_POST['Addresses']['latitude'];
                        $usermodel->location_lng = $_POST['Addresses']['longitude'];
                    }
                    if ($usermodel->save()) {

                        //employer user update
                        $empusermodel->employer_id = $_POST['employer_id'];
                        $empusermodel->user_id = $usermodel->id;
                        $empusermodel->role = 'employer';
                        $empusermodel->created_at = $now->format('Y-m-d H:i:s');
                        if($empusermodel->save()){
                            $this->useraccountnotify($usermodel, $_POST['Users']['password']);
                        }
                        
                        // Send the notification email
                        // if (isset($_POST['inform_user'])) {
                           
                        // }
                        
                        Yii::app()->user->setFlash('success', 'Successfully added new user');
                        $this->redirect($this->createUrl('employers/users',array('id'=>$model->id)));
                        
                    }
                }
           }
           
           $this->render('usercreate', array('model' => $model,'usermodel'=>$usermodel));
       
    }
    
     /**
     * Notify the user
     */
    public function useraccountnotify($model, $password)
    {
        $body = $this->renderPartial("//emails/user-account",array('user' => $model, 'password' => $password),true);
        // echo $body;
        // die();
        // Send register notifcation to employer admin
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->Host = 'mail.cvvid.com';
        // $mail->IsSMTP();
        $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
        $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
        $mail->SMTPSecure = 'ssl';
        // $mail->Mailer    = 'smtp';
        // $mail->Port      = 465;
        // $mail->SMTPAuth = true;
        $mail->SetFrom('info@cvvid.com', 'CVVID');
        $mail->Subject = 'Your account details for CVVid.com';
        $mail->MsgHTML($body);
        $mail->AddAddress($model->email, $model->forenames);
        // echo $model->email;
        // die();
        $mail->Send();
        
        
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actioncreateUsers($id) {

        $model = Employers::model()->findByPk($id);
        $empusermodel = new EmployerUsers;
        $usermodel = new Users;
        $addressmodel = new Addresses;
        $now = new DateTime();
        
        if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
          $usermodel->setScenario('confirm');
        
        if(isset($_POST['Users'])){
           $usermodel->attributes=$_POST['Users'];
        }
        if(isset($_POST['Employers'])){
           $model->attributes=$_POST['Employers'];
        }
        if(isset ($_POST["Addresses"])){
            $addressmodel->attributes = $_POST["Addresses"]; 
        }
        
        if(isset($_POST['Users'])){
            
            $d = str_replace('/', '-', $_POST["dob"]);
            $dobformat = date('Y-m-d', strtotime($d));
            $usermodel->dob = $dobformat;
            $usermodel->created_at = $now->format('Y-m-d H:i:s');
            $usermodel->type = 'employer';
            $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
            $usermodel->location = $_POST['Addresses']['county'] . ($_POST['Addresses']['county'] != '' ? ', ' . $_POST['Addresses']['country'] : '');
            $usermodel->location_lat = $_POST['Addresses']['latitude'];
            $usermodel->location_lng = $_POST['Addresses']['longitude'];
            if($usermodel->save())
            {
                //employer user update
                $empusermodel->employer_id = $id;
                $empusermodel->user_id = $usermodel->id;
                $empusermodel->role = 'employer';
                $empusermodel->created_at = $now->format('Y-m-d H:i:s');
                $empusermodel->save();
            }
            //address update
            $addressmodel->created_at = $now->format('Y-m-d H:i:s');
            $addressmodel->model_type = 'employer';
            $addressmodel->model_id = $usermodel->id;
            if($addressmodel->save()){
                  Yii::app()->user->setFlash('success', 'Successfully created user');
                 $this->redirect($this->createUrl('employers/tabusers', array('id' =>$id)));
            }
            
        }
         $this->render('_employerusercreate', array(
            'model' => $model,'usermodel'=>$usermodel,'addressmodel'=>$addressmodel
        ));

    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionemployerUsersEdit($id) {

        $empusermodel = EmployerUsers::model()->findByPk($id);
        $usermodel = Users::model()->findByPk($empusermodel->user_id);
        $model = Employers::model()->findByPk($empusermodel->employer_id);
        $cri = new CDbCriteria();
        $cri->condition = "model_id = ".$usermodel->id." and model_type like '%employer%' and deleted_at is null";
        $addressmodel = Addresses::model()->find($cri);
        if($addressmodel == null)
            $addressmodel = new Addresses;
        
          $now = new DateTime();
          
          $pwd = $usermodel->password;
          
        if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
          $usermodel->setScenario('confirm');
        
        if (isset($_POST['Users'])) {
            $usermodel->attributes = $_POST['Users'];
        }
        if (isset($_POST["Addresses"])) {
            $addressmodel->attributes = $_POST["Addresses"];
        }
        
        if (isset($_POST['Users'])) {

//             if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
//                 $usermodel->setScenario('confirm');
            
            $valid = $usermodel->validate();
            $valid = $addressmodel->validate() && $valid;
    
            if ($valid) {
                 //      insert user details
                $d = str_replace('/', '-', $_POST["dob"]);
                $dobformat = date('Y-m-d', strtotime($d));
                $usermodel->dob = $dobformat;
                
                if ($_POST["Users"]["password"] == "")
                    $usermodel->password = $pwd;
                else 
                    $usermodel->password =  password_hash($_POST['Users']['password'], 1,['cost' => 10]);
                $usermodel->updated_at = $now->format('Y-m-d H:i:s');
                if ($usermodel->save()) {
//                    employer user
                    $empusermodel->updated_at = $now->format('Y-m-d H:i:s');
                    $empusermodel->save();
                }
                //address update
                $addressmodel->model_id = $usermodel->id;
                $addressmodel->model_type = 'employer';
                $addressmodel->updated_at = $now->format('Y-m-d H:i:s');
                if($addressmodel->save()){
                      Yii::app()->user->setFlash('success', 'Successfully updated user');
                      $this->redirect($this->createUrl('employers/tabusers', array('id' =>$model->id)));
                }
                
            }
        }
        $this->render('_employeruseredit', array('model' => $model,'eumodel'=>$empusermodel,'usermodel'=>$usermodel,'addressmodel'=>$addressmodel));

    }

    public function actionsaveBody() {
        $employerid = $_POST['employerid'];
      $body = nl2br(htmlentities($_POST['body'], ENT_QUOTES, 'UTF-8'));

        $model = Employers::model()->findByPk($employerid);

        $model->body = $body;
        $model->save();
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actioneditUsers($id) {

        $this->layout='main';
        $usermodel = Users::model()->findByPk($id);
        $empusrmodel = EmployerUsers::model()->findByAttributes(array('user_id'=>$usermodel->id));
        $empmodel = Employers::model()->findByPk($empusrmodel->employer_id);
        
        $pass = $usermodel->password;

        $now = new DateTime();
        
        if(isset($_POST['Users'])) {
            $usermodel->attributes = $_POST['Users'];
        }
        if (isset($_POST['Users'])) {
              if($_POST["Users"]['password']!="")
            {
                if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
                    $usermodel->setScenario('confirm');
                else
                    $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
            }
            else
                $usermodel->password = $pass;
            $valid = $usermodel->validate();
            if ($valid) {
                
                 $usermodel->created_at = $now->format('Y-m-d H:i:s');
                 $usermodel->type = 'employer';
                 if ($usermodel->save()) {
                    //employer user update
                    $empmodel->email = $_POST["Users"]['email'];
                    if($empmodel->save()){
                        $this->useraccountnotify($usermodel, $_POST['Users']['password']);
                    }
                    
                    // Send the notification email
                    // if ($_POST['inform_user']) {
                        
                    // }

                    Yii::app()->user->setFlash('success', 'Successfully updated profile');
                    $this->redirect(Yii::app()->createUrl('employers/users', array('id' => $empmodel->id)));

                }
            }
        }
        $this->render('useredit', array('usermodel'=>$usermodel,'empmodel'=>$empmodel));

        
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Employers'])) {
            $model->attributes = $_POST['Employers'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function actiondeleteEmployers() {
        $ids = implode(',', $_POST['ids']);

        $userids = explode(",", $ids);

        $usr_arr = array();
        foreach ($userids as $id) {
            $uid = Employers::model()->findByPk($id);
            $usr_arr[] = $uid->user_id;
        }

        $user_ids = implode(',', $usr_arr);

        $now = new DateTime();

        $cri = new CDbCriteria();
        $cri->condition = "id in (" . $ids . ")";
        Employers::model()->updateAll(array('deleted_at' => $now->format('Y-m-d H:i:s')), $cri);

        $cri1 = new CDbCriteria();
        $cri1->condition = "id in (" . $user_ids . ")";
        Users::model()->updateAll(array('deleted_at' => $now->format('Y-m-d H:i:s')), $cri1);

        $cri2 = new CDbCriteria();
        $cri2->condition = "owner_id in (" . $ids . ")";
        Profiles::model()->updateAll(array('deleted_at' => $now->format('Y-m-d H:i:s')), $cri2);

        $cri3 = new CDbCriteria();
        $cri3->condition = "model_id in (" . $ids . ")";
        Addresses::model()->updateAll(array('deleted_at' => $now->format('Y-m-d H:i:s')), $cri3);
    }

    public function actionupdatePublish() {
        $employerid = $_POST['employerid'];
        $status = $_POST['status'];

        $publish = 1;
        if ($status == 1) {
            $publish = 0;
        }

        $model = Employers::model()->findByPk($employerid);

        $model->published = $publish;
        $model->save();
    }

    public function actionchangeEmployerVisiblity() {
        $employerid = $_POST['employerid'];
        $status = $_POST['status'];

        $visiblity = 1;
        if ($status == 1) {
            $visiblity = 0;
        }

        $promodel = Profiles::model()->findByAttributes(array('owner_id' => $employerid, 'type' => 'employer'));

        $promodel->visibility = $visiblity;
        $promodel->save();
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Employers');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Employers('search');
        $empmodel = new Employers();
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Employers']))
            $model->attributes = $_GET['Employers'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

//        /**
//   * Manages all models.
//   */
//  public function actionEdit()
//  {
//      $model=new Employers('search');
//      $empmodel=new Employers();
//      $model->unsetAttributes();  // clear any default values
//      if(isset($_GET['Employers']))
//          $model->attributes=$_GET['Employers'];
//
//      $this->render('admin',array(
//          'model'=>$model,
//      ));
//  }



    public function actionEmployer($slug) {
        $this->layout = "main";
        $model = $this->loadModelSlug($slug);//Employers::model()->findByPk($id);
        $id = $model->id;
        $this->employerid = $id;
        //$model = Employers::model()->findByPk($id);
        $this->render('employer', array(
            'model' => $model,
        ));
    }

    public function actionemployeruserCreate($id) {
        $model = Employers::model()->findByPk($id);
        $usermodel = new Users();
        $addressmodel = new Addresses();
        $this->render('_employerusercreate', array(
            'model' => $model,'usermodel'=>$usermodel,'addressmodel'=>$addressmodel
        ));
    }

    public function actionEdit($slug) {
        if(Yii::app()->user->getState('role') != "employer")
            $this->redirect($this->createUrl('site/index'));
        
        $this->layout = "main";
        $model = $this->loadModelSlug($slug); //Employers::model()->findByPk($id);
        $id = $model->id;
        $this->employerid = $id;
        $this->edit = 1;
       
        
//         if($model->trial_ends_at != "" && $model->trial_ends_at != null)
//         {
//             $today = date('Y-m-d');
//             $date_of_expiry = date_format(new DateTime($model->trial_ends_at),"Y-m-d");
//             // $date_of_expiry = $model->trial_ends_at;
//             $datetime1 = new DateTime($today);
//             $datetime2 = new DateTime($date_of_expiry);
//             $interval = $datetime1->diff($datetime2);
//             $diff = $interval->format('%a');
//             if($diff <= 7)
//             {
//                 if($model->email != "")
//                 {
//                     $body = $this->renderPartial("//emails/employer-renew-subscription", array('model'=>$model), true);
//                     // Send employer viewed notifcation to candidate
//                     Yii::import('application.extensions.phpmailer.JPhpMailer');
//                     $mail = new JPhpMailer;
//                     $mail->Host = 'mail.cvvid.com';
//                     $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
//                     $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
//                     $mail->SMTPSecure = 'ssl';
//                     $mail->SetFrom('info@cvvid.com', 'CVVID');
//                     $mail->Subject = "Renew Subscription";
//                     $mail->MsgHTML($body);
//                     $mail->AddAddress($model->email, $model->name);
//                     $mail->Send();
                    
//     //                 if($diff > 0)
//     //                     Yii::app()->user->setFlash('subscription-ends','Your Subscription plan expired !');
//                 }
//             }
//         }
        
        $this->render('editemployer', array(
            'model' => $model,
        ));
    }
    
    public function actionjobsadd($slug) {
        
        $this->layout = "main";
        
        $model=new Jobs;
        
        $cri = new CDbCriteria();
        $cri->condition = "slug='".$slug."'";
        $profilemodel = Profiles::model()->find($cri);
    
        $empmodel = Employers::model()->findByPk($profilemodel->owner_id);
        $industrymodel = Industries::model();
        $skillcategmodel = SkillCategories::model();
        $skillsmodel = Skills::model();
        $jobskillmodel = JobSkill::model();
        
        $this->render('addjobs', array(
            'model' => $model,'empmodel' => $empmodel,'industrymodel'=>$industrymodel,'skillcategmodel'=>$skillcategmodel,'skillsmodel'=>$skillsmodel,
            'jobskillmodel'=>$jobskillmodel
        ));
    }
    
     public function actionjobsedit($id) {
        $this->layout = "main";
        $model = Jobs::model()->findByPk($id);
        $employermodel = Employers::model()->findByPk($model->owner_id);
        $jobskillmodel = JobSkill::model()->findAllByAttributes(array('job_id'=>$model->id));
        $this->render('editjobs',array('model'=>$model,'employermodel'=>$employermodel,'jobskillmodel'=>$jobskillmodel));
    }
    
     /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionupdateJobs($id)
    {
            $this->layout='main';
            $now = new DateTime();
            $model = Jobs::model()->findByPk($id);
            $empmodel = Employers::model()->findByPk($model->owner_id);
            $usermodel = Users::model()->findByPk($empmodel->user_id);
            $jobskillmodel = JobSkill::model()->findAllByAttributes(array('job_id'=>$model->id));
            
            if(isset($_POST['Jobs']))
            {
                $model->attributes = $_POST["Jobs"];
             if(!isset($_POST['skills']))
                 $model->setScenario("skillsrequired");
               if(isset($_POST['competitive_salary']))
             {
                 $model->competitive_salary = 1;
                 $model->salary_min = 0;
                 $model->salary_max = 0;
             }
             if ($model->validate()) {
                //  insert jobs details
                   
                    $model->industry_id = $_POST["industry_id"];
                    $d = str_replace('/','-',$_POST["end_date"]);
                    $endformat = date('Y-m-d' , strtotime($d));
                   // $endformat = date_format(new DateTime($_POST["end_date"]),"Y-m-d");
                $model->end_date = $endformat;
                //$model->skill_category_id = $_POST["skill_category_id"];
                $model->type = $_POST["type"];
                $model->description = $_POST["description"];
                $model->video_only = isset($_POST["video_only"]) ? 1 : 0;
                $model->additional = $_POST["additional"];
                $model->location_lat = $_POST["location_lat"];
                $model->location_lng = $_POST["location_lng"];
                    $model->salary_type = $_POST["salary_type"];
                    $model->updated_at = $now->format('Y-m-d H:i:s');
                   if($model->save())
                    {
                                if(isset($_POST["careers"]))
                       {
                           $careerjob = new Jobs();
                           $careerjob->attributes = $model->attributes;
                           $careerjob->careers = 1;
                           if($careerjob->save())
                           {
                               if(isset($_POST['skills']))
                               {
                                    $skills = $_POST['skills'];
                        
                        $jkillmodel= JobSkill::model()->deleteAllByAttributes(array('job_id'=>$model->id));
                        
                        foreach ($skills as $skills_val){
                            $jobskillmodel=new JobSkill;
                            $jobskillmodel->job_id = $model->id;
                            $jobskillmodel->skill_id = $skills_val;
                            $jobskillmodel->save();
                        }
                                }
                             }
                         }
                        
                        if(isset($_FILES['jdfile']['name']) && $_FILES['jdfile']['name'] != "")
                        {
                            if($model->media_id > 0)
                            {
                                $mediamodel = Media::model()->findByPk($model->media_id);
                                $mediamodel->updated_at = $now->format('Y-m-d H:i:s');
                            }
                            else
                            {
                                $mediamodel = new Media();
                                $mediamodel->created_at = $now->format('Y-m-d H:i:s');
                            }
                            $mediamodel->model_type = "Employer";
                            $mediamodel->model_id = $model->owner_id;
                            $mediamodel->collection_name = "documents";
                            $mediamodel->name = pathinfo($_FILES['jdfile']['name'], PATHINFO_FILENAME);
                            $mediamodel->file_name = $_FILES['jdfile']['name'];
                            $mediamodel->size = $_FILES['jdfile']['size'];
                            $mediamodel->disk = "documents";
                            
                            if($mediamodel->save())
                            {
                                $mediafolder = "images/media/" . $mediamodel->id . "/conversions";
                                
                                if (!file_exists($mediafolder)) {
                                    mkdir($mediafolder, 0777, true);
                                }
                                $filename = $_FILES['jdfile']['name'];
                                $fullpath = "images/media/" . $mediamodel->id . "/" . $filename;
                                if (move_uploaded_file($_FILES['jdfile']['tmp_name'], $fullpath)) {
                                    
                                }
                                Jobs::model()->updateByPk($model->id,array('media_id'=>$mediamodel->id));
                            }
                        }
                        
                        Yii::app()->user->setFlash('success', 'Successfully updated job');
                        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $empmodel->id, 'type' => 'employer'));
                        $this->redirect(Yii::app()->createUrl('employers/edit', array('slug' => $profilemodel->slug)));
                       
                    }
                }
                
                $this->render('editjobs',array(
                    'model'=>$model,'employermodel'=>$empmodel,'jobskillmodel'=>$jobskillmodel
                ));
            }
                
        }
    
    /**
        * Creates a new model.
        * If creation is successful, the browser will be redirected to the 'view' page.
        */
       public function actioncreateJobs($id)
       {
           $this->layout='main';
           
           $expired = 0;
           
           $model=new Jobs;
           $employermodel = Employers::model()->findByPk($id);
           $industrymodel = Industries::model();
           $skillcategmodel = SkillCategories::model();
           $skillsmodel = Skills::model();
           $jobskillmodel = JobSkill::model();

            $usermodel = Users::model()->findByPk($employermodel->user_id);
           $now = new DateTime();
           
            $model->attributes = $_POST['Jobs'];
            $model->owner_id = $id;
            $model->description = $_POST["description"];
            if(isset($_POST['competitive_salary']))
            {
                $model->competitive_salary = 1;
                $model->salary_min = 0;
                $model->salary_max = 0;
            }
            if(!isset($_POST['skills']))
                $model->setScenario("skillsrequired");
            if ($model->validate()) {
                   //  insert jobs details
                
                $model->owner_type = 'employer';
                
                   $model->user_id = Yii::app()->user->getState('userid');
                   $model->industry_id = $_POST["industry_id"];
                   $d = str_replace('/','-',$_POST["end_date"]);
                   $endformat = date('Y-m-d' , strtotime($d));
                   //$endformat = date_format(new DateTime($_POST["end_date"]),"Y-m-d");
                   $model->end_date = $endformat;
                  // $model->skill_category_id = $_POST["skill_category_id"];
                   $model->type = $_POST["type"];
                   $model->video_only = isset($_POST["video_only"]) ? 1 : 0;
                   $model->additional = $_POST["additional"];
                   $model->location_lat = $_POST["location_lat"];
                   $model->location_lng = $_POST["location_lng"];
                   $model->salary_type = $_POST["salary_type"];
                   $model->created_at = $now->format('Y-m-d H:i:s');
                   if($model->save())
                   {
                                if(isset($_POST["careers"]))
                       {
                           $careerjob = new Jobs();
                           $careerjob->attributes = $model->attributes;
                           $careerjob->careers = 1;
                           if($careerjob->save())
                           {
                               if(isset($_POST['skills']))
                               {
                                   $skills = $_POST['skills'];
                                   foreach ($skills as $skills_val){
                                       $jobskillmodel=new JobSkill;
                                       $jobskillmodel->job_id = $careerjob->id;
                                       $jobskillmodel->skill_id = $skills_val;
                                       if(isset($_POST[$skills_val]))
                                           $jobskillmodel->vacancies = $_POST[$skills_val];
                                           $jobskillmodel->save();
                                           
                                   }
                                }
                             }
                         }

                       if(Membership::subscribed($employermodel->stripe_active, $employermodel->trial_ends_at, $employermodel->subscription_ends_at)) 
                       {
                           if($employermodel->stripe_plan == "employer_vacancy" && $employermodel->current_vacancy_count > 0)
                           {
                               $curr = $employermodel->current_vacancy_count - 1;
                               if($curr <= 0)
                               {
                                   $expired = 1;
                                   
                                   $yest_timestamp = strtotime("- 1 day");
                                   $yest_date = date("Y-m-d H:i:s", $yest_timestamp);
                                   $employermodel->subscription_ends_at = $yest_date;
                                   //$employermodel->trial_ends_at = $yest_date;
                                   $employermodel->stripe_active = 0;
                                   $employermodel->save();
                               }
                              
                               Employers::model()->updateByPk($employermodel->id, array('current_vacancy_count'=>$curr));
                           }
                       }
                       
                       $aemodel = AgencyEmployers::model()->findByAttributes(array('employer_id'=>$id));
                       if($aemodel != null)
                       {
                           $avacmodel = new AgencyVacancies();
                           $avacmodel->agency_id = $aemodel->agency_id;
                           $avacmodel->job_id = $model->id;
                           $avacmodel->created_at = $now->format('Y-m-d H:i:s');
                           $avacmodel->save();
                       }
                       if(isset($_POST['skills']))
                       {
                           $skills = $_POST['skills'];
                           foreach ($skills as $skills_val){
                               $jobskillmodel=new JobSkill;
                               $jobskillmodel->job_id = $model->id;
                               $jobskillmodel->skill_id = $skills_val;
                               if(isset($_POST[$skills_val]))
                                   $jobskillmodel->vacancies = $_POST[$skills_val];
                               $jobskillmodel->save();
                           }
                       }
                       if(isset($_FILES['jdfile']['name']) && $_FILES['jdfile']['name'] != "")
                       {
                           $mediamodel = new Media();
                           $mediamodel->model_type = "Employer";
                           $mediamodel->model_id = $id;
                           $mediamodel->collection_name = "documents";
                           $mediamodel->name = pathinfo($_FILES['jdfile']['name'], PATHINFO_FILENAME);
                           $mediamodel->file_name = $_FILES['jdfile']['name'];
                           $mediamodel->size = $_FILES['jdfile']['size'];
                           $mediamodel->disk = "documents";
                           $mediamodel->created_at = $now->format('Y-m-d H:i:s');
                           if($mediamodel->save())
                           {
                               $mediafolder = "images/media/" . $mediamodel->id . "/conversions";
                               
                               if (!file_exists($mediafolder)) {
                                   mkdir($mediafolder, 0777, true);
                               }
                               $filename = $_FILES['jdfile']['name'];
                               $fullpath = "images/media/" . $mediamodel->id . "/" . $filename;
                               if (move_uploaded_file($_FILES['jdfile']['tmp_name'], $fullpath)) {
                                    
                               }
                               Jobs::model()->updateByPk($model->id,array('media_id'=>$mediamodel->id));
                           }
                       }

                      
                       //employer notification
                       if($employermodel->email != "")
                         $this->notifyjob($model, $employermodel->email, $employermodel->name,$expired);
                                              
                      Yii::app()->user->setFlash('success', 'Successfully created job');
                      $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $employermodel->id, 'type' => 'employer'));
                      $this->redirect(Yii::app()->createUrl('employers/edit', array('slug' => $profilemodel->slug)));
                   }
            }
             $this->render('addjobs',array(
                   'model'=>$model,'empmodel'=>$employermodel,'industrymodel'=>$industrymodel,'skillcategorymodel'=>$skillcategmodel,'skillmodel'=>$skillsmodel,'jobskillmodel'=>$jobskillmodel
           ));

       }
       
       /**
     * Notify the user
     */
    public function notifyjob($model, $email, $name,$expired)
    {
        $body = $this->renderPartial("//emails/jobs-created",array('model' => $model, 'email' => $email,'name'=>$name,'expired'=>$expired),true);
        // Send register notifcation to employer admin
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->Host = 'mail.cvvid.com';
        $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
        $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
        $mail->SMTPSecure = 'ssl';
        $mail->SetFrom('info@cvvid.com', 'CVVID');
        $mail->Subject = 'Thank you for uploading your vacancy';
        $mail->MsgHTML($body);
        $mail->AddAddress($email, $name);
        $mail->Send();
        
    }


    public function actiontabedit($id) {
        $model = Employers::model()->findByPk($id);
        if($model->user_id > 0)
        {
            $usermodel = Users::model()->findByPk($model->user_id);
            $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $model->id, 'type' => $usermodel->type));
            $cri = new CDbCriteria();
            $cri->condition = "model_id = ".$model->id." and model_type like '%employer%' and deleted_at is null";
            
            $addressmodel = Addresses::model()->find($cri);
            if($addressmodel == null)
                $addressmodel = new Addresses();
         }
        else 
        {
            $usermodel = new Users();
            $profilemodel = new Profiles();
            $addressmodel = new Addresses();
            
        }       
            
        $this->render('tabedit', array(
            'model' => $model,'profilemodel'=>$profilemodel,'addressmodel'=>$addressmodel,'usermodel'=>$usermodel
        ));
    }

    public function actiontabusers($id) {
        $model = Employers::model()->findByPk($id);
        $this->render('tabusers', array(
            'model' => $model,
        ));
    }

    public function actiontabjobs($id) {
        $model = Employers::model()->findByPk($id);
        $this->render('tabjobs', array(
            'model' => $model,
        ));
    }

    public function actioneditDetails($slug) {
        $this->layout = "main";
        $model = $this->loadModelSlug($slug);//Employers::model()->findByPk($id);
        $id = $model->id;
        $usermodel = Users::model()->findByPk($model->user_id);
        $cri = new CDbCriteria();
        $cri->condition = "model_id =".$model->id." and model_type like '%employer%' and deleted_at is null";
        $addressmodel = Addresses::model()->find($cri);
        if($addressmodel == null)
            $addressmodel = new Addresses();
        $this->render('editDetails', array(
            'model' => $model, 'usermodel' => $usermodel,'addressmodel'=>$addressmodel,'profilemodel'=>Profiles::model()
        ));
    }

//         /**
//   * Manages Employers Edit.
//   */
//        function actionemployerEdit($id)
//  {
//      $model = Employers::model()->findByPk($id);
//      $this->render('employerEdit',array('model'=>$model));
//  }
    /**
     * Manages Employers Edit.
     */
    function actionUsersEdit($id) {
        $eumodel = EmployerUsers::model()->findByPk($id);
        $model = Employers::model()->findByPk($eumodel->employer_id);
        $usermodel = Users::model()->findByPk($eumodel->user_id);
        $cri = new CDbCriteria();
        $cri->condition = "model_id = ".$usermodel->id." and model_type like '%employer%' and deleted_at is null";
        $addressmodel = Addresses::model()->find($cri);
        if($addressmodel == null)
            $addressmodel = new Addresses;
        $this->render('_employeruseredit', array('model' => $model,'eumodel'=>$eumodel,'usermodel'=>$usermodel,'addressmodel'=>$addressmodel));
    }

    function actionUsers($id) {
        $this->layout = "main";
        $model = Employers::model()->findByPk($id);
        $this->render('users', array('model' => $model));
    }

    function actionuserCreate($id) {
        $this->layout = "main";
        $model = Employers::model()->findByPk($id);
        $usermodel = new Users;
        $this->render('usercreate', array('model' => $model,'usermodel'=>$usermodel));
    }

    function actionuserEdit($id) {
        $this->layout='main';
        $usermodel = Users::model()->findByPk($id);
        $empusrmodel = EmployerUsers::model()->findByAttributes(array('user_id'=>$usermodel->id));
        $empmodel = Employers::model()->findByPk($empusrmodel->employer_id);
        $this->render('useredit', array('usermodel'=>$usermodel,'empmodel'=>$empmodel));
    }

    public function actionusersActivate() {
        $ids = implode(',', $_POST['ids']);

        $cri = new CDbCriteria();
        $cri->condition = "id in (" . $ids . ")";
        Users::model()->updateAll(array('status' => 'active'), $cri);
        
         Yii::app()->user->setFlash('success', 'Successfully Activated');
    }

    public function actionusersBlock() {
        $ids = implode(',', $_POST['ids']);

        $cri = new CDbCriteria();
        $cri->condition = "id in (" . $ids . ")";
        Users::model()->updateAll(array('status' => 'blocked'), $cri);
        
        Yii::app()->user->setFlash('success', 'Successfully Blocked');
        
    }

    function actiongetProfilePhoto() {
        echo $_FILES['profile']['tmp_name'];
    }

    function actionupdateProfileDetails($id) {
//         echo var_dump($_POST);
//         die();
        $this->layout = "main";
        $now = new DateTime();
        $empmodel = Employers::model()->findByPk($id);
        $usermodel = Users::model()->findByPk($empmodel->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $empmodel->id, 'type' => 'employer'));
        
        $cri = new CDbCriteria();
        $cri->condition = "model_id =".$empmodel->id." and model_type like '%employer%' and deleted_at is null";
        $addressmodel = Addresses::model()->find($cri);
        if($addressmodel == null)
            $addressmodel = new Addresses();
        
        $pwd = $usermodel->password;        
            
        if(isset($_POST['Employers'])){
          $empmodel->attributes=$_POST['Employers'];
        }
        if(isset($_POST['Profiles'])){
          $profilemodel->attributes=$_POST['Profiles'];  
        }
         if(isset($_POST["Addresses"])){
            $addressmodel->attributes = $_POST["Addresses"];
        }
      
        if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
            $usermodel->setScenario('confirm');
            
        if($_POST['Users']['password'] != "")
            $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
        else
            $usermodel->password = $pwd;
            
        if($usermodel->save())
        {
            if(isset($_POST['Employers']))
            {
                $valid = $empmodel->validate();
                $valid = $profilemodel->validate(false) && $valid;
                $valid = $addressmodel->validate() && $valid; 
                if($valid){
                   
                    $empmodel->updated_at = $now->format('Y-m-d H:i:s');
                    if($empmodel->save())
                    {
                        $addressmodel->model_id = $empmodel->id;
                        $addressmodel->model_type = 'employer';
                        $addressmodel->save();
                    }
                    
                    $profilemodel->industry_id = $_POST['industry_id'];
                    $profilemodel->slug = $_POST["Profiles"]['slug'];
                    $profilemodel->updated_at = $now->format('Y-m-d H:i:s');
                    $profilemodel->save();
                    
                     Yii::app()->user->setFlash('success', 'Successfully updated profile');
                     $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $empmodel->id, 'type' => 'employer'));
                     $this->redirect(Yii::app()->createUrl('employers/edit', array('slug' => $profilemodel->slug)));
    
                }
             }
        }
        
        $this->render('editDetails', array(
            'model' => $empmodel, 'usermodel' => $usermodel,'addressmodel'=>$addressmodel,'profilemodel'=>$profilemodel
        ));
        
    }

    function actionupdateEmployerDetails($id) {
        $now = new DateTime();
        $model = Employers::model()->findByPk($id);
        $usermodel = Users::model()->findByPk($model->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $model->id, 'type' => $usermodel->type));
        $cri = new CDbCriteria();
        $cri->condition = "model_id = ".$model->id." and model_type like '%employer%' and deleted_at is null";
        $addressmodel = Addresses::model()->find($cri);
        
        $pwd = $usermodel->password;
        if($addressmodel == null)
            $addressmodel = new Addresses();
        if(isset($_POST['Users'])){
            
          // $usermodel->attributes=$_POST['Users'];
           if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
               $usermodel->setScenario('confirm');
               
               if($_POST['Users']['password'] != "")
                   $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
                   else
                       $usermodel->password = $pwd;
        }
        if(isset($_POST['Employers'])){
           $model->attributes=$_POST['Employers'];
        }
        if(isset ($_POST["Addresses"])){
            $addressmodel->attributes = $_POST["Addresses"]; 
        }
        if(isset ($_POST["Profiles"])){
            $profilemodel->attributes = $_POST["Profiles"];
        }
        
         if(isset($_POST['Employers']))
        {
             $valid = $usermodel->validate();
             $valid = $model->validate() && $valid;
             $valid = $addressmodel->validate() && $valid;
             $valid = $profilemodel->validate() && $valid;
             
             if($valid){
                 $usermodel->save();
                $model->updated_at = $now->format('Y-m-d H:i:s');
                $model->body = $_POST['body'];
                $model->save();
//                profile update
                if ($_POST['photo_id'] > 0)
                $profilemodel->photo_id = $_POST['photo_id'];
                $profilemodel->updated_at = $now->format('Y-m-d H:i:s');
                $profilemodel->save();
//                address model

                if($addressmodel == null)
                {
                    $addressmodel->model_id = $model->id;
                    $addressmodel->model_type = "employer";
                }
                
                $addressmodel->updated_at = $now->format('Y-m-d H:i:s');
                if ($addressmodel->save()) {
                     Yii::app()->user->setFlash('success', 'Successfully updated employer');
                     $this->redirect($this->createUrl('employers/tabedit', array('id' => $model->id)));
                }
                
             }
        }
        
        $this->render('tabedit', array(
            'model' => $model,'profilemodel'=>$profilemodel,'addressmodel'=>$addressmodel,'usermodel'=>$usermodel
        ));
    }

    function resize_image($image, $image_type, $fullpath, $destpath, $width, $height) {
        $size_arr = getimagesize($fullpath);

        list($width_orig, $height_orig, $img_type) = $size_arr;
        $ratio_orig = $width_orig / $height_orig;

        $tempimg = imagecreatetruecolor($width, $height);
        imagecopyresampled($tempimg, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

        if ($image_type == "image/jpg" || $image_type == "image/jpeg")
            imagejpeg($tempimg, $destpath);
        else if ($image_type == "image/png")
            imagepng($tempimg, $destpath);
        else if ($image_type == "image/gif")
            imagegif($tempimg, $destpath);
    }

    /*     * updateEmployerDetails
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Employers the loaded model
     * @throws CHttpException
     */

    public function loadModel($id) {
        $model = Employers::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Employers $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'employers-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function beforeAction($action) {
        Yii::app()->user->setReturnUrl(Yii::app()->request->getUrl());
        $requestUrl = Yii::app()->request->url;
        if (Yii::app()->user->isGuest && !strpos(Yii::app()->request->getUrl(), 'employers/profile'))
            $this->redirect(Yii::app()->createUrl('site/login'));

        return parent::beforeAction($action);
    }

    public function actionInbox($id) {
        $this->layout = "main";
        $empmodel = Employers::model()->findByPk($id);
        $model = Users::model()->findByPk(Yii::app()->user->getState('userid'));
        $this->render("inbox", array('model' => $model,'empmodel'=>$empmodel));
    }

    public function actiongetInboxMessages() {
        $this->layout = "main";
        $convmodel = Conversations::model()->findByPk($_POST['id']);
        
        $now = new DateTime();
        $ccri = new CDbCriteria();
        $ccri->condition = 'conversation_id ='.$convmodel->id;
        ConversationMembers::model()->updateAll(array('last_read'=>$now->format('Y-m-d H:i:s')),$ccri);
         
        $this->renderPartial('inbox_messages', array('convmodel' => $convmodel, 'to_user' => $_POST['msg_to']));
    }

    public function actionsaveMessage() {
        $this->layout = "main";
        $conid = $_POST['id'];
        $fromuser = Yii::app()->user->getState('userid');
        $touser = $_POST['msg_to'];
        $msg = $_POST['message'];
        $now = new DateTime();

        $convmodel = Conversations::model()->findByPk($_POST['id']);
        $usermodel = Users::model()->findByPk($fromuser);

        $convmsgmodel = new ConversationMessages();
        $convmsgmodel->conversation_id = $conid;
        $convmsgmodel->user_id = $fromuser;
        $convmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
        $convmsgmodel->message = $msg;
        $convmsgmodel->created_at = $now->format('Y-m-d H:i:s');
        $convmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($convmsgmodel->save()) {
            $conmemmodel_sender = new ConversationMembers();
            $conmemmodel_sender->conversation_id = $convmodel->id;
            $conmemmodel_sender->user_id = Yii::app()->user->getState('userid');
            $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
            $conmemmodel_sender->last_read = NULL;
            $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
            $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
            if ($conmemmodel_sender->save()) {
                $usermodel1 = Users::model()->findByPk($_POST['msg_to']);
                $conmemmodel_rec = new ConversationMembers();
                $conmemmodel_rec->conversation_id = $convmodel->id;
                $conmemmodel_rec->user_id = $_POST['msg_to'];
                $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
                $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                if ($conmemmodel_rec->save()) {
                    $activitymodel = new Activities();
                    $activitymodel->user_id = $fromuser;
                    $activitymodel->type = "message";
                    $activitymodel->model_id = $convmsgmodel->id;
                    $activitymodel->model_type = "App\Messaging\ConversationMessage";
                    $activitymodel->subject = "You sent a message in conversation: " . $convmodel->subject;
                    $activitymodel->message = $msg;
                    $activitymodel->created_at = $now->format('Y-m-d H:i:s');
                    $activitymodel->updated_at = $now->format('Y-m-d H:i:s');
                    $activitymodel->save();
        
                    $activitymodel = new Activities();
                    $activitymodel->user_id = $touser;
                    $activitymodel->type = "message";
                    $activitymodel->model_id = $convmsgmodel->id;
                    $activitymodel->model_type = "App\Messaging\ConversationMessage";
                    $activitymodel->subject = "You sent a message in conversation: " . $convmodel->subject;
                    $activitymodel->message = $msg;
                    $activitymodel->created_at = $now->format('Y-m-d H:i:s');
                    $activitymodel->updated_at = $now->format('Y-m-d H:i:s');
                    $activitymodel->save();
               }
            }
        }
        Yii::app()->user->setFlash('success', 'Message sent successfully');
        $this->renderPartial('inbox_messages', array('convmodel' => $convmodel, 'to_user' => $_POST['msg_to']));
    }

   public function actionupgradestore() {
        //echo var_dump($_POST);
        $now = new DateTime();
        $empusrmodel = EmployerUsers::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
        $model = Employers::model()->findByPk($empusrmodel->employer_id);
        $usermodel = Users::model()->findByPk(array('id'=>Yii::app()->user->getState('userid')));
        
        require Yii::app()->basePath . '/extensions/stripe/init.php';
        Stripe::setApiKey(Yii::app()->params['SECRET_Key']);
        
        $plan = $_POST['subscription_plan'];
        $token = $_POST['stripeToken'];
        $amount = $_POST['amount'];
        // $trial_ends = Carbon::now()->addYear()->toDateTimeString();
        
        $planarr = Plan::retrieve($plan);
        
        //                                $amount = $planarr->amount;
        $planname = $planarr->name;
        $currency = $planarr->currency;
        
        
        //        create subscriber
        $customer = Customer::create(array(
            'source'   => $token,
            'email'    => $model->email,
            'description' => $model->name,
        ));
        
        //                                $charge = Charge::create(array(
        //                                    "amount" => $amount * 100,
        //                                    "currency" => $currency,
        //                                    "customer" => $customer->id,
        //                                    "receipt_email" => $model->email,
        //                                ));
        
        $coupon = $_POST['coupon_id'];
        $validcoupon = $_POST['validcoupon'];
        
        if($validcoupon == "1")
        {
            try {
                $coupon = Coupon::retrieve($coupon);
                $couponid = $coupon->id;
                
                $subscription = \Stripe\Subscription::create([
                    "customer" => $customer->id,
                    "items" => array(
                        array(
                            "plan" => $plan,
                            'quantity' => $_POST['vacancy_count'],
                        ),
                    ),
                    'tax_percent' => Yii::app()->params['TAX_PERCENT'],
                    'coupon' => $couponid,
                ]);
                
            } catch (\Stripe\Error\InvalidRequest $e) {
                
            }
        }else{
            $subscription = \Stripe\Subscription::create([
                "customer" => $customer->id,
                "items" => array(
                    array(
                        "plan" => $plan,
                        'quantity' => $_POST['vacancy_count'],
                    ),
                ),
                'tax_percent' => Yii::app()->params['TAX_PERCENT'],
            ]);
        }
        
        
        $model->stripe_active = 1;
        
        $link = "";
        
        if($plan != "employer_annual" && isset($_POST['vacancy_count']) && $_POST['vacancy_count'] > 0)
        {
            $model->vacancy_count = $_POST['vacancy_count'];
            $model->current_vacancy_count = $_POST['vacancy_count'];
            $model->stripe_plan = $plan;
            
            $subs = Carbon::now()->addMonths($model->vacancy_count)->toDateTimeString();
            
            $charge = Charge::create(array(
                
                "amount" => $amount , // amount in cents, again
                "currency" => "gbp",
                "customer" => $customer->id,
                "description" => "vacancy count",
                'receipt_email' => $model->email
            ));
        }
        else if($plan == "employer_annual")
        {
            $subs = Carbon::now()->addYear()->toDateTimeString();
            $model->stripe_subscription = $subscription->id;
            $model->stripe_plan = $plan;
            $model->last_four = $_POST['cvc'];
            
            $invoiceitemlist = Invoice::all(array("customer" => $subscriber->stripe_id));
            foreach ($invoiceitemlist->autoPagingIterator() as $invoice) {
                $link = $invoice['invoice_pdf'];
            }
            
            
        }
            $model->subscription_ends_at = $subs;
        
        if(isset($_POST['vacancy_count']) && $_POST['vacancy_count'] > 0)
        {
            $model->vacancy_count = $_POST['vacancy_count'];
            $model->current_vacancy_count = $_POST['vacancy_count'];
        }
        $model->stripe_id = $customer->id;
       
        //$model->trial_ends_at = $trial_ends;
        $model->updated_at = $now->format('Y-m-d H:i:s');
        
        $model->premium_user_count = 2;
        
        $model->save();
        
        if($plan == "employer_annual")
            $this->notifyemployerupgrade($model, $customer->id);
        
        $subscriber = Employers::model()->findByPk($model->id);
                                
        if($link != ""){      
            $message = "Successfully updated your membership <a href='$link'>Click here to download your invoice</a>";
            $this->sendMessageE("Successfully updated your membership",$message, 57, $model->user_id);
        }
        
        Yii::app()->user->setFlash('success', 'Successfully updated your membership');
        
        $cri = new CDbCriteria();
        $cri->condition = "owner_id =".$model->id." and type like '%employer%'";
        $profilemodel = Profiles::model()->find($cri);
        $this->redirect(Yii::app()->createUrl('employers/edit', array('slug' => $profilemodel->slug)));
        
    }
    
    public function actioncardupdate() {
        
          //echo var_dump($_POST);
        $now = new DateTime();
        $model = Employers::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid')));
        $usermodel = Users::model()->findByPk(array('id'=>Yii::app()->user->getState('userid')));
        
        require Yii::app()->basePath . '/extensions/stripe/init.php';
        Stripe::setApiKey(Yii::app()->params['SECRET_Key']);
                
        $token = $_POST['stripeToken'];
        
        $cu = Customer::retrieve($model->stripe_id);
        $cu->source = $token; // obtained with Stripe.js
        $cu->save();
        
        $model->last_four = $_POST['cvc'];
        $model->updated_at = $now->format('Y-m-d H:i:s');
        $model->save();
        
        Yii::app()->user->setFlash('success', 'Your card information has been successfully updated');
        $this->redirect(Yii::app()->createUrl('employers/mysubscription'));


    }
    
    public function actionsendMessage() {
        
        $now = new DateTime();
        $conmodel = new Conversations();
        $conmodel->subject = $_POST['subject'];
        $conmodel->created_at = $now->format('Y-m-d H:i:s');
        $conmodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($conmodel->save()) {
            $usermodel = Users::model()->findByPk(Yii::app()->user->getState('userid'));
            $conmsgmodel = new ConversationMessages();
            $conmsgmodel->conversation_id = $conmodel->id;
            $conmsgmodel->user_id = Yii::app()->user->getState('userid');
            $conmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
            $conmsgmodel->message = $_POST['message'];
            $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
            $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($conmsgmodel->save()) {
                $conmemmodel_sender = new ConversationMembers();
                $conmemmodel_sender->conversation_id = $conmodel->id;
                $conmemmodel_sender->user_id = Yii::app()->user->getState('userid');
                $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
                $conmemmodel_sender->last_read = NULL;
                $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                if ($conmemmodel_sender->save()) {
                    $usermodel1 = Users::model()->findByPk($_POST['user_id']);
                    $conmemmodel_rec = new ConversationMembers();
                    $conmemmodel_rec->conversation_id = $conmodel->id;
                    $conmemmodel_rec->user_id = $_POST['user_id'];
                    $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
                    $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                    $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                    if ($conmemmodel_rec->save()) {
                        $actmodel_sender = new Activities();
                        $actmodel_sender->user_id = Yii::app()->user->getState('userid');
                        $actmodel_sender->type = "message";
                        $actmodel_sender->model_id = $conmsgmodel->id;
                        $actmodel_sender->model_type = "ConversationMessage";
                        $actmodel_sender->subject = "You sent a message in conversation: " . $_POST['subject'];
                        $actmodel_sender->message = $_POST['message'];
                        $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                        $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                        if ($actmodel_sender->save()) {
                            $actmodel_rec = new Activities();
                            $actmodel_rec->user_id = $_POST['user_id'];
                            $actmodel_rec->type = "message";
                            $actmodel_rec->model_id = $conmsgmodel->id;
                            $actmodel_rec->model_type = "ConversationMessage";
                            $actmodel_rec->subject = "You received a message in conversation: " . $_POST['subject'];
                            $actmodel_rec->message = $_POST['message'];
                            $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                            $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                            $actmodel_rec->save();
                        }
                    }
                }
            }
        }
    }
    
    public function actionaddIndustry()
    {
        $empmodel = Employers::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid')));
        $now = new DateTime();
        $indmodel = new Industries();
        $indmodel->name = $_POST['value'];
        $indmodel->created_at = $now->format('Y-m-d H:i:s');
        if($indmodel->save())
        {
            $tindmodel = new TempIndustry();
            $tindmodel->employer_id = $empmodel->id;
            $tindmodel->industry_id = $indmodel->id;
            $tindmodel->save();
        }
        $orgcri = new CDbCriteria();
        $orgcri->condition = "deleted_at is null";
        $orgdata = Industries::model()->findAll($orgcri);
        $orgdata = CHtml::listData($orgdata,'id','name');
        
        $cri = new CDbCriteria();
        $cri->condition = "id in (select industry_id from cv16_temp_industry where active = 1 and employer_id =".$empmodel->id.") and deleted_at is null";
        $data = Industries::model()->findAll($cri); //ByAttributes(array('deleted_at'=>null));        
        $data = CHtml::listData($data,'id','name');
        
        $datas = $data + $orgdata;
        echo CHtml::tag('option',array('value'=>""),CHtml::encode("Select Industry"),true);
        foreach($datas as $value=>$name)  {
            if($name == trim($_POST['value']))
                echo CHtml::tag('option',array('value'=>$value,'selected'=>'selected'),CHtml::encode($name),true);
            else 
                echo CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
        }
        
        
     //   $jobmodel = Jobs::model()->findByPk($_POST['job_id']);
        
       // $usermodel = Users::model()->findByPk(Yii::app()->user->getState('userid'));
        
        $body = $this->renderPartial("//emails/industry-notification", array('skill' => $_POST['value'], 'name' => $empmodel->name,'job_id' => $_POST['job_id']), true);
        // Send employer viewed notifcation to candidate
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->Host = 'mail.cvvid.com';
        $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
        $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
        $mail->SMTPSecure = 'ssl';
        $mail->SetFrom('mail@cvvid.com', 'CVVID');
        $mail->Subject = "INDUSTRY WAITING APPROVAL!";
        $mail->MsgHTML($body);
        $mail->AddAddress('info@cvvid.com', 'CVVID');
        $mail->Send();
      
    }
    
    public function actionaddSkillCategory()
    {
        $empmodel = Employers::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid')));
        $cri = new CDbCriteria();
        $cri->select = "max(ordering) as ordering";
        $skcmodel = SkillCategories::model()->find($cri);
        $now = new DateTime();
        $scmodel = new SkillCategories();
        $scmodel->name = $_POST['value'];
        $scmodel->ordering = $skcmodel['ordering'] + 1;
        $scmodel->created_at = $now->format('Y-m-d H:i:s');
        if($scmodel->save())
        {
            $tsmodel = new TempSkillCategory();
            $tsmodel->employer_id = $empmodel->id;
            $tsmodel->skill_category_id = $scmodel->id;
            $tsmodel->save();
        }
        
        
        $orgdata = SkillCategories::model()->findAll();
        $orgdata = CHtml::listData($orgdata,'id','name');
        
        $cri = new CDbCriteria();
        $cri->condition = "id in (select skill_category_id from cv16_temp_skill_category where active=1 and employer_id =".$empmodel->id.")";
        $data = SkillCategories::model()->findAll($cri);        
        $data = CHtml::listData($data,'id','name');
        
        $datas = $data + $orgdata;
        echo CHtml::tag('option',array('value'=>""),CHtml::encode("-Please Select-"),true);
        foreach($datas as $value=>$name)  {
            if($name == trim($_POST['value']))
                echo CHtml::tag('option',array('value'=>$value,'selected'=>'selected'),CHtml::encode($name),true);
                else
                    echo CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
        }
        
        $body = $this->renderPartial("//emails/skill-category-notification", array('skill' => $_POST['value'], 'name' => $empmodel->name,'job_id' => $_POST['job_id']), true);
        // Send employer viewed notifcation to candidate
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->Host = 'mail.cvvid.com';
        $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
        $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
        $mail->SMTPSecure = 'ssl';
        $mail->SetFrom('mail@cvvid.com', 'CVVID');
        $mail->Subject = "SKILL CATEGORY WAITING APPROVAL!";
        $mail->MsgHTML($body);
        $mail->AddAddress('info@cvvid.com', 'CVVID');
        $mail->Send();
    }
    public function actionaddSkill()
    {        
        $empmodel = Employers::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid')));
        $now = new DateTime();
        $smodel = new Skills();
        $smodel->name = $_POST['value'];
        $smodel->skill_category_id = $_POST['skill_category_id'];
        $smodel->created_at = $now->format('Y-m-d H:i:s');
        if($smodel->save())
        {
            $tsmodel = new TempSkills();
            $tsmodel->employer_id = $empmodel->id;
            $tsmodel->skill_category_id = $smodel->skill_category_id;
            $tsmodel->skill_id = $smodel->id;
            $tsmodel->save();
        }
     $orgdata = Skills::model()->findAllByAttributes(array("skill_category_id"=>$smodel->skill_category_id));
        $orgdata = CHtml::listData($orgdata,'id','name');
        
        $cri = new CDbCriteria();
        $cri->condition = "id in (select skill_id from cv16_temp_skills where active=1 and employer_id =".$empmodel->id." and skill_category_id = ".$smodel->skill_category_id.")";
        $data = Skills::model()->findAll($cri);
        $data = CHtml::listData($data,'id','name');
        
        $datas = $data + $orgdata;
        
        echo CHtml::tag('option',array('value'=>""),CHtml::encode("-Please Select-"),true);
        foreach($datas as $value=>$name)  {
            if($name == trim($_POST['value']))
                echo CHtml::tag('option',array('value'=>$value,'selected'=>'selected'),CHtml::encode($name),true);
                else
                    echo CHtml::tag('option',array('value'=>$value),CHtml::encode($name),true);
        }
        
        $body = $this->renderPartial("//emails/skill-notification", array('skill' => $_POST['value'], 'name' => $empmodel->name,'job_id' => $_POST['job_id']), true);
        // Send employer viewed notifcation to candidate
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->Host = 'mail.cvvid.com';
        $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
        $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
        $mail->SMTPSecure = 'ssl';
        $mail->SetFrom('mail@cvvid.com', 'CVVID');
        $mail->Subject = "SKILL WAITING APPROVAL!";
        $mail->MsgHTML($body);
        $mail->AddAddress('info@cvvid.com', 'CVVID');
        $mail->Send();
        
    }
    public function actionviewLikes($id)
    {
        $this->layout = "main";
        $profilemodel = Profiles::model()->findByPk($id);
        $model = Employers::model()->findByPk($profilemodel->owner_id);
        $this->render('profilelikes', array(
            'model' => $model, 'profilemodel' => $profilemodel
        ));
    }
    public function actionassignJob()
    {
        $jobid = $_POST['jobid'];
        $empuser = $_POST['userid'];
        $now =  new DateTime();
        $jmodel = Jobs::model()->findByPk($jobid);
        Jobs::model()->updateByPk($jobid,array('user_id'=>$empuser,'updated_at'=>$now->format('Y-m-d H:i:s')));
    }
    public function actionduplicateJobs($id)
    {
      
         $now = new DateTime();
        $jmodel = Jobs::model()->findByPk($id);
        $newjobmodel = new Jobs();
       
        $newjobmodel->attributes = $jmodel->attributes;
 unset($newjobmodel['id']);
        $newjobmodel->created_at = $now->format('Y-m-d H:i:s');
        $newjobmodel->num_views = 0;
        $newjobmodel->updated_at = null;
        if($newjobmodel->save())
        {
          $jskillmodel = JobSkill::model()->findAllByAttributes(array('job_id'=>$id));
            foreach ($jskillmodel as $js)
            {
                $newjskill = new JobSkill();
                $newjskill->job_id = $newjobmodel->id;
                $newjskill->skill_id = $js['skill_id'];
                $newjskill->save();
            }
            $employermodel = Employers::model()->findByPk($jmodel->owner_id);
            if(Membership::subscribed($employermodel->stripe_active, $employermodel->trial_ends_at, $employermodel->subscription_ends_at))
            {
                if($employermodel->stripe_plan == "employer_vacancy" && $employermodel->current_vacancy_count > 0)
                {
                    $curr = $employermodel->current_vacancy_count - 1;
                    if($curr <= 0)
                    {
                        $expired = 1;
                        
                        $yest_timestamp = strtotime("- 1 day");
                        $yest_date = date("Y-m-d H:i:s", $yest_timestamp);
                        $employermodel->subscription_ends_at = $yest_date;
                       // $employermodel->trial_ends_at = $yest_date;
                        $employermodel->stripe_active = 0;
                        $employermodel->save();
                    }
                    
                    Employers::model()->updateByPk($employermodel->id, array('current_vacancy_count'=>$curr));
                }
            }
        }
        $this->redirect($this->createUrl('employers/jobsedit',array('slug'=>$this->getslug($newjobmodel->owner_id),"id"=>$newjobmodel->id)));
     //  $this->actionjobsedit($newjobmodel->id);
    }
 public function getslug($id)
    {
        $cri = new CDbCriteria();
        $cri->condition = "owner_id =".$id." and type like '%employer%'";
        $profilemodel = Profiles::model()->find($cri);
       
        return $profilemodel->slug;
    }
  public function actionaddProfileVideo() {
        $now = new DateTime();
        $empid = $_POST['employerid'];
        $name = str_replace(' ', '_', $_POST['video_name']);
        $videoname = str_replace(' ', '_', $_FILES['video']['name']);
        $size = $_FILES['video']['size'];
        $image_type = $_FILES['video']['type'];
        $empmodel = Employers::model()->findByPk($empid);
         $tmp = $_FILES['video']['tmp_name'];
         $ext = $this->getExtension($videoname);
        Yii::import('application.extensions.amazon.components.*');
        
        $bucket = Yii::app()->params['AWS_BUCKET'];
        $s3 = new A2S3();
        
        $vname = pathinfo($name, PATHINFO_FILENAME);
        
        $actual_image_name = $vname.time() . "." . $ext;
        
        if (strlen($name) > 0) {
            
            try {
                //                $s3->putObject(array(
                //                    'Bucket' => $bucket,
                //                    'Key' => $image_name_actual,
                //                    'SourceFile' => $tmp,
                //                    'StorageClass' => 'REDUCED_REDUNDANCY'
                //                ));
                
                $response =  $s3->putObject(array(
                    'SourceFile' => $tmp,
                    'Bucket' => $bucket,
                    'Key' => $actual_image_name,
                    'ACL' => 'public-read',
                    'x-amz-storage-class' => 'REDUCED_REDUNDANCY'
                ));
                
                //  $message = "S3 Upload Successful.";
                //  $s3file = 'http://' . $bucket . '.s3.amazonaws.com/' . $actual_image_name;
                //  echo $s3file;die();
                // echo "<img src='$s3file'/>";
                //  echo 'S3 File URL:' . $s3file;
                
                //    $vname = pathinfo($name, PATHINFO_FILENAME);
                
                $duration = 6;
                
                $videomodel = new Videos();
                $videomodel->model_id = $empid;
                $videomodel->video_id = $actual_image_name;
                $videomodel->name = $vname;
                $videomodel->duration = (int)$duration;
                $videomodel->model_type = ucfirst(Yii::app()->user->getState('role'));
                $videomodel->category_id = 1;
                $videomodel->is_processed = 1;
                $videomodel->state = 1;
                $videomodel->status = "active";
                $videomodel->created_at = $now->format('Y-m-d H:i:s');
                if($videomodel->save()){
                    
                    Employers::model()->updateByPk($empmodel->id,array('video_id'=>$videomodel->id));
                    
                    $sec = 0;
                    
                    $fullpath = 'http://' . $bucket . '.s3.amazonaws.com/' . $actual_image_name;
                    
                    $cmd = "ffmpeg -i $fullpath 2>&1";
                    $ffmpeg = shell_exec($cmd);
                    
                    $search = "/Duration: (.*?)\./";
                    preg_match($search, $ffmpeg, $matches);
                    
                    if (isset($matches[1])) {
                        $data['duration'] = $matches[1];
                        $time_sec = explode(':', $data['duration']);
                        $sec = ($time_sec['0'] * 3600) + ($time_sec['1'] * 60) + $time_sec['2'];
                    }
                    
                    Videos::model()->updateByPk($videomodel->id, array('duration' => $sec));
                    
                    $mediamodel = new Media();
                    $mediamodel->model_id = $videomodel->id;
                    $mediamodel->disk = "media";
                    $mediamodel->model_type = $videomodel->model_type;
                    $mediamodel->collection_name = "video";
                    $mediamodel->name = $vname;
                    $mediamodel->file_name = $vname.".jpg";
                    $mediamodel->created_at = $now->format('Y-m-d H:i:s');
                    $mediamodel->size = (int)$duration;
                    
                    if ($mediamodel->save()) {
                        //$filename = $vname;
                        
                        Media::model()->updateByPk($mediamodel->id, array('model_id' => $videomodel->id, 'size' => $sec));
                        
                        $iname = $vname . ".jpg";
                        $mediafolder = "images/media/" . $mediamodel->id;
                        
                        if (!file_exists($mediafolder)) {
                            mkdir($mediafolder, 0777, true);
                        }
                        
                        $imgfullpath = "images/media/" . $mediamodel->id . "/" . $iname;
                        $imgprefix = "images/media/" . $mediamodel->id;
                        
                        $ffurl = "ffmpeg -i $fullpath -ss 00:00:2 -vframes 1 $imgfullpath";
                        
                        $process = exec($ffurl);
                        
                        $jlwidth = 350;
                        $jlheight = 190;
                        $jldestpath = $imgprefix . "/conversions/profile.jpg";
                        
                        //create search
                        $searchwidth = 167;
                        $searchheight = 167;
                        $searchdestpath = $imgprefix . "/conversions/search.jpg";
                        
                        //create thumb
                        $thumbwidth = 126;
                        $thumbheight = 126;
                        $thumbdestpath = $imgprefix . "/conversions/thumb.jpg";
                        $image_type == "image/jpg";
                        
                        $image = imagecreatefromjpeg($imgfullpath);
                        
                        $this->resize_image($image, $image_type, $imgfullpath, $jldestpath, $jlwidth, $jlheight); //create joblisting
                        $this->resize_image($image, $image_type, $imgfullpath, $searchdestpath, $searchwidth, $searchheight); //create search
                        $this->resize_image($image, $image_type, $imgfullpath, $thumbdestpath, $thumbwidth, $thumbheight); //create thumb
                        
                        
                    }
                }
                else
                {
                    echo var_dump($videomodel->getErrors());
                    die();
                }
                
            } catch (S3Exception $e) {
                // Catch an S3 specific exception.
                echo $e->getMessage();
            }
        }
        
        
        echo $this->renderPartial('employerVideos', array('model' => $empmodel));
    }
      public function getExtension($str)
    {
        $i = strrpos($str,".");
        if (!$i) { return ""; }
        $l = strlen($str) - $i;
        $ext = substr($str,$i+1,$l);
        return $ext;
    }
    public function actiondeleteCompanyProfile($id)
    {
        $now = new DateTime();
        Employers::model()->updateByPk($id, array('video_id'=>null,'updated_at'=>$now->format('Y-m-d H:i:s')));
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $id, 'type' => 'employer'));
        $this->redirect($this->createUrl('employers/edit',array('slug'=>$profilemodel->slug)));
    }
    public function actionbulkCreate()
    {
        $model = new Employers();
        $this->render("bulkemployers", array('model' => $model));
    }
    public function actioncreateBulkEmployers()
    {
        $now = new DateTime();
        
        $forenames_arr = $_POST['forenames'];
        array_pop($forenames_arr);
        $surname_arr = $_POST['surname'];
        array_pop($surname_arr);
        $password_arr = $_POST['password'];
        array_pop($password_arr);
        $email_arr = $_POST['email'];
        array_pop($email_arr);
        for($i = 0; $i < count($forenames_arr); $i++)
        {
            $usermodel = new Users;            
            $usermodel->forenames = $forenames_arr[$i];
            $usermodel->surname = $surname_arr[$i];
            $usermodel->email = $email_arr[$i];
            $usermodel->created_at = $now->format('Y-m-d H:i:s');
            $usermodel->type = 'employer';
            $usermodel->status = "active";
            $usermodel->password = password_hash($password_arr[$i], 1,['cost' => 10]);
            if($usermodel->save())
            {
                //  insert employer details
                $model = new Employers;
                $model->name = $usermodel->forenames." ".$usermodel->surname;             
                $model->email = $email_arr[$i];
                $model->created_at = $now->format('Y-m-d H:i:s');
                $model->user_id = $usermodel->id;
                if($model->save())
                {
                    //employer user update
                    $empusermodel = new EmployerUsers;
                    $empusermodel->employer_id = $model->id;
                    $empusermodel->user_id = $usermodel->id;
                    $empusermodel->role = Yii::app()->user->getState('role');
                    $empusermodel->created_at = $now->format('Y-m-d H:i:s');
                    if($empusermodel->save())
                    {
                        //  profile model
                        $profilemodel = new Profiles;
                        $profilemodel->owner_id = $model->id;
                        $profilemodel->owner_type = 'employer';
                        $profilemodel->type = 'employer';
                        $profilemodel->created_at = $now->format('Y-m-d H:i:s');
                        $profilemodel->slug = $this->generateSlug(null,$usermodel->forenames."-".$usermodel->surname);
                        $profilemodel->save();
                    }
                }
          
            }
        }
           Yii::app()->user->setFlash('success', 'Successfully Employers created.');
           $this->redirect($this->createUrl('employers/admin'));
    }
    public function actioncheckEmails(){
        $index = array();
        $cnt = 1;
        foreach ($_POST['emails'] as $email)
        {
            $cri = new CDbCriteria();
            $cri->condition = "email ='".$email."'";
            $model = Employers::model()->find($cri);
            if($model != null)
                $index[] = $cnt;
                
                $cnt++;
        }
        if(count($index) > 0)
            echo json_encode($index);
            else
                echo -1;
    }
    public function getValidatedSlug($slug)
    {
        $cri = new CDbCriteria();
        $cri->condition = "slug ='".$slug."'";
        $pmodel = Profiles::model()->findAll($cri);   
        
        $cri1 = new CDbCriteria();
        $cri1->condition = "slug like '".$slug."-%'";
        $pmodel1 = Profiles::model()->findAll($cri1);  
        
        if(count($pmodel1) == 0 && count($pmodel) == 1)
        {
            return strtolower($slug)."-1";
        }
        else if(count($pmodel1) > 0)
        {
            return (strtolower($slug)."-".rand()); //(count($pmodel1)+1));
        }
        else if(count($pmodel) == 0)
            return strtolower($slug);
        else
            return (strtolower($slug)."-".rand());
                  
    }
    
    public function actionapproval()
    {
        $imodel = TempIndustry::model();
        $scmodel = TempSkillCategory::model();
        $smodel = TempSkills::model();
        
        $this->render('approval',array('imodel'=>$imodel,'scmodel'=>$scmodel,'smodel'=>$smodel));
    }
    
    public function actionindustryAction()
    {
        $ids = implode(',', $_POST['ids']);
        $action = $_POST['action'];
        $cri = new CDbCriteria();
        $cri->condition = "id in (" . $ids . ")";
        
        $tmpmodel = TempIndustry::model()->findAll($cri);
        
        foreach ($tmpmodel as $tmp)
        {
            $empmodel = Employers::model()->findByPk($tmp->employer_id);
            $insmodel = Industries::model()->findByPk($tmp->industry_id);
            
            $body = $this->renderPartial("//emails/admin-industry-notification", array('skill' => $insmodel->name,'action' => $action,'model'=>$empmodel), true);
            // Send employer viewed notifcation to candidate
            Yii::import('application.extensions.phpmailer.JPhpMailer');
            $mail = new JPhpMailer;
            $mail->Host = 'mail.cvvid.com';
            $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
            $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
            $mail->SMTPSecure = 'ssl';
            $mail->SetFrom('mail@cvvid.com', 'CVVID');
            $mail->Subject = "INDUSTRY APPROVAL NOTIFICATION!";
            $mail->MsgHTML($body);
            $mail->AddAddress($empmodel->email, $empmodel->name);
            $mail->Send();
        }
        
        if($action == "approve")
            TempIndustry::model()->deleteAll($cri);
        else 
            TempIndustry::model()->updateAll(array('active' => 0), $cri);
        
    }
    
   public function actionskillcategoryAction()
    {
        $ids = implode(',', $_POST['ids']);
        $action = $_POST['action'];
        $cri = new CDbCriteria();
        $cri->condition = "id in (" . $ids . ")";
        
        if($action == "approve")
            TempSkillCategory::model()->deleteAll($cri);
        else
            TempSkillCategory::model()->updateAll(array('active' => 0), $cri);
      
    }
    
    public function actionskillsAction()
    {
        $ids = implode(',', $_POST['ids']);
        $action = $_POST['action'];
        $cri = new CDbCriteria();
        $cri->condition = "id in (" . $ids . ")";
        
        if($action == "approve")
            TempSkills::model()->deleteAll($cri);
        else
            TempSkills::model()->updateAll(array('active' => 0), $cri);
    }
    /**
     * Notify the user
     */
    public function notifyemployerupgrade($model, $customerid) {
        $body = $this->renderPartial("//emails/invoice", array('customerid' => $customerid), true);
        // Send register notifcation to employer admin
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->Host = 'mail.cvvid.com';
        $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
        $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
        $mail->SMTPSecure = 'ssl';
        $mail->SetFrom('info@cvvid.com', 'CVVID');
        $mail->Subject = 'Invoice details for CVVid.com';
        $mail->MsgHTML($body);
        $mail->AddAddress($model->email, $model->name);
        $mail->Send();
    }
    
    public function str_slug($title, $separator = '-')
    {
        
        // $title = static::ascii($title);
        
        // Convert all dashes/underscores into separator
        $flip = $separator == '-' ? '_' : '-';
        
        $title = preg_replace('!['.preg_quote($flip).']+!u', $separator, $title);
        
        // Remove all characters that are not the separator, letters, numbers, or whitespace.
        $title = preg_replace('![^'.preg_quote($separator).'\pL\pN\s]+!u', '', mb_strtolower($title));
        
        // Replace all separator characters and whitespace by a single separator
        $title = preg_replace('!['.preg_quote($separator).'\s]+!u', $separator, $title);
        
        return trim($title, $separator);
        
        
        
        // return Str::slug($title, $separator);
    }
    
    
    public function actiongenerateSlug($id = null,$slug)
    {
            $slug = str_replace("'", "", $slug);
       $slug = str_replace(" ", "", $slug);
       
        $cri = new CDbCriteria();
        $cri->condition = "slug REGEXP '^$slug(-[0-9]*)?$'";
        $pmdoel = Profiles::model()->findAll($cri);
        
        $cri1 = new CDbCriteria();
        $cri1->condition = "slug REGEXP '^$slug(-[0-9]*)?$' and id != ".$id;
        $index = ($id) ? count(Profiles::model()->findAll($cri1)) : count($pmdoel);
        
        return ($index > 0) ? "{$slug}-{$index}" : $slug;
        
    }
    
    public function loadModelSlug($slug)
    {
        //         $umodel = Users::model()->findByPk($id);
        $model = Profiles::model()->findByAttributes(array('slug'=>$slug));
        $emodel = Employers::model()->findByPk($model->owner_id);
        if($emodel===null)
            throw new CHttpException(404,'The requested page does not exist.');
            return $emodel;
    }
    
    public function actiontabupgrade($id) {
       
        $model = Employers::model()->findByPk($id);
        if($model->user_id > 0)
        {
            $usermodel = Users::model()->findByPk($model->user_id);
            $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $model->id, 'type' => $usermodel->type));
            $cri = new CDbCriteria();
            $cri->condition = "model_id = ".$model->id." and model_type like '%employer%' and deleted_at is null";
            
            $addressmodel = Addresses::model()->find($cri);
            if($addressmodel == null)
                $addressmodel = new Addresses();
        }
        else
        {
            $usermodel = new Users();
            $profilemodel = new Profiles();
            $addressmodel = new Addresses();
            
        }
        
        $this->render('tabupgrade', array('model' => $model, 'profilemodel' => $profilemodel, 'addressmodel' => $addressmodel));
    }
    
    public function actionpremiumUpgrade($id)
    {
        $d = str_replace('/', '-', $_POST["end_date"]);
        $enddate = date('Y-m-d H:i:s', strtotime($d));
        
        $model = Employers::model()->findByPk($id);
        if($model->user_id > 0)
        {
            $usermodel = Users::model()->findByPk($model->user_id);
            $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $model->id, 'type' => $usermodel->type));
            $cri = new CDbCriteria();
            $cri->condition = "model_id = ".$model->id." and model_type like '%employer%' and deleted_at is null";
            
            $addressmodel = Addresses::model()->find($cri);
            if($addressmodel == null)
                $addressmodel = new Addresses();
        }
        else
        {
            $usermodel = new Users();
            $profilemodel = new Profiles();
            $addressmodel = new Addresses();
            
        }     
            
            $model->stripe_active = 1;
            $model->subscription_ends_at = $enddate;
            if($model->save())
                Yii::app()->user->setFlash('success', 'Upgraded Successfully.');
                $this->render('tabupgrade', array('model' => $model, 'profilemodel' => $profilemodel, 'addressmodel' => $addressmodel,'usermodel'=>$usermodel));
    }

public function actionadminupgrade($id)
    {
        $emodel = Employers::model()->findByPk($id);
        $emodel->stripe_active = 1;
        
        $subs = Carbon::now()->addMonth()->toDateTimeString();        
        $emodel->subscription_ends_at = $subs;
        $emodel->save();
        
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $id, 'type' => 'employer'));
        $this->redirect($this->createUrl('employers/employer', array('slug' => $profilemodel->slug)));
        
    }
    
    public function actionadminChangeStatus()
    {
        $status = $_POST['status'];
        $id = $_POST['employerid'];
        $emodel = Employers::model()->findByPk($id);
        $empusermodel = EmployerUsers::model()->findByAttributes(array('employer_id'=>$id));
       
        $umodel = Users::model()->findByPk($empusermodel->user_id);
        
        if ($status == 'blocked') {
            $prostat = 'active';
        }
        else 
        {
            $prostat = 'blocked';
        }
        $umodel->status = $prostat;
        $umodel->save();
        
    }
    
     public function actionadminChangeEnddate()
    {
        $end_date = $_POST['date'];
        $id = $_POST['employerid'];
        
        $now = new DateTime();
        $curr_date =  $now->format('Y-m-d');
        
        $date = DateTime::createFromFormat('d/m/Y', $end_date);
        $end_date = $date->format('Y-m-d');
        
        $datetime1 = date_create($curr_date);
        $datetime2 = date_create($end_date);
        $interval = date_diff($datetime1, $datetime2);

        $days = $interval->days;
      
        $emodel = Employers::model()->findByPk($id);
        $emodel->stripe_active = 1;
        
        $subs = Carbon::now()->addDays($days)->toDateTimeString();
        $emodel->subscription_ends_at = $subs;
        $emodel->save();
        
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $id, 'type' => 'employer'));
        $this->redirect($this->createUrl('employers/employer', array('slug' => $profilemodel->slug)));
    }

 public function sendMessageE($subject,$message,$from,$to) {
        
        $now = new DateTime();
        $conmodel = new Conversations();
        $conmodel->subject = $subject;
        $conmodel->created_at = $now->format('Y-m-d H:i:s');
        $conmodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($conmodel->save()) {
            $usermodel = Users::model()->findByPk($from);
            $conmsgmodel = new ConversationMessages();
            $conmsgmodel->conversation_id = $conmodel->id;
            $conmsgmodel->user_id = $from;
            $conmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
            $conmsgmodel->message = $message;
            $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
            $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($conmsgmodel->save()) {
                $conmemmodel_sender = new ConversationMembers();
                $conmemmodel_sender->conversation_id = $conmodel->id;
                $conmemmodel_sender->user_id = $from;
                $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
                $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
                $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                if ($conmemmodel_sender->save()) {
                    $usermodel1 = Users::model()->findByPk($to);
                    $conmemmodel_rec = new ConversationMembers();
                    $conmemmodel_rec->conversation_id = $conmodel->id;
                    $conmemmodel_rec->user_id = $to;
                    $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
                    $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                    $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                    if ($conmemmodel_rec->save()) {
                        $actmodel_sender = new Activities();
                        $actmodel_sender->user_id = $from;
                        $actmodel_sender->type = "message";
                        $actmodel_sender->model_id = $conmsgmodel->id;
                        $actmodel_sender->model_type = "ConversationMessage";
                        $actmodel_sender->subject = "You sent a message in conversation: " . $subject;
                        $actmodel_sender->message = $message;
                        $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                        $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                        if ($actmodel_sender->save()) {
                            $actmodel_rec = new Activities();
                            $actmodel_rec->user_id = $to;
                            $actmodel_rec->type = "message";
                            $actmodel_rec->model_id = $conmsgmodel->id;
                            $actmodel_rec->model_type = "ConversationMessage";
                            $actmodel_rec->subject = "You received a message in conversation: " . $subject;
                            $actmodel_rec->message = $message;
                            $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                            $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                            $actmodel_rec->save();
                        }
                    }
                }
            }
        }
    }
    
    public function actionextendTrail()
    {
        $user_id = $_POST['user_id'];
        $trial_ends_at = $_POST['trial_ends_at'];
        
        $user = Users::model()->findByPk($user_id);
        
        if($user->id != $user_id)
            echo "0";
        
        $timestamp = strtotime($trial_ends_at);
        
        $user->trial_ends_at = date("Y-m-d H:i:s", $timestamp);
        $user->stripe_active = 1;
        $user->save(FALSE);
        
        echo "1";
        
    }
    
    public function actionextendSubscription()
    {
        $employer_id = $_POST['employer_id'];
        $subscription_ends_at = $_POST['subscription_ends_at'];
        
        $user = Employers::model()->findByPk($employer_id);
        
        if($user->id != $employer_id)
            echo "0";
        
        $timestamp = strtotime($subscription_ends_at);
        
        $user->subscription_ends_at = date("Y-m-d H:i:s", $timestamp);
        $user->stripe_active = 1;
        $user->trial_ends_at = NULL;
        $user->save(FALSE);
        
        echo "1";
        
    }
    
    public function actionmakeBasic()
    {
        $user_id = $_POST['user_id'];
        
        $user = Users::model()->findByPk($user_id);
        
        if($user->id != $user_id)
            echo "0";
        
        $user->trial_ends_at = NULL;
        $user->stripe_active = 0;
        $user->stripe_id = NULL;
        $user->subscription_ends_at = NULL;
        $user->save(FALSE);
        
        echo "1";
        
    }
public function actionusersDelete() {
        $ids = implode(',', $_POST['ids']);
        
        $dcri = new CDbCriteria();
        $dcri->condition = "user_id in (" . $ids . ")";
        $eusers = EmployerUsers::model()->deleteAll($dcri);
        
        $cri = new CDbCriteria();
        $cri->condition = "id in (" . $ids . ")";
        Users::model()->deleteAll($cri);
        
        Yii::app()->user->setFlash('success', 'Successfully Deleted');
        
    }
	

    public function actionemployerjobCreate($id)
    {
        $model=new Jobs;
        $employermodel = Employers::model()->findByPk($id);
        $industrymodel = Industries::model();
        $skillcategmodel = SkillCategories::model();
        $skillsmodel = Skills::model();
        $jobskillmodel = JobSkill::model();
        
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        
        if(isset($_POST['Jobs']))
        {
            $model->attributes=$_POST['Jobs'];
            $model->save();
            //if($model->save())
                //$this->redirect(array('view','id'=>$model->id));
        }
        
        $this->render('employerjobs',array(
            'model'=>$model,'empmodel'=>$employermodel,'industrymodel'=>$industrymodel,'skillcategorymodel'=>$skillcategmodel,'skillmodel'=>$skillsmodel,'jobskillmodel'=>$jobskillmodel
        ));
    }
    
    public function actioncreateEmpJobs($id)
    {
//        echo var_dump($_POST);
//        die();
        $expired = 0;
        $model=new Jobs;
        $employermodel = Employers::model()->findByPk($id);
        $industrymodel = Industries::model();
        $skillcategmodel = SkillCategories::model();
        $skillsmodel = Skills::model();
        $jobskillmodel = JobSkill::model();
        
        $usermodel = Users::model()->findByPk($employermodel->user_id);
        $now = new DateTime();
        
        $model->attributes = $_POST['Jobs'];
        $model->owner_id = $id;
//         $model->description = $_POST["description"];
        if(isset($_POST['competitive_salary']))
        {
            $model->competitive_salary = 1;
            $model->salary_min = 0;
            $model->salary_max = 0;
        }
        if(isset($_POST['industry_id']))
            $model->industry_id = $_POST["industry_id"];
        
        if(!isset($_POST['skills']))
            $model->setScenario("skillsrequired");
            if ($model->validate()) {
                //  insert jobs details
                
                $model->owner_type = 'employer';
                
                $model->user_id = Yii::app()->user->getState('userid');
               
                $d = str_replace('/','-',$_POST["end_date"]);
                $endformat = date('Y-m-d' , strtotime($d));
                //$endformat = date_format(new DateTime($_POST["end_date"]),"Y-m-d");
                $model->end_date = $endformat;
                // $model->skill_category_id = $_POST["skill_category_id"];
                $model->type = $_POST["type"];
                $model->video_only = isset($_POST["video_only"]) ? 1 : 0;
                //$model->additional = $_POST["additional"];
                $model->location_lat = $_POST["location_lat"];
                $model->location_lng = $_POST["location_lng"];
                $model->created_at = $now->format('Y-m-d H:i:s');
                $model->careers = 1;
                $model->salary_type = $_POST["salary_type"];
                if($model->save())
                {
                    if(isset($_POST["cvvid"]))
                    {
                        $cvvidjob = new Jobs();
                        $cvvidjob->attributes = $model->attributes;
                        $cvvidjob->careers = 0;
                        if($cvvidjob->save())
                        {
                            if(isset($_POST['skills']))
                            {
                                $skills = $_POST['skills'];
                                foreach ($skills as $skills_val){
                                    $jobskillmodel=new JobSkill;
                                    $jobskillmodel->job_id = $cvvidjob->id;
                                    $jobskillmodel->skill_id = $skills_val;
                                    if(isset($_POST[$skills_val]))
                                        $jobskillmodel->vacancies = $_POST[$skills_val];
                                        $jobskillmodel->save();
                                        
                                }
                            }
                        }
                    }
                    
                    if(Membership::subscribed($employermodel->stripe_active, $employermodel->trial_ends_at, $employermodel->subscription_ends_at))
                    {
                        if($employermodel->stripe_plan == "employer_vacancy" && $employermodel->current_vacancy_count > 0)
                        {
                            $curr = $employermodel->current_vacancy_count - 1;
                            if($curr <= 0)
                            {
                                $expired = 1;
                                
                                $yest_timestamp = strtotime("- 1 day");
                                $yest_date = date("Y-m-d H:i:s", $yest_timestamp);
                                $employermodel->subscription_ends_at = $yest_date;
                                //$employermodel->trial_ends_at = $yest_date;
                                $employermodel->stripe_active = 0;
                                $employermodel->save();
                            }
                            
                            Employers::model()->updateByPk($employermodel->id, array('current_vacancy_count'=>$curr));
                        }
                    }
                    
                    $aemodel = AgencyEmployers::model()->findByAttributes(array('employer_id'=>$id));
                    if($aemodel != null)
                    {
                        $avacmodel = new AgencyVacancies();
                        $avacmodel->agency_id = $aemodel->agency_id;
                        $avacmodel->job_id = $model->id;
                        $avacmodel->created_at = $now->format('Y-m-d H:i:s');
                        $avacmodel->save();
                    }
                    if(isset($_POST['skills']))
                    {
                        $skills = $_POST['skills'];
                        foreach ($skills as $skills_val){
                            $jobskillmodel=new JobSkill;
                            $jobskillmodel->job_id = $model->id;
                            $jobskillmodel->skill_id = $skills_val;
                            if(isset($_POST[$skills_val]))
                                $jobskillmodel->vacancies = $_POST[$skills_val];
                                $jobskillmodel->save();
                        }
                    }
                    if(isset($_FILES['jdfile']['name']) && $_FILES['jdfile']['name'] != "")
                    {
                        $mediamodel = new Media();
                        $mediamodel->model_type = "Employer";
                        $mediamodel->model_id = $id;
                        $mediamodel->collection_name = "documents";
                        $mediamodel->name = pathinfo($_FILES['jdfile']['name'], PATHINFO_FILENAME);
                        $mediamodel->file_name = $_FILES['jdfile']['name'];
                        $mediamodel->size = $_FILES['jdfile']['size'];
                        $mediamodel->disk = "documents";
                        $mediamodel->created_at = $now->format('Y-m-d H:i:s');
                        if($mediamodel->save())
                        {
                            $mediafolder = "images/media/" . $mediamodel->id . "/conversions";
                            
                            if (!file_exists($mediafolder)) {
                                mkdir($mediafolder, 0777, true);
                            }
                            $filename = $_FILES['jdfile']['name'];
                            $fullpath = "images/media/" . $mediamodel->id . "/" . $filename;
                            if (move_uploaded_file($_FILES['jdfile']['tmp_name'], $fullpath)) {
                                
                            }
                            Jobs::model()->updateByPk($model->id,array('media_id'=>$mediamodel->id));
                        }
                    }
                    
                    
                    //employer notification
                  //  if($employermodel->email != "")
//                         $this->notifyjob($model, $employermodel->email, $employermodel->name,$expired);
                        
                        Yii::app()->user->setFlash('success', 'Successfully created job');
                        $this->redirect(Yii::app()->createUrl('employers/tabjobs', array('id' => $id)));
                }
            }
            $this->render('employerjobs',array(
                'model'=>$model,'empmodel'=>$employermodel,'industrymodel'=>$industrymodel,'skillcategorymodel'=>$skillcategmodel,'skillmodel'=>$skillsmodel,'jobskillmodel'=>$jobskillmodel
            ));
            
    }
}
