<?php

class AgencyEmployersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	public $employerid = 0;
	public $edit = 0;
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
// 			array('deny',  // deny all users
// 				'users'=>array('*'),
// 			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new AgencyEmployers;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AgencyEmployers']))
		{
			$model->attributes=$_POST['AgencyEmployers'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AgencyEmployers']))
		{
			$model->attributes=$_POST['AgencyEmployers'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('AgencyEmployers');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new AgencyEmployers('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['AgencyEmployers']))
			$model->attributes=$_GET['AgencyEmployers'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return AgencyEmployers the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=AgencyEmployers::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param AgencyEmployers $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='agency-employers-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function loadModelSlug($slug)
	{
	    //         $umodel = Users::model()->findByPk($id);
	    $model = Profiles::model()->findByAttributes(array('slug'=>$slug));
	    $emodel = Employers::model()->findByPk($model->owner_id);
	    if($emodel===null)
	        throw new CHttpException(404,'The requested page does not exist.');
	        return $emodel;
	}
	
	public function actionEdit($slug) {
// 	    if(Yii::app()->user->getState('role') != "employer")
// 	        $this->redirect($this->createUrl('site/index'));
	        
	        $this->layout = "main";
	        $model = $this->loadModelSlug($slug); //Employers::model()->findByPk($id);
	        $id = $model->id;
	        $this->employerid = $id;
	        $this->edit = 1;
	        
            $this->render('editemployer', array(
                'model' => $model,
            ));
	}
	public function actionInbox($id) {
	    $this->layout = "main";
	    $empmodel = Employers::model()->findByPk($id);
	    $model = Users::model()->findByPk(Yii::app()->user->getState('userid'));
	    $this->render("inbox", array('model' => $model,'empmodel'=>$empmodel));
	}
	
	public function actiongetInboxMessages() {
	    $this->layout = "main";
	    $convmodel = Conversations::model()->findByPk($_POST['id']);
	    
	    $now = new DateTime();
	    $ccri = new CDbCriteria();
	    $ccri->condition = 'conversation_id ='.$convmodel->id;
	    ConversationMembers::model()->updateAll(array('last_read'=>$now->format('Y-m-d H:i:s')),$ccri);
	    
	    $this->renderPartial('inbox_messages', array('convmodel' => $convmodel, 'to_user' => $_POST['msg_to']));
	}
	
	public function actionsaveMessage() {
	    $this->layout = "main";
	    $conid = $_POST['id'];
	    $fromuser = Yii::app()->user->getState('userid');
	    $touser = $_POST['msg_to'];
	    $msg = $_POST['message'];
	    $now = new DateTime();
	    
	    $convmodel = Conversations::model()->findByPk($_POST['id']);
	    $usermodel = Users::model()->findByPk($fromuser);
	    
	    $convmsgmodel = new ConversationMessages();
	    $convmsgmodel->conversation_id = $conid;
	    $convmsgmodel->user_id = $fromuser;
	    $convmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
	    $convmsgmodel->message = $msg;
	    $convmsgmodel->created_at = $now->format('Y-m-d H:i:s');
	    $convmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
	    if ($convmsgmodel->save()) {
	        $conmemmodel_sender = new ConversationMembers();
	        $conmemmodel_sender->conversation_id = $convmodel->id;
	        $conmemmodel_sender->user_id = Yii::app()->user->getState('userid');
	        $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
	        $conmemmodel_sender->last_read = NULL;
	        $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
	        $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
	        if ($conmemmodel_sender->save()) {
	            $usermodel1 = Users::model()->findByPk($_POST['msg_to']);
	            $conmemmodel_rec = new ConversationMembers();
	            $conmemmodel_rec->conversation_id = $convmodel->id;
	            $conmemmodel_rec->user_id = $_POST['msg_to'];
	            $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
	            $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
	            $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
	            if ($conmemmodel_rec->save()) {
	                $activitymodel = new Activities();
	                $activitymodel->user_id = $fromuser;
	                $activitymodel->type = "message";
	                $activitymodel->model_id = $convmsgmodel->id;
	                $activitymodel->model_type = "App\Messaging\ConversationMessage";
	                $activitymodel->subject = "You sent a message in conversation: " . $convmodel->subject;
	                $activitymodel->message = $msg;
	                $activitymodel->created_at = $now->format('Y-m-d H:i:s');
	                $activitymodel->updated_at = $now->format('Y-m-d H:i:s');
	                $activitymodel->save();
	                
	                $activitymodel = new Activities();
	                $activitymodel->user_id = $touser;
	                $activitymodel->type = "message";
	                $activitymodel->model_id = $convmsgmodel->id;
	                $activitymodel->model_type = "App\Messaging\ConversationMessage";
	                $activitymodel->subject = "You sent a message in conversation: " . $convmodel->subject;
	                $activitymodel->message = $msg;
	                $activitymodel->created_at = $now->format('Y-m-d H:i:s');
	                $activitymodel->updated_at = $now->format('Y-m-d H:i:s');
	                $activitymodel->save();
	            }
	        }
	    }
	    Yii::app()->user->setFlash('success', 'Message sent successfully');
	    $this->renderPartial('inbox_messages', array('convmodel' => $convmodel, 'to_user' => $_POST['msg_to']));
	}
	
	public function actiondeleteEmployers()
	{
	    $idstring = implode(',', $_POST['ids']);
	    $ids = $_POST['ids'];
	    $now = new DateTime();
	    
	    for($i = 0; $i < count($ids); $i++)
	    {
	        $now = new DateTime();
	        $aempmodel = AgencyEmployers::model()->findByPk($ids[$i]);
	        $empmodel = Employers::model()->findByPk($aempmodel->employer_id);
	        $empmodel->deleted_at = $now->format('Y-m-d H:i:s');
	        $empmodel->save();
	        
	        $usermodel = Users::model()->findByPk($empmodel->user_id);
	        $usermodel->deleted_at = $now->format('Y-m-d H:i:s');
	        $usermodel->save();
	        
	        $cri = new CDbCriteria();
	        $cri->condition = "type like '%employer%' and owner_id=".$empmodel->id;
	        $profilemodel = Profiles::model()->find($cri);
	        $profilemodel->deleted_at = $now->format('Y-m-d H:i:s');
	        $profilemodel->save();
	        
	        $cri1 = new CDbCriteria();
	        $cri1->condition = "model_type like '%employer%' and model_id=".$empmodel->id." and deleted_at is null";
	        $addressmodel = Addresses::model()->find($cri1);
	        $addressmodel->deleted_at = $now->format('Y-m-d H:i:s');
	        $addressmodel->save();
	        
	        $umodel = Users::model()->findByPk($ids[$i]);
	        Users::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')),'id =:id',array(':id'=>$umodel->id));
	        Employers::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')),'id =:id',array(':id'=>$umodel->id));
	        
	        AgencyEmployers::model()->deleteByPk($ids[$i]);
	    }
	    
	    
	}
	
	public function actionjobsadd($slug) {
	    
	    $this->layout = "main";
	    
	    $model=new Jobs;
	    
	    $cri = new CDbCriteria();
	    $cri->condition = "slug='".$slug."'";
	    $profilemodel = Profiles::model()->find($cri);
	    
	    $empmodel = Employers::model()->findByPk($profilemodel->owner_id);
	    $industrymodel = Industries::model();
	    $skillcategmodel = SkillCategories::model();
	    $skillsmodel = Skills::model();
	    $jobskillmodel = JobSkill::model();
	    
	    $this->render('addjobs', array(
	        'model' => $model,'empmodel' => $empmodel,'industrymodel'=>$industrymodel,'skillcategmodel'=>$skillcategmodel,'skillsmodel'=>$skillsmodel,
	        'jobskillmodel'=>$jobskillmodel
	    ));
	}
}
