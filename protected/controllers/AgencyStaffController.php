<?php

class AgencyStaffController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
    public $layout = '//layouts/admin';
    public $userid = 0;
    public $edit = 0;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
// 			array('deny',  // deny all users
// 				'users'=>array('*'),
// 			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new AgencyStaff;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AgencyStaff']))
		{
			$model->attributes=$_POST['AgencyStaff'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['AgencyStaff']))
		{
			$model->attributes=$_POST['AgencyStaff'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('AgencyStaff');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new AgencyStaff('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['AgencyStaff']))
			$model->attributes=$_GET['AgencyStaff'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return AgencyStaff the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=AgencyStaff::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param AgencyStaff $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='agency-staff-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	public function actionEdit($id)
	{
	    $astaffmodel = AgencyStaff::model()->findByPk($id);
	    
	    $this->layout = "main";
	    $this->userid = $astaffmodel->user_id;
	    $this->edit = 1;
	    	    
	    $model = Users::model()->findByPk($astaffmodel->user_id);
	    $agencymodel = Agency::model()->findByPk($astaffmodel->agency_id);
	    $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$model->id));
	    
	    $this->render('editprofile', array(
	        'model' => $model,'agencymodel'=>$agencymodel,'profilemodel'=>$profilemodel,'astaffmodel'=>$astaffmodel
	    ));
	}
	
	public function actionaddCandidate()
	{
	    $this->layout = "main";
	    $agencystaffmodel = AgencyStaff::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
	    $model = Agency::model()->findByPk($agencystaffmodel->agency_id);
	    $acanmodel = new AgencyCandidates();
	    $usermodel = new Users();
	    $addressmodel = new Addresses();
	    
	    $this->render("candidateform",array('model'=>$model,'agencystaffmodel'=>$agencystaffmodel,'agencycanmodel'=>$acanmodel,'usermodel'=>$usermodel,'addressmodel'=>$addressmodel));
	    
	}
	
	public function actionsaveCandidate()
	{
	    $this->layout='main';
	    $agencystaffmodel = AgencyStaff::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
	    $model = Agency::model()->findByPk($agencystaffmodel->agency_id);
	    $usermodel = new Users;
	    $profilemodel = new Profiles;
	    $addressmodel = new Addresses;
	    
	    $now = new DateTime();
	    
	    if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
	        $usermodel->setScenario('confirm');
	        
	        if(isset($_POST['Users'])){
	            $usermodel->attributes=$_POST['Users'];
	        }
	        if(isset ($_POST["Addresses"])){
	            $addressmodel->attributes = $_POST["Addresses"];
	        }
	        if(isset($_POST['Users']))
	        {
	            $valid = $usermodel->validate();
	            $valid = $addressmodel->validate() && $valid;
	            
	            if($valid){
	                $usermodel->created_at = $now->format('Y-m-d H:i:s');
	                $usermodel->type = 'candidate';
	                $usermodel->stripe_active = 1;
	                $usermodel->trial_ends_at = Carbon::now()->addYear()->toDateTimeString();
	                
	                $usermodel->location = $_POST['Addresses']['county'].($_POST['Addresses']['county']!=''? ', '.$_POST['Addresses']['country']: '');
	                $usermodel->location_lat = $_POST['Addresses']['latitude'];
	                $usermodel->location_lng = $_POST['Addresses']['longitude'];
	                $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
	                if($usermodel->save())
	                {
	                   
	                    $agencycanmodel = new AgencyCandidates();
	                    $agencycanmodel->agency_id = $model->id;
	                    $agencycanmodel->user_id = $usermodel->id;
	                    $agencycanmodel->created_at = $now->format('Y-m-d H:i:s');
	                    if($agencycanmodel->save())
	                    {
    	                    $staffcanmodel = new AgencyStaffCandidates();
    	                    $staffcanmodel->staff_id = $agencystaffmodel->user_id;
    	                    $staffcanmodel->agency_candidate_id = $agencycanmodel->id;
    	                    $staffcanmodel->agency_id = $model->id;
    	                    $staffcanmodel->created_at = $now->format('Y-m-d H:i:s');
    	                    $staffcanmodel->save();
	                    }
	                }
	                $profilemodel->owner_id = $usermodel->id;
	                $profilemodel->owner_type = 'candidate';
	                
	                $profilemodel->type = 'candidate';
	                $profilemodel->slug = $this->getValidatedSlug($usermodel->forenames."-".$usermodel->surname);
	                $profilemodel->save();
	                //address update
	                $addressmodel->created_at = $now->format('Y-m-d H:i:s');
	                $addressmodel->model_type = 'Candidate';
	                $addressmodel->model_id = $usermodel->id;
	                if ($addressmodel->save()) {
	                    
	                    $umodel = Users::model()->findByPk($model->user_id);
	                    
	                    $this->notifycandidate($usermodel, $_POST['Users']['password']);
	                    
	                    $this->redirect($this->createUrl('agencyStaff/edit', array('id' => $agencystaffmodel->id)));
	                    
	                }
	                
	            }
	        }
	        $this->render('candidateform', array('model' => $model,'agencystaffmodel'=>$agencystaffmodel,'agencycanmodel' => new AgencyCandidates(),'profilemodel' => $profilemodel, 'addressmodel' => $addressmodel, 'usermodel' => $usermodel));
	} 
	
	public function actioneditcandidate($id)
	{
	    $this->layout='main';
	    $agencystaffmodel = AgencyStaff::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
	    $agencystaffcanmodel = AgencyStaffCandidates::model()->findByPk($id);
	    $agencycanmodel = AgencyCandidates::model()->findByPk($agencystaffcanmodel->agency_candidate_id);
	    $model = Agency::model()->findByPk($agencycanmodel->agency_id);
	    
	    $usermodel = Users::model()->findByPk($agencycanmodel->user_id);
	    $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id));
	    
	    $cri = new CDbCriteria();
	    $cri->condition = "model_id =".$usermodel->id." and model_type like '%candidate%' and deleted_at is null";
	    $addressmodel = Addresses::model()->find($cri);
	    $this->render("candidateform",array('model'=>$model,'agencystaffmodel'=>$agencystaffmodel,'agencycanmodel'=>$agencycanmodel,'usermodel'=>$usermodel,'profilemodel' => $profilemodel,'addressmodel'=>$addressmodel));
	}
	
	public function actionupdateCandidate($id)
	{
	    $this->layout='main';
	    $agencystaffmodel = AgencyStaff::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
	   
	    $agencycanmodel = AgencyCandidates::model()->findByPk($id);
	    $userid = $agencycanmodel->user_id;
	    $agencyid = $agencycanmodel->agency_id;
	    	    
	    $agencymodel = Agency::model()->findByPk($agencyid);
	    $usermodel = Users::model()->findByPk($userid);
	    $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id));
	    
	    $cri = new CDbCriteria();
	    $cri->condition = "model_id =".$userid." and model_type like '%candidate%' and deleted_at is null";
	    $addressmodel = Addresses::model()->find($cri);
	    
	    $now = new DateTime();
	    
	    if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
	        $usermodel->setScenario('confirm');
	        
	        if(isset($_POST['Users'])){
	            $usermodel->attributes=$_POST['Users'];
	        }
	        if(isset ($_POST["Addresses"])){
	            $addressmodel->attributes = $_POST["Addresses"];
	        }
	        if(isset ($_POST["Profiles"])){
	            $profilemodel->attributes = $_POST["Profiles"];
	        }
	        if(isset($_POST['Users']))
	        {
	            $valid = $usermodel->validate();
	            // 	            $valid = $model->validate() && $valid;
	            $valid = $addressmodel->validate() && $valid;
	            
	            if($valid){
	                $usermodel->updated_at = $now->format('Y-m-d H:i:s');
	                $usermodel->location = $_POST['Addresses']['county'].($_POST['Addresses']['county']!=''? ', '.$_POST['Addresses']['country']: '');
	                $usermodel->location_lat = $_POST['Addresses']['latitude'];
	                $usermodel->location_lng = $_POST['Addresses']['longitude'];
	                // 	                $usermodel->tel = $_POST['Employers']['tel'];
	                $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
	                $usermodel->save();
	                
	                $profilemodel->slug = $this->getValidatedSlug($usermodel->forenames."-".$usermodel->surname);
	                $profilemodel->save();
	                //address update
	                $addressmodel->updated_at = $now->format('Y-m-d H:i:s');
	                if ($addressmodel->save()) {
	                    
	                    $this->redirect($this->createUrl('agencyStaff/edit', array('id' => $agencystaffmodel->id)));
	                    
	                }
	                
	            }
	        }
	        
	        $this->render("candidateform",array('model'=>$agencymodel,'agencycanmodel'=>$agencycanmodel,'usermodel'=>$usermodel,'profilemodel' => $profilemodel,'addressmodel'=>$addressmodel));
	}
	
	public function actiondeleteCandidates()
	{
	    $idstring = implode(',', $_POST['ids']);
	    $ids = $_POST['ids'];
	    $now = new DateTime();
	    for($i = 0; $i < count($ids); $i++)
	    {
	        $astaffcanmodel = AgencyStaffCandidates::model()->findByPk($ids[$i]);
	        $acanid = $astaffcanmodel->agency_candidate_id;
	        $acanmodel = AgencyCandidates::model()->findByPk($astaffcanmodel->agency_candidate_id);
	        $umodel = Users::model()->findByPk($acanmodel->user_id);
	        $cri = new CDbCriteria();
	        $cri->condition = "owner_id =".$umodel->id." and owner_type like '%candidate%'";
	        
	        Profiles::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')),$cri);
	        Users::model()->updateAll(array('deleted_at'=>$now->format('Y-m-d H:i:s')),'id =:id',array(':id'=>$umodel->id));
	        AgencyStaffCandidates::model()->deleteByPk($astaffcanmodel->id);
	        AgencyCandidates::model()->deleteByPk($acanid);
	        
	    }
	    
	}
	
	public function actionInbox() {
	    $this->layout = "main";
	    $agencystaffmodel = AgencyStaff::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
	    $agencymodel = Agency::model()->findByPk($agencystaffmodel->agency_id);
	    $model = Users::model()->findByPk($agencystaffmodel->user_id);
	    $this->render("inbox", array('model' => $model,'agencymodel'=>$agencymodel));
	}
	
	public function actiongetInboxMessages() {
	    $this->layout = "main";
	    $convmodel = Conversations::model()->findByPk($_POST['id']);
	    $this->renderPartial('inbox_messages', array('convmodel' => $convmodel, 'to_user' => $_POST['msg_to']));
	}
	
	public function actionsendMessage() {
	    $now = new DateTime();
	    $conmodel = new Conversations();
	    $conmodel->subject = $_POST['subject'];
	    $conmodel->created_at = $now->format('Y-m-d H:i:s');
	    $conmodel->updated_at = $now->format('Y-m-d H:i:s');
	    if ($conmodel->save()) {
	        $usermodel = Users::model()->findByPk(Yii::app()->user->getState('userid'));
	        $conmsgmodel = new ConversationMessages();
	        $conmsgmodel->conversation_id = $conmodel->id;
	        $conmsgmodel->user_id = Yii::app()->user->getState('userid');
	        $conmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
	        $conmsgmodel->message = $_POST['message'];
	        $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
	        $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
	        if ($conmsgmodel->save()) {
	            $conmemmodel_sender = new ConversationMembers();
	            $conmemmodel_sender->conversation_id = $conmodel->id;
	            $conmemmodel_sender->user_id = Yii::app()->user->getState('userid');
	            $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
	            $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
	            $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
	            $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
	            if ($conmemmodel_sender->save()) {
	                $usermodel1 = Users::model()->findByPk($_POST['user_id']);
	                $conmemmodel_rec = new ConversationMembers();
	                $conmemmodel_rec->conversation_id = $conmodel->id;
	                $conmemmodel_rec->user_id = $_POST['user_id'];
	                $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
	                $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
	                $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
	                if ($conmemmodel_rec->save()) {
	                    $actmodel_sender = new Activities();
	                    $actmodel_sender->user_id = Yii::app()->user->getState('userid');
	                    $actmodel_sender->type = "message";
	                    $actmodel_sender->model_id = $conmsgmodel->id;
	                    $actmodel_sender->model_type = "ConversationMessage";
	                    $actmodel_sender->subject = "You sent a message in conversation: " . $_POST['subject'];
	                    $actmodel_sender->message = $_POST['message'];
	                    $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
	                    $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
	                    if ($actmodel_sender->save()) {
	                        $actmodel_rec = new Activities();
	                        $actmodel_rec->user_id = $_POST['user_id'];
	                        $actmodel_rec->type = "message";
	                        $actmodel_rec->model_id = $conmsgmodel->id;
	                        $actmodel_rec->model_type = "ConversationMessage";
	                        $actmodel_rec->subject = "You received a message in conversation: " . $_POST['subject'];
	                        $actmodel_rec->message = $_POST['message'];
	                        $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
	                        $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
	                        $actmodel_rec->save();
	                    }
	                }
	            }
	        }
	    }
	}
	
	public function actionsaveMessage() {
	    $this->layout = "main";
	    $conid = $_POST['id'];
	    $fromuser = Yii::app()->user->getState('userid');
	    $touser = $_POST['msg_to'];
	    $msg = $_POST['message'];
	    $now = new DateTime();
	    
	    $convmodel = Conversations::model()->findByPk($_POST['id']);
	    $usermodel = Users::model()->findByPk($fromuser);
	    
	    $convmsgmodel = new ConversationMessages();
	    $convmsgmodel->conversation_id = $conid;
	    $convmsgmodel->user_id = $fromuser;
	    $convmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
	    $convmsgmodel->message = $msg;
	    $convmsgmodel->created_at = $now->format('Y-m-d H:i:s');
	    $convmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
	    if ($convmsgmodel->save()) {
	        $conmemmodel_sender = new ConversationMembers();
	        $conmemmodel_sender->conversation_id = $convmodel->id;
	        $conmemmodel_sender->user_id = Yii::app()->user->getState('userid');
	        $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
	        $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
	        $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
	        $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
	        if ($conmemmodel_sender->save()) {
	            $usermodel1 = Users::model()->findByPk($_POST['msg_to']);
	            $conmemmodel_rec = new ConversationMembers();
	            $conmemmodel_rec->conversation_id = $convmodel->id;
	            $conmemmodel_rec->user_id = $_POST['msg_to'];
	            $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
	            $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
	            $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
	            if ($conmemmodel_rec->save()) {
	                $activitymodel = new Activities();
	                $activitymodel->user_id = $fromuser;
	                $activitymodel->type = "message";
	                $activitymodel->model_id = $convmsgmodel->id;
	                $activitymodel->model_type = "App\Messaging\ConversationMessage";
	                $activitymodel->subject = "You sent a message in conversation: " . $convmodel->subject;
	                $activitymodel->message = $msg;
	                $activitymodel->created_at = $now->format('Y-m-d H:i:s');
	                $activitymodel->updated_at = $now->format('Y-m-d H:i:s');
	                $activitymodel->save();
	                
	                $activitymodel = new Activities();
	                $activitymodel->user_id = $touser;
	                $activitymodel->type = "message";
	                $activitymodel->model_id = $convmsgmodel->id;
	                $activitymodel->model_type = "App\Messaging\ConversationMessage";
	                $activitymodel->subject = "You sent a message in conversation: " . $convmodel->subject;
	                $activitymodel->message = $msg;
	                $activitymodel->created_at = $now->format('Y-m-d H:i:s');
	                $activitymodel->updated_at = $now->format('Y-m-d H:i:s');
	                $activitymodel->save();
	            }
	        }
	    }
	    Yii::app()->user->setFlash('success', 'Message sent successfully');
	    $this->renderPartial('inbox_messages', array('convmodel' => $convmodel, 'to_user' => $_POST['msg_to']));
	}
	
	/**
	 * Notify the user
	 */
	public function notifycandidate($model, $password) {
	    $body = $this->renderPartial("//emails/user-account", array('user' => $model, 'password' => $password), true);
	    // Send register notifcation to employer admin
	    Yii::import('application.extensions.phpmailer.JPhpMailer');
	    $mail = new JPhpMailer;
	    $mail->Host = 'smtp.gmail.com';
	    $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
	    $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
	    $mail->SMTPSecure = 'ssl';
	    $mail->SetFrom('mail@cvvid.com', 'CVVID');
	    $mail->Subject = 'Your account details for CVVid.com';
	    $mail->MsgHTML($body);
	    $mail->AddAddress($model->email, $model->forenames . " " . $model->surname);
	    $mail->Send();
	}
	public function getValidatedSlug($slug)
	{
	    $cri = new CDbCriteria();
	    $cri->condition = "slug ='".$slug."'";
	    $pmodel = Profiles::model()->findAll($cri);
	    
	    $cri1 = new CDbCriteria();
	    $cri1->condition = "slug like '".$slug."-%'";
	    $pmodel1 = Profiles::model()->findAll($cri1);
	    
	    if(count($pmodel1) == 0 && count($pmodel) == 1)
	    {
	        return strtolower($slug)."-1";
	    }
	    else if(count($pmodel1) > 0)
	    {
	        return (strtolower($slug)."-".(count($pmodel1)+1));
	    }
	    else if(count($pmodel) == 0)
	        return strtolower($slug);
	        else
	            return (strtolower($slug)."-".rand());
	            
	}
	
	function actionApplications($id)
	{
	    $this->layout = 'main';
	    $model = Jobs::model()->findByPk($id);
	    $jobappmodel = JobApplications::model()->findAllByAttributes(array('job_id'=>$model->id));
	    $this->render('_jobsapplication',array('model'=>$model,'jobappmodel'=>$jobappmodel));
	}
	
	function actionShortlists($id)
	{
	    $this->layout = 'main';
	    $model = Jobs::model()->findByPk($id);
	    $jobappmodel = JobApplications::model()->findAllByAttributes(array('job_id'=>$model->id));
	    $this->render('_jobshortlisted',array('model'=>$model,'jobappmodel'=>$jobappmodel));
	}
	
	
	public function actioncreateemployer($id)
	{
	    if(Yii::app()->user->getState('role') != 'admin')
	        $this->layout='main';
	        $model = Agency::model()->findByPk($id);
	        $aempmodel = new AgencyEmployers();
	        $empmodel = new Employers();
	        $usermodel = new Users();
	        $addressmodel = new Addresses();
	        $this->render("employerform",array('model'=>$model,'agencyempmodel'=>$aempmodel,'empmodel'=>$empmodel,'usermodel'=>$usermodel,'addressmodel'=>$addressmodel));
	}
	public function actionsaveEmployer($id)
	{
	    if(Yii::app()->user->getState('role') != 'admin')
	        $this->layout='main';
	        $agencymodel = Agency::model()->findByPk($id);
	        $model = new Employers;
	        $empusermodel = new EmployerUsers;
	        $usermodel = new Users;
	        $profilemodel = new Profiles;
	        $addressmodel = new Addresses;
	        $agencyempmodel = new AgencyEmployers();
	        $now = new DateTime();
	        
	        if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
	            $usermodel->setScenario('confirm');
	            
	            if(isset($_POST['Users'])){
	                $usermodel->attributes=$_POST['Users'];
	            }
	            if(isset($_POST['Employers'])){
	                $model->attributes=$_POST['Employers'];
	            }
	            if(isset ($_POST["Addresses"])){
	                $addressmodel->attributes = $_POST["Addresses"];
	            }
	            if(isset ($_POST["Profiles"])){
	                $profilemodel->attributes = $_POST["Profiles"];
	            }
	            if(isset($_POST['Users']))
	            {
	                $valid = $usermodel->validate();
	                $valid = $model->validate() && $valid;
	                $valid = $addressmodel->validate() && $valid;
	                
	                if($valid){
	                    $usermodel->created_at = $now->format('Y-m-d H:i:s');
	                    $usermodel->type = 'employer';
	                    $usermodel->location = $_POST['Addresses']['county'].($_POST['Addresses']['county']!=''? ', '.$_POST['Addresses']['country']: '');
	                    $usermodel->location_lat = $_POST['Addresses']['latitude'];
	                    $usermodel->location_lng = $_POST['Addresses']['longitude'];
	                    $usermodel->tel = $_POST['Employers']['tel'];
	                    $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
	                    $usermodel->api_key = md5(uniqid(rand(), true));
	                    $usermodel->save();
	                    
	                    // insert employer details
	                    $model->email = $usermodel->email;
	                    $model->created_at = $now->format('Y-m-d H:i:s');
	                    $model->user_id = $usermodel->id;
	                    if($model->save())
	                    {
	                        //employer user update
	                        $empusermodel->employer_id = $model->id;
	                        $empusermodel->user_id = $usermodel->id;
	                        $empusermodel->role = 'admin';
	                        $empusermodel->created_at = $now->format('Y-m-d H:i:s');
	                        $empusermodel->save();
	                        
	                        //create Agency employer
	                        
	                        $agencyempmodel->agency_id = $agencymodel->id;
	                        $agencyempmodel->employer_id = $model->id;
	                        $agencyempmodel->add_jobs = $_POST['AgencyEmployers']['add_jobs'];
	                        $agencyempmodel->created_at = $now->format('Y-m-d H:i:s');
	                        $agencyempmodel->save();
	                        
	                        
	                        $agencymodel->employer_vacancy = 0;
	                        $agencymodel->save();
	                    }
	                    $profilemodel->owner_id = $model->id;
	                    $profilemodel->owner_type = 'employer';
	                    if($_POST['industry_id'] > 0)
	                        $profilemodel->industry_id = $_POST['industry_id'];
	                        $profilemodel->type = 'employer';
	                        $profilemodel->slug = $this->getValidatedSlug($model->name);
	                        $profilemodel->save();
	                        //address update
	                        $addressmodel->created_at = $now->format('Y-m-d H:i:s');
	                        $addressmodel->model_type = 'employer';
	                        $addressmodel->model_id = $model->id;
	                        if ($addressmodel->save()) {
	                            
	                            $umodel = Users::model()->findByPk($model->user_id);
	                            
	                            $this->redirect($this->createUrl('agencyStaff/edit', array('id' => $agencymodel->id)));
	                        }
	                        
	                }
	            }
	            $this->render('employerform', array('model' => $agencymodel,'agencyempmodel' => $agencyempmodel,'empmodel' => $model,'profilemodel' => $profilemodel, 'addressmodel' => $addressmodel, 'usermodel' => $usermodel));
	}
	
	public function actioneditemployer($id)
	{
	 
	    if(Yii::app()->user->getState('role') != 'admin')
	        $this->layout='main';
	        $agencyempmodel = AgencyEmployers::model()->findByPk($id);
	        $model = Agency::model()->findByPk($agencyempmodel->agency_id);
	        
	        $empmodel = Employers::model()->findByPk($agencyempmodel->employer_id);
	        $usermodel = Users::model()->findByPk($empmodel->user_id);
	        $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$model->id));
	      //  echo var_dump($expression)
	        $cri = new CDbCriteria();
	        $cri->condition = "model_id =".$empmodel->id." and model_type like '%employer%' and deleted_at is null"; 
	        
	        $addressmodel = Addresses::model()->find($cri);
	       
	        if($addressmodel == null)
	            $addressmodel = new Addresses();
	           
	        $this->render("employerform",array('model'=>$model,'agencyempmodel'=>$agencyempmodel,'empmodel'=>$empmodel,'usermodel'=>$usermodel,'profilemodel' => $profilemodel,'addressmodel'=>$addressmodel));
	}
	
	public function actionupdateEmployer($id)
	{
	    if(Yii::app()->user->getState('role') != 'admin')
	        $this->layout='main';
	        $agencyempmodel = AgencyEmployers::model()->findByPk($id);
	        $empid = $agencyempmodel->employer_id;
	        $agencyid = $agencyempmodel->agency_id;
	        
	       
	        $agencymodel = Agency::model()->findByPk($agencyid);
	        $model = Employers::model()->findByPk($empid);
	        $usermodel = Users::model()->findByPk($model->user_id);
	        $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$model->id));
	        
	        $cri = new CDbCriteria();
	        $cri->condition = "model_id =".$empid." and model_type like '%employer%' and deleted_at is null";
	        $addressmodel = Addresses::model()->find($cri);
	        if($addressmodel == null)
	            $addressmodel = new Addresses();
	        
	           
	        $now = new DateTime();
	        
	        if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
	            $usermodel->setScenario('confirm');
	            
	            if(isset($_POST['Users'])){
	                $usermodel->attributes=$_POST['Users'];
	            }
	            if(isset($_POST['Employers'])){
	                $model->attributes=$_POST['Employers'];
	            }
	            if(isset ($_POST["Addresses"])){
	                $addressmodel->attributes = $_POST["Addresses"];
	            }
	            if(isset ($_POST["Profiles"])){
	                $profilemodel->attributes = $_POST["Profiles"];
	            }
	            if(isset($_POST['Users']))
	            {
	                $valid = $usermodel->validate();
	                $valid = $model->validate() && $valid;
	                $valid = $addressmodel->validate() && $valid;
	                
	                if($valid){
	                    $usermodel->updated_at = $now->format('Y-m-d H:i:s');
	                    $usermodel->location = $_POST['Addresses']['county'].($_POST['Addresses']['county']!=''? ', '.$_POST['Addresses']['country']: '');
	                    $usermodel->location_lat = $_POST['Addresses']['latitude'];
	                    $usermodel->location_lng = $_POST['Addresses']['longitude'];
	                    $usermodel->tel = $_POST['Employers']['tel'];
	                    $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
	                    $usermodel->save();
	                    
	                    $agencyempmodel->add_jobs = $_POST['AgencyEmployers']['add_jobs'];
	                    $agencyempmodel->save();
	                    
	                    // insert employer details
	                    $model->email = $usermodel->email;
	                    $model->updated_at = $now->format('Y-m-d H:i:s');
	                    $model->save();
	                    
	                    if($_POST['industry_id'] > 0)
	                        $profilemodel->industry_id = $_POST['industry_id'];
	                        
	                        $profilemodel->slug = $this->getValidatedSlug($model->name);
	                        $profilemodel->save();
	                        $addressmodel->model_id = $empid;
	                        $addressmodel->model_type = "employer";
	                        //address update
	                        $addressmodel->updated_at = $now->format('Y-m-d H:i:s');
	                        if ($addressmodel->save()) {
	                                    $this->redirect($this->createUrl('agencyStaff/edit', array('id' => $agencymodel->id)));
	                                    
	                        }
	                        
	                }
	            }
	            $this->render("employerform",array('model'=>$agencymodel,'agencyempmodel'=>$agencyempmodel,'empmodel'=>$empmodel,'usermodel'=>$usermodel,'profilemodel' => $profilemodel,'addressmodel'=>$addressmodel));
	}
	
	public function actionemployers($id)
	{
	    $this->layout='main';
	    $agencymodel = Agency::model()->findByPk($id);
	    $model = AgencyEmployers::model();
	    $usermodel = Users::model();
	    $addressmodel = Addresses::model();
	    $profilemodel = Profiles::model();
	    
	    $this->render('employers',array(
	        'model'=>$model,'usermodel' => $usermodel, 'addressmodel' => $addressmodel, 'profilemodel' => $profilemodel,'agencymodel'=>$agencymodel
	    ));
	}
	
	public function actioncreatevacancy($id)
	{
	    if(Yii::app()->user->getState('role') != 'admin')
	        $this->layout='main';
	        $agencymodel = Agency::model()->findByPk($id);
	        $avacmodel = new AgencyVacancies();
	        $usermodel = new Users();
	        $model = new Jobs();
	        $jobskillmodel = JobSkill::model();
	        $this->render("vacancyform",array('model'=>$model,'agencyvacmodel'=>$avacmodel,'usermodel'=>$usermodel,'agencymodel'=>$agencymodel,'jobskillmodel'=>$jobskillmodel));
	}
	public function actionsaveVacancy($id)
	{
	    if(Yii::app()->user->getState('role') != 'admin')
	        $this->layout='main';
	        $model=new Jobs;
	        $agencymodel = Agency::model()->findByPk($id);
	        $avacmodel = new AgencyVacancies();
	        $industrymodel = Industries::model();
	        $skillcategmodel = SkillCategories::model();
	        $skillsmodel = Skills::model();
	        $jobskillmodel = JobSkill::model();
	        
	        $now = new DateTime();
	        
	        $model->attributes = $_POST['Jobs'];
	        $model->owner_id = $id;
	        $model->description = $_POST["description"];
	        if(isset($_POST['competitive_salary']))
	        {
	            $model->competitive_salary = 1;
	            $model->salary_min = 0;
	            $model->salary_max = 0;
	        }
	        if(!isset($_POST['skills']))
	            $model->setScenario("skillsrequired");
	            if ($model->validate()) {
	                //  insert jobs details
	                
	                $model->owner_type = 'agency';
	                $model->user_id = Yii::app()->user->getState('userid');
	                $model->industry_id = $_POST["industry_id"];
	                $d = str_replace('/','-',$_POST["end_date"]);
	                $endformat = date('Y-m-d' , strtotime($d));
	                //$endformat = date_format(new DateTime($_POST["end_date"]),"Y-m-d");
	                $model->end_date = $endformat;
	                // $model->skill_category_id = $_POST["skill_category_id"];
	                $model->type = $_POST["type"];
	                $model->video_only = isset($_POST["video_only"]) ? 1 : 0;
	                $model->additional = $_POST["additional"];
	                $model->salary_type = $_POST["salary_type"];
	                $model->created_at = $now->format('Y-m-d H:i:s');
	                
	                $model->employer_id = $_POST["employer_id"];
	                if($model->save())
	                {
	                    $agencymodel->job_vacancy = 0;
	                    $agencymodel->save();
	                    
	                    if(isset($_POST['skills']))
	                    {
	                        $skills = $_POST['skills'];
	                        foreach ($skills as $skills_val){
	                            $jobskillmodel=new JobSkill;
	                            $jobskillmodel->job_id = $model->id;
	                            $jobskillmodel->skill_id = $skills_val;
	                            if(isset($_POST[$skills_val]))
	                                $jobskillmodel->vacancies = $_POST[$skills_val];
	                                $jobskillmodel->save();
	                        }
	                    }
	                    if(isset($_FILES['jdfile']['name']) && $_FILES['jdfile']['name'] != "")
	                    {
	                        $mediamodel = new Media();
	                        $mediamodel->model_type = "Agency";
	                        $mediamodel->model_id = $id;
	                        $mediamodel->collection_name = "documents";
	                        $mediamodel->name = pathinfo($_FILES['jdfile']['name'], PATHINFO_FILENAME);
	                        $mediamodel->file_name = $_FILES['jdfile']['name'];
	                        $mediamodel->size = $_FILES['jdfile']['size'];
	                        $mediamodel->disk = "documents";
	                        $mediamodel->created_at = $now->format('Y-m-d H:i:s');
	                        if($mediamodel->save())
	                        {
	                            $mediafolder = "images/media/" . $mediamodel->id . "/conversions";
	                            
	                            if (!file_exists($mediafolder)) {
	                                mkdir($mediafolder, 0777, true);
	                            }
	                            $filename = $_FILES['jdfile']['name'];
	                            $fullpath = "images/media/" . $mediamodel->id . "/" . $filename;
	                            if (move_uploaded_file($_FILES['jdfile']['tmp_name'], $fullpath)) {
	                                
	                            }
	                            Jobs::model()->updateByPk($model->id,array('media_id'=>$mediamodel->id));
	                        }
	                    }
	                    
	                    $avacmodel->agency_id = $agencymodel->id;
	                    $avacmodel->job_id = $model->id;
	                    $avacmodel->created_at = $now->format('Y-m-d H:i:s');
	                    $avacmodel->save();
	                    
	                    // 	                if($agencymodel->email != "")
	                        // 	                    $this->notifyjob($model, $agencymodel->email, $agencymodel->name);
	                    
	                    Yii::app()->user->setFlash('success', 'Successfully created job');
	                    
	                    if(Yii::app()->user->getState('role') == 'admin')
	                        $this->redirect(Yii::app()->createUrl('agency/tabvacancies', array('id' => $agencymodel->id)));
                        else
                            $this->redirect($this->createUrl('agencyStaff/edit', array('id' => $agencymodel->id)));
	                }
	            }
	            $usermodel = new Users();
	            $this->render("vacancyform",array('model'=>$model,'agencyvacmodel'=>$avacmodel,'usermodel'=>$usermodel,'agencymodel'=>$agencymodel,'jobskillmodel'=>$jobskillmodel));
	}
	
	public function actioneditVacancy($id)
	{
	    if(Yii::app()->user->getState('role') != 'admin')
	        $this->layout='main';
	        $avacmodel = AgencyVacancies::model()->findByPk($id);
	        $agencymodel = Agency::model()->findByPk($avacmodel->agency_id);
	        $model = Jobs::model()->findByPk($avacmodel->job_id);
	        $jobskillmodel = JobSkill::model()->findAllByAttributes(array('job_id'=>$model->id));
	        $this->render("vacancyform",array('model'=>$model,'agencyvacmodel'=>$avacmodel,'agencymodel'=>$agencymodel,'jobskillmodel'=>$jobskillmodel));
	}
	
	public function actionupdateVacancy($id)
	{
	    if(Yii::app()->user->getState('role') != 'admin')
	        $this->layout='main';
	        $now = new DateTime();
	        $avacmodel = AgencyVacancies::model()->findByPk($id);
	        $agencymodel = Agency::model()->findByPk($avacmodel->agency_id);
	        $model = Jobs::model()->findByPk($avacmodel->job_id);
	        $jobskillmodel = JobSkill::model()->findAllByAttributes(array('job_id'=>$model->id));
	        if(isset($_POST['Jobs']))
	        {
	            $model->attributes = $_POST["Jobs"];
	            if(!isset($_POST['skills']))
	                $model->setScenario("skillsrequired");
	                if ($model->validate()) {
	                    //  insert jobs details
	                    
	                    $model->industry_id = $_POST["industry_id"];
	                    $d = str_replace('/','-',$_POST["end_date"]);
	                    $endformat = date('Y-m-d' , strtotime($d));
	                    // $endformat = date_format(new DateTime($_POST["end_date"]),"Y-m-d");
	                    $model->end_date = $endformat;
	                    //$model->skill_category_id = $_POST["skill_category_id"];
	                    $model->type = $_POST["type"];
	                    $model->description = $_POST["description"];
	                    $model->video_only = isset($_POST["video_only"]) ? 1 : 0;
	                    $model->additional = $_POST["additional"];
	                    $model->location_lat = $_POST["location_lat"];
	                    $model->location_lng = $_POST["location_lng"];
	                    $model->updated_at = $now->format('Y-m-d H:i:s');
	                    
	                    $model->employer_id = $_POST["employer_id"];
	                    if($model->save())
	                    {
	                        $skills = $_POST['skills'];
	                        
	                        $jkillmodel= JobSkill::model()->deleteAllByAttributes(array('job_id'=>$model->id));
	                        
	                        foreach ($skills as $skills_val){
	                            $jobskillmodel=new JobSkill;
	                            $jobskillmodel->job_id = $model->id;
	                            $jobskillmodel->skill_id = $skills_val;
	                            $jobskillmodel->save();
	                        }
	                        
	                        if(isset($_FILES['jdfile']['name']) && $_FILES['jdfile']['name'] != "")
	                        {
	                            if($model->media_id > 0)
	                            {
	                                $mediamodel = Media::model()->findByPk($model->media_id);
	                                $mediamodel->updated_at = $now->format('Y-m-d H:i:s');
	                            }
	                            else
	                            {
	                                $mediamodel = new Media();
	                                $mediamodel->created_at = $now->format('Y-m-d H:i:s');
	                            }
	                            $mediamodel->model_type = "Agency";
	                            $mediamodel->model_id = $model->owner_id;
	                            $mediamodel->collection_name = "documents";
	                            $mediamodel->name = pathinfo($_FILES['jdfile']['name'], PATHINFO_FILENAME);
	                            $mediamodel->file_name = $_FILES['jdfile']['name'];
	                            $mediamodel->size = $_FILES['jdfile']['size'];
	                            $mediamodel->disk = "documents";
	                            
	                            if($mediamodel->save())
	                            {
	                                $mediafolder = "images/media/" . $mediamodel->id . "/conversions";
	                                
	                                if (!file_exists($mediafolder)) {
	                                    mkdir($mediafolder, 0777, true);
	                                }
	                                $filename = $_FILES['jdfile']['name'];
	                                $fullpath = "images/media/" . $mediamodel->id . "/" . $filename;
	                                if (move_uploaded_file($_FILES['jdfile']['tmp_name'], $fullpath)) {
	                                    
	                                }
	                                Jobs::model()->updateByPk($model->id,array('media_id'=>$mediamodel->id));
	                            }
	                        }
	                        
	                        Yii::app()->user->setFlash('success', 'Successfully updated job');
	                        if(Yii::app()->user->getState('role') == 'admin')
	                            $this->redirect(Yii::app()->createUrl('agency/tabvacancies', array('id' => $agencymodel->id)));
	                            else
	                                $this->redirect($this->createUrl('agencyStaff/edit', array('id' => $agencymodel->id)));
	                                
	                    }
	                }
	                
	                $usermodel = new Users();
	                $this->render("vacancyform",array('model'=>$model,'agencyvacmodel'=>$avacmodel,'usermodel'=>$usermodel,'agencymodel'=>$agencymodel,'jobskillmodel'=>$jobskillmodel));
	        }
	        
	}
	public function actiondeleteVacancies()
	{
	    $idstring = implode(',', $_POST['ids']);
	    $ids = $_POST['ids'];
	    $now = new DateTime();
	    
	    for($i = 0; $i < count($ids); $i++)
	    {
	        $avacmodel = AgencyVacancies::model()->findByPk($ids[$i]);
	        $umodel = Users::model()->findByPk($avacmodel->agency_id);
	        
	        $cri1 = new CDbCriteria();
	        $cri1->condition = "job_id =".$avacmodel->job_id;
	        JobSkill::model()->deleteAll($cri1);
	        
	        Jobs::model()->deleteByPk($avacmodel->job_id);
	        
	        AgencyVacancies::model()->deleteByPk($ids[$i]);
	    }
	}
}
