<?php

class CareersController extends Controller {

    /**
     * Declares class-based actions.
     */
   public $layout = false;
   
    //  public $is_premiun = 0;

    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
       Yii::app()->theme='coming';
        $this->render('index');
         //$this->redirect($this->createUrl('site/registerinstitution'));
    }
   public function actionCareers() {
        $this->render('index');
    }
    
}
