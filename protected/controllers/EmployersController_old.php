<?php
use Stripe\Charge;
use Stripe\Invoice;
use Stripe\InvoiceItem;
use Stripe\Stripe;
use Stripe\Token;
use Stripe\Customer;
use Stripe\Plan;
use Stripe\Subscription;

class EmployersController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/admin';
    public $employerid = 0;
    public $edit = 0;

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
//			array('deny',  // deny all users
//				'users'=>array('*'),
//			),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionupgrade() {
        $this->layout = 'main';
        $this->render('upgrade');
    }
    
     /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionmysubscription() {
        $this->layout = 'main';
        $this->render('subscription');
    }

    public function actiondeleteEmployer($id) {

        $now = new DateTime();

        $empmodel = Employers::model()->findByPk($id);

        $empmodel->deleted_at = $now->format('Y-m-d H:i:s');
        $empmodel->save();

        $usermodel = Users::model()->findByPk($empmodel->user_id);
        $usermodel->deleted_at = $now->format('Y-m-d H:i:s');
        $usermodel->save();

        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $id, 'type' => 'employer'));
        $profilemodel->deleted_at = $now->format('Y-m-d H:i:s');
        $profilemodel->save();

        $addressmodel = Addresses::model()->findByAttributes(array('model_id' => $id, 'model_type' => 'employer'));
        $addressmodel->deleted_at = $now->format('Y-m-d H:i:s');
        $addressmodel->save();

        $this->redirect($this->createUrl('employers/admin'));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Employers;
        $usermodel = Users::model();
        $addressmodel = Addresses::model();
        $profilemodel = Profiles::model();
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Employers'])) {
            $now = new DateTime();
            $model->attributes = $_POST['Employers'];
            $model->save();
        }

        $this->render('create', array('model' => $model, 'usermodel' => $usermodel, 'addressmodel' => $addressmodel, 'profilemodel' => $profilemodel));
    }

    /**
     * Manages all models.
     */
    public function actionsearchAdmin() {
        $model = new Employers('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Employers'])) {
            $model->attributes = $_GET['Employers'];
        }

        $this->render('admin', array(
            'model' => $model, 'issearch' => 1));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actioncreateEmployerDetails() {

        $model = new Employers;
        $empusermodel = new EmployerUsers;
        $usermodel = new Users;
        $profilemodel = new Profiles;
        $addressmodel = new Addresses;
        $now = new DateTime();
        
        if(isset($_POST['Users'])){
           $usermodel->attributes=$_POST['Users'];
        }
        if(isset($_POST['Employers'])){
           $model->attributes=$_POST['Employers'];
        }
        if(isset ($_POST["Addresses"])){
            $addressmodel->attributes = $_POST["Addresses"]; 
        }
        if(isset ($_POST["Profiles"])){
            $profilemodel->attributes = $_POST["Profiles"];
        } 
        
        if(isset($_POST['Employers']))
        {
//             if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
//                 $usermodel->setScenario('confirm');
            
             $valid = $usermodel->validate();
             $valid = $model->validate() && $valid;
             $valid = $addressmodel->validate() && $valid;
             $valid = $profilemodel->validate() && $valid;
             
             if($valid){
                $usermodel->created_at = $now->format('Y-m-d H:i:s');
                $usermodel->type = 'employer';
                $usermodel->location = $_POST['Addresses']['county'].($_POST['Addresses']['county']!=''? ', '.$_POST['Addresses']['country']: '');
                $usermodel->location_lat = $_POST['Addresses']['latitude'];
                $usermodel->location_lng = $_POST['Addresses']['longitude'];
                $usermodel->tel = $_POST['Employers']['tel'];
                $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
                $usermodel->save();
//                insert employer details
                $model->email = $usermodel->email;
                $model->created_at = $now->format('Y-m-d H:i:s');
                $model->user_id = $usermodel->id;
                $model->body = $_POST['body'];
                if($model->save())
                {
                    //employer user update
                    $empusermodel->employer_id = $model->id;
                    $empusermodel->user_id = $usermodel->id;
                    $empusermodel->role = Yii::app()->user->getState('role');
                    $empusermodel->created_at = $now->format('Y-m-d H:i:s');
                    $empusermodel->save();
                }
//                profile model
                $profilemodel->owner_id = $model->id;
                $profilemodel->owner_type = 'employer';
                $profilemodel->type = 'employer';
                $profilemodel->slug = $_POST["Profiles"]['slug'];
                if ($_POST['photo_id'] > 0)
                    $profilemodel->photo_id = $_POST['photo_id'];
                $profilemodel->save();
                
                //address update
                $addressmodel->attributes = $_POST["Addresses"];
                $addressmodel->created_at = $now->format('Y-m-d H:i:s');
                $addressmodel->model_type = 'employer';
                $addressmodel->model_id = $usermodel->id;
                if ($addressmodel->save()) {
                    Yii::app()->user->setFlash('success', 'Successfully created employer');
                    $this->redirect($this->createUrl('employers/tabedit', array('id' => $model->id)));
                }
             }
        }
        
        $this->render('create', array('model' => $model, 'usermodel' => $usermodel, 'addressmodel' => $addressmodel, 'profilemodel' => $profilemodel));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actioncreateEmployerUsers() {
           
           $this->layout='main';
           $model = Employers::model()->findByPk($_POST['employer_id']);
        
            $empusermodel = new EmployerUsers;
            $usermodel = new Users;
            $now = new DateTime();

            if(isset($_POST['Users'])){
               $usermodel->attributes=$_POST['Users'];
            }   
           if(isset($_POST['Users']))
           {
                $valid = $usermodel->validate();
                if($valid){
                     // insert user details
                    $usermodel->created_at = $now->format('Y-m-d H:i:s');
                    $usermodel->type = 'employer';
                    $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
                    $usermodel->location_lat = $_POST['Addresses']['latitude'];
                    $usermodel->location_lng = $_POST['Addresses']['longitude'];

                    if ($usermodel->save()) {
                        //employer user update
                        $empusermodel->employer_id = $_POST['employer_id'];
                        $empusermodel->user_id = $usermodel->id;
                        $empusermodel->role = Yii::app()->user->getState('role');
                        $empusermodel->created_at = $now->format('Y-m-d H:i:s');
                        $empusermodel->save();
                        
                        // Send the notification email
                        if ($_POST['inform_user']) {
                            $this->useraccountnotify($usermodel, $_POST['Users']['password']);
                        }
                        
                        Yii::app()->user->setFlash('success', 'Successfully added new user');
                        $this->redirect($this->createUrl('employers/users',array('id'=>$model->id)));
                        
                    }
                }
           }
           
           $this->render('usercreate', array('model' => $model,'usermodel'=>$usermodel));
       
    }
    
     /**
     * Notify the user
     */
    public function useraccountnotify($model, $password)
    {
        $body = $this->renderPartial("//emails/user-account",array('user' => $model, 'password' => $password),true);
        // Send register notifcation to employer admin
        Yii::import('application.extensions.phpmailer.JPhpMailer');
        $mail = new JPhpMailer;
        $mail->Host = 'smtp.gmail.com';
        $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
        $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
        $mail->SMTPSecure = 'ssl';
        $mail->SetFrom('info@cvvid.com', 'CVVID');
        $mail->Subject = 'Your account details for CVVid.com';
        $mail->MsgHTML($body);
        $mail->AddAddress($model->email, $model->forenames." ".$model->surname);
        $mail->Send();
        
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actioncreateUsers($id) {

        $model = Employers::model()->findByPk($id);
        $empusermodel = new EmployerUsers;
        $usermodel = new Users;
        $addressmodel = new Addresses;
        $now = new DateTime();
        
        if(isset($_POST['Users'])){
           $usermodel->attributes=$_POST['Users'];
        }
        if(isset($_POST['Employers'])){
           $model->attributes=$_POST['Employers'];
        }
        if(isset ($_POST["Addresses"])){
            $addressmodel->attributes = $_POST["Addresses"]; 
        }
        
        if(isset($_POST['Users'])){
            
            $d = str_replace('/', '-', $_POST["dob"]);
            $dobformat = date('Y-m-d', strtotime($d));
            $usermodel->dob = $dobformat;
            $usermodel->created_at = $now->format('Y-m-d H:i:s');
            $usermodel->type = 'employer';
            $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
            $usermodel->location = $_POST['Addresses']['county'] . ($_POST['Addresses']['county'] != '' ? ', ' . $_POST['Addresses']['country'] : '');
            $usermodel->location_lat = $_POST['Addresses']['latitude'];
            $usermodel->location_lng = $_POST['Addresses']['longitude'];
            if($usermodel->save())
            {
                //employer user update
                $empusermodel->employer_id = $id;
                $empusermodel->user_id = $usermodel->id;
                $empusermodel->role = Yii::app()->user->getState('role');
                $empusermodel->created_at = $now->format('Y-m-d H:i:s');
                $empusermodel->save();
            }
            //address update
            $addressmodel->created_at = $now->format('Y-m-d H:i:s');
            $addressmodel->model_type = 'employer';
            $addressmodel->model_id = $usermodel->id;
            if($addressmodel->save()){
                  Yii::app()->user->setFlash('success', 'Successfully created user');
                 $this->redirect($this->createUrl('employers/tabusers', array('id' =>$id)));
            }
            
        }
         $this->render('_employerusercreate', array(
            'model' => $model,'usermodel'=>$usermodel,'addressmodel'=>$addressmodel
        ));

    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionemployerUsersEdit($id) {

        $empusermodel = EmployerUsers::model()->findByPk($id);
        $usermodel = Users::model()->findByPk($empusermodel->user_id);
        $model = Employers::model()->findByPk($empusermodel->employer_id);
        $addressmodel = Addresses::model()->findByAttributes(array('model_id'=>$usermodel->id,'model_type'=>'employer'));
        $pwd = $usermodel->password;
        $now = new DateTime();
        
        if (isset($_POST['Users'])) {
            $usermodel->attributes = $_POST['Users'];
        }
        if (isset($_POST["Addresses"])) {
            $addressmodel->attributes = $_POST["Addresses"];
        }
        
        if (isset($_POST['Users'])) {

//             if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
//                 $usermodel->setScenario('confirm');
            
            $valid = $usermodel->validate();
            $valid = $addressmodel->validate() && $valid;
    
            if ($valid) {
                 //      insert user details
                $d = str_replace('/', '-', $_POST["dob"]);
                $dobformat = date('Y-m-d', strtotime($d));
                $usermodel->dob = $dobformat;
               
                if ($_POST["Users"]["password"] == "")
                    $usermodel->password = $pwd;
                else 
                    $usermodel->password =  password_hash($_POST['Users']['password'], 1,['cost' => 10]);
                $usermodel->updated_at = $now->format('Y-m-d H:i:s');
                if ($usermodel->save()) {
//                    employer user
                    $empusermodel->updated_at = $now->format('Y-m-d H:i:s');
                    $empusermodel->save();
                }
                //address update
                $addressmodel->updated_at = $now->format('Y-m-d H:i:s');
                if($addressmodel->save()){
                      Yii::app()->user->setFlash('success', 'Successfully updated user');
                      $this->redirect($this->createUrl('employers/tabusers', array('id' =>$model->id)));
                }
                
            }
        }
        $this->render('_employeruseredit', array('model' => $model,'eumodel'=>$empusermodel,'usermodel'=>$usermodel,'addressmodel'=>$addressmodel));

    }

    public function actionsaveBody() {
        $employerid = $_POST['employerid'];
        $body = $_POST['body'];

        $model = Employers::model()->findByPk($employerid);

        $model->body = $body;
        $model->save();
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actioneditUsers($id) {

        $this->layout='main';
        $usermodel = Users::model()->findByPk($id);
        $empusrmodel = EmployerUsers::model()->findByAttributes(array('user_id'=>$usermodel->id));
        $empmodel = Employers::model()->findByPk($empusrmodel->employer_id);
        
        $pass = $usermodel->password;

        $now = new DateTime();
        
        if(isset($_POST['Users'])) {
            $usermodel->attributes = $_POST['Users'];
        }
        if (isset($_POST['Users'])) {
            $valid = $usermodel->validate();
            if ($valid) {
                 if($_POST["Users"]['password']!="")
                     $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
                 else
                     $usermodel->password = $pass;
                 $usermodel->created_at = $now->format('Y-m-d H:i:s');
                 $usermodel->type = 'employer';
                 if ($usermodel->save()) {
                    //employer user update
                    $empmodel->email = $_POST["Users"]['email'];
                    $empmodel->save();
                    
                    // Send the notification email
                    if ($_POST['inform_user']) {
                        $this->useraccountnotify($usermodel, $_POST['Users']['password']);
                    }

                    Yii::app()->user->setFlash('success', 'Successfully updated profile');
                    $this->redirect(Yii::app()->createUrl('employers/users', array('id' => $empmodel->id)));

                }
            }
        }
        $this->render('useredit', array('usermodel'=>$usermodel,'empmodel'=>$empmodel));

        
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Employers'])) {
            $model->attributes = $_POST['Employers'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function actiondeleteEmployers() {
        $ids = implode(',', $_POST['ids']);

        $userids = explode(",", $ids);

        $usr_arr = array();
        foreach ($userids as $id) {
            $uid = Employers::model()->findByPk($id);
            $usr_arr[] = $uid->user_id;
        }

        $user_ids = implode(',', $usr_arr);

        $now = new DateTime();

        $cri = new CDbCriteria();
        $cri->condition = "id in (" . $ids . ")";
        Employers::model()->updateAll(array('deleted_at' => $now->format('Y-m-d H:i:s')), $cri);

        $cri1 = new CDbCriteria();
        $cri1->condition = "id in (" . $user_ids . ")";
        Users::model()->updateAll(array('deleted_at' => $now->format('Y-m-d H:i:s')), $cri1);

        $cri2 = new CDbCriteria();
        $cri2->condition = "owner_id in (" . $ids . ")";
        Profiles::model()->updateAll(array('deleted_at' => $now->format('Y-m-d H:i:s')), $cri2);

        $cri3 = new CDbCriteria();
        $cri3->condition = "model_id in (" . $ids . ")";
        Addresses::model()->updateAll(array('deleted_at' => $now->format('Y-m-d H:i:s')), $cri3);
    }

    public function actionupdatePublish() {
        $employerid = $_POST['employerid'];
        $status = $_POST['status'];

        $publish = 1;
        if ($status == 1) {
            $publish = 0;
        }

        $model = Employers::model()->findByPk($employerid);

        $model->published = $publish;
        $model->save();
    }

    public function actionchangeEmployerVisiblity() {
        $employerid = $_POST['employerid'];
        $status = $_POST['status'];

        $visiblity = 1;
        if ($status == 1) {
            $visiblity = 0;
        }

        $promodel = Profiles::model()->findByAttributes(array('owner_id' => $employerid, 'type' => 'employer'));

        $promodel->visibility = $visiblity;
        $promodel->save();
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Employers');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Employers('search');
        $empmodel = new Employers();
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Employers']))
            $model->attributes = $_GET['Employers'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

//        /**
//	 * Manages all models.
//	 */
//	public function actionEdit()
//	{
//		$model=new Employers('search');
//		$empmodel=new Employers();
//		$model->unsetAttributes();  // clear any default values
//		if(isset($_GET['Employers']))
//			$model->attributes=$_GET['Employers'];
//
//		$this->render('admin',array(
//			'model'=>$model,
//		));
//	}



    public function actionEmployer($id) {
        $this->layout = "main";
        $this->employerid = $id;
        $model = Employers::model()->findByPk($id);
        $this->render('employer', array(
            'model' => $model,
        ));
    }

    public function actionemployeruserCreate($id) {
        $model = Employers::model()->findByPk($id);
        $usermodel = new Users();
        $addressmodel = new Addresses();
        $this->render('_employerusercreate', array(
            'model' => $model,'usermodel'=>$usermodel,'addressmodel'=>$addressmodel
        ));
    }

    public function actionEdit($id) {
        $this->layout = "main";
        $this->employerid = $id;
        $this->edit = 1;
        $model = Employers::model()->findByPk($id);
        $this->render('editemployer', array(
            'model' => $model,
        ));
    }
    
    public function actionjobsadd($id) {
        $this->layout = "main";
        
        $model=new Jobs;
        $empmodel = Employers::model()->findByPk($id);
        $industrymodel = Industries::model();
        $skillcategmodel = SkillCategories::model();
        $skillsmodel = Skills::model();
        $jobskillmodel = JobSkill::model();
        
        $this->render('addjobs', array(
            'model' => $model,'empmodel' => $empmodel,'industrymodel'=>$industrymodel,'skillcategmodel'=>$skillcategmodel,'skillsmodel'=>$skillsmodel,
            'jobskillmodel'=>$jobskillmodel
        ));
    }
    
     public function actionjobsedit($id) {
        $this->layout = "main";
        $model = Jobs::model()->findByPk($id);
        $employermodel = Employers::model()->findByPk($model->owner_id);
        $jobskillmodel = JobSkill::model()->findAllByAttributes(array('job_id'=>$model->id));
        $this->render('editjobs',array('model'=>$model,'employermodel'=>$employermodel,'jobskillmodel'=>$jobskillmodel));
    }
    
     /**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionupdateJobs($id)
	{
            $this->layout='main';
            $now = new DateTime();
            
            $model = Jobs::model()->findByPk($id);
            $empmodel = Employers::model()->findByPk($model->owner_id);
            $usermodel = Users::model()->findByPk($empmodel->user_id);
            
            if(isset($_POST['Jobs']))
            {
                //  insert jobs details
                    $model->attributes = $_POST["Jobs"];
                    $model->industry_id = $_POST["industry_id"];
                    $d = str_replace('/','-',$_POST["end_date"]);
                    $endformat = date('Y-m-d' , strtotime($d));
                   // $endformat = date_format(new DateTime($_POST["end_date"]),"Y-m-d");
	            $model->end_date = $endformat;
	            $model->skill_category_id = $_POST["skill_category_id"];
	            $model->type = $_POST["type"];
	            $model->description = $_POST["description"];
	            $model->additional = $_POST["additional"];
	            $model->location_lat = $_POST["location_lat"];
	            $model->location_lng = $_POST["location_lng"];
                    $model->updated_at = $now->format('Y-m-d H:i:s');
                    if($model->save())
                    {
                        $skills = $_POST['skills'];
                        
                        $jkillmodel= JobSkill::model()->deleteAllByAttributes(array('job_id'=>$model->id));
                        
                        foreach ($skills as $skills_val){
                            $jobskillmodel=new JobSkill;
                            $jobskillmodel->job_id = $model->id;
                            $jobskillmodel->skill_id = $skills_val;
                            $jobskillmodel->save();
                        }
                        Yii::app()->user->setFlash('success', 'Successfully updated job');
                        $this->redirect(Yii::app()->createUrl('employers/edit', array('id' => $empmodel->id)));
                       
                    }
            }
                
        }
    
    /**
        * Creates a new model.
        * If creation is successful, the browser will be redirected to the 'view' page.
        */
       public function actioncreateJobs($id)
       {
           $this->layout='main';
           $model=new Jobs;
           $employermodel = Employers::model()->findByPk($id);
           $industrymodel = Industries::model();
           $skillcategmodel = SkillCategories::model();
           $skillsmodel = Skills::model();
           $jobskillmodel = JobSkill::model();

           $now = new DateTime();
           
           
            $model->attributes = $_POST['Jobs'];
            $model->owner_id = $id;
            $model->description = $_POST["description"];

            if ($model->validate()) {
                   //  insert jobs details
                  
                   $model->owner_type = 'employer';
                   $model->user_id = Yii::app()->user->getState('userid');
                   $model->industry_id = $_POST["industry_id"];
                   $d = str_replace('/','-',$_POST["end_date"]);
                   $endformat = date('Y-m-d' , strtotime($d));
                   //$endformat = date_format(new DateTime($_POST["end_date"]),"Y-m-d");
                   $model->end_date = $endformat;
                   $model->skill_category_id = $_POST["skill_category_id"];
                   $model->type = $_POST["type"];
                   $model->additional = $_POST["additional"];
                   $model->location_lat = $_POST["location_lat"];
                   $model->location_lng = $_POST["location_lng"];
                   $model->created_at = $now->format('Y-m-d H:i:s');
                   if($model->save())
                   {
                       if(isset($_POST['skills']))
                       {
                           $skills = $_POST['skills'];
                           foreach ($skills as $skills_val){
                               $jobskillmodel=new JobSkill;
                               $jobskillmodel->job_id = $model->id;
                               $jobskillmodel->skill_id = $skills_val;
                               $jobskillmodel->save();
                           }
                       }
                      $this->notifyjob($model, $employermodel->email, $employermodel->name);
                                              
                      Yii::app()->user->setFlash('success', 'Successfully created job');
                      $this->redirect(Yii::app()->createUrl('employers/edit', array('id' => $employermodel->id)));
                   }
            }
             $this->render('addjobs',array(
                   'model'=>$model,'empmodel'=>$employermodel,'industrymodel'=>$industrymodel,'skillcategorymodel'=>$skillcategmodel,'skillmodel'=>$skillsmodel,'jobskillmodel'=>$jobskillmodel
           ));

       }
       
       /**
	 * Notify the user
	 */
	public function notifyjob($model, $email, $name)
	{
	    $body = $this->renderPartial("//emails/jobs-created",array('model' => $model, 'email' => $email,'name'=>$name),true);
	    // Send register notifcation to employer admin
	    Yii::import('application.extensions.phpmailer.JPhpMailer');
	    $mail = new JPhpMailer;
	    $mail->Host = 'smtp.gmail.com';
	    $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
	    $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
	    $mail->SMTPSecure = 'ssl';
	    $mail->SetFrom('info@cvvid.com', 'CVVID');
	    $mail->Subject = 'Thank you for uploading your vacancy';
	    $mail->MsgHTML($body);
	    $mail->AddAddress($email, $name);
	    $mail->Send();
	    
	}


    public function actiontabedit($id) {
        $model = Employers::model()->findByPk($id);
        if($model->user_id > 0)
        {
            $usermodel = Users::model()->findByPk($model->user_id);
            $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $model->id, 'type' => $usermodel->type));
            $addressmodel = Addresses::model()->findByAttributes(array('model_id' => $usermodel->id,'model_type'=>$usermodel->type));
            if($addressmodel == null)
                $addressmodel = new Addresses();
         }
        else 
        {
            $usermodel = new Users();
            $profilemodel = new Profiles();
            $addressmodel = new Addresses();
            
        }       
            
        $this->render('tabedit', array(
            'model' => $model,'profilemodel'=>$profilemodel,'addressmodel'=>$addressmodel,'usermodel'=>$usermodel
        ));
    }

    public function actiontabusers($id) {
        $model = Employers::model()->findByPk($id);
        $this->render('tabusers', array(
            'model' => $model,
        ));
    }

    public function actiontabjobs($id) {
        $model = Employers::model()->findByPk($id);
        $this->render('tabjobs', array(
            'model' => $model,
        ));
    }

    public function actioneditDetails($id) {
        $this->layout = "main";
        $model = Employers::model()->findByPk($id);
        $usermodel = Users::model()->findByPk($model->user_id);
        $this->render('editDetails', array(
            'model' => $model, 'usermodel' => $usermodel
        ));
    }

//         /**
//	 * Manages Employers Edit.
//	 */
//        function actionemployerEdit($id)
//	{
//	    $model = Employers::model()->findByPk($id);
//	    $this->render('employerEdit',array('model'=>$model));
//	}
    /**
     * Manages Employers Edit.
     */
    function actionUsersEdit($id) {
        $eumodel = EmployerUsers::model()->findByPk($id);
        $model = Employers::model()->findByPk($eumodel->employer_id);
        $usermodel = Users::model()->findByPk($eumodel->user_id);
        $addressmodel = Addresses::model()->findByAttributes(array('model_id'=>$usermodel->id,'model_type'=>'employer'));
        $this->render('_employeruseredit', array('model' => $model,'eumodel'=>$eumodel,'usermodel'=>$usermodel,'addressmodel'=>$addressmodel));
    }

    function actionUsers($id) {
        $this->layout = "main";
        $model = Employers::model()->findByPk($id);
        $this->render('users', array('model' => $model));
    }

    function actionuserCreate($id) {
        $this->layout = "main";
        $model = Employers::model()->findByPk($id);
        $usermodel = new Users;
        $this->render('usercreate', array('model' => $model,'usermodel'=>$usermodel));
    }

    function actionuserEdit($id) {
        $this->layout='main';
        $usermodel = Users::model()->findByPk($id);
        $empusrmodel = EmployerUsers::model()->findByAttributes(array('user_id'=>$usermodel->id));
        $empmodel = Employers::model()->findByPk($empusrmodel->employer_id);
        $this->render('useredit', array('usermodel'=>$usermodel,'empmodel'=>$empmodel));
    }

    public function actionusersActivate() {
        $ids = implode(',', $_POST['ids']);

        $cri = new CDbCriteria();
        $cri->condition = "id in (" . $ids . ")";
        Users::model()->updateAll(array('status' => 'active'), $cri);
        
         Yii::app()->user->setFlash('success', 'Successfully Activated');
    }

    public function actionusersBlock() {
        $ids = implode(',', $_POST['ids']);

        $cri = new CDbCriteria();
        $cri->condition = "id in (" . $ids . ")";
        Users::model()->updateAll(array('status' => 'blocked'), $cri);
        
        Yii::app()->user->setFlash('success', 'Successfully Blocked');
        
    }

    function actiongetProfilePhoto() {
        echo $_FILES['profile']['tmp_name'];
    }

    function actionupdateProfileDetails($id) {

        $now = new DateTime();
        $empmodel = Employers::model()->findByPk($id);
        $usermodel = Users::model()->findByPk($empmodel->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $empmodel->id, 'type' => 'employer'));
        
        if(isset($_POST['Employers'])){
          $empmodel->attributes=$_POST['Employers'];
        }else if(isset($_POST['Profiles'])){
          $profilemodel->attributes=$_POST['Profiles'];  
        }
        if(isset($_POST['Employers']))
        {
            $valid = $empmodel->validate();
            $valid = $profilemodel->validate(false) && $valid;
            
            if($valid){
                
                $empmodel->updated_at = $now->format('Y-m-d H:i:s');
                $empmodel->save();
                 
                $profilemodel->slug = $_POST["Profiles"]['slug'];
                $profilemodel->updated_at = $now->format('Y-m-d H:i:s');
                $profilemodel->save();
                
                 Yii::app()->user->setFlash('success', 'Successfully updated profile');
                 $this->redirect(Yii::app()->createUrl('employers/edit', array('id' => $empmodel->id)));

            }
        }
        
        $this->render('editDetails', array(
            'model' => $empmodel, 'usermodel' => $usermodel
        ));
        
    }

    function actionupdateEmployerDetails($id) {
        $now = new DateTime();
        
        $model = Employers::model()->findByPk($id);
        $usermodel = Users::model()->findByPk($model->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $model->id, 'type' => $usermodel->type));
        $addressmodel = Addresses::model()->findByAttributes(array('model_id'=>$usermodel->id,'model_type' => $usermodel->type));
        
        if(isset($_POST['Users'])){
           $usermodel->attributes=$_POST['Users'];
        }
        if(isset($_POST['Employers'])){
           $model->attributes=$_POST['Employers'];
        }
        if(isset ($_POST["Addresses"])){
            $addressmodel->attributes = $_POST["Addresses"]; 
        }
        if(isset ($_POST["Profiles"])){
            $profilemodel->attributes = $_POST["Profiles"];
        }
        
         if(isset($_POST['Employers']))
        {
             $valid = $usermodel->validate();
             $valid = $model->validate() && $valid;
             $valid = $addressmodel->validate() && $valid;
             $valid = $profilemodel->validate() && $valid;
             
             if($valid){
                $model->updated_at = $now->format('Y-m-d H:i:s');
                $model->body = $_POST['body'];
                $model->save();
//                profile update
                if ($_POST['photo_id'] > 0)
                $profilemodel->photo_id = $_POST['photo_id'];
                $profilemodel->updated_at = $now->format('Y-m-d H:i:s');
                $profilemodel->save();
//                address model
                $addressmodel->updated_at = $now->format('Y-m-d H:i:s');
                if ($addressmodel->save()) {
                     Yii::app()->user->setFlash('success', 'Successfully updated employer');
                     $this->redirect($this->createUrl('employers/tabedit', array('id' => $model->id)));
                }
                
             }
        }
        
        $this->render('tabedit', array(
            'model' => $model,'profilemodel'=>$profilemodel,'addressmodel'=>$addressmodel,'usermodel'=>$usermodel
        ));
    }

    function resize_image($image, $image_type, $fullpath, $destpath, $width, $height) {
        $size_arr = getimagesize($fullpath);

        list($width_orig, $height_orig, $img_type) = $size_arr;
        $ratio_orig = $width_orig / $height_orig;

        $tempimg = imagecreatetruecolor($width, $height);
        imagecopyresampled($tempimg, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

        if ($image_type == "image/jpg" || $image_type == "image/jpeg")
            imagejpeg($tempimg, $destpath);
        else if ($image_type == "image/png")
            imagepng($tempimg, $destpath);
        else if ($image_type == "image/gif")
            imagegif($tempimg, $destpath);
    }

    /*     * updateEmployerDetails
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Employers the loaded model
     * @throws CHttpException
     */

    public function loadModel($id) {
        $model = Employers::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Employers $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'employers-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function beforeAction($action) {
        Yii::app()->user->setReturnUrl(Yii::app()->request->getUrl());
        $requestUrl = Yii::app()->request->url;
        if (Yii::app()->user->isGuest && !strpos(Yii::app()->request->getUrl(), 'employers/employer'))
            $this->redirect(Yii::app()->createUrl('site/login'));

        return parent::beforeAction($action);
    }

    public function actionInbox($id) {
        $this->layout = "main";
        $empmodel = Employers::model()->findByPk($id);
        $model = Users::model()->findByPk(Yii::app()->user->getState('userid'));
        $this->render("inbox", array('model' => $model,'empmodel'=>$empmodel));
    }

    public function actiongetInboxMessages() {
        $this->layout = "main";
        $convmodel = Conversations::model()->findByPk($_POST['id']);
        $this->renderPartial('inbox_messages', array('convmodel' => $convmodel, 'to_user' => $_POST['msg_to']));
    }

    public function actionsaveMessage() {
        $this->layout = "main";
        $conid = $_POST['id'];
        $fromuser = Yii::app()->user->getState('userid');
        $touser = $_POST['msg_to'];
        $msg = $_POST['message'];
        $now = new DateTime();

        $convmodel = Conversations::model()->findByPk($_POST['id']);
        $usermodel = Users::model()->findByPk($fromuser);

        $convmsgmodel = new ConversationMessages();
        $convmsgmodel->conversation_id = $conid;
        $convmsgmodel->user_id = $fromuser;
        $convmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
        $convmsgmodel->message = $msg;
        $convmsgmodel->created_at = $now->format('Y-m-d H:i:s');
        $convmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($convmsgmodel->save()) {
            $activitymodel = new Activities();
            $activitymodel->user_id = $fromuser;
            $activitymodel->type = "message";
            $activitymodel->model_id = $convmsgmodel->id;
            $activitymodel->model_type = "App\Messaging\ConversationMessage";
            $activitymodel->subject = "You sent a message in conversation: " . $convmodel->subject;
            $activitymodel->message = $msg;
            $activitymodel->created_at = $now->format('Y-m-d H:i:s');
            $activitymodel->updated_at = $now->format('Y-m-d H:i:s');
            $activitymodel->save();

            $activitymodel = new Activities();
            $activitymodel->user_id = $touser;
            $activitymodel->type = "message";
            $activitymodel->model_id = $convmsgmodel->id;
            $activitymodel->model_type = "App\Messaging\ConversationMessage";
            $activitymodel->subject = "You sent a message in conversation: " . $convmodel->subject;
            $activitymodel->message = $msg;
            $activitymodel->created_at = $now->format('Y-m-d H:i:s');
            $activitymodel->updated_at = $now->format('Y-m-d H:i:s');
            $activitymodel->save();
        }
        Yii::app()->user->setFlash('success', 'Message sent successfully');
        $this->renderPartial('inbox_messages', array('convmodel' => $convmodel, 'to_user' => $_POST['msg_to']));
    }

    public function actionupgradestore() {
        //echo var_dump($_POST);
        $now = new DateTime();
	$model = Employers::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid')));
        $usermodel = Users::model()->findByPk(array('id'=>Yii::app()->user->getState('userid')));
        
        require Yii::app()->basePath . '/extensions/stripe/init.php';
	Stripe::setApiKey(Yii::app()->params['SECRET_Key']);
                
        $plan = $_POST['subscription_plan'];
        $token = $_POST['stripeToken'];
        $trial_ends = Carbon::now()->addYear()->toDateTimeString();
        
        $planarr = Plan::retrieve($plan);
        
        $amount = $planarr->amount;
        $planname = $planarr->name;
        $currency = $planarr->currency;
     
     //        create subscriber
         $customer = Customer::create(array(
            'source'   => $token,
            'email'    => $model->email,
            'plan'     => $plan,
            'description' => $model->name
        ));
         
         
       $charge = Charge::create(array(
            "amount" => $amount,
            "currency" => $currency,
            "customer" => $customer->id,
            "description" => $planname
        ));


////         create subscription
        $subscription = Subscription::create(array(
            "customer" => $customer->id,
            "items" => array(
                array(
                    "plan" => $plan,
                ),
            )
        ));
        
        $model->stripe_active = 1;
        $model->stripe_id = $customer->id;
        $model->stripe_subscription = $subscription->id;
        $model->stripe_plan = $plan;
        $model->last_four = $_POST['cvc'];
        $model->trial_ends_at = $trial_ends;
        $model->updated_at = $now->format('Y-m-d H:i:s');
        $model->save();
        
        Yii::app()->user->setFlash('success', 'Successfully updated your membership');
        $this->redirect(Yii::app()->createUrl('employers/edit', array('id' => $model->id)));
        
    }
    
    
    public function actioncardupdate() {
        
          //echo var_dump($_POST);
        $now = new DateTime();
        $model = Employers::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid')));
        $usermodel = Users::model()->findByPk(array('id'=>Yii::app()->user->getState('userid')));
        
        require Yii::app()->basePath . '/extensions/stripe/init.php';
	Stripe::setApiKey(Yii::app()->params['SECRET_Key']);
                
        $token = $_POST['stripeToken'];
        
        $cu = Customer::retrieve($model->stripe_id);
        $cu->source = $token; // obtained with Stripe.js
        $cu->save();
        
        $model->last_four = $_POST['cvc'];
        $model->updated_at = $now->format('Y-m-d H:i:s');
        $model->save();
        
        Yii::app()->user->setFlash('success', 'Your card information has been successfully updated');
        $this->redirect(Yii::app()->createUrl('employers/mysubscription'));


    }
    
    public function actionsendMessage() {
        
        $now = new DateTime();
        $conmodel = new Conversations();
        $conmodel->subject = $_POST['subject'];
        $conmodel->created_at = $now->format('Y-m-d H:i:s');
        $conmodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($conmodel->save()) {
            $usermodel = Users::model()->findByPk(Yii::app()->user->getState('userid'));
            $conmsgmodel = new ConversationMessages();
            $conmsgmodel->conversation_id = $conmodel->id;
            $conmsgmodel->user_id = Yii::app()->user->getState('userid');
            $conmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
            $conmsgmodel->message = $_POST['message'];
            $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
            $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($conmsgmodel->save()) {
                $conmemmodel_sender = new ConversationMembers();
                $conmemmodel_sender->conversation_id = $conmodel->id;
                $conmemmodel_sender->user_id = Yii::app()->user->getState('userid');
                $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
                $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
                $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                if ($conmemmodel_sender->save()) {
                    $usermodel1 = Users::model()->findByPk($_POST['user_id']);
                    $conmemmodel_rec = new ConversationMembers();
                    $conmemmodel_rec->conversation_id = $conmodel->id;
                    $conmemmodel_rec->user_id = $_POST['user_id'];
                    $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
                    $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                    $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                    if ($conmemmodel_rec->save()) {
                        $actmodel_sender = new Activities();
                        $actmodel_sender->user_id = Yii::app()->user->getState('userid');
                        $actmodel_sender->type = "message";
                        $actmodel_sender->model_id = $conmsgmodel->id;
                        $actmodel_sender->model_type = "ConversationMessage";
                        $actmodel_sender->subject = "You sent a message in conversation: " . $_POST['subject'];
                        $actmodel_sender->message = $_POST['message'];
                        $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                        $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                        if ($actmodel_sender->save()) {
                            $actmodel_rec = new Activities();
                            $actmodel_rec->user_id = $_POST['user_id'];
                            $actmodel_rec->type = "message";
                            $actmodel_rec->model_id = $conmsgmodel->id;
                            $actmodel_rec->model_type = "ConversationMessage";
                            $actmodel_rec->subject = "You received a message in conversation: " . $_POST['subject'];
                            $actmodel_rec->message = $_POST['message'];
                            $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                            $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                            $actmodel_rec->save();
                        }
                    }
                }
            }
        }
    }
    
    

}
