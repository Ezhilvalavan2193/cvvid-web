<?php
class VideosController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/admin';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
// 			array('deny',  // deny all users
// 				'users'=>array('*'),
// 			),
		);
	}

	public function beforeAction($action)
	{
	    Yii::app()->user->setReturnUrl(Yii::app()->request->getUrl());
	    $requestUrl=Yii::app()->request->url;
	    if (Yii::app()->user->isGuest)
	        $this->redirect(Yii::app()->createUrl('site/login'));
	        
	        return parent::beforeAction($action);
	} 
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Videos;
   
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Videos']))
		{
// 		    $vid = $_POST['Videos']['video_id'];
// 		    $json_url = 'http://vimeo.com/api/v2/video/'.$vid.'.xml';
// 		    $ch = curl_init($json_url);
// 		    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// 		    curl_setopt($ch, CURLOPT_HEADER, 0);
// 		    $data = curl_exec($ch);
// 		    curl_close($ch);
// 		    $data = new SimpleXmlElement($data, LIBXML_NOCDATA);
// 		    $duration = isset($data->video->duration) ? $data->video->duration : 0;
// 		    $imgpath = $data->video->thumbnail_large;
		   
		    
		    Yii::import('application.extensions.amazon.components.*');
		    
		    $bucket = Yii::app()->params['AWS_BUCKET'];
		    $s3 = new A2S3();
		    
		    $vname = pathinfo($_POST['Videos']['video_id'], PATHINFO_FILENAME);
		    $sec = 0;
// 		    $actual_image_name = $vname.time() . "." . $ext;
		    
		    //        video validation
$fullpath = 'http://' . $bucket . '.s3.amazonaws.com/' . $_POST['Videos']['video_id'];
		    if($model->category_id == 3)
		        $fullpath = 'http://' . $bucket . '.s3.amazonaws.com/how-to/' . $_POST['Videos']['video_id'];
	        if($model->category_id == 2)
	            $fullpath = 'http://' . $bucket . '.s3.amazonaws.com/success-stories/' . $_POST['Videos']['video_id'];
            if($model->category_id == 4)
                $fullpath = 'http://' . $bucket . '.s3.amazonaws.com/feedback/' . $_POST['Videos']['video_id'];
            if($model->category_id == 5)
                $fullpath = 'http://' . $bucket . '.s3.amazonaws.com/why-cvvid/' . $_POST['Videos']['video_id'];
            if($model->category_id == 6)
                $fullpath = 'https://' . $bucket . '.s3.amazonaws.com/top-tips/' . $_POST['Videos']['video_id'];     
		                    
            $array = get_headers($fullpath);
            $string = $array[0];
            if(!strpos($string,"200"))
            {
                Yii::app()->user->setFlash('VideoError', 'Video not found!');
                
                $this->render('create',array(
                    'model'=>$model,
                ));
                exit();
            }
		    $now = new DateTime();
		   
			$model->attributes=$_POST['Videos'];
			$model->model_type = ucfirst(Yii::app()->user->getState('role'));
			$model->model_id = Yii::app()->user->getState('userid');
			$model->is_processed = 1;
			$model->state = 1;
			$model->status = "active";
			$model->duration = (int)$sec;
			$model->created_at = $now->format('Y-m-d H:i:s');
			if($model->save())
			{			   
			    $vid = $model->getPrimaryKey();
			   // $fullpath = 'http://' . $bucket . '.s3.amazonaws.com/how-to/' . $_POST['Videos']['video_id'];
			   /*  $fullpath = 'http://' . $bucket . '.s3.amazonaws.com/' . $_POST['Videos']['video_id'];
			    if($model->category_id == 3)
			          $fullpath = 'http://' . $bucket . '.s3.amazonaws.com/how-to/' . $_POST['Videos']['video_id'];
			    if($model->category_id == 2)
			        $fullpath = 'http://' . $bucket . '.s3.amazonaws.com/success-stories/' . $_POST['Videos']['video_id'];
			   if($model->category_id == 4)
		            $fullpath = 'http://' . $bucket . '.s3.amazonaws.com/feedback/' . $_POST['Videos']['video_id'];
	            if($model->category_id == 5)
	                $fullpath = 'http://' . $bucket . '.s3.amazonaws.com/why-cvvid/' . $_POST['Videos']['video_id'];
                if($model->category_id == 6)
                    $fullpath = 'https://' . $bucket . '.s3.amazonaws.com/top-tips/' . $_POST['Videos']['video_id'];     */

			    
			    $cmd = "ffmpeg -i $fullpath 2>&1";
			    $ffmpeg = shell_exec($cmd);
			    
			    $search = "/Duration: (.*?)\./";
			    preg_match($search, $ffmpeg, $matches);
			    
			    if (isset($matches[1])) {
			        $data['duration'] = $matches[1];
			        $time_sec = explode(':', $data['duration']);
			        $sec = ($time_sec['0'] * 3600) + ($time_sec['1'] * 60) + $time_sec['2'];
			    }
			    
			    Videos::model()->updateByPk($model->id, array('duration' => $sec));
			    
			    $img_name = base64_encode($model->video_id).".jpg";//substr($imgpath, $pos + strlen($del), strlen($imgpath) - 1);
			    
			    $mediamodel = new Media();
			    $mediamodel->model_id = $vid;
			    $mediamodel->model_type = "App\Videos\Video";
			    $mediamodel->collection_name = "video";
			    $mediamodel->name = $vname;
			    $mediamodel->file_name = $vname.".jpg";
			    $mediamodel->disk = "media";
			    $mediamodel->size = (int)$sec;
			    $mediamodel->created_at = $now->format('Y-m-d H:i:s');
			    if($mediamodel->save())
			    {
			        Media::model()->updateByPk($mediamodel->id, array('model_id' => $vid, 'size' => $sec));
			        
			        $iname = $vname . ".jpg";
			        $mediafolder = "images/media/" . $mediamodel->id.'/conversions';
			        
			        if (!file_exists($mediafolder)) {
			            mkdir($mediafolder, 0777, true);
			        }
			        
			        $imgfullpath = "images/media/" . $mediamodel->id . "/" . $iname;
			        $imgprefix = "images/media/" . $mediamodel->id;
			        
			        $ffurl = "ffmpeg -i $fullpath -ss 00:00:2 -vframes 1 $imgfullpath";
			        
			        $process = exec($ffurl);
			        
			        $jlwidth = 350;
			        $jlheight = 190;
			        $jldestpath = $imgprefix . "/conversions/profile.jpg";
			        
			        //create search
			        $searchwidth = 167;
			        $searchheight = 167;
			        $searchdestpath = $imgprefix . "/conversions/search.jpg";
			        
			        //create thumb
			        $thumbwidth = 126;
			        $thumbheight = 126;
			        $thumbdestpath = $imgprefix . "/conversions/thumb.jpg";
			        $image_type = "image/jpg";
			        
			        $image = imagecreatefromjpeg($imgfullpath);
			        
			        $this->resize_image($image, $image_type, $imgfullpath, $jldestpath, $jlwidth, $jlheight); //create joblisting
			        $this->resize_image($image, $image_type, $imgfullpath, $searchdestpath, $searchwidth, $searchheight); //create search
			        $this->resize_image($image, $image_type, $imgfullpath, $thumbdestpath, $thumbwidth, $thumbheight); //create thumb
			        
			        
			    }
			   
				$this->redirect(array('admin'));
			}
			
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Videos']))
		{
		    $now = new DateTime();		    
			$model->attributes=$_POST['Videos'];
			$model->updated_at = $now->format('Y-m-d H:i:s');
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Videos');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Videos('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Videos']))
			$model->attributes=$_GET['Videos'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Videos the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Videos::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Videos $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='videos-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	public function actionEdit($id)
	{
	    $model = Videos::model()->findByPk($id);
	    $usermodel = Users::model()->findByPk($model->model_id);
           // if($usermodel == NULL)
               // $usermodel = new Users();
	    $this->render('edit',array(
	        'model'=>$model,'usermodel'=>$usermodel
	    ));
	}
	
	public function actionmarkvideo()
	{
	   $now = new DateTime();
           
           $video = Videos::model()->findByPk($_POST['id']);
           $model = Users::model()->findByPk($video->model_id);
           
           $video->status = 'flagged';
           $video->updated_at = $now->format('Y-m-d H:i:s');
           $video->save();
           
           $subject = $_POST['subject'];
           if($subject == "")
           {
               $subject = 'Your Video has been flagged';
           }
           
           $body = $_POST['message'];
           
           $login_url = Yii::app()->createUrl('site/login');
           
           $bodymsg = $this->renderPartial("//emails/flagged-video", array('title' => $subject,'body' => $body,'login_url' => $login_url), true);
            
           // Send employer viewed notifcation to candidate
           Yii::import('application.extensions.phpmailer.JPhpMailer');
           $mail = new JPhpMailer;
           $mail->Host = 'mail.cvvid.com';
           $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
           $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
           $mail->SMTPSecure = 'ssl';
           $mail->SetFrom('mail@cvvid.com', 'CVVID');
           $mail->Subject = "Your Video has been flagged";
           $mail->MsgHTML($bodymsg);
           $mail->AddAddress($model->email, $model->forenames);
           $mail->Send();
           
           Yii::app()->user->setFlash('success', 'Successfully flagged video');
           
           $this->redirect(Yii::app()->createUrl('videos/admin'));
           
	}
        
        public function actionvideoUpdate()
	{
	   $now = new DateTime();
           
           $video = Videos::model()->findByPk($_GET['id']);
           
           $video->attributes=$_POST['Videos'];
           if($video->save(FALSE))
           {
                Yii::app()->user->setFlash('success', 'Successfully updated');
                $this->redirect(Yii::app()->createUrl('videos/edit', array('id' => $video->id)));
           }
           
	}
        
        public function actiondeletevideo()
	{
	   $now = new DateTime();
           
           $video = Videos::model()->findByPk($_POST['id']);
           $model = Users::model()->findByPk($video->model_id);
           
           $video->delete();
           
           $subject = $_POST['subject'];
           if($subject == "")
           {
               $subject = 'Deleted Video';
           }
           
           $body = $_POST['message'];
           
           $login_url = Yii::app()->createUrl('site/login');
           
           $bodymsg = $this->renderPartial("//emails/flagged-video", array('title' => $subject,'body' => $body,'login_url' => $login_url), true);
            
           // Send employer viewed notifcation to candidate
           Yii::import('application.extensions.phpmailer.JPhpMailer');
           $mail = new JPhpMailer;
           $mail->Host = 'mail.cvvid.com';
           $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
           $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
           $mail->SMTPSecure = 'ssl';
           $mail->SetFrom('mail@cvvid.com', 'CVVID');
           $mail->Subject = "Successfully deleted video";
           $mail->MsgHTML($bodymsg);
           $mail->AddAddress($model->email, $model->forenames);
           $mail->Send();
           
           Yii::app()->user->setFlash('success', 'Successfully deleted video');
           
           $this->redirect(Yii::app()->createUrl('videos/admin'));
           
	}
	
	function resize_image($image, $image_type, $fullpath, $destpath, $width, $height) {
	    $size_arr = getimagesize($fullpath);
	    
	    list($width_orig, $height_orig, $img_type) = $size_arr;
	    $ratio_orig = $width_orig / $height_orig;
	    
	    $tempimg = imagecreatetruecolor($width, $height);
	    imagecopyresampled($tempimg, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
	    
	    if ($image_type == "image/jpg" || $image_type == "image/jpeg")
	        imagejpeg($tempimg, $destpath);
	        else if ($image_type == "image/png")
	            imagepng($tempimg, $destpath);
	            else if ($image_type == "image/gif")
	                imagegif($tempimg, $destpath);
	}
}
