<?php
use Spipu\Html2Pdf\Tag\Address;
use Stripe\Charge;
use Stripe\Invoice;
use Stripe\InvoiceItem;
use Stripe\Stripe;
use Stripe\Token;
use Stripe\Customer;
use Stripe\Plan;
use Stripe\Subscription;

class UsersController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/admin';
    public $userid = 0;
    public $edit = 0;
    public $dashboard = 0;

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
                //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('admin', 'delete'),
                'users' => array('admin'),
            ),
// 			array('deny',  // deny all users
// 				'users'=>array('*'),
// 			),
        );
    }

    public function beforeAction($action) {
        Yii::app()->user->setReturnUrl(Yii::app()->request->getUrl());
        $requestUrl = Yii::app()->request->url;
        if (Yii::app()->user->isGuest)
            $this->redirect(Yii::app()->createUrl('site/login'));

        return parent::beforeAction($action);
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Users;
        $profilemodel = new Profiles;
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Users'])) {
            $now = new DateTime();
            $model->attributes = $_POST['Users'];
            $model->type = 'candidate';
            $model->created_at = $now->format('Y-m-d H:i:s');
           // $model->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
            if ($model->save()) {
                $profilemodel->owner_id = $model->id;
                $profilemodel->created_at = $model->created_at;
                $profilemodel->type = $model->type;
                //$profilemodel->slug = lcfirst($model->forenames);
                if ($profilemodel->save())
                    $this->redirect(array('admin'));
            }
        }

        $this->render('create', array(
            'model' => $model,
            'pmodel' => $profilemodel
        ));
    }

    public function actioncreateCandidate($trial, $paid, $basic) {
        
        $model = new Users();
        $profilemodel = new Profiles();
        $addressmodel = new Addresses();
        $now = new DateTime();
        
       
        
        if(isset($_POST['Users'])){
        if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
           $model->setScenario('confirm');
           $model->attributes=$_POST['Users'];
        }
        if(isset ($_POST["Addresses"])){
            $addressmodel->attributes = $_POST["Addresses"]; 
        }
        if(isset ($_POST["Profiles"])){
            $profilemodel->attributes = $_POST["Profiles"];
        }     
        if(isset($_POST['Users']))
        {
             $valid = $model->validate();
             $valid = $addressmodel->validate() && $valid;
             $valid = $profilemodel->validate() && $valid;
             
             if($valid){
                 
                $d = str_replace('/', '-', $_POST["dob"]);
                $dobformat = date('Y-m-d', strtotime($d));
                $model->dob = $dobformat;
                $model->type = 'candidate';
				 $model->mobile = $_POST['Users']['mobile'];
                $model->created_at = $now->format('Y-m-d H:i:s');
                $model->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
				 $model->location = $_POST['Addresses']['county'].($_POST['Addresses']['county']!=''? ', '.$_POST['Addresses']['country']: '');
            $model->location_lat = $_POST['Addresses']['latitude'];
            $model->location_lng = $_POST['Addresses']['longitude'];
				 $model->api_key = md5(uniqid(rand(), true));
                if($trial == 1)
                    $model->trial_ends_at = Carbon::now()->addYear()->toDateTimeString();
                $model->save();
                
//                profile model
                $profilemodel->owner_type = "Candidate";
                $profilemodel->owner_id = $model->id;
                if ($_POST['photo_id'] > 0)
                    $profilemodel->photo_id = $_POST['photo_id'];
                $profilemodel->created_at = $model->created_at;
                $profilemodel->type = $model->type;
                $profilemodel->save();
//                address insert
                $addressmodel->attributes = $_POST['Addresses'];
                $addressmodel->model_id = $model->id;
                $addressmodel->model_type = "Candidate";
                $addressmodel->created_at = $now->format('Y-m-d H:i:s');
                if ($addressmodel->save())
                {
                    Yii::app()->user->setFlash('success', 'Successfully candidate created.');
                    $this->redirect($this->createUrl('users/admin', array('trial' => $trial, 'paid' => $paid, 'basic' => $basic)));
                }
             }
        }
        $this->render('createcandidate', array(
            'model' => $model, 'profilemodel' => $profilemodel, 'addressmodel' => $addressmodel,
            'trial' => $trial, 'paid' => $paid, 'basic' => $basic
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Users'])) {
            $now = new DateTime();
            $model->attributes = $_POST['Users'];
            $model->updated_at = $now->format('Y-m-d H:i:s');
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        //$this->loadModel($id)->delete();

        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        $now = new DateTime();
        $model->deleted_at = $now->format('Y-m-d H:i:s');
        if ($model->save()) {
            $pmodel = new Profiles();
            $now = new DateTime();
            $pmodel->deleted_at = $now->format('Y-m-d H:i:s');
            $pmodel->save();
        }

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $dataProvider = new CActiveDataProvider('Users');
        $this->render('index', array(
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin($trial = 0, $paid = 0, $basic = 0) {
        $model = new Users('search');
        $model->unsetAttributes();  // clear any default values

        $namesearch = 0;
        $fullname = '';
        $userarr = array();

        if (isset($_GET['Users']))
            $model->attributes = $_GET['Users'];
        if (isset($_GET['fullname']) && $_GET['fullname'] != "") {
            $namesearch = 1;
            $fullname = $_GET['fullname'];
            $cri = new CDbCriteria();
            $cri->condition = "concat_ws(' ',forenames,surname) like '%" . $fullname . "%' and type = 'candidate' and deleted_at is null";

            if ($trial == 1)
                $cri->addCondition('trial_ends_at is not null');
            else if ($paid == 1)
                $cri->addCondition('stripe_id > 0');
            else if ($basic == 1)
                $cri->addCondition('trial_ends_at is null and stripe_id is null');

            $searchusers = Users::model()->findAll($cri);
            $userarr = array();
            foreach ($searchusers as $users)
                $userarr[] = $users['id'];
        }

        $this->render('admin', array(
            'model' => $model, 'trial' => $trial, 'paid' => $paid, 'basic' => $basic, 'namesearch' => $namesearch, 'namearray' => $userarr, 'fullname' => $fullname
        ));
    }

    /**
     * Manages all models.
     */
    public function actionsearchAdmin($trial = 0, $paid = 0, $basic = 0) {
        $model = new Users('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Users'])) {
            $model->attributes = $_GET['Users'];
        }

        $this->render('admin', array(
            'model' => $model, 'issearch' => 1, 'trial' => $trial, 'paid' => $paid, 'basic' => $basic
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Users the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Users::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Users $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'users-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

   public function actionProfile($slug) {
        
        $model = $this->loadModelSlug($slug);//Users::model()->findByPk($id);
        $id = $model->id;
        $this->layout = 'main';
        $pmodel = Profiles::model()->findByAttributes(array('owner_id' => $id));
        $pid = $pmodel->id;
        // $this->layout = "profile";
        $this->userid = $id;
       
        
        $now = new DateTime();
        $type = ucfirst(Yii::app()->user->getState('role'));
        $modelid = Yii::app()->user->getState('userid');
        if(Yii::app()->user->getState('role') != "candidate")
        {
               
                $cri = new CDbCriteria();
                $cri->condition = "profile_id =" . $pid . " and model_type like '%" . $type . "%' and model_id=" . $modelid;
        
                $profileviews = ProfileViews::model()->find($cri);
                if ($profileviews == null) {
                    
                    $canprofmodel = Profiles::model()->findByPk($pid);
                    
                    $pvmodel = new ProfileViews();
                    $pvmodel->profile_id = $pid;
                    $pvmodel->model_type = ucfirst(Yii::app()->user->getState('role'));
                    $pvmodel->model_id = Yii::app()->user->getState('userid');
                    $pvmodel->view_count = 1;
                    $pvmodel->created_at = $now->format('Y-m-d H:i:s');
                    if($pvmodel->save() && Yii::app()->user->getState('role') == 'employer')
                    {
                        $empusermodel = EmployerUsers::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid')));
                        $empmodel = Employers::model()->findByPk($empusermodel->employer_id);
                         $epmodel = Profiles::model()->findByAttributes(array('owner_id' => $empusermodel->employer_id,'type'=>'employer'));
                        
                        $subject = "An employer viewed your profile";
                        $msg = "Your profile has been viewed by employer <a href=".$this->createUrl('employers/employer',array('slug'=>$epmodel->slug))."> ".$empmodel->name." </a>";
                       
                        $now = new DateTime();
                        $conmodel = new Conversations();
                        $conmodel->subject = $subject;
                        $conmodel->created_at = $now->format('Y-m-d H:i:s');
                        $conmodel->updated_at = $now->format('Y-m-d H:i:s');
                        if ($conmodel->save()) {
                            $usermodel = Users::model()->findByPk(Yii::app()->user->getState('userid'));
                            $conmsgmodel = new ConversationMessages();
                            $conmsgmodel->conversation_id = $conmodel->id;
                            $conmsgmodel->user_id = Yii::app()->user->getState('userid');
                            $conmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
                            $conmsgmodel->message = $msg;
                            $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
                            $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
                            if ($conmsgmodel->save()) {
                                $conmemmodel_sender = new ConversationMembers();
                                $conmemmodel_sender->conversation_id = $conmodel->id;
                                $conmemmodel_sender->user_id = Yii::app()->user->getState('userid');
                                $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
                                $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
                                $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                                $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                                if ($conmemmodel_sender->save()) {
                                    $usermodel1 = Users::model()->findByPk($canprofmodel->owner_id);
                                    $conmemmodel_rec = new ConversationMembers();
                                    $conmemmodel_rec->conversation_id = $conmodel->id;
                                    $conmemmodel_rec->user_id = $canprofmodel->owner_id;
                                    $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
                                    $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                                    $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                                    if ($conmemmodel_rec->save()) {
                                        $actmodel_sender = new Activities();
                                        $actmodel_sender->user_id = Yii::app()->user->getState('userid');
                                        $actmodel_sender->type = "message";
                                        $actmodel_sender->model_id = $conmsgmodel->id;
                                        $actmodel_sender->model_type = "ConversationMessage";
                                        $actmodel_sender->subject = "You sent a message in conversation: " . $subject;
                                        $actmodel_sender->message = $msg;
                                        $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                                        $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                                        if ($actmodel_sender->save()) {
                                            $actmodel_rec = new Activities();
                                            $actmodel_rec->user_id = $canprofmodel->owner_id;
                                            $actmodel_rec->type = "message";
                                            $actmodel_rec->model_id = $conmsgmodel->id;
                                            $actmodel_rec->model_type = "ConversationMessage";
                                            $actmodel_rec->subject = "You received a message in conversation: " . $subject;
                                            $actmodel_rec->message = $msg;
                                            $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                                            $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                                            $actmodel_rec->save();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    $body = $this->renderPartial("//emails/profile-viewed", array('pmodel' => $pmodel,'model'=>$model), true);
                    // Send employer viewed notifcation to candidate
                    Yii::import('application.extensions.phpmailer.JPhpMailer');
                    $mail = new JPhpMailer;
                    $mail->Host = 'mail.cvvid.com';
                    $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
                    $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
                    $mail->SMTPSecure = 'ssl';
                    $mail->SetFrom('mail@cvvid.com', 'CVVID');
                    $mail->Subject = "An Employer viewed your profile!";
                    $mail->MsgHTML($body);
                    $mail->AddAddress($model->email, $model->forenames);
                    $mail->Send();
                    
                    
                } else {
        //          ProfileViews::model()->updateCounters(array('view_count'=>1),
        //                          "profile_id =:profile_id and model_type like '%:model_type%' and model_id =:model_id",array(':profile_id'=>$id,':model_type'=>$type,':model_id'=>$modelid));
        
                    $countcri = new CDbCriteria();
                    $countcri->select = 'max(view_count) as view_count';
                    $countcri->condition = "profile_id =" . $pid . " and model_type like '%" . $type . "%' and model_id=" . $modelid;
                    $proview = ProfileViews::model()->find($countcri);
                    $viewcount = $proview['view_count'] + 1;
        
                    $cri = new CDbCriteria();
                    $cri->condition = "profile_id =" . $pid . " and model_type like '%" . $type . "%' and model_id=" . $modelid;
                    ProfileViews::model()->updateAll(array('updated_at' => $now->format('Y-m-d H:i:s'), 'view_count' => $viewcount), $cri);
        
                    $pcri = new CDbCriteria();
                    $pcri->select = 'sum(view_count) as view_count';
                    $pcri->condition = "profile_id =" . $pid;
                    $profviews = ProfileViews::model()->find($pcri);
                    
                    $cri1 = new CDbCriteria();
                    $cri1->condition = "id =" . $pid;
                    
                    Profiles::model()->updateAll(array('updated_at' => $now->format('Y-m-d H:i:s'),'num_views'=>$profviews['view_count']),$cri1);
                    //"profile_id =:profile_id and model_type like '%:model_type%' and model_id =:model_id",array(':profile_id'=>$id,':model_type'=>$type,':model_id'=>$modelid));
                }
                
                
                
        }
        
        $this->render('profile', array(
            'model' => $model,
        ));
    }

    public function actionEdit($slug) {
        if(Yii::app()->user->getState('role') != "candidate")
            $this->redirect($this->createUrl('site/index'));
       
        $model = $this->loadModelSlug($slug);// Users::model()->findByPk($id);
        $id = $model->id;
        $this->layout = "main";
        $this->userid = $id;
        $this->edit = 1;
       
        
        if($model->trial_ends_at != "" && $model->trial_ends_at != null)
        {
            $today = date('Y-m-d');
            $date_of_expiry = date_format(new DateTime($model->trial_ends_at),"Y-m-d");
            // $date_of_expiry = $model->trial_ends_at;
            $datetime1 = new DateTime($today);
            $datetime2 = new DateTime($date_of_expiry);
            $interval = $datetime1->diff($datetime2);
            $diff = $interval->format('%a');
            if($diff <= 7)
            {                
                $body = $this->renderPartial("//emails/renew-subscription", array('model'=>$model), true);
                // Send employer viewed notifcation to candidate
                Yii::import('application.extensions.phpmailer.JPhpMailer');
                $mail = new JPhpMailer;
                $mail->Host = 'mail.cvvid.com';
                $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
                $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
                $mail->SMTPSecure = 'ssl';
                $mail->SetFrom('mail@cvvid.com', 'CVVID');
                $mail->Subject = "Renew Subscription - action required";
                $mail->MsgHTML($body);
                $mail->AddAddress($model->email, $model->forenames);
                $mail->Send();
                
                Yii::app()->user->setFlash('subscription-ends','Your Subscription plan ends in '.$diff.' days');
            }
        }
        $this->render('editprofile', array(
            'model' => $model,
        ));
    }

    public function actionsaveUserDoc() {
        $usermodel = Users::model()->findByPk($_POST['user_id']);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));

        $now = new DateTime();

        $mediamodel = new Media();
        $mediamodel->model_type = "Candidate";
        $mediamodel->model_id = $_POST['user_id'];
        $mediamodel->collection_name = "documents";
        $mediamodel->name = $_FILES['doc_file']['name'];
        $mediamodel->file_name = $_FILES['doc_file']['name'];
        $mediamodel->size = $_FILES['doc_file']['size'];
        $mediamodel->disk = "documents";
        $mediamodel->created_at = $now->format('Y-m-d H:i:s');
        if ($mediamodel->save()) {
            $model = new UserDocuments();
            $model->user_id = $_POST['user_id'];
            $model->media_id = $mediamodel->id;
            $model->name = $_POST['doc_name'];
            $model->category_id = $_POST['category_id'];
            $model->published = $_POST['published'];
            $model->created_at = $now->format('Y-m-d H:i:s');
            if ($model->save()) {
                $pdmodel = new ProfileDocuments();
                $pdmodel->profile_id = $profilemodel->id;
                $pdmodel->user_document_id = $model->id;
                $pdmodel->created_at = $now->format('Y-m-d H:i:s');
                $pdmodel->save();
            }
            $docmodel = Users::model()->findByPk($model->user_id);
            
            
            echo $this->renderPartial('userDocuments', array('model' => $docmodel));
        }
    }

    public function actionupdateUserDoc() {
        $now = new DateTime();
        $model = UserDocuments::model()->findByPk($_POST['id']);

        $usermodel = Users::model()->findByPk($model->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));

        $mediamodel = Media::model()->findByPk($_POST['media_id']);
        $mediamodel->name = $_FILES['doc_file']['name'];
        $mediamodel->file_name = $_FILES['doc_file']['name'];
        $mediamodel->size = $_FILES['doc_file']['size'];
        $mediamodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($mediamodel->save()) {
            $model->name = $_POST['name'];
            $model->category_id = $_POST['category_id'];
            $model->published = $_POST['published'];
            $model->updated_at = $now->format('Y-m-d H:i:s');
            if ($model->save()) {
                $pdmodel = ProfileDocuments::model()->findByAttributes(array('profile_id' => $profilemodel->id, 'user_document_id' => $model->id));
                $pdmodel->updated_at = $now->format('Y-m-d H:i:s');
                $pdmodel->save();
            }

            $docmodel = Users::model()->findByPk($model->user_id);
            echo $this->renderPartial('userDocuments', array('model' => $docmodel));
        }
    }

    public function actiondeleteUserDoc() {
        $model = UserDocuments::model()->findByPk($_POST['id']);
        $userid = $model->user_id;

        $docmodel = Users::model()->findByPk($userid);

        $cri = new CDbCriteria();
        $cri->condition = "model_id =" . $model->media_id;
        Media::model()->deleteAll($cri);

        $cri1 = new CDbCriteria();
        $cri1->condition = "user_document_id =" . $_POST['id'];
        ProfileDocuments::model()->deleteAll($cri1);

        $cri2 = new CDbCriteria();
        $cri2->condition = "id =" . $_POST['id'];
        UserDocuments::model()->deleteAll($cri2);

        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $docmodel->id, 'type' => $docmodel->type));
        $this->updatepercentage($userid, $profilemodel);

        echo $this->renderPartial('userDocuments', array('model' => $docmodel));
    }

    public function actionsaveUserExp() {
        $usermodel = Users::model()->findByPk($_POST['user_id']);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));

        $now = new DateTime();
        $current = null;
        $start_month = $_POST['start_month'];
        $start_year = $_POST['start_year'];
        $start_date = $_POST['start_year'] . "-" . $_POST['start_month'] . "-01";
        
        if (isset($_POST['is_current']))
            $end_date = null;
        else 
        {
            $end_month = $_POST['end_month'];
            $end_year = $_POST['end_year'];
            $end_date = $_POST['end_year'] . "-" . $_POST['end_month'] . "-01";
        }
            
        $model = new UserExperiences();
        $model->user_id = $_POST['user_id'];
        $model->start_date = $start_date;
        $model->end_date = $end_date;
        $model->description = $_POST['description'];
        $model->position = $_POST['position'];
        $model->company_name = $_POST['company_name'];
        $model->location = $_POST['location'];
        $model->created_at = $now->format('Y-m-d H:i:s');
        if ($model->save()) {
            $pemodel = new ProfileExperiences();
            $pemodel->profile_id = $profilemodel->id;
            $pemodel->user_experience_id = $model->id;
            $pemodel->created_at = $now->format('Y-m-d H:i:s');
            $pemodel->save();

            $this->updatepercentage($usermodel->id,$profilemodel);   
        }

        $expmodel = Users::model()->findByPk($model->user_id);
        echo $this->renderPartial('userExp', array('model' => $expmodel));
    }

     public function actionupdateUserExp() {
        $model = UserExperiences::model()->findByPk($_POST['id']);
        $usermodel = Users::model()->findByPk($model->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));

        $now = new DateTime();
        $current = null;
        $start_month = $_POST['start_month'];
        $start_year = $_POST['start_year'];
        $start_date = $_POST['start_year'] . "-" . $_POST['start_month'] . "-01";
        
        if (isset($_POST['is_current']))
            $end_date = null;
        else
        {
            $end_month = $_POST['end_month'];
            $end_year = $_POST['end_year'];
            $end_date = $_POST['end_year'] . "-" . $_POST['end_month'] . "-01";
        }

        $model->start_date = $start_date;
        $model->end_date = $end_date;
        $model->description = $_POST['description'];
        $model->position = $_POST['position'];
        $model->company_name = $_POST['company_name'];
        $model->location = $_POST['location'];
        $model->updated_at = $now->format('Y-m-d H:i:s');
        if ($model->save()) {
            $pemodel = ProfileExperiences::model()->findByAttributes(array('profile_id' => $profilemodel->id, 'user_experience_id' => $model->id));
            $pemodel->updated_at = $now->format('Y-m-d H:i:s');
            $pemodel->save();
        }

        $expmodel = Users::model()->findByPk($model->user_id);
        echo $this->renderPartial('userExp', array('model' => $expmodel));
    }

    public function actiondeleteUserExp() {
        $model = UserExperiences::model()->findByPk($_POST['id']);
        $userid = $model->user_id;

        $cri = new CDbCriteria();
        $cri->condition = "user_experience_id =" . $_POST['id'];
        ProfileExperiences::model()->deleteAll($cri);

        $cri1 = new CDbCriteria();
        $cri1->condition = "id =" . $_POST['id'];
        UserExperiences::model()->deleteAll($cri1);

        $expmodel = Users::model()->findByPk($userid);

        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $expmodel->id, 'type' => $expmodel->type));
        $this->updatepercentage($userid, $profilemodel);

        echo $this->renderPartial('userExp', array('model' => $expmodel));
    }

    public function actionsaveUserEducation() {
        $usermodel = Users::model()->findByPk($_POST['user_id']);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));

        $now = new DateTime();
        $completion_month = $_POST['completion_month'];
        $completion_year = $_POST['completion_year'];
        $completion_date = $_POST['completion_year'] . "-" . $_POST['completion_month'] . "-01";

        $model = new UserQualifications();
        $model->user_id = $_POST['user_id'];
        $model->completion_date = $completion_date;
        $model->institution = $_POST['institution'];
        $model->location = $_POST['location'];
        $model->field = $_POST['field'];
        $model->level = $_POST['level'];
        $model->grade = $_POST['grade'];
        $model->description = $_POST['description'];
        $model->created_at = $now->format('Y-m-d H:i:s');
        if ($model->save()) {
            $pqmodel = new ProfileQualifications();
            $pqmodel->profile_id = $profilemodel->id;
            $pqmodel->user_qualification_id = $model->id;
            $pqmodel->created_at = $now->format('Y-m-d H:i:s');
            $pqmodel->save();

             $this->updatepercentage($usermodel->id,$profilemodel);    
        }

        $edumodel = Users::model()->findByPk($model->user_id);
        echo $this->renderPartial('userEducation', array('model' => $edumodel));
    }

    public function actionupdateUserEducation() {
        $model = UserQualifications::model()->findByPk($_POST['id']);
        $usermodel = Users::model()->findByPk($model->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));

        $now = new DateTime();

        $completion_month = $_POST['completion_month'];
        $completion_year = $_POST['completion_year'];
        $completion_date = $_POST['completion_year'] . "-" . $_POST['completion_month'] . "-01";


        $model->completion_date = $completion_date;
        $model->institution = $_POST['institution'];
        $model->location = $_POST['location'];
        $model->field = $_POST['field'];
        $model->level = $_POST['level'];
        $model->grade = $_POST['grade'];
        $model->description = $_POST['description'];
        $model->updated_at = $now->format('Y-m-d H:i:s');
        if ($model->save()) {
            $pemodel = ProfileQualifications::model()->findByAttributes(array('profile_id' => $profilemodel->id, 'user_qualification_id' => $model->id));
            $pemodel->updated_at = $now->format('Y-m-d H:i:s');
            $pemodel->save();
        }

        $edumodel = Users::model()->findByPk($model->user_id);
        echo $this->renderPartial('userEducation', array('model' => $edumodel));
    }

    public function actiondeleteUserEducation() {
        $model = UserQualifications::model()->findByPk($_POST['id']);
        $userid = $model->user_id;

        $cri = new CDbCriteria();
        $cri->condition = "user_qualification_id =" . $_POST['id'];
        ProfileQualifications::model()->deleteAll($cri);

        $cri1 = new CDbCriteria();
        $cri1->condition = "id =" . $_POST['id'];
        $model = UserQualifications::model()->deleteAll($cri1);

        $expmodel = Users::model()->findByPk($userid);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $expmodel->id, 'type' => $expmodel->type));
        $this->updatepercentage($userid, $profilemodel);
        echo $this->renderPartial('userEducation', array('model' => $expmodel));
    }

    public function actionsaveUserHobbies() {
        $usermodel = Users::model()->findByPk($_POST['user_id']);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));
        $now = new DateTime();

        $model = new UserHobbies();
        $model->user_id = $_POST['user_id'];
        $model->activity = $_POST['activity'];
        $model->description = $_POST['description'];
        $model->created_at = $now->format('Y-m-d H:i:s');
        if ($model->save()) {
            $pqmodel = new ProfileHobbies();
            $pqmodel->profile_id = $profilemodel->id;
            $pqmodel->user_hobby_id = $model->id;
            $pqmodel->created_at = $now->format('Y-m-d H:i:s');
            $pqmodel->save();

             $this->updatepercentage($usermodel->id,$profilemodel); 
        }

        $edumodel = Users::model()->findByPk($model->user_id);
        echo $this->renderPartial('userHobbies', array('model' => $edumodel));
    }

    public function actionupdateUserHobbies() {
        $model = UserHobbies::model()->findByPk($_POST['id']);
        $usermodel = Users::model()->findByPk($model->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));

        $now = new DateTime();

        $model->activity = $_POST['activity'];
        $model->description = $_POST['description'];
        $model->updated_at = $now->format('Y-m-d H:i:s');
        if ($model->save()) {
            $pemodel = ProfileHobbies::model()->findByAttributes(array('profile_id' => $profilemodel->id, 'user_hobby_id' => $model->id));
            $pemodel->updated_at = $now->format('Y-m-d H:i:s');
            $pemodel->save();
        }

        $edumodel = Users::model()->findByPk($model->user_id);
        echo $this->renderPartial('userHobbies', array('model' => $edumodel));
    }

    public function actiondeleteUserHobbies() {
        $model = UserHobbies::model()->findByPk($_POST['id']);
        $userid = $model->user_id;

        $cri = new CDbCriteria();
        $cri->condition = "user_hobby_id =" . $_POST['id'];
        ProfileHobbies::model()->deleteAll($cri);

        $cri1 = new CDbCriteria();
        $cri1->condition = "id =" . $_POST['id'];
        $model = UserHobbies::model()->deleteAll($cri1);

        $expmodel = Users::model()->findByPk($userid);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $expmodel->id, 'type' => $expmodel->type));
        $this->updatepercentage($userid, $profilemodel);
        echo $this->renderPartial('userHobbies', array('model' => $expmodel));
    }

    public function actionsaveUserLanguages() {
        $usermodel = Users::model()->findByPk($_POST['user_id']);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));
        $now = new DateTime();

        $model = new UserLanguages();
        $model->user_id = $_POST['user_id'];
        $model->name = $_POST['name'];
        $model->proficiency = $_POST['proficiency'];
        $model->created_at = $now->format('Y-m-d H:i:s');
        if ($model->save()) {
            $pqmodel = new ProfileLanguages();
            $pqmodel->profile_id = $profilemodel->id;
            $pqmodel->user_language_id = $model->id;
            $pqmodel->created_at = $now->format('Y-m-d H:i:s');
            $pqmodel->save();

             $this->updatepercentage($usermodel->id,$profilemodel);  
        }

        $langmodel = Users::model()->findByPk($model->user_id);
        echo $this->renderPartial('userLanguage', array('model' => $langmodel));
    }

    public function actionupdateUserLanguages() {
        $model = UserLanguages::model()->findByPk($_POST['id']);
        $usermodel = Users::model()->findByPk($model->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));

        $now = new DateTime();

        $model->name = $_POST['name'];
        $model->proficiency = $_POST['proficiency'];
        $model->updated_at = $now->format('Y-m-d H:i:s');
        if ($model->save()) {
            $pemodel = ProfileLanguages::model()->findByAttributes(array('profile_id' => $profilemodel->id, 'user_language_id' => $model->id));
            $pemodel->updated_at = $now->format('Y-m-d H:i:s');
            $pemodel->save();
        }

        $langmodel = Users::model()->findByPk($model->user_id);
        echo $this->renderPartial('userLanguage', array('model' => $langmodel));
    }

    public function actiondeleteUserLanguages() {
        $model = UserLanguages::model()->findByPk($_POST['id']);
        $userid = $model->user_id;

        $cri = new CDbCriteria();
        $cri->condition = "user_language_id =" . $_POST['id'];
        ProfileLanguages::model()->deleteAll($cri);

        $cri1 = new CDbCriteria();
        $cri1->condition = "id =" . $_POST['id'];
        $model = UserLanguages::model()->deleteAll($cri1);

        $langmodel = Users::model()->findByPk($userid);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $langmodel->id, 'type' => $langmodel->type));
        $this->updatepercentage($userid, $profilemodel);
        echo $this->renderPartial('userLanguage', array('model' => $langmodel));
    }

    public function actionresume($slug) {
		
        $usermodel = $this->loadModelSlug($slug);
        $id = $usermodel->id;
       // $usermodel = Users::model()->findByPk($id);
     
        $mPDF1 = Yii::app()->ePdf->mpdf();
        $mPDF1 = Yii::app()->ePdf->mpdf('', 'A4');
       // $stylesheet = file_get_contents(Yii::app()->getBaseUrl(true) . '/themes/Dev/css/app.css');
       // $mPDF1->WriteHTML($stylesheet, 1);
        $mPDF1->WriteHTML($this->renderPartial('resume', array('model' => $usermodel), true));
        $mPDF1->Output();
        
        //$this->render('resume',array('model'=>$usermodel));
    }

    public function actionviewBCard($slug) {
        $usermodel = $this->loadModelSlug($slug);//Users::model()->findByPk($id);
        $id = $usermodel->id;
        $mPDF1 = Yii::app()->ePdf->mpdf();
        $mPDF1 = Yii::app()->ePdf->mpdf('c', 'A7-L');
        //     	$stylesheet = file_get_contents(Yii::app()->getBaseUrl(true) . '/themes/Dev/css/style.css');
        //     	$mPDF1->WriteHTML($stylesheet, 1);
        // $mPDF1->AddPage('L');
        // $mPDF1->SetDisplayMode('fullwidth');
        $mPDF1->WriteHTML($this->renderPartial('business-card', array('model' => $usermodel), true));
        $mPDF1->Output();
        
        // $this->render('businesscard',array('model'=>$usermodel));
    }

    public function actiondownloadBCard($id) {
        $usermodel = Users::model()->findByPk($id);

        $mPDF1 = Yii::app()->ePdf->mpdf();
        $mPDF1 = Yii::app()->ePdf->mpdf('c', 'A7-L');
        //     	$stylesheet = file_get_contents(Yii::app()->getBaseUrl(true) . '/themes/Dev/css/style.css');
        //     	$mPDF1->WriteHTML($stylesheet, 1);
//         $mPDF1->AddPage('L');
//         $mPDF1->SetDisplayMode('fullwidth');
        $mPDF1->WriteHTML($this->renderPartial('business-card', array('model' => $usermodel), true));
        $mPDF1->Output('business-card.pdf', EYiiPdf::OUTPUT_TO_DOWNLOAD);

        // $this->render('businesscard',array('model'=>$usermodel));
    }

    public function actionchangeUserSkills() {
        $skillid = $_POST['skill_id'];
        $userid = $_POST['user_id'];
        $action = $_POST['action'];

        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $userid));
        $profileid = $profilemodel['id'];
        if ($action == "add") {
            $psmodel = new ProfileSkills();
            $psmodel->profile_id = $profileid;
            $psmodel->skill_id = $skillid;
            $psmodel->save();

            $this->updatepercentage($userid,$profilemodel); 
        } else {
            ProfileSkills::model()->deleteAllByAttributes(array('profile_id' => $profileid, 'skill_id' => $skillid));
        }

        $pmodel = Users::model()->findByPk($userid);

        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $pmodel->id, 'type' => $pmodel->type));
        echo $this->renderPartial('userCareer', array('model' => $pmodel, 'profilemodel' => $profilemodel));
    }

    public function actionrenderUserSkills() {
        $userid = $_POST['user_id'];
        $pmodel = Users::model()->findByPk($userid);

        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $pmodel->id, 'type' => $pmodel->type));
        echo $this->renderPartial('userCareer', array('model' => $pmodel, 'profilemodel' => $profilemodel));
    }

    public function actionuserAccountInfo($slug) {
        $this->layout = 'main';
        
        $model = $this->loadModelSlug($slug);//Users::model()->findByPk($id);
        $id = $model->id;
        $profile = Profiles::model()->findByAttributes(array('owner_id' => $id));
        $pmodel = Profiles::model()->findByPk($profile['id']);
        $this->render('accountinfo', array('model' => $model, 'pmodel' => $pmodel));
    }

    public function actionupdateAccountInfo($id) {
       
        $this->layout ='main';
        $now = new DateTime();
        $model = Users::model()->findByPk($id);
        $pmodel = Profiles::model()->findByAttributes(array('owner_id' => $id));
        $profilemodel = Profiles::model()->findByPk($pmodel->id);
        $pwd = $model->password;        
        $model->attributes = $_POST['Users'];
        if($_POST['dob'] != "")
        {
            $d = str_replace('/', '-', $_POST["dob"]);
            $dobformat = date('Y-m-d', strtotime($d));
            $model->dob = $dobformat;
        }
        if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
            $model->setScenario('confirm');
        
        if($_POST['Users']['password'] != "")
            $model->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
        else 
            $model->password = $pwd;
        
        $model->type = 'candidate';
        $model->updated_at = $now->format('Y-m-d H:i:s');
        if ($model->save()) {
            $profilemodel->updated_at = $model->created_at;
            $profilemodel->type = $model->type;
            $profilemodel->slug = $_POST['Profiles']['slug'];
            $profilemodel->visibility = $_POST['status'];
            $profilemodel->save();
            
            Yii::app()->user->setFlash('success', 'Successfully updated user');
            $this->redirect($this->createUrl('users/edit', array('slug' => $profilemodel->slug)));
        }

        $this->render('accountinfo', array('model' => $model, 'pmodel' => $profilemodel));
    }

    public function actionchangeUserVisiblity() {
        $status = $_POST['status'];
        $model = Users::model()->findByPk($_POST['userid']);
        $pmodel = Profiles::model()->findByAttributes(array('owner_id' => $model->id));
        $profilemodel = Profiles::model()->findByPk($pmodel->id);
        
        
        $profilemodel->visibility = ($status == "Public" ? 0 : 1);
        $profilemodel->save();
        
    }

    public function actionchangeUserStatus() {
        $status = $_POST['status'];
        $model = Users::model()->findByPk($_POST['userid']);
        $pmodel = Profiles::model()->findByAttributes(array('owner_id' => $model->id, 'type' => $model->type));
        $profilemodel = Profiles::model()->findByPk($pmodel->id);
        $profilemodel->published = (strtolower($status) == 'active' ? 0 : 1);
        $profilemodel->save();
    }

    public function actionsaveCoverPhoto() {
        $profileid = $_POST['profileid'];
        $pmodel = Profiles::model()->findByPk($profileid);
        $now = new DateTime();
        if ($pmodel->cover_id > 0) {
            $mediamodel = Media::model()->findByPk($pmodel->cover_id);
            $mediamodel->updated_at = $now->format('Y-m-d H:i:s');
        } else {
            $mediamodel = new Media();
            $mediamodel->created_at = $now->format('Y-m-d H:i:s');
            $mediamodel->collection_name = "sample";
            $mediamodel->model_id = $profileid;
        }
        $filename = $_FILES['cover']['name'];
        $image_type = $_FILES['cover']['type'];
        $file = $_FILES['cover']['tmp_name'];
        $name = pathinfo($_FILES['cover']['name'], PATHINFO_FILENAME);

        $mediamodel->name = $name;
        $mediamodel->file_name = $filename;
        $mediamodel->size = $_FILES['cover']['size'];

        if ($mediamodel->save()) {
            $mediafolder = "images/media/" . $mediamodel->id . "/conversions";

            if (!file_exists($mediafolder)) {
                mkdir($mediafolder, 0777, true);
            }

            $imgprefix = "images/media/" . $mediamodel->id;
            $fullpath = "images/media/" . $mediamodel->id . "/" . $filename;

            Profiles::model()->updateAll(array('cover_id' => $mediamodel->id), 'id =:id', array(':id' => $profileid));

            if (move_uploaded_file($_FILES['cover']['tmp_name'], $fullpath)) {
                //create cover
                $coverwidth = 1000;
                $coverheight = 400;
                $coverdestpath = $imgprefix . "/conversions/cover.jpg";

                //create icon
                $iconwidth = 40;
                $iconheight = 40;
                $icondestpath = $imgprefix . "/conversions/icon.jpg";

                //create joblisting
                $jlwidth = 150;
                $jlheight = 85;
                $jldestpath = $imgprefix . "/conversions/joblisting.jpg";

                //create search
                $searchwidth = 167;
                $searchheight = 167;
                $searchdestpath = $imgprefix . "/conversions/search.jpg";

                //create thumb
                $thumbwidth = 126;
                $thumbheight = 126;
                $thumbdestpath = $imgprefix . "/conversions/thumb.jpg";

                if ($image_type == "image/jpg" || $image_type == "image/jpeg") {

                    $image = imagecreatefromjpeg($fullpath);

                    $this->resize_image($image, $image_type, $fullpath, $coverdestpath, $coverwidth, $coverheight); //create cover
                    $this->resize_image($image, $image_type, $fullpath, $icondestpath, $iconwidth, $iconheight); //create icon
                    $this->resize_image($image, $image_type, $fullpath, $jldestpath, $jlwidth, $jlheight); //create joblisting
                    $this->resize_image($image, $image_type, $fullpath, $searchdestpath, $searchwidth, $searchheight); //create search
                    $this->resize_image($image, $image_type, $fullpath, $thumbdestpath, $thumbwidth, $thumbheight); //create thumb
                } elseif ($image_type == "image/gif") {

                    $image = imagecreatefromgif($fullpath);

                    $this->resize_image($image, $image_type, $fullpath, $coverdestpath, $coverwidth, $coverheight); //create cover
                    $this->resize_image($image, $image_type, $fullpath, $icondestpath, $iconwidth, $iconheight); //create icon
                    $this->resize_image($image, $image_type, $fullpath, $jldestpath, $jlwidth, $jlheight); //create joblisting
                    $this->resize_image($image, $image_type, $fullpath, $searchdestpath, $searchwidth, $searchheight); //create search
                    $this->resize_image($image, $image_type, $fullpath, $thumbdestpath, $thumbwidth, $thumbheight); //create thumb
                } elseif ($image_type == "image/png") {

                    $image = imagecreatefrompng($fullpath);

                    $this->resize_image($image, $image_type, $fullpath, $coverdestpath, $coverwidth, $coverheight); //create cover
                    $this->resize_image($image, $image_type, $fullpath, $icondestpath, $iconwidth, $iconheight); //create icon
                    $this->resize_image($image, $image_type, $fullpath, $jldestpath, $jlwidth, $jlheight); //create joblisting
                    $this->resize_image($image, $image_type, $fullpath, $searchdestpath, $searchwidth, $searchheight); //create search
                    $this->resize_image($image, $image_type, $fullpath, $thumbdestpath, $thumbwidth, $thumbheight); //create thumb
                }
            }

            echo Yii::app()->baseUrl . "/" . $fullpath;
        }
    }

    function resize_image($image, $image_type, $fullpath, $destpath, $width, $height) {
        $size_arr = getimagesize($fullpath);

        list($width_orig, $height_orig, $img_type) = $size_arr;
        $ratio_orig = $width_orig / $height_orig;

        $tempimg = imagecreatetruecolor($width, $height);
        imagecopyresampled($tempimg, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

        if ($image_type == "image/jpg" || $image_type == "image/jpeg")
            imagejpeg($tempimg, $destpath);
        else if ($image_type == "image/png")
            imagepng($tempimg, $destpath);
        else if ($image_type == "image/gif")
            imagegif($tempimg, $destpath);
    }

    public function actiontabedit($id) {
        $model = Users::model()->findByPk($id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $model->id, 'type' => $model->type));
        $cri = new CDbCriteria();
        $cri->condition = "model_id = ".$model->id." and model_type like '%candidate%' and deleted_at is null";
        
        $addressmodel = Addresses::model()->find($cri);
        if($addressmodel == null)
            $addressmodel = new Addresses();
        $this->render('tabedit', array('model' => $model, 'profilemodel' => $profilemodel, 'addressmodel' => $addressmodel));
    }

    public function actiontabprofile($id) {
        $model = Users::model()->findByPk($id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $model->id, 'type' => $model->type));
        $cri = new CDbCriteria();
        $cri->condition = "model_id = ".$model->id." and model_type like '%candidate%' and deleted_at is null";
        $addressmodel = Addresses::model()->find($cri);
        if($addressmodel == null)
            $addressmodel = new Addresses();
        $this->render('tabprofile', array('model' => $model, 'profilemodel' => $profilemodel, 'addressmodel' => $addressmodel));
    }

    public function actiontabdocuments($id) {
        $model = Users::model()->findByPk($id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $model->id, 'type' => $model->type));
        $cri = new CDbCriteria();
        $cri->condition = "model_id = ".$model->id." and model_type like '%candidate%' and deleted_at is null";
        $addressmodel = Addresses::model()->find($cri);
        if($addressmodel == null)
            $addressmodel = new Addresses();
        $this->render('tabdocuments', array('model' => $model, 'profilemodel' => $profilemodel, 'addressmodel' => $addressmodel));
    }

    public function actiontabapplications($id) {
        $model = Users::model()->findByPk($id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $model->id, 'type' => $model->type));
        $cri = new CDbCriteria();
        $cri->condition = "model_id = ".$model->id." and model_type like '%candidate%' and deleted_at is null";
        $addressmodel = Addresses::model()->find($cri);
        if($addressmodel == null)
            $addressmodel = new Addresses();
        $this->render('tabapplications', array('model' => $model, 'profilemodel' => $profilemodel, 'addressmodel' => $addressmodel));
    }

//	function actionprofileEdit($id)
//	{
//	    $model = Users::model()->findByPk($id);
//	    $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$model->id,'type'=>$model->type));
//	    $addressmodel = Addresses::model()->findByAttributes(array('model_id'=>$model->id));
//	    $this->render('profileEdit',array('model'=>$model,'profilemodel'=>$profilemodel,'addressmodel'=>$addressmodel));
//	}

    function actiongetProfilePhoto() {
        echo $_FILES['profile']['tmp_name'];
    }

    function actionupdateProfileDetails($id) {
        
        $now = new DateTime();
        $usermodel = Users::model()->findByPk($id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));
        $cri = new CDbCriteria();
        $cri->condition = "model_id = ".$usermodel->id." and model_type like '%candidate%' and deleted_at is null";
        $addressmodel = Addresses::model()->find($cri);
        
        if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
          $usermodel->setScenario('confirm');
        
        if(isset($_POST['Users'])){
           $usermodel->attributes=$_POST['Users'];
        }
        if(isset ($_POST["Addresses"])){
            $addressmodel->attributes = $_POST["Addresses"]; 
        }
        if(isset ($_POST["Profiles"])){
            $profilemodel->attributes = $_POST["Profiles"];
        }     
        if(isset($_POST['Users']))
        {
             $valid = $usermodel->validate();
             $valid = $addressmodel->validate() && $valid;
             $valid = $profilemodel->validate() && $valid;
             
             if ($valid) {

                $pwd = $usermodel->password;
                if ($_POST["Users"]["password"] == "")
                    $usermodel->password = $pwd;
                else 
                    $usermodel->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
                $usermodel->type = "candidate";
                $d = str_replace('/', '-', $_POST["dob"]);
                $dobformat = date('Y-m-d', strtotime($d));
                $usermodel->dob = $dobformat;
                $usermodel->updated_at = $now->format('Y-m-d H:i:s');
                $usermodel->save();

                $profilemodel->slug = $_POST["Profiles"]['slug'];
                if ($_POST['photo_id'] > 0)
                    $profilemodel->photo_id = $_POST['photo_id'];
                $profilemodel->updated_at = $now->format('Y-m-d H:i:s');
                $profilemodel->save();
                
                $addressmodel->updated_at = $now->format('Y-m-d H:i:s');
                if($addressmodel->save())
                {
                     Yii::app()->user->setFlash('success', 'Successfully updated candidate user');
                     $this->redirect($this->createUrl('users/tabedit', array('id' => $usermodel->id)));
                }
                
            }
        }
        
        $this->render('tabedit', array('model' => $usermodel, 'profilemodel' => $profilemodel, 'addressmodel' => $addressmodel));
    }
    public function getExtension($str)
    {
        $i = strrpos($str,".");
        if (!$i) { return ""; }
        $l = strlen($str) - $i;
        $ext = substr($str,$i+1,$l);
        return $ext;
    }
    public function actionaddProfileVideo() {
        $now = new DateTime();
        $userid = $_POST['userid'];
        $name = str_replace(' ', '_', $_POST['video_name']);
        $videoname = str_replace(' ', '_', $_FILES['video']['name']);
        $size = $_FILES['video']['size'];
        $image_type = $_FILES['video']['type'];
        $tmp = $_FILES['video']['tmp_name'];
        $ext = $this->getExtension($videoname);
        
        $usermodel = Users::model()->findByPk($userid);
        $pmodel = Profiles::model()->findByAttributes(array('owner_id' => $userid, 'type' => $usermodel->type));
        
        Yii::import('application.extensions.amazon.components.*');
        
        $bucket = Yii::app()->params['AWS_BUCKET'];
        $s3 = new A2S3();
        
        $vname = pathinfo($name, PATHINFO_FILENAME);
        
        $actual_image_name = $vname.time() . "." . $ext;
        
        //        video validation
        if (strlen($name) > 0) {
            
            try {
                //                $s3->putObject(array(
                //                    'Bucket' => $bucket,
                //                    'Key' => $image_name_actual,
                //                    'SourceFile' => $tmp,
                //                    'StorageClass' => 'REDUCED_REDUNDANCY'
                //                ));
                
                $response =  $s3->putObject(array(
                    'SourceFile' => $tmp,
                    'Bucket' => $bucket,
                    'Key' => $actual_image_name,
                    'ACL' => 'public-read',
                    'x-amz-storage-class' => 'REDUCED_REDUNDANCY'
                ));
                
                //  $message = "S3 Upload Successful.";
                //  $s3file = 'http://' . $bucket . '.s3.amazonaws.com/' . $actual_image_name;
                //  echo $s3file;die();
                // echo "<img src='$s3file'/>";
                //  echo 'S3 File URL:' . $s3file;
                
                //    $vname = pathinfo($name, PATHINFO_FILENAME);
                
                $duration = 6;
                
                $videomodel = new Videos();
                $videomodel->model_id = $userid;
                $videomodel->video_id = $actual_image_name;
                $videomodel->name = $vname;
                $videomodel->duration = 0;
                $videomodel->model_type = $usermodel->type;
                $videomodel->category_id = 1;
                $videomodel->is_processed = 1;
                $videomodel->state = 1;
                $videomodel->status = "active";
                $videomodel->created_at = $now->format('Y-m-d H:i:s');
                if ($videomodel->save()) {
                    
                    $pvideomodel = new ProfileVideos();
                    $pvideomodel->profile_id = $pmodel->id;
                    $pvideomodel->video_id = $videomodel->id;
                    $pvideomodel->created_at = $now->format('Y-m-d H:i:s');
                    $pvideomodel->state = 1;
                    if($pvideomodel->save())
                    {
                        $this->updatepercentage($userid,$pmodel);

                        ProfileVideos::model()->updateAll(array('is_default' => 0), 'profile_id =:profile_id', array(':profile_id' => $pmodel->id));
                        ProfileVideos::model()->updateByPk($pvideomodel->id, array('is_default' => 1));
                    }
                    
                    $sec = 0;
                    
                    $fullpath = 'http://' . $bucket . '.s3.amazonaws.com/' . $actual_image_name;
                    
                    $cmd = "ffmpeg -i $fullpath 2>&1";
                    $ffmpeg = shell_exec($cmd);
                    
                    $search = "/Duration: (.*?)\./";
                    preg_match($search, $ffmpeg, $matches);
                    
                    if (isset($matches[1])) {
                        $data['duration'] = $matches[1];
                        $time_sec = explode(':', $data['duration']);
                        $sec = ($time_sec['0'] * 3600) + ($time_sec['1'] * 60) + $time_sec['2'];
                    }
                    
                    Videos::model()->updateByPk($videomodel->id, array('duration' => $sec));
                    
                    $mediamodel = new Media();
                    $mediamodel->model_id = $videomodel->id;
                    $mediamodel->disk = "media";
                    $mediamodel->model_type = $usermodel->type;
                    $mediamodel->collection_name = "video";
                    $mediamodel->name = $vname;
                    $mediamodel->file_name = $vname.".jpg";
                    $mediamodel->created_at = $now->format('Y-m-d H:i:s');
                    $mediamodel->size = (int)$sec;
                    if ($mediamodel->save()) {
                        //$filename = $vname;
                        
                        Media::model()->updateByPk($mediamodel->id, array('model_id' => $videomodel->id, 'size' => $sec));
                        
                        $iname = $vname . ".jpg";
                        $mediafolder = "images/media/" . $mediamodel->id."/conversions/";
                        
                        if (!file_exists($mediafolder)) {
                            mkdir($mediafolder, 0777, true);
                        }
                        
                        $imgfullpath = "images/media/" . $mediamodel->id . "/" . $iname;
                        $imgprefix = "images/media/" . $mediamodel->id;
                        
                        $ffurl = "ffmpeg -i $fullpath -ss 00:00:2 -vframes 1 $imgfullpath";
                        
                        $process = exec($ffurl);
                        
                        $jlwidth = 350;
                        $jlheight = 190;
                        $jldestpath = $imgprefix . "/conversions/profile.jpg";
                        
                        //create search
                        $searchwidth = 167;
                        $searchheight = 167;
                        $searchdestpath = $imgprefix . "/conversions/search.jpg";
                        
                        //create thumb
                        $thumbwidth = 126;
                        $thumbheight = 126;
                        $thumbdestpath = $imgprefix . "/conversions/thumb.jpg";
                        $image_type = "image/jpg";
                        
                        $image = imagecreatefromjpeg($imgfullpath);
                        
                        $this->resize_image($image, $image_type, $imgfullpath, $jldestpath, $jlwidth, $jlheight); //create joblisting
                        $this->resize_image($image, $image_type, $imgfullpath, $searchdestpath, $searchwidth, $searchheight); //create search
                        $this->resize_image($image, $image_type, $imgfullpath, $thumbdestpath, $thumbwidth, $thumbheight); //create thumb
                        
                        
                    }
                }
                
                
            } catch (S3Exception $e) {
                // Catch an S3 specific exception.
                echo $e->getMessage();
            }
        }
        //echo $this->renderPartial('userVideos', array('model' => $usermodel, 'profilemodel' => $pmodel));
    }
    /*public function actionaddProfileVideo() {
        $now = new DateTime();
        $userid = $_POST['userid'];
        $name = $_POST['video_name'];
        $size = $_FILES['video']['size'];
        $image_type = $_FILES['video']['type'];

        $usermodel = Users::model()->findByPk($userid);
        $pmodel = Profiles::model()->findByAttributes(array('owner_id' => $userid, 'type' => $usermodel->type));

        require_once Yii::app()->basePath . '/extensions/vimeo/autoload.php';

        //ezhil new
//         $client_id = "37d94df2c3ed974c5ef3731b6208fa7b18fb5ff5";
//         $client_secret = "AgASOUTy55PtV9np6qGnwbL5fjNrLTzj8mrFBUUXJfeOz1U/dkBRuPv0HpClA7jFqHgPcHdfTTIay3oz9/FEvlPIsAPtsBCk/OguikJIAlT85/wU4Cly71gfPVmS8GDd";
        //live credentials
       // $client_id = "698fd10c1f51dcb9777207a3241e9ef8e07127fc";
     //   $client_secret = "4Yl0Pmx1e2IWPqN625FxVuWdd6DoZtJkyuuTncIpRZDo4NG4tiH4GpJzTj/KA77OICWlwzamLK5aN1yj6EN9/9zgcZ6r/TlvxEl8wealfABOS5H994z/83A+Cy7FJiTK";
       //ayyanar
        $client_id = "4b7c96cce1b91f014fda20977a566fb3f2fad55b";
        $client_secret = "0YCMBrjyjvYcg1NNnADG8NcqUz4tlefTZwWgo710izgAg61shhbA40cBNUPSpESkBvk/isCpKSVzbhw4R9Lo3RU7T7CyO9k8a9LkyGZfwA4e08fO7v8AHmAcIpwWDkk6";
        $lib = new \Vimeo\Vimeo($client_id, $client_secret);

        $token = $lib->clientCredentials(array('public,upload'));

        //ayyanar
         $lib->setToken('b770581b1abc8b52c32078b2d2c39e57');
        
        //live
        //$lib->setToken('8a3759b4f04c3e6c5deae35e130673a9');
        
        //ezhil
//        $lib->setToken('d013c75b4dcee8ed89486d7f9f44fb32');

        $response = $lib->upload($_FILES['video']['tmp_name'], true);
// 	    $response = $lib->replace('/videos/241658003', $_FILES['video']['tmp_name'], false);
        if ($response) {

            $response1 = $lib->request($response, array('name' => $name), 'PATCH');

            $url = $response1['body']['link'];
            $videoid = substr($url, 0, strpos($url, "/videos/"));

            $str = $response;
            $del = "/videos/";
            $pos = strpos($str, $del);
            $videoid = substr($str, $pos + strlen($del), strlen($str) - 1);

            
            $json_url = 'http://vimeo.com/api/v2/video/'.$videoid.'.xml';
            $ch = curl_init($json_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            $data = curl_exec($ch);
            curl_close($ch);
            $data = new SimpleXmlElement($data, LIBXML_NOCDATA);
            $duration = isset($data->video->duration) ? $data->video->duration : 0;
            
            $vname = pathinfo($name, PATHINFO_FILENAME);
            
            $videomodel = new Videos();
            $videomodel->model_id = $userid;
            $videomodel->video_id = $videoid;
            $videomodel->name = $vname;
            $videomodel->duration = (int)$duration;
            $videomodel->model_type = $usermodel->type;
            $videomodel->category_id = 1;
            $videomodel->is_processed = 1;
            $videomodel->state = 1;
            $videomodel->status = "active";
            $videomodel->created_at = $now->format('Y-m-d H:i:s');
            if ($videomodel->save()) {
                
                $pvideomodel = new ProfileVideos();
                $pvideomodel->profile_id = $pmodel->id;
                $pvideomodel->video_id = $videomodel->id;
                $pvideomodel->created_at = $now->format('Y-m-d H:i:s');
                $pvideomodel->state = 1;
                if ($pvideomodel->save()) {
                    $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$videoid.php"));
                    $imgpath = $hash[0]['thumbnail_medium'];
                   // $del = "http://i.vimeocdn.com/video/";
                   // $pos = strpos($imgpath, $del);
                    $iname = base64_encode($videoid);//substr($imgpath, $pos + strlen($del), strlen($imgpath) - 1);
                    $img_name = base64_encode($videoid).".jpg";
                    
                    
                    $mediamodel = new Media();
                    $mediamodel->model_id = $videomodel->id;
                    $mediamodel->disk = "media";
                    $mediamodel->model_type = $usermodel->type;
                    $mediamodel->collection_name = "video";
                    $mediamodel->name = base64_encode($videoid);
                    $mediamodel->file_name = base64_encode($videoid).".jpg";
                    $mediamodel->created_at = $now->format('Y-m-d H:i:s');
                    $mediamodel->size = (int)$duration;
                    if ($mediamodel->save()) {
                        $filename = $img_name;
                        $mediafolder = "images/media/" . $mediamodel->id . "/conversions";

                        $imgprefix = "images/media/" . $mediamodel->id;
                        $fullpath = "images/media/" . $mediamodel->id . "/" .$img_name;

                        if (!file_exists($mediafolder)) {
                            mkdir($mediafolder, 0777, true);
                        }

                        if (copy($imgpath, $fullpath)) {

                            //create joblisting
                            $jlwidth = 350;
                            $jlheight = 190;
                            $jldestpath = $imgprefix . "/conversions/profile.jpg";

                            //create search
                            $searchwidth = 167;
                            $searchheight = 167;
                            $searchdestpath = $imgprefix . "/conversions/search.jpg";

                            //create thumb
                            $thumbwidth = 126;
                            $thumbheight = 126;
                            $thumbdestpath = $imgprefix . "/conversions/thumb.jpg";

                            if ($image_type == "image/jpg" || $image_type == "image/jpeg") {

                                $image = imagecreatefromjpeg($fullpath);

                                //$this->resize_image($image, $image_type, $fullpath, $coverdestpath, $coverwidth, $coverheight); //create cover
                                //$this->resize_image($image, $image_type, $fullpath, $icondestpath, $iconwidth, $iconheight); //create icon
                                $this->resize_image($image, $image_type, $fullpath, $jldestpath, $jlwidth, $jlheight); //create joblisting
                                $this->resize_image($image, $image_type, $fullpath, $searchdestpath, $searchwidth, $searchheight); //create search
                                $this->resize_image($image, $image_type, $fullpath, $thumbdestpath, $thumbwidth, $thumbheight); //create thumb
                            } elseif ($image_type == "image/gif") {

                                $image = imagecreatefromgif($fullpath);
 
                                //$this->resize_image($image, $image_type, $fullpath, $coverdestpath, $coverwidth, $coverheight); //create cover
                               // $this->resize_image($image, $image_type, $fullpath, $icondestpath, $iconwidth, $iconheight); //create icon
                                $this->resize_image($image, $image_type, $fullpath, $jldestpath, $jlwidth, $jlheight); //create joblisting
                                $this->resize_image($image, $image_type, $fullpath, $searchdestpath, $searchwidth, $searchheight); //create search
                                $this->resize_image($image, $image_type, $fullpath, $thumbdestpath, $thumbwidth, $thumbheight); //create thumb
                            } elseif ($image_type == "image/png") {

                                $image = imagecreatefrompng($fullpath);

                                //$this->resize_image($image, $image_type, $fullpath, $coverdestpath, $coverwidth, $coverheight); //create cover
                                //$this->resize_image($image, $image_type, $fullpath, $icondestpath, $iconwidth, $iconheight); //create icon
                                $this->resize_image($image, $image_type, $fullpath, $jldestpath, $jlwidth, $jlheight); //create joblisting
                                $this->resize_image($image, $image_type, $fullpath, $searchdestpath, $searchwidth, $searchheight); //create search
                                $this->resize_image($image, $image_type, $fullpath, $thumbdestpath, $thumbwidth, $thumbheight); //create thumb
                            }
                        } 
                    }
                }
            } 
        }

        echo $this->renderPartial('userVideos', array('model' => $usermodel, 'profilemodel' => $pmodel));
    }*/

    public function actionupdateProfileVideoName() {
        $videoid = $_POST['id'];
        $videoname = $_POST['name'];

        $now = new DateTime();
        $vmodel = Videos::model()->findByPk($videoid);
        $vmodel->name = $videoname;
        $vmodel->updated_at = $now->format('Y-m-d H:i:s');
        $vmodel->save();

        $userid = $vmodel->model_id;
        $usermodel = Users::model()->findByPk($userid);
        $pmodel = Profiles::model()->findByAttributes(array('owner_id' => $userid, 'type' => $usermodel->type));

        echo $this->renderPartial('userVideos', array('model' => $usermodel, 'profilemodel' => $pmodel));
    }

    public function actionprofileVideoDefault() {

        $videoid = $_POST['id'];
        $vmodel = Videos::model()->findByPk($videoid);
        $userid = $vmodel->model_id;
        $usermodel = Users::model()->findByPk($userid);
        $pmodel = Profiles::model()->findByAttributes(array('owner_id' => $userid, 'type' => $usermodel->type));

        ProfileVideos::model()->updateAll(array('is_default' => 0), 'profile_id =:profile_id', array(':profile_id' => $pmodel->id));

        ProfileVideos::model()->updateAll(array('is_default' => 1), 'video_id =:video_id', array(':video_id' => $videoid));

        echo $this->renderPartial('userVideos', array('model' => $usermodel, 'profilemodel' => $pmodel));
    }

    public function actionprofileVideoDelete() {

        $videoid = $_POST['id'];
        $vmodel = Videos::model()->findByPk($videoid);
        $userid = $vmodel->model_id;

        $vmodel = Videos::model()->deleteByPk($videoid);

        $usermodel = Users::model()->findByPk($userid);
        $pmodel = Profiles::model()->findByAttributes(array('owner_id' => $userid, 'type' => $usermodel->type));

        ProfileVideos::model()->deleteAllByAttributes(array('video_id' => $videoid));

        $pmodel = Profiles::model()->findByAttributes(array('owner_id' => $userid, 'type' => $usermodel->type));
        $this->updatepercentage($userid, $pmodel);

        echo $this->renderPartial('userVideos', array('model' => $usermodel, 'profilemodel' => $pmodel));
    }

    public function actionprofileVideoPublish() {

        $videoid = $_POST['id'];
        $publishstatus = $_POST['status'];

        if (trim($publishstatus) == "Published")
            $state = 0;
        else
            $state = 1;

        ProfileVideos::model()->updateAll(array('state' => $state), 'video_id =:video_id', array(':video_id' => $videoid));

        $vmodel = Videos::model()->findByPk($videoid);
        $userid = $vmodel->model_id;

        $usermodel = Users::model()->findByPk($userid);
        $pmodel = Profiles::model()->findByAttributes(array('owner_id' => $userid, 'type' => $usermodel->type));

        echo $this->renderPartial('userVideos', array('model' => $usermodel, 'profilemodel' => $pmodel));
    }

    public function actiondeleteUsers() {
        $ids = implode(',', $_POST['ids']);
        $now = new DateTime();

        $cri = new CDbCriteria();
        $cri->condition = "id in (" . $ids . ")";
        Users::model()->updateAll(array('deleted_at' => $now->format('Y-m-d H:i:s')), $cri);

        $cri1 = new CDbCriteria();
        $cri->condition = "owner_id in (" . $ids . ")";
// 	    $cri1->addInCondition('owner_id', array($ids));
        Profiles::model()->updateAll(array('deleted_at' => $now->format('Y-m-d H:i:s')), $cri1);
    }

    public function actionexportCSV($trial, $paid, $basic) {
        Yii::import('ext.ECSVExport');
        $criteria = new CDbCriteria();
        $criteria->select = "CONCAT_WS(' ', forenames, surname) as name,email,created_at";
        if (Yii::app()->user->getState('role') == 'admin') {
            $criteria->condition = "type like 'candidate' AND deleted_at is null";
        }
        if ($trial == 1)
            $criteria->addCondition('trial_ends_at is not null');
        else if ($paid == 1)
            $criteria->addCondition('stripe_id > 0');
        else if ($basic == 1)
            $criteria->addCondition('trial_ends_at is null and stripe_id is null');

        // CActiveDataProvider
        $dataProvider = new CActiveDataProvider('Users', array(
            'criteria' => $criteria,
        ));

        $csv = new ECSVExport($dataProvider);
        $content = $csv->toCSV();
        Yii::app()->getRequest()->sendFile('export.csv', $content, "text/csv", false);
    }

    public function actiondashboard() {
        $this->layout = 'admin';
        $this->dashboard = 1;
        $model = new Users();
        $this->render('dashboard', array(
            'model' => $model
        ));
    }
    
    public function actiondeleteCandidate($id) {

        $now = new DateTime();

        $user = Users::model()->deleteAllByAttributes(array('id'=>$id));
        $profile = Profiles::model()->deleteAllByAttributes(array('owner_id'=>$id,'owner_type'=>'candidate'));
        $address = Addresses::model()->deleteAllByAttributes(array('model_id'=>$id,'model_type'=>'candidate'));
        
        Yii::app()->user->setFlash('success', 'Successfully candidate deleted.');
        $this->redirect($this->createUrl('users/admin',array('trial'=>1)));
    }

    public function actionactivateUsers() {
        $ids = implode(',', $_POST['ids']);

        $now = new DateTime();

        $cri = new CDbCriteria();
        $cri->condition = "id in (" . $ids . ")";
        Users::model()->updateAll(array('status' => 'active'), $cri);
    }

    public function actionblockUsers() {
        $ids = implode(',', $_POST['ids']);

        $now = new DateTime();

        $cri = new CDbCriteria();
        $cri->condition = "id in (" . $ids . ")";
        Users::model()->updateAll(array('status' => 'blocked'), $cri);
    }

    public function actionfavourites($id) {
        $this->layout = "main";
        $profilemodel = Profiles::model()->findByPk($id);
        $model = Users::model()->findByPk($profilemodel->owner_id);
        $this->render('favourites', array(
            'model' => $model, 'profilemodel' => $profilemodel
        ));
    }

    public function actioncreateAdmin() {
        $model = new Users();
        $addressmodel = new Addresses();
        $this->render('admindetails', array(
            'model' => $model, 'addressmodel' => $addressmodel
        ));
    }

    public function actionmanageUsers() {
        $model = Users::model();

        $namesearch = 0;
        $fullname = '';
        $userarr = array();
        if (isset($_GET['Users']))
            $model->attributes = $_GET['Users'];

        if (isset($_GET['fullname']) && $_GET['fullname'] != "") {
            $namesearch = 1;
            $fullname = $_GET['fullname'];
            $cri = new CDbCriteria();
            $cri->condition = "concat_ws(' ',forenames,surname) like '%" . $fullname . "%' and type = 'admin' and deleted_at is null";

            $searchusers = Users::model()->findAll($cri);
            foreach ($searchusers as $users)
                $userarr[] = $users['id'];
        }

        $this->render('manageusers', array(
            'model' => $model, 'namesearch' => $namesearch, 'namearray' => $userarr, 'fullname' => $fullname
        ));
    }

    public function actioneditManageUser($id) {
        $model = Users::model()->findByPk($id);
        $cri = new CDbCriteria();
        $cri->condition = "model_id = ".$model->id." and model_type like '%admin%' and deleted_at is null";
        
        $addressmodel = Addresses::model()->find($cri);
        if ($addressmodel == null)
            $addressmodel = new Addresses();

        $this->render('admindetails', array(
            'model' => $model, 'addressmodel' => $addressmodel
        ));
    }

    public function actionsaveAdmin() {
        
        $now = new DateTime();
        
        $model = new Users();
        $addressmodel = new Addresses();
        
        if($_POST['Users']['password'] != $_POST['Users']['confirmpassword'])
            $model->setScenario('confirm');
        
        if (isset($_POST['Users'])) {
            $model->attributes = $_POST['Users'];
        }
        
         if (isset($_POST['Addresses'])) {
            $addressmodel->attributes = $_POST['Addresses'];
        }
        
        if (isset($_POST['Users'])) {
            
            $valid = $model->validate();
            $valid = $addressmodel->validate() && $valid;
            
            if($valid)
            {
//                insert user details
                $model->type = "admin";
                $d = str_replace('/', '-', $_POST["dob"]);
                $dobformat = date('Y-m-d', strtotime($d));
                $model->dob = $dobformat;
                $model->created_at = $now->format('Y-m-d H:i:s');
                $model->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
                $model->save();
                
//                address insert
                $addressmodel->model_id = $model->id;
                $addressmodel->model_type = "Admin";
                $addressmodel->created_at = $now->format('Y-m-d H:i:s');
                if ($addressmodel->save())
                {  
                    Yii::app()->user->setFlash('success', 'Successfully created user');
                    $this->redirect($this->createUrl('users/manageUsers')); 
                }
            }
            
        }

        $this->render('admindetails', array(
            'model' => $model,
            'addressmodel' => $addressmodel
        ));
    }

    public function actionupdateAdmin($id) {
        
        $now = new DateTime();
        
        $model = Users::model()->findByPk($id);
        $cri = new CDbCriteria();
        $cri->condition = "model_id = ".$id." and model_type like '%admin%' and deleted_at is null";
        $addressmodel = Addresses::model()->find($cri);
        if ($addressmodel == null) {
               $addressmodel = new Addresses();
        }
        
        $pwd = $model->password;
        
         if (isset($_POST['Users'])) {
              $model->attributes = $_POST['Users'];
         }
         if(isset($_POST['Addresses'])){
             $addressmodel->attributes = $_POST['Addresses'];
         }
        
         if (isset($_POST['Users'])) {
             
             $valid = $model->validate();
             $valid = $addressmodel->validate() && $valid;
             
             if($valid)
             {
                if ($_POST['Users']['password'] == "")
                $model->password = $pwd;
                else 
                    $model->password = password_hash($_POST['Users']['password'], 1,['cost' => 10]);
                $d = str_replace('/', '-', $_POST["dob"]);
                $dobformat = date('Y-m-d', strtotime($d));
                //  $dobformat = date_format(new DateTime($_POST["dob"]),"Y-m-d");
                $model->dob = $dobformat;
                $model->created_at = $now->format('Y-m-d H:i:s');
                $model->save();
                
                $addressmodel->updated_at = $now->format('Y-m-d H:i:s');
                $addressmodel->model_id = $model->id;
                $addressmodel->model_type = "Admin";
                if ($addressmodel->save())
                {    
                    Yii::app()->user->setFlash('success', 'Successfully updated user');
                    $this->redirect($this->createUrl('users/manageUsers'));
                }

             }
             
         }
        $this->render('admindetails', array(
            'model' => $model,
            'addressmodel' => $addressmodel
        ));
    }

    public function actiondeleteAdmin() {
        $ids = implode(',', $_POST['ids']);
        $now = new DateTime();

        $cri = new CDbCriteria();
        $cri->condition = "id in (" . $ids . ")";
        Users::model()->deleteAll($cri);
    }

     public function actionsendMessage() {
        $now = new DateTime();
        $conmodel = new Conversations();
        $conmodel->subject = $_POST['subject'];
        $conmodel->created_at = $now->format('Y-m-d H:i:s');
        $conmodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($conmodel->save()) {
            $usermodel = Users::model()->findByPk(Yii::app()->user->getState('userid'));
            $conmsgmodel = new ConversationMessages();
            $conmsgmodel->conversation_id = $conmodel->id;
            $conmsgmodel->user_id = Yii::app()->user->getState('userid');
            $conmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
            $conmsgmodel->message = $_POST['message'];
            $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
            $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
            if ($conmsgmodel->save()) {
                $conmemmodel_sender = new ConversationMembers();
                $conmemmodel_sender->conversation_id = $conmodel->id;
                $conmemmodel_sender->user_id = Yii::app()->user->getState('userid');
                $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
                $conmemmodel_sender->last_read = NULL;
                $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                if ($conmemmodel_sender->save()) {
                    $usermodel1 = Users::model()->findByPk( $_POST['user_id']);
                    
                    //check recruitment candidate
                    $agecanmodel = AgencyCandidates::model()->findByAttributes(array('user_id'=>$_POST['user_id']));
                    if($agecanmodel != null)
                    {
                        $user_name = "XXXX";
                        
                    }
                    else {
                        $user_name = $usermodel1->forenames." ".$usermodel1->surname;
                    }
                    //ends
                    
                    $conmemmodel_rec = new ConversationMembers();
                    $conmemmodel_rec->conversation_id = $conmodel->id;
                    $conmemmodel_rec->user_id = $_POST['user_id'];
                    $conmemmodel_rec->name = $user_name;
                    $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                    $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                    if ($conmemmodel_rec->save()) {
                        $actmodel_sender = new Activities();
                        $actmodel_sender->user_id = Yii::app()->user->getState('userid');
                        $actmodel_sender->type = "message";
                        $actmodel_sender->model_id = $conmsgmodel->id;
                        $actmodel_sender->model_type = "ConversationMessage";
                        $actmodel_sender->subject = "You sent a message in conversation: " . $_POST['subject'];
                        $actmodel_sender->message = $_POST['message'];
                        $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                        $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                        if ($actmodel_sender->save()) {
                            $actmodel_rec = new Activities();
                            $actmodel_rec->user_id = $_POST['user_id'];
                            $actmodel_rec->type = "message";
                            $actmodel_rec->model_id = $conmsgmodel->id;
                            $actmodel_rec->model_type = "ConversationMessage";
                            $actmodel_rec->subject = "You received a message in conversation: " . $_POST['subject'];
                            $actmodel_rec->message = $_POST['message'];
                            $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                            $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                            $actmodel_rec->save();
                        }
                    }
                }
            }
        }
    }

    public function actioncheckSlug() {
        $pmodel = Profiles::model()->countByAttributes(array('slug' => $_POST['slug'], 'deleted_at' => null));
        if ($pmodel > 0)
            echo -1;
        else
            echo 1;
    }

    public function actionInbox($id) {
        $this->layout = "main";
        $model = Users::model()->findByPk($id);
        $this->render("inbox", array('model' => $model));
    }

    public function actiongetInboxMessages() {
        $this->layout = "main";
        $convmodel = Conversations::model()->findByPk($_POST['id']);
        
        $now = new DateTime();
         $ccri = new CDbCriteria();
         $ccri->condition = 'conversation_id ='.$convmodel->id;
         ConversationMembers::model()->updateAll(array('last_read'=>$now->format('Y-m-d H:i:s')),$ccri);

        $this->renderPartial('inbox_messages', array('convmodel' => $convmodel, 'to_user' => $_POST['msg_to']));
    }

     public function actionsaveMessage() {
        $this->layout = "main";
        $conid = $_POST['id'];
        $fromuser = Yii::app()->user->getState('userid');
        $touser = $_POST['msg_to'];
        $msg = $_POST['message'];
        $now = new DateTime();

        $convmodel = Conversations::model()->findByPk($_POST['id']);
        $usermodel = Users::model()->findByPk($fromuser);

        //check recruitment candidate
        $agecanmodel = AgencyCandidates::model()->findByAttributes(array('user_id'=>$fromuser));
        if($agecanmodel != null)
        {
            $user_name = "XXXX";
            
        }
        else {
            $user_name = $usermodel->forenames." ".$usermodel->surname;
        }
        //ends
        
        $convmsgmodel = new ConversationMessages();
        $convmsgmodel->conversation_id = $conid;
        $convmsgmodel->user_id = $fromuser;
        $convmsgmodel->name = $user_name;
        $convmsgmodel->message = $msg;
        $convmsgmodel->created_at = $now->format('Y-m-d H:i:s');
        $convmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
        if ($convmsgmodel->save()) {
            $conmemmodel_sender = new ConversationMembers();
            $conmemmodel_sender->conversation_id = $convmodel->id;
            $conmemmodel_sender->user_id = Yii::app()->user->getState('userid');
            $conmemmodel_sender->name = $user_name;
            $conmemmodel_sender->last_read = NULL;
//            $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
            $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
            $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
            if ($conmemmodel_sender->save()) {
                $usermodel1 = Users::model()->findByPk($_POST['msg_to']);
                $conmemmodel_rec = new ConversationMembers();
                $conmemmodel_rec->conversation_id = $convmodel->id;
                $conmemmodel_rec->user_id = $_POST['msg_to'];
                $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
                $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                if ($conmemmodel_rec->save()) {
                    $activitymodel = new Activities();
                    $activitymodel->user_id = $fromuser;
                    $activitymodel->type = "message";
                    $activitymodel->model_id = $convmsgmodel->id;
                    $activitymodel->model_type = "App\Messaging\ConversationMessage";
                    $activitymodel->subject = "You sent a message in conversation: " . $convmodel->subject;
                    $activitymodel->message = $msg;
                    $activitymodel->created_at = $now->format('Y-m-d H:i:s');
                    $activitymodel->updated_at = $now->format('Y-m-d H:i:s');
                    $activitymodel->save();
        
                    $activitymodel = new Activities();
                    $activitymodel->user_id = $touser;
                    $activitymodel->type = "message";
                    $activitymodel->model_id = $convmsgmodel->id;
                    $activitymodel->model_type = "App\Messaging\ConversationMessage";
                    $activitymodel->subject = "You sent a message in conversation: " . $convmodel->subject;
                    $activitymodel->message = $msg;
                    $activitymodel->created_at = $now->format('Y-m-d H:i:s');
                    $activitymodel->updated_at = $now->format('Y-m-d H:i:s');
                    $activitymodel->save();
             }
            }
        }
        Yii::app()->user->setFlash('success', 'Message sent successfully');
        $this->renderPartial('inbox_messages', array('convmodel' => $convmodel, 'to_user' => $_POST['msg_to']));
    }
    
    
     /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionupgrade($target = '') {
        $this->layout = 'main';
        $this->render('upgrade',array('target'=>$target));
    }
    
     /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionmysubscription() {
        $this->layout = 'main';
        $this->render('subscription');
    }
    
    public function actionupgradestore($target) {
        //echo var_dump($_POST);
        $now = new DateTime();
        $model = Users::model()->findByPk(array('id'=>Yii::app()->user->getState('userid')));
        
        require Yii::app()->basePath . '/extensions/stripe/init.php';
	Stripe::setApiKey(Yii::app()->params['SECRET_Key']);
                
        $plan = $_POST['subscription_plan'];
        $token = $_POST['stripeToken'];
        
        $subscription_ends_at = Carbon::now()->addYear()->toDateTimeString();
        
        $planarr = Plan::retrieve($plan);
        
        $amount = $planarr->amount;
        $planname = $planarr->name;
        $currency = $planarr->currency;
     
     //        create subscriber
         $customer = Customer::create(array(
            'source'   => $token,
            'email'    => $model->email,
            'plan'     => $plan,
            'description' => $model->forenames." ".$model->surname
        ));
                 
        $model->stripe_active = 1;
        $model->stripe_id = $customer->id;
        $model->stripe_subscription = $customer->subscriptions->data[0]->id;
        $model->stripe_plan = $plan;
        $model->last_four = $_POST['cvc'];
        $model->subscription_ends_at = $subscription_ends_at;
        $model->updated_at = $now->format('Y-m-d H:i:s');
        $model->save(false);
        
        Yii::app()->user->setFlash('success', 'Successfully updated your membership');
        if($target == ""){
            $pmodel = Profiles::model()->findByAttributes(array('owner_id'=>$model->id));
            $this->redirect(Yii::app()->createUrl('users/edit', array('slug' => $pmodel->slug)));
        }
        else
            $this->redirect($this->createUrl($target));
        
        
    }
    
    
    public function actioncardupdate() {
        
          //echo var_dump($_POST);
        $now = new DateTime();
        $model = Users::model()->findByPk(array('id'=>Yii::app()->user->getState('userid')));
        
        require Yii::app()->basePath . '/extensions/stripe/init.php';
	Stripe::setApiKey(Yii::app()->params['SECRET_Key']);
                
        $token = $_POST['stripeToken'];
        
        $cu = Customer::retrieve($model->stripe_id);
        $cu->source = $token; // obtained with Stripe.js
        $cu->save();
        
        $model->last_four = $_POST['cvc'];
        $model->updated_at = $now->format('Y-m-d H:i:s');
        $model->save();
        
        Yii::app()->user->setFlash('success', 'Your card information has been successfully updated');
        $this->redirect(Yii::app()->createUrl('users/mysubscription'));

    }
    
    public function actionprofileViews($id)
    {
        $this->layout = "main";
        $profilemodel = Profiles::model()->findByPk($id);
        $model = Users::model()->findByPk($profilemodel->owner_id);
        $this->render('profile_views', array(
            'model' => $model, 'profilemodel' => $profilemodel
        ));
    }
    public function actionfilterViewers()
    {        
       
        $profilemodel = Profiles::model()->findByPk($_POST['id']);
        
        $cri = new CDbCriteria();
        if($_POST['time'] == "week")
        {
            $cri->condition = "created_at between date_sub(now(),INTERVAL 1 WEEK) and now()";
        }
        else if($_POST['time'] == "month")
        {
            $cri->condition = "created_at between date_sub(now(),INTERVAL 1 MONTH) and now()";
        }
        else if($_POST['time'] == "year")
        {
            $cri->condition = "created_at between date_sub(now(),INTERVAL 1 YEAR) and now()";
        }        
        
        $cri->addCondition("profile_id =".$profilemodel->id);
        
        $pvmodel = ProfileViews::model()->findAll($cri);
        
        $this->renderPartial('viewerslist', array(
                 'pvmodel' => $pvmodel
        ));
    }
    public function actiongetPercentage($id)
    {
        $profilemodel = Profiles::model()->findByPk($id);
        
        $percentage = 0;
        
        $statementdocs = UserStatement::model()->findAllByAttributes(array('user_id' => $profilemodel->owner_id));
        
        if (count($statementdocs) > 0)
            $percentage += '13';
        
        $profiledocs = ProfileDocuments::model()->findAllByAttributes(array('profile_id' => $profilemodel->id));
        if (count($profiledocs) > 0)
            $percentage += '13';
            
        $profileexp = ProfileExperiences::model()->findAllByAttributes(array('profile_id' => $profilemodel->id));
        if (count($profileexp) > 0)
            $percentage += '13';
                
        $profilehob = ProfileHobbies::model()->findAllByAttributes(array('profile_id' => $profilemodel->id));
        if (count($profilehob) > 0)
            $percentage += '13';
                    
        $profilelang = ProfileLanguages::model()->findAllByAttributes(array('profile_id' => $profilemodel->id));
        if (count($profilelang) > 0)
            $percentage += '12';
                        
        $profilequal = ProfileQualifications::model()->findAllByAttributes(array('profile_id' => $profilemodel->id));
        if (count($profilequal) > 0)
            $percentage += '12';
                            
        $profileskill = ProfileSkills::model()->findAllByAttributes(array('profile_id' => $profilemodel->id));
        if (count($profileskill) > 0)
            $percentage += '12';
                                
        $profilevid = ProfileVideos::model()->findAllByAttributes(array('profile_id' => $profilemodel->id));
        if (count($profilevid) > 0)
            $percentage += '12';
       
        if($percentage > 45 && $percentage < 60)
        {
            $umodel = Users::model()->findByAttributes(array('owner_id'=>$profilemodel->id));
            $body = $this->renderPartial("//emails/profile-half", array('pmodel' => $profilemodel,'model'=>$umodel), true);
            // Send 50% profile completion notifcation to candidate
            Yii::import('application.extensions.phpmailer.JPhpMailer');
            $mail = new JPhpMailer;
            $mail->Host = 'mail.cvvid.com';
            $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
            $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
            $mail->SMTPSecure = 'ssl';
            $mail->SetFrom('mail@cvvid.com', 'CVVID');
            $mail->Subject = "You have achieved 50% profile completion!";
            $mail->MsgHTML($body);
            $mail->AddAddress($umodel->email, $umodel->forenames);
            $mail->Send();
        }
        else if($percentage == 100)
        {
            $umodel = Users::model()->findByAttributes(array('owner_id'=>$profilemodel->id));
            $body = $this->renderPartial("//emails/profile-complete", array('pmodel' => $profilemodel,'model'=>$umodel), true);
            // Send 100% profile completion notifcation to candidate
            Yii::import('application.extensions.phpmailer.JPhpMailer');
            $mail = new JPhpMailer;
            $mail->Host = 'mail.cvvid.com';
            $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
            $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
            $mail->SMTPSecure = 'ssl';
            $mail->SetFrom('mail@cvvid.com', 'CVVID');
            $mail->Subject = "Congratulations! You�ve completed your profile.";
            $mail->MsgHTML($body);
            $mail->AddAddress($umodel->email, $umodel->forenames);
            $mail->Send();
        }
        echo  $this->renderPartial('profileCompletion', array(
            'profilemodel' => $profilemodel,'percentage'=>$percentage
        ));
    }
   public function actionlikeVideo()
    {
        $empusrmodel = EmployerUsers::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
        $empmodel = Employers::model()->findByPk($empusrmodel->employer_id);
        
        $empprofilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$empmodel->id,'type'=>'employer'));
        
        $model = Users::model()->findByPk($_POST['user_id']);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$model->id,'type'=>$model->type));
        $plikes = ProfileLikes::model()->findByAttributes(array('profile_id'=>$empprofilemodel->id,'like_id'=>$profilemodel->id));
        $like = 0;
        if($plikes == null)
        {
            $profilelikes = new ProfileLikes();
            $profilelikes->profile_id = $empprofilemodel->id;
            $profilelikes->like_id = $profilemodel->id;
            $profilelikes->save();
            $like = 1;
            
            $link = $this->createAbsoluteUrl('employers/employer', array('slug' => $empprofilemodel->slug));
            
            $body = $this->renderPartial("//emails/liked-profile", array('name' => $empmodel->name,'link' => $link), true);
            // Send employer viewed notifcation to candidate
            Yii::import('application.extensions.phpmailer.JPhpMailer');
            $mail = new JPhpMailer;
            $mail->Host = 'mail.cvvid.com';
            $mail->Username = Yii::app()->params['EMAIL_USERNAME'];
            $mail->Password = Yii::app()->params['EMAIL_PASSWORD'];
            $mail->SMTPSecure = 'ssl';
            $mail->SetFrom('mail@cvvid.com', 'CVVID');
            $mail->Subject = "Your CVVid profile has been liked";
            $mail->MsgHTML($body);
            $mail->AddAddress($model->email, $model->forenames." ".$model->surname);
            $mail->Send();
            
            $message = "<a href=".$this->createUrl('employers/employer',array('slug'=>$empprofilemodel->slug))."> $empmodel->name </a> liked your profile";
            
            $this->sendMessage("Your profile has been liked",$message,Yii::app()->user->getState('userid'),$model->id);
            
        }
        else
        {
            ProfileLikes::model()->deleteAllByAttributes(array('profile_id'=>$empprofilemodel->id,'like_id'=>$profilemodel->id));
        }
        echo $like;
    }
    public function actionchangeHiredStatus() {
       
        $status = strtolower($_POST['status']) == "hired" ? 0 : 1;
        $model = Users::model()->findByPk($_POST['userid']);
        $pmodel = Profiles::model()->findByAttributes(array('owner_id' => $model->id));
        $profilemodel = Profiles::model()->findByPk($pmodel->id);          
        $profilemodel->hired = $status;
        if($status > 0)
            $profilemodel->visibility = 0;
        $profilemodel->save();
        
    }
    public function actionbulkCreate($trial, $paid, $basic)
    {
        $model = new Users();
        $this->render("bulkcandidates", array('model' => $model,'trial'=>$trial,'paid'=>$paid,'basic'=>$basic));
    }
    public function actioncreateBulkCandidates($trial, $paid, $basic)
    {
        $now = new DateTime();
        
        $forenames_arr = $_POST['forenames'];
        array_pop($forenames_arr);
        $surname_arr = $_POST['surname'];
        array_pop($surname_arr);
        $password_arr = $_POST['password'];
        array_pop($password_arr);
        $email_arr = $_POST['email'];
        array_pop($email_arr);
        for($i = 0; $i < count($forenames_arr); $i++)
        {
            $model = new Users(); 
            $model->forenames = $forenames_arr[$i];
            $model->surname = $surname_arr[$i];
            $model->email = $email_arr[$i];
            $model->type = 'candidate';
            $model->created_at = $now->format('Y-m-d H:i:s');
            $model->password = password_hash($password_arr[$i], 1,['cost' => 10]);
            if($trial == 1)
                $model->trial_ends_at = Carbon::now()->addYear()->toDateTimeString();
            if($model->save())
            {
                //  profile model
                $profilemodel = new Profiles();
                $profilemodel->owner_type = "Candidate";
                $profilemodel->slug = $this->getValidatedSlug($model->forenames."-".$model->surname);
                $profilemodel->owner_id = $model->id;
                $profilemodel->created_at = $model->created_at;
                $profilemodel->type = $model->type;
                $profilemodel->save();                
            }
        }
        
        Yii::app()->user->setFlash('success', 'Successfully candidates created.');
        $this->redirect($this->createUrl('users/admin', array('trial' => $trial, 'paid' => $paid, 'basic' => $basic)));
    }
    public function actioncheckEmails(){
        $index = array();
        $cnt = 1;
        foreach ($_POST['emails'] as $email)
        {
            $cri = new CDbCriteria();
            $cri->condition = "email ='".$email."'";
            $model = Users::model()->find($cri);
            if($model != null)
                $index[] = $cnt;
                
                $cnt++;
        }
        if(count($index) > 0)
            echo json_encode($index);
            else
                echo -1;
    }
    public function getValidatedSlug($slug)
    {
        $cri = new CDbCriteria();
        $cri->condition = "slug ='".$slug."'";
        $pmodel = Profiles::model()->findAll($cri);
        
        $cri1 = new CDbCriteria();
        $cri1->condition = "slug like '".$slug."-%'";
        $pmodel1 = Profiles::model()->findAll($cri1);
        
        if(count($pmodel1) == 0 && count($pmodel) == 1)
        {
            return strtolower($slug)."-1";
        }
        else if(count($pmodel1) > 0)
        {
            return (strtolower($slug)."-".rand()); //(count($pmodel1)+1));
        }
        else if(count($pmodel) == 0)
            return strtolower($slug);
            else
                return (strtolower($slug)."-".rand());
                
    }
    
//     public function actioninterview($id)
//     {
//         $this->layout = 'main';
//         $jamodel = JobApplications::model()->findByPk($id);
//         $model = Users::model()->findByPk($jamodel->user_id);
        
//         $this->render("interview", array('model' => $model,'jamodel'=>$jamodel));
//     }
    
    public function actioninterviewinst($id)
    {
        $this->layout = 'main';
        $jamodel = JobApplications::model()->findByPk($id);
        $model = Users::model()->findByPk($jamodel->user_id);
        
        $cri = new CDbCriteria();
        $cri->alias = "q";
        $cri->join = "left join cv16_job_answers a on a.question_id = q.id";
        $cri->condition = "a.id is null";
        $jqmodel = JobQuestions::model()->find($cri);
        
        $this->render("interview-instructions", array('model' => $model,'jamodel'=>$jamodel,'jqmodel'=>$jqmodel));
    }
    public function actioninterview($id)
    {
        $this->layout = 'main';
        $jamodel = JobApplications::model()->findByPk($id);
        $model = Users::model()->findByPk($jamodel->user_id);
        
        $cri2 = new CDbCriteria();
        $cri2->condition = "type like '%candidate%' and owner_id =".$model->id;
        $pmodel = Profiles::model()->find($cri2);
        
        $cri = new CDbCriteria();
        $cri->alias = "q";
        $cri->condition = "q.job_id = ".$jamodel->job_id." and q.id not in (select question_id from cv16_job_answers where job_id = ".$jamodel->job_id.")";
        $jqmodel = JobQuestions::model()->find($cri);
        
        //         $cri1 = new CDbCriteria();
        //         $cri1->select = "sum(answer_duration) as answer_duration";
        //         $cri1->condition = "job_id =".$jamodel['job_id']." and status = 1";
        //         $jquestions = JobQuestions::model()->find($cri1);
        //         $duration_mins = $jquestions['answer_duration'];
        
        $duration_mins = $jqmodel['answer_duration'];
        $this->render("interview", array('model' => $model,'pmodel'=>$pmodel,'jamodel'=>$jamodel,'jqmodel'=>$jqmodel ,'duration'=>$duration_mins ));
    }
    public function actioninterviewVideo($id)
    {
        $jamodel = JobApplications::model()->findByPk($id);
        $umodel = Users::model()->findByPk(Yii::app()->user->getState('userid'));
        $now = new DateTime();
        $userid = $umodel->id;
        $quesid = $_POST['question_id'];
        
        //video start
        
        if (!isset($_POST['audio-filename']) && !isset($_POST['video-filename'])) {
            echo 'Empty file name.';
            return;
        }
        
        // do NOT allow empty file names
        if (empty($_POST['audio-filename']) && empty($_POST['video-filename'])) {
            echo 'Empty file name.';
            return;
        }
        
        // do NOT allow third party audio uploads
        if (isset($_POST['audio-filename']) && strrpos($_POST['audio-filename'], "RecordRTC-") !== 0) {
            echo 'File name must start with "RecordRTC-"';
            return;
        }
        
        // do NOT allow third party video uploads
        /*  if (isset($_POST['video-filename']) && strrpos($_POST['video-filename'], "RecordRTC-") !== 0) {
         echo 'File name must start with "RecordRTC-"';
         return;
         }*/
        
        $name = '';
        $tmp = '';
        $file_idx = '';
        
        
        
        //         $name = str_replace(' ', '_', $_POST['video-filename']);
        //         $videoname = str_replace(' ', '_', $_FILES['video']['name']);
        $size = 0;//$_FILES['video']['size'];
        // $image_type = $_FILES['video']['type'];
        //         $tmp = $_FILES['video']['tmp_name'];
        //$ext = $this->getExtension($videoname);
        
        if (!empty($_FILES['audio-blob'])) {
            $file_idx = 'audio-blob';
            $name = $_POST['audio-filename'];
            $tmp = $_FILES[$file_idx]['tmp_name'];
        } else {
            $file_idx = 'video-blob';
            $name = str_replace(' ', '_', $_POST['video-filename']);
            $tmp = $_FILES[$file_idx]['tmp_name'];
        }
        
        if (empty($name) || empty($tmp)) {
            if(empty($tmp)) {
                echo 'Invalid temp_name: '.$tmp;
                return;
            }
            
            echo 'Invalid file name: '.$name;
            return;
        }
        
        /*
         $upload_max_filesize = return_bytes(ini_get('upload_max_filesize'));
         
         if ($_FILES[$file_idx]['size'] > $upload_max_filesize)
         echo 'upload_max_filesize exceeded.';
         return;
         }
         
         $post_max_size = return_bytes(ini_get('post_max_size'));
         
         if ($_FILES[$file_idx]['size'] > $post_max_size)
         echo 'post_max_size exceeded.';
         return;
         }
         */
         
         //          $filePath = 'uploads/' . $name;
         
         //          // make sure that one can upload only allowed audio/video files
         //          $allowed = array(
         //              'webm',
         //              'wav',
         //              'mp4',
         //              "mkv",
         //              'mp3',
         //              'ogg'
         //          );
         //          $ext = pathinfo($filePath, PATHINFO_EXTENSION);
         //          if (!$ext || empty($ext) || !in_array($ext, $allowed)) {
         //              echo 'Invalid file extension: '.$ext;
         //              return;
         //          }
         
         //          if (!move_uploaded_file($tmp, $filePath)) {
         //              echo 'Problem saving file: '.$tmp;
         //              return;
         //          }
         
                 
         //video end
         
         
         
         $ext = pathinfo($name, PATHINFO_EXTENSION);
         $usermodel = Users::model()->findByPk($userid);
         $pmodel = Profiles::model()->findByAttributes(array('owner_id' => $userid, 'type' => $usermodel->type));
         
         Yii::import('application.extensions.amazon.components.*');
         
         $bucket = Yii::app()->params['AWS_BUCKET'];
         $s3 = new A2S3();
         
         $vname = pathinfo($name, PATHINFO_FILENAME);
         
         $actual_image_name = $vname.time() . "." . $ext;
         
         //        video validation
         if (strlen($name) > 0) {
             
             try {
                 //                $s3->putObject(array(
                 //                    'Bucket' => $bucket,
                 //                    'Key' => $image_name_actual,
                 //                    'SourceFile' => $tmp,
                 //                    'StorageClass' => 'REDUCED_REDUNDANCY'
                 //                ));
                 
                 $response =  $s3->putObject(array(
                     'SourceFile' => $tmp,
                     'Bucket' => $bucket,
                     'Key' => $actual_image_name,
                     'ACL' => 'public-read',
                     'x-amz-storage-class' => 'REDUCED_REDUNDANCY'
                 ));
                 
                 //  $message = "S3 Upload Successful.";
                 //  $s3file = 'http://' . $bucket . '.s3.amazonaws.com/' . $actual_image_name;
                 //  echo $s3file;die();
                 // echo "<img src='$s3file'/>";
                 //  echo 'S3 File URL:' . $s3file;
                 
                 //    $vname = pathinfo($name, PATHINFO_FILENAME);
                 
                 $duration = 6;
                 
                 $videomodel = new Videos();
                 $videomodel->model_id = $userid;
                 $videomodel->video_id = $actual_image_name;
                 $videomodel->name = $vname;
                 $videomodel->duration = 0;
                 $videomodel->model_type = $usermodel->type;
                 $videomodel->category_id = 1;
                 $videomodel->is_processed = 1;
                 $videomodel->state = 1;
                 $videomodel->status = "active";
                 $videomodel->created_at = $now->format('Y-m-d H:i:s');
                 if($videomodel->save())
                 {
                     $ansmodel = new JobAnswers();
                     $ansmodel->user_id = $userid;
                     $ansmodel->job_id = $jamodel->job_id;
                     $ansmodel->question_id = $quesid;
                     $ansmodel->videoid = $videomodel->id;
                     $ansmodel->created_at = $now->format('Y-m-d H:i:s');
                     $ansmodel->save();
                     
                     $jamodel->status = 3;
                     $jamodel->save(false);
                 }
                 
                 echo "success";
                 
             } catch (S3Exception $e) {
                 // Catch an S3 specific exception.
                 echo $e->getMessage();
             }
         }
         //echo $this->renderPartial('userVideos', array('model' => $usermodel, 'profilemodel' => $pmodel));
         
    }
    
    public function actionsaveUserStatement() {
        $usermodel = Users::model()->findByPk($_POST['user_id']);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));
        $now = new DateTime();
        
        $model = new UserStatement();
        $model->user_id = $_POST['user_id'];
        $model->description = $_POST['description'];
        $model->created_at = $now->format('Y-m-d H:i:s');
        if ($model->save()) {
            $pqmodel = new ProfileStatement();
            $pqmodel->profile_id = $profilemodel->id;
            $pqmodel->user_statement_id = $model->id;
            $pqmodel->created_at = $now->format('Y-m-d H:i:s');
            $pqmodel->save();

             $this->updatepercentage($usermodel->id,$profilemodel);       
        }
        
        $edumodel = Users::model()->findByPk($model->user_id);
        echo $this->renderPartial('userStatement', array('model' => $edumodel));
    }
    
    public function actionupdateUserStatement() {
        $model = UserStatement::model()->findByPk($_POST['id']);
        $usermodel = Users::model()->findByPk($model->user_id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $usermodel->id, 'type' => $usermodel->type));
        
        $now = new DateTime();
        
        $model->description = $_POST['description'];
        $model->updated_at = $now->format('Y-m-d H:i:s');
        if ($model->save()) {
            $pemodel = ProfileStatement::model()->findByAttributes(array('profile_id' => $profilemodel->id, 'user_statement_id' => $model->id));
            $pemodel->updated_at = $now->format('Y-m-d H:i:s');
            $pemodel->save();
        }
        
        $edumodel = Users::model()->findByPk($model->user_id);
        echo $this->renderPartial('userStatement', array('model' => $edumodel));
    }
    
    public function actiondeleteUserStatement() {
        $model = UserStatement::model()->findByPk($_POST['id']);
        $userid = $model->user_id;
        
        $cri = new CDbCriteria();
        $cri->condition = "user_statement_id =" . $_POST['id'];
        ProfileStatement::model()->deleteAll($cri);
        
        $cri1 = new CDbCriteria();
        $cri1->condition = "id =" . $_POST['id'];
        $model = UserStatement::model()->deleteAll($cri1);
        
        $expmodel = Users::model()->findByPk($userid);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $expmodel->id, 'type' => $expmodel->type));
        $this->updatepercentage($userid, $profilemodel);
        echo $this->renderPartial('userStatement', array('model' => $expmodel));
    }
    
    public function loadModelSlug($slug)
    {
//         $umodel = Users::model()->findByPk($id);      
        $model = Profiles::model()->findByAttributes(array('slug'=>$slug));
        $umodel = Users::model()->findByPk($model->owner_id);
        if($umodel===null)
            throw new CHttpException(404,'The requested page does not exist.');
         return $umodel;
       
           
    }
    
    
    public function sendMessage($subject,$msg,$from,$to) {
	    
	    $now = new DateTime();
	    $conmodel = new Conversations();
	    $conmodel->subject = $subject;
	    $conmodel->created_at = $now->format('Y-m-d H:i:s');
	    $conmodel->updated_at = $now->format('Y-m-d H:i:s');
	    if ($conmodel->save()) {
	        $usermodel = Users::model()->findByPk($from);
	        $conmsgmodel = new ConversationMessages();
	        $conmsgmodel->conversation_id = $conmodel->id;
	        $conmsgmodel->user_id = Yii::app()->user->getState('userid');
	        $conmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
	        $conmsgmodel->message = $msg;
	        $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
	        $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
	        if ($conmsgmodel->save()) {
	            $conmemmodel_sender = new ConversationMembers();
	            $conmemmodel_sender->conversation_id = $conmodel->id;
	            $conmemmodel_sender->user_id = $from;
	            $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
	            $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
	            $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
	            $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
	            if ($conmemmodel_sender->save()) {
	                $usermodel1 = Users::model()->findByPk($to);
	                $conmemmodel_rec = new ConversationMembers();
	                $conmemmodel_rec->conversation_id = $conmodel->id;
	                $conmemmodel_rec->user_id =$to;
	                $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
	                $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
	                $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
	                if ($conmemmodel_rec->save()) {
	                    $actmodel_sender = new Activities();
	                    $actmodel_sender->user_id = $from;
	                    $actmodel_sender->type = "message";
	                    $actmodel_sender->model_id = $conmsgmodel->id;
	                    $actmodel_sender->model_type = "ConversationMessage";
	                    $actmodel_sender->subject = "You sent a message in conversation: " . $subject;
	                    $actmodel_sender->message = $msg;
	                    $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
	                    $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
	                    if ($actmodel_sender->save()) {
	                        $actmodel_rec = new Activities();
	                        $actmodel_rec->user_id = $to;
	                        $actmodel_rec->type = "message";
	                        $actmodel_rec->model_id = $conmsgmodel->id;
	                        $actmodel_rec->model_type = "ConversationMessage";
	                        $actmodel_rec->subject = "You received a message in conversation: " . $subject;
	                        $actmodel_rec->message = $msg;
	                        $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
	                        $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
	                        $actmodel_rec->save();
	                    }
	                }
	            }
	        }
	    }
	}

      public function actionprofileLikes($id)
    {
        $this->layout = "main";
        $profilemodel = Profiles::model()->findByPk($id);
        $model = Users::model()->findByPk($profilemodel->owner_id);
        $this->render('profile_likes', array(
            'model' => $model, 'profilemodel' => $profilemodel
        ));
    }
    
    public function actionextendTrail()
    {
        $user_id = $_POST['user_id'];
        $trial_ends_at = $_POST['trial_ends_at'];
        
        $user = Users::model()->findByPk($user_id);
        
        if($user->id != $user_id)
            echo "0";
        
        $timestamp = strtotime($trial_ends_at);
        
        $user->trial_ends_at = date("Y-m-d H:i:s", $timestamp);
        $user->stripe_active = 1;
        $user->subscription_ends_at = NULL;
        $user->save(FALSE);
        
        echo "1";
        
    }
    
    public function actionextendSubscription()
    {
        $user_id = $_POST['user_id'];
        $subscription_ends_at = $_POST['subscription_ends_at'];
        
        $user = Users::model()->findByPk($user_id);
        
        if($user->id != $user_id)
            echo "0";
        
        $timestamp = strtotime($subscription_ends_at);
        
        $user->subscription_ends_at = date("Y-m-d H:i:s", $timestamp);
        $user->stripe_active = 1;
        $user->trial_ends_at = NULL;
        $user->save(FALSE);
        
        echo "1";
        
    }
    
    public function actionmakeBasic()
    {
        $user_id = $_POST['user_id'];
        
        $user = Users::model()->findByPk($user_id);
        
        if($user->id != $user_id)
            echo "0";
        
        $user->trial_ends_at = NULL;
        $user->stripe_active = 0;
        $user->stripe_id = NULL;
        $user->subscription_ends_at = NULL;
        $user->subscription_ends_at = NULL;
        $user->save(FALSE);
        
        echo "1";
        
    }
    
    public function actionaddcomment()
    {
         $this->layout = "main";
        $candidate_id = $_POST['candidate_id'];
        $comment_msg = $_POST['comment'];
        
        $empusrmodel = EmployerUsers::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid')));
        $currentUser = Employers::model()->findByPk($empusrmodel->employer_id);
        
        $now = new DateTime();
        
        $comments = new Comments();
        
        $comments->candidate_id = $candidate_id;
        $comments->employer_id = $currentUser->id;
        $comments->comment = $comment_msg;
        $comments->created_date = $now->format('Y-m-d H:i:s');
        $comments->status = '1';
        if($comments->save())
        {
//            $this->sendMessage("An employer commented on your profile video",$comment_msg,Yii::app()->user->getState('userid'),$candidate_id);
            $subject = "An employer '".$currentUser->name."' commented on your profile video";
            $msg = $comment_msg;
            $from = Yii::app()->user->getState('userid');
            $to = $candidate_id;
            $now = new DateTime();
                $conmodel = new Conversations();
                $conmodel->subject = $subject;
                $conmodel->created_at = $now->format('Y-m-d H:i:s');
                $conmodel->updated_at = $now->format('Y-m-d H:i:s');
                if ($conmodel->save()) {
                    $usermodel = Users::model()->findByPk($from);
                    $conmsgmodel = new ConversationMessages();
                    $conmsgmodel->conversation_id = $conmodel->id;
                    $conmsgmodel->user_id = Yii::app()->user->getState('userid');
                    $conmsgmodel->name = $usermodel->forenames . " " . $usermodel->surname;
                    $conmsgmodel->message = $msg;
                    $conmsgmodel->created_at = $now->format('Y-m-d H:i:s');
                    $conmsgmodel->updated_at = $now->format('Y-m-d H:i:s');
                    if ($conmsgmodel->save()) {
                        $conmemmodel_sender = new ConversationMembers();
                        $conmemmodel_sender->conversation_id = $conmodel->id;
                        $conmemmodel_sender->user_id = $from;
                        $conmemmodel_sender->name = $usermodel->forenames . " " . $usermodel->surname;
                        $conmemmodel_sender->last_read = $now->format('Y-m-d H:i:s');
                        $conmemmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                        $conmemmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                        if ($conmemmodel_sender->save()) {
                            $usermodel1 = Users::model()->findByPk($to);
                            $conmemmodel_rec = new ConversationMembers();
                            $conmemmodel_rec->conversation_id = $conmodel->id;
                            $conmemmodel_rec->user_id =$to;
                            $conmemmodel_rec->name = $usermodel1->forenames . " " . $usermodel1->surname;
                            $conmemmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                            $conmemmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                            if ($conmemmodel_rec->save()) {
                                $actmodel_sender = new Activities();
                                $actmodel_sender->user_id = $from;
                                $actmodel_sender->type = "message";
                                $actmodel_sender->model_id = $conmsgmodel->id;
                                $actmodel_sender->model_type = "ConversationMessage";
                                $actmodel_sender->subject = "You sent a message in conversation: " . $subject;
                                $actmodel_sender->message = $msg;
                                $actmodel_sender->created_at = $now->format('Y-m-d H:i:s');
                                $actmodel_sender->updated_at = $now->format('Y-m-d H:i:s');
                                if ($actmodel_sender->save()) {
                                    $actmodel_rec = new Activities();
                                    $actmodel_rec->user_id = $to;
                                    $actmodel_rec->type = "message";
                                    $actmodel_rec->model_id = $conmsgmodel->id;
                                    $actmodel_rec->model_type = "ConversationMessage";
                                    $actmodel_rec->subject = "You received a message in conversation: " . $subject;
                                    $actmodel_rec->message = $msg;
                                    $actmodel_rec->created_at = $now->format('Y-m-d H:i:s');
                                    $actmodel_rec->updated_at = $now->format('Y-m-d H:i:s');
                                    $actmodel_rec->save();
                                }
                            }
                        }
                    }
                }
            $model = Users::model()->findByPk($candidate_id);
            echo  $this->renderPartial("comments", array('model' => $model,'empmodel'=>$currentUser));
        }
    }
    
    public function actiondeletecomment()
    {
        $cid = $_POST['id'];
        Comments::model()->deleteByPk($cid);
    }
    public function actionskypeinterview()
    {
        $this->layout = 'main';
        //         $jamodel = JobApplications::model()->findByPk($id);
        //         $model = Users::model()->findByPk($jamodel->user_id);
        
        //         $cri = new CDbCriteria();
        //         $cri->alias = "q";
        //         $cri->join = "left join cv16_job_answers a on a.question_id = q.id";
        //         $cri->condition = "a.id is null";
        //         $jqmodel = JobQuestions::model()->find($cri);
        
        $this->render("skypeinterview", array('model' => JobQuestions::model()));
    }
   
    public function actionskipAnswer($id)
    {
        $this->layout = 'main';
        
        $jamodel = JobApplications::model()->findByPk($id);
        $model = Users::model()->findByPk(Yii::app()->user->getState('userid'));
        
        $now = new DateTime();
        $cri = new CDbCriteria();
        $cri->alias = "q";
        $cri->condition = "q.job_id = ".$jamodel->job_id." and q.id not in (select question_id from cv16_job_answers where job_id = ".$jamodel->job_id.")";
        $jqmodel = JobQuestions::model()->find($cri);
        
        $ansmodel = new JobAnswers();
        $ansmodel->user_id = $model->id;
        $ansmodel->job_id = $jamodel->job_id;
        $ansmodel->question_id = $jqmodel->id;
        $ansmodel->videoid = 0;
        $ansmodel->created_at = $now->format('Y-m-d H:i:s');
        $ansmodel->save();
        
        // 	    $this->layout = 'main';
        
        // 	    $model = Users::model()->findByPk($jamodel->user_id);
        
        $cri2 = new CDbCriteria();
        $cri2->condition = "type like '%candidate%' and owner_id =".$model->id;
        $pmodel = Profiles::model()->find($cri2);
        
        $cri = new CDbCriteria();
        $cri->alias = "q";
        $cri->condition = "q.job_id = ".$jamodel->job_id." and q.id not in (select question_id from cv16_job_answers where job_id = ".$jamodel->job_id.")";
        $jqmodel = JobQuestions::model()->find($cri);
        
        $duration_mins = $jqmodel['answer_duration'];
        $this->render("interview", array('model' => $model,'pmodel'=>$pmodel,'jamodel'=>$jamodel,'jqmodel'=>$jqmodel ,'duration'=>$duration_mins ));
    }
	
	  public function actiontabupgrade($id) {
        
        $model = Users::model()->findByPk($id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $model->id, 'type' => $model->type));
        $cri = new CDbCriteria();
        $cri->condition = "model_id = ".$model->id." and model_type like '%candidate%' and deleted_at is null";
        $addressmodel = Addresses::model()->find($cri);
            if($addressmodel == null)
                $addressmodel = new Addresses();
     
        $this->render('tabupgrade', array('model' => $model, 'profilemodel' => $profilemodel, 'addressmodel' => $addressmodel));
    }
    
    public function actionpremiumUpgrade($id)
    {
        $d = str_replace('/', '-', $_POST["end_date"]);
        $enddate = date('Y-m-d H:i:s', strtotime($d));
        
        $model = Users::model()->findByPk($id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $model->id, 'type' => $model->type));
      
        $cri = new CDbCriteria();
        $cri->condition = "model_id = ".$model->id." and model_type like '%candidate%' and deleted_at is null";
        $addressmodel = Addresses::model()->find($cri);
        if($addressmodel == null)
            $addressmodel = new Addresses();
        
        
        $model->stripe_active = 1;
		$model->trial_ends_at = null;
        $model->subscription_ends_at = $enddate;
        if($model->save())
            Yii::app()->user->setFlash('success', 'Upgraded Successfully.');
            $this->render('tabupgrade', array('model' => $model, 'profilemodel' => $profilemodel, 'addressmodel' => $addressmodel));
    }
	
	public function updatepercentage($userid,$pmodel)
    {
        // update percentage
        $percentage = 0;
        $statementdocs = UserStatement::model()->findAllByAttributes(array('user_id' => $userid));
        
        if (count($statementdocs) > 0)
            $percentage += '13';
            
        $profiledocs = ProfileDocuments::model()->findAllByAttributes(array('profile_id' => $pmodel->id));
        if (count($profiledocs) > 0)
            $percentage += '13';
            
        $profileexp = ProfileExperiences::model()->findAllByAttributes(array('profile_id' => $pmodel->id));
        if (count($profileexp) > 0)
            $percentage += '13';
            
        $profilehob = ProfileHobbies::model()->findAllByAttributes(array('profile_id' => $pmodel->id));
        if (count($profilehob) > 0)
            $percentage += '13';
            
        $profilelang = ProfileLanguages::model()->findAllByAttributes(array('profile_id' => $pmodel->id));
        if (count($profilelang) > 0)
            $percentage += '12';
            
        $profilequal = ProfileQualifications::model()->findAllByAttributes(array('profile_id' => $pmodel->id));
        if (count($profilequal) > 0)
            $percentage += '12';
            
        $profileskill = ProfileSkills::model()->findAllByAttributes(array('profile_id' => $pmodel->id));
        if (count($profileskill) > 0)
            $percentage += '12';
            
        $profilevid = ProfileVideos::model()->findAllByAttributes(array('profile_id' => $pmodel->id));
        if (count($profilevid) > 0)
            $percentage += '12';
            
        $pmodel->profile_completed = $percentage;
        $pmodel->save();
        // update percentage
    }
}
