<?php

/**
 * This is the model class for table "cv16_employers".
 *
 * The followings are the available columns in table 'cv16_employers':
 * @property string $id
 * @property string $user_id
 * @property string $name
 * @property string $email
 * @property string $body
 * @property string $location
 * @property string $location_lat
 * @property string $location_lng
 * @property string $website
 * @property string $tel
 * @property integer $stripe_active
 * @property string $stripe_id
 * @property string $stripe_subscription
 * @property string $stripe_plan
 * @property string $last_four
 * @property string $card_expiry
 * @property string $card_expiry_sent
 * @property string $trial_ends_at
 * @property string $subscription_ends_at
 * @property integer $published
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property string $video_id
 * @property string $vacancy_count
 * @property string $current_vacancy_count
 *
 * The followings are the available model relations:
 * @property EmployerUsers[] $employerUsers
 * @property Users $user
 */
class Employers extends CActiveRecord
{
    public $captcha;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cv16_employers';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required','message'=>'The employer {attribute} field is required.'),
		        array('email', 'unique','attributeName' => 'email','message'=>'This Email is already taken by an account'),
			 array('website','validatewebsite'),
			array('stripe_active, published', 'numerical', 'integerOnly'=>true),
			array('user_id', 'length', 'max'=>10),
		        array('captcha', 'required','message'=>'The captcha form field is required', 'on' => 'captcha'),
			array('name, email, location, location_lat, location_lng, website, tel, stripe_id, stripe_subscription', 'length', 'max'=>255),
			array('stripe_plan', 'length', 'max'=>100),
			array('last_four', 'length', 'max'=>4),
		   // array('tel', 'numerical', 'integerOnly'=>true,'message'=>'Phone number must be a number'),
			array('body, card_expiry,vacancy_count,current_vacancy_count, card_expiry_sent, trial_ends_at, subscription_ends_at, created_at, updated_at, deleted_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, name, email,vacancy_count,current_vacancy_count, body,video_id, location, location_lat, location_lng, website, tel, stripe_active, stripe_id, stripe_subscription, stripe_plan, last_four, card_expiry, card_expiry_sent, trial_ends_at, subscription_ends_at, published, created_at, updated_at, deleted_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'employerUsers' => array(self::HAS_MANY, 'EmployerUsers', 'employer_id'),
			'user' => array(self::BELONGS_TO, 'Users', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'name' => 'Name',
			'email' => 'Email',
			'body' => 'Body',
			'location' => 'Location',
			'location_lat' => 'Location Lat',
			'location_lng' => 'Location Lng',
			'website' => 'Website',
			'tel' => 'Tel',
			'stripe_active' => 'Stripe Active',
			'stripe_id' => 'Stripe',
			'stripe_subscription' => 'Stripe Subscription',
			'stripe_plan' => 'Stripe Plan',
			'last_four' => 'Last Four',
			'card_expiry' => 'Card Expiry',
			'card_expiry_sent' => 'Card Expiry Sent',
			'trial_ends_at' => 'Trial Ends At',
			'subscription_ends_at' => 'Subscription Ends At',
			'published' => 'Published',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'deleted_at' => 'Deleted At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('body',$this->body,true);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('location_lat',$this->location_lat,true);
		$criteria->compare('location_lng',$this->location_lng,true);
		$criteria->compare('website',$this->website,true);
		$criteria->compare('tel',$this->tel,true);
		$criteria->compare('stripe_active',$this->stripe_active);
		$criteria->compare('stripe_id',$this->stripe_id,true);
		$criteria->compare('stripe_subscription',$this->stripe_subscription,true);
		$criteria->compare('stripe_plan',$this->stripe_plan,true);
		$criteria->compare('last_four',$this->last_four,true);
		$criteria->compare('card_expiry',$this->card_expiry,true);
		$criteria->compare('card_expiry_sent',$this->card_expiry_sent,true);
		$criteria->compare('trial_ends_at',$this->trial_ends_at,true);
		$criteria->compare('subscription_ends_at',$this->subscription_ends_at,true);
		$criteria->compare('published',$this->published);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('deleted_at',$this->deleted_at,true);
                
                $criteria->addCondition('deleted_at is null');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Employers the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getSlugURL()
	{
	    $cri = new CDbCriteria();
	    $cri->condition = "owner_id =".$this->id." and type like '%employer%'";
	    $profilemodel = Profiles::model()->find($cri);
	    
	    return Yii::app()->createUrl("employers/employer", array("slug"=>$profilemodel->slug));
	}
	
	public function validatewebsite($attribute,$params)
	{
	    if($this->website != "" && substr_count($this->website, ".") < 2)
	    $this->addError($attribute, 'Website format should be www.example.com');
	}
}
