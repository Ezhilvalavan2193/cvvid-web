<?php

/**
 * This is the model class for table "cv16_agency_staff_candidates".
 *
 * The followings are the available columns in table 'cv16_agency_staff_candidates':
 * @property string $id
 * @property integer $agency_id
 * @property integer $agency_candidate_id
 * @property string $staff_id
 * @property string $created_at
 * @property string $updated_at
 */
class AgencyStaffCandidates extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cv16_agency_staff_candidates';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('agency_id, staff_id', 'required'),
			array('agency_id, agency_candidate_id', 'numerical', 'integerOnly'=>true),
			array('staff_id', 'length', 'max'=>10),
			array('created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, agency_id, agency_candidate_id, staff_id, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		    'agency' => array(self::BELONGS_TO, 'Agency', 'agency_id')
// 		    'staff' => array(self::BELONGS_TO, 'Users', 'staff_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'agency_id' => 'Agency',
			'agency_candidate_id' => 'Agency Candidate',
			'staff_id' => 'Staff',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('agency_id',$this->agency_id);
		$criteria->compare('agency_candidate_id',$this->agency_candidate_id);
		$criteria->compare('staff_id',$this->staff_id,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function searchByStaff($id)
	{
	    // @todo Please modify the following code to remove attributes that should not be searched.
	    
	    $criteria=new CDbCriteria;
	    $criteria->alias = "asc";
	    $criteria->join = "inner join cv16_agency_candidates ac on ac.id = asc.agency_candidate_id";
	    $criteria->compare('id',$this->id,true);
	    $criteria->compare('agency_id',$this->agency_id);
	    $criteria->compare('agency_candidate_id',$this->agency_candidate_id);
	    $criteria->compare('staff_id',$this->staff_id,true);
	    $criteria->compare('created_at',$this->created_at,true);
	    $criteria->compare('updated_at',$this->updated_at,true);
	    
	    $criteria->addCondition("staff_id =".$id);
	    
	    return new CActiveDataProvider($this, array(
	        'criteria'=>$criteria,
	    ));
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AgencyStaffCandidates the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function getName()
	{
	    $acmodel = AgencyCandidates::model()->findByPk($this->agency_candidate_id);
	    if($acmodel != null)
	    {
    	    $umodel = Users::model()->findByPk($acmodel->user_id);
    	    return $umodel->forenames." ".$umodel->surname;
	    }
	    else 
	        return "";
	}
	public function getEmail()
	{
	    $acmodel = AgencyCandidates::model()->findByPk($this->agency_candidate_id);
	    if($acmodel != null)
	    {
    	    $umodel = Users::model()->findByPk($acmodel->user_id);
    	    return $umodel->email;
	    }
	    else
	        return "";
	}
	public function getCurrentJob()
	{
	    $acmodel = AgencyCandidates::model()->findByPk($this->agency_candidate_id);
	    if($acmodel != null)
	    {
    	    $umodel = Users::model()->findByPk($acmodel->user_id);
    	    return $umodel->current_job;
	    }
	    else
	        return "";
	}
}
