<?php

/**
 * This is the model class for table "cv16_slideshows".
 *
 * The followings are the available columns in table 'cv16_slideshows':
 * @property string $id
 * @property string $user_id
 * @property string $title
 * @property string $slug
 * @property string $type
 * @property string $body
 * @property string $status
 * @property string $background_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property string $background_x
 * @property string $background_y
 *
 * The followings are the available model relations:
 * @property SlideshowSlides[] $slideshowSlides
 * @property Media $background
 */
class Slideshows extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cv16_slideshows';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, slug', 'required'),
			array('user_id, background_id', 'length', 'max'=>10),
			array('title, slug, type, status', 'length', 'max'=>255),
			array('background_x, background_y', 'length', 'max'=>50),
			array('body, created_at, updated_at, deleted_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, user_id, title, slug, type, body, status, background_id, created_at, updated_at, deleted_at, background_x, background_y', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'slideshowSlides' => array(self::HAS_MANY, 'SlideshowSlides', 'slideshow_id'),
			'background' => array(self::BELONGS_TO, 'Media', 'background_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'User',
			'title' => 'Title',
			'slug' => 'Alias',
			'type' => 'Type',
			'body' => 'Body',
			'status' => 'Status',
			'background_id' => 'Background',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'deleted_at' => 'Deleted At',
			'background_x' => 'Background X',
			'background_y' => 'Background Y',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('body',$this->body,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('background_id',$this->background_id,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('deleted_at',$this->deleted_at,true);
		$criteria->compare('background_x',$this->background_x,true);
		$criteria->compare('background_y',$this->background_y,true);
                
                $criteria->addCondition('deleted_at is null');

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Slideshows the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function getNumSlides()
	{
	    return SlideshowSlides::model()->countByAttributes(array('slideshow_id'=>$this->id));
	}
}
