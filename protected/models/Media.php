<?php

/**
 * This is the model class for table "cv16_media".
 *
 * The followings are the available columns in table 'cv16_media':
 * @property string $id
 * @property string $model_type
 * @property integer $model_id
 * @property string $collection_name
 * @property string $name
 * @property string $file_name
 * @property string $disk
 * @property integer $size
 * @property string $manipulations
 * @property string $custom_properties
 * @property integer $order_column
 * @property string $created_at
 * @property string $updated_at
 *
 * The followings are the available model relations:
 * @property Profiles[] $profiles
 * @property Profiles[] $profiles1
 * @property SlideshowSlides[] $slideshowSlides
 * @property Slideshows[] $slideshows
 * @property UserDocuments[] $userDocuments
 */
class Media extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cv16_media';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('collection_name, name, file_name', 'required'),
			array('model_id, size, order_column', 'numerical', 'integerOnly'=>true),
			array('model_type, collection_name, name, file_name, disk', 'length', 'max'=>255),
			array('created_at, updated_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('model_type, model_id, collection_name, name, file_name, disk, size, manipulations, custom_properties, order_column, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'profiles' => array(self::HAS_MANY, 'Profiles', 'cover_id'),
			'profiles1' => array(self::HAS_MANY, 'Profiles', 'photo_id'),
			'slideshowSlides' => array(self::HAS_MANY, 'SlideshowSlides', 'media_id'),
			'slideshows' => array(self::HAS_MANY, 'Slideshows', 'background_id'),
			'userDocuments' => array(self::HAS_MANY, 'UserDocuments', 'media_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'model_type' => 'Model Type',
			'model_id' => 'Model',
			'collection_name' => 'Collection Name',
			'name' => 'Name',
			'file_name' => 'File Name',
			'disk' => 'Disk',
			'size' => 'Size',
			'manipulations' => 'Manipulations',
			'custom_properties' => 'Custom Properties',
			'order_column' => 'Order Column',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('model_type',$this->model_type,true);
		$criteria->compare('model_id',$this->model_id);
		$criteria->compare('collection_name',$this->collection_name,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('file_name',$this->file_name,true);
		$criteria->compare('disk',$this->disk,true);
		$criteria->compare('size',$this->size);
		$criteria->compare('manipulations',$this->manipulations,true);
		$criteria->compare('custom_properties',$this->custom_properties,true);
		$criteria->compare('order_column',$this->order_column);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Media the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
