<?php

/**
 * This is the model class for table "cv16_users".
 *
 * The followings are the available columns in table 'cv16_users':
 * @property string $id
 * @property string $type
 * @property string $forenames
 * @property string $surname
 * @property string $email
 * @property string $password
 * @property string $dob
 * @property string $status
 * @property integer $is_premium
 * @property string $tel
 * @property string $mobile
 * @property string $current_job
 * @property string $location
 * @property string $location_lat
 * @property string $location_lng
 * @property string $remember_token
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property integer $stripe_active
 * @property string $stripe_id
 * @property string $stripe_subscription
 * @property string $stripe_plan
 * @property string $last_four
 * @property string $card_expiry
 * @property string $card_expiry_sent
 * @property string $trial_ends_at
 * @property string $subscription_ends_at
 * @property string $gender
 * @property string $display_gender
 * @property string $api_key
 * @property string $platform
 *
 * The followings are the available model relations:
 * @property Activities[] $activities
 * @property EmployerUsers[] $employerUsers
 * @property Employers[] $employers
 * @property InstitutionUsers[] $institutionUsers
 * @property JobApplications[] $jobApplications
 * @property Jobs[] $jobs
 * @property Jobs[] $jobs1
 * @property TutorGroups[] $tutorGroups
 * @property TutorGroupsStudents[] $tutorGroupsStudents
 * @property UserDocuments[] $userDocuments
 * @property UserExperiences[] $userExperiences
 * @property UserHobbies[] $userHobbies
 * @property UserLanguages[] $userLanguages
 * @property UserQualifications[] $userQualifications
 * @property UserQualifications[] $userQualifications
 */
class Users extends CActiveRecord {

    //public $password;
    public $confirmpassword;
    public $captcha;

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'cv16_users';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('forenames, surname, email', 'required', 'message' => 'The {attribute} field is required.'),
           // array('forenames,surname','match' ,'pattern'=>'/^[A-Za-z_]+$/u','message'=>"{attribute} should contain only letters."),
            array('password', 'required', 'message' => 'The {attribute} field is required.','on'=>'insert'),
            array('email', 'unique', 'attributeName' => 'email', 'message' => 'This Email is already taken by an account', 'on' => 'insert'),
            array('email', 'match', 'pattern' => '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD'),
             array('tel', 'numerical','message' => 'The {attribute} field will not accept letters or special characters.', 'integerOnly' => true),
			array('is_premium, stripe_active', 'numerical', 'integerOnly' => true),
            array('captcha', 'required', 'message' => 'The captcha form field is required', 'on' => 'captcha'),
            array('type, forenames, surname, email, tel,  current_job, location, location_lat, location_lng, stripe_id, stripe_subscription', 'length', 'max' => 255),
           // array('mobile', 'numerical', 'integerOnly' => true),
            array('password, confirmpassword', 'length', 'min' => 6),
           array('confirmpassword', 'compare', 'compareAttribute'=>'password' , 'on'=>'confirm'),
           array('dob', 'required',  'message' => 'The {attribute} field is required.','on'=>'doberror'),
          // array('confirmpassword', 'required', 'message' => 'The password confirmation does not match.','on'=>'confirmpassword'),
            array('status, remember_token, stripe_plan', 'length', 'max' => 100),
            array('last_four', 'length', 'max' => 4),
            array('dob, created_at, updated_at,move_cvvid, deleted_at,email,gender, card_expiry, card_expiry_sent, trial_ends_at, subscription_ends_at,loggeddatetime,display_gender,api_key,platform', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id,platform, type, forenames, move_cvvid,surname, email, password, dob,gender, status, is_premium, tel, mobile, current_job, location, location_lat, location_lng, remember_token, created_at, updated_at, deleted_at, stripe_active, stripe_id, stripe_subscription, stripe_plan, last_four, card_expiry, card_expiry_sent, trial_ends_at, subscription_ends_at, loggeddatetime,display_gender,api_key', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'activities' => array(self::HAS_MANY, 'Activities', 'user_id'),
            'employerUsers' => array(self::HAS_MANY, 'EmployerUsers', 'user_id'),
            'employers' => array(self::HAS_MANY, 'Employers', 'user_id'),
            'institutionUsers' => array(self::HAS_MANY, 'InstitutionUsers', 'user_id'),
            'jobApplications' => array(self::HAS_MANY, 'JobApplications', 'user_id'),
            'jobs' => array(self::HAS_MANY, 'Jobs', 'successful_applicant_id'),
            'jobs1' => array(self::HAS_MANY, 'Jobs', 'user_id'),
            'tutorGroups' => array(self::HAS_MANY, 'TutorGroups', 'teacher_id'),
            'tutorGroupsStudents' => array(self::HAS_MANY, 'TutorGroupsStudents', 'student_id'),
            'userDocuments' => array(self::HAS_MANY, 'UserDocuments', 'user_id'),
            'userExperiences' => array(self::HAS_MANY, 'UserExperiences', 'user_id'),
            'userHobbies' => array(self::HAS_MANY, 'UserHobbies', 'user_id'),
            'userLanguages' => array(self::HAS_MANY, 'UserLanguages', 'user_id'),
            'userQualifications' => array(self::HAS_MANY, 'UserQualifications', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'type' => 'Type',
            'forenames' => 'Forenames',
            'surname' => 'Surname',
            'email' => 'Email',
            'password' => 'Password',
            'dob' => 'Dob',
            'status' => 'Status',
            'is_premium' => 'Is Premium',
            'tel' => 'Telephone',
            'mobile' => 'Mobile',
            'current_job' => 'Current Job',
            'location' => 'Location',
            'location_lat' => 'Location Lat',
            'location_lng' => 'Location Lng',
            'remember_token' => 'Remember Token',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
            'stripe_active' => 'Stripe Active',
            'stripe_id' => 'Stripe',
            'stripe_subscription' => 'Stripe Subscription',
            'stripe_plan' => 'Stripe Plan',
            'last_four' => 'Last Four',
            'card_expiry' => 'Card Expiry',
            'card_expiry_sent' => 'Card Expiry Sent',
            'trial_ends_at' => 'Trial Ends At',
            'subscription_ends_at' => 'Subscription Ends At',
            'gender' => 'Gender',
            'loggeddatetime' => 'Loggeddatetime',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
  public function search($trial = 0, $paid = 0, $basic = 0) {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('forenames', $this->forenames, true);
        $criteria->compare('surname', $this->surname, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('dob', $this->dob, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('is_premium', $this->is_premium);
        $criteria->compare('tel', $this->tel, true);
        $criteria->compare('mobile', $this->mobile, true);
        $criteria->compare('current_job', $this->current_job, true);
        $criteria->compare('location', $this->location, true);
        $criteria->compare('location_lat', $this->location_lat, true);
        $criteria->compare('location_lng', $this->location_lng, true);
        $criteria->compare('remember_token', $this->remember_token, true);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->compare('updated_at', $this->updated_at, true);
        $criteria->compare('deleted_at', $this->deleted_at, true);
        $criteria->compare('stripe_active', $this->stripe_active);
        $criteria->compare('stripe_id', $this->stripe_id, true);
        $criteria->compare('stripe_subscription', $this->stripe_subscription, true);
        $criteria->compare('stripe_plan', $this->stripe_plan, true);
        $criteria->compare('last_four', $this->last_four, true);
        $criteria->compare('card_expiry', $this->card_expiry, true);
        $criteria->compare('card_expiry_sent', $this->card_expiry_sent, true);
        $criteria->compare('trial_ends_at', $this->trial_ends_at, true);
        $criteria->compare('subscription_ends_at', $this->subscription_ends_at, true);
        $criteria->compare('loggeddatetime',$this->loggeddatetime,true);


        if (Yii::app()->user->getState('role') == 'admin') {
            $criteria->condition = "type = 'candidate' AND deleted_at is null";

            if ($trial == 1)
                $criteria->addCondition('trial_ends_at is not null');
            else if ($paid == 1)
            {    
                $criteria->addCondition('stripe_active  > 0');
                $criteria->addCondition('subscription_ends_at is not null');
            }
            else if ($basic == 1)
            {    $criteria->addCondition('trial_ends_at is null');
                 $criteria->addCondition('stripe_id is null');
                $criteria->addCondition('stripe_active <= 0');
            }
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchByName($namearray) {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('forenames', $this->forenames, true);
        $criteria->compare('surname', $this->surname, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('dob', $this->dob, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('is_premium', $this->is_premium);
        $criteria->compare('tel', $this->tel, true);
        $criteria->compare('mobile', $this->mobile, true);
        $criteria->compare('current_job', $this->current_job, true);
        $criteria->compare('location', $this->location, true);
        $criteria->compare('location_lat', $this->location_lat, true);
        $criteria->compare('location_lng', $this->location_lng, true);
        $criteria->compare('remember_token', $this->remember_token, true);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->compare('updated_at', $this->updated_at, true);
        $criteria->compare('deleted_at', $this->deleted_at, true);
        $criteria->compare('stripe_active', $this->stripe_active);
        $criteria->compare('stripe_id', $this->stripe_id, true);
        $criteria->compare('stripe_subscription', $this->stripe_subscription, true);
        $criteria->compare('stripe_plan', $this->stripe_plan, true);
        $criteria->compare('last_four', $this->last_four, true);
        $criteria->compare('card_expiry', $this->card_expiry, true);
        $criteria->compare('card_expiry_sent', $this->card_expiry_sent, true);
        $criteria->compare('trial_ends_at', $this->trial_ends_at, true);
        $criteria->compare('subscription_ends_at', $this->subscription_ends_at, true);
        $criteria->compare('loggeddatetime',$this->loggeddatetime,true);

        $criteria->addInCondition('id', $namearray, 'AND');

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchAdmins() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('forenames', $this->forenames, true);
        $criteria->compare('surname', $this->surname, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('dob', $this->dob, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('is_premium', $this->is_premium);
        $criteria->compare('tel', $this->tel, true);
        $criteria->compare('mobile', $this->mobile, true);
        $criteria->compare('current_job', $this->current_job, true);
        $criteria->compare('location', $this->location, true);
        $criteria->compare('location_lat', $this->location_lat, true);
        $criteria->compare('location_lng', $this->location_lng, true);
        $criteria->compare('remember_token', $this->remember_token, true);
        $criteria->compare('created_at', $this->created_at, true);
        $criteria->compare('updated_at', $this->updated_at, true);
        $criteria->compare('deleted_at', $this->deleted_at, true);
        $criteria->compare('stripe_active', $this->stripe_active);
        $criteria->compare('stripe_id', $this->stripe_id, true);
        $criteria->compare('stripe_subscription', $this->stripe_subscription, true);
        $criteria->compare('stripe_plan', $this->stripe_plan, true);
        $criteria->compare('last_four', $this->last_four, true);
        $criteria->compare('card_expiry', $this->card_expiry, true);
        $criteria->compare('card_expiry_sent', $this->card_expiry_sent, true);
        $criteria->compare('trial_ends_at', $this->trial_ends_at, true);
        $criteria->compare('subscription_ends_at', $this->subscription_ends_at, true);
        $criteria->compare('loggeddatetime',$this->loggeddatetime,true);

        $criteria->addCondition("type = 'admin' and deleted_at is null");

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

 /*public function searchAlumini()
    {
        $name = isset($_GET['name']) ? $_GET['name'] : "";
        $skillcategid = isset($_GET['skill_category']) ? $_GET['skill_category'] : "";
        $gender = isset($_GET['gender']) ? $_GET['gender'] : "";
        $skillarr = isset($_GET['skills']) ? $_GET['skills'] : array();
        $location = isset($_GET['location']) ? $_GET['location'] : "";
        $lat = isset($_GET['latitude']) ? $_GET['latitude'] : "";
        $long = isset($_GET['longitude']) ? $_GET['longitude'] : "";
        $from_age = isset($_GET['from_age']) ? $_GET['from_age'] : "";
        $to_age = isset($_GET['to_age']) ? $_GET['to_age'] : "";
        $grade = isset($_GET['grade']) ? $_GET['grade'] : "";
        
        $from_job_cnt = isset($_GET['from_job_cnt']) ? $_GET['from_job_cnt'] : "";
        $to_job_cnt = isset($_GET['to_job_cnt']) ? $_GET['to_job_cnt'] : "";
        
        $from_job_view_cnt = isset($_GET['from_job_view_cnt']) ? $_GET['from_job_view_cnt'] : "";
        $to_job_view_cnt = isset($_GET['to_job_view_cnt']) ? $_GET['to_job_view_cnt'] : "";
        
        $from_profile_view_cnt = isset($_GET['from_profile_view_cnt']) ? $_GET['from_profile_view_cnt'] : "";
        $to_profile_view_cnt = isset($_GET['to_profile_view_cnt']) ? $_GET['to_profile_view_cnt'] : "";
        
        $skillids = implode(',', $skillarr);
        
        $cri = new CDbCriteria();
        $cri->alias = "u";
        $cri->distinct = true;
        
        $select = "u.forenames,u.surname,u.gender,u.id,u.status,uq.grade as loggeddatetime,(YEAR(CURDATE()) - YEAR(u.dob) - IF(STR_TO_DATE(CONCAT(YEAR(CURDATE()), '-', MONTH(u.dob), '-', DAY(u.dob)) ,'%Y-%c-%e') > CURDATE(), 1, 0)) as dob";
        $join = "inner join cv16_profiles p on p.owner_id = u.id
                      left outer join cv16_profile_skills ps on ps.profile_id = p.id
                      left outer join cv16_skills s on s.id = ps.skill_id
                      left outer join cv16_skill_categories sc on sc.id = s.skill_category_id
                      left outer join cv16_profile_videos pv on pv.profile_id = p.id
                      left outer join cv16_user_qualifications uq on uq.user_id = u.id
                      left outer join cv16_profile_views pview on pview.profile_id = p.id";

 
        if ($skillcategid > 0)
            $cri->addCondition("s.skill_category_id =" . $skillcategid);
        if ($skillids != "")
            $cri->addCondition('ps.skill_id in (' . $skillids . ')', 'AND');
        if ($lat != "") 
            $cri->addCondition("u.location_lat =".$lat." and u.location_lng=".$long);          
        if(trim($name) != "")
            $cri->addCondition("u.forenames like '%".$name."%' or u.surname like '%".$name."%'");
        if($gender != "")
            $cri->addCondition("u.gender ='".$gender."'");
        if($grade != "")
            $cri->addCondition("uq.grade ='".$grade."'");
        
        if($from_age != "" && $to_age != "")
        {            
            $cri->having = "dob > ".$from_age." and dob < ".$to_age;
        }
        elseif ($from_age != "")
        {
            $cri->having = "dob > ".$from_age;
        }
        elseif ($to_age != "")
        {
            $cri->having = "dob > ".$to_age;
        }
        
        $select .= ",(select count(user_id) from cv16_job_applications ja where ja.user_id = u.id) as created_at";              
        if($from_job_cnt != ""  && $to_job_cnt != "")
        {
           
           // $select .= ",count(ja.user_id) as created_at";
           // $join .= " inner join cv16_job_applications ja on ja.user_id = u.id";
           $cri->having = "created_at >= ".$from_job_cnt." and created_at <= ".$to_job_cnt;
        //   $cri->group = "ja.user_id";
         
        }
        elseif ($from_job_cnt != "")
        {
            $cri->having = "created_at > ".$from_job_cnt;
        }
        elseif ($from_job_cnt != "")
        {
            $cri->having = "created_at > ".$to_job_cnt;
        }
      
        $select .= ",(select sum(view_count) from cv16_job_views where model_type like '%candidate%' and model_id = u.id group by model_id) as updated_at";
        if($from_job_view_cnt != ""  && $to_job_view_cnt != "")
        {
            
            $cri->having = "updated_at >= ".$from_job_view_cnt." and updated_at <= ".$to_job_view_cnt;
           
        }
        elseif ($from_job_view_cnt != "")
        {
            $cri->having = "updated_at > ".$from_job_view_cnt;
        }
        elseif ($from_job_view_cnt != "")
        {
            $cri->having = "updated_at > ".$to_job_view_cnt;
        }
        
        
        $select .= ",(select sum(view_count) from cv16_profile_views where model_type like '%employer%' and profile_id = p.id group by profile_id) as display_gender";
        if($from_profile_view_cnt != ""  && $to_profile_view_cnt != "")
        {
            
            $cri->having = "display_gender >= ".$from_profile_view_cnt." and display_gender <= ".$to_profile_view_cnt;
            
        }
        elseif ($from_profile_view_cnt != "")
        {
            $cri->having = "display_gender > ".$from_profile_view_cnt;
        }
        elseif ($from_profile_view_cnt != "")
        {
            $cri->having = "display_gender > ".$to_profile_view_cnt;
        }
        
            $cri->addCondition("p.visibility = 1 and p.type like '%candidate%'");
            $cri->select = $select;
            $cri->join = $join;
            
//             echo var_dump($cri);
//                         die();
//           $cri->group = "u.id";
//             echo var_dump($cri);die();
        return new CActiveDataProvider($this, array(
            'criteria' => $cri,'pagination'=>false   
        ));
                
    }*/
    public function searchAlumini()
    {
        $name = isset($_GET['name']) ? $_GET['name'] : "";
        $skillcategid = isset($_GET['skill_category']) ? $_GET['skill_category'] : "";
        $gender = isset($_GET['gender']) ? $_GET['gender'] : "";
        $skillarr = isset($_GET['skills']) ? $_GET['skills'] : array();
        $location = isset($_GET['location']) ? $_GET['location'] : "";
        $lat = isset($_GET['latitude']) ? $_GET['latitude'] : "";
        $long = isset($_GET['longitude']) ? $_GET['longitude'] : "";
        $from_age = isset($_GET['from_age']) ? $_GET['from_age'] : "";
        $to_age = isset($_GET['to_age']) ? $_GET['to_age'] : "";
        $grade = isset($_GET['grade']) ? $_GET['grade'] : "";
        
        $from_job_cnt = isset($_GET['from_job_cnt']) ? $_GET['from_job_cnt'] : "";
        $to_job_cnt = isset($_GET['to_job_cnt']) ? $_GET['to_job_cnt'] : "";
        
        $from_job_view_cnt = isset($_GET['from_job_view_cnt']) ? $_GET['from_job_view_cnt'] : "";
        $to_job_view_cnt = isset($_GET['to_job_view_cnt']) ? $_GET['to_job_view_cnt'] : "";
        
        $from_profile_view_cnt = isset($_GET['from_profile_view_cnt']) ? $_GET['from_profile_view_cnt'] : "";
        $to_profile_view_cnt = isset($_GET['to_profile_view_cnt']) ? $_GET['to_profile_view_cnt'] : "";
        
        $skillids = implode(',', $skillarr);
        
        $cri = new CDbCriteria();
        $cri->alias = "u";
        $cri->distinct = true;
        
        $select = "u.forenames,u.surname,u.gender,u.id,u.status,uq.grade as loggeddatetime,(YEAR(CURDATE()) - YEAR(u.dob) - IF(STR_TO_DATE(CONCAT(YEAR(CURDATE()), '-', MONTH(u.dob), '-', DAY(u.dob)) ,'%Y-%c-%e') > CURDATE(), 1, 0)) as dob";
        $join = "inner join cv16_profiles p on p.owner_id = u.id
                      left outer join cv16_profile_skills ps on ps.profile_id = p.id
                      left outer join cv16_skills s on s.id = ps.skill_id
                      left outer join cv16_skill_categories sc on sc.id = s.skill_category_id
                      left outer join cv16_profile_videos pv on pv.profile_id = p.id
                      left outer join cv16_user_qualifications uq on uq.user_id = u.id
                      left outer join cv16_profile_views pview on pview.profile_id = p.id";

 
        if ($skillcategid > 0)
            $cri->addCondition("s.skill_category_id =" . $skillcategid);
        if ($skillids != "")
            $cri->addCondition('ps.skill_id in (' . $skillids . ')', 'AND');
        if ($lat != "") 
            $cri->addCondition("u.location_lat =".$lat." and u.location_lng=".$long);          
        if(trim($name) != "")
            $cri->addCondition("u.forenames like '%".$name."%' or u.surname like '%".$name."%'");
        if($gender != "")
            $cri->addCondition("u.gender ='".$gender."'");
        if($grade != "")
            $cri->addCondition("uq.grade ='".$grade."'");
        
        $having = "";
        if($from_age != "" && $to_age != "")
        {            
            $having .= "dob >= ".$from_age." and dob <= ".$to_age;
        }
        elseif ($from_age != "")
        {
            $having .= "dob >= ".$from_age;
        }
        elseif ($to_age != "")
        {
            $having .= "dob >= ".$to_age;
        }
        
        $select .= ",(select count(user_id) from cv16_job_applications ja where ja.user_id = u.id) as created_at";   
        
      
        if($from_job_cnt != ""  && $to_job_cnt != "")
        {
            if($having != "")
                $having .= " AND ";
           // $select .= ",count(ja.user_id) as created_at";
           // $join .= " inner join cv16_job_applications ja on ja.user_id = u.id";
            $having .= "created_at >= ".$from_job_cnt." and created_at <= ".$to_job_cnt;
        //   $cri->group = "ja.user_id";
        }
        elseif ($from_job_cnt != "")
        {
            if($having != "")
                $having .= " AND ";
            $having .= "created_at >= ".$from_job_cnt;
        }
        elseif ($from_job_cnt != "")
        {
            if($having != "")
                $having .= " AND ";
            $having .= "created_at >= ".$to_job_cnt;
        }
      
        $select .= ",(select sum(view_count) from cv16_job_views where model_type like '%candidate%' and model_id = u.id group by model_id) as updated_at";
               
       
        if($from_job_view_cnt != ""  && $to_job_view_cnt != "")
        {
            if($having != "")
                $having .= " AND ";
            $having .= "updated_at >= ".$from_job_view_cnt." and updated_at <= ".$to_job_view_cnt;
           
        }
        elseif ($from_job_view_cnt != "")
        {
            if($having != "")
                $having .= " AND ";
            $having .= "updated_at >= ".$from_job_view_cnt;
        }
        elseif ($from_job_view_cnt != "")
        {
            if($having != "")
                $having .= " AND ";
            $having .= "updated_at >= ".$to_job_view_cnt;
        }
        
        
        $select .= ",(select sum(view_count) from cv16_profile_views where model_type like '%employer%' and profile_id = p.id group by profile_id) as display_gender";
        
        
        
        
        if($from_profile_view_cnt != ""  && $to_profile_view_cnt != "")
        {
            if($having != "")
                $having .= " AND ";
            
            $having .= "display_gender >= ".$from_profile_view_cnt." and display_gender <= ".$to_profile_view_cnt;
            
        }
        elseif ($from_profile_view_cnt != "")
        {
            if($having != "")
                $having .= " AND ";
            $having .= "display_gender > ".$from_profile_view_cnt;
        }
        elseif ($from_profile_view_cnt != "")
        {
            if($having != "")
                $having .= " AND ";
            $having .= "display_gender > ".$to_profile_view_cnt;
        }
        
            $cri->addCondition("p.visibility = 1 and p.type like '%candidate%'");
            $cri->select = $select;
            $cri->join = $join;
            $cri->having = $having;
//             echo var_dump($cri);
//                         die();
//           $cri->group = "u.id";
//             echo var_dump($cri);die();
        return new CActiveDataProvider($this, array(
            'criteria' => $cri,'pagination'=>false   
        ));
                
    }
   public function searchPassport()
    {
        $video_cv = "";
        $profile_image = "";
        $profile_completed = "";
        $applied_jobs = "";
        $secured_jobs = "";
        $video_secured_jobs = "";
        
        if(isset($_GET['digital_passport']) && (urldecode($_GET['digital_passport']) == 'Video Profile'))
            $video_cv = 1;
        if(isset($_GET['digital_passport']) && (urldecode($_GET['digital_passport']) == 'Have Profile Image'))
            $profile_image = 1;
        if(isset($_GET['digital_passport']) && (urldecode($_GET['digital_passport']) == '50 percentage Profile Completed'))
            $profile_completed = 1;
        if(isset($_GET['digital_passport']) && (urldecode($_GET['digital_passport']) == 'Applied for Jobs'))
            $applied_jobs = 1;
        if(isset($_GET['digital_passport']) && (urldecode($_GET['digital_passport']) == 'Secured Jobs'))
            $secured_jobs = 1;
        if(isset($_GET['digital_passport']) && (urldecode($_GET['digital_passport']) == 'Secured Jobs By Video'))
             $video_secured_jobs = 1;
        
        $criteria = new CDbCriteria;
        $criteria->alias = "u";
        $criteria->distinct = true;
        
        $select = "u.forenames,u.surname,u.gender,u.id,u.email";
        $join = "inner join cv16_profiles p on p.owner_id = u.id";
      
        if($video_cv > 0 || $video_secured_jobs > 0)
            $join .= " inner join cv16_profile_videos pv on pv.profile_id = p.id";
        
        $criteria->select = $select;
                
        if($profile_image > 0)
            $criteria->addCondition('p.photo_id > 0');
        
        if($profile_completed > 0)
            $criteria->addCondition('p.profile_completed >= 50'); 
        
        if($applied_jobs > 0 || $secured_jobs > 0 || $video_secured_jobs > 0)
            $join .= " inner join cv16_job_applications ja on ja.user_id = u.id";
        
        if($secured_jobs > 0 || $video_secured_jobs > 0)
            $criteria->addCondition('ja.status = 4'); 
        
            
        $criteria->join = $join;
            
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,'pagination'=>false
        ));  
    }
    
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Users the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function getSlugURL()
    {
        $cri1 = new CDbCriteria();
        $cri1->condition = "owner_id =".$this->id." and type like '%candidate%'";
        $candprofile = Profiles::model()->find($cri1);
        return Yii::app()->createUrl("users/profile", array("slug"=>$candprofile['slug']));
    }
}
