<?php

/**
 * This is the model class for table "cv16_slideshow_slides".
 *
 * The followings are the available columns in table 'cv16_slideshow_slides':
 * @property string $id
 * @property string $slideshow_id
 * @property string $user_id
 * @property string $title
 * @property string $body
 * @property string $media_id
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property string $background_x
 * @property string $background_y
 *
 * The followings are the available model relations:
 * @property Media $media
 * @property Slideshows $slideshow
 */
class SlideshowSlides extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cv16_slideshow_slides';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('slideshow_id, body', 'required'),
			array('slideshow_id, user_id, media_id', 'length', 'max'=>10),
			array('title', 'length', 'max'=>255),
			array('background_x, background_y', 'length', 'max'=>50),
			array('created_at, updated_at, deleted_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, slideshow_id, user_id, title, body, media_id, created_at, updated_at, deleted_at, background_x, background_y', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'media' => array(self::BELONGS_TO, 'Media', 'media_id'),
			'slideshow' => array(self::BELONGS_TO, 'Slideshows', 'slideshow_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'slideshow_id' => 'Slideshow',
			'user_id' => 'User',
			'title' => 'Title',
			'body' => 'Body',
			'media_id' => 'Media',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'deleted_at' => 'Deleted At',
			'background_x' => 'Background X',
			'background_y' => 'Background Y',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('slideshow_id',$this->slideshow_id,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('body',$this->body,true);
		$criteria->compare('media_id',$this->media_id,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('deleted_at',$this->deleted_at,true);
		$criteria->compare('background_x',$this->background_x,true);
		$criteria->compare('background_y',$this->background_y,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
  
	public function searchByID($id)
	{
	    // @todo Please modify the following code to remove attributes that should not be searched.
	    
	    $criteria=new CDbCriteria;
	    
	    $criteria->compare('id',$this->id,true);
	    $criteria->compare('slideshow_id',$this->slideshow_id,true);
	    $criteria->compare('user_id',$this->user_id,true);
	    $criteria->compare('title',$this->title,true);
	    $criteria->compare('body',$this->body,true);
	    $criteria->compare('media_id',$this->media_id,true);
	    $criteria->compare('created_at',$this->created_at,true);
	    $criteria->compare('updated_at',$this->updated_at,true);
	    $criteria->compare('deleted_at',$this->deleted_at,true);
	    $criteria->compare('background_x',$this->background_x,true);
	    $criteria->compare('background_y',$this->background_y,true);
	    
	    $criteria->addCondition('slideshow_id = '.$id);
	    
	    return new CActiveDataProvider($this, array(
	        'criteria'=>$criteria,
	    ));
	}
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SlideshowSlides the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
