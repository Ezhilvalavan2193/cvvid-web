<?php

/**
 * This is the model class for table "cv16_employer_offices".
 *
 * The followings are the available columns in table 'cv16_employer_offices':
 * @property string $id
 * @property integer $employer_id
 * @property integer $address_id
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 */
class EmployerOffices extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cv16_employer_offices';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('address_id', 'numerical', 'integerOnly'=>true),
			array('status', 'length', 'max'=>255),
			array('created_at, deleted_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, address_id,employer_id, status, created_at,updated_at deleted_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		    'employerOffices' => array(self::HAS_MANY, 'EmployerOffices', 'employer_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'address_id' => 'Address',
			'status' => 'Status',
			'created_at' => 'Created At',
		    'updated_at' => 'Updated At',
			'deleted_at' => 'Deleted At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('address_id',$this->address_id);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('deleted_at',$this->deleted_at,true);
		$criteria->addCondition('deleted_at is null');
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function searchByID($id)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('address_id',$this->address_id);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('deleted_at',$this->deleted_at,true);
		// $criteria->addCondition('deleted_at is null');
		$criteria->addCondition('deleted_at is null and employer_id ='.$id);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return InstitutionOffices the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function getAddress()
	{
	    $addrmodel = Addresses::model()->findByPk($this->address_id);
	    $addr = "";
	    if($addrmodel->address != "")	        
	       $addr .= $addrmodel->address;
	    if($addrmodel->town != "")
	       $addr .= ",".$addrmodel->town;
        if($addrmodel->county != "")
            $addr .= ",".$addrmodel->county;
        if($addrmodel->postcode != "")
            $addr .= ",".$addrmodel->postcode;
        if($addrmodel->country != "")
            $addr .= ",".$addrmodel->country;
        
        return $addr;
	}
}
