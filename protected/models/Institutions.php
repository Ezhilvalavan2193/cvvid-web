<?php

/**
 * This is the model class for table "cv16_institutions".
 *
 * The followings are the available columns in table 'cv16_institutions':
 * @property string $id
 * @property string $admin_id
 * @property string $name
 * @property string $email
 * @property string $body
 * @property string $address_1
 * @property string $address_2
 * @property string $town
 * @property string $post_code
 * @property string $status
 * @property integer $num_pupils
 * @property integer $stripe_active
 * @property string $stripe_id
 * @property string $stripe_subscription
 * @property string $stripe_plan
 * @property string $last_four
 * @property string $card_expiry
 * @property string $card_expiry_sent
 * @property string $trial_ends_at
 * @property string $subscription_ends_at
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 * @property string $payment_type
 * @property string $amount
 * @property string $additional_amount
 *
 * The followings are the available model relations:
 * @property InstitutionUsers[] $institutionUsers
 * @property TutorGroups[] $tutorGroups
 * @property TutorGroupsStudents[] $tutorGroupsStudents
 */
class Institutions extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cv16_institutions';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required','message'=>'The school/institution {attribute} field is required.'),
			array('num_pupils', 'required','message'=>'The number of pupils field is required'),
		        array('email', 'unique','attributeName' => 'email','message'=>'This Email is already taken by an account'),
			array('num_pupils, stripe_active', 'numerical', 'integerOnly'=>true),
			array('admin_id', 'length', 'max'=>10),
			array('name, email, address_1, address_2, town, post_code, status, stripe_id, stripe_subscription', 'length', 'max'=>255),
			array('stripe_plan', 'length', 'max'=>100),
			array('last_four', 'length', 'max'=>4),
			array('body, card_expiry, card_expiry_sent, trial_ends_at, subscription_ends_at, created_at, updated_at, deleted_at', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, admin_id, name, email, body,payment_type,amount,additional_amount, address_1, address_2, town, post_code, status, num_pupils, stripe_active, stripe_id, stripe_subscription, stripe_plan, last_four, card_expiry, card_expiry_sent, trial_ends_at, subscription_ends_at, created_at, updated_at, deleted_at', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'institutionUsers' => array(self::HAS_MANY, 'InstitutionUsers', 'institution_id'),
			'tutorGroups' => array(self::HAS_MANY, 'TutorGroups', 'institution_id'),
			'tutorGroupsStudents' => array(self::HAS_MANY, 'TutorGroupsStudents', 'institution_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'admin_id' => 'Admin',
			'name' => 'Name',
			'email' => 'Email',
			'body' => 'Body',
			'address_1' => 'Address 1',
			'address_2' => 'Address 2',
			'town' => 'Town',
			'post_code' => 'Post Code',
			'status' => 'Status',
			'num_pupils' => 'Num Pupils',
			'stripe_active' => 'Stripe Active',
			'stripe_id' => 'Stripe',
			'stripe_subscription' => 'Stripe Subscription',
			'stripe_plan' => 'Stripe Plan',
			'last_four' => 'Last Four',
			'card_expiry' => 'Card Expiry',
			'card_expiry_sent' => 'Card Expiry Sent',
			'trial_ends_at' => 'Trial Ends At',
			'subscription_ends_at' => 'Subscription Ends At',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'deleted_at' => 'Deleted At',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('admin_id',$this->admin_id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('body',$this->body,true);
		$criteria->compare('address_1',$this->address_1,true);
		$criteria->compare('address_2',$this->address_2,true);
		$criteria->compare('town',$this->town,true);
		$criteria->compare('post_code',$this->post_code,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('num_pupils',$this->num_pupils);
		$criteria->compare('stripe_active',$this->stripe_active);
		$criteria->compare('stripe_id',$this->stripe_id,true);
		$criteria->compare('stripe_subscription',$this->stripe_subscription,true);
		$criteria->compare('stripe_plan',$this->stripe_plan,true);
		$criteria->compare('last_four',$this->last_four,true);
		$criteria->compare('card_expiry',$this->card_expiry,true);
		$criteria->compare('card_expiry_sent',$this->card_expiry_sent,true);
		$criteria->compare('trial_ends_at',$this->trial_ends_at,true);
		$criteria->compare('subscription_ends_at',$this->subscription_ends_at,true);
		$criteria->compare('created_at',$this->created_at,true);
		$criteria->compare('updated_at',$this->updated_at,true);
		$criteria->compare('deleted_at',$this->deleted_at,true);

		$criteria->addCondition('deleted_at is null');
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function searchByName($namearray)
	{
	    // @todo Please modify the following code to remove attributes that should not be searched.
	    
	    $criteria=new CDbCriteria;
	    
	    $criteria->compare('id',$this->id,true);
	    $criteria->compare('admin_id',$this->admin_id,true);
	    $criteria->compare('name',$this->name,true);
	    $criteria->compare('email',$this->email,true);
	    $criteria->compare('body',$this->body,true);
	    $criteria->compare('address_1',$this->address_1,true);
	    $criteria->compare('address_2',$this->address_2,true);
	    $criteria->compare('town',$this->town,true);
	    $criteria->compare('post_code',$this->post_code,true);
	    $criteria->compare('status',$this->status,true);
	    $criteria->compare('num_pupils',$this->num_pupils);
	    $criteria->compare('stripe_active',$this->stripe_active);
	    $criteria->compare('stripe_id',$this->stripe_id,true);
	    $criteria->compare('stripe_subscription',$this->stripe_subscription,true);
	    $criteria->compare('stripe_plan',$this->stripe_plan,true);
	    $criteria->compare('last_four',$this->last_four,true);
	    $criteria->compare('card_expiry',$this->card_expiry,true);
	    $criteria->compare('card_expiry_sent',$this->card_expiry_sent,true);
	    $criteria->compare('trial_ends_at',$this->trial_ends_at,true);
	    $criteria->compare('subscription_ends_at',$this->subscription_ends_at,true);
	    $criteria->compare('created_at',$this->created_at,true);
	    $criteria->compare('updated_at',$this->updated_at,true);
	    $criteria->compare('deleted_at',$this->deleted_at,true);
	    
	    $criteria->addInCondition('admin_id', $namearray,'AND');
	   
	    return new CActiveDataProvider($this, array(
	        'criteria'=>$criteria,
	    ));
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Institutions the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	public function getAdmin()
	{
	    if($this->admin_id > 0)
	    {
	        $usermodel = Users::model()->findByPk($this->admin_id);
	        return $usermodel->forenames." ".$usermodel->surname;
	    }
	}
	
	public function getRoute()
	{
	    if($this->stripe_active == 1)
	        return "institutions/tabeditinstitute";
	    else 
	        return "institutions/editDetails";
	}
	public function searchByPending()
	{
	    // @todo Please modify the following code to remove attributes that should not be searched.
	    
	    $criteria=new CDbCriteria;
	    
	    $criteria->compare('id',$this->id,true);
	    $criteria->compare('admin_id',$this->admin_id,true);
	    $criteria->compare('name',$this->name,true);
	    $criteria->compare('email',$this->email,true);
	    $criteria->compare('body',$this->body,true);
	    $criteria->compare('address_1',$this->address_1,true);
	    $criteria->compare('address_2',$this->address_2,true);
	    $criteria->compare('town',$this->town,true);
	    $criteria->compare('post_code',$this->post_code,true);
	    $criteria->compare('status',$this->status,true);
	    $criteria->compare('num_pupils',$this->num_pupils);
	    $criteria->compare('stripe_active',$this->stripe_active);
	    $criteria->compare('stripe_id',$this->stripe_id,true);
	    $criteria->compare('stripe_subscription',$this->stripe_subscription,true);
	    $criteria->compare('stripe_plan',$this->stripe_plan,true);
	    $criteria->compare('last_four',$this->last_four,true);
	    $criteria->compare('card_expiry',$this->card_expiry,true);
	    $criteria->compare('card_expiry_sent',$this->card_expiry_sent,true);
	    $criteria->compare('trial_ends_at',$this->trial_ends_at,true);
	    $criteria->compare('subscription_ends_at',$this->subscription_ends_at,true);
	    $criteria->compare('created_at',$this->created_at,true);
	    $criteria->compare('updated_at',$this->updated_at,true);
	    $criteria->compare('deleted_at',$this->deleted_at,true);
	    
	    $criteria->addCondition('status="pending"');
	    
	    return new CActiveDataProvider($this, array(
	        'criteria'=>$criteria,
	    ));
	}
	
	public function getExpiryDate()
	{
	    $cri = new CDbCriteria();
	    $cri->condition = "owner_type like '%Institution%' and owner_id =".$this->id;
	    $planmodel = Plans::model()->find($cri);
	    if($planmodel == null)
	        return "";
	    else
	       return  $planmodel->active_to;
	    
	}
	public function getColor()
	{
	    $cri = new CDbCriteria();
	    $cri->condition = "owner_type like '%Institution%' and owner_id =".$this->id;
	    $planmodel = Plans::model()->find($cri);
	    
	    $now = new DateTime();
	    $currdate = $now->format('Y-m-d H:i:s');	   
	    $expdate = new DateTime($planmodel->active_to);
	    $interval = date_diff($now, $expdate);
	    if($interval->format('%m') <= 2)
	        return "red";
	    else 
	        return "green";
	  
	}
}
