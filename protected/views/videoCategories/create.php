<?php
/* @var $this VideoCategoriesController */
/* @var $model VideoCategories */

$this->breadcrumbs=array(
	'Video Categories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List VideoCategories', 'url'=>array('index')),
	array('label'=>'Manage VideoCategories', 'url'=>array('admin')),
);
?>

<h1>Create VideoCategories</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>