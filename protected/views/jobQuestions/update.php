<?php
/* @var $this JobQuestionsController */
/* @var $model JobQuestions */

$this->breadcrumbs=array(
	'Job Questions'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List JobQuestions', 'url'=>array('index')),
	array('label'=>'Create JobQuestions', 'url'=>array('create')),
	array('label'=>'View JobQuestions', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage JobQuestions', 'url'=>array('admin')),
);
?>

<h1>Update JobQuestions <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>