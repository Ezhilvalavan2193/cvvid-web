<?php
/* @var $this JobQuestionsController */
/* @var $model JobQuestions */

$this->breadcrumbs=array(
	'Job Questions'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List JobQuestions', 'url'=>array('index')),
	array('label'=>'Create JobQuestions', 'url'=>array('create')),
	array('label'=>'Update JobQuestions', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete JobQuestions', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage JobQuestions', 'url'=>array('admin')),
);
?>

<h1>View JobQuestions #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'job_id',
		'user_id',
		'question',
		'status',
		'created_at',
		'updated_at',
	),
)); ?>
