<?php
/* @var $this JobQuestionsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Job Questions',
);

$this->menu=array(
	array('label'=>'Create JobQuestions', 'url'=>array('create')),
	array('label'=>'Manage JobQuestions', 'url'=>array('admin')),
);
?>

<h1>Job Questions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
