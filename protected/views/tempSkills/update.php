<?php
/* @var $this TempSkillsController */
/* @var $model TempSkills */

$this->breadcrumbs=array(
	'Temp Skills'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TempSkills', 'url'=>array('index')),
	array('label'=>'Create TempSkills', 'url'=>array('create')),
	array('label'=>'View TempSkills', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TempSkills', 'url'=>array('admin')),
);
?>

<h1>Update TempSkills <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>