<?php
/* @var $this TempSkillsController */
/* @var $model TempSkills */

$this->breadcrumbs=array(
	'Temp Skills'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TempSkills', 'url'=>array('index')),
	array('label'=>'Create TempSkills', 'url'=>array('create')),
	array('label'=>'Update TempSkills', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TempSkills', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TempSkills', 'url'=>array('admin')),
);
?>

<h1>View TempSkills #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'employer_id',
		'skill_category_id',
		'skill_id',
		'active',
	),
)); ?>
