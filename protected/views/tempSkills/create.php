<?php
/* @var $this TempSkillsController */
/* @var $model TempSkills */

$this->breadcrumbs=array(
	'Temp Skills'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TempSkills', 'url'=>array('index')),
	array('label'=>'Manage TempSkills', 'url'=>array('admin')),
);
?>

<h1>Create TempSkills</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>