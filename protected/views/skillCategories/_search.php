<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */
/* @var $form CActiveForm */
?>


<?php
$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl('skillCategories/searchAdmin'),
    'method' => 'get',
        ));
?>
<div class="form-inline">
    <?php echo $form->textField($model, 'name', array('class' => 'form-control')); ?>
    <?php
    echo CHtml::submitButton('Search', array("class" => 'btn btn-primary'));
    //echo CHtml::button('Clear',array("class"=>'btn-search',"onClick"=>"window.location='".Yii::app()->createUrl('skillCategories/admin',array())."'"));
    ?>

</div>
<?php $this->endWidget(); ?>

