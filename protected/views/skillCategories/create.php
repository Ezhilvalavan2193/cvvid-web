<?php
/* @var $this SkillCategoriesController */
/* @var $model SkillCategories */

$this->breadcrumbs=array(
	'Skill Categories'=>array('index'),
	'Create',
);

$this->menu=array(
	//array('label'=>'List SkillCategories', 'url'=>array('index')),
	array('label'=>'Manage SkillCategories', 'url'=>array('admin')),
);
?>


<?php $this->renderPartial('_form', array('model'=>$model)); ?>