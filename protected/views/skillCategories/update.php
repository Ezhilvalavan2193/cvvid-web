<?php
/* @var $this SkillCategoriesController */
/* @var $model SkillCategories */

$this->breadcrumbs=array(
	'Skill Categories'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List SkillCategories', 'url'=>array('index')),
	array('label'=>'Create SkillCategories', 'url'=>array('create')),
	array('label'=>'View SkillCategories', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage SkillCategories', 'url'=>array('admin')),
);
?>

<h1>Update SkillCategories <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>