<div class="form-group">
    <label class="control-label">Street Address</label>
    <?php echo $form->textField($addressmodel, 'address', array('class' => 'form-control', 'placeholder' => 'Address','id'=>'route')); ?>
</div>

<div class="form-group">
    <label class="control-label">Post Code</label>
    <?php echo $form->textField($addressmodel, 'postcode', array('class' => 'form-control location-search', 'placeholder' => 'Post Code','id'=>'postal_code')); ?>
     <input id="location_lat" name="Addresses[latitude]" type="hidden" value="<?php echo $addressmodel->latitude ?>">
    <input id="location_lng" name="Addresses[longitude]" type="hidden" value="<?php echo $addressmodel->longitude ?>">
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="control-label">City</label>
            <?php echo $form->textField($addressmodel, 'town', array('class' => 'form-control', 'placeholder' => 'City','id'=>'postal_town')); ?>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="control-label">County</label>
            <?php echo $form->textField($addressmodel, 'county', array('class' => 'form-control', 'placeholder' => 'County','id'=>'administrative_area_level_2')); ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="control-label">Country</label>
            <?php echo $form->textField($addressmodel, 'country', array('class' => 'form-control', 'placeholder' => 'Country','id'=>'country')); ?>
        </div>
    </div>
</div>