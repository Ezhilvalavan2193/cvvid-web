<?php

/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>

<?php

$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl('jobs/searchAdmin'), //Yii::app()->createUrl($this->route),
    'method' => 'get',
        ));
?>

<?php echo $form->textField($model, 'title', array('class' => 'form-control','placeholder'=>'Search')); ?>

<?php

echo CHtml::submitButton('Search', array("class" => "btn btn-primary"));
?>

<?php $this->endWidget(); ?>
