<?php
/* @var $this JobsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    'Jobs',
);

?>

<div id="page-content">
<div class="container">
 <h1 align="center">Jobs by Location</h1>
 <hr>
<ul class="job-by-list clearfix">
<?php 
   $cri = new CDbCriteria();
   $cri->select = "SUBSTRING_INDEX(location, ',', 1) as location,location as title";
   $cri->distinct = true;
   $cri->condition = "location is not null and location <> ''";
   $locmodel = Jobs::model()->findAll($cri);
   foreach ($locmodel as $loc):
   $firstloc = str_replace(" ", "-", $loc['location']);
    $location = $firstloc;
?> 
       <li class="jobs-li"> <a href="<?php echo $this->createUrl('site/searchLocation',array('location'=>$location)) ?>"><?php echo "Jobs in ".$loc['location'] ?> </a></li>
       
<?php endforeach; ?>   
 </ul>
 </div></div>
    