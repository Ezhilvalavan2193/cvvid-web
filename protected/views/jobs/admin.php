<?php
/* @var $this JobsController */
/* @var $model Jobs */

$this->breadcrumbs=array(
	'Jobs'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#jobs-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<script>
$(document).ready(function(){
	

	$('#delete-job').click(function(e){

		
         if($("#jobs-grid").find("input:checked").length > 0)
         {

        	 if (confirm('Are you sure you want to delete these jobs?')) {

         		 var ids = $.fn.yiiGridView.getChecked("jobs-grid", "selectedIds");
             	
       			  $.ajax({
       		            url: '<?php echo $this->createUrl('jobs/deleteJobs') ?>',
       		            type: 'POST',
       		            data: {ids : ids}, 	    
       		            success: function(data) {
                                $.fn.yiiGridView.update('jobs-grid');
       			            //alert(data);
       		            },
                            error: function(data) {		
                                alert('err');
                            }
       		         });
        	 } 
        	
         }
         else
         {
            alert('Please select at least one item');
         }
	});
	
});

</script>
  <?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
       <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
    <?php endif; ?>
<h1>Jobs</h1>
<div class="Admin__content__inner">
    <div class="AdminFilters">
        <div class="form-inline">
          <?php $this->renderPartial('_search',array(
                    'model'=>$model,
            )); ?>
            <div class="pull-right">
                   <?php  
                        echo CHtml::button('Create',array("class"=>"btn btn-primary","onclick"=>"window.location='".Yii::app()->createUrl('jobs/create')."'"));
                        echo CHtml::button('Delete',array("class"=>"btn btn-primary",'id'=>'delete-job'));
                  ?>
            </div>
        </div>
    </div>

  <?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'jobs-grid',
        'itemsCssClass' => 'table',
	'summaryText'=>'',
	//'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',
    'pager' => array(
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'header'=>'',
        'firstPageLabel' => '<<',
        'lastPageLabel' => '>>',
        'nextPageLabel' => '>>',
        'prevPageLabel' => '<<'),
			'ajaxUpdate'=>false,
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
			//'id',
                       array(
                            // 'name' => 'check',
                             'id' => 'selectedIds',
                             'value' => '$data->id',
                             'class' => 'CCheckBoxColumn',
                             'selectableRows'  => 2,
                            // 'checked'=>'Yii::app()->user->getState($data->id)',
                            // 'checkBoxHtmlOptions' => array('name' => 'idList[]'),

                         ),
			array(
					'header'=>'Name',
					'value'=>'CHtml::link("<i class=\"fa fa-eye\"></i> ".$data->title,Yii::app()->createUrl("jobs/jobsView",array("id"=>$data->id)),array("class"=>"jobs-edit-button","id"=>"jobs-edit"))',
			                'type'=>'raw'
			),
			array(
					'header'=>'Edit',
					'value'=>'CHtml::link("<i class=\"fa fa-pencil\"></i> "."Edit",Yii::app()->createUrl("jobs/updateJobs", array("id"=>$data->id)),array("class"=>"profile-view-button","id"=>"profile-view"))',
					'type'=>'raw'
			),
			array(
					'header'=>'Company',
					'value'=>'$data->employer->name',
			),
			array(
					'header'=>'End Date',
					'value'=>'$data->end_date',
			),
			array(
					'header'=>'Salary Min',
					'value'=>'"£ ".$data->salary_min',
			),
			array(
					'header'=>'Salary Max',
					'value'=>'"£ ".$data->salary_max',
			),
    	    array(
    	        'header'=>'Duplicate',
    	        'value'=>'CHtml::link("Duplicate",Yii::app()->createUrl("jobs/duplicateJobs", array("id"=>$data->id)),array())',
    	        'type'=>'raw'
    	    ),
		
	),
)); ?>

</div>
