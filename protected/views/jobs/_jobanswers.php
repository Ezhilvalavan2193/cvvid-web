<?php
/* @var $this UsersController */
/* @var $model Users */

?>
<script>
$(document).ready(function () {
	
	 $('body').on('click', '.reply-message', function () {
         //alert(getValue);return false;
         $('#NewConversationModal').addClass('in');
         $('body').addClass('modal-open');
         $('#NewConversationModal').show();
         
         $('#target_id').val($(this).attr('id'));
         
     });
	 
	 $('#NewConversationModal').on('click', '.SendMessageBtn', function () {

         var errors = false;
         var errors_mgs = '';
         var $subject = $('input[id="subject"]');
         var subject_val = $subject.val();
         var $target_id = $('input[id="target_id"]');
         var target_val = $target_id.val();
         var $message = $('textarea[id="message"]')
         var message_val = $message.val();
         // Validate Subject
         if (target_val == '') {
             errors_mgs += '<div>The target field is required.</div>';
             errors = true;
         } else {
             errors = false;
         }

         // Validate Subject
         if (subject_val == '') {
             errors_mgs += '<div>The subject is required</div>';
             errors = true;
         } else {
             errors = false;
         }

         // Validate Message
         if (message_val == '') {
             errors_mgs += '<div>A message is required</div>';
             errors = true;
         } else {
             errors = false;
         }
         if (!errors) {
             $('.modal-loader').show();
             $.ajax({
                 url: '<?php echo $this->createUrl('jobs/replyInterviewAnswer/id/') ?>/'+target_val,
                 type: 'POST',
                 data: {message: message_val, subject: subject_val, user_id: target_val},
                 success: function (data) {
                     $('#msg_scc_msg').show();
                     $('.modal-loader').hide();
                     setTimeout(function () {
                         closepopup();
                     }, 3000);
                 },
             });
         } else {
             $('#msg_err_msg').html(errors_mgs);
             $('#msg_err_msg').show();
         }



         return false;
     });
	 
	 
    $('body').on('click', '.view_ans_btn', function (e) {

		var qid = $(this).attr('id');
		var uid = $("#"+qid+"_user_id").val();

    	$.ajax({
            url: '<?php echo $this->createUrl('jobs/getAnswers',array('id'=>$model->id)) ?>',
            type: 'POST',
            data: {qid : qid , uid : uid},
            success: function (response) {
                
                var obj = JSON.parse(response);
            	$('#AnswerModal').addClass('in');
                $('body').addClass('modal-open');
                $('#AnswerModal').show();
                
            	jwplayer("my-video").setup({
                    'playlist': obj
//                    width: "100%",
//                    height: "100%",
                   // 'primary': 'flash',
                    //'autostart': 'true'
                   });
                
            	
            },
           error: function (data) {
               alert('err');
           }
        });
        
    	
        
    });
    
	$('body').on('click', '#save-question', function (e) {
       
		var value = $("#question").val();
		if($.trim($("#question").val()) != "")
		{
    		$.ajax({
                url: '<?php echo $this->createUrl('jobs/saveQuestion',array('id'=>$model->id)) ?>',
                type: 'POST',
                data: {value: value},
                success: function (response) {
                	$.fn.yiiGridView.update('emp-ques-grid');
                },
               error: function (data) {
                   alert('err');
               }
            });
		}
		else
			$("#question").css("border-color","red");
    });
    
	$('body').on('click', '.edit-question', function (e) {
	       
		var id = $(this).attr("id");
		$("#job_question_id").val(id);
		
		var value = $(this).closest("tr").find("td:nth-child(2)").text();
		$("#question").val(value);
		$('#QuestionModal').addClass('in');
        $('body').addClass('modal-open');
        $('#QuestionModal').show();
        
        $("#save-question").attr("id","update-question");
    });
	
	$('body').on('click', '#update-question', function (e) {
	       
		var value = $("#question").val();
		
		if($.trim($("#question").val()) != "")
		{
			var id = $("#job_question_id").val();
			
			if(id > 0)
			{
        		$.ajax({
                    url: '<?php echo $this->createUrl('jobs/updateQuestion') ?>',
                    type: 'POST',
                    data: {value: value, id: id},
                    success: function (response) {
                    	$.fn.yiiGridView.update('emp-ques-grid');
                    },
                   error: function (data) {
                       alert('err');
                   }
                });
			}
			else
				location.reload();
		}
		else
			$("#question").css("border-color","red");
    });
	
	$('body').on('click', '.delete-question', function (e) {
	       
		var id = $(this).attr("id");
		if (confirm('Do you want to delete this item ?')) {
    		$.ajax({
                url: '<?php echo $this->createUrl('jobs/deleteQuestion') ?>',
                type: 'POST',
                data: {id: id},
                success: function (response) {
                	$.fn.yiiGridView.update('emp-ques-grid');
                },
               error: function (data) {
                   alert('err');
               }
            });
		}
    });
	
});   

</script>
 <style>
                                                                                                                                                                                                                                                                                                                                                                                                                            
#my-video {
font-size: 0.9em;
height: 0;
overflow: hidden;
padding-bottom: 53%;
padding-top: 20px;
position: relative;
}
#my-video embed, #my-video object, #my-video iframe {
max-height: 100%;
max-width: 100%;
height: 100%;
left: 0pt;
position: absolute;
top: 0pt;
width: 100%;
}
.jwplayer {
    width: 100%!important;
    }
</style>

<div class="page-title text-left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Questions</h1>
            </div>
        </div>
    </div>
</div>
<div id="page-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
              <?php if (Yii::app()->user->hasFlash('success')): ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?php echo Yii::app()->user->getFlash('success'); ?>
                    </div>
                <?php endif; ?>
                <div class="JobApplications__job">

                    <div class="Job">
                       <div class="Job__header clearfix">
                            <div class="pull-left">
                                <?php echo $model->title; ?><br/>
                                <div class="Job__deadline">Deadline: <?php echo $model->end_date; ?></div>
                            </div>
                            <div class="pull-right">
                                <form method="POST" action="<?php echo $this->createUrl('jobs/jobstatus',array('id'=>$model->id)); ?>" accept-charset="UTF-8" class="JobStatus__form">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <?php 
                                                 if($model->status=='active')
                                                    echo "Active";
                                                 else
                                                    echo "On Hold";
                                            ?> <span class="caret"></span>
                                            <input name="status" type="hidden">
                                        </button>
                                        <?php 
                                        $cri = new CDbCriteria();
                                        $cri->condition = "owner_id =".$model->owner_id." and type like '%employer%'";
                                        $profilemodel = Profiles::model()->find($cri);
                                        ?>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" class="JobBtn__active">Active</a></li>
                                            <li><a href="#" class="JobBtn__hold">On Hold</a></li>
                                            <li><a href="<?php echo $this->createUrl('employers/jobsedit',array('slug'=>$profilemodel->slug,'id'=>$model->id)); ?>">Extend</a></li>
                                        </ul>
                                    </div><!-- .btn-group -->
                                </form>                         
                            </div>
                        </div>
                        <div class="Job__meta">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="Job__details">
                                        <ul class="row list-unstyled">
                                             <?php if($model->salary_min > 0){ ?>
                                            <li class="col-sm-4"><?php echo Yii::app()->numberFormatter->formatCurrency($model->salary_min, 'GBP')." to ".Yii::app()->numberFormatter->formatCurrency($model->salary_max, 'GBP')." per ".($model->salary_type == "annual" ? "annum" : "hour")?></li>
                                            <?php } ?>
                                            <li class="col-sm-4"><?php echo $model->type ?></li>
                                             <?php 
                                                $date1 = new DateTime();
                                                $date2 = new DateTime($model->created_at);
                                                
                                                $interval= $date1->diff($date2);
                                                $hourdiff = ($interval->days * 24) + $interval->h;
                                                if($hourdiff <= 23)
                                                    $postedtime = $hourdiff." hours ago";
                                                else 
                                                {
                                                    $daysdiff = $date2->diff($date1)->format("%a");
                                                    if($daysdiff <= 28)
                                                        $postedtime = $daysdiff . " days ago";
                                                    else             
                                                    {
                                                        
                                                        $postedtime = ($daysdiff / 30). " months ago";
                                                    }
                                                }
                                                
                                                
            //                                     $postedtime = $diff;
                                               // echo "diff=".$diff;
                                                
                                            ?>
                                            <li class="col-sm-4">Posted: <?php echo timeAgo($model->created_at); ?></li>
                                            <li class="col-sm-4"><?php echo $model->location ?></li>
                                            <?php if($model->vacancies > 0){ ?>
                                            	<li class="col-sm-4"><?php echo $model->vacancies." vacancies" ?></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-sm-12">

                <div class="JobApplications__applications">
                      <?php
                        $cri3 = new CDbCriteria();
                        $cri3->condition = "status < 1 and job_id = ".$model->id;
                        $count2 = count(JobApplications::model()->findAll($cri3));
                        ?>
                    <ul class="nav nav-tabs">
                        <li role="presentation">
                            <a href="<?php echo Yii::app()->createUrl("jobs/applications",array("id"=>$model->id,"title"=>  str_replace(" ", "-", $model->title))); ?>"><i class="fa fa-inbox"></i>Applications (<?php echo $count2; ?>)</a>
                        </li>
                        <li role="presentation">
                            <a href="<?php echo Yii::app()->createUrl("jobs/shortlists",array("id"=>$model->id,"title"=>  str_replace(" ", "-", $model->title))); ?>"><i class="fa fa-star"></i>Shortlisted (<?php echo JobApplications::model()->getShortCount($model->id); ?>)</a>
                        </li>
                        <li role="presentation">
                            <a href="<?php echo Yii::app()->createUrl("jobs/interviews",array("id"=>$model->id,"title"=>  str_replace(" ", "-", $model->title))); ?>"><i class="fa fa-user"></i>Interviewees (<?php
                            $cri1 = new CDbCriteria();
                            $cri1->condition = "status = 2 and job_id = ".$model->id;
                            $count = count(JobApplications::model()->findAll($cri1));
                            echo $count; ?>)</a>
                        </li>
                           <li role="presentation">
                            <a href="<?php echo Yii::app()->createUrl("jobs/appointments",array("id"=>$model->id,"title"=>  str_replace(" ", "-", $model->title))); ?>"><i class="fa fa-calendar"></i>Appointments (<?php echo JobApplications::model()->getAppointmentsCount($model->id); ?>)</a>
                        </li>
                        <li role="presentation">
                            <a href="<?php echo Yii::app()->createUrl("jobs/questions",array("id"=>$model->id,"title"=>  str_replace(" ", "-", $model->title))); ?>"><i class="fa fa-question"></i>Questions</a>
                        </li>
                         <li role="presentation" class="active">
                            <a href="<?php echo Yii::app()->createUrl("jobs/answers",array("id"=>$model->id,"title"=>  str_replace(" ", "-", $model->title))); ?>"><i class="fa fa-star"></i>Answers (<?php echo JobApplications::model()->getAnswersCount($model->id); ?>)</a>
                        </li>
                    </ul>                    
<!--                     <form method="GET" action="http://localhost/cvvid_laran/public/mykitchenbakery/jobs/22/applications" accept-charset="UTF-8"> -->
<!--                         <div class="JobApplications__filter form-inline"> -->
<!--                             <div class="form-group"> -->
<!--                                 <div class="input-group"> -->
<!--                                     <input class="form-control" placeholder="Search..." name="search" type="text" value=""> -->
<!--                                     <span class="input-group-btn"> -->
<!--                                         <button class="btn btn-primary"><i class="fa fa-search"></i></button> -->
<!--                                     </span> -->
<!--                                 </div> -->
<!--                             </div> -->
<!--                         </div> -->
<!--                     </form> -->
		
                    <?php
                   
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'job-ans-grid',
                         'itemsCssClass' => 'table',
                        'summaryText' => '',
                       // 'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                        'enableSorting' => true,
                        'ajaxUpdate' => true,
                        'pager' => array(
                            //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                            'header'=>'',
                            'firstPageLabel' => '<<',
                            'lastPageLabel' => '>>',
                            'nextPageLabel' => '>>',
                            'prevPageLabel' => '<<'),
                            'ajaxUpdate' => false,
                            'dataProvider' => JobAnswers::model()->searchByJobID($model->id),
                            //'filter'=>$model,
                            'columns' => array(    
                                array(
                                    'header' => 'Candidate',
                                    'value' =>'$data->user_id > 0 ? $data->getUser() : ""'
                                ),   
                                array(
                                    'header' => 'Question',
                                    'value' =>'$data->question_id > 0 ? $data->getQuestion() : ""'
                                ),     
                                array(
                                    'header' => 'Answers',
                                    'value'=>'CHtml::link("<i class=\"fa fa-eye\"> View</i>","#",array("id"=>$data->question_id,"class"=>"view_ans_btn")).
                                               CHtml::hiddenField("user_id",$data->user_id,array("id"=>$data->question_id."_user_id"))',
                                    'type' => 'raw'
                                ),
								 array(
                                    'header' => 'Delete',
                                    'value'=>'CHtml::link("<i class=\"fa fa-trash\"></i>",Yii::app()->createUrl("jobs/deleteAnswers",array("id"=>$data->id)),array("id"=>$data->question_id))',
                                    'type' => 'raw'
                                ),
                                array(
                                    'header' => 'Message',
                                    'value'=>'CHtml::link("<i class=\"fa fa-envelope\"></i>","#",array("id"=>$data->id,"class"=>"reply-message"))',
                                    'type' => 'raw'
                                ),
                            ),
                        ));
                    
                     ?>
                </div>
						<div class="modal fade" id="AnswerModal">
                               <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <input type="hidden" id="job-id" value="<?php echo $model->id; ?>">
                                                    <button type="button" class="close" data-dismiss="modal" onclick='closepopup()' aria-label="Close">
                                                    <span aria-hidden="true">&#10006;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">View Answers</h4>
                                                </div>
                                                <div class="ModalApplicationStatus__body">

                                                    <div class="ModalApplicationStatus__form">
                                                        <div class="form-group">
                                                        <input type="hidden" value="" id="answer_id">
                                    
                               							 <div id="my-video"></div>
                               							<!-- <script type="text/javascript">
                               							var playlistThree = [{
                               								"file":"//content.jwplatform.com/videos/RDn7eg0o-cIp6U8lV.mp4",
                               								"image":"//content.jwplatform.com/thumbs/RDn7eg0o-720.jpg",
                               								"title": "Surfing Ocean Wave"
                               								},{
                               								"file": "//content.jwplatform.com/videos/tkM1zvBq-cIp6U8lV.mp4",
                               								"image": "//content.jwplatform.com/thumbs/tkM1zvBq-720.jpg",
                               								"title": "Surfers at Sunrise"
                               								},{
                               								"file": "//content.jwplatform.com/videos/i3q4gcBi-cIp6U8lV.mp4",
                               								"image":"//content.jwplatform.com/thumbs/i3q4gcBi-720.jpg",
                               								"title": "Road Cycling Outdoors"
                               								}];
                               								                               							 
                               							jwplayer("my-video").setup({
                                                            'playlist':playlistThree
//                                                            width: "100%",
//                                                            height: "100%",
                                                           // 'primary': 'flash',
                                                            //'autostart': 'true'
                                                           });
                               							
                               							 </script>-->
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="ModalApplicationStatus__loader" style="display:none;">
                                                        <div class="ModalApplicationStatus__loader-inner">
                                                            <div>
                                                                <img src="<?php echo Yii::app()->baseUrl?>/images/loading.gif" alt="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" onclick='closepopup()' data-dismiss="modal">Close</button>
                                                    
                                                </div>
                                            </div>
                                  </div>
                          </div>
            </div>
        </div>
    </div>
</div>


<!--conver messgae-->
<div id="NewConversationModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closepopup()"><span aria-hidden="true">Ã—</span></button>
                <h4 class="modal-title">Send a Message</h4>
            </div>
            <div class="modal-body">
                <form class="MessageForm">
                    <div class="alert alert-success" id='msg_scc_msg' style="display:none;">
                        Your message has been successfully sent
                    </div>
                    <div class="alert alert-danger" id='msg_err_msg' style="display: none">
                    </div>
                  
                    <div class="form-group">
                        <label for="subject">Subject</label>
                        <input name="subject" type="text" id="subject" class="form-control" value="">
                        <input name="target_id" type="hidden" id="target_id" class="form-control" value="">
                    </div>
                    <div class="form-group">
                        <label for="message">Message</label>
                        <textarea name="message" id="message" class="form-control" rows="10"></textarea>
                    </div>
                </form>
                <div class="modal-loader">
                    <img src="http://localhost/cvvid_laran/public/images/loading.gif">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default-btn" data-dismiss="modal" onclick="closepopup()">Close</button>
                <button type="button" class="SendMessageBtn btn main-btn">Send Message</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!--conver messgae-->

 <?php
    $this->beginWidget(
            'booster.widgets.TbModal', array('id' => 'AnswerModal')
    );
    ?>

        <div class="modal-header">
            <a class="close" data-dismiss="modal" onclick='closepopup()'>&times;</a>
            <h4>Video Answers</h4>
        </div>

        <div class="modal-body clearfix" id="load-accept-div">
            
        </div>

        <div class="modal-footer">
            <?php
            $this->widget(
                'booster.widgets.TbButton', array(
                'context' => 'primary',
                'label' => 'Save changes',
                'url' => '#',
                'htmlOptions' => array('data-dismiss' => 'modal','id'=>'view-answers'),
                    )
            );
            ?>
            <?php
            $this->widget(
                'booster.widgets.TbButton', array(
                'label' => 'Close',
                'url' => '#',
                'htmlOptions' => array('data-dismiss' => 'modal','onclick'=>'closepopup()'),
                    )
            );
            ?>
        </div>

<?php $this->endWidget(); ?>
 <?php 
    function timeAgo($time_ago)
    {
        $time_ago = strtotime($time_ago);
        $cur_time   = time();
        $time_elapsed   = $cur_time - $time_ago;
        $seconds    = $time_elapsed ;
        $minutes    = round($time_elapsed / 60 );
        $hours      = round($time_elapsed / 3600);
        $days       = round($time_elapsed / 86400 );
        $weeks      = round($time_elapsed / 604800);
        $months     = round($time_elapsed / 2600640 );
        $years      = round($time_elapsed / 31207680 );
        // Seconds
        if($seconds <= 60){
            return "just now";
        }
        //Minutes
        else if($minutes <=60){
            if($minutes==1){
                return "one minute ago";
            }
            else{
                return "$minutes minutes ago";
            }
        }
        //Hours
        else if($hours <=24){
            if($hours==1){
                return "an hour ago";
            }else{
                return "$hours hrs ago";
            }
        }
        //Days
        else if($days <= 7){
            if($days==1){
                return "yesterday";
            }else{
                return "$days days ago";
            }
        }
        //Weeks
        else if($weeks <= 4.3){
            if($weeks==1){
                return "a week ago";
            }else{
                return "$weeks weeks ago";
            }
        }
        //Months
        else if($months <=12){
            if($months==1){
                return "a month ago";
            }else{
                return "$months months ago";
            }
        }
        //Years
        else{
            if($years==1){
                return "one year ago";
            }else{
                return "$years years ago";
            }
        }
    }
 ?>
