<?php
/* @var $this JobsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
    'Jobs',
);

?>

<div id="page-content">
<div class="container">
 <h1 align="center">Jobs by Skills</h1>
 <hr>
<ul class="job-by-list clearfix">
<?php 
    $skillmodel = Skills::model()->findAll();
    foreach ($skillmodel as $skill):
        $smodel = Skills::model()->findByPk($skill['id']);
    
        $string = str_replace(' ', '-', trim($smodel->name)); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        $skilname = strtolower($string);
?> 
       <li class="jobs-li"> <a href="<?php echo $this->createUrl('site/searchSkills',array('skills'=>$skilname)) ?>"><?php echo $skill['name']." Jobs" ?> </a></li>
       
<?php endforeach; ?>   
 </ul>
 </div></div>
    