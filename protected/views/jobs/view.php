<?php
/* @var $this JobsController */
/* @var $model Jobs */

$this->breadcrumbs=array(
	'Jobs'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Jobs', 'url'=>array('index')),
	array('label'=>'Create Jobs', 'url'=>array('create')),
	array('label'=>'Update Jobs', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Jobs', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Jobs', 'url'=>array('admin')),
);
?>

<h1>View Jobs #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'owner_id',
		'owner_type',
		'user_id',
		'industry_id',
		'company_name',
		'title',
		'salary_type',
		'salary_min',
		'salary_max',
		'end_date',
		'skill_category_id',
		'location',
		'location_lat',
		'location_lng',
		'type',
		'description',
		'additional',
		'created_at',
		'updated_at',
		'status',
		'closed_date',
		'successful_applicant_id',
		'num_views',
	),
)); ?>
