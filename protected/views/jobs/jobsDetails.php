<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs = array(
    'Jobs' => array('index'),
    'Manage',
);
?>
<h1><?php echo $model->title; ?></h1>
<div class="Admin__content__inner">

   <div class="row">
        <div class="col-sm-6">

            <div class="row">
                <div class="col-sm-6">

                    <div class="form-group">
                        <label class="control-label">Company</label>
                        <p class="form-control-static"><?php echo $employermodel->name; ?></p>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Job Title</label>
                        <p class="form-control-static"><?php echo $model->title; ?></p>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Type</label>
                        <p class="form-control-static"><?php echo $model->type; ?></p>
                    </div>

                </div>
                <div class="col-sm-6">

                    <div class="form-group">
                        <label class="control-label">Salary</label>
                        <p class="form-control-static"><?php echo "£".$model->salary_min; ?> to <?php echo "£".$model->salary_max; ?> per <?php echo $model->salary_type; ?>  </p>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Location</label>
                        <p class="form-control-static"><?php echo $model->location; ?></p>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Created On</label>
                        <p class="form-control-static"><?php echo $model->created_at; ?></p>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="form-group">
                <label class="control-label">Description</label>
                <p class="form-control-static"><p><?php echo $model->description; ?></p>
            </div>
            <div class="form-group">
                <label class="control-label">Skills</label>
                <p class="form-control-static"><?php
                    $arr_val = array();
                    foreach ($jobskillmodel as $js){
                         $skillsmodel = Skills::model()->findByPk($js['skill_id']);
                         $arr_val[] = $skillsmodel->name;
                     }
                     echo implode(',', $arr_val);
                    ?></p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="JobApplications">
                <h3>Applications</h3>
                <?php
                $this->widget('zii.widgets.grid.CGridView', array(
                    'id' => 'job-applications-grid',
                    'itemsCssClass' => 'table',
                    'summaryText' => '',
                   // 'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                    'dataProvider' => JobApplications::model()->searchByID($model->id),
                    'columns' => array(
                        array(
                            'header' => 'Name',
                            'value' => '$data->user->forenames." ".$data->user->surname',
                        ),
                        array(
                            'header' => 'Status',
                            'value' => '$data->status',
                        ),
                        array(
                            'header' => 'Applied On',
                            'value' => '$data->created_at',
                        ),
                        array(
                            'header' => 'Interview Date',
                            'value' => '$data->interview_date',
                        )
                    ),
                ));
                ?>

            </div><!-- .JobApplications -->
        </div>
    </div> 
    
    
</div>


