<?php
/* @var $this JobsController */
/* @var $model Jobs */
/* @var $form CActiveForm */
?>

<script>
$(function () {	 
	  var $rateYo = $("#rateYo").rateYo({
                	    rating: 3,
                	    fullStar: true
    	    		 });
	  var rating = $rateYo.rateYo("rating");

	  $('#job-reference-form').submit(function()
	  {
		  var text = $('#reference_text').val();
		  var rate = $rateYo.rateYo("rating");
		  $.ajax({
	            url: '<?php echo $this->createUrl('jobApplications/saveReference') ?>',
	            type: 'POST',
	            async:false,
	            data: {reference_text: text, rating : rate},
	            success: function (data) {
	                location.reload();
	            },
	            error: function (data) {
	                alert('err');
	            }
	          });
	  });
 });
</script>

<div id="page-content">
    <div class="container">
     <h1>Verify Candidate</h1>
     <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
     <?php
     $form = $this->beginWidget('CActiveForm', array(
         'id' => 'job-reference-form',
         'action' => $this->createUrl('jobApplications/saveReference'),
         'enableAjaxValidation' => false,
         'htmlOptions' => array('enctype' => 'multipart/form-data','class' => 'form-horizontal'),
     ));
     ?>
     
     <div class="form-container">
         <div class="form-group">
          <h4>About Candidate</h4>
             <?php echo CHtml::textArea('reference_text','', array('rows'=>'10','cols'=>'20','placeholder' => 'About...','class'=>'form-control','id' => 'reference_text')); ?>
         </div>
     </div>
    <div class="form-container">
     	<div id="rateYo"></div>
    </div>
     <div class="form-actions">
         <?php 
             echo CHtml::submitButton('Submit', array("class" => "btn btn-primary")); 
        // echo CHtml::button('Back',array("class"=>"btn btn-default","onclick"=>"window.location='".Yii::app()->createUrl('jobs/admin')."'"));
         ?>
     </div> 
     <?php $this->endWidget(); ?>
</div>
 </div>