<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs = array(
    'Jobs' => array('index'),
    'Manage',
);
?>
<script>
function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
}
$(document).ready(function()
{
	$(".apply_btn").click(function(){
		$('#SummaryModal').addClass('in');
        $('body').addClass('modal-open');
        $('#SummaryModal').show();
	});

	$(".login-model").click(function(){
		$('#LoginRegisterModal').addClass('in');
        $('body').addClass('modal-open');
        $('#LoginRegisterModal').show();
	});
	$(".job_apply").click(function(){
		var summary = $("#job_summary").val();
		var reference = $("#job_reference").val();
		if( reference != "" && !isValidEmailAddress( reference ) ) 
		{ 
			$('.err_msg').html('Email is invalid.');
			return false;
		}
		 $.ajax({
             url: '<?php echo $this->createUrl('jobs/apply',array('id'=>$model->id)) ?>',
             type: 'POST',
             data: {summary: summary,reference : reference},
             success: function (data) {
                  location.reload();
             },
             error: function (data) {
                 alert('err');
             }
         });
		
	});
	
});
</script>
<div class="page-title text-left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $model->title." job"?></h1>
            </div>
        </div>
    </div>
</div>

<div id="page-content">
                <div class="container">
                <?php 
//                     if($model->owner_type == "employer")
                        $empmodel = Employers::model()->findByPk($model->owner_id);
                     if($model->owner_type == "agency")
                        $empmodel = Agency::model()->findByPk($model->owner_id);
                    
                    //$empmodel = Employers::model()->findByPk($model->owner_id);
                    $indmodel = Industries::model()->findByPk($model->industry_id);
                   // $_SESSION['target'] = Yii::app()->controller->getRoute()."/".$model->id;
                ?>
                <div class="Job">
            <div class="Job__header">
                <div class="Job__header__company">
                <?php 
                    $cri = new CDbCriteria();
                    //$cri->condition = "owner_id =".$model->owner_id." and type like '%employer%'";
                    $cri->condition = "owner_id =".$model->owner_id;
                    $profilemodel = Profiles::model()->find($cri);
                ?>
                    <?php 
                        echo $empmodel->name; 
                        
                        //if($model->owner_type == "employer")
                            $viewurl =  Yii::app()->createUrl("employers/employer",array('slug'=>$profilemodel->slug));
                         if($model->owner_type == "agency")
                            $viewurl =  Yii::app()->createUrl("agency/profile",array('id'=>$empmodel->id));
                    
                    ?> 
                    
                    <a href="<?php echo $viewurl?>">view</a>
                </div>
                <div class="Job__header__title">
                    <?php echo $model->title ?>
                </div>
            </div>
            <div class="Job__meta">
            <?php 
               
                $cri = new CDbCriteria();
               // $cri->condition = "owner_id =".$empmodel->id." and type ='employer'";
                $cri->condition = "owner_id =".$empmodel->id;
                $profilemodel = Profiles::model()->find($cri);
                $src = "";
                if($profilemodel->photo_id > 0)
                {
                    $mediamodel = Media::model()->findByPk($profilemodel->photo_id);
                    $src = Yii::app()->baseUrl."/images/media/".$profilemodel->photo_id."/conversions/thumb.jpg";
                }
            ?>
                <div class="row">
                    <div class="col-md-3 col-md-push-9">
                        <div class="Job__logo">
                            <img src="<?php echo $src ?>" class="img-responsive">
                        </div>
                    </div>
                    <div class="col-md-9 col-md-pull-3">
                        <div class="Job__details">
                            <ul class="row list-unstyled">
                                 <li class="col-sm-4">
                            	 <?php if($model->salary_min <= 0 && $model->salary_max > 0) { 
                                    echo Yii::app()->numberFormatter->formatCurrency($model->salary_max, 'GBP')." ".($model->salary_type == "annual" ? "annum" : "hour");
                                    }else if($model->salary_max <= 0 && $model->salary_min > 0) {
                                    echo Yii::app()->numberFormatter->formatCurrency($model->salary_min, 'GBP')." ".($model->salary_type == "annual" ? "annum" : "hour");
                                    }else if($model->salary_max <= 0 && $model->salary_min <= 0) {
                                    echo "Not mentioned";
                                    } else {
                                    echo Yii::app()->numberFormatter->formatCurrency($model->salary_min, 'GBP')." to ".Yii::app()->numberFormatter->formatCurrency($model->salary_max, 'GBP')." per ".($model->salary_type == "annual" ? "annum" : "hour")?>
                                   <?php } ?>
                                     </li>
                                <li class="col-sm-4"><?php echo $model->type ?></li>
                                 <?php 
                                    $date1 = new DateTime();
                                    $date2 = new DateTime($model->created_at);
                                    
                                    $interval= $date1->diff($date2);
                                    $hourdiff = ($interval->days * 24) + $interval->h;
                                    if($hourdiff <= 23)
                                        $postedtime = $hourdiff." hours ago";
                                    else 
                                    {
                                        $daysdiff = $date2->diff($date1)->format("%a");
                                        if($daysdiff <= 28)
                                            $postedtime = $daysdiff . " days ago";
                                        else             
                                        {
                                            
                                            $postedtime = ($daysdiff / 30). " months ago";
                                        }
                                    }
                                    
                                    
//                                     $postedtime = $diff;
                                   // echo "diff=".$diff;
                                    
                                ?>
                                <li class="col-sm-4">Posted: <?php echo timeAgo($model->created_at); ?></li>
                                <li class="col-sm-4"><?php echo $model->location ?></li>
                                <?php 
                                    $jobappliedcnt = JobApplications::model()->countByAttributes(array('job_id'=>$model->id,'status'=>4));
                                    $job_remain = $model->vacancies - $jobappliedcnt;
                                    if($job_remain > 0){ ?>
                                		<li class="col-sm-4"><?php echo $job_remain." vacancies" ?></li>
                                <?php } ?>
								<li class="col-sm-4">Expires on: <?php echo $model->end_date ?></li>
                             </ul>
                        </div>
						<?php 
						if(Yii::app()->user->isGuest)
						{
						    echo '<a href="#" class="btn btn-primary login-model" data-toggle="modal" >Apply</a>';
						}						
						else if(Yii::app()->user->getState('role') == "candidate")
						{
						    $usermodel = Users::model()->findByPk(Yii::app()->user->getState('userid'));
						    $agencycan = AgencyCandidates::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid')));
						    
						    if($agencycan != null)
						    {
						        echo '<a href="#" class="btn btn-primary apply_btn" data-toggle="modal" >Apply Now</a>';
						    }
						    else if(Membership::subscribed($usermodel->stripe_active, $usermodel->trial_ends_at, $usermodel->subscription_ends_at)) {

						        $jobapp = JobApplications::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid'),'job_id'=>$model->id));
						        if($jobapp == null)
						        {
						            if($model->video_only > 0)
						            {
						                $pmodel = Profiles::model()->findByAttributes(array('owner_id'=>$usermodel->id));
						                $pvmodel = ProfileVideos::model()->findByAttributes(array('profile_id'=>$pmodel->id,'is_default'=>1));
						                if($pvmodel == null)
						                    echo "<input type='button' class='btn btn-primary apply_btn' value='Apply Now' disabled>"." (Only Candidates with default profile video can apply to this job)";
						                else 
						                    echo '<a href="#" class="btn btn-primary apply_btn" data-toggle="modal" >Apply Now</a>';
						            }
						            else    
						              echo '<a href="#" class="btn btn-primary apply_btn" data-toggle="modal" >Apply Now</a>';
						        }
						          //echo '<a href="'.$this->createUrl('jobs/apply',array('id'=>$model->id)).'" class="btn btn-primary apply_btn" data-toggle="modal" >Apply Now</a>';
						        else
						        {
						          echo '<button class="btn main-btn" disabled >Already Applied</button>';
						        }
						    }						    
						    else 
						        echo '<a href="'.$this->createUrl('users/upgrade',array('target'=>Yii::app()->controller->getRoute()."/".$model->id)).'" class="btn btn-default">Upgrade membership to apply</a>';
						    
						        
						}
						?>
                        

                    </div>
                </div>
            </div>
            <div class="Job__description">
                <h5>Job Description</h5>
                <p><?php echo $model->description?></p>
            </div>
            <div class="Job__additional">
                <h5>Additional</h5>
                <p><?php echo $model->additional ?></p>
            </div>
            <div class="job_spec">
            <h5>Download Job specification Details</h5>
            <?php 
                   if($model->media_id > 0)
                   {
                       echo CHtml::link('Download',$this->createUrl('jobs/downloadJobSpec',array('id'=>$model->id)),array());
                   }
            ?>
            </div>
        </div>
    </div>

        <div class="modal fade" id="LoginRegisterModal" >
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
					
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="text-center" style="padding: 20px;">
                                <p style="color: #000;">Please login if you have an account in order to apply for this job.</p>
                                <a href="<?php echo Yii::app()->createUrl('site/login',array('target'=>'job/'.str_replace(' ', '-', $model->title)."-".$model->id)) ?>" class="btn btn-primary">Login</a>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="text-center" style="padding: 20px;">
                                <p style="color: #000;">Please register for a premium account if you wish to apply for this job.</p>
                                <a href="<?php echo Yii::app()->createUrl('site/createCandidatePremium',array('target'=>'job/'.str_replace(' ', '-', $model->title)."-".$model->id)) ?>" class="btn btn-primary">Register</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    
        </div>
        
        <div id="SummaryModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closepopup()"><span aria-hidden="true">Ã—</span></button>
                    <h4 class="modal-title">Enter Details</h4>
                </div>
                <div class="modal-body">
                    <form class="MessageForm">
                    	<div class="form-group">              
                    		<label style="color:red" class="err_msg"></label>   
                            <input type="text" id="job_reference" class="form-control" placeholder="Enter Reference Mail (optional)">
                        </div>
                        <div class="form-group">                            
                            <textarea id="job_summary" class="form-control" style="width: 100%;height:300px" placeholder="Enter short summary about you (optional)"></textarea>
                        </div>                        
                    </form>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default-btn" data-dismiss="modal" onclick="closepopup()">Cancel</button>
                    <button type="button" class="SendMessageBtn btn main-btn job_apply">Apply</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
        <?php 
    function timeAgo($time_ago)
    {
        $time_ago = strtotime($time_ago);
        $cur_time   = time();
        $time_elapsed   = $cur_time - $time_ago;
        $seconds    = $time_elapsed ;
        $minutes    = round($time_elapsed / 60 );
        $hours      = round($time_elapsed / 3600);
        $days       = round($time_elapsed / 86400 );
        $weeks      = round($time_elapsed / 604800);
        $months     = round($time_elapsed / 2600640 );
        $years      = round($time_elapsed / 31207680 );
        // Seconds
        if($seconds <= 60){
            return "just now";
        }
        //Minutes
        else if($minutes <=60){
            if($minutes==1){
                return "one minute ago";
            }
            else{
                return "$minutes minutes ago";
            }
        }
        //Hours
        else if($hours <=24){
            if($hours==1){
                return "an hour ago";
            }else{
                return "$hours hrs ago";
            }
        }
        //Days
        else if($days <= 7){
            if($days==1){
                return "yesterday";
            }else{
                return "$days days ago";
            }
        }
        //Weeks
        else if($weeks <= 4.3){
            if($weeks==1){
                return "a week ago";
            }else{
                return "$weeks weeks ago";
            }
        }
        //Months
        else if($months <=12){
            if($months==1){
                return "a month ago";
            }else{
                return "$months months ago";
            }
        }
        //Years
        else{
            if($years==1){
                return "one year ago";
            }else{
                return "$years years ago";
            }
        }
    }
 
 ?>
        