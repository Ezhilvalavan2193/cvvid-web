<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */
/* @var $form CActiveForm */
?>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script>
    
    $(document).ready(function(){
    	var Password = {
   			 
  			  _pattern : /[a-zA-Z0-9_\-\+\.]/,
  			  
  			  
  			  _getRandomByte : function()
  			  {
  			    // http://caniuse.com/#feat=getrandomvalues
  			    if(window.crypto && window.crypto.getRandomValues) 
  			    {
  			      var result = new Uint8Array(1);
  			      window.crypto.getRandomValues(result);
  			      return result[0];
  			    }
  			    else if(window.msCrypto && window.msCrypto.getRandomValues) 
  			    {
  			      var result = new Uint8Array(1);
  			      window.msCrypto.getRandomValues(result);
  			      return result[0];
  			    }
  			    else
  			    {
  			      return Math.floor(Math.random() * 256);
  			    }
  			  },
  			  
  			  generate : function(length)
  			  {
  			    return Array.apply(null, {'length': length})
  			      .map(function()
  			      {
  			        var result;
  			        while(true) 
  			        {
  			          result = String.fromCharCode(this._getRandomByte());
  			          if(this._pattern.test(result))
  			          {
  			            return result;
  			          }
  			        }        
  			      }, this)
  			      .join('');  
  			  }    
  			    
  			};

    	$(".generate-password").click(function(){

    		$(".user-list").find('.row').each(function( index ) {
    			if($.trim($(this).find('.col-sm-3').find('.form-group').find(".passwordbox").val()) == "")
    				$(this).find('.col-sm-3').find('.form-group').find(".passwordbox").val(Password.generate(10));
    		});
    		
    	});
    	
	$(".bulk-upload").change(function() {

		if($('#student_cnt').val() > 0)
		{
		
		 var file = this.files[0];

		  var reader = new FileReader();
                    reader.onload = function (progressEvent) {
                        // Entire file


                        // By lines
                        var lines = this.result.split('\n');
                        var cnt = 1;
// 		    alert(lines.length);

                       // for (var line = 0; line < lines.length; line++) {
 						for (var line = 0; line <= $('#student_cnt').val(); line++) {
                            if (cnt > 1)
                            {
                                var row = lines[line];
                                if (row != "")
                                {
                                    var array = row.split(',');

                                    var studclone = $('.student_line').find('.row').clone();
                                    $(".user-list").append(studclone);

                                    var c = 0;
                                    $(studclone).find('input').each(function () {
                                        $(this).val(array[c]);
                                        c++;
                                    });
                                }

                            }
                            cnt++;
                        }
                    };
                    reader.readAsText(file);
                } else
                {
                    var $el = $('.bulk-upload');
                    $el.wrap('<form>').closest('form').get(0).reset();
                    $el.unwrap();
                }
            });

            $('#tutorslist').change(function () {
                var id = $(this).val();
                if (id > 0)
                {
                    $.ajax({
                        url: '<?php echo $this->createUrl('institutions/getAllocStudents') ?>',
                        type: 'POST',
                        data: {id: id},
                        success: function (data) {
                            var obj = $.parseJSON(data);
                            $('#allocated_students').text(obj.count + ' / ' + obj.total);
                            $('#student_cnt').val(obj.total - obj.count);
                            $('.user-limit').html(obj.total - obj.count);
                        },
                        error: function (data) {
                            alert('err');
                        }
                    });
                }
            });

            $('#teacherslist').change(function () {
                if ($(this).val() > 0)
                    $('.teacher-section').hide();
                else
                    $('.teacher-section').show();
            });

            $("#add_student").click(function () {

                var cnt = $("#student_cnt").val();
                if ($('.user-list').find('.row').length >= cnt)
                {
                    alert('Allocated Students limit reached');
                } else
                {
                    var studclone = $('.student_line').find('.row').clone();
                    $(".user-list").append(studclone);
                }

            });

            $(".user-list").on('click', '.delete-user', function () {
                $(this).closest('.row').remove();
            });

            $('#stud-bulk-form').submit(function () {
        		
       		 var err = 0;
       		 if($("#student_cnt").val() > 0 && $('.user-list').find('.row').length <= 0)
       		 {
       			alert("Please add student");
       			return false;
       		 }
       		 else if($('.user-list').find('.row').length > 0)
       		 {
       			 var arr = [];
       			 $(".user-list").find('.row').each(function( index ) {
       				 arr.push($(this).find('input.emailbox').val());
       			 });
       			 
       			 $.ajax({
       		            url: '<?php echo $this->createUrl('institutions/checkEmails') ?>',
       		            type: 'POST',
       		            data: {emails : arr}, 	
       		            dataType: "JSON",
       		            async:false,    
       		            success: function(data) {
       			            
       			            if(data != -1)
       			            {
       				            var msg = "";
       	    	            	 for(var i=0;i<data.length;i++){
       	    	                     msg += "<div>student "+data[i]+ " email already taken</div>";
       	    	                 }
       	    	            	 $('#msg_err_msg').html(msg);
       	    	            	 $('#msg_err_msg').show();
       	    	            	 $(window).scrollTop(0);
       	    	            	 err = 1;
       			            }
       			            else
       			            	$('#msg_err_msg').hide();
//       	 		            for(i = 0 )
//       	 	            	$('#msg_err_msg').html("<div>"+)
       		            },
       				    error: function(data) {		
       				        alert('err');
       				    }
       		         });

       	         if(err > 0)
       			 	return false;
       	         else
       		         return true;
       			
       		 }
       			 
       	});
        });
</script>



<div class="page-title text-left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Bulk Add Students</h1>
            </div>
        </div>
    </div>
</div>
<div id="page-content">
    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'stud-bulk-form',
    'action'=>$this->createUrl('institutions/saveBulkStudent'),
	'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data')
)); ?>
    <div class="container">
			<div class="alert alert-danger" id='msg_err_msg' style="display: none">
			</div>
        <div class="form-container">
            <div id="student-tutor-group">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="tutor_group_id" class="control-label">Class</label>
                            <?php
                            $tgmodel = TutorGroups::model()->findAllByAttributes(array('deleted_at' => null, 'institution_id' => $model->id));
                            $tglist = CHtml::listData($tgmodel, 'id', 'name');
                            echo $form->dropDownList($tutormodel, 'id', $tglist, array('class' => 'form-control', 'id' => 'tutorslist', 'empty' => '- Select Class -'));
                            ?>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Allocated Accounts</label>
                            <p class="form-control-static tutor-group-limit" id="allocated_students"></p>
                           
                            <?php echo CHtml::hiddenField('student_cnt', '0', array('id' => 'student_cnt')); ?>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="form-container">
            <h4>Students</h4>
            <div id="user-upload">
                <h5>File Upload</h5>
                <div class="user-file-upload form-inline">
                    <div class="form-group">
                        <?php
                        echo CHtml::fileField('students', '', array('class' => 'bulk-upload'));
                        ?>
                    </div>
                    <span data-toggle="modal" data-target="#SampleDownload">
                        <a class="btn btn-default" data-toggle="tooltip" data-placement="top" title="" data-original-title="Download a sample upload file">
                            <i class="fa fa-info"></i>
                        </a>
                    </span>
                </div>
                <h5>
                    Manual Upload
                    <span class="user-limit-count pull-right">
                        <span class="user-count">0</span> / <span class="user-limit">0</span>
                    </span>
                </h5>
                <div class="user-list"></div>
                <div class="user-item student_line" style="display: none;">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?php echo CHtml::textField('forenames[]', '', array('class' => "form-control", 'placeholder' => 'Forename')); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?php echo CHtml::textField('surname[]', '', array('class' => "form-control", 'placeholder' => 'Surname')); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                 <?php echo CHtml::textField('email[]', '', array('class' => "form-control emailbox", 'placeholder' => 'Email')); ?>
                                <!--<input type="text" name="email[]" class="form-control" placeholder="Email Address" value="">-->
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?php echo CHtml::textField('password[]', '', array('class' => "form-control passwordbox", 'placeholder' => 'Password')); ?>
                                <button type="button" class="delete-user btn btn-danger"><i class="fa fa-close"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="user-actions clearfix">
                <?php
                echo CHtml::button('Add Student', array('class' => 'btn btn-default add-new pull-left', 'id' => 'add_student'));
                ?>
                <button type="button" class="btn btn-default generate-password pull-right">Generate Passwords</button>
            </div>
        </div>
        <?php echo CHtml::submitButton('Save', array('class' => 'btn btn-primary')); ?>
        <?php echo CHtml::button('Cancel', array('class' => 'btn btn-default')); ?>
    </div><!-- .form-container -->

    <div class="modal fade" tabindex="-1" role="dialog" id="SampleDownload">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Download Sample Upload File</h4>
                </div>
                <div class="modal-body">
                    <p>When using the sample document, please alter the data in the columns accordingly. Please however do not add additional columns or alter the column headings.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a href="<?php echo $this->createUrl('institutions/sampleDownload')?>" class="btn btn-primary">Download File</a>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</div>
<?php $this->endWidget(); ?>
</div>



