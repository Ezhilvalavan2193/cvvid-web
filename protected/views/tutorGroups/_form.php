<?php
/* @var $this TutorGroupsController */
/* @var $model TutorGroups */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tutor-groups-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'institution_id'); ?>
		<?php echo $form->textField($model,'institution_id',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'institution_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'teacher_id'); ?>
		<?php echo $form->textField($model,'teacher_id',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'teacher_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'location'); ?>
		<?php echo $form->textField($model,'location',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'location'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'location_lat'); ?>
		<?php echo $form->textField($model,'location_lat',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'location_lat'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'location_lng'); ?>
		<?php echo $form->textField($model,'location_lng',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'location_lng'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'graduation_date'); ?>
		<?php echo $form->textField($model,'graduation_date'); ?>
		<?php echo $form->error($model,'graduation_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'account_limit'); ?>
		<?php echo $form->textField($model,'account_limit'); ?>
		<?php echo $form->error($model,'account_limit'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'created_at'); ?>
		<?php echo $form->textField($model,'created_at'); ?>
		<?php echo $form->error($model,'created_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'updated_at'); ?>
		<?php echo $form->textField($model,'updated_at'); ?>
		<?php echo $form->error($model,'updated_at'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'deleted_at'); ?>
		<?php echo $form->textField($model,'deleted_at'); ?>
		<?php echo $form->error($model,'deleted_at'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->