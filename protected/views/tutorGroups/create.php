<?php
/* @var $this TutorGroupsController */
/* @var $model TutorGroups */

$this->breadcrumbs=array(
	'Tutor Groups'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TutorGroups', 'url'=>array('index')),
	array('label'=>'Manage TutorGroups', 'url'=>array('admin')),
);
?>

<h1>Create TutorGroups</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>