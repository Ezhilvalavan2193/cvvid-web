<?php
/* @var $this TutorGroupsController */
/* @var $model TutorGroups */

$this->breadcrumbs=array(
	'Tutor Groups'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TutorGroups', 'url'=>array('index')),
	array('label'=>'Create TutorGroups', 'url'=>array('create')),
	array('label'=>'View TutorGroups', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TutorGroups', 'url'=>array('admin')),
);
?>

<h1>Update TutorGroups <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>