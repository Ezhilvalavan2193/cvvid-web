<?php
/* @var $this UserLanguagesController */
/* @var $model UserLanguages */

$this->breadcrumbs=array(
	'User Languages'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List UserLanguages', 'url'=>array('index')),
	array('label'=>'Create UserLanguages', 'url'=>array('create')),
	array('label'=>'Update UserLanguages', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete UserLanguages', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage UserLanguages', 'url'=>array('admin')),
);
?>

<h1>View UserLanguages #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'name',
		'proficiency',
		'created_at',
		'updated_at',
		'deleted_at',
	),
)); ?>
