<?php
/* @var $this UserLanguagesController */
/* @var $model UserLanguages */

$this->breadcrumbs=array(
	'User Languages'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List UserLanguages', 'url'=>array('index')),
	array('label'=>'Create UserLanguages', 'url'=>array('create')),
	array('label'=>'View UserLanguages', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage UserLanguages', 'url'=>array('admin')),
);
?>

<h1>Update UserLanguages <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>