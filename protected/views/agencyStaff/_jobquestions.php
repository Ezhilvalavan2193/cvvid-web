<?php
/* @var $this UsersController */
/* @var $model Users */

?>
<script>
$(document).ready(function () {
	
    $('body').on('click', '#add_question', function (e) {
       
    	$('#QuestionModal').addClass('in');
        $('body').addClass('modal-open');
        $('#QuestionModal').show();
        
    });
    
	$('body').on('click', '#save-question', function (e) {
       
		var value = $("#question").val();
		var duration = $("#answer_duration").val();
		if($.trim($("#question").val()) != "")
		{
    		$.ajax({
                url: '<?php echo $this->createUrl('jobs/saveQuestion',array('id'=>$model->id)) ?>',
                type: 'POST',
                data: {value: value, duration : duration},
                success: function (response) {
					
                	$.fn.yiiGridView.update('emp-ques-grid');
                },
               error: function (data) {
                   alert('err');
               }
            });
		}
		else
			$("#question").css("border-color","red");
    });
    
	$('body').on('click', '.edit-question', function (e) {
	       
		var id = $(this).attr("id");
		$("#job_question_id").val(id);
		
		var value = $(this).closest("tr").find("td:nth-child(2)").text();
		var dur = $(this).closest("tr").find("td:nth-child(3)").text();
		$("#question").val(value);
		$("#answer_duration").val(dur);
		$('#QuestionModal').addClass('in');
        $('body').addClass('modal-open');
        $('#QuestionModal').show();
        
        $("#save-question").attr("id","update-question");
    });
	
	$('body').on('click', '#update-question', function (e) {
	       
		var value = $("#question").val();
		var duration = $("#answer_duration").val();
		
		if($.trim($("#question").val()) != "")
		{
			var id = $("#job_question_id").val();
			
			if(id > 0)
			{
        		$.ajax({
                    url: '<?php echo $this->createUrl('jobs/updateQuestion') ?>',
                    type: 'POST',
                    data: {value: value, id: id, duration:duration},
                    success: function (response) {
                    	$.fn.yiiGridView.update('emp-ques-grid');
                    },
                   error: function (data) {
                       alert('err');
                   }
                });
			}
			else
				location.reload();
		}
		else
			$("#question").css("border-color","red");
    });
	
	$('body').on('click', '.delete-question', function (e) {
	       
		var id = $(this).attr("id");
		if (confirm('Do you want to delete this item ?')) {
    		$.ajax({
                url: '<?php echo $this->createUrl('jobs/deleteQuestion') ?>',
                type: 'POST',
                data: {id: id},
                success: function (response) {
                	$.fn.yiiGridView.update('emp-ques-grid');
                },
               error: function (data) {
                   alert('err');
               }
            });
		}
    });
	
});   

</script>

<div class="page-title text-left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Questions</h1>
            </div>
        </div>
    </div>
</div>
<div id="page-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
              <?php if (Yii::app()->user->hasFlash('success')): ?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?php echo Yii::app()->user->getFlash('success'); ?>
                    </div>
                <?php endif; ?>
                <div class="JobApplications__job">

                    <div class="Job">
                        <div class="Job__header clearfix">
                            <div class="pull-left">
                                <?php echo $model->title; ?><br/>
                                <div class="Job__deadline">Deadline: <?php echo $model->end_date; ?></div>
                            </div>
                            <div class="pull-right">
                                <form method="POST" action="<?php echo $this->createUrl('jobs/jobstatus',array('id'=>$model->id)); ?>" accept-charset="UTF-8" class="JobStatus__form">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <?php 
                                                 if($model->status=='active')
                                                    echo "Active";
                                                 else
                                                    echo "On Hold";
                                            ?> <span class="caret"></span>
                                            <input name="status" type="hidden">
                                        </button>
                                        <?php 
                                        $cri = new CDbCriteria();
                                        $cri->condition = "owner_id =".$model->owner_id." and type like '%employer%'";
                                        $profilemodel = Profiles::model()->find($cri);
                                        ?>
                                        <ul class="dropdown-menu">
                                            <li><a href="#" class="JobBtn__active">Active</a></li>
                                            <li><a href="#" class="JobBtn__hold">On Hold</a></li>
                                            <li><a href="<?php echo $this->createUrl('employers/jobsedit',array('slug'=>$profilemodel->slug,'id'=>$model->id)); ?>">Extend</a></li>
                                        </ul>
                                    </div><!-- .btn-group -->
                                </form>                         
                            </div>
                        </div>
                        <div class="Job__meta">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="Job__details">
                                        <ul class="row list-unstyled">
                                             <?php if($model->salary_min > 0){ ?>
                                            <li class="col-sm-4"><?php echo Yii::app()->numberFormatter->formatCurrency($model->salary_min, 'GBP')." to ".Yii::app()->numberFormatter->formatCurrency($model->salary_max, 'GBP')." per ".($model->salary_type == "annual" ? "annum" : "hour")?></li>
                                           <?php } ?>
                                            <li class="col-sm-4"><?php echo $model->type ?></li>
                                             <?php 
                                                $date1 = new DateTime();
                                                $date2 = new DateTime($model->created_at);
                                                
                                                $interval= $date1->diff($date2);
                                                $hourdiff = ($interval->days * 24) + $interval->h;
                                                if($hourdiff <= 23)
                                                    $postedtime = $hourdiff." hours ago";
                                                else 
                                                {
                                                    $daysdiff = $date2->diff($date1)->format("%a");
                                                    if($daysdiff <= 28)
                                                        $postedtime = $daysdiff . " days ago";
                                                    else             
                                                    {
                                                        
                                                        $postedtime = ($daysdiff / 30). " months ago";
                                                    }
                                                }
                                                
                                                
            //                                     $postedtime = $diff;
                                               // echo "diff=".$diff;
                                                
                                            ?>
                                            <li class="col-sm-4">Posted: <?php echo timeAgo($model->created_at); ?></li>
                                            <li class="col-sm-4"><?php echo $model->location ?></li>
                                            <?php if($model->vacancies > 0){ ?>
                                            	<li class="col-sm-4"><?php echo $model->vacancies." vacancies" ?></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-sm-12">

                <div class="JobApplications__applications">
                      <?php
                        $cri3 = new CDbCriteria();
                        $cri3->condition = "status < 1 and job_id = ".$model->id;
                        $count2 = count(JobApplications::model()->findAll($cri3));
                        ?>
                    <ul class="nav nav-tabs">
                        <li role="presentation">
                            <a href="<?php echo Yii::app()->createUrl("jobs/applications",array("id"=>$model->id,"title"=>  str_replace(" ", "-", $model->title))); ?>"><i class="fa fa-inbox"></i>Applications (<?php echo $count2; ?>)</a>
                        </li>
                        <li role="presentation">
                            <a href="<?php echo Yii::app()->createUrl("jobs/shortlists",array("id"=>$model->id,"title"=>  str_replace(" ", "-", $model->title))); ?>"><i class="fa fa-question"></i>Shortlisted (<?php echo JobApplications::model()->getShortCount($model->id); ?>)</a>
                        </li>
                        <li role="presentation">
                            <a href="<?php echo Yii::app()->createUrl("jobs/interviews",array("id"=>$model->id,"title"=>  str_replace(" ", "-", $model->title))); ?>"><i class="fa fa-user"></i>Interviewees (<?php
                            $cri1 = new CDbCriteria();
                            $cri1->condition = "status = 2 and job_id = ".$model->id;
                            $count = count(JobApplications::model()->findAll($cri1));
                            echo $count; ?>)</a>
                        </li>
                         <li role="presentation">
                            <a href="<?php echo Yii::app()->createUrl("jobs/appointments",array("id"=>$model->id,"title"=>  str_replace(" ", "-", $model->title))); ?>"><i class="fa fa-calendar"></i>Appointments (<?php echo JobApplications::model()->getAppointmentsCount($model->id); ?>)</a>
                        </li>
                        <li role="presentation" class="active">
                            <a href="<?php echo Yii::app()->createUrl("jobs/questions",array("id"=>$model->id,"title"=>  str_replace(" ", "-", $model->title))); ?>"><i class="fa fa-star"></i>Questions</a>
                        </li>
                           <li role="presentation" >
                            <a href="<?php echo Yii::app()->createUrl("jobs/answers",array("id"=>$model->id,"title"=>  str_replace(" ", "-", $model->title))); ?>"><i class="fa fa-star"></i>Answers (<?php echo JobApplications::model()->getAnswersCount($model->id); ?>)</a>
                        </li>
                    </ul>                    
<!--                     <form method="GET" action="http://localhost/cvvid_laran/public/mykitchenbakery/jobs/22/applications" accept-charset="UTF-8"> -->
<!--                         <div class="JobApplications__filter form-inline"> -->
<!--                             <div class="form-group"> -->
<!--                                 <div class="input-group"> -->
<!--                                     <input class="form-control" placeholder="Search..." name="search" type="text" value=""> -->
<!--                                     <span class="input-group-btn"> -->
<!--                                         <button class="btn btn-primary"><i class="fa fa-search"></i></button> -->
<!--                                     </span> -->
<!--                                 </div> -->
<!--                             </div> -->
<!--                         </div> -->
<!--                     </form> -->
		
			<div class="pull-right" style="padding: 10px">
				<?php echo CHtml::button("Add",array("id"=>'add_question','class'=>'btn btn-primary')); ?>
			</div>
                    <?php
                   
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'emp-ques-grid',
                         'itemsCssClass' => 'table',
                        'summaryText' => '',
                       // 'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                        'enableSorting' => true,
                        'ajaxUpdate' => true,
                        'pager' => array(
                            //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                            'header'=>'',
                            'firstPageLabel' => '<<',
                            'lastPageLabel' => '>>',
                            'nextPageLabel' => '>>',
                            'prevPageLabel' => '<<'),
                            'ajaxUpdate' => false,
                            'dataProvider' => JobQuestions::model()->searchByJobID($model->id),
                            //'filter'=>$model,
                            'columns' => array(    
                                array(
                                    'header' => 'Edit',
                                    'value'=>'CHtml::link("<i class=\"fa fa-pencil\"></i>","#",array("id"=>$data->id,"class"=>"edit-question"))',
                                    'type' => 'raw'
                                ),
                                array(
                                    'header' => 'Question',
                                    'value' =>'$data->question'
                                ),     
                                array(
                                    'header' => 'Duration',
                                    'value' =>'$data->answer_duration'
                                ), 
                                array(
                                    'header' => 'Delete',
                                    'value'=>'CHtml::link("<i class=\"fa fa-trash\"></i>","#",array("id"=>$data->id,"class"=>"delete-question"))',
                                    'type' => 'raw'
                                ),
                            ),
                        ));
                     ?>
                </div>
						<div class="modal fade" id="QuestionModal">
                               <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <input type="hidden" id="job-id" value="<?php echo $model->id; ?>">
                                                    <button type="button" class="close" data-dismiss="modal" onclick='closepopup()' aria-label="Close">
                                                    <span aria-hidden="true">×</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Add Question</h4>
                                                </div>
                                                <div class="ModalApplicationStatus__body">

													<div class="ModalApplicationStatus__form">
                                                                                              
                                                            <label class="control-label">Duration (in minutes)</label>
                                                            <input id="answer_duration" name="answer_duration" class="form-control"/>
                                                        
                                                    </div>
                                                    <div class="ModalApplicationStatus__form">
                                                       
                                                        <input type="hidden" value="" id="job_question_id">
                                                            <label class="control-label">Question</label>
                                                            <textarea id="question" name="question" class="form-control" rows="10"></textarea>
                                                      
                                                    </div>
                                                    <div class="ModalApplicationStatus__loader" style="display:none;">
                                                        <div class="ModalApplicationStatus__loader-inner">
                                                            <div>
                                                                <img src="<?php echo Yii::app()->baseUrl?>/images/loading.gif" alt="">
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" onclick='closepopup()' data-dismiss="modal">Close</button>
                                                    <button type="button" id="save-question" class="btn btn-primary SubmitBtn">Submit</button>
                                                </div>
                                            </div>
                                  </div>
                          </div>
            </div>
        </div>
    </div>
</div>
 <?php
    $this->beginWidget(
            'booster.widgets.TbModal', array('id' => 'QuestionModal')
    );
    ?>

        <div class="modal-header">
            <a class="close" data-dismiss="modal" onclick='closepopup()'>&times;</a>
            <h4>Add Question</h4>
        </div>

        <div class="modal-body clearfix" id="load-accept-div">
            
        </div>

        <div class="modal-footer">
            <?php
            $this->widget(
                'booster.widgets.TbButton', array(
                'context' => 'primary',
                'label' => 'Save changes',
                'url' => '#',
                'htmlOptions' => array('data-dismiss' => 'modal','id'=>'save-question'),
                    )
            );
            ?>
            <?php
            $this->widget(
                'booster.widgets.TbButton', array(
                'label' => 'Close',
                'url' => '#',
                'htmlOptions' => array('data-dismiss' => 'modal','onclick'=>'closepopup()'),
                    )
            );
            ?>
        </div>

<?php $this->endWidget(); ?>
 <?php 
    function timeAgo($time_ago)
    {
        $time_ago = strtotime($time_ago);
        $cur_time   = time();
        $time_elapsed   = $cur_time - $time_ago;
        $seconds    = $time_elapsed ;
        $minutes    = round($time_elapsed / 60 );
        $hours      = round($time_elapsed / 3600);
        $days       = round($time_elapsed / 86400 );
        $weeks      = round($time_elapsed / 604800);
        $months     = round($time_elapsed / 2600640 );
        $years      = round($time_elapsed / 31207680 );
        // Seconds
        if($seconds <= 60){
            return "just now";
        }
        //Minutes
        else if($minutes <=60){
            if($minutes==1){
                return "one minute ago";
            }
            else{
                return "$minutes minutes ago";
            }
        }
        //Hours
        else if($hours <=24){
            if($hours==1){
                return "an hour ago";
            }else{
                return "$hours hrs ago";
            }
        }
        //Days
        else if($days <= 7){
            if($days==1){
                return "yesterday";
            }else{
                return "$days days ago";
            }
        }
        //Weeks
        else if($weeks <= 4.3){
            if($weeks==1){
                return "a week ago";
            }else{
                return "$weeks weeks ago";
            }
        }
        //Months
        else if($months <=12){
            if($months==1){
                return "a month ago";
            }else{
                return "$months months ago";
            }
        }
        //Years
        else{
            if($years==1){
                return "one year ago";
            }else{
                return "$years years ago";
            }
        }
    }
 ?>
