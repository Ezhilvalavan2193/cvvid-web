<?php
/* @var $this AgencyStaffController */
/* @var $model AgencyStaff */

$this->breadcrumbs=array(
	'Agency Staff'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List AgencyStaff', 'url'=>array('index')),
	array('label'=>'Create AgencyStaff', 'url'=>array('create')),
	array('label'=>'Update AgencyStaff', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AgencyStaff', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AgencyStaff', 'url'=>array('admin')),
);
?>

<h1>View AgencyStaff #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'agency_id',
		'user_id',
		'created_at',
		'updated_at',
	),
)); ?>
