<?php
/* @var $this AgencyStaffController */
/* @var $model AgencyStaff */

$this->breadcrumbs=array(
	'Agency Staff'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AgencyStaff', 'url'=>array('index')),
	array('label'=>'Create AgencyStaff', 'url'=>array('create')),
	array('label'=>'View AgencyStaff', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AgencyStaff', 'url'=>array('admin')),
);
?>

<h1>Update AgencyStaff <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>