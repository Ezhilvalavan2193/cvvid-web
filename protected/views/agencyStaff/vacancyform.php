<script>

$(document).ready(function(event){
	
// 	$('#skills').on('select2:selecting', function(e) {
		
// 		 var id = e.params.args.data.id;
// 		 var text = e.params.args.data.text;
// 		   // console.log('Selecting: ' , e.params.args.data);
// 		 $('#skills-block').append('<div class="skill-selected" id='+id+'><div>'+text+'</div><input name='+id+' class="form-control" style="width:10%;display:inline" type="text"> <div style="display:inline">(No. of vacancies)</div></div>');
		
// 	});

// 	 $('#skills').on('select2:unselecting', function(e) {
// 		    var id = e.params.args.data.id;
// 		    $('#skills-block').find('#'+id).remove();
// 	});
		
		
	$("#competitive_salary").click(function() {
		
		if($(this).is(":checked"))
		{
			$('#min_salary').attr('disabled',true);
			$('#max_salary').attr('disabled',true);
		}
		else
		{
			$('#min_salary').attr('disabled',false);
			$('#max_salary').attr('disabled',false);
		}

		
	});	
		
		
	$(".request-industry").click(function() {
		
			$('#IndustryModal').addClass('in');
            $('body').addClass('modal-open');
            $('#IndustryModal').show();
		
	});

	$(".SaveIndustryBtn").click(function() {
		var sel_industry = $("#industry_name").val();
		var job_id = $("#job_id").val();
		$.ajax({
            url: '<?php echo $this->createUrl('employers/addIndustry') ?>',
            type: 'POST',
            data: {value : sel_industry, job_id : job_id}, 	    
            success: function(data) {
             //   alert(data);
//             	$('#industry').val();
            	$('#industry-select').html(data);
            	closepopup();
            	//$('#industry').val(sel_industry);
            },
		    error: function(data) {		
		        alert('err');
		    }
         });
	});

	$(".request-industry-exp").click(function() {
		
			$('#SkillCategModal').addClass('in');
            $('body').addClass('modal-open');
            $('#SkillCategModal').show();
	
	});

	$(".SaveSkillCategBtn").click(function() {
		var skillcateg = $("#skillcategname").val();
                var job_id = $("#job_id").val();
		$.ajax({
            url: '<?php echo $this->createUrl('employers/addSkillCategory') ?>',
            type: 'POST',
            data: {value : skillcateg, job_id : job_id}, 	    
            success: function(data) {
//             	$('#industry').val();
            	$('#skill_category_id').html(data);
            	closepopup();
            	 $('.request-career').removeClass('hidden');
            	 $('#skills').empty();
            	//$('#industry').val(sel_industry);
            },
		    error: function(data) {		
		        alert('err');
		    }
         });
	});

	$(".request-career").click(function() {
		$('#SkillsModal').addClass('in');
        $('body').addClass('modal-open');
        $('#SkillsModal').show();
	});

	$(".SaveSkillBtn").click(function() {
		var skillcateg = $("#skill_category_id").val();
		var skillname = $('#skillname').val();
                var job_id = $("#job_id").val();
		$.ajax({
            url: '<?php echo $this->createUrl('employers/addSkill') ?>',
            type: 'POST',
            data: {value : skillname,skill_category_id : skillcateg, job_id : job_id}, 	    
            success: function(data) {
            	 //$('#skills').empty(); //remove all child nodes
            	
                $('#skills').append(data);
                $('#skills').trigger('liszt:updated');
               
//                var id =  $('#skills option:last-child').val();
//                var text = $('#skills option:last-child').text();
//        		   $('#skills-block').append('<div class="skill-selected" id='+id+'><div>'+text+'</div><input name='+id+' class="form-control" style="width:10%;display:inline" type="text"> <div style="display:inline">(No. of vacancies)</div></div>');
       		   closepopup();
            },
		    error: function(data) {		
		        alert('err');
		    }
         });
	});

	
	$("#add_careers").click(function() {
		
		$('#CareersModal').addClass('in');
        $('body').addClass('modal-open');
        $('#CareersModal').show();

	});

	 
	
});


$(document).click(function(event) {
	 if ($(event.target).closest(".modal,#CareersModal").length && $("#CareersModal").hasClass('in')) {		  
		 	$('#CareersModal').hide();
	 }	
});
</script>
<script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
   <script type="text/javascript">
        tinymce.init({
            selector: 'textarea',
            plugins: 'pagebreak code link',
            pagebreak_split_block: true,
            min_height: 300,
            link_list: [
                {title: 'Home', value: 'http://www.cvvid.com'},
                {title: 'Login', value: 'http://www.cvvid.com/auth/login'},
                {title: 'About Us', value: 'http://www.cvvid.com/about-us'},
                {title: 'Worry List', value: 'http://www.cvvid.com/worry-list'},
                {title: 'How To', value: 'http://www.cvvid.com/how-to'},
                {title: 'Employers', value: 'http://www.cvvid.com/employers'},
                {title: 'Top Tips', value: 'http:///www.cvvid.com/top-tips'},
                {title: 'Success Stories', value: 'http://www.cvvid.com/success-stories'},
                {title: 'Contact Us', value: 'http:///www.cvvid.com/contact-us'},
                {title: 'Candidate Register', value: 'http://www.cvvid.com/candidate/register'},
                {title: 'Employer Register', value: 'http://www.cvvid.com/employer/register'}
            ],
            link_class_list: [
                {title: 'None', value: ''},
                {title: 'Yellow Text', value: 'yellow'},
                {title: 'Default', value: 'btn btn-default'},
                {title: 'Primary', value: 'btn btn-primary'}
            ]
        });
    </script>
<div class="page-title text-left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1> 
                    Post a Vacancy
                </h1>
            </div>
        </div>
    </div>
</div>
<div id="page-content">
    <div class="container">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'agency-can-form',
            'action' => ($agencyvacmodel->isNewRecord ? $this->createUrl('agencyStaff/saveVacancy',array('id'=>$agencymodel->id)) : $this->createUrl('agencyStaff/updateVacancy', array('id' => $agencyvacmodel->id))),
            'enableAjaxValidation' => false,
            'htmlOptions' => array('enctype' => 'multipart/form-data','class' => 'form-horizontal','autocomplete'=>'off'),
        ));
        ?>
        <div class="form-container">
            <?php //echo $form->errorSummary(array($model, $jobskillmodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
            <input type="hidden" name="job_id" id="job_id" value="<?php echo $model->id; ?>">
            <div class="row">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                             <h4>Job Specifics</h4>
                                <?php echo $form->textField($model, 'title', array('class' => 'form-control', 'placeholder' => "Title*")); ?>
                            </div>
                        </div>
                    </div><!-- .row -->
            
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <select name="type" class="form-control">
                                    <!--<option value="">Select</option>-->
                                    <option value="Permanent" <?php echo $model->type == "Permanent" ? "selected" : "selected" ?>>Permanent</option>
                                    <option value="Temporary" <?php echo $model->type == "Temporary" ? "selected" : "" ?>>Temporary</option>
                                    <option value="Fixed Term Contract" <?php echo $model->type == "Fixed Term Contract" ? "selected" : "" ?>>Fixed Term Contract</option>
                                    <option value="Work Experience" <?php echo $model->type == "Work Experience" ? "selected" : "" ?>>Work Experience</option>
                                    <option value="Apprenticeships" <?php echo $model->type == "Apprenticeships" ? "selected" : "" ?>>Apprenticeships</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12">
                          <div class="form-group">
                           
                           	 <div class="col-sm-6">
                                <div class="form-group">
                                	<select name="industry_id" class="form-control select2" id="industry-select">
                                        <!--<option selected="selected" disabled>Select Industry</option>-->
                                        <?php 
                                        
                                        $cri1 = new CDbCriteria();
                                        $cri1->condition = "deleted_at is null";
                                        $data1 = Industries::model()->findAll($cri1); 
                                        
                                        $data1 = CHtml::listData($data1,'id','name');
                                        $tot = $data1;
                                        
//                                             $cri = new CDbCriteria();
//                                             $cri->condition = "id not in (select industry_id from cv16_temp_industry where employer_id =".$empmodel->id.") and deleted_at is null";
//                                             $industrymodel = Industries::model()->findAll($cri);
                                            
                                            
//                                             $cri1 = new CDbCriteria();
//                                             $cri1->condition = "deleted_at is null";
//                                             $industrymodel = Industries::model()->findAll($cri1);
                                            ?>
<!--                                          <option value="">Select Industry</option> -->
                                        <?php  foreach($tot as $value=>$name)  {
                                           
                                            ?>                  
                                            <option value="<?php echo $value ?>" <?php echo $value == $model->industry_id ? "selected" : "" ?>> <?php echo $name ?> </option>
                                        <?php 
                                           }
                                         ?>
                               		 </select>
                                 </div>
                        	  </div>
<!--                         	  <div class="col-sm-6"> -->
<!--                                 <button type="button" class="btn btn-primary request-industry">Request New Industry</button> -->
<!--                             </div> -->
                          
                          </div>
                        </div>
                    </div><!-- .row -->
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" name="end_date" placeholder="Deadline" class="datepicker form-control" value="<?php echo (isset($model) ? $model->end_date : '') ?>">
                            </div>
                        </div><!-- .col-sm-6 -->
                    </div><!-- .row -->

					<div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <select name="employer_id" class="form-control">
                                 <option>Select Employer</option>  
                                    <?php 
                                        $cri = new CDbCriteria();
                                        $cri->condition = "deleted_at is null";
                                        $empmodel = Employers::model()->findAll($cri);
                                        foreach ($empmodel as $emp)
                                        { ?>
                                             <option value="<?php echo $emp['id'] ?>" <?php echo ($model->employer_id == $emp['id'] ? "selected" : "") ?>><?php echo $emp['name'] ?></option>  
                                   <?php } ?>
                                   
                                   
                                </select>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-salary">
                        <div class="row">
                            <div class="col-sm-4">
                                
                                <div class="form-group">
                                <h4>Salary Type</h4>
                                    <div class="radio-inline">
                                        <label>
                                            <input checked="checked" name="salary_type" type="radio" value="annual" <?php echo $model->salary_type == "annual" ? "checked" : "" ?>>
                                            Annual
                                        </label>
                                    </div>
                                    <div class="radio-inline">
                                        <label>
                                            <input name="salary_type" type="radio" value="hourly" <?php echo $model->salary_type == "hourly" ? "checked" : "" ?>>
                                            Hourly
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-4">                                
                                <div class="form-group">          
                                        <label>
                                            <input name="competitive_salary" type="checkbox" id="competitive_salary" <?php echo $model->competitive_salary == 1 ? "checked" : "" ?>>
                                            Competitive Salary
                                        </label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">&pound;</span>
                                        <?php echo $form->textField($model, 'salary_min', array('class' => 'form-control', 'placeholder' => 'Min Salary','id'=>'min_salary','disabled'=>($model->competitive_salary == 1 ? true : false))); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">&pound;</span>
                                        <?php echo $form->textField($model, 'salary_max', array('class' => 'form-control', 'placeholder' => 'Max Salary','id'=>'max_salary','disabled'=>($model->competitive_salary == 1 ? true : false))); ?>
                                    </div>
                                </div>
                            </div>
                        </div><!-- .row -->
                    </div><!-- .form-salary -->

                </div>
            </div>
        </div><!-- .form-container -->

        <div class="form-container">
           
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                    <h4>Required Industry Experience</h4>
                            <div class="col-sm-6">
                                <div class="form-group">
                        <select name="Jobs[skill_category_id]" id="skill_category_id" class="form-control select2">
                            <option value="">-Please Select-</option>
                            <?php
//                                 $cri = new CDbCriteria();
//                                 $cri->order = "name asc";
//                                 $skillcategmodel = SkillCategories::model()->findAll($cri);
                              
                                $cri1 = new CDbCriteria();
                                $cri1->order = "name asc";
                                $data1 = SkillCategories::model()->findAll($cri1);                                
                                $data1 = CHtml::listData($data1,'id','name');
                                
                                $tot = $data1;
                                
                                foreach($tot as $value=>$name)  {
                                   
                                ?>                  
                                <option value="<?php echo $value ?>"  <?php echo ($model->skill_category_id==$value?"selected":"") ?>> <?php echo $name ?> </option>
                            <?php  
                                }
                            ?>
                        </select>
                                </div>
                            </div>
<!--                             <div class="col-sm-6"> -->
<!--                                 <button type="button" class="btn btn-primary request-industry-exp">Request New Industry Experience</button> -->
<!--                             </div> -->
                       
                    </div>
                </div>
            </div>
             <div class="row">
                <div class="col-sm-12">
                <h4>Career Skills</h4>
                <span>Please select a maximum of 5 career choices from the dropdown menu</span>
                    <div class="form-group">
                        
                       
                        
                            <div class="col-sm-6">
                                <div class="form-group">
                                 <?php 
                                    if($model->skill_category_id)
                                   {
                                       
                                       $skillmodel = Skills::model()->findAllByAttributes(array('skill_category_id'=>$model->skill_category_id));
                                       $seletedskills = CHtml::listData($jobskillmodel, 'skill_id', 'skill_id');
                                       ?>
                                        <select name="skills[]" id="skills" class="form-control select2" multiple="" data-placeholder="Select Career Choices">
                                            <option value="">Select</option>
                                            <?php 
                                        foreach ($skillmodel as $mcat) { 
                                            
                                            ?>
                                        <option  value='<?php echo $mcat['id']; ?>' <?php echo (in_array($mcat['id'], $seletedskills) ? 'selected' : ''); ?>><?php echo $mcat['name']; ?></option>
                                       <?php 
                                          }
                                ?>
                         		 </select>
                         		 
                                 <?php   }else {  ?>
                                 
                                    <select name="skills[]" id="skills" class="form-control select2" multiple="" data-placeholder="Select Career Choices">
                                        <option value="">Select</option>
                                    </select>
                                    
                                    <?php } ?>
<!--                                     <div id="skills-block"></div> -->
                               </div>
                            </div>
<!--                             <div class="col-sm-6"> -->
<!--                                 <button type="button" class="btn btn-primary request-career hidden">Request New Career Skill</button> -->
<!--                             </div> -->
                        
                    </div>
                </div>
            </div>
            
      	 <div class="row">
            <div class="col-sm-4">
           
                <div class="form-group">
                 <h4>No. of Vacancies</h4>
            		 <?php echo $form->textField($model,'vacancies', array('class'=>'form-control', 'id' => 'vacancies')); ?>
             	</div>
            </div>
         </div>
          <div class="row">
            <div class="col-sm-4">
            
                <div class="form-group">
                <h4>Upload Job Description file</h4>
                	 <label for="choose_jd_file" class="main-btn"><i class="fa fa-file"></i>Choose File</label>
            		 <input id="choose_jd_file" name="jdfile" type="file" accept=".doc,.docx,.pdf,.txt" style="display: none">
             	</div>
            </div>
         </div>
          <div class="form-container">        
          <div class="row">
             <div class="col-sm-3">
                 <div class="form-group">
                  <h4>Candidates with videos only</h4>
                      <?php echo CHtml::checkBox('video_only',$model->video_only == 1 ? true : false,array()); ?>
                 </div>
        	 </div>
         </div>
    	 </div>
        <div class="form-container">
            
            <!-- Description form field -->
            <div class="form-group">
            <h4>Job Description</h4>
                <?php echo CHtml::textArea('description', $model->description, array('empty' => 'Click ball to enter/edit comments','id' => 'comment_box')); ?>
            </div>
        </div>

        <div class="form-container">
            
            <div class="form-group">
            <h4>Required & Desired</h4>
            <span>For example, number of years experience, qualifications, languages etc.</span>
                <?php echo CHtml::textArea('additional',  $model->additional, array('empty' => 'Click ball to enter/edit comments','class'=>'form-control', 'id' => 'additional','rows'=>'10','cols'=>'50')); ?>
            </div>
        </div>
        <!--  <div class="form-container">        
          <div class="row">
             <div class="col-sm-3">
                 <div class="form-group">
                  <h4>Add to <a href="http://cvvidcareers.co.uk" target="_blank" style="text-decoration: underline;">CVVID Careers</a><a href="#/" id="add_careers">[?]</a></h4>
                      <?php //echo CHtml::checkBox('careers',false ,array()); ?>
                 </div>
        	 </div>
         </div>
    	 </div>-->
         <?php 
               
            //if (($empmodel->trial_ends_at != null && Membership::onTrail($trail)) || $empmodel->stripe_active) {
                ?>
            <div class="form-actions">
                <?php
                echo CHtml::submitButton('Post Now', array("class" => "btn btn-primary",'style'=>'margin-right:10px'));
                echo CHtml::button('Cancel', array("class" => "btn btn-default", "onclick" => "window.location='" . Yii::app()->createUrl("agency/vacancies",array('id'=>$agencymodel->id)). "'"));
                ?>
            </div> 
        <?php //} else { ?>
            <div class="form-actions">

            </div>
        <?php //} ?>
        
          <?php $this->endWidget(); ?>


    </div>
</div>
<div id="IndustryModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closepopup()"><span aria-hidden="true">X</span></button>
                    <h4 class="modal-title">Add Industry</h4>
                </div>
                <div class="modal-body">
                    <form class="MessageForm">                        
                       
                        <div class="form-group">
                            <label for="industry_name">Industry Name</label>
                            <input name="industry_name" type="text" id="industry_name" class="form-control" value="">
                        </div>
                       
                    </form>
                    <div class="modal-loader">
                        <img src="<?php echo Yii::app()->baseUrl?>/images/loading.gif">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default-btn" data-dismiss="modal" onclick="closepopup()">Cancel</button>
                    <button type="button" class="SaveIndustryBtn btn main-btn">Save</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    
    <div id="SkillCategModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closepopup()"><span aria-hidden="true">X</span></button>
                    <h4 class="modal-title">Add Industry Experience</h4>
                </div>
                <div class="modal-body">
                    <form class="MessageForm">                        
                       
                        <div class="form-group">
                            <label for="skillcategname">Name</label>
                            <input name="skillcategname" type="text" id="skillcategname" class="form-control" value="">
                        </div>
                       
                    </form>
                    <div class="modal-loader">
                        <img src="<?php echo Yii::app()->baseUrl?>/images/loading.gif">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default-btn" data-dismiss="modal" onclick="closepopup()">Cancel</button>
                    <button type="button" class="SaveSkillCategBtn btn main-btn">Save</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    
    <div id="SkillsModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closepopup()"><span aria-hidden="true">X</span></button>
                    <h4 class="modal-title">Add Career Choices</h4>
                </div>
                <div class="modal-body">
                    <form class="MessageForm">                        
                       
                        <div class="form-group">
                            <label for="skillname">Name</label>
                            <input name="skillname" type="text" id="skillname" class="form-control" value="">
                        </div>
                       
                    </form>
                    <div class="modal-loader">
                        <img src="<?php echo Yii::app()->baseUrl?>/images/loading.gif">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default-btn" data-dismiss="modal" onclick="closepopup()">Cancel</button>
                    <button type="button" class="SaveSkillBtn btn main-btn">Save</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    
     <div id="CareersModal" class="modal fade" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closepopup()"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add to CVVID Careers</h4>
                </div>
                <div class="modal-body">
                    <p><b>CVVID Careers</b> is a resource for <b>young talent</b> which is being delivered through <b>schools, colleges and universities</b>.</p>
                    <p>If you want to promote your vacancy to these target groups, please tick the box and we will add it to the jobs board on this site.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="closepopup()">Ok</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    
 <script>
        $(document).ready(function(){
        $('body').on('change','#skill_category_id',function(e){	
                var id = $(this).val();
                e.preventDefault();
                if(id > 0)
                {
                    $.ajax({
                       url: '<?php echo $this->createUrl('jobs/loadSkills') ?>',
                       type: 'POST',
                       data: {id : id},
                       success: function(data) {
    //                       console.log(data);
                               $('#skills').empty(); //remove all child nodes
                               $('#skills').append(data);
                               $('#skills').trigger('liszt:updated');
    						   $('.request-career').removeClass('hidden');
    						   //$('#skills-block').find('.skill-selected').remove();
                       },
                        error: function(data) {		
                            //alert('data');
                        }
                   });
                }
                else
                	 $('.request-career').addClass('hidden');
        });
        });
    </script>