<?php
/* @var $this AgencyStaffController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Agency Staff',
);

$this->menu=array(
	array('label'=>'Create AgencyStaff', 'url'=>array('create')),
	array('label'=>'Manage AgencyStaff', 'url'=>array('admin')),
);
?>

<h1>Agency Staff</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
