<?php
/* @var $this AgencyEmployersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Agency Employers',
);

$this->menu=array(
	array('label'=>'Create AgencyEmployers', 'url'=>array('create')),
	array('label'=>'Manage AgencyEmployers', 'url'=>array('admin')),
);
?>

<h1>Agency Employers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
