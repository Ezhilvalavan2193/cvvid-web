<?php
/* @var $this AgencyEmployersController */
/* @var $model AgencyEmployers */

$this->breadcrumbs=array(
	'Agency Employers'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AgencyEmployers', 'url'=>array('index')),
	array('label'=>'Create AgencyEmployers', 'url'=>array('create')),
	array('label'=>'View AgencyEmployers', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AgencyEmployers', 'url'=>array('admin')),
);
?>

<h1>Update AgencyEmployers <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>