<?php
/* @var $this AgencyEmployersController */
/* @var $model AgencyEmployers */

$this->breadcrumbs=array(
	'Agency Employers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AgencyEmployers', 'url'=>array('index')),
	array('label'=>'Manage AgencyEmployers', 'url'=>array('admin')),
);
?>

<h1>Create AgencyEmployers</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>