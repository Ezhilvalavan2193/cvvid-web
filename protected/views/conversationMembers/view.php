<?php
/* @var $this ConversationMembersController */
/* @var $model ConversationMembers */

$this->breadcrumbs=array(
	'Conversation Members'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List ConversationMembers', 'url'=>array('index')),
	array('label'=>'Create ConversationMembers', 'url'=>array('create')),
	array('label'=>'Update ConversationMembers', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ConversationMembers', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ConversationMembers', 'url'=>array('admin')),
);
?>

<h1>View ConversationMembers #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'conversation_id',
		'user_id',
		'name',
		'last_read',
		'created_at',
		'updated_at',
		'deleted_at',
	),
)); ?>
