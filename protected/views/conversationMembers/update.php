<?php
/* @var $this ConversationMembersController */
/* @var $model ConversationMembers */

$this->breadcrumbs=array(
	'Conversation Members'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ConversationMembers', 'url'=>array('index')),
	array('label'=>'Create ConversationMembers', 'url'=>array('create')),
	array('label'=>'View ConversationMembers', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ConversationMembers', 'url'=>array('admin')),
);
?>

<h1>Update ConversationMembers <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>