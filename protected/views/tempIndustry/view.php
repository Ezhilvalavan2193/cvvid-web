<?php
/* @var $this TempIndustryController */
/* @var $model TempIndustry */

$this->breadcrumbs=array(
	'Temp Industries'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TempIndustry', 'url'=>array('index')),
	array('label'=>'Create TempIndustry', 'url'=>array('create')),
	array('label'=>'Update TempIndustry', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TempIndustry', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TempIndustry', 'url'=>array('admin')),
);
?>

<h1>View TempIndustry #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'employer_id',
		'industry_id',
		'active',
	),
)); ?>
