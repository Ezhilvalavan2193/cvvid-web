<?php
/* @var $this TempIndustryController */
/* @var $model TempIndustry */

$this->breadcrumbs=array(
	'Temp Industries'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TempIndustry', 'url'=>array('index')),
	array('label'=>'Manage TempIndustry', 'url'=>array('admin')),
);
?>

<h1>Create TempIndustry</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>