<?php
/* @var $this TempIndustryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Temp Industries',
);

$this->menu=array(
	array('label'=>'Create TempIndustry', 'url'=>array('create')),
	array('label'=>'Manage TempIndustry', 'url'=>array('admin')),
);
?>

<h1>Temp Industries</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
