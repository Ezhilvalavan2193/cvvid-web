<?php
/* @var $this ProfileViewsController */
/* @var $model ProfileViews */

$this->breadcrumbs=array(
	'Profile Views'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ProfileViews', 'url'=>array('index')),
	array('label'=>'Manage ProfileViews', 'url'=>array('admin')),
);
?>

<h1>Create ProfileViews</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>