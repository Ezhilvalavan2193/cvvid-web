<?php
/* @var $this ProfileViewsController */
/* @var $model ProfileViews */

$this->breadcrumbs=array(
	'Profile Views'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ProfileViews', 'url'=>array('index')),
	array('label'=>'Create ProfileViews', 'url'=>array('create')),
	array('label'=>'Update ProfileViews', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ProfileViews', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProfileViews', 'url'=>array('admin')),
);
?>

<h1>View ProfileViews #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'profile_id',
		'model_type',
		'model_id',
		'view_count',
		'created_at',
		'updated_at',
	),
)); ?>
