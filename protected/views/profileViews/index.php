<?php
/* @var $this ProfileViewsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Profile Views',
);

$this->menu=array(
	array('label'=>'Create ProfileViews', 'url'=>array('create')),
	array('label'=>'Manage ProfileViews', 'url'=>array('admin')),
);
?>

<h1>Profile Views</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
