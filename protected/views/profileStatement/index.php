<?php
/* @var $this ProfileStatementController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Profile Statements',
);

$this->menu=array(
	array('label'=>'Create ProfileStatement', 'url'=>array('create')),
	array('label'=>'Manage ProfileStatement', 'url'=>array('admin')),
);
?>

<h1>Profile Statements</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
