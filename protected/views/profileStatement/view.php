<?php
/* @var $this ProfileStatementController */
/* @var $model ProfileStatement */

$this->breadcrumbs=array(
	'Profile Statements'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ProfileStatement', 'url'=>array('index')),
	array('label'=>'Create ProfileStatement', 'url'=>array('create')),
	array('label'=>'Update ProfileStatement', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ProfileStatement', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProfileStatement', 'url'=>array('admin')),
);
?>

<h1>View ProfileStatement #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'profile_id',
		'user_statement_id',
		'created_at',
		'updated_at',
	),
)); ?>
