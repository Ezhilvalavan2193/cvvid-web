<?php

/* @var $this InstitutionsController */
/* @var $model Institutions */
/* @var $form CActiveForm */
?>

<?php

$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
        ));
?>

<?php echo $form->textField($model, 'name', array('class' => 'form-control','placeholder'=>'Search')); ?>

<?php

// $empmodel = new Employers;
$skmodel = SkillCategories::model()->findAll();
$skclist = CHtml::listData($skmodel, 'id', 'name');
echo $form->dropDownList($skillcmodel, 'id', $skclist, array('class' => 'form-control', 'empty' => '- Filter Category -'));
?>

<?php

echo CHtml::submitButton('Search', array("class" => 'btn btn-primary'));
?>

<?php $this->endWidget(); ?>

