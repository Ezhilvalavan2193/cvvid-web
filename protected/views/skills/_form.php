<?php
/* @var $this IndustriesController */
/* @var $model Industries */
/* @var $form CActiveForm */
?>

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'skills-form',
    'action' => $this->createUrl('skills/createSkills'),
    'enableAjaxValidation' => false,
    'htmlOptions' => array('enctype' => 'multipart/form-data')
        ));
?>
<?php echo  $form->errorSummary(array($model), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <?php echo $form->labelEx($model, 'name'); ?>
            <?php echo $form->textField($model, 'name', array('class' => 'form-control', 'placeholder' => 'Name')); ?>
        </div>
        <div class="form-group">
            <?php
            // $empmodel = new Employers;
            $skmodel = SkillCategories::model()->findAll();
            $skclist = CHtml::listData($skmodel, 'id', 'name');
            echo $form->dropDownList(SkillCategories::model(), 'id', $skclist, array('class' => 'form-control'));
            ?>
        </div>
        <?php echo CHtml::submitButton('Save', array("class" => "btn btn-primary")); ?>
        <?php echo CHtml::button('Cancel', array("class" => 'btn btn-primary', "onClick" => "window.location='" . Yii::app()->createUrl('skills/admin', array()) . "'")); ?>

    </div>
</div>

<?php $this->endWidget(); ?>

