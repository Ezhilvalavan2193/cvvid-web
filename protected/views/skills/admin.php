<?php
/* @var $this SkillsController */
/* @var $model Skills */

$this->breadcrumbs = array(
    'Skills' => array('index'),
    'Manage',
);

//$this->menu=array(
//	array('label'=>'List Skills', 'url'=>array('index')),
//	array('label'=>'Create Skills', 'url'=>array('create')),
//);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#skills-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<script>
    $(document).ready(function () {


        $('#delete-skills').click(function (e) {


            if ($("#skills-grid").find("input:checked").length > 0)
            {

                if (confirm('Are you sure you want to delete these skills?')) {

                    var ids = $.fn.yiiGridView.getChecked("skills-grid", "selectedIds");

                    $.ajax({
                        url: '<?php echo $this->createUrl('skills/deleteSkills') ?>',
                        type: 'POST',
                        data: {ids: ids},
                        success: function (data) {
                            location.reload();
                        },
                        error: function (data) {
                            alert('err');
                        }
                    });
                }

            } else
            {
                alert('Please select at least one item');
            }
        });

    });

</script>
<h1>Career Paths</h1>
<div class='Admin__content__inner'>
    <?php if (Yii::app()->user->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>
    <div class='AdminFilters'>
        <div class="form-inline">
            <?php
            $this->renderPartial('_search', array(
                'model' => $model, 'skillcmodel' => $skillcmodel
            ));
            ?>
        </div>
        <div class="pull-right">
            <?php
            echo CHtml::button('Create', array("class" => "btn btn-primary", "onclick" => "window.location='" . Yii::app()->createUrl('skills/create') . "'"));
            echo CHtml::button('Delete', array("class" => "btn btn-primary", 'id' => 'delete-skills'));
            ?>
        </div>
    </div>

    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'skills-grid',
        'itemsCssClass' => 'table',
        'summaryText' => '',
        //'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',
    'pager' => array(
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'header'=>'',
        'firstPageLabel' => '<<<',
        'lastPageLabel' => '>>>',
        'nextPageLabel' => '>>',
        'prevPageLabel' => '<<'),
        'ajaxUpdate' => false,
        'dataProvider' => $model->search(),
        //'filter'=>$model,
        'columns' => array(
            //'id',
            array(
                // 'name' => 'check',
                'id' => 'selectedIds',
                'value' => '$data->id',
                'class' => 'CCheckBoxColumn',
                'selectableRows' => 2,
            // 'checked'=>'Yii::app()->user->getState($data->id)',
            // 'checkBoxHtmlOptions' => array('name' => 'idList[]'),
            ),
            array(
                'header' => 'Name',
                'value' => 'CHtml::link("<i class=\"fa fa-pencil\"></i> ".$data->name,Yii::app()->createUrl("skills/skillsEdit",array("id"=>$data->id)),array("class"=>"skills-edit-button","id"=>"skills-edit"))',
                'type' => 'raw'
            ),
            array(
                'header' => 'Industry',
                'value' => '$data->skill_category_id > 0 ? $data->getName() : ""',
                'type' => 'raw'
            ),
        ),
    ));
    ?>

</div>


