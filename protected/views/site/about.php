<?php
/* @var $this SiteController */
?>
<script type="text/javascript">
$(document).ready(function(){
   
        var link = "<?php echo Yii::app()->baseUrl.'/site/top-tips'; ?>";
        $('a.btn').attr('href',link);
    
});
</script>
<div id="page-content">
    <section id="about-us1">
<?php 
  if($model->background_id > 0)
  {
      $mediamodel = Media::model()->findByPk($model->background_id);
      if($mediamodel != null && $mediamodel->file_name != null)
      {
          $src = Yii::app()->baseUrl.'/images/media/'.$mediamodel->id.'/'.$mediamodel->file_name; ?>
    <div class="fullscreen" style="background-image: url('<?php echo $src; ?>'); background-position: <?php echo $model->background_x; ?> <?php echo $model->background_x; ?>;">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">

                    <div class="Slide__content">
                    <h1>ABOUT CVVID</h1>
                        <div class="PageSlideshow">
                            <div class="PageSlideshow__slides">
                                <?php
                                foreach ($slidemodel as $slide) {
                                    ?>
                                    <div class="PageSlideshow__slide">
                                        <?php echo $slide['body']; ?>
                                    </div>
                                <?php } ?>

                            </div>
                            <div class="PageSlideshow__controls" style="margin-top: 20px;">
                                <a href="#" class="PageSlideshow__nav-btn PageSlideshow__prev"><i class="fa fa-chevron-left"></i></a>
                            <span class="PageSlideshow__pages">
                                <span class="PageSlideshow__current-page">1</span>/<span class="PageSlideshow__count">4</span>
                            </span>
                                <a href="#" class="PageSlideshow__nav-btn PageSlideshow__next"><i class="fa fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>

                </div>
                
        </div>
    </div>
  <?php } } else { ?>
    
    <div class="Slideshow FullscreenSlideshow" data-autoplay="true" data-autoplay-speed="4000" data-dots="false" data-arrows="false">
        <?php
        if (isset($slidemodel)){
        $cnt = 0;
        foreach ($slidemodel as $slide) { 
           $mediamodel = Media::model()->findByPk($slide['media_id']);  
        ?>
        <div class="Slide" style="background-image: url('<?php echo Yii::app()->baseUrl . "/images/media/" . $mediamodel->id . "/" . $mediamodel->file_name; ?>'); background-position: <?php echo $slide['background_x']; ?> <?php echo $slide['background_y']; ?>;">
            <div class="container">
                <div class="Slide__inner">
                    <div class="Slide__caption">
                       <?php echo $slide['body']; ?>
                        <a href="<?php echo Yii::app()->createUrl('site/about'); ?>" class="btn btn-primary">Learn More</a>
                        <a href="#" class="view-showcase btn btn-primary">View CV Video Showcase</a>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php } ?>
    </div>
      <?php }  ?>
</div>
        </section>
        <section id="about-us2">
<div class="col-sm-12">
<h2 class="text-center padd-font">ABOUT CVVID</h2>
                <div class="swiper-container about-container">
        <div class="swiper-wrapper" >
        <?php
                                foreach ($slidemodel as $slide) {
                                    ?>
            <div class="swiper-slide">
            <?php echo $slide['body']; ?></div>
            <?php } ?>
            <!-- <li class="swiper-slide">Slide 2</li>
            <li class="swiper-slide">Slide 3</li>
            <li class="swiper-slide">Slide 4</li>
            <li class="swiper-slide">Slide 5</li>
            <li class="swiper-slide">Slide 6</li>
            <li class="swiper-slide">Slide 7</li>
            <li class="swiper-slide">Slide 8</li>
            <li class="swiper-slide">Slide 9</li>
            <li class="swiper-slide">Slide 10</li> -->
                                </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
    </div>
                <!-- <div class="swiper-container">
    <div class="swiper-wrapper">
    <?php
                                foreach ($slidemodel as $slide) {
                                    ?>
      <div class="swiper-slide">
      <?php echo $slide['body']; ?>
      </div>
      <?php } ?>
    </div>
    <!-- Add Pagination -->
    <!-- <div class="swiper-pagination"></div>
  </div> -->
            </div>
                                </section>
</div>

<script>
$(document).ready(function(){
    $('li.dropdown').click(function () {
		// alert ('hai');
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    // }, function () {
    //     $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
	});
    // alert('hai');
    // var swiper = new Swiper('.swiper-container', {
    //     pagination: '.swiper-pagination',
    //     // slidesPerView: 'auto',
    //     // paginationClickable: true,
    //     spaceBetween: 0,
    //     speed: 700,
    //             autoplay: 1500,
    // });
    var swiper = new Swiper('.about-container', {
      spaceBetween: 0,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
    });
});
   
  </script>
  <style>
        /* .swiper-container {
      width: 100%;
      height: 100%;
    }
    .swiper-slide {
      text-align: center;
      font-size: 18px;
      background: #fff;
      /* Center slide text vertically */
      /* display: -webkit-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      -webkit-box-pack: center;
      -ms-flex-pack: center;
      -webkit-justify-content: center;
      justify-content: center;
      -webkit-box-align: center;
      -ms-flex-align: center;
      -webkit-align-items: center;
      align-items: center;
    }  */

    .swiper-container {
        width: 100%;
        height: 100%;
    }
    .swiper-wrapper{
        padding-left: 1px;
      /* padding-left: initial; */
      margin: 0;
    }
    .swiper-slide h2{
        font-size:15px;
    }
    
    .swiper-slide {
        /* text-align: center; */
        padding: 20px;
    /* text-align: center; */
    font-size: 15px;
        background: #fff;
        width: 100%;
        /* border-right: 1px solid black; */
        /* Center slide text vertically */
        display: -webkit-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: block;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        -webkit-justify-content: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        -webkit-align-items: center;
        align-items: center;
    }
  </style>

<script>
     $(document).ready(function () {
        $('li.dropdown').click(function () {
		// alert ('hai');
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    // }, function () {
    //     $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
	});
        //  alert('hai');
        if ($(window).innerWidth() >= 600) {
     
            
        
        } else if ($(window).innerWidth() < 600) {
         
            

            
        }
    // var $drillDown = $("#drilldown");
});
     </script>