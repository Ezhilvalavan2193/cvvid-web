<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Digital'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#users-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});

$('#export-button').on('click',function() {
    $.fn.yiiGridView.export();
});

$.fn.yiiGridView.export = function() {
    $.fn.yiiGridView.update('passport-grid',{ 
    	success: function() {
    		$('#alumini-grid').removeClass('grid-view-loading');
    		window.location = '". $this->createUrl('exportFile')  . "';
    	},
    	data: $('.form-inline form').serialize() + '&export=true'
    });
}

");
?>
<script>
$(document).ready(function(){
	$('#export-passport').click(function()
	{
		var value =  window.location.href.substring(window.location.href.lastIndexOf('?') + 1);
		window.location = '<?php echo $this->createUrl("site/exportPassport") ?>'+"?"+value;
	});
});
</script>
<h1><?php echo "Digital Passport"; ?></h1>
<?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
<div class="Admin__content__inner">
    <div class="AdminFilters">
        <div class="form-inline">
         				
          <?php $this->renderPartial('_passportsearch',array(
                'model'=>$model
            )); 

//                 $digiarr = array('video_cv'=>'Video Profile','profile_image'=>'Have Profile Image','profile_completed'=>'50% Profile Completed','applied_jobs'=>'Applied for Jobs',
//                                  'secured_jobs'=>'Secured Jobs','video_secured_jobs'=>'Secured Jobs By Video');

          /*   $this->widget('zii.widgets.jui.CJuiAutoComplete',array(
                'name'=>'digital_passport',
                    'source'=>array('Video Profile','Have Profile Image','50% Profile Completed','Applied for Jobs','Secured Jobs','Secured Jobs By Video'),
                // additional javascript options for the autocomplete plugin
                'options'=>array(
                    'minLength'=>'1',
                ),
                'htmlOptions'=>array(
                   'class'=>'form-control'
                //'style'=>'height:20px;',
                ),
                ));*/
            ?>
           
        </div>
    </div>
    <div class="pull-right clearfix">
      <?php echo CHtml::button('Export', array('id'=>'export-passport','class'=>'btn btn-primary')); ?>
      </div>
  <?php //echo CHtml::button('Export', array('id'=>'export-button','class'=>'btn btn-primary')); ?>
<?php 
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'passport-grid',
     'itemsCssClass' => 'table',
//	'summaryText'=>'',
	//'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',    
    'enableSorting'=>true,
    'ajaxUpdate'=>true,
    
    'pager' => array(
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'header'=>'',
        'firstPageLabel' => '<<',
        'lastPageLabel' => '>>',
        'nextPageLabel' => '>>',
        'prevPageLabel' => '<<'),
			'ajaxUpdate'=>false,
    'dataProvider'=>$model->searchPassport(),
	'columns'=>array(  
	   
       array(
				'header'=>'Forenames',
				'value'=>'$data->forenames',	    			       
    		),
	    array(
	        'header'=>'Surname',
	        'value'=>'$data->surname',
	    ),
	    array(
	        'header'=>'Profile',
	        'value'=>'CHtml::link("View",$data->getSlugURL(),array("class"=>"profile-view-button","id"=>"profile-view"))',
	        'type'=>'raw'
	    ),
	    array(
	        'header'=>'Email',
	        'value'=>'$data->email',
	    ),
	),
)); ?>
</div>

