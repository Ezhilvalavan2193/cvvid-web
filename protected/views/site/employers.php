<div id="page-content">
    <section id="employers1">
    <div class="fullscreen" style="background-image: url('../images/slideshow/slide10.jpg'); background-position: 80% center;">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="Slide__content">
                        <h2>CVVID FOR EMPLOYERS.</h2>
                        <p>CVVID is proving to be an enormous asset to employers, making recruitment more effective
                            and more efficient. It can work for you too, saving you valuable time and money.</p>
                        <p>How?</p>
                        <p>Paper CVs are fine for listing academic achievements, but they cannot give an adequate
                            picture of a candidate’s personality - so crucial to the smooth running of your
                            business.</p>
                        <p>Now you don’t have to wade through hundreds of paper CVs. Instead, you can quickly
                            assess a candidate’s suitability based on a far more personal introduction – Video CV.
                            You can get a much clearer idea of what each candidate is about before calling them for
                            interview. It’s the next best thing to a face-to-face – and far quicker.</p>
                        <a href="<?php echo Yii::app()->createUrl('site/employerfaqs') ?>" class="btn main-btn">FAQs</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="mobile-site-col" id="employers2">
<div class="container mobile-site-con">
            <div class="row">
                <div class="col-sm-12">
                    <div class="Slide__content1">
                        <h2 class="text-center padd-font">CVVID FOR EMPLOYERS.</h2>
                        <p>CVVID is proving to be an enormous asset to employers, making recruitment more effective
                            and more efficient. It can work for you too, saving you valuable time and money.</p>
                        <p>How?</p>
                        <p>Paper CVs are fine for listing academic achievements, but they cannot give an adequate
                            picture of a candidate’s personality - so crucial to the smooth running of your
                            business.</p>
                        <p>Now you don’t have to wade through hundreds of paper CVs. Instead, you can quickly
                            assess a candidate’s suitability based on a far more personal introduction – Video CV.
                            You can get a much clearer idea of what each candidate is about before calling them for
                            interview. It’s the next best thing to a face-to-face – and far quicker.</p>
                        <a href="<?php echo Yii::app()->createUrl('site/employerfaqs') ?>" class="btn main-btn faqs">FAQs</a>
                    </div>
                </div>
            </div>
        </div>
</section>
</div>

