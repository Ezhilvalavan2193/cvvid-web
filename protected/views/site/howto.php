<?php
/* @var $this SiteController */
?>
 <style>
                                                                                                                                                                                                                                                                                                                                                                                                                            
                                #my-video {
font-size: 0.9em;
height: 0;
overflow: hidden;
padding-bottom: 53%;
padding-top: 20px;
position: relative;
width:100%!important;
}
#my-video embed, #my-video object, #my-video iframe {
max-height: 100%;
max-width: 100%;
height: 100%;
left: 0pt;
position: absolute;
top: 0pt;
width: 100%;
}
.jwplayer {
    width: 100%!important;}
                            </style>
<div id="page-content">
    <div id="how-to1">
    <div class="HowTo__header" style="background-image: <?php echo Yii::app()->theme->baseUrl."/images/success-stories-header.jpg"; ?>">
        <div class="container">
            <div class="row">
                <div class="col-sm-7">
                    <h1>How To - Create your video CV</h1>
                    <h4>You never get a second chance to make a first impression.</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="HowTo">
        <div class="container">
             <?php
        $cnt = 0;
        foreach ($videomodel as $video) {
            $cnt++;
            if ($cnt == 1) {
                $cri = new CDbCriteria();
                $cri->condition = "model_type like '%Video%' and model_id =" . $video['id'];
                $mediamodel = Media::model()->find($cri);
                ?>
            
             <?php
            } else {

                if ($cnt == 2) {
                    ?> 
            <h4>Some great tips on how to create your video CV</h4>
            <div class="HowTo__other-videos">
                <?php
                    }
                    $cri = new CDbCriteria();
                    $cri->condition = "model_type like '%Video%' and model_id =" . $video['id'];
                    $mediamodel = Media::model()->find($cri);
                    ?>
                <div class="VideoItem" style="width: 350px;height:auto;">
                    <div class="ModalVideo" data-video="<?php echo $video['id']; ?>" data-name="<?php echo $video['name']; ?>">
                        <div class="VideoItem__image">
                         <?php 
                            $bucket = "cvvid-new";
                           $videomodel = Videos::model()->findByPk($video['video_id']);
                            $url = 'http://' . Yii::app()->params['AWS_BUCKET'] . '.s3.amazonaws.com/how-to/'.$video['video_id'];
                        ?>
                                <div id="my-video<?php echo $cnt?>" ></div>
                                <script type="text/javascript">
                                jwplayer("my-video<?php echo $cnt ?>").setup({
                                 'file': '<?php echo $url; ?>',//'https://s3.us-east-2.amazonaws.com/cvvid/small.mp4',
                                 'image': '<?php echo $mediamodel != null ? Yii::app()->baseUrl."/images/media/".$mediamodel->id."/".$mediamodel->file_name : ""?>',
//                                 width: "100%",
//                                 height: "100%",
                                // 'primary': 'flash',
                                 //'autostart': 'true'
                                });
                                
                                </script>
                           <!--  <img src="<?php //echo Yii::app()->baseUrl . '/images/media/' . $mediamodel->id . '/conversions/profile.jpg' ?>" class="img-responsive"> -->
                        </div>
                    </div>
                    <div class="VideoItem__meta clearfix" style="margin:0">
                        <div class="VideoItem__title"><?php echo $video['name']; ?></div>
                        <div class="VideoItem__duration"><?php echo gmdate('i:s', $video['duration']); ?></div>
                    </div>
                </div>
                <?php
                    if (count($videomodel) == ($cnt - 1))
                        echo "</div>";
                }
            }
            ?>
            </div><!-- .SuccessStory__other-videos -->
        </div>
    </div>
        </div>
<?php
    $model = VideoCategories::model()->findByAttributes(array('slug' => 'how-to'));
    $videomodel = Videos::model()->findAllByAttributes(array('category_id' => $model->id, 'deleted_at' => null));
    ?>
    <section class="section" id="how-to2" style="padding-top: 15px;padding-bottom: 15px;background: #80808047;">
        <div class="container">
           
            <div class="divider divider-center divider-dark"></div>
            <div class="col-xs-12" style="background:white;">
            <h3 class="text-center padd-font">HOW TO - CREATE YOUR VIDEO CV.</h3>
            <h4>You never get a second chance to make a first impression.</h4>
                   <div class="Slide__content" style="padding-top:0px;">
                        <div class="PageSlideshow">
                            <div class="PageSlideshow__slides slides-show">
                        <?php
                        $cnt = 12;
                        foreach ($videomodel as $video) {
                        $cri = new CDbCriteria();
                        $cri->condition = "model_type like '%Video%' and model_id =" . $video['id'];
                        $mediamodel = Media::model()->find($cri);
                        
                        ?>
                        <?php 
                            $bucket = "cvvid-new";
                            
                           $videomodel = Videos::model()->findByPk($video['video_id']);
                            $url = 'https://' . Yii::app()->params['AWS_BUCKET'] . '.s3.amazonaws.com/how-to/'.$video['video_id'];
                        ?>
                        <div class="col-xs-3">
                       <div class="PageSlideshow__slide">

                            <div id="my-video-<?php echo $cnt; ?>">
                                <script type="text/javascript">
                                jwplayer("my-video-<?php echo $cnt; ?>").setup({
                                 'file': '<?php echo $url; ?>',
                                 'image': '<?php echo $mediamodel != null ? Yii::app()->baseUrl."/images/media/".$mediamodel->id."/".$mediamodel->file_name : ""?>',         
                                });
                                
                                </script>
                          
                            </div>
                                <div class="bottom-wrap" style="color:black;"><div class="VideoItem__meta clearfix" style="margin:0">
                        <div class="VideoItem__title"><?php echo $video['name']; ?></div>
                        <div class="VideoItem__duration"><?php echo gmdate('i:s', $video['duration']); ?></div>
                    </div></div>
                       </div>
                       </div>
                        <?php
         $cnt++;
                }
            
            ?>
                        
                        </div>
                            <div class="PageSlideshow__controls" style="margin-top: 20px;text-align: center;">
                                <a href="#" class="PageSlideshow__nav-btn PageSlideshow__prev"><i class="fa fa-chevron-left left-prev"></i></a>
                            <span class="PageSlideshow__pages">
                                
                            </span>
                                <a href="#" class="PageSlideshow__nav-btn PageSlideshow__next"><i class="fa fa-chevron-right right-next"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <script>
     $(document).ready(function () {
        //  alert('hai');
        if ($(window).innerWidth() >= 600) {
            
            $('#how-to2').remove();
            $('#how-to1').add();
        
        } else if ($(window).innerWidth() < 600) {
            
            $('#how-to1').remove();
            $('#how-to2').add(); 

            
        }
    // var $drillDown = $("#drilldown");
});
     </script>


