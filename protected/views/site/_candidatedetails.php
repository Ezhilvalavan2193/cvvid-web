<script>
$(document).ready(function() {
    $(".noSpace").keydown(function(event) {

     if (event.keyCode == 32) {

         event.preventDefault();

     }

  });

//     $('.noSpace').keyup(function() {
//  this.value = this.value.replace(/\s/g,'');
// });


});
</script>
<div class="form-container">
    <!-- Your details -->
    <h4>Your details</h4>
    <div class="row">
        <div class="col-sm-3">
            <div class="form-group">
                <?php echo $form->textField($model, 'forenames', array('class' => 'form-control noSpace', 'placeholder' => 'First Name')); ?>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <?php echo $form->textField($model, 'surname', array('class' => 'form-control noSpace', 'placeholder' => 'Surname')); ?>
            </div>
        </div>
        <div class="col-sm-4">
        <div class="form-group">
           <?php 
$dob = ((empty($model->dob) || $model->dob == null || $model->dob == "0000-00-00") ? "" : date("d/m/Y", strtotime($model->dob)));
$model->dob = $dob;
?>
<?php echo $form->textField($model, 'dob', array('class' => 'form-control dob', 'placeholder' => 'Date of Birth')); ?>

        </div>
    </div>
        <div class="col-sm-3">
            <div class="form-group">
                <?php echo $form->textField($model, 'email', array('class' => 'form-control noSpace', 'placeholder' => 'E-mail Address')); ?>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="form-group">
                <div class="form-group">
                    <?php echo $form->textField($model, 'current_job', array('class' => 'form-control', 'placeholder' => 'Current Job Title')); ?>
                </div>
            </div>
        </div>
           <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="form-group">
                                        <?php echo $form->textField($model, 'tel', array('class' => 'form-control', 'placeholder' => 'Contact Number')); ?>
                                    </div>
                                </div>
                            </div>
    </div>
</div><!-- .form-container -->
		<div class="form-container">
            <h4>Gender</h4>
            <div id="gender-fields">
                <?php $model->gender = 'M';
                    $accountStatus = array('M'=>'Male', 'F'=>'Female');
                    echo $form->radioButtonList($model, 'gender',$accountStatus,array('separator'=>' '));
                    
                    echo $form->checkBox($model, 'display_gender',array("checked"=>false,"style"=>"margin-left:20px"));
                    echo CHtml::label("Prefer not to mention gender", '',array());
                    ?>
            </div>
         </div>
<div class="form-container">
    <h4>Address</h4>
    <div id="location-fields">
        <?php $this->renderPartial('//addresses/address-form', array('form' => $form, 'addressmodel' => $addressmodel)); ?>
    </div>

</div>

<div class="form-container">
    <!-- Custom url -->
    <h4>Custom URL</h4>
    <div class="row">
        <div class="col-sm-12">
           <div class="form-inline slug-field">
                <div class="form-group">http://<?php echo $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF'])."/"?></div>
               <div class="form-group">
                   <?php echo $form->textField($profilemodel, 'slug', array('class' => 'form-control noSpace','placeholder'=>'forenames-surname','id'=>'slug')); ?>
               </div>
               <div class="form-group">
                   <span class="help-block"></span>
               </div>
           </div>
        </div>
    </div>
</div><!-- .form-container -->


<!-- Privacy -->
<div class="form-container">
    <!-- Account -->
    <h4>Account Settings</h4>
    <div class="row">
        <div class="col-sm-3 pass-field">
            <?php echo $model->password = ""; ?>
            <?php echo $form->passwordField($model, 'password', array('class' => 'form-control media-btn1', 'placeholder' => 'Password')); ?>
        </div>
        <div class="col-sm-3">
            <input class="form-control" placeholder="Confrim Password" name="Users[confirmpassword]" id="Users_confirmpassword" type="password" value="">
        </div>
    </div>
</div><!-- .form-container -->

<script src='https://www.google.com/recaptcha/api.js'></script>
<div class="g-recaptcha" data-sitekey="6Le5Vz4UAAAAAG8IXqMe7onsjTZxNXkLj70i72CG"></div>
<div class="form-container">
    <a class="btn main-btn" role="button" data-toggle="collapse" data-parent="#registerAccordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
        Next <i class="fa fa-chevron-right"></i>
    </a>
</div>


