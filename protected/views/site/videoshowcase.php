<?php
/* @var $this SiteController */
?>
<Script>
    $(document).ready(function () {

        $('.SuccessStories').on('click', '.SuccessStories__item', function (evt) {

            var videoid = $(this).attr('id');
            var videoname = $(this).find('.SuccessStories__item__title').text();
            $.ajax({
                url: '<?php echo $this->createUrl('site/loadVideo') ?>',
                type: 'POST',
                data: {videoid: videoid},
                success: function (data) {
                	$('#load-video-div').html("");
                	$('#VideoModal').addClass('in');
                    $('body').addClass('modal-open');
                    $('.video-title').html(videoname);
                    $('#load-video-div').html(data);
                          
                    $('#VideoModal').show();
                }
            });

        });

    });

</script>

<div id="page-content">
    <div class="SuccessStories__header" id="video1" style="background-image: <?php echo Yii::app()->theme->baseUrl . "/images/success-stories-header.jpg"; ?>">
        <div class="container">
            <div class="row">
                <div class="col-sm-7">
                    <h1>CV Video Showcase your profession</h1>
                    <h4>Positive proof that you can realise your career ambitions with a great video CV.</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="SuccessStories">
        <div class="container">
            <div class="row" id="video3">
            <ul class="listing">
            <?php
                foreach ($videomodel as $video) {

                    $cri = new CDbCriteria();
                    $cri->condition = "model_type like '%Video%' and model_id =" . $video['id'];
                    $mediamodel = Media::model()->find($cri);
                    ?>   

        <li>
                    <div class="col-sm-4 col-md-3">
                    <!-- <a href="https://www.youtube.com/watch?v=DcBAJQY0QVY" target="_blank"><img class="succes-video" src="<?php echo Yii::app()->baseUrl . "/images/feedback/cv-1.png"; ?>" ></a>
                        </div>
                        </li>
                        <li>
                        <div class="col-sm-4 col-md-3">
                        <a href="https://www.youtube.com/watch?v=Et3ZLktBebw" target="_blank"><img class="succes-video" src="<?php echo Yii::app()->baseUrl . "/images/feedback/cv-2.png"; ?>" ></a> -->
                     
                        <div class="SuccessStories__item" id="<?php 
                        echo $video['video_id'] ?>">
                            <a class="modalVideo" data-video="<?php 
                            echo $video['video_id'] ?>" data-name="<?php 
                            echo $video['name']; ?>"><div class="SuccessStories__item__image" style="background-image: url('<?php 
                            echo Yii::app()->baseUrl . '/images/media/' . $mediamodel->id . '/conversions/profile.jpg' ?>');"></div></a>
                            <div class="SuccessStories__item__meta clearfix">
                                <div class="SuccessStories__item__title"><?php 
                                echo $video['name']; ?> </div>
                                <div class="SuccessStories__item__duration">(<?php 
                                echo gmdate('i:s', $video['duration']); ?>)</div>
                            </div>
                        </div>
                    </div>
                    </li>
                <?php 
            }
             ?>
             </ul>
            </div>
            <div class="row" id="video2">
            <h2 class="text-center padd-font">CV Video Showcase</h2>
                        <div class="Slide__content">
                       
                            <div class="PageSlideshow">
                            
                                <div class="PageSlideshow__slides">
                                    
                                <?php
                foreach ($videomodel as $video) {

                    $cri = new CDbCriteria();
                    $cri->condition = "model_type like '%Video%' and model_id =" . $video['id'];
                    $mediamodel = Media::model()->find($cri);
                    ?>   
                    <div class="PageSlideshow__slide succes-slides">
                    <div class="col-sm-4 col-md-3">
                    <!-- <a href="https://www.youtube.com/watch?v=DcBAJQY0QVY" target="_blank"><img class="succes-video" src="<?php echo Yii::app()->baseUrl . "/images/feedback/cv-1.png"; ?>" ></a>
                          </div>
                          </div>
                          <div class="PageSlideshow__slide succes-slides">
                    <div class="col-sm-4 col-md-3">
                    <a href="https://www.youtube.com/watch?v=Et3ZLktBebw" target="_blank"><img class="succes-video" src="<?php echo Yii::app()->baseUrl . "/images/feedback/cv-2.png"; ?>" ></a> -->
                           
                        <div class="SuccessStories__item" id="<?php 
                        echo $video['video_id'] ?>">
                            <a class="modalVideo" data-video="<?php 
                            echo $video['video_id'] ?>" data-name="<?php 
                            echo $video['name']; ?>"><div class="SuccessStories__item__image" style="background-image: url('<?php 
                            echo Yii::app()->baseUrl . '/images/media/' . $mediamodel->id . '/conversions/profile.jpg' ?>');"></div></a>
                            <div class="SuccessStories__item__meta clearfix">
                                <div class="SuccessStories__item__title"><?php 
                                echo $video['name']; ?> </div>
                                <div class="SuccessStories__item__duration" style="color: black;">(<?php 
                                echo gmdate('i:s', $video['duration']); ?>)</div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php 
            } 
            ?>
                </div>
                <div class="PageSlideshow__controls" style="margin-top: 20px;text-align: center;">
                                    <a href="#" class="PageSlideshow__nav-btn PageSlideshow__prev left-prev"><i class="fa fa-chevron-left"></i></a>
                                    <span class="PageSlideshow__pages">
                                        <!-- <span class="PageSlideshow__current-page">1</span>/<span class="PageSlideshow__count">4</span> -->
                                    </span>
                                    <a href="#" class="PageSlideshow__nav-btn PageSlideshow__next right-next"><i class="fa fa-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>
            </div>
        </div>
    </div>
</div>


<div id="VideoModal" class="modal fade">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
    <a class="close" onclick="location.reload()">X</a>
    <h4 class="video-title"></h4>
</div>

<div class="modal-body clearfix">
    <div class="col-sm-12">
        <div class="videodiv" id="load-video-div">
        <iframe src="" class="iframe-video" width="100%" height="500px" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
        </div>
    </div>
</div>

<div class="modal-footer">
    <button onclick="location.reload()" class="btn btn-default" type="button">Close</button>
    </div>

</div>
</div>
</div>

