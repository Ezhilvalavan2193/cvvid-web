<?php
/* @var $this SiteController */
?>
<script type="text/javascript">
$(document).ready(function(){
    if ($(window).innerWidth() >= 600) {
                // alert('hai');
                $('#top-tipsid1').add();
            $('#top-tipsid2').remove();
           
            } else if ($(window).innerWidth() < 600) {
                // alert('hai');
            $('#top-tipsid2').add();
          
       
            $('#top-tipsid1').remove();
  }
   
    //    if($('a').href.indexOf('cvvid.com') != -1)
    // {
    //     var link = "<?php echo Yii::app()->baseUrl.'/site/how-to'; ?>";
    //     $('a.yellow').attr('href',link);
    // }

   
    
});
</script>
<div id="page-content">
    <section id="top-tipsid1">
<?php 
  if($model->background_id > 0)
  {
      $mediamodel = Media::model()->findByPk($model->background_id);
      if($mediamodel != null && $mediamodel->file_name != null)
      {
          $src = Yii::app()->baseUrl.'/images/media/'.$mediamodel->id.'/'.$mediamodel->file_name; ?>
    <div class="fullscreen" style="background-image: url('<?php echo $src; ?>'); background-position: <?php echo $model->background_x; ?> <?php echo $model->background_x; ?>;">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">

                    <div class="Slide__content">
                        <div class="PageSlideshow">
                            <div class="PageSlideshow__slides">
                                <?php
                                foreach ($slidemodel as $slide) {
                                    ?>
                                    <div class="PageSlideshow__slide">
                                        <?php echo $slide['body']; ?>
                                    </div><!-- .PageSlideshow__slide -->
                                <?php } ?>

                            </div>
                            <div class="PageSlideshow__controls" style="margin-top: 20px;">
                                <a href="#" class="PageSlideshow__nav-btn PageSlideshow__prev"><i class="fa fa-chevron-left"></i></a>
                            <span class="PageSlideshow__pages">
                                <span class="PageSlideshow__current-page">1</span>/<span class="PageSlideshow">7</span>
                            </span>
                                <a href="#" class="PageSlideshow__nav-btn PageSlideshow__next"><i class="fa fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
  <?php } } else { ?>
    
    <div class="Slideshow FullscreenSlideshow" data-autoplay="true" data-autoplay-speed="4000" data-dots="false" data-arrows="false">
        <?php
        if (isset($slidemodel)){
        $cnt = 0;
        foreach ($slidemodel as $slide) { 
           $mediamodel = Media::model()->findByPk($slide['media_id']);  
        ?>
        <div class="Slide" style="background-image: url('<?php echo Yii::app()->baseUrl . "/images/media/" . $mediamodel->id . "/" . $mediamodel->file_name; ?>'); background-position: <?php echo $slide['background_x']; ?> <?php echo $slide['background_y']; ?>;">
            <div class="container">
                <div class="Slide__inner">
                    <div class="Slide__caption">
                       <?php echo $slide['body']; ?>
                        <a href="<?php echo Yii::app()->createUrl('site/about'); ?>" class="btn btn-primary">Learn More</a>
                        <a href="#" class="view-showcase btn btn-primary">View CV Video Showcase</a>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php } ?>
    </div>
      <?php }  ?>
</section>
<?php
    $model = VideoCategories::model()->findByAttributes(array('slug' => 'top-tips'));
    $videomodel = Videos::model()->findAllByAttributes(array('category_id' => $model->id, 'deleted_at' => null));
    ?>
<section id="top-tipsid2">
     <div class="col-xs-12">
                <div class="Slide__content" style="padding-top: 10px;">
                <h2 class="text-center padd-font">Top Tips</h2>
                        <div class="PageSlideshow text-center">
                            <div class="PageSlideshow__slides">
                            <?php
                                      $cnt = 53;
                                      foreach ($videomodel as $video) {
                                      $cri = new CDbCriteria();
                                      $cri->condition = "model_type like '%Video%' and model_id =" . $video['id'];
                                      $mediamodel = Media::model()->find($cri);
                                      
                                      ?>
                                      <?php 
                                          $bucket = "cvvid-new";
                                          
                                         $videomodel = Videos::model()->findByPk($video['video_id']);
                                          $url = 'https://' . Yii::app()->params['AWS_BUCKET'] . '.s3.amazonaws.com/top-tips/'.$video['video_id'];
                                      ?>
                                      
                                     <div class="PageSlideshow__slide">
              
                                          <div id="my-video-<?php echo $cnt; ?>" class="">
                                              <script type="text/javascript">
                                              jwplayer("my-video-<?php echo $cnt; ?>").setup({
                                               'file': '<?php echo $url; ?>',
                                               'image': '<?php echo $mediamodel != null ? Yii::app()->baseUrl."/images/media/".$mediamodel->id."/".$mediamodel->file_name : ""?>',		 
                                              });
                                                 
                                              </script>
                                        
                                             
                                     </div>
                                     </div>
                                      <?php
                       $cnt++;
                              }
                          
                          ?>
<!--                                 
                                     <div class="PageSlideshow__slide">
                                     <video width="320" height="240" style="width:100%; height: 260px;" controls>
                                     <source src="<?php echo Yii::app()->baseUrl . "/images/videos/how-to-1.mp4"; ?>" type="video/mp4">
                                     </video> -->
                                     <!-- <iframe src='<?php echo Yii::app()->baseUrl . "/images/videos/why-cvvid.mp4"; ?>' class="iframe-video" width="100%" height="200px" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe> -->
                                     <!-- </div>
                                     <div class="PageSlideshow__slide">
                                     <video width="320" height="240" style="width:100%;     height: 260px;" controls>
                                     <source src="<?php echo Yii::app()->baseUrl . "/images/videos/how-to-2.mp4"; ?>" type="video/mp4">
                                     </video>
                                     </div>
                                     <div class="PageSlideshow__slide">
                                     <video width="320" height="240" style="width:100%;     height: 260px;" controls>
                                     <source src="<?php echo Yii::app()->baseUrl . "/images/videos/how-to-3.mp4"; ?>" type="video/mp4">
                                     </video>
                                     </div> -->
                               

                            </div>
                            <div class="PageSlideshow__controls" >
                                <a href="#" class="PageSlideshow__nav-btn PageSlideshow__prev left-prev"><i class="fa fa-chevron-left"></i></a>
                            <span class="PageSlideshow__pages">
                                <!-- <span class="PageSlideshow__current-page">1</span>/<span class="PageSlideshow__count">4</span> -->
                            </span>
                                <a href="#" class="PageSlideshow__nav-btn PageSlideshow__next right-next"><i class="fa fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
            </div>
</section>


</div>


<script>
     $(document).ready(function () {
        
});
     </script>