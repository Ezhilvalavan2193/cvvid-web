<?php
/* @var $this SiteController */
?>

<div id="page-content">
    <div class="HowTo__header" style="background-image: <?php echo Yii::app()->theme->baseUrl."/images/success-stories-header.jpg"; ?>">
        <div class="container">
            <div class="row">
                <div class="col-sm-7">
                    <h1>How To - The Path To Success</h1>
                    <h4>You never get a second chance to make a first impression.</h4>
                </div>
            </div>
        </div>
    </div>
    <div class="HowTo">
        <div class="container">
             <?php
        $cnt = 0;
        foreach ($videomodel as $video) {
            $cnt++;
            if ($cnt == 1) {
                ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="VideoItem__video">
                        <iframe src="//player.vimeo.com/video/<?php echo $video['video_id']?>" width="100%" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
                    </div>
                </div>
            </div>
             <?php
            } else {

                if ($cnt == 2) {
                    ?> 
            <h4>The 5 Golden Rules</h4>
            <div class="HowTo__other-videos">
                <?php
                    }
                    $cri = new CDbCriteria();
                    $cri->condition = "model_type like '%Video%' and model_id =" . $video['id'];
                    $mediamodel = Media::model()->find($cri);
                    ?>
                <div class="VideoItem">
                    <div class="ModalVideo" data-video="<?php echo $video['id']; ?>" data-name="<?php echo $video['name']; ?>">
                        <div class="VideoItem__image">
                            <img src="<?php echo Yii::app()->baseUrl . '/images/media/' . $mediamodel->id . '/conversions/profile.jpg' ?>" class="img-responsive">
                        </div>
                    </div>
                    <div class="VideoItem__meta clearfix">
                        <div class="VideoItem__title"><?php echo $video['name']; ?></div>
                        <div class="VideoItem__duration"><?php echo gmdate('i:s', $video['duration']); ?></div>
                    </div>
                </div>
                <?php
                    if (count($videomodel) == ($cnt - 1))
                        echo "</div>";
                }
            }
            ?>
            </div><!-- .SuccessStory__other-videos -->
        </div>
    </div>
</div>
