<?php
/* @var $this SiteController /
  / @var $model LoginForm /
  / @var $form CActiveForm */
?>

<div class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Change Password</h1>
            </div>
        </div>
    </div>
</div>
<div id="page-content">
    <div class="container">

        <div class="login-container">
            <?php if (Yii::app()->user->hasFlash('success')): ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php echo Yii::app()->user->getFlash('success'); ?>
                </div>
            <?php endif; ?>
             <?php if (Yii::app()->user->hasFlash('error')): ?>
                <div class="alert alert-danger alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php echo Yii::app()->user->getFlash('error'); ?>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-sm-6">
                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'login-form',
                        'enableClientValidation' => true,
                        'action'=> $this->createUrl('site/savePassword',array('id'=>$model->id)),
                        'clientOptions' => array(
                        'validateOnSubmit' => true,
                        ),
                    ));
                    ?>
                    <?php echo $form->errorSummary(array($model), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span></button>', '', array('class' => 'alert alert-danger')); ?>
                    <h5>Reset Password</h5>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <?php $model->password = ""; ?>
                                    <?php echo $form->passwordField($model, 'password', array('class' => 'form-control', 'id' => 'password', 'placeholder' => 'New Password','required'=>'required')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                    <?php echo $form->passwordField($model, 'confirmpassword', array('class' => 'form-control', 'id' => 'confirmpassword', 'placeholder' => 'Confirm Password','required'=>'required')); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <?php echo CHtml::submitButton('Save Password', array('class' => 'main-btn btn-block')); ?>
                    </div>

                    <div class="login-links">
                        <a href="<?php echo Yii::app()->createUrl('site/login') ?>">Back to Login</a>
                    </div>

                    <?php $this->endWidget(); ?>
                </div>
                <div class="col-sm-6">

                </div>
            </div>
        </div><!-- .login-container -->
    </div><!-- .container -->
</div>