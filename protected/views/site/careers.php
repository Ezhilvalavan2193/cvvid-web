<?php
/* @var $this SiteController */
?>
<!-- <script type="text/javascript">
$(document).ready(function(){
   
        var link = "<?php echo Yii::app()->baseUrl.'/site/top-tips'; ?>";
        $('a.btn').attr('href',link);
    
});
</script> -->
<div id="page-content">
    <!-- <section id="care-us1">

<p>test</p>
        </section> -->
        <?php
    $model = VideoCategories::model()->findByAttributes(array('slug' => 'why-cvvid'));
    $videomodel = Videos::model()->findAllByAttributes(array('category_id' => $model->id, 'deleted_at' => null));
    ?>
        <section id="care-us2">
            <div style="padding: 20px;text-align: center;">
                <h2 style="color:black;text-align:center;">CVVID Career Guide</h2>
                <div class="Slide__content">
                        <div class="PageSlideshow">
                            <div class="PageSlideshow__slides slides-show why-cvvid-slide">
                        <?php
                        $cnt = 42;
                        foreach ($videomodel as $video) {
                        $cri = new CDbCriteria();
                        $cri->condition = "model_type like '%Video%' and model_id =" . $video['id'];
                        $mediamodel = Media::model()->find($cri);
                        
                        ?>
                        <?php 
                            $bucket = "cvvid-new";
                            
                           $videomodel = Videos::model()->findByPk($video['video_id']);
                            $url = 'https://' . Yii::app()->params['AWS_BUCKET'] . '.s3.amazonaws.com/why-cvvid/'.$video['video_id'];
                        ?>
                        <div class="col-xs-3">
                       <div class="PageSlideshow__slide why-cvvid">

                            <div id="my-video-<?php echo $cnt; ?>" class="">
                                <script type="text/javascript">
                                jwplayer("my-video-<?php echo $cnt; ?>").setup({
                                 'file': '<?php echo $url; ?>',
                                 'image': '<?php echo $mediamodel != null ? Yii::app()->baseUrl."/images/media/".$mediamodel->id."/".$mediamodel->file_name : ""?>',		 
                                });
                               	
                                </script>
                          
                            </div>
                                <div class="bottom-wrap"><?php echo $video['name']; ?> (<?php echo gmdate('i:s', $video['duration']); ?>)</div>
                       </div>
                       </div>
                        <?php
         $cnt++;
                }
            
            ?>
                        
                        </div>
                            <!-- <div class="PageSlideshow__controls" style="margin-top: 20px;text-align: center;">
                                <a href="#" class="PageSlideshow__nav-btn PageSlideshow__prev"><i class="fa fa-chevron-left left-prev"></i></a>
                            <span class="PageSlideshow__pages">
                                
                            </span>
                                <a href="#" class="PageSlideshow__nav-btn PageSlideshow__next"><i class="fa fa-chevron-right right-next"></i></a>
                            </div> -->
                        </div>
                    </div>
</div>
<div class="col-sm-12" style="padding:0px;    margin-top: 184px;">
    <div>
<div id="rcorners1">

                    <div id="rcorners2">
                    </div>
                    <div id="rcorners">
                    </div>
                    </div>
</div>
<div>
<p class="quest">Questions</p>
                   <p class="drop-quest dropes1"> <i class="fa fa-caret-down"></i></p>
</div>
<div class="padd-li">
<p>Here are some examples of the sort of questions you might ask yourself. Make sure your questions give you the chance to show the real you.</p>
<ul>
    <li>Tell me something suprising about yourself.</li>
    <li>What are your strong points?</li>
    <li>What about a failure - how did you learn from it and what did you learn?</li>
    <li>What do you see yourself doing in 5 years of time?</li>
    <li>What motivates you?</li>
    <li>What are the things you most want to learn?</li>
    <li>Talk about a big event, achievement or experience- what did you learn about yourself?</li>
    <li>Talk about something that makes you smile.</li>
    <li>Talk about something that lets us see you at your best.</li>
  </ul>
  </div>

                   
               
            </div>
            <div class="col-sm-12" style="padding:0px;">
    <div>
<div id="rcorners1">

                    <div id="rcorners2">
                    </div>
                    <div id="rcorners">
                    </div>
                    </div>
</div>
<div>
<p class="quest">Finally: Prepare to be your best</p>
                   <p class="drop-quest dropes2"> <i class="fa fa-caret-down"></i></p>
</div>
<div class="content-blk">
<div class="step-by-step">
<div class="title-blk">
    <span></span>
    <p>When do you feel at your most confident? Imagine yourself there.</p>
</div>
<div class="step-to" style="display: block;">
    <p></p>
    <br>
    <br>
</div>
</div>     
<div class="step-by-step">
<div class="title-blk">
    <span></span>
    <p>who makes you feel most like you? Imagine you're with them. Better still, ask them to help you.</p>
</div>
<div class="step-to" style="display: block;">
  <p></p>  
  <br>
  <br>
</div>
</div> 
<div class="step-by-step">
<div class="title-blk">
    <span></span>
    <p>In fact it's always a good idea to get someone you trust to help you.</p>
</div>
<div class="step-to" style="display: block;">
<p></p>  
  <br>
  <br>
</div>
</div>  
<div class="step-by-step">
<div class="title-blk">
    <span></span>
    <p>Treat this like a project. Think it through. Walk around it. Build it up. Get some feedback from others.</p>
</div>
<div class="step-to" style="display: block;">
<p></p>  
  <br>
  <br>
</div>
</div>  
<div class="step-by-step">
<div class="title-blk">
    <span></span>
    <p>Take a look at some of your Best Practice examples here.</p>
</div>
<div class="step-to" style="display: block;">
<p></p>  
  <br>
  <br>
</div>
</div>  
<div class="step-by-step">
<div class="title-blk">
    <span></span>
    <p>But always remember, You don't have to be like anyone else, and you shouldn't be. That's the whole point: you just have to be you. You at your best.</p>
</div>
<div class="step-to" style="display: block;">
<p></p>  
  <br>
  <br>
</div>
</div>      
</div>
               
            </div>
                                </section>
</div>
<script>
$(document).ready(function() {

$(".dropes1").click(function (){
    $(".padd-li").toggle();
    // $(".content-blk").toggle();
});
$(".dropes2").click(function (){
    // $(".padd-li").toggle();
    $(".content-blk").toggle();
});
});
</script>

<!-- <script>
     $(document).ready(function () {
        //  window.location.reload();
        // $( window ).resize(function() {
            // window.location.reload();
            if ($(window).innerWidth() >= 600) {
                $('#care-us1').add();
                // window.location.reload();
            $('#care-us2').remove();
            
           
            
        
        } else if ($(window).innerWidth() < 600) {
            //
            $('#care-us2').add();
            $('#care-us1').remove();
  
            
            
     
            
        }
// });
        //  alert('hai');
      
    // var $drillDown = $("#drilldown");
});
     </script> -->
     <style>
     .padd-li{
        padding: 20px;
     }
    #rcorners1 {
        border-radius: 0px 0px 0px 50px;
        background: #080808;
        padding: 20px;
        width: 100%;
        height: 100px;
}
#rcorners2 {
    margin-left: -19px;
    margin-top: -20px;
    /* border-radius: 35px 0px 0px 35px; */
    /* background: #73AD21; */
    /* padding: 20px; */
    /* width: 200px; */
    /* height: 150px; */
    height: 44px;
    width: 91px;
    border-radius: 0 0 0px 90px;
    background: #ffffff;  
}
#rcorners{
    width: 100%;
    background: white;
    height: 45px;
    margin-left: 71px;
    margin-top: -45px;
}
.quest {
    margin-top: -43px;
    margin-bottom: 32px;
    margin-left: 37px;
    color: #fbd943;
    font-size: 20px;
}

.drop-quest {
    margin-top: -70px;
    float: right;
    padding: 0px 20px;
    font-size: 35px;
    color: white;
}

ul > li {
    list-style-type:decimal;
}
.step-by-step{
    padding-left:100px;
    padding-right:50px;
    position: relative;
    z-index: 3;
    /*// padding-bottom:30px;*/
}
.step-by-step:last-child{
    padding-bottom: 0
}
.step-by-step:before{
    content: "";
    position: absolute;
    background-color: black;
    height: 100%;
    top: 0;
    left: 30px;
    width: 4px;
    z-index: -1;
}
.step-by-step:last-child:before{
    background-color:white;
}
/*.title-blk{
    padding-bottom: 30px;
}*/
.title-blk h3{  color:#004834; padding-bottom: 0; line-height:50px; display:inline-block;font-weight: 600}
/*.title-blk h3{ cursor: pointer; color:#ff0000; padding-bottom: 0; line-height:50px; display:inline-block;} */
.title-blk span{
    background-color: #fbd943;
    -webit-border-radius: 100%;
    -moz-border-radius: 100%;
    border-radius: 100%;
    float: left;
    color: #fff;
    font-size: 18px;
    font-weight: 700;
    text-align: center;
    line-height: 50px;
    margin-left: -80px;
    height: 25px;
    width: 25px;
}
.title-blk span, .step-by-step:before{
    -webkit-transition: all 0.3s linear;
    -moz-transition: all 0.3s linear;
    transition: all 0.3s linear;
}
.step-to .form-row{
    max-width: 100%;
}
.step-to{ display: none; background-color:#fff;}
.step-to .form-row .btn:first-child{ margin-right:20px;}
input[type=submit][disabled=disabled]{ opacity:0.5 }
.step-by-step.active .title-blk span{ background-color:#f7941e }
.step-by-step.active:before{
    background-color:#f7941e;
}
.step-to .radio-btn{ float: none }	
.step-by-step.active .title-blk h3{color:#f7941e}
.step-by-step h3{
    border: none;
    overflow: hidden !important;
    text-overflow: ellipsis;
    text-transform: uppercase;
}
table {
    border-collapse: collapse;
    border-spacing: 0;
}
.backtotop {
    padding: 10px 20px 0 0;
    text-align: right;
    overflow: hidden;
}
.secondary_link {
    color: #004834;
}
.content-blk td:first-child {
    font-weight: 600;
    width: 20%;
    padding-left: 0;
}

.step-to table td {
    border-bottom: 0px;
}
.content-blk td {
    border-bottom: 1px solid #e9e9e9;
    font-size: 17px;
    padding: 9px;
    line-height: 1.8157894737;
}
.abt-hr{
    font-weight: 400!important;
}
.profile-basic-info .profile-name {
    color: #01030f;
}
.content-blk {
    background-color: #fff;
    -webkit-box-shadow: 2px 2px 2px 0 rgba(0, 0, 0, 0.1);
    -moz-box-shadow: 2px 2px 2px 0 rgba(0, 0, 0, 0.1);
    box-shadow: 2px 2px 2px 0 rgba(0, 0, 0, 0.1);
    position: relative;
}
.content-blk {
    padding: 30px;
}
.content-blk {
    margin-bottom: 20px;
}
.full-profile .photo-section {
    display: inline-block;
    vertical-align: top;
    width: 29%;
    height: auto;
    border: solid 1px #d9d9d9;
}
.full-profile .profile-basic-info {
    padding: 15px;
    vertical-align: top;
    width: 69%;
    display: inline-block;
    padding-left: 15px;
    background: #fff;
    box-shadow: 1px 2px 2px rgba(43,59,93,0.29);
    border: solid 1px #dfe0e39;
    border-radius: 3px;
    border: 1px solid #d6dae0;
}
    </style>