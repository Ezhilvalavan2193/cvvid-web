<?php
/* @var $this SiteController */
/* @var $model ForgotPassword */
/* @var $form CActiveForm  */

?>

<div class="page-title">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1> Reset Password</h1>
            </div>
        </div>
    </div>
</div>
<div id="page-content">
    <div class="container">
           <?php if(Yii::app()->user->hasFlash('success')):?>
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
         <?php endif; ?>
          <?php if(Yii::app()->user->hasFlash('error')):?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo Yii::app()->user->getFlash('error'); ?>
        </div>
         <?php endif; ?>
        <div class="login-container">
            <div class="row">
                <div class="col-sm-6">
                   <?php $form=$this->beginWidget('CActiveForm', array(
                            'id'=>'forgot-password-form',
                            'action'=>$this->createUrl('site/password'),
                            'enableClientValidation'=>true,
                            'clientOptions'=>array(
                            'validateOnSubmit'=>true,
                            ),
                    )); ?>
	            <?php echo  $form->errorSummary(array($model), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
                    <h5>Forgot my password</h5>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <?php echo $form->emailField($model,'email',array('class'=>'form-control','id' => 'email','placeholder'=>'E-mail Address','required'=>true)); ?>
                            </div>
                        </div>
                    </div>

                    <div class="form-actions">
                         <?php echo CHtml::submitButton('Send Reset Password',array('class'=>'main-btn')); ?>
                    </div>

                    <div class="login-links">
                        <a href="<?php echo Yii::app()->createUrl('site/login') ?>">Back to Login</a>
                    </div>

                      <?php $this->endWidget(); ?>
                </div>
                <div class="col-sm-6">

                </div>
            </div>
        </div><!-- .login-container -->
    </div><!-- .container -->
</div>