<script>
$(document).ready(function() {
    $(".noSpace").keydown(function(event) {

     if (event.keyCode == 32) {

         event.preventDefault();

     }

  });

//     $('.noSpace').keyup(function() {
//  this.value = this.value.replace(/\s/g,'');
// });


});
</script>
<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <?php echo $form->textField($model, 'name', array('class' => 'form-control', 'placeholder' => 'Company Name')); ?>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <?php echo $form->textField($model, 'website', array('class' => 'form-control', 'placeholder' => 'www.example.com')); ?>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <?php echo $form->textField($model, 'tel', array('class' => 'form-control', 'placeholder' => 'Phone Number')); ?>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <select name="industry_id" class="form-control select2">
<!--                 <option selected="selected" disabled>Search Industry</option> -->
                <?php
                $industrymodel = Industries::model()->findAll();
                foreach ($industrymodel as $ind) {
                    ?>                  
                    <option value="<?php echo $ind['id'] ?>"> <?php echo $ind['name'] ?> </option>
                <?php } ?>
            </select>
        </div>
    </div>
</div>

<div class="form-container">
    <h4>Address</h4>
    <div class="clearfix" style="text-align: right">
        <button id="enter_manually" style='display:none' class="main-btn" type="button">Enter Manually</button>
    </div>
    <div id="location-fields">
        <?php $this->renderPartial('//addresses/address-form', array('form' => $form, 'addressmodel' => $addressmodel)); ?>
    </div>

</div>

<div class="form-container">
    <!-- Custom url -->
    <h4>Custom URL</h4>
    <div class="row">
        <div class="col-sm-12">
           <div class="form-inline slug-field">
                <div class="form-group">http://<?php echo $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF'])."/"?></div>
               <div class="form-group">
                   <?php echo $form->textField($profilemodel, 'slug', array('class' => 'form-control noSpace','placeholder'=>'company-name','id'=>'slug')); ?>
               </div>
               <div class="form-group">
                   <span class="help-block"></span>
               </div>
           </div>
        </div>
    </div>
</div><!-- .form-container -->

<!-- Account -->
<h4>Your Account</h4>
<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <?php echo $form->textField($usermodel, 'forenames', array('class' => 'form-control noSpace', 'placeholder' => 'First Name')); ?>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <?php echo $form->textField($usermodel, 'surname', array('class' => 'form-control noSpace', 'placeholder' => 'Surname')); ?>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <?php echo $form->textField($usermodel, 'current_job', array('class' => 'form-control', 'placeholder' => 'Job Title')); ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-3">
        <div class="form-group">
            <?php echo $form->textField($usermodel, 'email', array('class' => 'form-control noSpace', 'placeholder' => 'Email Address')); ?>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <?php echo $usermodel->password = ""; ?>
            <?php echo $form->passwordField($usermodel, 'password', array('class' => 'form-control noSpace', 'placeholder' => 'Password')); ?>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="form-group">
            <input class="form-control noSpace" placeholder="Confirm Password" name="Users[confirmpassword]" id="Users_confirmpassword" type="password" value="">
        </div>
    </div>
</div>

<script src="https://www.google.com/recaptcha/api.js" async="" defer=""></script>
<div class="g-recaptcha" data-sitekey="6Le5Vz4UAAAAAG8IXqMe7onsjTZxNXkLj70i72CG"></div>

<div class="form-container">
    <a class="btn main-btn" role="button" data-toggle="collapse" data-parent="#registerAccordion" href="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
        Next <i class="fa fa-chevron-right"></i>
    </a>
</div>