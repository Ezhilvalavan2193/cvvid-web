<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Candidates'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#users-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});

$('#export-button').on('click',function() {
    $.fn.yiiGridView.export();
});

$.fn.yiiGridView.export = function() {
    $.fn.yiiGridView.update('alumini-grid',{ 
    	success: function() {
    		$('#alumini-grid').removeClass('grid-view-loading');
    		window.location = '". $this->createUrl('exportFile')  . "';
    	},
    	data: $('.form-inline form').serialize() + '&export=true'
    });
}

");
?>
<script>
$(document).ready(function(){
	
});
</script>

<h2 style="text-align: center;">Digital Passport Report - <?php echo date("Y"); ?></h2>
<?php 
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'passport-grid',
   //  'itemsCssClass' => 'table',
//     'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',
	'summaryText'=>'',
    'dataProvider'=>$model->searchPassport(),
    'columns'=>array(
        
        array(
            'header'=>'Forenames',
            'value'=>'$data->forenames',
        ),
        array(
            'header'=>'Surname',
            'value'=>'$data->surname',
        ),
        array(
            'header'=>'Email',
            'value'=>'$data->email',
        ),
    ),
)); ?>


