<?php
/* @var $this SiteController 
/* @var $model Banner */
/* @var $form CActiveForm */

$this->pageTitle=Yii::app()->name;
Yii::app()->clientScript->registerScript('gallery', "

	$('.delete').click(function(){
	var gallery_id = $(this).parent('div').find('a.fancybox-buttons').attr('id');
		 $.ajax
		    ({ 
		        type: 'POST',
		        url: '".$this->createUrl("site/deleteGallery")."',
		        data: {id: gallery_id},
		        success: function(result)
		        {
		          location.reload();
		        },
		        error: function(result)
		        {
					alert('error !!!');
		        }
		    });
	});
	
	$('#gallery-form').on('submit', function(e) {
		e.preventDefault();
		var data = new FormData($(this)[0]);
		 $.ajax
		    ({ 
		        url: '".$this->createUrl("site/GalleryUpload")."',
		        data: data,
			    cache: false,
			    contentType: false,
			    processData: false,
			    type: 'POST',
		        success: function(result)
		        {
		          location.reload();
		        },
		        error: function(result)
		        {
					alert('error !!!');
		        }
		    });
		
	});
	
");
$this->pagename = "Gallery";
?>
<head>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/main.css" media="screen, projection" />
	<!-- Add mousewheel plugin (this is optional) -->
	<script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/fancybox/lib/jquery.mousewheel-3.0.6.pack.js'?>"></script>

	<!-- Add fancyBox main JS and CSS files -->
	<script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/fancybox/source/jquery.fancybox.js?v=2.1.5'?>"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl.'/fancybox/source/jquery.fancybox.css?v=2.1.5'?>" media="screen" />

	<!-- Add Button helper (this is optional) -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl.'/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5'?>" />
	<script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5'?>"></script>

	<!-- Add Thumbnail helper (this is optional) -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl.'/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7'?>" />
	<script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7'?>"></script>

	<!-- Add Media helper (this is optional) -->
	<script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6'?>"></script>

	<script type="text/javascript">
		$(document).ready(function() {
			$('.fancybox').fancybox();

			$('.fancybox-buttons').fancybox({
				openEffect  : 'none',
				closeEffect : 'none',

				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,

				helpers : {
					title : {
						type : 'inside'
					},
					buttons	: {}
				},

				afterLoad : function() {
					this.title = 'Image ' + (this.index + 1) + ' of ' + this.group.length + (this.title ? ' - ' + this.title : '');
				}
			});

		});
	</script>
<style type="text/css">
	div.show-image {
	    position: relative;
	    float:left;
	    margin:5px;
	}
	div.show-image:hover{
	}
	div.show-image:hover a.delete img{
	    display: block;
	}
	div.show-image a.delete img {
	    position:absolute;
	    display:none;
	}
	
	div.show-image a.delete img{
	    top:0;
	    left:85%;
	}
		.fancybox-custom .fancybox-skin {
			box-shadow: 0 0 50px #222;
		}

		body {
			margin: 0 auto;
		}
	</style>
</head>
<body>
	<div id="">
		
		<!-- <section id="hero-banner">
			<img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/hero-banner-2.jpg" alt="">
			<div class="banner-title">
				<h1>Gallery</h1>
			</div>
		</section>-->

	<section id="content-section">
			<div class="content-row">
				<div class="pull-top">
					<ul class="member-list list-col">
						
					
<div class="form" style="display: <?php echo (Yii::app()->user->getState('userid') || Yii::app()->user->getState('association_id')||Yii::app()->user->getState('team_id') ? "" : "none") ?>">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'gallery-form',
	'enableAjaxValidation'=>false,
	//'action'=>'upload',
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>
    <div class="middle-wrap col-xs-8">
    <div class="formBlk">
	<div class="formRow">
            <?php echo CHtml::label('Upload Gallery Photos','',array());?><br><br>
<?php
  $this->widget('CMultiFileUpload', array(
     'name'=>'photos',
     'accept'=>'jpg|gif|png',
     'options'=>array('class'=>'textBox'
        // 'onFileSelect'=>'function(e, v, m){ alert("onFileSelect - "+v) }',
        // 'afterFileSelect'=>'function(e, v, m){ alert("afterFileSelect - "+v) }',
        // 'onFileAppend'=>'function(e, v, m){ alert("onFileAppend - "+v) }',
        // 'afterFileAppend'=>'function(e, v, m){ alert("afterFileAppend - "+v) }',
        // 'onFileRemove'=>'function(e, v, m){ alert("onFileRemove - "+v) }',
        // 'afterFileRemove'=>'function(e, v, m){ alert("afterFileRemove - "+v) }',
     ),
     'denied'=>'File is not allowed',
     //'max'=>3, // max 10 files
 
 
  ));
  ?><br><br>
        </div></div>
 <?php echo  CHtml::submitButton('Upload',array('class'=>'btn btn-primary','id'=>'photo_upload'));?>

<?php $this->endWidget(); ?>
</div></div>


		
<?php 
if(Yii::app()->user->getState('team_id'))
	$gmodel = Gallery::model()->findAllByAttributes(array('team_id'=>Yii::app()->user->getState('team_id')));
else 
	$gmodel = Gallery::model()->findAll();

//$dir = new DirectoryIterator("images/gallery");
foreach ($gmodel as $gal) {
  //  if (!$fileinfo->isDot()) { ?>
    <li>
	<a href="#">
            <img src="<?php echo Yii::app()->baseUrl."/images/gallery/".$gal['name'];?>" alt="">
	</a>
    </li>                                   	
   <?php // }
}?>


					</ul>
				</div>
			</div>
		</section>
	
    </div>
</body>