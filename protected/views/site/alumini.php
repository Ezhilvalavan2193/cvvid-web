<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Candidates'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#users-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});

$('#export-button').on('click',function() {
    $.fn.yiiGridView.export();
});

$.fn.yiiGridView.export = function() {
    $.fn.yiiGridView.update('alumini-grid',{ 
    	success: function() {
    		$('#alumini-grid').removeClass('grid-view-loading');
    		window.location = '". $this->createUrl('exportFile')  . "';
    	},
    	data: $('.form-inline form').serialize() + '&export=true'
    });
}

");
?>
<script>
$(document).ready(function(){
	$('#export-alumini').click(function()
	{
		var value =  window.location.href.substring(window.location.href.lastIndexOf('?') + 1);
		window.location = '<?php echo $this->createUrl("site/exportAlumini") ?>'+"?"+value;
	});
});
</script>
<h1><?php echo "Alumni"; ?></h1>
<?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
<div class="Admin__content__inner">
    <div class="AdminFilters">
        <div class="form-inline">
          <?php $this->renderPartial('_search',array(
                'model'=>$model
            )); ?>
            
        </div>
    </div>
    <div class="pull-right clearfix">
      <?php echo CHtml::button('Export', array('id'=>'export-alumini','class'=>'btn btn-primary')); ?>
      </div>
  <?php //echo CHtml::button('Export', array('id'=>'export-button','class'=>'btn btn-primary')); ?>
<?php 
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'alumini-grid',
     'itemsCssClass' => 'table',
	'summaryText'=>'',
	//'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',    
    'enableSorting'=>true,
    'ajaxUpdate'=>true,
    
    'pager' => array(
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'header'=>'',
        'firstPageLabel' => '<<',
        'lastPageLabel' => '>>',
        'nextPageLabel' => '>>',
        'prevPageLabel' => '<<'),
			'ajaxUpdate'=>false,
    'dataProvider'=>$model->searchAlumini(),
	'columns'=>array(
			//'id',
//     	    array(
//     	       // 'name' => 'check',
//     	        'id' => 'selectedIds',
//     	        'value' => '$data->id',
//     	        'class' => 'CCheckBoxColumn',
//     	        'selectableRows'  => 2,
//     	       // 'checked'=>'Yii::app()->user->getState($data->id)',
//     	       // 'checkBoxHtmlOptions' => array('name' => 'idList[]'),
    	        
//     	    ),
// 			array(
// 					'header'=>'Name',
// 					'value'=>'CHtml::link($data->forenames." ".$data->surname,Yii::app()->createUrl("users/tabedit", array("id"=>$data->id)),array("class"=>"profile-edit-button","id"=>"profile-edit"))',
// 			        'type'=>'raw'
// 			),
	       array(
					'header'=>'Age',
					'value'=>'strlen($data->dob) > 2 ? "" : $data->dob',	    			       
	    		),
    	    array(
    	        'header'=>'Gender',
    	        'value'=>'$data->gender == "F" ? "Female" : "Male"',
    	    ),
    	    array(
    	        'header'=>'Grade',
    	        'value'=>'$data->loggeddatetime',
    	    ),
			array(
					'header'=>'Profile',
					'value'=>'CHtml::link("View",$data->getSlugURL(),array("class"=>"profile-view-button","id"=>"profile-view"))',
					'type'=>'raw'
			),
			array(
					'header'=>'Status',
					'value'=>'$data->status',
			),
// 			array(
// 					'header'=>'Date Joined',
// 					'value'=>'$data->created_at',
// 			),
// 			array(
// 					'header'=>'Last Active',
// 					'value'=>'$data->loggeddatetime',
//                    // 'visible'=>$basic,
// 			),
			
    	    array
	        (
    	        'header'=>'Job count',
    	        'value'=>'$data->created_at != "" ? $data->created_at : 0',
    	       // 'visible'=>$paid,
    	    ),

    	    array
    	    (
    	        'header'=>'Job View count',
    	        'value'=>'$data->updated_at != "" ? $data->updated_at : 0',
    	        // 'visible'=>$paid,
    	    ),
    	    array
    	    (
    	        'header'=>'Profile Viewed count',
    	        'value'=>'$data->display_gender != "" ? $data->display_gender : 0',
    	        // 'visible'=>$paid,
    	    ),
	),
)); ?>
</div>

