<?php
/* @var $this SiteController */
?>


<script>
    function message(id)
    {
        $('#msgdialog').addClass('in');
        $('body').addClass('modal-open');
        $('#msgdialog').show();
        $("#user_id").val(id);

    }
 var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = decodeURIComponent(window.location.search.substring(1)),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : sParameterName[1];
            }
        }
    };
    
    function removeURLParameter(url, parameter) {
        //prefer to use l.search if you have a location/link object
        var urlparts= url.split('?');   
        if (urlparts.length>=2) {

            var prefix= encodeURIComponent(parameter)+'=';
            var pars= urlparts[1].split(/[&;]/g);

            //reverse iteration as may be destructive
            for (var i= pars.length; i-- > 0;) {    
                //idiom for string.startsWith
                if (pars[i].lastIndexOf(prefix, 0) !== -1) {  
                    pars.splice(i, 1);
                }
            }

            url= urlparts[0]+'?'+pars.join('&');
            return url;
        } else {
            return url;
        }
    }
    $(document).ready(function ()
    {
    	
//         $(".sort-cand").click(function()
//         {           
//             var sort = $('.sorting').val();
//             if(!getUrlParameter('sort') || getUrlParameter('sort') == null)
//                 window.location.search += '&sort=1';
//             else
//             {
//                 var t =  window.location.href;
//                 t = t.substr(0, t.lastIndexOf("&"));
//                 t += '&sort='+sort;
//                 window.location.href = t;
//             }
//         });
        
//         $(".sort-video").click(function()
//         {           
//             var video = $('.video').val();
//             if(!getUrlParameter('has_video') || getUrlParameter('has_video') == null)
//                 window.location.search += '&has_video=1';
//             else
//             {
//                 var t =  window.location.href;
//                 t = t.substr(0, t.lastIndexOf("&"));
//                 t += '&has_video='+video;
//                 window.location.href = t;
//             }
//         });
        
        	$("#sorting").change(function()
	        {         

	            var sort = $(this).val();
	            var arr = ['a_z','z_a','recent','old','video','novideo'];
                
                var url =  window.location.href;
                for(var i = 0; i <= arr.length; i++)
                {
                	url = removeURLParameter(url,arr[i]);
                }
//                 url = url.substr(0, url.lastIndexOf("&"));
                url += '&'+sort+'=1';
                window.location.href = url;

	        });
        	
        $(".favourite-links").click(function(e)
        		 {    
        		                 e.preventDefault();
        		                 
        		       var ele = $(this);
        		       var id = $(this).attr('id');
        		                 var $linkText = $(this).find('span').text();
        		             
        		       if(id > 0)
        		              {
        		                    $.ajax({
        		                      url: '<?php echo $this->createUrl('site/addfavourite') ?>',
        		                      type: 'POST',
        		                      data: {fid: id},
        		                      success: function (data) {
        		                          
        		                          if($linkText == "Remove from Favourites")
        		                          { 
        		                              $('#'+id).find('span').html("Add to Favourites");
        		                          }
        		                          else
        		                          {   
        		                              $('#'+id).find('span').html("Remove from Favourites");
        		                          }
        		                       }
        		                  });
        		              }
        		  });
		
        $("#skill_categ").change(function ()
        {
            var id = $(this).val();
            if (id > 0)
            {
                $.ajax({
                    url: '<?php echo $this->createUrl('site/getSkills') ?>',
                    type: 'POST',
                    data: {id: id},
                    success: function (data) {
                        $('#skills').html('');
                        $('#skills').append(data);
                        $('#skills').trigger('liszt:updated');
                    },
                    error: function (data) {
                        alert('err');
                    }
                });
            } else
            {
                $('#skills').html('');
                $('#skills').trigger('liszt:updated');
            }
        });

        $("#sendmsg_btn").click(function ()
        {
            $.ajax({
                url: '<?php echo $this->createUrl('site/sendMessage') ?>',
                type: 'POST',
                data: {user_id: $('#user_id').val(), subject: $('#subject').val(), message: $('#message').val()},
                success: function (data) {
                    closepopup();
                },
                error: function (data) {
                    alert('err');
                }
            });
        });

        $("#msg_cancel").click(function ()
        {
            closepopup();
        });

      
        
    });

   $(document).on("blur",".location-search",function() {
		if($(this).val() == "")
		{
		 	$("#location_lat").val("");
		 	$("#location_lng").val("");
		}
    });
</script>

<div id="page-content">
    <div class="CandidateSearchResults">
        <?php $view = $_GET['view_style']; ?>

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'candidate-search-form',
            'method' => 'get',
            'action' => $this->createUrl('site/searchCandidates'),
        ));
        ?>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-lg-3">
                    <div class="CandidateFilters">
                        <h4>Your Search</h4>
                        
                        <div class="form-group">
                            <label for="name" class="control-label">Candidate Name</label>
                            <?php
                            echo CHtml::textField('name',(isset($_GET['name']) ? $_GET['name'] : ""), array('class' => 'form-control'));

                            ?>
                        </div>
                        
                        <div class="form-group">
                            <label for="category_id" class="control-label">Category</label>
                            <?php
                            $skillcategmodel = SkillCategories::model()->findAll();
                            $skillcateglist = CHtml::listData($skillcategmodel, 'id', 'name');
                            echo CHtml::label('Search Category', '', array());
                            echo CHtml::dropDownList('skill_category', (isset($_GET['skill_category']) ? $_GET['skill_category'] : ''), $skillcateglist, array('empty' => 'Search by Category', 'id' => "skill_categ", 'class' => 'form-control', 'required' => ""));
                            ?>
                        </div>
                        <div class="form-group">
                            <label for="skills" class="control-label">Required Career paths</label>
                            <?php
                            $skilllist = array();
                            if (isset($_GET['skill_category']) && $_GET['skill_category'] > 0) {
                                $skillmodel = Skills::model()->findAllByAttributes(array('skill_category_id' => $_GET['skill_category']));
                                $skilllist = CHtml::listData($skillmodel, 'id', 'name');
                            }
                            ?>

                            <select name="skills[]" id="skills" class="form-control select2" multiple="">	
                                <?php
                                if (isset($_GET['skills'])) {
                                    foreach ($skilllist as $skill => $skillname) {
                                        ?>
                                        <option value="<?php echo $skill ?>" <?php echo in_array($skill, $_GET['skills']) ? 'selected' : '' ?>><?php echo $skillname ?></option>
                                        <?php
                                    }
                                }
                                ?>	
                            </select> 
                        </div>
                        <div class="form-group">
                            <label for="location" class="control-label">Location</label>
                            <?php
                            echo CHtml::textField('location', $_GET['location'], array('class' => 'form-control location-search'));
                            echo CHtml::hiddenField('latitude', $_GET['latitude'], array('id' => 'location_lat'));
                            echo CHtml::hiddenField('longitude', $_GET['longitude'], array('id' => 'location_lng'));
                            ?>
                        </div>
                        <div class="form-group">
                            <select class="form-control" name="radius">
                                <option value="" selected="selected">- Select Distance -</option>
                                <option value="5" <?php echo isset($_GET['radius']) && $_GET['radius'] == "5" ? "selected" : "" ?>>Within 5 miles</option>
                                <option value="10" <?php echo isset($_GET['radius']) && $_GET['radius'] == "10" ? "selected" : "" ?>>Within 10 miles</option>
                                <option value="20" <?php echo isset($_GET['radius']) && $_GET['radius'] == "20" ? "selected" : "" ?>>Within 20 miles</option>
                                <option value="30" <?php echo isset($_GET['radius']) && $_GET['radius'] == "30" ? "selected" : "" ?>>Within 30 miles</option>
                                <option value="40" <?php echo isset($_GET['radius']) && $_GET['radius'] == "40" ? "selected" : "" ?>>Within 40 miles</option>
                                <option value="50" <?php echo isset($_GET['radius']) && $_GET['radius'] == "50" ? "selected" : "" ?>>Within 50 miles</option>
                            </select>
                        </div>
                        <div class="form-group">
                        	<input type="checkbox" name="video" <?php echo isset($_GET['video'])? "checked" : ""  ?> "/>
                        	<label>Candidates with videos only</label>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn main-btn btn-block">Filter</button>
                        </div>
                    </div>                </div>
                <div class="col-md-8 col-lg-9">
                    <div class="CandidateSearchMeta clearfix">
                        <div class="CandidateSearchMeta__page-info pull-left">
                            <h4>Showing <?php echo count($model). " results"?></h4>
                        </div>
                        <div class="pull-right">
                            <div class="CandidateSearchMeta__view-switch">
                                <label for="grid-view" class="<?php echo $view == "grid" ? 'active' : '' ?>">
                                    <i class="fa fa-table"></i>
                                    <div>Grid</div>
                                    <input id="grid-view" onclick="this.form.submit();" checked="checked" name="view_style" type="radio" value="grid">
                                </label>
                                <label for="list-view" class="<?php echo $view == "list" ? 'active' : '' ?>">
                                    <i class="fa fa-list"></i>
                                    <div>List</div>
                                    <input id="list-view" onclick="this.form.submit();" name="view_style" type="radio" value="list">
                                </label>
                            </div>
                        </div>
                    </div>
                   <!-- <div class="clearfix">
                      <div class="pull-right" style="margin-top: 10px">
                                <input type="hidden" name="sorting" class="sorting" value="<?php //echo isset($_GET['sort']) ? ($_GET['sort'] == 1 ? 0 : 1) : 1 ?>">
                                <p><a href="#" class="sort-cand"><?php //echo (isset($_GET['sort']) ? ($_GET['sort'] == 1 ? "A-Z <i class='fa fa-long-arrow-down'></i>" : "Z-A <i class='fa fa-long-arrow-up'></i>") : "A-Z"); ?></a></p>
                        </div>
                        <div class="pull-right" style="margin: 10px">
                                <input type="hidden" name="video" class="video" value="<?php //echo isset($_GET['has_video']) ? ($_GET['has_video'] == 1 ? 0 : 1) : 1 ?>">
                                <p><a href="#" class="sort-video"><?php //echo (isset($_GET['has_video']) ? ($_GET['has_video'] == 1 ? "Without video" : "With video") : "With video"); ?></a></p>
                        </div>
                     </div> --> 
                     
                     <div class="clearfix">
                          <div class="pull-right" style="margin-top: 10px">
                          	    <select name="sorting" id="sorting" class="form-control">
                          			<option value="a_z" <?php echo (isset($_GET['a_z']) && $_GET['a_z'] == 1 ? "selected" : "" ) ?>>Candidate (A-Z)</option>
                          			<option value="z_a" <?php echo (isset($_GET['z_a']) && $_GET['z_a'] == 1 ? "selected" : "" ) ?>>Candidate (Z-A)</option>
                          			<option value="recent" <?php echo (isset($_GET['recent']) && $_GET['recent'] == 1 ? "selected" : "selected" ) ?>>Recent</option>
                          			<option value="old" <?php echo (isset($_GET['old']) && $_GET['old'] == 1 ? "selected" : "" ) ?>>Old</option>
                          			<option value="video" <?php echo (isset($_GET['recent']) && $_GET['video'] == 1 ? "selected" : "" ) ?>>With Video</option>
                          			<option value="novideo" <?php echo (isset($_GET['old']) && $_GET['novideo'] == 1 ? "selected" : "" ) ?>>Without Video</option>
                          		</select>
                          </div>
                     </div>
                    <div class="CandidateList">
                        <div class="row">
                            <?php if ($view == "grid") { ?>
                                <?php
                                foreach ($model as $cand) {
                                    $usermodel = Users::model()->findByPk($cand['owner_id']);
                                    
                                    //check recruitment candidate
                                    $agecanmodel = AgencyCandidates::model()->findByAttributes(array('user_id'=>$cand['owner_id']));
                                    if($agecanmodel != null && Yii::app()->user->getState('role') == "employer")
                                    {
                                        $user_name = "XXXX";
                                        $location = "XXXX";
                                    }
                                    else {
                                        $user_name = $usermodel->forenames." ".$usermodel->surname;
                                        $location = $usermodel->location;
                                    }
                                    //ends
                                    
//                                  $cri = new CDbCriteria();
//                                             $cri->select = "*,DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(dob, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(dob, '00-%m-%d')) AS gender";
//                                             $cri->condition = "id = ".$cand['owner_id'];
                                            
                                            
//                                             $usermodel = Users::model()->find($cri);
//                                             if($usermodel['gender'] > 16)
//                                             {
                                    if ($cand['photo_id'] > 0) {
                                        $mediamodel = Media::model()->findByPk($cand['photo_id']);
                                        $src = Yii::app()->baseUrl . '/images/media/' . $mediamodel->id . '/conversions/search.jpg';
                                    } else {
                                        $src = Yii::app()->baseUrl . '/images/defaultprofile.jpg';
                                    }
                                    ?>
                                    <div class="col-sm-4 profilelist">
                                        <div class="CandidateItem CandidateItem--grid">
                                            <div class="CandidateItem__inner">
                                                <div class="CandidateItem__image">
                                                    <img src="<?php echo $src ?>" class="img-responsive">
                                                </div>
                                                <div class="CandidateItem__name">
                                                    <a class="href" href="<?php echo $this->createUrl('users/profile', array('slug' => $cand['slug'])) ?>">
                                                        <?php echo $user_name; ?>
                                                    </a>
                                                </div>
                                                <div class="CandidateItem__meta">
                                                    <div class="CandidateItem__job"><i class="fa fa-info"></i><?php echo $usermodel->current_job; ?></div>
                                                    <div class="CandidateItem__location"><i class="fa fa-map-marker"></i><?php echo $location; ?></div>
                                                    <?php 
                                                        $pvmodel = ProfileVideos::model()->findByAttributes(array('profile_id'=> $cand['id']));
                                                        if($pvmodel['video_id'] > 0)
                                                        {
                                                    ?>
                                                    <div class="CandidateItem__videolink">
                                                        <a class="btn hollow-btn" href="<?php echo $this->createUrl('users/profile', array('slug' => $cand['slug'])) ?>"><i class="fa fa-play"></i>Play Video CV</a>
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <?php 
                                            if(Yii::app()->user->getState('role') == "employer"):
                                            $empusrmodel = EmployerUsers::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid')));
                                            $empmodel = Employers::model()->findByPk($empusrmodel->employer_id);
                                            if($empmodel->stripe_active):
                                            ?>
                                            <div class="CandidateItem__buttons">
                                                <a class="OpenMessageBox" onclick="message(<?php echo $usermodel->id ?>)"><i class="fa fa-envelope"></i>Message</a>
                                                <?php  
                                                $empusrmodel = EmployerUsers::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
                                                $promodel = Profiles::model()->findByAttributes(array('owner_id' => $empusrmodel->employer_id,'type'=>'employer'));
                                                    $pfav = ProfileFavourites::model()->findByAttributes(array('profile_id'=>$promodel->id,'favourite_id'=>$cand['id']));
                                                    if($pfav == null)
                                                    {
                                                  ?>
                                                	<a class="favourite-links" id="<?php echo $cand['id']?>" ><i class="fa fa-heart"></i><span><?php echo "Add to Favourites"?></span></a>
                                                <?php 
                                                    }
                                                    else 
                                                    {
                                                ?>	
                                                	<a class="favourite-links" id="<?php echo $cand['id']?>"><i class="fa fa-heart"></i><span><?php echo "Remove from Favourites"?></span></a>
                                                
                                                <?php }?>
                                            </div>
                                                   <?php endif; ?>
                                               <?php endif; ?>
                                        </div>
                                    </div>
                                     <?php //} 
                                }} if ($view == "list") { ?>
                                    <?php
                                    foreach ($model as $cand) {

                                        $usermodel = Users::model()->findByPk($cand['owner_id']);
                                        
                                        //check recruitment candidate
                                        $agecanmodel = AgencyCandidates::model()->findByAttributes(array('user_id'=>$cand['owner_id']));
                                        if($agecanmodel != null && Yii::app()->user->getState('role') == "employer")
                                        {
                                            $user_name = "XXXX";
                                            $location = "XXXX";
                                            
                                        }
                                        else {
                                            $user_name = $usermodel->forenames." ".$usermodel->surname;
                                            $location = $usermodel->location;
                                        }
                                        //ends
//                                         $cri = new CDbCriteria();
//                                             $cri->select = "*,DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(dob, '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(dob, '00-%m-%d')) AS gender";
//                                             $cri->condition = "id = ".$cand['owner_id'];
                                            
                                            
//                                             $usermodel = Users::model()->find($cri);
//                                             if($usermodel['gender'] > 16)
//                                             {
                                        if ($cand['photo_id'] > 0) {
                                            $mediamodel = Media::model()->findByPk($cand['photo_id']);
                                            $src = Yii::app()->baseUrl . '/images/media/' . $mediamodel->id . '/conversions/search.jpg';
                                        } else {
                                            $src = Yii::app()->baseUrl . '/images/defaultprofile.jpg';
                                        }
                                        ?>
                                        <div class="search-item candidate clearfix">
                                            <div class="col-sm-2">
                                                <div class="candidate-image">
                                                    <img src="<?php echo $src ?>" class="img-responsive">
                                                    <?php 
                                                        $pvmodel = ProfileVideos::model()->findByAttributes(array('profile_id'=> $cand['id']));
                                                        if($pvmodel['video_id'] > 0)
                                                        {
                                                            
                                                    ?>
                                                        <a class="btn hollow-btn btn-block" href="<?php echo $this->createUrl('users/profile', array('slug' => $cand['slug'])) ?>"><i class="fa fa-play"></i>Video CV</a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="col-sm-10">
                                                <div class="candidate-name">
                                                    <a href="<?php echo $this->createUrl('users/profile', array('slug' => $cand['slug'])) ?>">
                                                        <?php echo $user_name; ?>
                                                    </a>
                                                </div>
                                                <div class="candidate-meta">
                                                    <span class="candidate-job"><i class="fa fa-info"></i><?php echo $usermodel->current_job; ?></span>
                                                    <span class="candidate-location"><i class="fa fa-map-marker"></i><?php echo $location; ?></span>
                                                </div>
                                                <div class="candidate-skills-container">
                                                    <div class="skill-label">Key Skills:</div>
                                                    <div class="candidate-skills">
                                                        <?php
                                                        $psmodel = ProfileSkills::model()->findAllByAttributes(array('profile_id' => $cand['id']));
                                                        foreach ($psmodel as $ps) {
                                                            $skillmodel = Skills::model()->findByPk($ps['skill_id']);
                                                            ?>
                                                            <div class="candidate-skill"><?php echo $skillmodel->name ?> </div>                                    
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                                <div class="candidate-buttons">
                                                
                                                  <?php 
 if(Yii::app()->user->getState('role') == "employer"){
                                                  $empusrmodel = EmployerUsers::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid')));
                                                  $empmodel = Employers::model()->findByPk($empusrmodel->employer_id);
                                                  if($empmodel->stripe_active){
                                                  $emodel = Employers::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid')));
                                                  $promodel = Profiles::model()->findByAttributes(array('owner_id'=>$emodel->id));
                                                   
                                                  $pfav = ProfileFavourites::model()->findByAttributes(array('profile_id'=>$promodel->id,'favourite_id'=>$cand['id']));
                                                  if($pfav == null)
                                                  {
                                                      ?>
                                                   <div><a class="favourite-links" id="<?php echo $cand['id']?>" href="#"> <i class="fa fa-heart"></i><span><?php echo "Add to Favourites" ?></span></a></div>
                                                    <?php }
                                                    else { ?>
                                                    <div><a class="favourite-links" id="<?php echo $cand['id']?>" href="#"> <i class="fa fa-heart"></i><span><?php echo "Remove from Favourites" ?></span></a></div>
                                                  <?php } ?>
                                                  <div><a class="MessageBox" onclick="message(<?php echo $usermodel->id ?>)" href="#"><i class="fa fa-envelope"></i>Send Message</a></div>
                                                   <?php } }?>
                                                </div>
                                            </div>
                                        </div> 
                                      <?php //}
                                    }
							} ?>
                                </div>
                            </div>

                            <div class="CandidatePager">

                            </div>
                        </div>
                    </div>
                </div>
                  <?php $this->endWidget(); ?>
            </div>
        </div>



    <?php 
    $this->beginWidget(
            'booster.widgets.TbModal', array('id' => 'msgdialog')
    );
    ?>
    <div class="modal-header">
        <a class="close" data-dismiss="modal" onclick='closepopup()'>&times;</a>
        <h4 class="msg-title">Send Message</h4>
    </div>

    <div class="modal-body clearfix">
        <div class="col-sm-12">
            <div class="form-group">
                    <?php
                    echo CHtml::hiddenField('user_id', '', array('id' => 'user_id'));
                    echo CHtml::label('Subject', '', array());
                    echo CHtml::textField('subject', '', array('class' => 'form-control', 'id' => 'subject', 'required' => 'required'));
                    ?>
            </div>
            <div class="form-group">
                    <?php
                    echo CHtml::label('Message', '', array());
                    echo CHtml::textArea('message', '', array('class' => 'form-control', 'id' => 'message', 'rows'=>"10"));
                    ?> 
            </div>

        </div>
    </div>

    <div class="modal-footer">
        <?php
        echo CHtml::button('Send', array("class" => "btn btn-default", "id" => "sendmsg_btn"));
        echo CHtml::button('Cancel', array("class" => "btn btn-default", "id" => "msg_cancel"));
        ?>
    </div>

    <?php $this->endWidget(); ?>
