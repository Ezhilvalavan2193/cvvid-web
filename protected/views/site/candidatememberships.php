<?php
/* @var $this SiteController */
?>

<div class="site-content">
    <div class="page-title text-left">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1 class="text-center padd-font">   
                        CHOOSE YOUR CANDIDATE MEMBERSHIP
                    </h1>
                </div>
            </div>
        </div>
    </div>
    <div class="page-content">
        
        <div class="price-tables-container">
            <div class="container">
                <div class="row">
                    <div class="col-sm-offset-1 col-sm-5 col-md-offset-2 col-md-4">
                        <div class="subscription-plan candidate-sub">
                            <div class="subscription-plan-header">
                                <h2 class="plan-title">Basic</h2>
                                <div class="plan-price">FREE</div>
                            </div>
                            <div class="subscription-plan-content">
                                <div class="subscription-plan-inner">
                                    <div class="plan-description">
                                        <div class="plan-description">
                                            Enjoy the basic features of the CVVid platform.
                                        </div>
                                        <div class="plan-features">
                                            <ul class="fa-ul">
                                                <li><i class="fa fa-circle"></i>Create your own CVVid profile</li>
                                                <li><i class="fa fa-circle"></i>Upload your portfolio</li>
                                                <li><i class="fa fa-circle"></i>Browse all vacancies</li>
                                                <li><i class="fa fa-circle"></i>Create your CVVid profile PDF</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="plan-button">
                                        <a href="<?php echo $this->createUrl('site/basicregistercandidate'); ?>" class="btn hollow-btn">Basic Account</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- .col-sm-4 -->
                    <div class="col-sm-5 col-md-4">
                        <div class="subscription-plan premium candidate-sub">
                            <div class="subscription-plan-header">
                                <h2 class="plan-title">Premium</h2>

                                <div class="plan-price">
                                   <?php if (Membership::freeTrailEnabled()){ ?>
                                    <p style="padding-bottom:0px;"><strike style="text-decoration-color: red;">£ 252.00</strike>  Free</p>
                                   <?php }else{ ?>
                                    <small>From</small> £25.00
                                   <?php } ?>
                                </div>
                            </div>
                            <div class="subscription-plan-content">
                                <div class="subscription-plan-inner">
                                    <div class="plan-description">
                                        Enjoy the full benefits of the CVVid platform.
                                    </div>
                                    <div class="plan-features">
                                        <div>All Basic features plus</div>
                                        <ul class="fa-ul">
                                            <li><i class="fa fa-circle"></i>Browse and apply for jobs</li>
                                            <li><i class="fa fa-circle"></i>Unique real-time messaging platform</li>
                                            <li><i class="fa fa-circle"></i>See who has visited your profile</li>
                                            <li><i class="fa fa-circle"></i>See who has liked your profile</li>
                                        </ul>
                                    </div>

                                    <?php if (Membership::freeTrailEnabled()){ ?>
                                    <div class="free-trial plan-button">
                                        <a href="<?php echo $this->createUrl('site/premiumregistercandidate'); ?>" class="btn btn-danger">
                                            JOIN FOR <span>FREE</span>
                                        </a>
                                        <br/>
                                        <small>for a limited time only</small>
                                    </div>
                                    <?php }else{ ?>
                                    <div class="plan-button">
                                        <a href="<?php echo $this->createUrl('site/premiumregistercandidate'); ?>" class="btn default-btn">Premium Account</a>
                                    </div>
                                     <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div><!-- .col-sm-4 -->
                </div>
            </div><!-- .container -->
        </div><!-- .price-tables -->
    </div>
</div>