<?php 
/* @var $this SiteController */
/* @var $model ContactForm */
/* @var $form CActiveForm */
?>
<div class="page-title text-left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="text-contact padd-font">Contact Us</h1>
            </div>
        </div>
    </div>
</div>
<div id="page-content" style="background: white;">
    <div class="container">
    <?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
     <?php endif; ?>
     <?php 
     $forenames = "";
     $surname = "";
     $email = "";
        if(Yii::app()->user->getState('userid') > 0)
        {
            $usermodel = Users::model()->findByPk(Yii::app()->user->getState('userid'));
            $model->forenames = $usermodel->forenames;
            $model->surname = $usermodel->surname;
            $model->email = $usermodel->email;
        }
     ?>
        <div class="row" style="margin-top: 30px;">
        
            <div class="col-sm-offset-3 col-sm-6">
                <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'contact-form',
                        'action' => $this->createUrl('site/contact'),
                        'enableAjaxValidation' => false
                    ));
                ?>
                <?php echo  $form->errorSummary(array($model), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
                <p>Send us a message and a member of client services will get back to you within 48 hours.</p>

                <div class="row contact-row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="forenames" class="control-label">First Name</label>
                            <?php echo $form->textField($model,'forenames', array('class' => 'form-control')); ?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="surname" class="control-label">Surname</label>
                            <?php echo $form->textField($model,'surname',array('class' => 'form-control')); ?>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="control-label">Email</label>
                    <?php echo $form->textField($model,'email', array('class' => 'form-control')); ?>
                </div>

                <div class="form-group">
                    <label for="message" class="control-label">Message</label>
                    <?php echo $form->textArea($model,'message', array('class' => 'form-control', 'cols'=>'50','rows'=>'10')); ?>
                </div>

				<div class="row">
                    <div class="col-sm-6">
						<div class="form-group">
                    		<label for="message" class="control-label">User Type</label><br>
                            <?php 
                            $model->topic = 1;
                           // echo $form->radioButtonList($model,'topic', array(1=>'Institution', 2=>'Employer',3=>'Candidate',4=>'Recruitment Agency',5=>'Others'), array('separator'=>" "));
                            echo $form->dropDownList($model,'topic', array(1=>'Institution', 2=>'Employer',3=>'Candidate',4=>'Recruitment Agency',5=>'Others'), array('class' => 'form-control'));
                           
                           ?>
                        </div>
               	      </div>
                </div>
                
				<script src='https://www.google.com/recaptcha/api.js'></script>
                <div class="g-recaptcha" data-sitekey="6Le5Vz4UAAAAAG8IXqMe7onsjTZxNXkLj70i72CG"></div>
                    
                <div class="form-actions">
                    <?php echo CHtml::submitButton('Send', array("class" => "btn btn-default contact-btn")); ?> 
                </div>

                <?php $this->endWidget(); ?>

            </div>
        </div>
    </div>
</div>

