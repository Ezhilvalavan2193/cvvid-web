<div id="page-content">
<section id="employer-faqs1">
  <?php 
  if($model->background_id > 0)
  {
      $mediamodel = Media::model()->findByPk($model->background_id);
      if($mediamodel != null && $mediamodel->file_name != null)
      {
          $src = Yii::app()->baseUrl.'/images/media/'.$mediamodel->id.'/'.$mediamodel->file_name; ?>
    <div class="fullscreen" style="background-image: url('<?php echo $src; ?>'); background-position: <?php echo $model->background_x; ?> <?php echo $model->background_x; ?>;">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">

                    <div class="Slide__content">
                    <h1>Frequently Asked Questions</h1>
                        <div class="PageSlideshow">
                            <div class="PageSlideshow__slides">
                                <?php
                                foreach ($slidemodel as $slide) {
                                    ?>
                                    <div class="PageSlideshow__slide">
                                        <?php echo $slide['body']; ?>
                                    </div><!-- .PageSlideshow__slide -->
                                <?php } ?>

                            </div>
                            <div class="PageSlideshow__controls" style="margin-top: 20px;">
                                <a href="#" class="PageSlideshow__nav-btn PageSlideshow__prev"><i class="fa fa-chevron-left"></i></a>
                            <span class="PageSlideshow__pages">
                                <span class="PageSlideshow__current-page">1</span>/<span class="PageSlideshow__count">4</span>
                            </span>
                                <a href="#" class="PageSlideshow__nav-btn PageSlideshow__next"><i class="fa fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
  <?php } } else { ?>
    
    <div class="Slideshow FullscreenSlideshow" data-autoplay="true" data-autoplay-speed="4000" data-dots="false" data-arrows="false">
        <?php
        if (isset($slidemodel)){
        $cnt = 0;
        foreach ($slidemodel as $slide) { 
           $mediamodel = Media::model()->findByPk($slide['media_id']);  
        ?>
        <div class="Slide" style="background-image: url('<?php echo Yii::app()->baseUrl . "/images/media/" . $mediamodel->id . "/" . $mediamodel->file_name; ?>'); background-position: <?php echo $slide['background_x']; ?> <?php echo $slide['background_y']; ?>;">
            <div class="container">
                <div class="Slide__inner">
                    <div class="Slide__caption">
                       <?php echo $slide['body']; ?>
                        <a href="<?php echo Yii::app()->createUrl('site/about'); ?>" class="btn btn-primary">Learn More</a>
                        <a href="#" class="view-showcase btn btn-primary">View CV Video Showcase</a>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <?php } ?>
    </div>
      <?php }  ?>
        </section>
      <section id="employer-faqs2">
<div class="col-sm-12">
                <div class="swiper-container frequent-container">
                <h2 class="text-center padd-font">Frequently Asked Questions</h1>
        <ul class="swiper-wrapper" style="transform: none!important;">
        <?php
                                foreach ($slidemodel as $slide) {
                                    ?>
            <li class="swiper-slide">
            <?php echo $slide['body']; ?></li>
            <?php } ?>
        </ul>
        <!-- Add Pagination -->
        <div class="swiper-pagination"></div>
    </div>
            </div>
                                </section>
</div>
<script>
$(document).ready(function(){
    var swiper = new Swiper('.frequent-container', {
      spaceBetween: 0,
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
    });
});
   
  </script>

<style>

    .swiper-container {
        width: 100%;
        height: 100%;
    }
    .swiper-wrapper{
        padding-left: 1px;
      margin: 0;
    }
    
    .swiper-slide {
        padding: 20px;
    font-size: 15px;
        background: #fff;
        width: 100%;
        /* border-right: 1px solid black; */
        display: -webkit-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: block;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        -webkit-justify-content: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        -webkit-align-items: center;
        align-items: center;
    }
  </style>

<script>
     $(document).ready(function () {
        //  alert('hai');
        if ($(window).innerWidth() >= 600) {
            
            $('#employer-faqs1').add();
            $('#employer-faqs2').remove();
        
        } else if ($(window).innerWidth() < 600) {
            
            $('#employer-faqs2').add();
            $('#employer-faqs1').remove();

            
        }
    // var $drillDown = $("#drilldown");
});
     </script>

