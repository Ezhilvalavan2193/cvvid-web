<?php

/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>
<script>
$(document).ready(function(){
	
	$("#skill_categ").change(function ()
	{
		            var id = $(this).val();
		            if (id > 0)
		            {
		                $.ajax({
		                    url: '<?php echo $this->createUrl('site/getSkills') ?>',
		                    type: 'POST',
		                    data: {id: id},
		                    success: function (data) {
		                        $('#skills').html('');
		                        $('#skills').append(data);
		                        $('#skills').trigger('liszt:updated');
		                    },
		                    error: function (data) {
		                        alert('err');
		                    }
		                });
		            } else
		            {
		                $('#skills').html('');
		                $('#skills').trigger('liszt:updated');
		            }
	 });

});
</script>
<style>
.filter-digi
{
  width: 30% !important;
}
.select2
{
    width: 263px!important;
}
.digital-pass
{
    list-style: none;
    display: inline-block;
    width: 32%;
    padding: 1px;
}
.digital-pass label
{
    padding-left: 5px;
}
@media only screen and (max-width: 767px)
{
    .digital-pass
    {
        list-style: none;
        display: inherit;
        width: 100%;
        padding: 1px;
    }
}
</style>

<?php

$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl('site/digitalpassport'), //Yii::app()->createUrl($this->route),
    'method' => 'get',
        ));
?>
<!-- <div class="row">
<div class="form-group" style='margin-right:5px'>
<?php 
    echo CHtml::checkBox('video_cv', (isset($_GET['video_cv']) ? $_GET['video_cv'] : ""),array('id'=>'video_cv'));
    echo CHtml::label('Have Video CV', 'video_cv',array());
?>
</div>
<div class="form-group" style='margin-right:5px'>
<?php 
    echo CHtml::checkBox('profile_image', (isset($_GET['profile_image']) ? $_GET['profile_image'] : ""),array('id'=>'profile_image'));
    echo CHtml::label('Have Profile Image', 'profile_image',array());
?>
</div>

<div class="form-group" style='margin-right:5px'>
<?php 
    echo CHtml::checkBox('profile_completed', (isset($_GET['profile_completed']) ? $_GET['profile_completed'] : ""),array('id'=>'profile_completed'));
    echo CHtml::label('50% Profile Completed', 'profile_completed',array());
?>
</div>

<div class="form-group" style='margin-right:5px'>
<?php 
    echo CHtml::checkBox('applied_jobs', (isset($_GET['applied_jobs']) ? $_GET['applied_jobs'] : ""),array('id'=>'applied_jobs'));
    echo CHtml::label('Applied for Jobs', 'applied_jobs',array());
?>
</div>

<div class="form-group" style='margin-right:5px'>
<?php 
    echo CHtml::checkBox('secured_jobs', (isset($_GET['secured_jobs']) ? $_GET['secured_jobs'] : ""),array('id'=>'secured_jobs'));
    echo CHtml::label('Secured Jobs', 'secured_jobs',array());
?>
</div>

<div class="form-group" style='margin-right:5px'>
<?php 
    echo CHtml::checkBox('video_secured_jobs', (isset($_GET['video_secured_jobs']) ? $_GET['video_secured_jobs'] : ""),array('id'=>'video_secured_jobs'));
    echo CHtml::label('Secured Jobs By Video', 'video_secured_jobs',array());
?>
</div>

</div> -->

 <ul>

<!--<li class="digital-pass">
<?php 
    echo CHtml::checkBox('video_cv', (isset($_GET['video_cv']) ? $_GET['video_cv'] : ""),array('id'=>'video_cv'));
    echo CHtml::label('Have Video CV', 'video_cv',array());
?>

</li>
<li class="digital-pass">
<?php 
    echo CHtml::checkBox('profile_image', (isset($_GET['profile_image']) ? $_GET['profile_image'] : ""),array('id'=>'profile_image'));
    echo CHtml::label('Have Profile Image', 'profile_image',array());
?>
</li>

<li class="digital-pass">
<?php 
    echo CHtml::checkBox('profile_completed', (isset($_GET['profile_completed']) ? $_GET['profile_completed'] : ""),array('id'=>'profile_completed'));
    echo CHtml::label('50% Profile Completed', 'profile_completed',array());
?>
</li>

<li class="digital-pass">
<?php 
    echo CHtml::checkBox('applied_jobs', (isset($_GET['applied_jobs']) ? $_GET['applied_jobs'] : ""),array('id'=>'applied_jobs'));
    echo CHtml::label('Applied for Jobs', 'applied_jobs',array());
?>
</li>

<li class="digital-pass">
<?php 
    echo CHtml::checkBox('secured_jobs', (isset($_GET['secured_jobs']) ? $_GET['secured_jobs'] : ""),array('id'=>'secured_jobs'));
    echo CHtml::label('Secured Jobs', 'secured_jobs',array());
?>
</li>

<li class="digital-pass">
<?php 
    echo CHtml::checkBox('video_secured_jobs', (isset($_GET['video_secured_jobs']) ? $_GET['video_secured_jobs'] : ""),array('id'=>'video_secured_jobs'));
    echo CHtml::label('Secured Jobs By Video', 'video_secured_jobs',array());
?>
</li>-->

<?php
$value = "";
if(isset($_GET['digital_passport']))
    $value = urldecode($_GET['digital_passport']);
    
                    
$this->widget('zii.widgets.jui.CJuiAutoComplete',array(
    'name'=>'digital_passport',
    'source'=>array('Video Profile','Have Profile Image','50 percentage Profile Completed','Applied for Jobs','Secured Jobs','Secured Jobs By Video'),
    'value'=>$value,
    // additional javascript options for the autocomplete plugin
    'options'=>array(
        'minLength'=>'1',
    ),
    'htmlOptions'=>array(
        'class'=>'form-control filter-digi',
        'placeholder'=>'Enter Keyword'
        //'style'=>'height:20px;',
    ),
));
?>
<?php 
    echo CHtml::submitButton('Search', array("class" => "btn btn-primary"));
    echo CHtml::button('Clear', array("class" => "btn btn-primary", "onclick" => "window.location='" . Yii::app()->createUrl("site/digitalpassport") . "'"));
?>

</ul>

<?php $this->endWidget(); ?>

