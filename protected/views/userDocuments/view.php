<?php
/* @var $this UserDocumentsController */
/* @var $model UserDocuments */

$this->breadcrumbs=array(
	'User Documents'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List UserDocuments', 'url'=>array('index')),
	array('label'=>'Create UserDocuments', 'url'=>array('create')),
	array('label'=>'Update UserDocuments', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete UserDocuments', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage UserDocuments', 'url'=>array('admin')),
);
?>

<h1>View UserDocuments #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'media_id',
		'name',
		'category_id',
		'published',
		'created_at',
		'updated_at',
		'deleted_at',
	),
)); ?>
