<?php
/* @var $this EmployerUsersController */
/* @var $model EmployerUsers */

$this->breadcrumbs=array(
	'Employer Users'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#employer-users-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<script>
    $(document).ready(function(){
    	$('#export-employer-users').on('click',function() {
	    window.location = '<?php echo $this->createUrl('employerUsers/exportCSV') ?>'
	});
	});
</script>
<?php if(Yii::app()->user->hasFlash('success')):?>
<div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
   <?php echo Yii::app()->user->getFlash('success'); ?>
</div>
<?php endif; ?>
<h1>Employer Users</h1>
<div class="Admin__content__inner">
    <div class="AdminFilters">
        <div class="form-inline">
           <?php $this->renderPartial('_search',array(
                    'model'=>$model,'empmodel'=>$empmodel,'namesearch'=>$namesearch,'namearray'=>$namearray,'fullname'=>$fullname
            )); ?>
            <div class="pull-right">
                  <?php  
                        echo CHtml::button('Create',array("class"=>"btn btn-primary","onclick"=>"window.location='".Yii::app()->createUrl('employerUsers/create')."'"));
                        echo CHtml::button('Export',array("class"=>"btn btn-primary",'id'=>'export-employer-users'));
                  ?>
            </div>
        </div>
    </div>

  <?php 
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'employer-users-grid',
        'itemsCssClass' => 'table',
	'summaryText'=>'',
	//'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',
    'pager' => array(
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'header'=>'',
        'firstPageLabel' => '<<',
        'lastPageLabel' => '>>',
        'nextPageLabel' => '>>',
        'prevPageLabel' => '<<'),
			'ajaxUpdate'=>false,
	'dataProvider'=> (($namesearch > 0) ? $model->searchByName($namearray) : $model->search()),
	//'filter'=>$model,
	'columns'=>array(
			//'id',
			array(
					'header'=>'Name',
					'value'=>'CHtml::link("<i class=\"fa fa-pencil\"></i> ".$data->user->forenames." ".$data->user->surname,Yii::app()->createUrl("employerUsers/edit",array("id"=>$data->user->id)),array("class"=>"employer-edit-button","id"=>"employer-edit"))',
			                'type'=>'raw'
			),
			array(
					'header'=>'Email',
					'value'=>'$data->user->email',
					'type'=>'raw'
			),
			array(
					'header'=>'Employer',
					'value'=>'$data->employer->name',
			),
			array(
					'header'=>'Created At',
					'value'=>'$data->created_at',
			),
	),
)); ?>

</div>


