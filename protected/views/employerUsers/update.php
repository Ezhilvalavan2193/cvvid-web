<?php
/* @var $this EmployerUsersController */
/* @var $model EmployerUsers */

$this->breadcrumbs=array(
	'Employer Users'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EmployerUsers', 'url'=>array('index')),
	array('label'=>'Create EmployerUsers', 'url'=>array('create')),
	array('label'=>'View EmployerUsers', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage EmployerUsers', 'url'=>array('admin')),
);
?>

<h1>Update EmployerUsers <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>