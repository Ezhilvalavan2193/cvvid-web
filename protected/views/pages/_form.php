
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'pages-form',
    'enableAjaxValidation' => false,
        ));
?>

<?php echo $form->errorSummary($model); ?>

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <?php echo $form->textField($model, 'title', array('class' => 'form-control')); ?>
            <?php echo $form->error($model, 'title'); ?>
        </div>
        <div class="form-group">
            <div class="input-group">
                <span class="input-group-addon"><?php echo "http://" . $_SERVER['SERVER_NAME'] . "/" ?></span>
                <?php echo $form->textField($model, 'slug', array('class' => 'form-control', 'placeholder' => 'Slug', 'required' => 'required')); ?>
                <?php echo $form->error($model, 'slug'); ?>
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model, 'body'); ?>
            <?php echo $form->textArea($model, 'body', array('class' => 'textArea', 'id' => 'content-body')); ?>
            <?php echo $form->error($model, 'body'); ?>
        </div>
    </div>
     <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array("class" => "btn btn-primary")); ?>
</div>
<?php $this->endWidget(); ?>
