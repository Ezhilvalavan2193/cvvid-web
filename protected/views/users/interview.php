<?php
/* @var $this UsersController */
/* @var $model Users */
?>
<style>
    .recordrtc button {
        font-size: inherit;
    }
    
    #page-content{
        background: #efefef;
    }

    .recordrtc button, .recordrtc select {
        vertical-align: middle;
        /*            line-height: 1;
                    padding: 2px 5px;
                    height: auto;*/
        font-size: 14px;
        margin: 0;
    }

    .recordrtc, .recordrtc .header {
        display: block;
        text-align: center;
        padding-top: 0;
    }

    .recordrtc video {
        width: 60%;
        background: #fff;
        border: 1px solid #efefef;
        border-radius: 8px;
        -webkit-box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2);
        box-shadow: 0 2px 2px 0 rgba(0,0,0,0.14), 0 3px 1px -2px rgba(0,0,0,0.12), 0 1px 5px 0 rgba(0,0,0,0.2); 
    }

    .recordrtc option[disabled] {
        display: none;
    }
</style>

<script src="https://cdn.webrtc-experiment.com/RecordRTC.js"></script>
<script src="https://cdn.webrtc-experiment.com/gif-recorder.js"></script>
<script src="https://cdn.webrtc-experiment.com/getScreenId.js"></script>

<!-- for Edige/FF/Chrome/Opera/etc. getUserMedia support -->
<script src="https://cdn.webrtc-experiment.com/gumadapter.js"></script>


<div id="page-content">
    <div class="container" id="basic-container">
        <?php
//         $cri = new CDbCriteria();
//         $cri->alias = "q";
//         $cri->join = "left join cv16_job_answers a on a.question_id = q.id";
//         $cri->condition = "a.id is null";
//         $jqmodel = JobQuestions::model()->find($cri);

        if ($jqmodel != null) {
            ?>
        
        <div class="card">
            <label>Question:</label>
            <span><?php echo $jqmodel['question']; ?></span>
        </div>
        <div class="timer pull-right">
        <div style="padding: 10px;background: #f9df6e;border: 1px solid #efefef;">
            <time id="countdown"></time>
            </div>
        </div>
            <!--<h3 style="text-align: center;"></h3>-->
            <input type="hidden" value="<?php echo $jqmodel['id'] ?>" id="question_id"> 
            <section class="experiment recordrtc">
                <h3 class="header">
                  <button class="btn btn-primary">Start Recording</button>
                </h3>

                <div style="text-align: center; display: none;">
                    <button id="save-to-disk" style="display: none;">Save To Disk</button>
                    <button id="open-new-tab" style="display: none;">Open New Tab</button>
                    <button id="upload-to-server" class="btn btn-primary">Upload Answer</button>
                </div>

                <br>

                <video controls muted></video>


            </section>
            <div class="form-actions pull-right">
                <button disabled="true" type="button" onclick="window.location.href='<?php echo $this->createUrl("users/interview", array('id' => $jamodel->id)) ?>'" class="btn default-btn next-question">Next</button>
                <!--<a href="" disabled="true" class="btn default-btn next-question">Next</a>-->
            </div>
        <?php
        } else { ?>
            <div class="card">
            <span>Thanks...There are no more questions...</span>
        </div>
       <?php }
        ?>
    </div>
    
   <!--  <div class="container" id="error-container" style="display: none;">-->
<!--      <div class="card"> -->
<!--             <span>Sorry, Your scheduled interview time expired...</span> -->
<!--         </div> -->
<!--     </div> -->
</div>
  <!--reject popup-->
        <div class="modal fade" id="ThankModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        
                        <h4 class="modal-title" id="myModalLabel">Thank You</h4>
                    </div>
                    <div class="ModalApplicationStatus__body">

                        <div class="ModalApplicationStatus__form">
                           Thank you for your time, Our team will you contact you shortly.
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary SubmitBtn" id="thank-you" type="button">Ok</button>
                    </div>
                </div>
            </div>
        </div>
<script>
    (function () {
        var params = {},
                r = /([^&=]+)=?([^&]*)/g;

        function d(s) {
            return decodeURIComponent(s.replace(/\+/g, ' '));
        }

        var match, search = window.location.search;
        while (match = r.exec(search.substring(1))) {
            params[d(match[1])] = d(match[2]);

            if (d(match[2]) === 'true' || d(match[2]) === 'false') {
                params[d(match[1])] = d(match[2]) === 'true' ? true : false;
            }
        }

        window.params = params;
    })();
</script>

<script>
    var recordingDIV = document.querySelector('.recordrtc');
//             var recordingMedia = recordingDIV.querySelector('.recording-media');
    var recordingPlayer = recordingDIV.querySelector('video');
//             var mediaContainerFormat = recordingDIV.querySelector('.media-container-format');

    recordingDIV.querySelector('button').onclick = function () {
        var button = this;

        if (button.innerHTML === 'Stop Recording') {
            button.disabled = true;
            button.disableStateWaiting = true;
            setTimeout(function () {
                button.disabled = false;
                button.disableStateWaiting = false;
            }, 2 * 1000);

            button.innerHTML = 'Start Recording';

            function stopStream() {
                if (button.stream && button.stream.stop) {
                    button.stream.stop();
                    button.stream = null;
                }
            }

            if (button.recordRTC) {
                if (button.recordRTC.length) {
                    button.recordRTC[0].stopRecording(function (url) {
                        if (!button.recordRTC[1]) {
                            button.recordingEndedCallback(url);
                            stopStream();

                            saveToDiskOrOpenNewTab(button.recordRTC[0]);
                            return;
                        }

                        button.recordRTC[1].stopRecording(function (url) {
                            button.recordingEndedCallback(url);
                            stopStream();
                        });
                    });
                }
                else {
                    button.recordRTC.stopRecording(function (url) {
                        button.recordingEndedCallback(url);
                        stopStream();

                        saveToDiskOrOpenNewTab(button.recordRTC);
                    });
                }
            }

            return;
        }

        button.disabled = true;

        var commonConfig = {
            onMediaCaptured: function (stream) {
                button.stream = stream;
                if (button.mediaCapturedCallback) {
                    button.mediaCapturedCallback();
                }
                //alert('start');
                button.innerHTML = 'Stop Recording';
                button.disabled = false;
            },
            onMediaStopped: function () {
                //alert('stop');
                button.innerHTML = 'Start Recording';

                if (!button.disableStateWaiting) {
                    button.disabled = false;
                }
            },
            onMediaCapturingFailed: function (error) {
                if (error.name === 'PermissionDeniedError' && !!navigator.mozGetUserMedia) {
                    InstallTrigger.install({
                        'Foo': {
                            // https://addons.mozilla.org/firefox/downloads/latest/655146/addon-655146-latest.xpi?src=dp-btn-primary
                            URL: 'https://addons.mozilla.org/en-US/firefox/addon/enable-screen-capturing/',
                            toString: function () {
                                return this.URL;
                            }
                        }
                    });
                }
                
                if(error.name == 'NotFoundError')
                {
                     alert("No video source detected!");
                }
                
               

                commonConfig.onMediaStopped();
            }
        };

        /*  if(recordingMedia.value === 'record-video') {
         captureVideo(commonConfig);
         
         button.mediaCapturedCallback = function() {
         button.recordRTC = RecordRTC(button.stream, {
         type: 'video',
         disableLogs: params.disableLogs || false,
         canvas: {
         width: params.canvas_width || 320,
         height: params.canvas_height || 240
         },
         frameInterval: typeof params.frameInterval !== 'undefined' ? parseInt(params.frameInterval) : 20 // minimum time between pushing frames to Whammy (in milliseconds)
         });
         
         button.recordingEndedCallback = function(url) {
         recordingPlayer.src = null;
         recordingPlayer.srcObject = null;
         
         if(mediaContainerFormat.value === 'Gif') {
         recordingPlayer.pause();
         recordingPlayer.poster = url;
         
         recordingPlayer.onended = function() {
         recordingPlayer.pause();
         recordingPlayer.poster = URL.createObjectURL(button.recordRTC.blob);
         };
         return;
         }
         
         recordingPlayer.src = url;
         recordingPlayer.play();
         
         recordingPlayer.onended = function() {
         recordingPlayer.pause();
         recordingPlayer.src = URL.createObjectURL(button.recordRTC.blob);
         };
         };
         
         button.recordRTC.startRecording();
         };
         }*/

        /*   if(recordingMedia.value === 'record-audio') {
         captureAudio(commonConfig);
         
         button.mediaCapturedCallback = function() {
         button.recordRTC = RecordRTC(button.stream, {
         type: 'audio',
         bufferSize: typeof params.bufferSize == 'undefined' ? 0 : parseInt(params.bufferSize),
         sampleRate: typeof params.sampleRate == 'undefined' ? 44100 : parseInt(params.sampleRate),
         leftChannel: params.leftChannel || false,
         disableLogs: params.disableLogs || false,
         recorderType: webrtcDetectedBrowser === 'edge' ? StereoAudioRecorder : null
         });
         
         button.recordingEndedCallback = function(url) {
         var audio = new Audio();
         audio.src = url;
         audio.controls = true;
         recordingPlayer.parentNode.appendChild(document.createElement('hr'));
         recordingPlayer.parentNode.appendChild(audio);
         
         if(audio.paused) audio.play();
         
         audio.onended = function() {
         audio.pause();
         audio.src = URL.createObjectURL(button.recordRTC.blob);
         };
         };
         
         button.recordRTC.startRecording();
         };
         }*/

        // if(recordingMedia.value === 'record-audio-plus-video') {
        captureAudioPlusVideo(commonConfig);

        button.mediaCapturedCallback = function () {

            /* if(webrtcDetectedBrowser !== 'firefox') { // opera or chrome etc.
             button.recordRTC = [];
             
             if(!params.bufferSize) {
             // it fixes audio issues whilst recording 720p
             params.bufferSize = 16384;
             }
             
             var audioRecorder = RecordRTC(button.stream, {
             type: 'audio',
             bufferSize: typeof params.bufferSize == 'undefined' ? 0 : parseInt(params.bufferSize),
             sampleRate: typeof params.sampleRate == 'undefined' ? 44100 : parseInt(params.sampleRate),
             leftChannel: params.leftChannel || false,
             disableLogs: params.disableLogs || false,
             recorderType: webrtcDetectedBrowser === 'edge' ? StereoAudioRecorder : null
             });
             
             var videoRecorder = RecordRTC(button.stream, {
             type: 'video',
             disableLogs: params.disableLogs || false,
             canvas: {
             width: params.canvas_width || 320,
             height: params.canvas_height || 240
             },
             frameInterval: typeof params.frameInterval !== 'undefined' ? parseInt(params.frameInterval) : 20 // minimum time between pushing frames to Whammy (in milliseconds)
             });
             
             // to sync audio/video playbacks in browser!
             videoRecorder.initRecorder(function() {
             audioRecorder.initRecorder(function() {
             audioRecorder.startRecording();
             videoRecorder.startRecording();
             });
             });
             
             button.recordRTC.push(audioRecorder, videoRecorder);
             
             button.recordingEndedCallback = function() {
             var audio = new Audio();
             audio.src = audioRecorder.toURL();
             audio.controls = true;
             audio.autoplay = true;
             
             audio.onloadedmetadata = function() {
             recordingPlayer.src = videoRecorder.toURL();
             recordingPlayer.play();
             };
             
             recordingPlayer.parentNode.appendChild(document.createElement('hr'));
             recordingPlayer.parentNode.appendChild(audio);
             
             if(audio.paused) audio.play();
             };
             return;
             }*/

            button.recordRTC = RecordRTC(button.stream, {
                type: 'video',
                disableLogs: params.disableLogs || false,
                // we can't pass bitrates or framerates here
                // Firefox MediaRecorder API lakes these features
            });

            button.recordingEndedCallback = function (url) {
                recordingPlayer.srcObject = null;
                recordingPlayer.muted = false;
                recordingPlayer.src = url;
                recordingPlayer.play();

                recordingPlayer.onended = function () {
                    recordingPlayer.pause();
                    recordingPlayer.src = URL.createObjectURL(button.recordRTC.blob);
                };
            };

            button.recordRTC.startRecording();
        };
        // }

        /*  if(recordingMedia.value === 'record-screen') {
         captureScreen(commonConfig);
         
         button.mediaCapturedCallback = function() {
         button.recordRTC = RecordRTC(button.stream, {
         type: mediaContainerFormat.value === 'Gif' ? 'gif' : 'video',
         disableLogs: params.disableLogs || false,
         canvas: {
         width: params.canvas_width || 320,
         height: params.canvas_height || 240
         }
         });
         
         button.recordingEndedCallback = function(url) {
         recordingPlayer.src = null;
         recordingPlayer.srcObject = null;
         
         if(mediaContainerFormat.value === 'Gif') {
         recordingPlayer.pause();
         recordingPlayer.poster = url;
         recordingPlayer.onended = function() {
         recordingPlayer.pause();
         recordingPlayer.poster = URL.createObjectURL(button.recordRTC.blob);
         };
         return;
         }
         
         recordingPlayer.src = url;
         recordingPlayer.play();
         };
         
         button.recordRTC.startRecording();
         };
         }
         
         if(recordingMedia.value === 'record-audio-plus-screen') {
         captureAudioPlusScreen(commonConfig);
         
         button.mediaCapturedCallback = function() {
         button.recordRTC = RecordRTC(button.stream, {
         type: 'video',
         disableLogs: params.disableLogs || false,
         // we can't pass bitrates or framerates here
         // Firefox MediaRecorder API lakes these features
         });
         
         button.recordingEndedCallback = function(url) {
         recordingPlayer.srcObject = null;
         recordingPlayer.muted = false;
         recordingPlayer.src = url;
         recordingPlayer.play();
         
         recordingPlayer.onended = function() {
         recordingPlayer.pause();
         recordingPlayer.src = URL.createObjectURL(button.recordRTC.blob);
         };
         };
         
         button.recordRTC.startRecording();
         };
         }*/
    };

    function captureVideo(config) {

        captureUserMedia({video: true}, function (videoStream) {
            recordingPlayer.srcObject = videoStream;
            recordingPlayer.play();

            config.onMediaCaptured(videoStream);

            videoStream.onended = function () {
                config.onMediaStopped();
            };
        }, function (error) {
            
            alert(error);
            
            config.onMediaCapturingFailed(error);
        });
    }

    function captureAudio(config) {
        captureUserMedia({audio: true}, function (audioStream) {
            recordingPlayer.srcObject = audioStream;
            recordingPlayer.play();

            config.onMediaCaptured(audioStream);

            audioStream.onended = function () {
                config.onMediaStopped();
            };
        }, function (error) {
            config.onMediaCapturingFailed(error);
        });
    }

    function captureAudioPlusVideo(config) {
        captureUserMedia({video: true, audio: true}, function (audioVideoStream) {
            recordingPlayer.srcObject = audioVideoStream;
            recordingPlayer.play();

            config.onMediaCaptured(audioVideoStream);

            audioVideoStream.onended = function () {
                config.onMediaStopped();
            };
        }, function (error) {
            config.onMediaCapturingFailed(error);
        });
    }

    function captureScreen(config) {
        getScreenId(function (error, sourceId, screenConstraints) {
            if (error === 'not-installed') {
                document.write('<h1><a target="_blank" href="https://chrome.google.com/webstore/detail/screen-capturing/ajhifddimkapgcifgcodmmfdlknahffk">Please install this chrome extension then reload the page.</a></h1>');
            }

            if (error === 'permission-denied') {
                alert('Screen capturing permission is denied.');
            }

            if (error === 'installed-disabled') {
                alert('Please enable chrome screen capturing extension.');
            }

            if (error) {
                config.onMediaCapturingFailed(error);
                return;
            }

            captureUserMedia(screenConstraints, function (screenStream) {
                recordingPlayer.srcObject = screenStream;
                recordingPlayer.play();

                config.onMediaCaptured(screenStream);

                screenStream.onended = function () {
                    config.onMediaStopped();
                };
            }, function (error) {
                config.onMediaCapturingFailed(error);
            });
        });
    }

    function captureAudioPlusScreen(config) {
        getScreenId(function (error, sourceId, screenConstraints) {
            if (error === 'not-installed') {
                document.write('<h1><a target="_blank" href="https://chrome.google.com/webstore/detail/screen-capturing/ajhifddimkapgcifgcodmmfdlknahffk">Please install this chrome extension then reload the page.</a></h1>');
            }

            if (error === 'permission-denied') {
                alert('Screen capturing permission is denied.');
            }

            if (error === 'installed-disabled') {
                alert('Please enable chrome screen capturing extension.');
            }

            if (error) {
                //alert('hhhhhhh');
                config.onMediaCapturingFailed(error);
                return;
            }

            screenConstraints.audio = true;

            captureUserMedia(screenConstraints, function (screenStream) {
                recordingPlayer.srcObject = screenStream;
                recordingPlayer.play();

                config.onMediaCaptured(screenStream);

                screenStream.onended = function () {
                    config.onMediaStopped();
                };
            }, function (error) {
              //  alert('ffff');
                config.onMediaCapturingFailed(error);
            });
        });
    }

    function captureUserMedia(mediaConstraints, successCallback, errorCallback) {
        navigator.mediaDevices.getUserMedia(mediaConstraints).then(successCallback).catch(errorCallback);
    }

    function setMediaContainerFormat(arrayOfOptionsSupported) {
        var options = Array.prototype.slice.call(
                mediaContainerFormat.querySelectorAll('option')
                );

        var selectedItem;
        options.forEach(function (option) {
            option.disabled = true;

            if (arrayOfOptionsSupported.indexOf(option.value) !== -1) {
                option.disabled = false;

                if (!selectedItem) {
                    option.selected = true;
                    selectedItem = option;
                }
            }
        });
    }

    // recordingMedia.onchange = function() {
    //     if(this.value === 'record-audio') {
    //         setMediaContainerFormat(['WAV', 'Ogg']);
    //         return;
    //     }
    //     setMediaContainerFormat(['WebM', 'Mp4', 'Gif']);
    // };

    // if(webrtcDetectedBrowser === 'edge') {
    //     // webp isn't supported in Microsoft Edge
    //     // neither MediaRecorder API
    //     // so lets disable both video/screen recording options

    //     console.warn('Neither MediaRecorder API nor webp is supported in Microsoft Edge. You cam merely record audio.');

    //     recordingMedia.innerHTML = '<option value="record-audio">Audio</option>';
    //     setMediaContainerFormat(['WAV']);
    // }

    // if(webrtcDetectedBrowser === 'firefox') {
    //     // Firefox implemented both MediaRecorder API as well as WebAudio API
    //     // Their MediaRecorder implementation supports both audio/video recording in single container format
    //     // Remember, we can't currently pass bit-rates or frame-rates values over MediaRecorder API (their implementation lakes these features)

    //     recordingMedia.innerHTML = '<option value="record-audio-plus-video">Audio+Video</option>'
    //                                 + '<option value="record-audio-plus-screen">Audio+Screen</option>'
    //                                 + recordingMedia.innerHTML;
    // }

    // disabling this option because currently this demo
    // doesn't supports publishing two blobs.
    // todo: add support of uploading both WAV/WebM to server.
    // if(false && webrtcDetectedBrowser === 'chrome') {
    //     recordingMedia.innerHTML = '<option value="record-audio-plus-video">Audio+Video</option>'
    //                                 + recordingMedia.innerHTML;
    //     console.info('This RecordRTC demo merely tries to playback recorded audio/video sync inside the browser. It still generates two separate files (WAV/WebM).');
    // }

    function saveToDiskOrOpenNewTab(recordRTC) {
        recordingDIV.querySelector('#save-to-disk').parentNode.style.display = 'block';
        recordingDIV.querySelector('#save-to-disk').onclick = function () {
            if (!recordRTC)
                return alert('No recording found.');

            recordRTC.save();
        };

        recordingDIV.querySelector('#open-new-tab').onclick = function () {
            if (!recordRTC)
                return alert('No recording found.');

            window.open(recordRTC.toURL());
        };

        recordingDIV.querySelector('#upload-to-server').disabled = false;
        recordingDIV.querySelector('#upload-to-server').onclick = function () {
            if (!recordRTC)
                return alert('No recording found.');
            this.disabled = true;

            var button = this;
            uploadToServer(recordRTC, function (progress, fileURL) {
                if (progress === 'ended') {
                    button.disabled = false;
                    button.innerHTML = 'Click to download from server';
                    button.onclick = function () {
                        window.open(fileURL);
                    };
                    return;
                }
                button.innerHTML = progress;
            });
        };
    }

    var listOfFilesUploaded = [];

    function uploadToServer(recordRTC, callback) {

        var blob = recordRTC instanceof Blob ? recordRTC : recordRTC.blob;
        var fileType = blob.type.split('/')[0] || 'audio';
        var fileName = (Math.random() * 1000).toString().replace('.', '');

        if (fileType === 'audio') {
            fileName += '.' + (!!navigator.mozGetUserMedia ? 'ogg' : 'wav');
        } else {
            fileName += '.mp4';
        }

        // create FormData
        var formData = new FormData();
        formData.append(fileType + '-filename', fileName);
        formData.append(fileType + '-blob', blob);

        var ques = $("#question_id").val();
        formData.append('question_id', ques);
        // callback('Uploading ' + fileType + ' recording to server.');

        makeXMLHttpRequest('<?php echo $this->createUrl('users/interviewVideo', array('id' => $jamodel->id)) ?>', formData, function (progress) {

            if (progress !== 'upload-ended') {
                callback(progress);
                return;
            }
            
            if(progress === 'upload-ended'){
                $('.next-question').removeAttr('disabled');
            }

            //var initialURL = location.href.replace(location.href.split('/').pop(), '') + 'uploads/';

            callback('ended', initialURL + fileName);

            // to make sure we can delete as soon as visitor leaves
            // listOfFilesUploaded.push(initialURL + fileName);
        });
    }

    function makeXMLHttpRequest(url, data, callback) {
        var request = new XMLHttpRequest();
        request.onreadystatechange = function () {
            if (request.readyState == 4 && request.status == 200) {
                callback('upload-ended');
//                      callback(request.response);
            }
        };

        request.upload.onloadstart = function () {
            callback('Upload started...');
        };

        request.upload.onprogress = function (event) {
            callback('Upload Progress ' + Math.round(event.loaded / event.total * 100) + "%");
        };

        request.upload.onload = function () {
            callback('progress-about-to-end');
        };

        request.upload.onload = function () {
            callback('progress-ended');
        };

        request.upload.onerror = function (error) {
            callback('Failed to upload to server');
            console.error('XMLHttpRequest failed', error);
        };

        request.upload.onabort = function (error) {
            callback('Upload aborted.');
            console.error('XMLHttpRequest aborted', error);
        };

        request.open('POST', url);
        request.send(data);
    }

    window.onbeforeunload = function () {
        recordingDIV.querySelector('button').disabled = false;
        // recordingMedia.disabled = false;
        //   mediaContainerFormat.disabled = false;

        if (!listOfFilesUploaded.length)
            return;

        listOfFilesUploaded.forEach(function (fileURL) {
            var request = new XMLHttpRequest();
            request.onreadystatechange = function () {
                if (request.readyState == 4 && request.status == 200) {
                    if (this.responseText === ' problem deleting files.') {
                        alert('Failed to delete ' + fileURL + ' from the server.');
                        return;
                    }

                    listOfFilesUploaded = [];
                    alert('You can leave now. Your files are removed from the server.');
                }
            };
            request.open('POST', 'delete.php');

            var formData = new FormData();
            formData.append('delete-file', fileURL.split('/').pop());
            request.send(formData);
        });

        return 'Please wait few seconds before your recordings are deleted from the server.';
    };
</script>
<script src="https://cdn.webrtc-experiment.com/commits.js" async></script>
<script>
var seconds = <?php echo ($duration > 0 ? ($duration * 60) : 0) ?>;
function secondPassed() {
    var minutes = Math.round((seconds - 30)/60),
        remainingSeconds = seconds % 60;

    if (remainingSeconds < 10) {
        remainingSeconds = "0" + remainingSeconds;
    }

    document.getElementById('countdown').innerHTML = minutes + ":" + remainingSeconds;
    if (seconds == 0) {
    	
    	clearInterval(countdownTimer);
    	
    	$.ajax({
            url: '<?php echo $this->createUrl('jobApplications/checkLastQuestion',array('id'=>$jamodel->job_id)); ?>',
            type: 'POST',
            success: function (resp) {
            	if(resp == 1)
            	{ 
                	$('#ThankModal').addClass('in');
                    $('body').addClass('modal-open');
                    $('#ThankModal').show();
            	}
            	else
            	{
            		window.location = "<?php echo $this->createUrl("users/skipAnswer", array('id' => $jamodel->id)); ?>"
            	}
            },
            error: function (data) {
                alert('err');
            }
        });
    	
        
    } else {
        seconds--;
    }
}
if(seconds > 0)
	var countdownTimer = setInterval('secondPassed()', 1000);
	
	
$('body').on('click', '#thank-you', function (e) {
	
    	$.ajax({
            url: '<?php echo $this->createUrl('jobApplications/updatePendingAnswers',array('id'=>$jamodel['id'])); ?>',
            type: 'POST',
            success: function (response) {
            	 window.location = "<?php echo $this->createUrl('users/Edit',array('slug'=>$pmodel->slug)); ?>"
            },
            error: function (data) {
                alert('err');
            }
        });
    	
   

});
</script>