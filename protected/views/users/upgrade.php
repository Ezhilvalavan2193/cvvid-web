<div class="page-title text-left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>   
                    Upgrade my Membership
                </h1>
            </div>
        </div>
    </div>
</div>
<div id="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <div class="payment-form-container">
                    <div class="payment-inner">
                      
                        <form method="POST" action="<?php echo $this->createUrl('users/upgradestore',array('target'=>$target)) ?>" accept-charset="UTF-8" class="payment-form">
                            <div class="row">
                                <div class="col-md-offset-1 col-md-10">
                                    <div class="row">
                                      <?php
                                     /* $monplans[] = Membership::plan('Monthly Subscription', 'premium_monthly')
                                                ->price(25)
                                                ->price_suffix('per month')
                                                ->description('Billed Monthly')
                                                ->monthly()
                                                ->permissions([
                                                    'send_messages' => 'Send messages',
                                                    'apply_jobs' => 'Apply for jobs',
                                                    'view_favourites' => 'View those who have favourited you',
                                                    'view_views' => 'View those who have viewed your profile'
                                                ]);
                                     // echo var_dump($plans);
                                      foreach($monplans as $plan){ ?>
                                        <div class="col-md-6">
                                            <div class="subscription-option <?php echo ($plan->id == 'premium_monthly' ? 'selected' : ''); ?>">
                                                <div class="option-title"><?php echo $plan->name  ?></div>
                                                <div class="option-price">£ <?php echo $plan->price  ?><small><?php echo $plan->price_suffix  ?></small></div>
                                                <div class="option-description"><?php echo $plan->description  ?></div>
                                                <input type="radio" name="subscription_plan" value="<?php echo $plan->id  ?>"
                                                    <?php echo ($plan->id == 'premium_monthly' ? 'checked' : ''); ?>/>
                                            </div>
                                        </div>
                                       <?php } */ ?>
                                        
                                        <?php
                                      $annplans[] =  Membership::plan('Free Annual Subscription', 'free_premium_annual')
                                            ->price(0)
                                            ->price_suffix('per year')
                                            ->description('Save 100%')
                                            ->permissions([
                                                'send_messages'     => 'Send messages',
                                                'apply_jobs'        => 'Apply for jobs',
                                                'view_favourites'   => 'View those who have favourited you',
                                                'view_views'        => 'View those who have viewed your profile'
                                            ]);
                                     // echo var_dump($plans);
                                      foreach($annplans as $plan){ ?>
                                        <div class="col-md-6" style="float: none;margin: auto;">
                                            <div class="subscription-option selected">
                                                <div class="option-title"><?php echo $plan->name  ?></div>
                                                <div class="option-price">£ <?php echo $plan->price  ?><small><?php echo $plan->price_suffix  ?></small></div>
                                                <div class="option-description"><?php echo $plan->description  ?></div>
                                                <input type="radio" name="subscription_plan" checked value="<?php echo $plan->id  ?>"/>
                                            </div>
                                        </div>
                                       <?php } ?>
                                    </div>
                                </div>
                            </div><!-- .row -->
                            <div class="row">
                                <div class="col-md-offset-1 col-md-10">
                                    <div class="payment-details-container">

                                        <div class="row">
                                            <div class="col-md-offset-2 col-md-8">

                                                <span class="payment-errors"></span>

                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="selected-subscription">
                                                            <div class="selected-subscription-price"></div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <div class="input-view">
                                                                <input type="tel" size="20" class="form-control cc-name"
                                                                       placeholder="Cardholder Name" autocomplete="off"/>
                                                                <span class="card-icon"></span>
                                                                <div class="view-icon">
                                                                    <i class="fa fa-pencil-square-o"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!-- .row -->

                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <div class="input-view">
                                                                <input type="tel" size="20" class="form-control cc-number" data-stripe="number"
                                                                       placeholder="Card Number" autocomplete="off"/>
                                                                <span class="card-icon"></span>
                                                                <div class="view-icon">
                                                                    <i class="fa fa-credit-card"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!-- .row -->

                                                <div class="row">
                                                    <div class="col-xs-7">
                                                        <div class="form-group form-expiration">
                                                            <span>Expiry</span>
                                                            <input id="cc-exp" type="tel" size="10" class="form-control cc-exp" name="card_expiry" autocomplete="cc-exp"
                                                                   placeholder="•• / ••" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-5">
                                                        <div class="form-group">
                                                            <div class="input-view cvc-input">
																<span>CVC</span>
                                                                <input type="tel" size="4" class="form-control cc-cvc" data-stripe="cvc"
                                                                        autocomplete="off"/>
<!--                                                                 <div class="view-icon"> -->
<!--                                                                     <i class="fa fa-lock"></i> -->
<!--                                                                 </div> -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button type="submit" class="submit-button btn btn-block main-btn">Submit Payment</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div><!-- .row -->

                                    </div><!-- .payment-details-container -->
                                </div>
                            </div><!-- .row -->
                           
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    CVPayment.init('<?php echo Yii::app()->params['PUBLIC_Key']; ?>');
</script>