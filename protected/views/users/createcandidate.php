<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
    'Candidates'=>array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
");
?>
<script>
$(document).ready(function() {
    $(".noSpace").keydown(function(event) {

     if (event.keyCode == 32) {

         event.preventDefault();

     }

  });

//     $('.noSpace').keyup(function() {
//  this.value = this.value.replace(/\s/g,'');
// });


});
</script>
<h1>Create Candidate User</h1>
<div class="Admin__content__inner">
      <?php 
       
        $form = $this->beginWidget('CActiveForm', array(
                    	'id'=>'create-candidate-form',
                        'action'=>$this->createUrl('users/createCandidate',array('trial'=>$trial,'paid'=>$paid,'basic'=>$basic)),
                    	'enableAjaxValidation'=>false,
                        'htmlOptions' => array('enctype' => 'multipart/form-data')
                ));         
        
    ?>
    <?php echo  $form->errorSummary(array($model,$profilemodel,$addressmodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
    <div class="row">
        <div class="col-sm-3 col-md-2">
            <div class="form-group">
                <label for="photo_id" class="control-label">Profile Photo</label>
                <div class="photo-section">
                    <img src="" alt="" id="profile-pic" class="img-responsive">
                     <input type="hidden" name="photo_id" id="photo_id" value="">
                    <button type="button" class="add-media btn btn-primary btn-xs" id="photo-btn">Add Logo</button>            
                    <button type="button" class="remove-media btn btn-primary btn-xs" id="remove-photo-btn" style="display: none">Remove Logo</button>
                </div>
            </div>
        </div>
        <div class="col-sm-9 col-md-10">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Account Details</h3>
                </div>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="forenames" class="control-label">Forenames</label>
                                <?php echo $form->textField($model,'forenames',array('class'=>'form-control noSpace','placeholder'=>"Forenames")); ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="surname" class="control-label">Surname</label>
                                <?php echo $form->textField($model,'surname',array('class'=>'form-control noSpace','placeholder'=>'')); ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="email" class="control-label">Email</label>
                                <?php echo $form->emailField($model,'email',array('class'=>'form-control','placeholder'=>'Email')); ?>
                            </div>
                        </div>
                    </div><!-- .row -->

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="password" class="control-label">Password</label>
                                <?php echo $model->password = "";?>
                                <?php echo $form->passwordField($model,'password',array('class'=>'form-control','placeholder'=>'Password')); ?>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="password_confirmation" class="control-label">Confirm Password</label>
                                <input class="form-control" placeholder="Confirm Password" type="password" value="" name="Users[confirmpassword]" id="confirmpassword">
                            </div>
                        </div>
                    </div><!-- .row -->

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="profile[slug]" class="control-label">Link</label>
                                <div class="input-group slug-field">
                                    <span class="input-group-addon"><?php echo "http://" . $_SERVER['SERVER_NAME'] . "/" ?></span>
                                    <?php echo $form->textField($profilemodel, 'slug', array('class' => 'form-control noSpace', 'id' => 'slug', 'placeholder' => 'Slug')); ?>
                                    <span class="input-group-addon help-block"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                </div><!-- .panel-body -->
            </div>
            
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Details</h3>
                </div>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="dob" class="control-label">Date of Birth</label>
                                <div class="input-group">
                                    <input type="text" name="dob" class="dob form-control" value="<?php echo ((empty($model->dob) || $model->dob == null || $model->dob == "0000-00-00") ? "" : date("d/m/Y", strtotime($model->dob))); ?>">
                                </div>
                            </div>
                        </div><!-- .col-sm-6 -->
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="tel" class="control-label">Telephone</label>
                                <?php echo $form->textField($model,'tel',array('class'=>'form-control','placeholder'=>'Telephone')); ?>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="mobile" class="control-label">Mobile</label>
                                <?php echo $form->textField($model,'mobile',array('class'=>'form-control','placeholder'=>'Mobile')); ?>
                            </div>
                        </div><!-- .col-sm-6 -->
                    </div><!-- .row -->

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="current_job" class="control-label">Current Job</label>
                                <?php echo $form->textField($model,'current_job',array('class'=>'form-control','placeholder'=>'Current Job')); ?>
                            </div>
                        </div>
                    </div>
                     <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="current_job" class="control-label">Gender</label>
                                <div class="input-group">
                                 <?php 
                                    $model->gender = 'M';
                                    $accountStatus = array('M'=>'Male', 'F'=>'Female');
                                    echo $form->radioButtonList($model, 'gender',$accountStatus,array('separator'=>' ')); ?>     
                                    </div>                          
                            </div>
                        </div>
                    </div>
                </div><!-- .panel-body -->
            </div>
            
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Address</h3>
                </div>
                <div class="panel-body">
                    <div id="location-fields">

                      <?php $this->renderPartial('//addresses/address-form', array('form'=>$form,'addressmodel'=>$addressmodel)); ?>

                    </div>

                </div>
            </div>
            
            <div class="form-actions clearfix">
                <?php echo CHtml::submitButton('Save',array("class"=>"btn btn-primary pull-right")); ?>                	
            </div>
            
        </div>
    </div>
    
      <?php $this->endWidget(); ?>
</div>
 <script type="text/javascript">
    $(function(){
        new CandidateSlugGenerator();
    });
</script>
