<?php            
      if($userid > 0)
      {
      	$model = Users::model()->findByPk($userid);
      	$ownerid = $userid;
      	$type = $model->type;
      }
      else if($institutionid > 0)
      {
          $ownerid = $institutionid;
          $type = "institution";
      }
      else if($agencyid > 0)
      {
          $ownerid = $agencyid;
          $type = "agency";
      }
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$ownerid,'type'=>$type));
      	$mediamodel = Media::model()->findByPk($profilemodel->cover_id);
           	
      	if($mediamodel != null && $mediamodel->file_name != null)
  	        $src = Yii::app()->baseUrl.'/images/media/'.$profilemodel->cover_id.'/conversions/cover.jpg';
  	    else
  	        $src = Yii::app()->theme->baseUrl.'/images/hero-banner-2.jpg';
      	        
      	 echo "<section class='cover-photo' id='cover-sec'>";
      	
	     echo "<div class='cover-photo' id='img-cover' style='background-image: url(".$src.");'></div>";
	     
	     if($edit == 1)
	     {
      		 echo '<input class="cover-upload" id="'.$profilemodel->id.'" type="file" style="display:none;"/>';
      		 echo '<a href="#" id="cover-photo-upload" style="display: inline-block;position: absolute;top: 10px;left:20px"><i class="fa fa-camera"></i> Change Cover Photo</a>';
	     }
         echo '</section>';
 ?>
           	    