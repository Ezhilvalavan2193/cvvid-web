<?php
$docmodel = UserDocuments::model()->findAllByAttributes(array('user_id'=>$userid));
 
foreach ($docmodel as $doc) {
	$categmodel = MediaCategories::model()->findByPk($doc['category_id']);
?>
<div class="document-item-inner" style="display: block;">
     <div class="row">
 
			<div class="col-sm-5">
					<label class="document-label">Title:</label>
					<span class="document-title"><?php echo $doc['name'] ?></span>
			</div>
	        <div class="col-sm-5">
	            <label class="document-label">Category:</label>
	            <span class="document-category"><?php echo $categmodel->name ?></span>
	        </div>
	        <div class="col-sm-2">
	            <div class="document-options">
	                <ul class="list-inline">
	                    <li><a href="#" class="edit-doc" id="<?php echo $doc['id'] ?>">Edit</a></li> | 
	                    <li><a href="#" class="delete-doc" id="<?php echo $doc['id'] ?>">Delete</a></li>
	                </ul>
	            </div>
	        </div>
    </div>
</div>
<?php } ?>