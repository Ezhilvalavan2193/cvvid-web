<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */

$this->breadcrumbs=array(
	'Institutions'=>array('index'),
	'Manage',
);

// $this->menu=array(
// 	array('label'=>'List Institutions', 'url'=>array('index')),
// 	array('label'=>'Create Institutions', 'url'=>array('create')),
// );

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#inst-admin-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<script>
$(document).ready(function(){
	$('.ConversationListItem').click(function()
	{
		
		var conv_id = $(this).find('#conversation_id').val();
		var userid = $(this).find('#user_id').val();
		$('.ConversationListItem').removeClass('active');
		$(this).addClass('active');
		
		$.ajax({
	            url: '<?php echo $this->createUrl('users/getInboxMessages') ?>',
	            type: 'POST',
	            data: {id : conv_id , msg_to : userid}, 	    
	            success: function(data) {
		            $('.ConversationInner').html(data);
	            },
			    error: function(data) {		
			        alert('err');
			    }
	         });  
	});
	
	$('#Conversation').on('click', '#send_msg', function (e) 
	{
		var userid = $('#ConversationList').find('.active').find('#user_id').val();
		var conv_id =$('#ConversationList').find('.active').find('#conversation_id').val();
		var msg = $('#message').val();
		$('.ConversationLoader').show();
		$.ajax({
            url: '<?php echo $this->createUrl('users/saveMessage') ?>',
            type: 'POST',
            data: {id : conv_id , msg_to : userid , message : msg }, 	    
            success: function(data) {
	            $('.ConversationInner').html(data);
	            $('.ConversationLoader').hide();
            },
		    error: function(data) {		
		        alert('err');
		    }
         });  
	});
});

</script>
<div class="page-title text-left">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h1>Messages</h1>
                    </div>
                </div>
            </div>
</div>
<div id="page-content">
<div class="container">
<div id="Mailbox"> 
<div class = "row">


<?php 
     $cri = new CDbCriteria();
     $cri->condition = "user_id =".$model->id;
     $cri->group = "conversation_id";
     $cri->order = "created_at desc";
     $convmembers = ConversationMembers::model()->findAll($cri);
?>
<div class="col-sm-4 col-md-4 col-lg-4">
<div id="ConversationList">
	<?php 
	       $conversationid = 0;
	       $msg_to = 0;
	       foreach ($convmembers as $mem)
            {
                $crit = new CDbCriteria();
                $crit->condition = "conversation_id = ".$mem['conversation_id']." and user_id not in (".$model->id.")";
                $convmem = ConversationMembers::model()->find($crit);
                $created = date_format(new DateTime($convmem['created_at']),"Y-m-d");

                $conv = Conversations::model()->findByPk($convmem['conversation_id']);
                
                $msgcri = new CDbCriteria();
                $msgcri->condition = "conversation_id = ".$mem['conversation_id']." and created_at ='".$mem['created_at']."'";              
                $convmsg = ConversationMessages::model()->find($msgcri);
                
                if($conversationid == 0)
                {
                    $conversationid = $convmsg['conversation_id'];
                    $msg_to = $convmem['user_id'];
                    $class = "active";
                }
                else
                {
                    $class = "";
                }
                
                echo '<div class="ConversationListItem '.$class.'">';
                echo CHtml::hiddenField('conversation_id',$convmsg['conversation_id'],array('id'=>'conversation_id'));
                echo CHtml::hiddenField('user_id',$convmem['user_id'],array('id'=>'user_id'));
                echo '<div class="ConversationItem__meta clearfix">';                
                
                echo '<div class="ConversationItem__user pull-left">'.$convmem['name'].'</div>';                
                
                echo '<div class="ConversationItem__date pull-right">'.$created.'</div>';
               
                echo '</div>';
                
                echo '<div class="ConversationItem__subject"><label>Subject:</label> '.$conv->subject.'</div>';
                
                echo '<div class="ConversationItem__message">'.$convmsg['message'].'</div>';
                
                echo '</div>';
                
            }
    ?>
</div>
</div>
<div class="col-dm-8 col-md-8 col-lg-8">


<div id="Conversation">

<?php  if ($conversationid > 0) { ?>
                        <div class="ConversationInner">
                            <?php
                           
                                $convmodel = Conversations::model()->findByPk($conversationid);
                                $this->renderPartial('inbox_messages', array('convmodel' => $convmodel, 'to_user' => $msg_to));
                           
                            ?>
                        </div>
<?php } else { ?>
                  <div class="alert alert-warning">You currently don't have any messages in your inbox.</div>      
<?php } ?>

</div>
</div>
</div>
</div>
</div>