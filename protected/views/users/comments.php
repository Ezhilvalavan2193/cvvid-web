<?php
  $cri = new CDbCriteria();
  $cri->condition = "candidate_id =".$model->id."";
  $cri->order='created_date DESC';
  $commentlist = Comments::model()->findAll($cri);
  
if (count($commentlist) > 0):
    foreach ($commentlist as $comment) {
        $emp_model = Employers::model()->findByPk($comment['employer_id']);
        $usr_model = Users::model()->findByPk($emp_model->user_id);
        $profile_model = Profiles::model()->findByAttributes(array('owner_id' => $usr_model->id, 'type' => $usr_model->type));
        if ($profile_model->photo_id > 0) {
            $mediamodel = Media::model()->findByPk($profile_model->photo_id);
            $src = ((!empty($mediamodel->file_name) && $mediamodel->file_name != null) ? Yii::app()->baseUrl . "/images/media/" . $profile_model->photo_id . "/" . $mediamodel->file_name : Yii::app()->baseUrl . "/images/profile.png");
        } else {
            $src = Yii::app()->baseUrl . "/images/defaultprofile.jpg";
        }
        ?>
<div class="clearfix">
        <div class="col-sm-1">
            <div class="thumbnail">
                <img class="img-responsive user-photo" src="<?php echo $src; ?>">
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <strong><?php echo $usr_model->forenames . " " . $usr_model->surname; ?></strong> <span class="text-muted"> commented <?php echo timeAgo($comment['created_date']); ?></span>
                </div>
                <div class="panel-body">
                    <?php echo $comment['comment']; ?>
                </div>
            </div>
        </div>
    </div>
        <?php
    }
endif;
?>

    <?php

function timeAgo($time_ago)
{
    $time_ago = strtotime($time_ago);
    $cur_time   = time();
    $time_elapsed   = $cur_time - $time_ago;
    $seconds    = $time_elapsed ;
    $minutes    = round($time_elapsed / 60 );
    $hours      = round($time_elapsed / 3600);
    $days       = round($time_elapsed / 86400 );
    $weeks      = round($time_elapsed / 604800);
    $months     = round($time_elapsed / 2600640 );
    $years      = round($time_elapsed / 31207680 );
    // Seconds
    if($seconds <= 60){
        return "just now";
    }
    //Minutes
    else if($minutes <=60){
        if($minutes==1){
            return "one minute ago";
        }
        else{
            return "$minutes minutes ago";
        }
    }
    //Hours
    else if($hours <=24){
        if($hours==1){
            return "an hour ago";
        }else{
            return "$hours hrs ago";
        }
    }
    //Days
    else if($days <= 7){
        if($days==1){
            return "yesterday";
        }else{
            return "$days days ago";
        }
    }
    //Weeks
    else if($weeks <= 4.3){
        if($weeks==1){
            return "a week ago";
        }else{
            return "$weeks weeks ago";
        }
    }
    //Months
    else if($months <=12){
        if($months==1){
            return "a month ago";
        }else{
            return "$months months ago";
        }
    }
    //Years
    else{
        if($years==1){
            return "one year ago";
        }else{
            return "$years years ago";
        }
    }
}
?>
