<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs = array(
    'Users' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
    
");
?>
<?php $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $model->id, 'type' => $model->type)); ?>
<script>
$(document).ready(function(){
	$('#profile-documents').on('click','.add-new-doc,.new-doc-cancel',function(){
		$('.new-document-form').slideToggle( "slow" );
		return false;
	});

	$('#profile-documents').on('click','.edit-doc',function(){
		var id = $(this).attr('id');
	 	var name = 'edit-document-form-'+id;
	 	$('.'+name).toggle();
		return false;
	});

	$('#profile-documents').on('submit','#new-doc-form',function(e){
		 e.preventDefault();	
		var formdata = new FormData(this);
		 $.ajax({
	            url: '<?php echo $this->createUrl('users/saveUserDoc')?>',
	            type: 'POST',
		        data: formdata,
				contentType: false,       
				cache: false,             
				processData:false,
	            success: function(data) {
						$('#profile-documents').html(data);	
	            },
			    error: function(data) {		
			      //  alert(data);
			    }
	        });			
		
	   return false;
   });
	   
	$('#profile-documents').on('submit','#edit-doc-form',function(e){
		 e.preventDefault();	
		 var formdata = new FormData(this);
		 $.ajax({
	            url: '<?php echo $this->createUrl('users/updateUserDoc')?>',
	            type: 'POST',
		        data: formdata,
				contentType: false,       
				cache: false,             
				processData:false,
	            success: function(data) {
		            $('#profile-documents').html(data);	
	            },
			    error: function(data) {		
			        alert(data);
			    }
	        });			
		
	     return false;
    });

	$('#profile-documents').on('click','.doc_edit_cancel',function(e){		
		 var id = $(this).attr('id');
		 $('.edit-document-form-'+id).toggle();
	});
	
	$('#profile-documents').on('click','.delete-doc',function(e){		
		 var id = $(this).attr('id');
		 e.preventDefault();
		 $.ajax({
	            url: '<?php echo $this->createUrl('users/deleteUserDoc')?>',
	            type: 'POST',
		        data: {id : id},
	            success: function(data) {
		            $('#profile-documents').html(data);	
	            },
			    error: function(data) {		
			        alert(data);
			    }
	        });
	 });

	
	$('#professional-experience').on('click','.add-new-exp,.new_exp_cancel',function(){
		$('.new-experience-form').slideToggle('slow');
		return false;
	});

	$('#professional-experience').on('click','.edit-exp,.exp_edit_cancel',function(){
		var id = $(this).attr('id');
	 	var name = 'experience-form-'+id;
	 	$('.'+name).slideToggle('slow');
	 	$('.experience-inner-'+id).slideToggle('slow');
		return false;
	});

	$('#professional-experience').on('submit','#new-exp-form',function(e){
		
		 e.preventDefault();	
		 
		var formdata = new FormData(this);
		 $.ajax({
	            url: '<?php echo $this->createUrl('users/saveUserExp')?>',
	            type: 'POST',
		        data: formdata,
		        contentType: false,       
				cache: false,             
				processData:false,
	            success: function(data) {
		          		$('#professional-experience').html(data);	
	            },
			    error: function(data) {		
			        alert(data);
			    }
	        });			
		
	   return false;
   });
	   
	$('#professional-experience').on('submit','#edit-exp-form',function(e){
		 e.preventDefault();	
		 var formdata = new FormData(this);
		 $.ajax({
	            url: '<?php echo $this->createUrl('users/updateUserExp')?>',
	            type: 'POST',
		        data: formdata,
				contentType: false,       
				cache: false,             
				processData:false,
	            success: function(data) {
		            $('#professional-experience').html(data);	
	            },
			    error: function(data) {		
			        alert(data);
			    }
	        });			
		
	     return false;
    });

	$('#professional-experience').on('click','.exp_edit_cancel',function(e){		
		 var id = $(this).attr('id');
		 $('.edit-exp-form-'+id).toggle();
	});
	
	$('#professional-experience').on('click','.exp-delete',function(e){ 		
		 var id = $(this).attr('id');
		 e.preventDefault();
		 $.ajax({
	            url: '<?php echo $this->createUrl('users/deleteUserExp')?>',
	            type: 'POST',
		        data: {id : id},
	            success: function(data) {
		            $('#professional-experience').html(data);	
	            },
			    error: function(data) {		
			        alert(data);
			    }
	        });
	 });


	$('#profile-education').on('click','.add-new-edu,.new-edu-cancel',function(){
		$('.new-education-form').slideToggle('slow');
		return false;
	});

	$('#profile-education').on('click','.edit-edu',function(){
		
                var id = $(this).attr('id');
	 	var name = 'edit-education-form-'+id;
	 	$('.'+name).slideToggle('slow');
	 	$('.education-item-inner-'+id).slideToggle('slow');
		return false;
	});

	$('#profile-education').on('submit','#new-edu-form',function(e){
		
		 e.preventDefault();	
		 
		var formdata = new FormData(this);
		 $.ajax({
	            url: '<?php echo $this->createUrl('users/saveUserEducation')?>',
	            type: 'POST',
		        data: formdata,
		        contentType: false,       
				cache: false,             
				processData:false,
	            success: function(data) {
		          		$('#profile-education').html(data);	
	            },
			    error: function(data) {		
			        alert(data);
			    }
	        });			
		
	   return false;
   });
	   
	$('#profile-education').on('submit','#edit-edu-form',function(e){
		 e.preventDefault();	
		 var formdata = new FormData(this);
		 $.ajax({
	            url: '<?php echo $this->createUrl('users/updateUserEducation')?>',
	            type: 'POST',
		        data: formdata,
				contentType: false,       
				cache: false,             
				processData:false,
	            success: function(data) {
		            $('#profile-education').html(data);	
	            },
			    error: function(data) {		
			        alert(data);
			    }
	        });			
		
	     return false;
    });

	$('#profile-education').on('click','.edu_edit_cancel',function(e){		
		 var id = $(this).attr('id');
		 $('.edit-education-form-'+id).slideToggle('slow');
		 $('.education-item-inner-'+id).slideToggle('slow');
	});
	
	$('#profile-education').on('click','.edu-delete',function(e){ 		
		 var id = $(this).attr('id');
		 e.preventDefault();
		 $.ajax({
	            url: '<?php echo $this->createUrl('users/deleteUserEducation')?>',
	            type: 'POST',
		        data: {id : id},
	            success: function(data) {
		            $('#profile-education').html(data);	
	            },
			    error: function(data) {		
			        alert(data);
			    }
	        });
	 });


	$('#profile-hobbies').on('click','.add-new-hobby,.new-hobby-cancel',function(){
		$('.new-hobbies-form').slideToggle('slow');
		return false;
	});

	$('#profile-hobbies').on('click','.edit-hobby',function(){
            
                 var id = $(this).attr('id');
		 $('.hobby-item-inner-'+id).slideToggle('slow');
		 $('.edit-hobby-form-'+id).slideToggle('slow');
		return false;
	});

	$('#profile-hobbies').on('submit','#new-hobby-form',function(e){
		
		 e.preventDefault();	
		 
		var formdata = new FormData(this);
		 $.ajax({
	            url: '<?php echo $this->createUrl('users/saveUserHobbies')?>',
	            type: 'POST',
		        data: formdata,
		        contentType: false,       
				cache: false,             
				processData:false,
	            success: function(data) {
		          		$('#profile-hobbies').html(data);	
	            },
			    error: function(data) {		
			        alert(data);
			    }
	        });			
		
	   return false;
   });
	   
	$('#profile-hobbies').on('submit','#edit-hobby-form',function(e){
		 e.preventDefault();	
		 var formdata = new FormData(this);
		 $.ajax({
	            url: '<?php echo $this->createUrl('users/updateUserHobbies')?>',
	            type: 'POST',
		        data: formdata,
				contentType: false,       
				cache: false,             
				processData:false,
	            success: function(data) {
		            $('#profile-hobbies').html(data);	
	            },
			    error: function(data) {		
			        alert(data);
			    }
	        });			
		
	     return false;
    });

	$('#profile-hobbies').on('click','.hobby_edit_cancel',function(e){		
		 var id = $(this).attr('id');
                 
		 $('.edit-hobby-form-'+id).slideToggle('slow');
		 $('.hobby-item-inner-'+id).slideToggle('slow');
                 return false;
	});
	
	$('#profile-hobbies').on('click','.hobby-delete',function(e){ 		
		 var id = $(this).attr('id');
		 e.preventDefault();
		 $.ajax({
	            url: '<?php echo $this->createUrl('users/deleteUserHobbies')?>',
	            type: 'POST',
		        data: {id : id},
	            success: function(data) {
		            $('#profile-hobbies').html(data);	
	            },
			    error: function(data) {		
			        alert(data);
			    }
	        });
	 });

	$('#profile-languages').on('click','.add-new-lang,.new-lang-cancel',function(){
		$('.new-language-form').slideToggle('slow');
		return false;
	});

	$('#profile-languages').on('click','.edit-lang,.lang_edit_cancel',function(){
		var id = $(this).attr('id');
		 $('.edit-language-form-'+id).slideToggle('slow');
		 $('.language-item-inner-'+id).slideToggle('slow');
	 	
		return false;
	});

	$('#profile-languages').on('submit','#new-lang-form',function(e){
		
		 e.preventDefault();	
		 
		var formdata = new FormData(this);
		 $.ajax({
	            url: '<?php echo $this->createUrl('users/saveUserLanguages')?>',
	            type: 'POST',
		        data: formdata,
		        contentType: false,       
				cache: false,             
				processData:false,
	            success: function(data) {
		          		$('#profile-languages').html(data);	
	            },
			    error: function(data) {		
			        alert(data);
			    }
	        });			
		
	   return false;
   });
	   
	$('#profile-languages').on('submit','#edit-lang-form',function(e){
		 e.preventDefault();	
		 var formdata = new FormData(this);
		 $.ajax({
	            url: '<?php echo $this->createUrl('users/updateUserLanguages')?>',
	            type: 'POST',
		        data: formdata,
				contentType: false,       
				cache: false,             
				processData:false,
	            success: function(data) {
		           $('#profile-languages').html(data);	
	            },
			    error: function(data) {		
			        alert(data);
			    }
	        });			
		
	     return false;
    });

	$('#profile-languages').on('click','.lang_edit_cancel',function(e){		
		 var id = $(this).attr('id');
		 $('.edit-lang-form-'+id).toggle();
	});
	
	$('#profile-languages').on('click','.lang-delete',function(e){ 		
		 var id = $(this).attr('id');
		 e.preventDefault();
		 $.ajax({
	            url: '<?php echo $this->createUrl('users/deleteUserLanguages')?>',
	            type: 'POST',
		        data: {id : id},
	            success: function(data) {
		            $('#profile-languages').html(data);	
	            },
			    error: function(data) {		
			        alert(data);
			    }
	        });
	 });

	$('#profile-skills').on('click','.add-new-skill,.new-skill-cancel',function(){
		$('#skills-form').slideToggle('slow');
		
       	if($(this).attr('class') == 'add-new main-btn add-new-skill')
       	{
			$('.add-new-skill').hide();
			$('.save-new-skill').show();
			
       	}
		return false;
	});

	$('#profile-skills').on('click','.edit-skill',function(){
		var id = $(this).attr('id');
	 	var name = 'edit-skill-form-'+id;
	 	$('.'+name).toggle();
		return false;
	});

	$('#profile-skills').on('click','.skill-category',function(){
		var id = $(this).attr('id');
	 	var name = 'skill-category-inner-'+id;
	 	$('.save-new-skill').attr('id',id);
	 	$('.'+name).slideToggle('slow');
	 	$('#skills-form').slideToggle('slow');
                return false;
	 	//$('.active-category-inner').slideToggle('slow');
	 	
		//return false;		
	});

	$('#profile-skills').on('click','.skill-change',function(){

		var curr = $(this);
		var id = $(this).attr('id');
		var userid = $('#user_id').val();
		var action = "";
	 
                var currclass = $.trim(curr.attr('class'));
                if(currclass == 'skill skill-change'){
                    action = "add";
                }else if(currclass == 'skill skill-change selected'){
                    action = "remove";
                }
             
		 $.ajax({
	            url: '<?php echo $this->createUrl('users/changeUserSkills')?>',
	            type: 'POST',
		    	data: {skill_id : id, user_id : userid, action : action},
	            success: function(data) {
		            $(curr).toggleClass('selected');
	            },
                    error: function(data) {		
                        alert('err');
                    }
	        });
		
		return false;		
	});	
		
	$('#profile-skills').on('click','.save-new-skill,.CategoryEditFinish',function(){
		var id = $(this).attr('id');
		var userid = $('#user_id').val();

		 $.ajax({
	            url: '<?php echo $this->createUrl('users/renderUserSkills')?>',
	            type: 'POST',
		    data: { user_id : userid },
	            success: function(data) {
	            	 $('#profile-skills').html(data);
	            },
                    error: function(data) {		
                        alert('err');
                    }
	        });
       
		
		return false;		
	});	

	$('#profile-skills').on('click','.edit-skill-categ',function(){

		var id = $(this).attr('id');
                
                $('.active-category#'+id+' > .active-category-inner').hide();
		$('.active-category-form-'+id).slideToggle('slow');
		$('.CategoryEditFinish').attr('id',id);
		return false;		
	});

	
	$('.ProfileEdit__operations').on('click','.visiblity',function(e){
		 e.preventDefault();
		 var userid = $('#user_id').val();
		 var status = $.trim($(this).text());
		
		 $.ajax({
	            url: '<?php echo $this->createUrl('users/changeUserVisiblity')?>',
	            type: 'POST',
	            async:false,
		        data: { userid : userid ,status : status },
	            success: function(data) {		           
$(".ProfileEdit__operations").load(location.href + " .ProfileEdit__operations>*", "");
//	       		 if(status == "Public")			
//	                {            	        
//	                	$('.visiblity').text('');    	
//	            		$('.visiblity').append( '<i class="fa fa-lock"></i> Private' );
//	                }
//	                else
//	                {		       
//	                	$('.visiblity').text('');    	     	
//	                	$('.visiblity').append( '<i class="fa fa-unlock"></i> Public' );
//	                }
	       		          
	            },
			    error: function(data) {		
			        alert('err');
			    }
	        });
		return false;		
	});

	$('.ProfileEdit__operations').on('click','.status',function(e){
		 e.preventDefault();
		 var userid = $('#user_id').val();
		 var status = $.trim($(this).text());
		
		 $.ajax({
	            url: '<?php echo $this->createUrl('users/changeUserStatus')?>',
	            type: 'POST',
	            async:false,
		        data: { userid : userid ,status : status },
	            success: function(data) {		           
 $(".ProfileEdit__operations").load(location.href + " .ProfileEdit__operations>*", "");
//	       		 	if(status.toLowerCase() == "active")			
//	                {            	        
//	                	$('.status').text('');    	
//	            		$('.status').append( '<i class="fa fa-times"></i> inactive' );
//	                }
//	                else
//	                {		       
//	                	$('.status').text('');    	     	
//	                	$('.status').append( '<i class="fa fa-check"></i> Active' );
//	                }
	       		          
	            },
			    error: function(data) {		
			        alert('err');
			    }
	        });
		return false;		
	});

	$('.form-profile-videos').on('change','#NewVideo',function(e){
                var id = $('#user_id').val();
                var name = $('#video_name').val();
			
	        var data = new FormData();
	        jQuery.each(jQuery(this)[0].files, function(i, file) {
	            data.append('video', file);
	        });
	        data.append('userid',id);
	        data.append('video_name',name);
	        $('.progress').show();
	        $.ajax({
	            url: '<?php echo $this->createUrl('users/addProfileVideo')?>',
	            type: 'POST',
	            data: data,
	            cache: false,
	            contentType: false,
	            processData: false,   	    
	            success: function(data) {
	            	$('.profile-videos-section').html(data);
                         $('.progress').hide();
	            },
                    error: function(data) {		
                        alert('err');
                    }
	        });

     });

	$('.profile-section-info').on('click','.ProfileVideo__edit',function(e){
		  e.preventDefault();
                  alert('hhh')
		  var id = $(this).attr('id');
                 $('#ProfileVideo-'+id).slideToggle('slow');
//          $('#'+id+'.video-name-label').hide();
	});
	
	$('.profile-videos-section').on('click','.video-edit-close',function(e){		
		  e.preventDefault();
		  var id = $(this).attr('id');
		  $('#'+id+'.video-name-textbox').hide();
          $('#'+id+'.video-name-label').show();
	});
	
	$('.profile-videos-section').on('click','.video-edit-save',function(e){

		var id = $(this).attr('id');
		var name = $('#'+id+'.video-name').val();
		  
		  $.ajax({
	            url: '<?php echo $this->createUrl('users/updateProfileVideoName') ?>',
	            type: 'POST',
	            data: {id:id,name:name}, 	    
	            success: function(data) {
		            $('.profile-videos-section').html(data);
	            },
			    error: function(data) {		
			        alert('err');
			    }
	        });

	});

	$('.profile-videos-section').on('click','.make_default',function(e){

		e.preventDefault();
		var id = $(this).attr('id');
		  
		  $.ajax({
	            url: '<?php echo $this->createUrl('users/profileVideoDefault') ?>',
	            type: 'POST',
	            data: {id:id}, 	    
	            success: function(data) {
		            $('.profile-videos-section').html(data);
	            },
			    error: function(data) {		
			        alert('err');
			    }
	        });

	});

	$('.profile-videos-section').on('click','.video_delete',function(e){

		e.preventDefault();
		var id = $(this).attr('id');
		  
		  $.ajax({
	            url: '<?php echo $this->createUrl('users/profileVideoDelete') ?>',
	            type: 'POST',
	            data: {id:id}, 	    
	            success: function(data) {
		            $('.profile-videos-section').html(data);
	            },
			    error: function(data) {		
			        alert('err');
			    }
	        });

	});

	$('.profile-videos-section').on('click','.video_publish',function(e){

		e.preventDefault();
		var id = $(this).attr('id');
		 var status = $('#'+id+'.video_publish').text();
		 
		  $.ajax({
	            url: '<?php echo $this->createUrl('users/profileVideoPublish') ?>',
	            type: 'POST',
	            data: {id:id, status:status}, 	    
	            success: function(data) {
		            $('.profile-videos-section').html(data);
	            },
			    error: function(data) {		
			        alert('err');
			    }
	        });

	});
	 $('body').on('click', '.open-media', function () {
            var getValue = $(this).attr("data-field");
            if(getValue == 'cover'){ $(".saveMedia").attr('id', 'save-media-cover'); }else if(getValue == 'photo'){ $(".saveMedia").attr('id', 'save-media-photo'); }
            
            //alert(getValue);return false;
            $('#mediamanager_modal').addClass('in');
            $('body').addClass('modal-open');
            $('#mediamanager_modal').show();
        });
        
       
        $('.upload-media').on('click', '.medaiamanager_add', function () {
           //e.preventDefault();
           $(".media-file").trigger('click');
            //return false;
        });

        $('.modal-content').on('change', '.media-file', function (e) {

            var data = new FormData();
            jQuery.each(jQuery(this)[0].files, function (i, file) {
                data.append('media', file);
            });
            data.append('collection_name', 'photos');
            data.append('model_type', "App\\Users\\Candidate");
            data.append('model_id', <?php echo $model->id; ?>);
            // alert(JSON.stringify(data));
            $.ajax({
                url: '<?php echo $this->createUrl('media/insertMediaManager') ?>',
                type: 'POST',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
//                    alert(data);form-cover-photo
                  $(".MediaManager__content").load(location.href + " .MediaManager__content>*", "");
                    //$('#img-media').attr("src", data);
//                    $('#img-media').src('background-image', 'url(' + data + ')');
                },
                error: function (data) {
                    alert('err');
                }
            });

            // return false;
        });

        $('.modal-content').on('click', '.MediaManager__item', function (evt) {
            $('div.selected').removeClass('selected');
            $(this).addClass('selected');
        });

        $('.modal-content').on('click', '#save-media-cover', function (evt) {

            var mediaid = $(".MediaManager__content").find(".selected").attr('id');

            $.ajax({
                url: '<?php echo $this->createUrl('profiles/updateCover') ?>',
                type: 'POST',
                data: {mediaid: mediaid, profileid: <?php echo $profilemodel->id; ?>},
                success: function (data) {
                    window.location.reload();
                    // $("#colcover").load(location.href + " #colcover>*", "");
                    //  closepopup();
                },
                error: function (data) {
                    alert('err');
                }
            });


        });

		$('#likes-pro').on('click',function(){
		$('.likes-pro').toggle();
	});
        
        
        $('.modal-content').on('click', '#save-media-photo', function (evt) {

            var mediaid = $(".MediaManager__content").find(".selected").attr('id');

            $.ajax({
                url: '<?php echo $this->createUrl('profiles/updatePhoto') ?>',
                type: 'POST',
                data: {mediaid: mediaid, profileid: <?php echo $profilemodel->id; ?>},
                success: function (data) {
                    window.location.reload();
                    // $("#colcover").load(location.href + " #colcover>*", "");
                    //  closepopup();
                },
                error: function (data) {
                    alert('err');
                }
            });


        });

});

</script>
<div id="profile">
    <?php
    $employerid = isset($this->employerid) ? $this->employerid : 0;
    $userid = isset($this->userid) ? $this->userid : 0;
    $institutionid = isset($this->institutionid) ? $this->institutionid : 0;
    $edit = isset($this->edit) ? $this->edit : 0;
    $this->renderPartial('//profiles/Cover', array('userid' => $userid, 'employerid' => $employerid, 'institutionid' => $institutionid, 'edit' => $edit));
    ?>
    <div id="form-details">
        <div class="container">
             <?php 
              echo CHtml::hiddenField('user_id',$model->id,array('id'=>'user_id')); 
            ?>
            <div class="form-details-inner">
                <div class="form-user-details form-section">
                    <?php if(Yii::app()->user->hasFlash('success')):?>
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                       <?php echo Yii::app()->user->getFlash('success'); ?>
                    </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="text-center col-sm-4 col-md-3 col-lg-2">
                            <div class="ProfileEdit__photo">
                                <?php
                                if ($profilemodel->photo_id > 0) {
                                    $mediamodel = Media::model()->findByPk($profilemodel->photo_id);
                                    $src = ((!empty($mediamodel->file_name) && $mediamodel->file_name != null) ? Yii::app()->baseUrl . "/images/media/" . $profilemodel->photo_id . "/" . $mediamodel->file_name : Yii::app()->baseUrl . "/images/profile.png");
                                } else {
                                    $src = Yii::app()->baseUrl . "/images/defaultprofile.jpg";
                                }
                                echo CHtml::image($src, "", array("id" => "profile-image", 'alt' => ''));
                                ?>
                                <a class="open-media profile-photo-btn" data-field="photo" data-target="#profile-image">
                                    <i class="fa fa-camera"></i>
                                    Change Photo
                                </a>
                            </div>                    
                            <a class="btn btn-primary" href="<?php echo $this->createUrl('users/profile', array('slug' => $profilemodel->slug)) ?>">
                                <i class="fa fa-search"></i>
                                <span>View Profile</span>
                            </a>
                        </div>
                        <div class="col-sm-5 col-md-5 col-lg-6">
                            <div class="ProfileEdit__details">
                                <h3>
                                    <?php  echo $model->forenames ?> <?php echo $model->surname ?>
                                    <?php
                                    $trail = "";
                                    if ($model->trial_ends_at != null) {
                                        $trail = date('Y-m-d', strtotime($model->trial_ends_at));
                                        $expiry = date('d/m/Y', strtotime($model->trial_ends_at));
                                    }

                                    if(Membership::subscribed($model->stripe_active, $model->trial_ends_at, $model->subscription_ends_at)) {
                                        ?>
                                        - <small class="subscription-marker subscription-marker--premium">Premium Account</small>
                                    <?php } else { ?>
                                        - <small class="subscription-marker subscription-marker--basic">Basic Account</small>
                                    <?php } ?>
                                </h3>
                                <div><?php echo $model->current_job ?></div>
                                <div><?php echo $model->location ?></div>

                                <span class="profile-link">URL: <?php echo "http://" . $_SERVER['SERVER_NAME'] . '/' . $profilemodel->slug ?></span>

                                <ul class="ProfileEdit__details__meta list-inline">
                                    <?php
                                    $pcri = new CDbCriteria();
                                    $pcri->alias = 'pv';
                                    $pcri->join = 'inner join cv16_users u on u.id = pv.model_id';
                                    $pcri->condition = "pv.profile_id = ".$profilemodel->id." and model_type like '%employer%'";
                                    $promodel = ProfileViews::model()->findAll($pcri);
                                    
                                    $fcri = new CDbCriteria();
//                                     $fcri->alias = 'pv';
//                                     $fcri->join = 'inner join cv16_users u on u.id = pv.favourite_id';
                                    $fcri->condition = "favourite_id = ".$profilemodel->id;
                                    $pfmodel = ProfileFavourites::model()->findAll($fcri);
                                    
                                    $lcri = new CDbCriteria();
                                    //$lcri->alias = 'pv';
                                    //$lcri->join = 'inner join cv16_users u on u.id = pv.favourite_id';
                                    $lcri->condition = "like_id = ".$profilemodel->id;
                                    $likemodel = ProfileLikes::model()->findAll($lcri);
                                    
                                    if(Membership::subscribed($model->stripe_active, $model->trial_ends_at, $model->subscription_ends_at)) {                                   
                                        ?>
                                        <li><i class="fa fa-eye"></i><a href="<?php echo $this->createUrl('users/profileViews', array('id' => $profilemodel->id)); ?>"><?php echo count($promodel) ?> Views</a></li>
                                        <li><i class="fa fa-heart"></i><a href="<?php echo $this->createUrl('users/favourites', array('id' => $profilemodel->id)); ?>"><?php echo count($pfmodel)  ?> Favourites</a></li>
                                  		 <li><i class="fa fa-thumb"></i><a href="<?php echo $this->createUrl('users/profileLikes', array('id' => $profilemodel->id)); ?>"><?php echo count($likemodel)  ?> Likes</a></li>
                                    <?php } else {
                                        ?>
                                        <li><i class="fa fa-eye"></i><?php echo count($promodel)  ?> Views</li>
                                        <li><i class="fa fa-heart"></i><?php echo count($pfmodel)  ?> Favourites</li>
                                        <li><i class="fa fa-thumb"></i><?php echo count($likemodel)  ?> Likes</li>
                                    <?php } ?>

                                </ul><!-- .ProfileEdit__details__meta -->
                            </div><!-- .ProfileEdit__details -->               
                        </div>
                        <div class="col-sm-3 col-md-4 col-lg-4">
                            <div class="ProfileEdit__operations">
                                <div>
                                    <?php
                                    $target = Yii::app()->controller->getRoute() . "/" . $model->id;
//                                    free user
                                    if(!Membership::subscribed($model->stripe_active, $model->trial_ends_at, $model->subscription_ends_at))
                                    {
                                    ?>
                                    <a class="btn btn-default btn-upgrade upgrade" href="<?php echo $this->createUrl('users/upgrade', array('target' => $target)) ?>">
                                        <i class="fa fa-user"></i>
                                        <span>Basic</span>
                                    </a>
                                    <?php } else {
                                     if(Membership::onTrail($model->trial_ends_at)){ ?>
                                        <div class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Expires on <?php echo $expiry; ?>">
                                           <i class="fa fa-credit-card"></i>
                                           <span>Premium</span>
                                       </div>
                                      <?php } else { ?>
                                         <a class="btn btn-default" href="<?php echo $this->createUrl('users/mysubscription') ?>">
                                            <i class="fa fa-credit-card"></i>
                                            <span>Account</span>
                                        </a>
                                    <?php } } ?>
                                    
                                    <?php if ($profilemodel->published) { ?>
                                        <a class="Profile__active btn status  btn-success" id="<?php echo $profilemodel->published ?>" href=""><i class="fa fa-check"></i>Active</a>
                                    <?php } else { ?>
                                        <a class="Profile__active btn status btn-default" id="<?php echo $profilemodel->published ?>" href=""><i class="fa fa-close"></i>Inactive</a>
<?php } ?>
                                </div>
                                <div>
                                    <?php
                                   $age = 0;
                                    if($model->dob != null)
                                    {
                                        $from = new DateTime($model->dob);
                                        $to   = new DateTime('today');
                                        $age = $from->diff($to)->y;
                                    }
                                    if($age != 0 && $age <= 16)
                                        $profilemodel->visibility = 0;
                                   if ($profilemodel->visibility) { ?>
                                        <a class="Profile__visibility btn visiblity  btn-success" id="<?php echo $profilemodel->visibility ?>" href="#"><i class="fa fa-unlock"></i>Public</a>
                                    <?php } else { ?>
                                        <a class="Profile__visibility btn visiblity btn-default" id="<?php echo $profilemodel->visibility ?>" href="#"><i class="fa fa-lock"></i>Private</a>
                                    <?php } ?>
                                    <?php // echo $model->stripe_active; ?>
                                    <?php
                                    //if (($model->trial_ends_at != null && Membership::onTrail($trail)) || $model->stripe_active) {

                                        $convmem = ConversationMembers::model()->findAllByAttributes(array('user_id' => $profilemodel->owner_id, 'last_read' => null));
                                        ?>
                                        <a class="btn btn-default" href="<?php echo $this->createUrl('users/inbox', array('id' => $model->id)) ?>">
                                            <i class="fa fa-envelope"></i>
                                            <span>Inbox (<?php echo count($convmem) ?>)</span>
                                        </a>
<?php //} ?>
								
                                </div>



                                <div>
                                    <a class="btn btn-default" href="<?php echo $this->createUrl('users/profile', array('slug' => $profilemodel->slug)) ?>">
                                        <i class="fa fa-user"></i>
                                        <span>View</span>
                                    </a>
                                    <a class="btn btn-default" href="<?php echo $this->createUrl('users/userAccountInfo', array('slug' => $profilemodel->slug)) ?>">
                                        <i class="fa fa-pencil"></i>
                                        <span>Account</span>
                                    </a>
                                </div>
                                <div>
                                    <?php if ($profilemodel->hired) { ?>					
                                                    <a class="btn btn-default hired btn-success" href="#"><i class="fa fa-check"></i><span>Hired</span></a>
                                           <?php } else { ?>	 
                                                    <a class="btn btn-default hired" href="#"><i class="fa fa-close"></i><span>Not Hired</span></a>
                                    <?php } ?>
                                </div>
                                
                                 <div>
                                    <?php 
                                        $jamodel = JobApplications::model()->findByAttributes(array('user_id'=>$model->id,'status'=>2));
                                        if($jamodel != null)
                                        {
                                    ?>	 
    									 <a class="btn btn-default" href="<?php echo $this->createUrl('users/interview', array('id' => $jamodel['id'])) ?>">
                                            <i class="fa fa-video-camera"></i>
                                            <span>Video Interview</span>
                                        </a>
                                    <?php } ?>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-section">
                    <div class="form-section-inner">
                        <h2>Liked by</h2>
                        <div class="favourites-list">
						<?php
                            $favmodel = ProfileLikes::model()->findAllByAttributes(array('like_id' => $profilemodel->id));
                            if (count($favmodel) > 0) {
                                ?> 

                                <div class="row">
                                    <?php
                                    foreach ($favmodel as $fav) {
                                        $pfav = Profiles::model()->findByPk($fav['profile_id']);
                                        if ($pfav->photo_id > 0) {
                                            $mediamodel = Media::model()->findByPk($pfav->photo_id);
                                            $src = ((!empty($mediamodel->file_name) && $mediamodel->file_name != null) ? Yii::app()->baseUrl . "/images/media/" . $pfav->photo_id . "/" . $mediamodel->file_name : Yii::app()->baseUrl . "/images/profile.png");
                                        } else
                                            $src = Yii::app()->baseUrl . "/images/defaultprofile.jpg";
                                        ?>
                                        <div class="favourite-profile">
                                            <div class="col-sm-3">
                                                <img src="<?php echo $src ?>" class="img-responsive">
												<?php 
												$modelo = Profiles::model()->findByAttributes(array('slug'=>$pfav->slug));
        										$emodelo = Employers::model()->findByPk($modelo->owner_id);
												if($emodelo === null){ ?>
												
												<h5 id="likes-pro" style="text-align: center; cursor: pointer;"><?php echo $pfav->slug ?></h5>
												
												<h5 class="likes-pro" style="display:none;">This user doesn't exist</h5>
											<?php	} else { ?>
												<a href="<?php echo $this->createUrl('employers/employer', array('slug' => $pfav->slug)) ?>">
                                                    <h5 style="text-align: center;"><?php echo $pfav->slug ?></h5>
                                                </a>
											<?php }
												?>
                                                
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } else { ?>
                                <p>You currently have not been favourited by any account.</p>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            </div>
        </div><!-- /.container -->
    </div>
</div>

