<?php 
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
");
?> 
<script>
$(document).ready(function(){
	
	$('#slug_field').on('blur', function() {

		 var ele = $(this);
	 	 var value = $(this).val();
	 	 if(value != "")
	 	 {
        	 $.ajax({
                   url: '<?php echo $this->createUrl('users/checkSlug')?>',
                   type: 'POST',
        	       data: {slug : value},
                   success: function(data) {
        				if(data == -1)
                            $(ele).closest('.slug-field').removeClass('has-success').addClass('has-error');
        				else
        					$(ele).closest('.slug-field').removeClass('has-error').addClass('has-success');
                   },
        		    error: function(data) {		
        		      //  alert(data);
        		    }
               });			
	 	 }
	 	 else
	 	 {
	 		$(ele).closest('.slug-field').removeClass('has-success').addClass('has-error');
	 	 }
     	 return false;
    });
});
</script>

<h1><?php echo $model->forenames." ".$model->surname; ?>: Upgrade</h1>
<div class="Admin__content__inner">

        <ul class="AdminTabs nav nav-tabs">
            <li role="presentation">
                <a href="<?php echo Yii::app()->createUrl("users/tabedit", array("id"=>$model->id)); ?>">Details</a>
            </li>
            <li role="presentation">
                <a href="<?php echo Yii::app()->createUrl("users/tabprofile", array("id"=>$model->id)); ?>">Profile</a>
            </li>
            <li role="presentation" >
                <a href="<?php echo Yii::app()->createUrl("users/tabdocuments", array("id"=>$model->id)); ?>">Documents</a>
            </li>
            <li role="presentation" >
                <a href="<?php echo Yii::app()->createUrl("users/tabapplications", array("id"=>$model->id)); ?>">Applications</a>
            </li>
             <li role="upgrade" class="active">
                <a href="<?php echo Yii::app()->createUrl("users/tabupgrade", array("id"=>$model->id)); ?>">Upgrade</a>
            </li>
        </ul>
          <?php
     $form = $this->beginWidget('CActiveForm', array(
         'id' => 'upgrade-form',
         'action' => $this->createUrl('users/premiumUpgrade',array('id'=>$model->id)),
         // Please note: When you enable ajax validation, make sure the corresponding
         // controller action is handling ajax validation correctly.
         // There is a call to performAjaxValidation() commented in generated controller code.
         // See class documentation of CActiveForm for details on this.
         'enableAjaxValidation' => false,
         'htmlOptions' => array('enctype' => 'multipart/form-data','class' => 'form-horizontal'),
     ));
     ?>
     <div class="form-container">
     <?php if (Yii::app()->user->hasFlash('success')): ?>
                		<div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                             <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                     <?php endif; ?>
        <div class="row">
                     <div class="col-sm-3">
                         <div class="form-group">
                             <input type="text" name="end_date" autocomplete='off' class="datepicker form-control" value="" Placeholder="End Date">
                             
                         </div>
                     </div>
                 </div><!-- .row -->
         <div class="form-actions">         
        		<input type="submit" class="btn btn-default" value="Upgrade" >
        </div>
    </div>
     <?php $this->endWidget(); ?>
</div>



