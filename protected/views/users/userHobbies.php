<?php   $hobmodel = UserHobbies::model()->findAllByAttributes(array('user_id'=>$model->id)); ?>
<div class="profile-section-header">
    <div class="row">
        <div class="col-sm-8">
            <h4>Hobbies and Interests</h4>
        </div>
        <div class="col-sm-4">
            <div class="pull-right">
                <button class="add-new main-btn add-new-hobby">Add</button>
            </div>
        </div>
    </div>
</div>
<div class="profile-section-content">
    <div class="profile-section-info" <?php if(count($hobmodel) > 0) { ?>style="display: none;" <?php } ?>>
        <p>Share your hobbies and interests so employers can know more about you.</p>
        <p>Help employers know more about you by looking at your hobbies and interests.</p>
    </div>
    <div class="new-hobbies-form form-profile" style="display: none;">
        <div>
              <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'new-hobby-form',
                        'enableAjaxValidation'=>false,
            'htmlOptions' => array( 'class' => 'form-horizontal','enctype' => 'multipart/form-data'),
                )); ?>
                <div class="form-group">
                    <label class="col-md-3">Hobby or Interest</label>
                    <div class="col-md-9">
                         <input type="hidden" name="user_id" value="<?php echo $model->id ?>">
                        <input type="text" class="form-control" name="activity" value="" required="true">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3">Description</label>
                    <div class="col-lg-9">
                        <textarea name="description" class="form-control" id="description" cols="30" rows="10"></textarea>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <button type="button" class="cancel-btn new-hobby-cancel">Cancel</button>

                </div>
             <?php $this->endWidget(); ?>
        </div>
    </div>
    <div class="hobbies-list">
          <?php 
          
            foreach ($hobmodel as $hob)
            {
          ?>
        <div class="hobby-item profile-item">
            <div class="hobby-item-inner-<?php echo $hob['id']?>" style="display: block;">
                <div class="profile-item-title">
                    <?php echo $hob['activity'] ?> <a class="edit-item edit-hobby" href="#" id="<?php echo $hob['id'] ?>"><i class="fa fa-pencil"></i>Edit</a>
                </div>
                <div class="profile-item-description hobby-description">
                    <?php echo nl2br($hob['description']); ?>
                </div>
            </div>
            <div class="edit-hobby-form-<?php echo $hob['id'] ?>" style="display: none;">
                <div>
                     <?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'edit-hobby-form',
					'enableAjaxValidation'=>false,
	      		   'htmlOptions' => array('class'=>'form-horizontal','enctype' => 'multipart/form-data'),
				)); ?>
                        <div class="form-group">
                            <label class="col-md-3">Hobby or Interest</label>
                            <div class="col-md-9"> <?php echo CHtml::hiddenField('id',$hob['id'],array()); ?>
                                <input type="text" class="form-control" name="activity" value="<?php echo $hob['activity'] ?>" required="true">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3">Description</label>
                            <div class="col-lg-9">
                                <textarea name="description" class="form-control" id="description" cols="30" rows="10"><?php echo str_replace("<br />", null, $hob['description']); ?></textarea>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="cancel-btn hobby_edit_cancel" id="<?php echo $hob['id'] ?>">Cancel</button>

                            <button type="button" class="delete-btn hobby-delete" id="<?php echo $hob['id'] ?>">Delete</button>

                        </div>
                   <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
          <?php } ?>
    </div>
</div>

