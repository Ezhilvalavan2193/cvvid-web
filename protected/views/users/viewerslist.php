<?php
/* @var $this UsersController */
/* @var $model Users */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
    
");
?>
<div class="row">
     <?php         
        foreach ($pvmodel as $pv)
        {
            $usermodel = Users::model()->findByPk($pv['model_id']);
            $empusermodel = EmployerUsers::model()->findByAttributes(array('user_id'=>$usermodel->id));
            if($usermodel != null && $empusermodel != null)
            {
                
                $empmodel = Employers::model()->findByPk($empusermodel->employer_id);
            
                $cri = new CDbCriteria();
                $cri->condition = "owner_id =".$empmodel->id." and type like '%employer%'";
                $profmodel = Profiles::model()->find($cri);
                
               
     ?>
      <div class="col-sm-4 profilelist">
        <div class="ProfileViewer">
            <div class="ProfileViewer__inner">
                <div class="ProfileViewer__image">
                    <img src="" class="img-responsive img-circle">
                    <div class="ProfileViewer__count"><?php echo $pv['view_count'] ?></div>
                </div>
                <div class="ProfileViewer__details">
                    <div class="ProfileViewer__name">
                        <i class="fa fa-user"></i><a href="<?php echo $this->createUrl('employers/employer',array('slug'=>$profmodel->slug))?>"><?php echo $usermodel->forenames." ".$usermodel->surname ?></a>
                    </div>
                    <div class="ProfileViewer__job">
                        <i class="fa fa-briefcase"></i><?php echo $usermodel->current_job?>
                    </div>
                    <div class="ProfileViewer__location">
                        <i class="fa fa-map-marker"></i><?php echo $usermodel->location?>
                    </div>
                </div>
            </div>
<!--             <a class="btn main-btn btn-block" href=""> -->
<!--                 <i class="fa fa-envelope-o"></i>Message -->
<!--             </a> -->
        </div>
	</div>  
	 <?php } 
        }
	 ?>                            
</div>