<?php  $qualmodel = UserQualifications::model()->findAllByAttributes(array('user_id'=>$model->id)); ?>
<div class="profile-section-header">
    <div class="row">
        <div class="col-sm-8">
            <h4>Education</h4>
        </div>
        <div class="col-sm-4">
            <div class="pull-right">
                <button class="add-new main-btn add-new-edu">Add</button>
            </div>
        </div>
    </div>
</div>
<div class="profile-section-content">
    <div class="profile-section-info" <?php if(count($qualmodel) > 0) { ?>style="display: none;" <?php } ?>>
        <p>What's your educational background?</p>
        <p>Tell employers more about your education; remember, be clear and concise.</p>
    </div>
    <div class="new-education-form form-profile" style="display: none;">
        <div>
            <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'new-edu-form',
                    'enableAjaxValidation'=>false,
        'htmlOptions' => array( 'class' => 'form-horizontal','enctype' => 'multipart/form-data'),
            )); ?>
                <div class="form-group">
                    <label class="col-md-3">Institution</label>
                    <div class="col-md-9">
                         <input type="hidden" name="user_id" value="<?php echo $model->id ?>">
                        <input type="text" class="form-control" name="institution" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3">Location</label>
                    <div class="col-md-9">
                        <input type="text" name="location" class="form-control location-search" value="" placeholder="Enter a location" autocomplete="off">
                        <input type="hidden" name="location_lat" value="">
                        <input type="hidden" name="location_lng" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3">Field of Study</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="field" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3">Level of Study</label>
                    <div class="col-md-5">
                        <select name="level" class="form-control">
                            <option value="GCSE">GCSE</option>
                            <option value="A Levels">A Levels</option>
                            <option value="Undergraduate">Undergraduate</option>
                            <option value="Postgraduate">Postgraduate</option>
                            <option value="Vocational">Vocational</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3">Completion Date</label>
                    <div class="col-md-5 form-inline">
                        <select name="completion_month" id="completion_month" class="form-control">
                    <option value="1" <?php echo (1 == date('m') ? 'selected' : '') ?>>January</option>    
                    <option value="2" <?php echo (2 == date('m') ? 'selected' : '') ?>>February</option>                    
                    <option value="3" <?php echo (3 == date('m') ? 'selected' : '') ?>>March</option>
                    <option value="4" <?php echo (4 == date('m') ? 'selected' : '') ?>>April</option>
                    <option value="5" <?php echo (5 == date('m') ? 'selected' : '') ?>>May</option>
                    <option value="6" <?php echo (6 == date('m') ? 'selected' : '') ?>>June</option>
                    <option value="7" <?php echo (7 == date('m') ? 'selected' : '') ?>>July</option>
                    <option value="8" <?php echo (8 == date('m') ? 'selected' : '') ?>>August</option>
                    <option value="9" <?php echo (9 == date('m') ? 'selected' : '') ?>>September</option>
                    <option value="10" <?php echo (10 == date('m') ? 'selected' : '') ?>>October</option>
                    <option value="11" <?php echo (11 == date('m') ? 'selected' : '') ?>>November</option>
                    <option value="12" <?php echo (12 == date('m') ? 'selected' : '') ?>>December</option>  
                        </select>
                        <select name="completion_year" id="completion_year" class="form-control">
                        <?php for($i = 1957; $i < date('Y')+30; $i++){  ?>
	               		 <option <?php echo ($i == date('Y') ? 'selected' : '') ?> value="<?php echo $i ?>"><?php echo $i ?></option>
	                <?php } ?>    
                
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3">Grade</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="grade" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3">Description</label>
                    <div class="col-lg-9">
                        <textarea name="description" class="form-control" id="education_description" cols="30" rows="10"></textarea>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary" id="new_exp_save">Save</button>
                    <button type="button" class="cancel-btn" id="new_edu_cancel">Cancel</button>

                </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
    <div class="education-list">
         <?php 
         
            foreach ($qualmodel as $qual)
            {
        ?>
        <div class="education-item profile-item">
            <div class="education-item-inner-<?php echo $qual['id']?>" style="display: block;">
                <div class="profile-item-title">
                   <?php echo $qual['institution']?><a class="edit-item edit-edu" href="#" id="<?php echo $qual['id'] ?>"><i class="fa fa-pencil"></i>Edit</a>
                </div>
                <div class="profile-item-subtitle education-title"> <?php echo $qual['field'] ?></div>
                <div class="education-level"><?php echo $qual['level'] ?></div>
                <div class="profile-item-dates education-dates"><?php echo $qual['completion_date'] ?></div>
                <div class="profile-item-description education-description">
                    <?php echo nl2br($qual['description']); ?>
                </div>
            </div>
            <div class="edit-education-form-<?php echo $qual['id'] ?>" style="display: none;">
                <div>
                    <?php  
             $complete_month = 0;
             $complete_year = 0;
             
	          if($qual['completion_date'] != null)
	          {
			      $d = date_parse_from_format("Y-m-d", $qual['completion_date']);
			      $complete_month = $d["month"];
			      $complete_year = $d["year"];
	          }
	          
       ?>
                    <?php $form=$this->beginWidget('CActiveForm', array(
					'id'=>'edit-edu-form',
					'enableAjaxValidation'=>false,
	      		    'htmlOptions' => array('class' => 'form-horizontal','enctype' => 'multipart/form-data'),
				)); ?>
                        <div class="form-group">
                            <label class="col-md-3">Institution</label>
                            <div class="col-md-9">
                                <?php echo CHtml::hiddenField('id',$qual['id'],array()); ?>
                                <input type="text" class="form-control" name="institution" value="<?php echo $qual['institution'] ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">Location</label>
                            <div class="col-md-9">
                                <input type="text" name="location" class="form-control location-search" value="<?php echo $qual['location'] ?>" placeholder="Enter a location" autocomplete="off">
                                <input type="hidden" name="location_lat" value="">
                                <input type="hidden" name="location_lng" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">Field of Study</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="field" value="<?php echo $qual['field'] ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">Level of Study</label>
                            <div class="col-md-5">
                                <select name="level" class="form-control">
                                    <option value="GCSE" <?php echo $qual['level'] == "GCSE" ? "selected" : "" ?> >GCSE</option>
                <option value="A Levels" <?php echo $qual['level'] == "A Levels" ? "selected" : "" ?>>A Levels</option>
                <option value="Undergraduate" <?php echo $qual['level'] == "Undergraduate" ? "selected" : "" ?>>Undergraduate</option>
                <option value="Postgraduate" <?php echo $qual['level'] == "Postgraduate" ? "selected" : "" ?>>Postgraduate</option>
                <option value="Vocational" <?php echo $qual['level'] == "Vocational" ? "selected" : "" ?>>Vocational</option>
            </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">Completion Date</label>
                            <div class="col-md-5 form-inline">
                                <select name="completion_month" id="completion_month" class="form-control">
 
                    <option value="1" <?php echo $complete_month == 1 ? "selected" : "" ?> >January</option>
                    <option value="2" <?php echo $complete_month == 2 ? "selected" : "" ?>>February</option>
                    <option value="3" <?php echo $complete_month == 3 ? "selected" : "" ?>>March</option>
                    <option value="4" <?php echo $complete_month == 4 ? "selected" : "" ?>>April</option>
                    <option value="5" <?php echo $complete_month == 5 ? "selected" : "" ?>>May</option>
                    <option value="6" <?php echo $complete_month == 6 ? "selected" : "" ?>>June</option>
                    <option value="7" <?php echo $complete_month == 7 ? "selected" : "" ?>>July</option>
                    <option value="8" <?php echo $complete_month == 8 ? "selected" : "" ?>>August</option>                    
                    <option value="9" <?php echo $complete_month == 9 ? "selected" : "" ?>>September</option>
                    <option value="10" <?php echo $complete_month == 10 ? "selected" : "" ?>>October</option>                    
                    <option value="11" <?php echo $complete_month == 11 ? "selected" : "" ?>>November</option>
                    <option value="12" <?php echo $complete_month == 12 ? "selected" : "" ?>>December</option>
                
                                </select>
                                <select name="completion_year" id="completion_year" class="form-control">
                                    <?php for($i = 1957; $i < date('Y')+30; $i++){  ?>
                                     <option <?php echo ($i == $complete_year ? 'selected' : '') ?> value="<?php echo $i ?>"><?php echo $i ?></option>
                                    <?php } ?>    
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">Grade</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="grade" value="<?php echo $qual['grade']; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3">Description</label>
                            <div class="col-lg-9">
                                <textarea name="description" class="form-control" id="education_description" cols="30" rows="10"><?php echo str_replace("<br />", null, $qual['description']); ?></textarea>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <button type="button" class="cancel-btn edu_edit_cancel" id="<?php echo $qual['id']?>">Cancel</button>

                            <button type="button" class="delete-btn edu-delete" id="<?php echo $qual['id']?>">Delete</button>

                        </div>
                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
           <?php } ?>
    </div>
</div>

