<?php
/* @var $this UsersController */
/* @var $model Users */
?>
<script>
$(document).ready(function(){
	
	$('#start_interview').click(function(){
		
		if(confirm('Do you have a webcam ?')) {
			window.location = "<?php echo $this->createUrl('users/interview',array('id'=>$jamodel['id'])); ?>";
		}
		
	});
	
});
</script>
<?php 
    $jobid = $jamodel['job_id'];
   
    $question_cnt = JobQuestions::model()->findAllByAttributes(array('job_id'=>$jobid,'status'=>1));
    
    $cri = new CDbCriteria();
//     $cri->select = "sum(answer_duration) as answer_duration";
    $cri->condition = "job_id =".$jobid." and status = 1";
    $jquestions = JobQuestions::model()->find($cri);
    $duration_mins = $jamodel['interview_duration'];
   
    $hours = $duration_mins / 60;
    if($hours >= 1)
        $mins = $duration_mins - ($hours * 60);
    else 
        $mins = $duration_mins;
?>
<div id="page-content">
    <div class="container" id="basic-container">
     <section class="experiment">
        	<div class="card" style="padding: 20px">
        		<h3>Video Interview Instructions:</h3>
        		 <p>In this interview you will attend <?php echo count($question_cnt) ?> questions</p>
        		 <p>Each question may or may not have time duration,depends on employer</p>
        		 <p>All the best...</p>
        	</div>
    	    <div align="center">
    		   <h3 class="header">
                  <button class="btn btn-primary" id="start_interview">Start Interview</button>
                </h3>
           </div>     
    	 </section>
    </div>
</div>