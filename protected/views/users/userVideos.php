<div class="profile-section-info">

    <div class="VideoList">
        <?php
       
        $pvideos = ProfileVideos::model()->findAllByAttributes(array('profile_id' => $profilemodel->id));
//         echo var_dump($pvideos);die();
        foreach ($pvideos as $videos) {
            $mediamodel = Media::model()->findByAttributes(array('model_id' => $videos['video_id']));
            if($mediamodel != null)
            {
                $videomodel = Videos::model()->findByPk($videos['video_id']);
//                if($videomodel->duration <= 0)
//                {
//                    $videoid = $videomodel->video_id;
//                    $json_url = 'http://vimeo.com/api/v2/video/'.$videoid.'.xml';
//                    $ch = curl_init($json_url);
//                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//                    curl_setopt($ch, CURLOPT_HEADER, 0);
//                    $data = curl_exec($ch);
//                    curl_close($ch);
//                    $data = new SimpleXmlElement($data, LIBXML_NOCDATA);
//                    $duration = isset($data->video->duration) ? $data->video->duration : 0;
//                    $imgpath = $data->video->thumbnail_large;
//                   
//                    //$iname = pathinfo($name, PATHINFO_FILENAME);//substr($imgpath, $pos + strlen($del), strlen($imgpath) - 1);
//                    $img_name = $mediamodel->file_name;
//                    
//                    $mediamodel->size = (int)$duration;
//                    if ($mediamodel->save()) {
//                        $videomodel->duration = (int)$duration;
//                        $videomodel->save();
//                        
//                        $filename = $img_name;
//                        $mediafolder = "images/media/" . $mediamodel->id . "/conversions";
//                        
//                        $imgprefix = "images/media/" . $mediamodel->id;
//                        $fullpath = "images/media/" . $mediamodel->id . "/" . $img_name;
//                        
//                        if (!file_exists($mediafolder)) {
//                            mkdir($mediafolder, 0777, true);
//                        }
//                        
//                        if (copy($imgpath, $fullpath)) {
//                            
//                            //create joblisting
//                            $jlwidth = 350;
//                            $jlheight = 190;
//                            $jldestpath = $imgprefix . "/conversions/profile.jpg";
//                            
//                            //create search
//                            $searchwidth = 167;
//                            $searchheight = 167;
//                            $searchdestpath = $imgprefix . "/conversions/search.jpg";
//                            
//                            //create thumb
//                            $thumbwidth = 126;
//                            $thumbheight = 126;
//                            $thumbdestpath = $imgprefix . "/conversions/thumb.jpg";
//                            
//                           
//                                $image = imagecreatefromjpeg($fullpath);
//                                
//                                //$this->resize_image($image, $image_type, $fullpath, $coverdestpath, $coverwidth, $coverheight); //create cover
//                                //$this->resize_image($image, $image_type, $fullpath, $icondestpath, $iconwidth, $iconheight); //create icon
//                                resize_image($image, "image/jpg", $fullpath, $jldestpath, $jlwidth, $jlheight); //create joblisting
//                                resize_image($image, "image/jpg", $fullpath, $searchdestpath, $searchwidth, $searchheight); //create search
//                                resize_image($image, "image/jpg", $fullpath, $thumbdestpath, $thumbwidth, $thumbheight); //create thumb
//                            
//                        }
//                    }
//                }
            ?>
            <div class="ProfileVideo">
                <div class="ProfileVideo__inner">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="ProfileVideo__thumb">
                                <img src="<?php echo Yii::app()->baseUrl . "/images/media/" . $mediamodel->id . "/" . $mediamodel->file_name ?>" class="img-responsive">
							<?php if($videos['is_default'] > 0) { ?>
                                <div class="ProfileVideo__default__marker">
                                    <i class="fa fa-star"></i>
                                </div>
							<?php } ?>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="ProfileVideo__title">
                                <span class="ProfileVideo__title__display"><?php echo $videomodel->name ?>  
                                    <a href="#" class="ProfileVideo__edit edit-video-name" id="<?php echo $videomodel->id ?>">Edit</a></span>
                                <div class="ProfileVideo__title__edit input-group" id="ProfileVideo-<?php echo $videomodel->id ?>" style="display: none">
                                    <input type="text" class="form-control evideo-name" value="<?php echo $videomodel->name ?>" id="<?php echo $videomodel->id ?>"  >
                                    <span class="input-group-btn">
                                        <button class="btn btn-primary ProfileVideo__title__submit video-edit-save" type="button" name="video_name_save" id="<?php echo $videomodel->id ?>"><i class="fa fa-check"></i></button>
                                        <button class="btn btn-default ProfileVideo__title__cancel video-edit-close" name="video_name_cancel" id="<?php echo $videomodel->id ?>" type="button"><i class="fa fa-close"></i></button>
                                    </span>
                                </div>
                            </div>
                            <div class="ProfileVideo__length">Length: <?php echo $videomodel->duration ?></div>
                            <div class="ProfileVideo__size">Is Default: <?php echo $videos['is_default'] == 1 ? "Yes" : "No" ?></div>
                            <div class="ProfileVideo__buttons">
                                <a class="ProfileVideo__default make_default" href="#" id="<?php echo $videomodel->id ?>"> <?php if ($videos['is_default'] == 0) { ?>Make Default<?php } ?></a>
                                <a class="ProfileVideo__delete video_delete" href="#" id="<?php echo $videomodel->id ?>">Delete</a>
                                <a class="ProfileVideo__publish video_publish" href="#" id="<?php echo $videomodel->id ?>">
                                    <?php echo ($videos['state'] == 1 ? "Published" : "UnPublish") ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } 
        
            }
            ?>
    </div>
    <div class="VideoUpload">
        <div>
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'users-pvideo-form',
                'action' => $this->createUrl('users/addProfileVideo'), 
                'enableAjaxValidation' => true,
                'htmlOptions' => array('class' => 'form-horizontal', 'enctype' => 'multipart/form-data')
            ));
            ?>
            <div class="progress" style="display:none;">
                <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                    <div class="status">0%</div>
                </div>
            </div>
            <div class="UploadMessage alert alert-success" role="alert" style="display:none;">
                Thanks for uploading your video. Our clever algorithms will optimise the file before it is available to view.
            </div>
            <input type="hidden" name="userid" value="<?php echo $model->id ?>">
            <input type="text" class="VideoUpload__name form-control col-sm-3 media-btn1" id="video-name" name="video_name" placeholder="Video Name" >
            <label class="btn main-btn add-new media-btn1" for="NewVideo">
                Choose Video
                <input type="file" id="NewVideo" name="video" accept="video/*">
            </label>
            <input type="submit" class="btn btn-primary main-btn media-btn1" value="Upload Video">
            <!--<button type="submit" class="btn btn-primary">Upload Video</button>-->
            <?php $this->endWidget(); ?>
        </div>
    </div>
</div>
<?php 
function resize_image($image, $image_type, $fullpath, $destpath, $width, $height) {
    $size_arr = getimagesize($fullpath);
    
    list($width_orig, $height_orig, $img_type) = $size_arr;
    $ratio_orig = $width_orig / $height_orig;
    
    $tempimg = imagecreatetruecolor($width, $height);
    imagecopyresampled($tempimg, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
    
    if ($image_type == "image/jpg" || $image_type == "image/jpeg")
        imagejpeg($tempimg, $destpath);
        else if ($image_type == "image/png")
            imagepng($tempimg, $destpath);
            else if ($image_type == "image/gif")
                imagegif($tempimg, $destpath);
}
?>