<?php  $expmodel = UserExperiences::model()->findAllByAttributes(array('user_id' => $model->id)); ?>
<div class="profile-section-header">
    <div class="row">
        <div class="col-sm-8">
            <h4>Professional Experience</h4>
        </div>
        <div class="col-sm-4">
            <div class="pull-right">
                <button class="add-experience main-btn add-new-exp">Add</button>
            </div>
        </div>
    </div>
</div>
<div class="profile-section-content">
    <div class="profile-section-info" <?php if(count($expmodel) > 0) { ?>style="display: none;" <?php } ?>>
        <p>What's your work experience? Your experience is one of the most important sections in a CV.</p>
        <p>List all relevant responsibilities, skills, projects and achievements against each role.
            If you're a fresh grad, you can add any volunteer work or any internship you've done before.</p>
    </div>
    <div class="new-experience-form form-profile" style="display: none;">
        <div class="experiences-form">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'new-exp-form',
                'enableAjaxValidation' => false,
                'htmlOptions' => array( 'class' => 'form-horizontal','enctype' => 'multipart/form-data'),
            ));
            ?>
            <form class="form-horizontal">
                <div class="form-group">
                    <label class="col-md-3">Start Date</label>
                    <div class="col-md-9 form-inline">
                        <input type="hidden" name="user_id" value="<?php echo $model->id ?>">
                        <select name="start_month" class="form-control" id = "start_month">
                            <option value="1">January</option>                    
                            <option value="2">February</option>                    
                            <option value="3">March</option>                    
                            <option value="4">April</option>                    
                            <option value="5">May</option>                    
                            <option value="6">June</option>                    
                            <option value="7">July</option>                    
                            <option value="8">August</option>                    
                            <option value="9">September</option>                   
                            <option value="10">October</option>                    
                            <option value="11">November</option>                    
                            <option value="12">December</option>
                        </select>
                        <select name="start_year" class="form-control" id = "start_year">
                            <?php for ($i = 1997; $i <= date('Y'); $i++) { ?>
                                <option value="<?php echo $i ?>"><?php echo $i ?></option>
                            <?php } ?>  
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3">End Date</label>
                    <div class="col-md-9 form-inline">
                        <select name="end_month" class="form-control" id = "end_month">
                            <option value="1">January</option>    
                            <option value="2">February</option>                    
                            <option value="3">March</option>
                            <option value="4">April</option>
                            <option value="5">May</option>
                            <option value="6">June</option>
                            <option value="7">July</option>
                            <option value="8">August</option>
                            <option value="9">September</option>
                            <option value="10">October</option>
                            <option value="11">November</option>
                            <option value="12">December</option>

                        </select>
                        <select name="end_year" class="form-control" id = "end_year">
                            <?php for ($i = 1997; $i <= date('Y'); $i++) { ?>
                                <option value="<?php echo $i ?>"><?php echo $i ?></option>
                            <?php } ?> 
                        </select>
                        or
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="is_current" value="1" id="is_current">
                                Present
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3">Position</label>
                    <div class="col-md-9">
                        <input type="text" name="position" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3">Company Name</label>
                    <div class="col-md-9">
                        <input type="text" name="company_name" class="form-control" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3">Location</label>
                    <div class="col-md-9">
                        <input type="text" name="location" class="form-control location-search" value="" placeholder="Enter a location" autocomplete="off">
                        <input type="hidden" name="location_lat" value="">
                        <input type="hidden" name="location_lng" value="">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-lg-3">Work Description</label>
                    <div class="col-lg-9">
                        <textarea name="description" class="form-control" cols="50" rows="10"></textarea>
                    </div>
                </div>
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary" id="new_exp_save">Save</button>
                    <button type="button" class="cancel-btn new_exp_cancel">Cancel</button>

                </div>
                <?php $this->endWidget(); ?>
        </div>
    </div>
    <div class="experiences-list">
        <?php
       
        foreach ($expmodel as $exp) {
            ?>
            <div class="profile-item experience">
                <div class="experience-inner-<?php echo $exp['id'] ?>" style="display: block;">
                    <div class="experience-position clearfix">
                        <div class="pull-left">
                            <div class="profile-item-title"><?php echo ($exp['company_name'] . ($exp['location'] != null ? "(" . $exp['location'] . ")" : "")) ?> <a class="edit-item edit-exp" id="<?php echo $exp['id'] ?>" href="#"><i class="fa fa-pencil"></i>Edit</a></div>
                            <div class="profile-item-subtitle"><?php echo $exp['position'] ?></div>
                        </div>
                    </div>
                    <div class="profile-item-dates experience-dates">
                        <?php echo $exp['start_date'] ?>
                        to
                        <?php echo $exp['end_date'] == null ? "Present" : $exp['end_date'] ?>

                    </div>
                    <div class="profile-item-description experience-description">
                        <?php echo nl2br($exp['description']); ?>
                    </div>
                </div>
                <div class="experience-form-<?php echo $exp['id'] ?>" style="display: none;">
                    <div class="experiences-form">
                        <?php
                        $start_exp_month = 0;
                        $start_exp_year = 0;
                        $end_exp_month = 0;
                        $end_exp_year = 0;

                        if ($exp['start_date'] != null) {
                            $d = date_parse_from_format("Y-m-d", $exp['start_date']);
                            $start_exp_month = $d["month"];
                            $start_exp_year = $d["year"];
                        }

                        if ($exp['end_date'] != null) {
                            $d = date_parse_from_format("Y-m-d", $exp['end_date']);
                            $end_exp_month = $d["month"];
                            $end_exp_year = $d["year"];
                        }
                        ?>
                        <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'edit-exp-form',
                            'enableAjaxValidation' => true,
                            'htmlOptions' => array( 'class' => 'form-horizontal','enctype' => 'multipart/form-data'),
                        ));
                        ?>
                        <div class="form-group">
                            <label class="col-md-3">Start Date</label>
                            <div class="col-md-9 form-inline">
                                <?php echo CHtml::hiddenField('id', $exp['id'], array()); ?>
                                <select name="start_month" class="form-control" id="start_month">
                                    <option value="1" <?php echo $start_exp_year == 1 ? "selected" : "" ?>>January</option>                    
                                    <option value="2" <?php echo $start_exp_year == 2 ? "selected" : "" ?>>February</option>                    
                                    <option value="3" <?php echo $start_exp_year == 3 ? "selected" : "" ?>>March</option>                    
                                    <option value="4" <?php echo $start_exp_year == 4 ? "selected" : "" ?>>April</option>                    
                                    <option value="5" <?php echo $start_exp_year == 5 ? "selected" : "" ?>>May</option>                    
                                    <option value="6" <?php echo $start_exp_year == 6 ? "selected" : "" ?>>June</option>                    
                                    <option value="7" <?php echo $start_exp_year == 7 ? "selected" : "" ?>>July</option>                    
                                    <option value="8" <?php echo $start_exp_year == 8 ? "selected" : "" ?>>August</option>                    
                                    <option value="9" <?php echo $start_exp_year == 9 ? "selected" : "" ?>>September</option>                   
                                    <option value="10" <?php echo $start_exp_year == 10 ? "selected" : "" ?>>October</option>                    
                                    <option value="11" <?php echo $start_exp_year == 11 ? "selected" : "" ?>>November</option>                    
                                    <option value="12" <?php echo $start_exp_year == 12 ? "selected" : "" ?>>December</option>
                                </select>
                                <select name="start_year" class="form-control" id = 'start_year'>
                                    <?php for ($i = 1997; $i < 2036; $i++) { ?>
                                        <option value="<?php echo $i ?>" <?php echo $start_exp_year == $i ? "selected" : "" ?> ><?php echo $i ?></option>
                                    <?php } ?>  
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">End Date</label>
                            <div class="col-md-9 form-inline">
                                <select name="end_month" class="form-control end_month" <?php echo $exp['end_date'] == null ? "disabled" : ""?> id="end_month">
                                    <option value="1" <?php echo $end_exp_year == 1 ? "selected" : "" ?> >January</option>    
                                    <option value="2" <?php echo $end_exp_year == 1 ? "selected" : "" ?> >February</option>                    
                                    <option value="3" <?php echo $end_exp_year == 1 ? "selected" : "" ?> >March</option>
                                    <option value="4" <?php echo $end_exp_year == 1 ? "selected" : "" ?> >April</option>
                                    <option value="5" <?php echo $end_exp_year == 1 ? "selected" : "" ?> >May</option>
                                    <option value="6" <?php echo $end_exp_year == 1 ? "selected" : "" ?> >June</option>
                                    <option value="7" <?php echo $end_exp_year == 1 ? "selected" : "" ?> >July</option>
                                    <option value="8" <?php echo $end_exp_year == 1 ? "selected" : "" ?> >August</option>
                                    <option value="9" <?php echo $end_exp_year == 1 ? "selected" : "" ?> >September</option>
                                    <option value="10" <?php echo $end_exp_year == 1 ? "selected" : "" ?> >October</option>
                                    <option value="11" <?php echo $end_exp_year == 1 ? "selected" : "" ?> >November</option>
                                    <option value="12" <?php echo $end_exp_year == 1 ? "selected" : "" ?> >December</option>  

                                </select>
                                <select name="end_year" class="form-control end_year" <?php echo $exp['end_date'] == null ? "disabled" : "" ?> id = 'end_year'>
                                    <?php for ($i = 1997; $i < 2036; $i++) { ?>
                                        <option value="<?php echo $i ?>" <?php echo $end_exp_year == $i ? "selected" : "" ?> ><?php echo $i ?></option>
                                    <?php } ?>   
                                </select>
                                or
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="is_current" id="edit_is_current"  <?php echo $exp['end_date'] == null ? "checked" : "" ?>>
                                        Present
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">Position</label>
                            <div class="col-md-9">
                                <input type="text" name="position" class="form-control" value="<?php echo $exp['position'] ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">Company Name</label>
                            <div class="col-md-9">
                                <input type="text" name="company_name" class="form-control" value="<?php echo $exp['company_name'] ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3">Location</label>
                            <div class="col-md-9">
                                <input type="text" name="location" class="form-control location-search" value="<?php echo $exp['location'] ?>" placeholder="Enter a location" autocomplete="off">
                                <input type="hidden" name="location_lat" value="">
                                <input type="hidden" name="location_lng" value="">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-lg-3">Work Description</label>
                            <div class="col-lg-9">
                                <textarea name="description" class="form-control" cols="50" rows="10"><?php echo str_replace("<br />", null, $exp['description']); ?></textarea>
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn btn-primary" id="<?php echo $exp['id'] ?>">Save</button>
                            <button type="button" class="cancel-btn exp_edit_cancel" id="<?php echo $exp['id'] ?>">Cancel</button>

                            <button type="button" class="delete-btn exp-delete"  id="<?php echo $exp['id'] ?>">Delete</button>

                        </div>
                        <?php $this->endWidget(); ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>


