<?php $pskillsmodel = ProfileSkills::model()->findAllByAttributes(array('profile_id' => $profilemodel->id)); ?>
<div class="profile-section-header">
    <div class="row">
        <div class="col-sm-8">
             <h4>Industry Choices and Skills</h4>
        </div>
        <div class="col-sm-4">
            <div class="pull-right">
                <button class="add-new main-btn add-new-skill">Add</button>
                <button class="add-new main-btn save-new-skill" style="display: none;">Done</button>
            </div>
        </div>
    </div>
</div>
<?php

$pskillarr = array();
foreach ($pskillsmodel as $pskill)
    $pskillarr[] = $pskill['skill_id'];

$cri = new CDbCriteria();
$cri->order = 'name';
$skillcateg = SkillCategories::model()->findAll($cri);
?>
<div class="profile-section-content">
    <div id="skills-form" class="form-profile clearfix" style="display: none;height: 550px;overflow-x: auto;">
        <div class="skill-category-slideshow">
            <div class="skill-category-slide">
                <?php foreach ($skillcateg as $categ) { ?>
                <div class="skill-category" id="<?php echo $categ['id']; ?>">
                    <div class="category-inner">
                        <i class="fa fa-book"></i>
                        <div><?php echo $categ['name'] ?></div>
                    </div>
                </div>
                 <?php } ?>
            </div>
        </div>
    </div>
    
    
    
<?php foreach ($skillcateg as $categ) { ?>
<div class="skill-category-inner-<?php echo $categ['id'] ?>" id="<?php echo $categ['id'] ?>" style="display: none;padding: 25px;
    background: #F5F5F5;">
        <div class="row">
            <div class="col-sm-3">
                <div class="skill-category-item">
                    <div class="skill-category-item-inner">
                        <i class="fa fa-book"></i>
                        <div><?php echo $categ['name'] ?></div>
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <ul class="category-skills list-inline">
                    <?php
                    $categskills = Skills::model()->findAllByAttributes(array('skill_category_id' => $categ['id']));
                    foreach ($categskills as $skils) {
                        ?>				                   
                        <li class="skill skill-change <?php echo in_array($skils['id'], $pskillarr) ? 'selected' : '' ?>" id="<?php echo $skils['id'] ?>">
                            <div class="skill-name"><?php echo $skils['name'] ?></div>
                        </li>
                   <?php } ?>
                </ul>
            </div>
        </div>
</div>
    <?php } ?>
    
   
    <div class="skills-list">
        <div class="skills-list-empty" <?php if(count($pskillsmodel) > 0) { ?>style="display: none;" <?php } ?>>
            <p>Please select your skills that best describe you.</p>
        </div>
        <div class="skills-list-items">
             <?php
        $skillcateg = array();
        $skillarr = array();
        $cri = new CDbCriteria();
        $cri->alias = "s";
        $cri->select = 'distinct s.skill_category_id';
        $cri->join = "inner join cv16_profile_skills ps on ps.skill_id = s.id
		                    inner join cv16_skill_categories sc on sc.id = s.skill_category_id";
        $cri->condition = "ps.profile_id=" . $profilemodel->id;

        $skillmodel = Skills::model()->findAll($cri);

        foreach ($skillmodel as $skill) {
            $skillcateg[] = $skill['skill_category_id'];
        }

        for ($i = 0; $i < count($skillcateg); $i++) {
            $skillcategmodel = SkillCategories::model()->findByPk($skillcateg[$i]);
//               	if($categarr[0] == $skillcategmodel->id)
//               	{
            ?>
            <div class="active-category" id="<?php echo $skillcategmodel->id ?>">
                <div class="active-category-inner" style="display: block;">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="ActiveCategory__header">
                                 <?php echo $skillcategmodel->name ?> <a class="ActiveCategoryEdit edit-skill-categ" id="<?php echo $skillcategmodel->id ?>"><i class="fa fa-pencil"></i> Edit</a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="active-skills list-inline">
                                <?php
                                $ids = implode(',', $pskillarr);
                                $pcri = new CDbCriteria();
                                $pcri->condition = "id in (" . $ids . ") and skill_category_id =" . $skillcateg[$i];
                                $skillmodel = Skills::model()->findAll($pcri);
                                foreach ($skillmodel as $skills) {
                                    //$skillarr[] = $skills['id'];
                                    ?>
                                <li class="skill">
                                    <div class="skill-name"><?php echo $skills['name'] ?></div>
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="active-category-form-<?php echo $skillcategmodel->id ?>" style="display: none;">
                    <div>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="skill-category-item">
                                    <div class="skill-category-item-inner">
                                        <i class="fa fa-book"></i>
                                        <div><?php echo $skillcategmodel->name ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-9">
                                <ul class="skill-items list-inline">
                                      <?php $skillsmodel = Skills::model()->findAllByAttributes(array('skill_category_id' => $skillcategmodel->id));
                                        foreach ($skillsmodel as $skils) {
                                            ?>
                                    <li class="skill skill-change <?php echo in_array($skils['id'], $pskillarr) ? 'selected' : '' ?>" id="<?php echo $skils['id']; ?>">
                                        <div class="skill-name"><?php echo $skils['name'] ?></div>
                                    </li>
                                    <?php } ?>
                                </ul>
                                <button type="button" class="CategoryEditFinish btn btn-primary pull-right">Done</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>


