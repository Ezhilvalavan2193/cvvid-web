<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs = array(
    'Users' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});

");
?> 
<script>
    $(function () {
        $('#subscription_ends_at, #trial_ends_at').datetimepicker();
    });
    $(document).ready(function () {

        $("#sendmsg").click(function (e) {
            e.preventDefault();
            $('#msgdialog').addClass('in');
            $('body').addClass('modal-open');
            $('#msgdialog').show();
        });

        $(".extendSubscription").click(function (e) {
            e.preventDefault();
            $('#extendSubscription').addClass('in');
            $('body').addClass('modal-open');
            $('#extendSubscription').show();
        });

        $(".extendTrail").click(function (e) {
            e.preventDefault();
            $('#extendTrail').addClass('in');
            $('body').addClass('modal-open');
            $('#extendTrail').show();
        });

        $("#msg_cancel").click(function (e) {
            closepopup();
        });

        $('.subs .open-datetimepicker').click(function (event) {
            event.preventDefault();
            $('#subscription_ends_at').datetimepicker('show');
        });

        $('.trail .open-datetimepicker').click(function (event) {
            event.preventDefault();
            $('#trial_ends_at').datetimepicker('show');
        });

//        $('.subs .open-datetimepicker').click(function (event) {
//            event.preventDefault();
//            $('#trial_ends_at').datetimepicker('show');
//        });

        $("#sendmsg_btn").click(function (e) {
            var subject = $("#subject").val();
            var message = $("#message").val();
            $.ajax({
                url: '<?php echo $this->createUrl('users/sendMessage') ?>',
                type: 'POST',
                data: {"subject": subject, "message": message, "user_id":<?php echo $model->id ?>},
                success: function (data) {
                    $('.alert-success').show();
                    setInterval(function () {
                        closepopup();
                    }, 3000);

                },
                error: function (data) {
                    alert(data);
                }
            });

        });

        $(".like_btn").click(function (e) {
            $.ajax({
                url: '<?php echo $this->createUrl('users/likeVideo') ?>',
                type: 'POST',
                data: {"user_id":<?php echo $model->id ?>},
                success: function (data) {
                    if (data > 0)
                        $('.like_btn').html('UnLike <i title="Unlike Profile Video" class="fa fa-thumbs-up" style="font-size:24px"></i>');
                    else
                        $('.like_btn').html('Like <i title="Like Profile Video" class="fa fa-thumbs-o-up" style="font-size:24px"></i>');

                },
                error: function (data) {
                    alert(data);
                }
            });

        });

        $("#trails_end").click(function (e) {

            var trails = $('#trial_ends_at').val();

            $.ajax({
                url: '<?php echo $this->createUrl('users/extendTrail') ?>',
                type: 'POST',
                data: {"user_id":<?php echo $model->id ?>, "trial_ends_at": trails},
                success: function (data) {
                    if (data > 0)
                    {
                        alert('successfully updated');
                        location.reload();
                    }
                    else
                        alert('error in updated');

                }
            });

        });

        $("#subs_ends").click(function (e) {

            var trails = $('#subscription_ends_at').val();

            $.ajax({
                url: '<?php echo $this->createUrl('users/extendSubscription') ?>',
                type: 'POST',
                data: {"user_id":<?php echo $model->id ?>, "subscription_ends_at": trails},
                success: function (data) {
                    if (data > 0)
                    {
                        alert('successfully updated');
                        location.reload();
                    }
                    else
                        alert('error in updated');

                }
            });

        });

        $(".profileBasic").click(function (e) {

            if (confirm("are you sure to make basic"))
            {
                $.ajax({
                    url: '<?php echo $this->createUrl('users/makeBasic') ?>',
                    type: 'POST',
                    data: {"user_id":<?php echo $model->id ?>},
                    success: function (data) {
                        if (data > 0)
                        {
                            alert('successfully updated');
                            location.reload();
                        }
                        else
                            alert('error in updated');

                    }
                });
            }

        });
        
        $('.comment_btn').click(function (e) {
           $('.comment-wrap').slideToggle('slow');
        });
        
        $('#comment_save').click(function (e) {
            var candidate_id = $('#candidate_id').val();
            var comment = $('#comment-box').val();
            
           $.ajax({
                url: '<?php echo $this->createUrl('users/addcomment') ?>',
                type: 'POST',
                data: {"candidate_id" : candidate_id,"comment" : comment},
                success: function (data) {
                    $('.comments-list').html(data);
                    $('#comment-box').val('');
                }
            });
        });
    });
</script>

<div id="profile">
    <?php
    $employerid = isset($this->employerid) ? $this->employerid : 0;
    $userid = isset($this->userid) ? $this->userid : 0;
    $institutionid = isset($this->institutionid) ? $this->institutionid : 0;
    $agencyid = isset($this->agencyid) ? $this->agencyid : 0;
    $edit = isset($this->edit) ? $this->edit : 0;
    $this->renderPartial('//profiles/Cover', array('userid' => $userid, 'employerid' => $employerid, 'institutionid' => $institutionid, 'agencyid' => $agencyid, 'edit' => $edit));
    ?>
    <?php $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $model->id, 'type' => $model->type)); ?>
    <div id="form-details">
        <div class="container">
            <div class="form-details-inner">
                <div class="form-user-details form-section">
                    <div class="row">
                        <div class="text-center col-sm-4 col-md-3 col-lg-2">
                            <div class="ProfileEdit__photo">
                                <?php
                                if ($profilemodel->photo_id > 0) {
                                    $mediamodel = Media::model()->findByPk($profilemodel->photo_id);
                                    $src = ((!empty($mediamodel->file_name) && $mediamodel->file_name != null) ? Yii::app()->baseUrl . "/images/media/" . $profilemodel->photo_id . "/" . $mediamodel->file_name : Yii::app()->baseUrl . "/images/profile.png");
                                } else {
                                    $src = Yii::app()->baseUrl . "/images/defaultprofile.jpg";
                                }
                                echo CHtml::image($src, "", array("id" => "profile-image"));
                                ?>
                            </div> 
                            <?php if (Yii::app()->user->getState('role') == "admin" || Yii::app()->user->getState('userid') == $model->id) {
                                ?>
                                <a href="<?php echo $this->createUrl('users/edit', array('slug' => $profilemodel->slug)) ?>" class="btn btn-primary" > <i class="fa fa-pencil"></i>
                                    <span>Edit Profile</span></a>
                            <?php } ?>
                        </div>
                        <div class="col-sm-5 col-md-5 col-lg-6">
                            <div class="ProfileEdit__details">
                                <h3>
                                    <?php echo $model->forenames ?> <?php echo $model->surname ?>
                                    <?php if (Yii::app()->user->getState('role') == "candidate") { ?>
                                        <?php if (Membership::subscribed($model->stripe_active, $model->trial_ends_at, $model->subscription_ends_at)) { ?> 
                                            - <small class="subscription-marker subscription-marker--premium">Premium Account</small>
                                        <?php } else { ?>
                                            - <small class="subscription-marker subscription-marker--basic">Basic Account</small>
                                        <?php } ?>
                                    <?php } ?>
                                </h3>
                                <div><?php echo $model->current_job ?></div>
                                <div><?php echo $model->location ?></div>

                                <span class="profile-link">URL: <?php echo "https://" . $_SERVER['SERVER_NAME'] . '/' . $profilemodel->slug ?></span>
                                <?php
                                if (Yii::app()->user->getState('role') == "employer") {
                                    $empusrmodel = EmployerUsers::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
                                    $empmodel = Employers::model()->findByPk($empusrmodel->employer_id);
                                    ?>
                                    <?php if (Membership::subscribed($empmodel->stripe_active, $empmodel->trial_ends_at, $empmodel->subscription_ends_at)) { ?>
                                        <div>
                                            Mobile: <?php echo ($model->tel != null ? $model->tel : '') ?>
                                        </div>
                                    <?php } ?>                                    

                                <?php } ?>
                                <ul class="ProfileEdit__details__meta list-inline">
                                    <?php
                                    $pcri = new CDbCriteria();
                                    $pcri->alias = 'pv';
                                    $pcri->join = 'inner join cv16_users u on u.id = pv.model_id';
                                    $pcri->condition = "pv.profile_id = " . $profilemodel->id . " and model_type like '%employer%'";
                                    $promodel = ProfileViews::model()->findAll($pcri);

                                    $fcri = new CDbCriteria();
                                    $fcri->alias = 'pv';
                                    $fcri->join = 'inner join cv16_users u on u.id = pv.favourite_id';
                                    $fcri->condition = "pv.profile_id = " . $profilemodel->id;
                                    $pfmodel = ProfileFavourites::model()->findAll($fcri);
                                    if (Membership::subscribed($model->stripe_active, $model->trial_ends_at, $model->subscription_ends_at)) {
                                        ?>
                                        <li><i class="fa fa-eye"></i><a href="<?php echo $this->createUrl('users/profileViews', array('id' => $profilemodel->id)); ?>"><?php echo count($promodel) ?> Views</a></li>
                                        <li><i class="fa fa-heart"></i><a href="<?php echo $this->createUrl('users/favourites', array('id' => $profilemodel->id)); ?>"><?php echo count($pfmodel) ?> Favourites</a></li>
                                    <?php } else {
                                        ?>
                                        <li><i class="fa fa-eye"></i><?php echo count($promodel) ?> Views</li>
                                        <li><i class="fa fa-heart"></i><?php echo count($pfmodel) ?> Favourites</li>
                                    <?php } ?>
                                </ul><!-- .ProfileEdit__details__meta -->
                            </div><!-- .ProfileEdit__details -->
                        </div>
                        <?php
                        if (Yii::app()->user->getState('role') == "employer"):
                            $empusrmodel = EmployerUsers::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
                            $empmodel = Employers::model()->findByPk($empusrmodel->employer_id);
//                             if($empmodel->stripe_active):
                            ?>
                            <div class="col-sm-3 col-md-4 col-lg-4">
                                <div class="ProfileEdit__operations">
                                    <div>
                                        <a class="OpenMessageBox btn btn-default" id="sendmsg">Send Message</a>
                                    </div>
                                </div>
                            </div>
                            <?php //endif; ?>
                        <?php endif; ?>
                        <?php if (Yii::app()->user->getState('role') == "admin"):
                            ?>
                            <div class="col-sm-3 col-md-4 col-lg-4">

                                <div class="ProfileEdit__operations">

                                    <?php if (Membership::subscribed($model->stripe_active, $model->trial_ends_at, $model->subscription_ends_at) && $model->subscription_ends_at != null && Membership::onGracePeriod($model->subscription_ends_at)) { ?> 
                                        - <small class="subscription-marker subscription-marker--premium">Premium Account</small>
                                        <div>
                                            <a class="btn btn-default profileBasic" style="text-align: center"><i class="fa fa-check"></i> Basic</a>
                                            <a class="btn btn-default extendSubscription btn-success" style="text-align: center"><i class="fa fa-calendar"></i> Extend Subscription</a>
                                        </div>
                                    <?php } else if (Membership::subscribed($model->stripe_active, $model->trial_ends_at, $model->subscription_ends_at) && $model->trial_ends_at != null && Membership::onTrail($model->trial_ends_at)) { ?>
                                        - <small class="subscription-marker subscription-marker--basic">Free Trail Account</small>
                                        <div>
                                            <a class="btn btn-default profileBasic" style="text-align: center"><i class="fa fa-check"></i> Basic</a>
                                            <a class="btn btn-default extendTrail btn-success" style="text-align: center"><i class="fa fa-calendar"></i> Extend Trail</a>
                                        </div>
                                    <?php } else { ?>
                                        - <small class="subscription-marker subscription-marker--basic">Basic Account</small>
                                        <div>
                                            <a class="btn btn-default extendSubscription" style="text-align: center"><i class="fa fa-check"></i> Upgrade</a>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <!-- CV Video -->
                <!-- CV Video -->
                <div class="video-container form-section">
                    <div class="form-section-inner">
                        <div class="ShowVideo">
                            <?php
                            $pvmodel = ProfileVideos::model()->findByAttributes(array('profile_id' => $profilemodel->id, 'is_default' => 1));
                            if ($pvmodel != null) {
                                $bucket = "cvvid-new";
                                $videomodel = Videos::model()->findByPk($pvmodel->video_id);
                                $url = 'https://' . Yii::app()->params['AWS_BUCKET'] . '.s3.amazonaws.com/' . $videomodel->video_id;
                                ?>
                                <style>

                                    #my-video {
                                        font-size: 0.9em;
                                        height: 0;
                                        overflow: hidden;
                                        padding-bottom: 53%;
                                        padding-top: 20px;
                                        position: relative;
                                        width:100%!important;
                                    }
                                    #my-video embed, #my-video object, #my-video iframe {
                                        max-height: 100%;
                                        max-width: 100%;
                                        height: 100%;
                                        left: 0pt;
                                        position: absolute;
                                        top: 0pt;
                                        width: 100%;
                                    }
                                    .jwplayer {
                                        width: 100%!important;}
                                    </style>
                                    <div id="my-video"></div>
                                <script type="text/javascript">
                                    jwplayer("my-video").setup({
                                        'file': '<?php echo $url; ?>', //'https://s3.us-east-2.amazonaws.com/cvvid/small.mp4',
                                    });

                                </script>
                                <div class="pull-right" style="padding-top: 5px;">
                                    <?php
                                    if (Yii::app()->user->getState('role') == "employer") {
                                        $empprofiles = Profiles::model()->findByAttributes(array('owner_id' => $empmodel->id, 'type' => 'employer'));
                                        $plikes = ProfileLikes::model()->findByAttributes(array('profile_id' => $empprofiles->id, 'like_id' => $profilemodel->id));
                                        if ($plikes == null) {
                                            ?>
                                            <a href="javascript:void(0)" class="like_btn modal-title" >Like <i title="Like Profile Video" class="fa fa-thumbs-o-up" style="font-size:24px"></i></a>
                                        <?php } else {
                                            ?>                                    
                                            <a href="javascript:void(0)" class="like_btn modal-title" >UnLike <i title="Unlike Profile Video" class="fa fa-thumbs-up" style="font-size:24px"></i></a>
                                            <?php
                                        }
                                        
                                      
                                        
                                <?php }    ?> 
                                   
                                </div>
                            <?php } else {
                                ?>
                                <p>No video has been uploaded yet</p>
                            <?php } ?>
                              
                        </div>
                    </div>
                </div>   
                <?php
                $statmodel = UserStatement::model()->findAllByAttributes(array('user_id' => $model->id));
                if (count($statmodel) > 0) {
                    ?>
                    <div class="form-section">
                        <div class="form-section-inner">
                            <div class="section-header">
                                <h4>Personal Statement</h4>
                            </div>
                            <div class="section-content">
                                <?php foreach ($statmodel as $stat) { ?>
                                    <div class="profile-item">

                                        <div class="item-description">
                                            <?php echo $stat['description'] ?>
                                        </div>
                                    </div>
                                    <br/>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php
                $docmodel = UserDocuments::model()->findAllByAttributes(array('user_id' => $model->id));
                if (count($docmodel) > 0) {
                    ?>
                    <!-- Supporting Docs -->
                    <div class="form-section">
                        <div class="form-section-inner">
                            <div class="section-header">
                                <h4>Supporting Documentation</h4>
                            </div>
                            <div class="section-content">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="profile-document-category">
                                            <div class="category-block">
                                                <i class="fa fa-book"></i>
                                                <h5>Qualifications (<?php echo count($docmodel) ?>)</h5>
                                            </div>
                                            <?php foreach ($docmodel as $doc) { ?>
                                                <div class="profile-document">
                                                    <i class="fa fa-file"></i>
                                                    <a href="#" target="_blank">
                                                        <?php echo $doc['name'] ?>
                                                    </a>
                                                </div>
                                            <?php } ?> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php
                $skillmodel = ProfileSkills::model()->findAllByAttributes(array('profile_id' => $profilemodel->id));
                if (count($skillmodel) > 0) {
                    $cri = new CDbCriteria();
                    $cri->alias = "s";
                    $cri->select = "s.name,sc.name as skill_category_id,sc.id as id";
                    $cri->join = "inner join cv16_profile_skills ps on ps.skill_id = s.id
                                    inner join cv16_skill_categories sc on sc.id = s.skill_category_id
                                    inner join cv16_profiles p on p.id = ps.profile_id";
                    $cri->condition = "ps.profile_id =" . $profilemodel->id;
                    $skimodel = Skills::model()->findAll($cri);
//                       foreach ($skimodel as $skil)
//                       {
//                       }
                    $cri->group = "sc.id";
                    $scmodel = Skills::model()->findAll($cri);
                    $scarr = array();
                    foreach ($scmodel as $skilc) {
                        $scarr[] = $skilc['id'];
                    }
                    ?>

                    <!-- Supporting Docs -->
                    <div class="form-section">
                        <div class="form-section-inner">
                            <div class="section-header">
                                <h4>Career Experience</h4>
                            </div>
                            <div class="section-content">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="profile-document-category">

                                            <?php
                                            foreach ($scarr as $cat) {
                                                $skillcat = SkillCategories::model()->findByPk($cat);
                                                echo "<b>" . $skillcat->name . " : </b>";
                                                $skillarr = array();
                                                foreach ($skimodel as $skil) {
                                                    if ($cat == $skil['id'])
                                                        $skillarr[] = $skil['name'];
                                                    ?>

                                                    <?php
                                                }
                                                echo implode(",", $skillarr);
                                                echo "<br/><br/>";
                                            }
                                            ?> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <?php
                $expmodel = UserExperiences::model()->findAllByAttributes(array('user_id' => $model->id));
                if (count($expmodel) > 0) {
                    ?>
                    <!-- Experience -->
                    <div class="form-section">
                        <div class="form-section-inner">
                            <div class="section-header">
                                <h4>Professional Experience</h4>
                            </div>
                            <div class="section-content">
                                <div class="profile-container">
                                    <?php foreach ($expmodel as $exp) { ?>
                                        <div class="profile-item">
                                            <div class="item-title">
                                                <h5><?php echo $exp['position'] ?></h5>
                                            </div>
                                            <div class="item-subtitle"><?php echo $exp['company_name'] ?> <span><?php echo $exp['location'] ?></span></div>
                                            <div class="item-dates"><?php echo $exp['start_date'] ?> - <?php echo ($exp['end_date'] == null ? "Present" : $exp['end_date']) ?></div>
                                            <div class="item-description">
                                                <?php echo $exp['description'] ?>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php
                $quamodel = UserQualifications::model()->findAllByAttributes(array('user_id' => $model->id));
                if (count($quamodel) > 0) {
                    ?>
                    <!-- Education -->
                    <div class="form-section">
                        <div class="form-section-inner">
                            <div class="section-header">
                                <h4>Education</h4>
                            </div>
                            <div class="section-content">
                                <?php foreach ($quamodel as $qua) { ?>
                                    <div class="profile-item">
                                        <div class="item-title">
                                            <h5><?php echo $qua['institution'] ?></h5>
                                        </div>
                                        <div class="item-subtitle"><?php echo $qua['level'] ?> <span><?php echo $qua['field'] ?></span></div>
                                        <div class="item-dates"><?php echo $qua['completion_date'] ?></div>
                                        <div class="item-description">
                                            <?php echo $qua['description'] ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php
                $hobmodel = UserHobbies::model()->findAllByAttributes(array('user_id' => $model->id));
                if (count($hobmodel) > 0) {
                    ?>
                    <!-- Hobbies -->
                    <div class="form-section">
                        <div class="form-section-inner">
                            <div class="section-header">
                                <h4>Hobbies and Interests</h4>
                            </div>
                            <div class="section-content">
                                <?php foreach ($hobmodel as $hob) { ?>
                                    <div class="profile-item">
                                        <div class="item-title">
                                            <h5><?php echo $hob['activity'] ?></h5>
                                        </div>		                        
                                        <div class="item-description">
                                            <?php echo $hob['description'] ?>
                                        </div>
                                    </div>
                                    <br/>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <!-- Languages -->
                <?php
                $langmodel = UserLanguages::model()->findAllByAttributes(array('user_id' => $model->id));
                if (count($langmodel) > 0) {
                    ?>
                    <!-- Hobbies -->
                    <div class="form-section">
                        <div class="form-section-inner">
                            <div class="section-header">
                                <h4>Languages</h4>
                            </div>
                            <div class="section-content">
                                <?php foreach ($langmodel as $lang) { ?>
                                    <div class="profile-item">
                                        <div class="item-title">
                                            <h5><b><?php echo ucfirst($lang['name']) . "</b> - " . $lang['proficiency'] ?></h5>
                                        </div>                              

                                    </div>
                                    <br/>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
 
                <!-- Endorsements -->
                  <?php
                          if (Yii::app()->user->getState('role') == "employer") {
                                $cri = new CDbCriteria();
                                $cri->condition = "candidate_id =".$model->id." and employer_id =".$empmodel->id;
                                $cri->order='created_date DESC';
                                $commentlist = Comments::model()->findAll($cri);
                                ?>
                                  <div class="form-section">
                        <div class="form-section-inner">
                            <div class="section-header">
                                <h4>ENDORSEMENTS and REFERENCES</h4>
                            </div>
                            <div class="section-content">
                                <div class="comment-wrap" style="margin-top: 50px;">
<!--                                     <div class="page-header"> 
                                        <h4><small class="pull-right"><?php //echo count($commentlist); ?> comments</small> Comments </h4>
                                   </div>  -->
                                    <div class="comments-list">
                                        
                                        <?php 
                                          
                                          if(count($commentlist) > 0):
                                            foreach ($commentlist as $comment)
                                            {
                                            $emp_model = Employers::model()->findByPk($comment['employer_id']);
                                            $usr_model = Users::model()->findByPk($emp_model->user_id);
                                            $profile_model = Profiles::model()->findByAttributes(array('owner_id' => $usr_model->id, 'type' => $usr_model->type));
                                            if($profile_model->photo_id > 0)
                                            {
                                                $mediamodel = Media::model()->findByPk($profile_model->photo_id);
                                                $src = ((!empty($mediamodel->file_name) && $mediamodel->file_name != null) ? Yii::app()->baseUrl."/images/media/".$profile_model->photo_id."/".$mediamodel->file_name : Yii::app()->baseUrl."/images/profile.png");
                                            } 
                                           else
                                           {
                                               $src = Yii::app()->baseUrl."/images/defaultprofile.jpg";
                                           }
                                        ?>
                                        <div class="clearfix">
                                        <div class="col-sm-1">
                                            <div class="thumbnail">
                                                <img class="img-responsive user-photo" src="<?php echo $src; ?>">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <strong><?php echo $usr_model->forenames." ".$usr_model->surname; ?></strong> <span class="text-muted"> commented <?php echo timeAgo($comment['created_date']); ?></span>
                                                </div>
                                                <div class="panel-body">
                                                    <?php echo $comment['comment']; ?>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        <?php
                                            }
                                          endif;
                                        ?>
                                    </div>
                            
                                 <div class=""><input id="candidate_id" type="hidden" value="<?php echo $model->id; ?>"><textarea class="form-control" rows="10" id="comment-box"  placeholder="Add a comment"></textarea><button class="btn btn-primary" id="comment_save" type="button">Send</button></div>
                                </div>
                                </div>
                                </div>
                                </div>
                                
                                <?php } ?>

            </div>
        </div><!-- /.container -->
    </div>
</div>



<?php
$this->beginWidget(
        'booster.widgets.TbModal', array('id' => 'msgdialog')
);
?>
<div class="modal-header">
    <a class="close" data-dismiss="modal" onclick='closepopup()'>&times;</a>
    <h4 class="msg-title">Send Message</h4>
</div>

<div class="modal-body">

    <div class="MessageForm">
        <div class="alert alert-success" style="display:none;">
            Your message has been successfully sent
        </div>

        <div class="form-group">
            <label for="subject">Subject</label>
            <?php
            echo CHtml::hiddenField('user_id', $model->id, array());
            echo CHtml::label('Subject', '', array());
            echo CHtml::textField('subject', '', array('class' => 'form-control', 'id' => 'subject'));
            ?>
        </div>
        <div class="form-group">
            <label for="message">Message</label>
            <?php
            echo CHtml::label('Message', '', array());
            echo CHtml::textArea('message', '', array('class' => 'form-control', 'id' => 'message', 'rows' => '10'));
            ?> 
        </div>
    </div>


</div>

<div class="modal-footer">
    <?php
    echo CHtml::button('Send', array("class" => "btn btn-default", "id" => "sendmsg_btn"));
    echo CHtml::button('Cancel', array("class" => "btn btn-default", "id" => "msg_cancel"));
    ?>
</div>

<?php $this->endWidget(); ?>


<?php
$this->beginWidget(
        'booster.widgets.TbModal', array('id' => 'extendSubscription')
);
?>
<div class="modal-header">
    <a class="close" data-dismiss="modal" onclick='closepopup()'>&times;</a>
    <h4 class="msg-title">Extend Subscription</h4>
</div>

<div class="modal-body">

    <div class="MessageForm">
        <div class="form-group">
            <label class="control-label">Subscription ends At</label>
            <div class='input-group date subs' id='datetimepicker1'>
                <?php
                $dt = new DateTime();
                $current = $dt->format('m/d/Y H:i');
                ?>
                <input type='text' name="subscription_ends_at" value="<?php echo ($model->subscription_ends_at == null ? $current : date("m/d/Y H:i", strtotime($model->subscription_ends_at))); ?>" id="subscription_ends_at" class="form-control"/>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar open-datetimepicker"></span>
                </span>
            </div>
        </div>

    </div>


</div>

<div class="modal-footer">
    <?php
    echo CHtml::button('Send', array("class" => "btn btn-default", "id" => "subs_ends"));
    echo CHtml::button('Cancel', array("class" => "btn btn-default", "onclick" => "closepopup()"));
    ?>
</div>

<?php $this->endWidget(); ?>


<?php
$this->beginWidget(
        'booster.widgets.TbModal', array('id' => 'extendTrail')
);
?>
<div class="modal-header">
    <a class="close" data-dismiss="modal" onclick='closepopup()'>&times;</a>
    <h4 class="msg-title">Extend Trail Ends</h4>
</div>

<div class="modal-body">

    <div class="MessageForm">
        <div class="form-group">
            <label class="control-label">Trail Ends At</label>
            <div class='input-group date trail' id='datetimepicker1'>
                <?php
                $dt = new DateTime();
                $current = $dt->format('m/d/Y H:i');
                ?>
                <input type='text' name="trial_ends_at" value="<?php echo ($model->trial_ends_at == null ? $current : date("m/d/Y H:i", strtotime($model->trial_ends_at))); ?>" id="trial_ends_at" class="form-control"/>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar open-datetimepicker"></span>
                </span>
            </div>
        </div>
    </div>
</div>

<div class="modal-footer">
    <?php
    echo CHtml::button('Send', array("class" => "btn btn-default", "id" => "trails_end"));
    echo CHtml::button('Cancel', array("class" => "btn btn-default", "onclick" => "closepopup()"));
    ?>
</div>

<?php $this->endWidget(); 

function timeAgo($time_ago)
{
    $time_ago = strtotime($time_ago);
    $cur_time   = time();
    $time_elapsed   = $cur_time - $time_ago;
    $seconds    = $time_elapsed ;
    $minutes    = round($time_elapsed / 60 );
    $hours      = round($time_elapsed / 3600);
    $days       = round($time_elapsed / 86400 );
    $weeks      = round($time_elapsed / 604800);
    $months     = round($time_elapsed / 2600640 );
    $years      = round($time_elapsed / 31207680 );
    // Seconds
    if($seconds <= 60){
        return "just now";
    }
    //Minutes
    else if($minutes <=60){
        if($minutes==1){
            return "one minute ago";
        }
        else{
            return "$minutes minutes ago";
        }
    }
    //Hours
    else if($hours <=24){
        if($hours==1){
            return "an hour ago";
        }else{
            return "$hours hrs ago";
        }
    }
    //Days
    else if($days <= 7){
        if($days==1){
            return "yesterday";
        }else{
            return "$days days ago";
        }
    }
    //Weeks
    else if($weeks <= 4.3){
        if($weeks==1){
            return "a week ago";
        }else{
            return "$weeks weeks ago";
        }
    }
    //Months
    else if($months <=12){
        if($months==1){
            return "a month ago";
        }else{
            return "$months months ago";
        }
    }
    //Years
    else{
        if($years==1){
            return "one year ago";
        }else{
            return "$years years ago";
        }
    }
}
?>
