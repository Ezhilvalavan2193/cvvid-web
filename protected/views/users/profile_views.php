<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs = array(
    'Users' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
    
");
?>
<script>
$(document).ready(function(){

   $('#timescale').change(function(){
		
			var id = $('#profileid').val();
			 $.ajax({
	            url: '<?php echo $this->createUrl('users/filterViewers')  ?>',
	            type: 'POST',
	 	       	data: {time :  $(this).val(),id : id},
	            success: function(data) {
	                  $('.view-list').html(data);	 				
	            },
	 		    error: function(data) {		
	 		        alert(data);
	 		    }
	        });
		
   });
	
});
</script>
<div id="profile">
    <?php
    $employerid = isset($this->employerid) ? $this->employerid : 0;
    $userid = isset($this->userid) ? $this->userid : 0;
    $institutionid = isset($this->institutionid) ? $this->institutionid : 0;
    $edit = isset($this->edit) ? $this->edit : 0;
    $this->renderPartial('//profiles/Cover', array('userid' => $userid, 'employerid' => $employerid, 'institutionid' => $institutionid, 'edit' => $edit));
    ?>
    <?php $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $model->id, 'type' => $model->type)); ?>
    <div id="form-details">
        <div class="container">
            <div class="form-details-inner">
                <div class="form-user-details form-section">
                    <div class="row">
                        <div class="text-center col-sm-4 col-md-3 col-lg-2">
                            <div class="ProfileEdit__photo">
                                <?php
                                if ($profilemodel->photo_id > 0) {
                                    $mediamodel = Media::model()->findByPk($profilemodel->photo_id);
                                    $src = ((!empty($mediamodel->file_name) && $mediamodel->file_name != null) ? Yii::app()->baseUrl . "/images/media/" . $profilemodel->photo_id . "/" . $mediamodel->file_name : Yii::app()->baseUrl . "/images/profile.png");
                                } else {
                                    $src = Yii::app()->baseUrl . "/images/defaultprofile.jpg";
                                }
                                echo CHtml::image($src, "", array("id" => "profile-image", 'alt' => ''));
                                ?>
                                <a class="open-media profile-photo-btn" data-field="photo" data-target="#profile-image">
                                    <i class="fa fa-camera"></i>
                                    Change Photo
                                </a>
                            </div>                    
                            <a class="btn btn-primary" href="<?php echo $this->createUrl('users/profile', array('slug' => $profilemodel->slug)) ?>">
                                <i class="fa fa-search"></i>
                                <span>View Profile</span>
                            </a>
                        </div>
                        <div class="col-sm-5 col-md-5 col-lg-6">
                            <div class="ProfileEdit__details">
                                <h3>
                                    <?php  echo $model->forenames ?> <?php echo $model->surname ?>
                                    <?php
                                    $trail = "";
                                    if ($model->trial_ends_at != null) {
                                        $trail = date('Y-m-d', strtotime($model->trial_ends_at));
                                        $expiry = date('d/m/Y', strtotime($model->trial_ends_at));
                                    }

                                    if(Membership::subscribed($model->stripe_active, $model->trial_ends_at, $model->subscription_ends_at)) {
                                        ?>
                                        - <small class="subscription-marker subscription-marker--premium">Premium Account</small>
                                    <?php } else { ?>
                                        - <small class="subscription-marker subscription-marker--basic">Basic Account</small>
                                    <?php } ?>
                                </h3>
                                <div><?php echo $model->current_job ?></div>
                                <div><?php echo $model->location ?></div>

                                <span class="profile-link">URL: <?php echo "http://" . $_SERVER['SERVER_NAME'] . '/' . $profilemodel->slug ?></span>

                                <ul class="ProfileEdit__details__meta list-inline">
                                    <?php
                                    $pcri = new CDbCriteria();
                                    $pcri->alias = 'pv';
                                    $pcri->join = 'inner join cv16_users u on u.id = pv.model_id';
                                    $pcri->condition = "pv.profile_id = ".$profilemodel->id." and model_type like '%employer%'";
                                    $promodel = ProfileViews::model()->findAll($pcri);
                                    
                                    $fcri = new CDbCriteria();
//                                     $fcri->alias = 'pv';
//                                     $fcri->join = 'inner join cv16_users u on u.id = pv.favourite_id';
                                    $fcri->condition = "favourite_id = ".$profilemodel->id;
                                    $pfmodel = ProfileFavourites::model()->findAll($fcri);
                                    
                                    $lcri = new CDbCriteria();
                                    //$lcri->alias = 'pv';
                                    //$lcri->join = 'inner join cv16_users u on u.id = pv.favourite_id';
                                    $lcri->condition = "like_id = ".$profilemodel->id;
                                    $likemodel = ProfileLikes::model()->findAll($lcri);
                                    
                                    if(Membership::subscribed($model->stripe_active, $model->trial_ends_at, $model->subscription_ends_at)) {                                   
                                        ?>
                                       <li><i class="fa fa-eye"></i><a href="<?php echo $this->createUrl('users/profileViews', array('id' => $profilemodel->id)); ?>"><?php echo count($promodel) ?> Views</a></li>
                                        <li><i class="fa fa-heart"></i><a href="<?php echo $this->createUrl('users/favourites', array('id' => $profilemodel->id)); ?>"><?php echo count($pfmodel)  ?> Favourites</a></li>
                                  		 <li><i class="fa fa-thumb"></i><a href="<?php echo $this->createUrl('users/profileLikes', array('id' => $profilemodel->id)); ?>"><?php echo count($likemodel)  ?> Likes</a></li>
                                    <?php } else {
                                        ?>
                                        <li><i class="fa fa-eye"></i><?php echo count($promodel)  ?> Views</li>
                                        <li><i class="fa fa-heart"></i><?php echo count($pfmodel)  ?> Favourites</li>
                                        <li><i class="fa fa-thumb"></i><?php echo count($likemodel)  ?> Likes</li>
                                    <?php } ?>

                                </ul><!-- .ProfileEdit__details__meta -->
                            </div><!-- .ProfileEdit__details -->               
                        </div>
                        <div class="col-sm-3 col-md-4 col-lg-4">
                            <div class="ProfileEdit__operations">
                                <div>
                                    <?php
                                    $target = Yii::app()->controller->getRoute() . "/" . $model->id;
//                                    free user
                                    if(!Membership::subscribed($model->stripe_active, $model->trial_ends_at, $model->subscription_ends_at))
                                    {
                                    ?>
                                    <a class="btn btn-default btn-upgrade upgrade" href="<?php echo $this->createUrl('users/upgrade', array('target' => $target)) ?>">
                                        <i class="fa fa-user"></i>
                                        <span>Basic</span>
                                    </a>
                                    <?php } else {
                                     if(Membership::onTrail($model->trial_ends_at)){ ?>
                                        <div class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Expires on <?php echo $expiry; ?>">
                                           <i class="fa fa-credit-card"></i>
                                           <span>Premium</span>
                                       </div>
                                      <?php } else { ?>
                                         <a class="btn btn-default" href="<?php echo $this->createUrl('users/mysubscription') ?>">
                                            <i class="fa fa-credit-card"></i>
                                            <span>Account</span>
                                        </a>
                                    <?php } } ?>
                                    
                                    <?php if ($profilemodel->published) { ?>
                                        <a class="Profile__active btn status  btn-success" id="<?php echo $profilemodel->published ?>" href=""><i class="fa fa-check"></i>Active</a>
                                    <?php } else { ?>
                                        <a class="Profile__active btn status btn-default" id="<?php echo $profilemodel->published ?>" href=""><i class="fa fa-close"></i>Inactive</a>
<?php } ?>
                                </div>
                                <div>
                                    <?php 
                                         $age = 0;
                                    if($model->dob != null)
                                    {
                                        $from = new DateTime($model->dob);
                                        $to   = new DateTime('today');
                                        $age = $from->diff($to)->y;
                                    }
                                    if($age != 0 && $age <= 16)
                                        $profilemodel->visibility = 0;
                                     if ($profilemodel->visibility) { ?>
                                        <a class="Profile__visibility btn visiblity  btn-success" id="<?php echo $profilemodel->visibility ?>" href="#"><i class="fa fa-unlock"></i>Public</a>
                                    <?php } else { ?>
                                        <a class="Profile__visibility btn visiblity btn-default" id="<?php echo $profilemodel->visibility ?>" href="#"><i class="fa fa-lock"></i>Private</a>
                                    <?php } ?>
                                    <?php // echo $model->stripe_active; ?>
                                    <?php
                                 //   if (($model->trial_ends_at != null && Membership::onTrail($trail)) || $model->stripe_active) {

                                        $convmem = ConversationMembers::model()->findAllByAttributes(array('user_id' => $profilemodel->owner_id, 'last_read' => null));
                                        ?>
                                        <a class="btn btn-default" href="<?php echo $this->createUrl('users/inbox', array('id' => $model->id)) ?>">
                                            <i class="fa fa-envelope"></i>
                                            <span>Inbox (<?php echo count($convmem) ?>)</span>
                                        </a>
<?php //} ?>
								
                                </div>



                                <div>
                                    <a class="btn btn-default" href="<?php echo $this->createUrl('users/profile', array('slug' => $profilemodel->slug)) ?>">
                                        <i class="fa fa-user"></i>
                                        <span>View</span>
                                    </a>
                                    <a class="btn btn-default" href="<?php echo $this->createUrl('users/userAccountInfo', array('slug' => $profilemodel->slug)) ?>">
                                        <i class="fa fa-pencil"></i>
                                        <span>Account</span>
                                    </a>
                                </div>
                                <div>
                                    <?php if ($profilemodel->hired) { ?>					
                                                    <a class="btn btn-default hired btn-success" href="#"><i class="fa fa-check"></i><span>Hired</span></a>
                                           <?php } else { ?>	 
                                                    <a class="btn btn-default hired" href="#"><i class="fa fa-close"></i><span>Not Hired</span></a>
                                    <?php } ?>
                                </div>
                                
                                 <div>
                                    <?php 
                                        $jamodel = JobApplications::model()->findByAttributes(array('user_id'=>$model->id,'status'=>2));
                                        if($jamodel != null)
                                        {
                                    ?>	 
    									 <a class="btn btn-default" href="<?php echo $this->createUrl('users/interview', array('id' => $jamodel['id'])) ?>">
                                            <i class="fa fa-video-camera"></i>
                                            <span>Video Interview</span>
                                        </a>
                                    <?php } ?>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                    </div>
                </div>

                <div class="form-section">
                    <div class="form-section-inner">
                        <h2>Viewed by</h2>
                        <div class="row">
                   			 <div class="col-lg-3">
                                <div class="ProfileViewsFilters">
                                <!-- Timescale -->
                                <div class="form-group">
                                        <label for="timescale">Timescale Filter</label>
                                        <input type="hidden" id="profileid" value="<?php echo $profilemodel->id ?>"/>
                                        <select class="form-control" id="timescale" name="timescale">
                                        	<option value="" selected="selected">- All -</option>
                                        	<option value="week">Within 7 Days</option>
                                        	<option value="month">Within the last month</option>
                                        	<option value="year">Within the last year</option>
                                        </select>
                                    </div>
                                </div>                   
                              </div>
                            <div class="col-lg-9">
                               		 <div class="view-list">
                                    	 <?php 
                                    	 $pvmodel = ProfileViews::model()->findAllByAttributes(array('profile_id'=>$profilemodel->id));
                                    	 echo $this->renderPartial('viewerslist',array('pvmodel'=>$pvmodel)); ?>
                                    </div><!-- .view-list -->
                              </div><!-- .col-sm-8 -->
                         </div>
                    </div>
                </div>

            </div>
        </div><!-- /.container -->
    </div>
</div>

