<?php
$pvideos = ProfileVideos::model()->findAllByAttributes(array('profile_id' => $profilemodel->id));
foreach ($pvideos as $videos) {
    $mediamodel = Media::model()->findByAttributes(array('model_id' => $videos['video_id']));
    $videomodel = Videos::model()->findByPk($videos['video_id']);
    ?>
    <div class="ProfileVideo">
        <div class="ProfileVideo__inner">
            <div class="row">
                <div class="col-sm-4">
                    <div class="ProfileVideo__thumb">
                        <img src="<?php echo Yii::app()->baseUrl . "/images/media/" . $mediamodel->id . "/" . $mediamodel->file_name ?>" class="img-responsive">

                        <div class="ProfileVideo__default__marker">
                            <i class="fa fa-star"></i>
                        </div>

                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="ProfileVideo__title">
                        <span class="ProfileVideo__title__display"><?php echo $videomodel->name ?>  
                            <a href="#" class="ProfileVideo__edit edit-video-name" id="<?php echo $videomodel->id ?>">Edit</a></span>
                        <div class="ProfileVideo__title__edit input-group" id="ProfileVideo-<?php echo $videomodel->id ?>" style="display: none">
                            <input type="text" class="form-control evideo-name" value="<?php echo $videomodel->name ?>" id="<?php echo $videomodel->id ?>"  >
                            <span class="input-group-btn">
                                <button class="btn btn-primary ProfileVideo__title__submit video-edit-save" type="button" name="video_name_save" id="<?php echo $videomodel->id ?>"><i class="fa fa-check"></i></button>
                                <button class="btn btn-default ProfileVideo__title__cancel video-edit-close" name="video_name_cancel" id="<?php echo $videomodel->id ?>" type="button"><i class="fa fa-close"></i></button>
                            </span>
                        </div>
                    </div>
                    <div class="ProfileVideo__length">Length: <?php echo $videomodel->duration ?></div>
                    <div class="ProfileVideo__size">Is Default: <?php echo $videos['is_default'] == 1 ? "Yes" : "No" ?></div>
                    <div class="ProfileVideo__buttons">
                        <a class="ProfileVideo__default make_default" href="#" id="<?php echo $videomodel->id ?>"> <?php if ($videos['is_default'] == 0) { ?>Make Default<?php } ?></a>
                        <a class="ProfileVideo__delete video_delete" href="#" id="<?php echo $videomodel->id ?>">Delete</a>
                        <a class="ProfileVideo__publish video_publish" href="#" id="<?php echo $videomodel->id ?>">
                            <?php echo ($videos['state'] == 1 ? "Published" : "UnPublish") ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>