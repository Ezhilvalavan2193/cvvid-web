<?php
/* @var $this UsersController */
/* @var $model Users */
?>
<?php
 // using composer https://getcomposer.org/
require Yii::app()->basePath . '/extensions/vendor/autoload.php';
 // JWT PHP Library https://github.com/firebase/php-jwt
 use \Firebase\JWT\JWT;

function generateToken($content,$type) {
    $API_KEY = '5f6360b4-d7c2-2640-e955-e3975cf68b02';
    $API_SECRET = '49801afd-e7f8-9105-325f-a3779dc7ccf1';

    if($type == "POST")
    {
        $payload = array(
            "jti" => getGUID(),
            "iss" => $API_KEY,
            "iat" => time(),
            "sub" => hash('sha256', $content),
            "exp" => time() + 10    // 10 seconds expiration
        );
    }
    else 
    {
        $payload = array(
            "jti" => getGUID(),
            "iss" => $API_KEY,
            "iat" => time(),
            "exp" => time() + 10    // 10 seconds expiration
        );
    }
    return JWT::encode($payload, $API_SECRET);
}

function getGUID(){
    if (function_exists('com_create_guid')){
        return com_create_guid();
    }else{
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);
        $uuid = chr(123)
            .substr($charid, 0, 8).$hyphen
            .substr($charid, 8, 4).$hyphen
            .substr($charid,12, 4).$hyphen
            .substr($charid,16, 4).$hyphen
            .substr($charid,20,12)
            .chr(125);
        return $uuid;
    }
}

function generateInterview () {
   // return generateToken('{}');
    $ch = curl_init('https://interviews.skype.com/api/interviews');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, "{}");
    curl_setopt($ch, CURLOPT_VERBOSE, true);

    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Bearer ' . generateToken('{}','POST'),
        'Content-Type: application/json'
    ));
    $response = curl_exec($ch);
  
    $response = (array)json_decode($response);
    //$result = json_decode($response[0]['tokens'], true);

    curl_close($ch);
    return $response;
}

function getInterviewDetails ($code) {
    // return generateToken('{}');
    $ch = curl_init('https://interviews.skype.com/api/interviews/code/'.$code);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_VERBOSE, true);
    
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization: Bearer ' . generateToken('{}','GET'),
        'Content-Type: application/json'
    ));
    $response = curl_exec($ch);
    
    $response = (array)json_decode($response);
    //$result = json_decode($response[0]['tokens'], true);
    
    curl_close($ch);
    return $response;
}

$skype_resp = generateInterview();
$urls = (array)$skype_resp['urls'][0];
$interview_code = substr($urls['url'], strrpos($urls['url'], '=') + 1);
$_SESSION['CODE'] = $interview_code;

?>
<div align="center">
<h3>Interview Section</h3>
  <p>
  	Please click on below link to start your skype interview session
  </p>
  <a href="<?php echo $urls['url'] ?>" class="btn btn-primary" target="_blank">Start Interview</a>
<?php 
// if(isset($_SESSION['CODE']) && $_SESSION['CODE'] != "")
// {
//          echo var_dump(getInterviewDetails($_SESSION['CODE']));
// }
?>
  
</div>