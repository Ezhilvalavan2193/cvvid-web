<?php
/* @var $this UsersController */
/* @var $model Users */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#users-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});

");
?>
<h1>Dashboard</h1>
<hr>
<h2 style="background-color: #e0ba94;padding: 10px">Statistics</h2>
<?php 
    $usermodel = Users::model()->countByAttributes(array('deleted_at'=>null));   
    $empmodel = Employers::model()->countByAttributes(array('deleted_at'=>null));   
    $jobmodel = Jobs::model()->count();   
    $jobappmodel = JobApplications::model()->countByAttributes(array('deleted_at'=>null));   
    $videomodel = Videos::model()->countByAttributes(array('deleted_at'=>null));   
?>
<div class="divTable stats">
    <div class="divTableBody">
        <div class="divTableRow">
                <div class="divTableCell">Users</div>
                <div class="divTableCell"><?php echo $usermodel ?></div>
        </div>
        <div class="divTableRow">
            <div class="divTableCell">Employers</div>
            <div class="divTableCell"><?php echo $empmodel ?></div>
        </div>
        <div class="divTableRow">
            <div class="divTableCell">Active Jobs</div>
            <div class="divTableCell"><?php echo $jobmodel ?></div>
        </div>
        <div class="divTableRow">
            <div class="divTableCell">Active Applications</div>
            <div class="divTableCell"><?php echo $jobappmodel ?></div>
        </div>
        <div class="divTableRow">
            <div class="divTableCell">Videos</div>
            <div class="divTableCell"><?php echo $videomodel ?></div>
        </div>
    </div>
</div>




