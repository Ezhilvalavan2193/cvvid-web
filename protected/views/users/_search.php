<?php

/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>

<?php

$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl('users/admin', array('trial' => $trial, 'paid' => $paid, 'basic' => $basic)), //Yii::app()->createUrl($this->route),
    'method' => 'get',
        ));
?>

<?php echo CHtml::textField('fullname', $fullname, array('class' => 'form-control', 'placeholder' => 'Search')); ?>

<?php
echo CHtml::submitButton('Search', array("class" => "btn btn-primary vid-marg"));
?>

<?php $this->endWidget(); ?>

