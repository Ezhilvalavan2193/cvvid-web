<?php 
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
");
?> 
<script>
$(document).ready(function(){
	
	$('#slug_field').on('blur', function() {

		 var ele = $(this);
	 	 var value = $(this).val();
	 	 if(value != "")
	 	 {
        	 $.ajax({
                   url: '<?php echo $this->createUrl('users/checkSlug')?>',
                   type: 'POST',
        	       data: {slug : value},
                   success: function(data) {
        				if(data == -1)
                            $(ele).closest('.slug-field').removeClass('has-success').addClass('has-error');
        				else
        					$(ele).closest('.slug-field').removeClass('has-error').addClass('has-success');
                   },
        		    error: function(data) {		
        		      //  alert(data);
        		    }
               });			
	 	 }
	 	 else
	 	 {
	 		$(ele).closest('.slug-field').removeClass('has-success').addClass('has-error');
	 	 }
     	 return false;
    });
});
</script>
<h1><?php echo $model->forenames." ".$model->surname; ?>: Profile</h1>
<div class="Admin__content__inner">
    <div class="UserInformation">
        <ul class="AdminTabs nav nav-tabs">
            <li role="presentation">
                <a href="<?php echo Yii::app()->createUrl("users/tabedit", array("id"=>$model->id)); ?>">Details</a>
            </li>
            <li role="presentation" class=active>
                <a href="<?php echo Yii::app()->createUrl("users/tabprofile", array("id"=>$model->id)); ?>">Profile</a>
            </li>
            <li role="presentation" >
                <a href="<?php echo Yii::app()->createUrl("users/tabdocuments", array("id"=>$model->id)); ?>">Documents</a>
            </li>
            <li role="presentation" >
                <a href="<?php echo Yii::app()->createUrl("users/tabapplications", array("id"=>$model->id)); ?>">Applications</a>
            </li>
            <li role="upgrade" >
                <a href="<?php echo Yii::app()->createUrl("users/tabupgrade", array("id"=>$model->id)); ?>">Upgrade</a>
            </li>
        </ul>
        <div class="row">
            <div class="col-sm-8">
                <div class="UserExperiences">
                    <h3>Professional Experience</h3>
                    <?php
                    $expmodel = UserExperiences::model()->findAllByAttributes(array('user_id' => $model->id));
                    if (count($expmodel) > 0) {

                        $this->widget('zii.widgets.grid.CGridView', array(
                            'id' => 'profile-exp-grid',
                            'itemsCssClass' => 'table',
                            'summaryText' => '',
                           // 'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                            'dataProvider' => ProfileExperiences::model()->searchByID($profilemodel->id),
                            'columns' => array(
                                array(
                                    'header' => 'Company',
                                    'value' => '$data->userExperience->company_name',
                                ),
                                array(
                                    'header' => 'Location',
                                    'value' => '$data->userExperience->location',
                                ),
                                array(
                                    'header' => 'Position',
                                    'value' => '$data->userExperience->position',
                                ),
                                array(
                                    'header' => 'Date',
                                    'value' => '$data->userExperience->start_date',
                                ),
                            ),
                        ));
                    }
                    ?>
                </div>
                <div class="UserQualifications">
                    <h3>Qualifications</h3>
                    <?php
                    $quamodel = UserQualifications::model()->findAllByAttributes(array('user_id' => $model->id));
                    if (count($quamodel) > 0) {
                        $this->widget('zii.widgets.grid.CGridView', array(
                            'id' => 'profile-qua-grid',
                             'itemsCssClass' => 'table',
                            'summaryText' => '',
                            //  'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                            'dataProvider' => ProfileQualifications::model()->searchByID($profilemodel->id),
                            'columns' => array(
                                array(
                                    'header' => 'Institutin',
                                    'value' => '$data->userQualification->institution',
                                ),
                                array(
                                    'header' => 'Location',
                                    'value' => '$data->userQualification->location',
                                ),
                                array(
                                    'header' => 'Field',
                                    'value' => '$data->userQualification->field',
                                ),
                                array(
                                    'header' => 'Level',
                                    'value' => '$data->userQualification->level',
                                ),
                                array(
                                    'header' => 'Completion Date',
                                    'value' => '$data->userQualification->completion_date',
                                ),
                                array(
                                    'header' => 'Grade',
                                    'value' => '$data->userQualification->grade',
                                ),
                            ),
                        ));
                    }
                    ?>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="UserHobbies">
                    <h3>Hobbies</h3>
                    <?php
                    $hobmodel = UserHobbies::model()->findAllByAttributes(array('user_id' => $model->id));
                    if (count($hobmodel) > 0) {
                        $this->widget('zii.widgets.grid.CGridView', array(
                            'id' => 'profile-hob-grid',
                            'itemsCssClass' => 'table',
                            'summaryText' => '',
                            //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                            'dataProvider' => ProfileHobbies::model()->searchByID($profilemodel->id),
                            'columns' => array(
                                array(
                                    'header' => 'Activity',
                                    'value' => '$data->userHobby->activity',
                                ),
                                array(
                                    'header' => 'Description',
                                    'value' => '$data->userHobby->description',
                                ),
                            ),
                        ));
                    }
                    ?>
                </div>                
                <div class="UserLanguages">
                    <h3>Languages</h3>
                    <?php
                    $langmodel = UserLanguages::model()->findAllByAttributes(array('user_id' => $model->id));
                    if (count($langmodel) > 0) {

                        $this->widget('zii.widgets.grid.CGridView', array(
                            'id' => 'profile-lang-grid',
                             'itemsCssClass' => 'table',
                            'summaryText' => '',
                            //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                            'dataProvider' => ProfileLanguages::model()->searchByID($profilemodel->id),
                            'columns' => array(
                                array(
                                    'header' => 'Name',
                                    'value' => '$data->userLanguage->name',
                                ),
                                array(
                                    'header' => 'Proficiency',
                                    'value' => '$data->userLanguage->proficiency',
                                ),
                            ),
                        ));
                    }
                    ?>
                </div>            
            </div>
        </div>
    </div>
</div>

