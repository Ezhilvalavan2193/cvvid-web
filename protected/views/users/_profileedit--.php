<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
    'Candidates'=>array('index'),
    'Manage',
);
?>

<h3>Professional Experience</h3>
<?php 
$expmodel = UserExperiences::model()->findAllByAttributes(array('user_id'=>$model->id));
if(count($expmodel) > 0)
{
  
    $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'profile-exp-grid',
	'summaryText'=>'',
	'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',
    'dataProvider'=>ProfileExperiences::model()->searchByID($profilemodel->id),
        	'columns'=>array(
        	    
        			array(
        					'header'=>'Company',
        					'value'=>'$data->userExperience->company_name',        			        
        			),
        			array(
        					'header'=>'Location',
        			        'value'=>'$data->userExperience->location',
        					
        			),
            	    array(
            	        'header'=>'Position',
            	        'value'=>'$data->userExperience->position',
            	        
            	    ),
            	    array(
            	        'header'=>'Date',
            	        'value'=>'$data->userExperience->start_date',
            	    ),
        	 ),
  ));
    
}
?>

<h3>Professional Qualification</h3>
<?php
$quamodel = UserQualifications::model()->findAllByAttributes(array('user_id' => $model->id));
if (count($quamodel) > 0) {
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'profile-qua-grid',
        'summaryText' => '',
      //  'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'dataProvider' => ProfileQualifications::model()->searchByID($profilemodel->id),
        'columns' => array(
            array(
                'header' => 'Institutin',
                'value' => '$data->userQualification->institution',
            ),
            array(
                'header' => 'Location',
                'value' => '$data->userQualification->location',
            ),
            array(
                'header' => 'Field',
                'value' => '$data->userQualification->field',
            ),
            array(
                'header' => 'Level',
                'value' => '$data->userQualification->level',
            ),
            array(
                'header' => 'Completion Date',
                'value' => '$data->userQualification->completion_date',
            ),
            array(
                'header' => 'Grade',
                'value' => '$data->userQualification->grade',
            ),
        ),
    ));
}
?>

<h3>Hobbies</h3>
<?php 
$hobmodel = UserHobbies::model()->findAllByAttributes(array('user_id'=>$model->id));
if(count($hobmodel) > 0)
{
    $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'profile-hob-grid',
	'summaryText'=>'',
	'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'dataProvider'=>ProfileHobbies::model()->searchByID($profilemodel->id),
        	'columns'=>array(
        	    
        			array(
        					'header'=>'Activity',
        					'value'=>'$data->userHobby->activity',        			        
        			),
        			array(
        					'header'=>'Description',
        			        'value'=>'$data->userHobby->description',        					
        			),
            	  
        	 ),
  ));
}
?>

<h3>Languages</h3>
<?php 
$langmodel = UserLanguages::model()->findAllByAttributes(array('user_id'=>$model->id));
if(count($langmodel) > 0)
{

    $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'profile-lang-grid',
	'summaryText'=>'',
	'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',
    'dataProvider'=>ProfileLanguages::model()->searchByID($profilemodel->id),
        	'columns'=>array(
        	    
        			array(
        					'header'=>'Name',
        					'value'=>'$data->userLanguage->name',        			        
        			),
        			array(
        					'header'=>'Proficiency',
        			        'value'=>'$data->userLanguage->proficiency',        					
        			),
            	  
        	 ),
     ));
}
?>