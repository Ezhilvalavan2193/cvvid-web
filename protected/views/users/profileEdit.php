<?php 
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});

");
?> 
<div>
<?php
//         $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$model->id,'type'=>$model->type));        
//         $addressmodel = Addresses::model()->findByAttributes(array('model_id'=>$model->id));
        if($addressmodel == null)
            $addressmodel = Addresses::model(); 
?>
        
    <div class="live-tab">
            <ul>
                <li data-id="details-section" class="active">Details</li>
                <li data-id="profile-section">Profile</li>
                <li data-id="documents-section">Documents</li>
                <li data-id="application-section">Applications</li>
            </ul>
    </div>
    
   		<section id="details-section" class="live-score-landing">
			
				<?php echo $this->renderPartial('_profiledetails',array('model'=>$model,'profilemodel'=>$profilemodel,'addressmodel'=>$addressmodel,'pwderr'=>$pwderr,'slugerr'=>$slugerr)); ?>
			
		</section>
		<section id="profile-section" class="live-score-landing">
			
				<?php echo $this->renderPartial('_profileedit',array('model'=>$model,'profilemodel'=>$profilemodel,'addressmodel'=>$addressmodel)); ?>
				
		
		</section>
		
		<section id="documents-section" class="live-score-landing">
			
				<?php echo $this->renderPartial('_profiledocuments',array('model'=>$model,'profilemodel'=>$profilemodel,'addressmodel'=>$addressmodel)); ?>
				
			
		</section>
		
		<section id="application-section" class="live-score-landing">
			
				<?php echo $this->renderPartial('_profilejobs',array('model'=>$model,'profilemodel'=>$profilemodel,'addressmodel'=>$addressmodel)); ?>
		
		</section>
 <?php           
//         $this->widget('CTabView', array(
//             'tabs'=>array(
//                 'tab1'=>array(
//                     'title'=>'Details',
//                     'view'=>'_profiledetails',
//                     'data'=>array('model'=>$model,'profilemodel'=>$profilemodel,'addressmodel'=>$addressmodel)
//                 ),
//                 'tab2'=>array(
//                     'title'=>'Profile',
//                     'view'=>'_profiledetails',
//                     'data'=>array('model'=>$model,'profilemodel'=>$profilemodel,'addressmodel'=>$addressmodel)
//                 ),
// //                 'tab3'=>array(
// //                     'title'=>'Document',
// //                     'view'=>'_profiledocuments',
// //                     'data'=>array('model'=>$model,'profilemodel'=>$profilemodel)
// //                 ),
// //                 'tab4'=>array(
// //                     'title'=>'Applications',
// //                     'view'=>'_profileapplications',
// //                     'data'=>array('model'=>$model,'profilemodel'=>$profilemodel)
// //                 ),
//             ),
//            // 'activeTab'=>'tab1',
//             //'cssFile'=>Yii::app()->baseUrl.'/css/jquery.yiitab.css',
// //             'htmlOptions'=>array(
// //                     'style'=>'',
// //                     ),
// //             'id'=>'profile-edit-tabs'    
//         ));
?>								
</div>
