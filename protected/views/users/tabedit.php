<?php 
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
");
?> 
<script>
//$(document).ready(function(){
//	
//	$('#slug_field').on('blur', function() {
//
//		 var ele = $(this);
//	 	 var value = $(this).val();
//	 	 if(value != "")
//	 	 {
//        	 $.ajax({
//                   url: '<?php echo $this->createUrl('users/checkSlug')?>',
//                   type: 'POST',
//        	       data: {slug : value},
//                   success: function(data) {
//        				if(data == -1)
//                            $(ele).closest('.slug-field').removeClass('has-success').addClass('has-error');
//        				else
//        					$(ele).closest('.slug-field').removeClass('has-error').addClass('has-success');
//                   },
//        		    error: function(data) {		
//        		      //  alert(data);
//        		    }
//               });			
//	 	 }
//	 	 else
//	 	 {
//	 		$(ele).closest('.slug-field').removeClass('has-success').addClass('has-error');
//	 	 }
//     	 return false;
//    });
//});
</script>

<h1>Edit Candidate User</h1>
<div class="Admin__content__inner">
    <?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
       <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
    <?php endif; ?>
    <div class="CandidateForm">
        <ul class="AdminTabs nav nav-tabs">
            <li role="presentation" class=active>
                <a href="<?php echo Yii::app()->createUrl("users/tabedit", array("id"=>$model->id)); ?>">Details</a>
            </li>
            <li role="presentation" >
                <a href="<?php echo Yii::app()->createUrl("users/tabprofile", array("id"=>$model->id)); ?>">Profile</a>
            </li>
            <li role="presentation" >
                <a href="<?php echo Yii::app()->createUrl("users/tabdocuments", array("id"=>$model->id)); ?>">Documents</a>
            </li>
            <li role="presentation" >
                <a href="<?php echo Yii::app()->createUrl("users/tabapplications", array("id"=>$model->id)); ?>">Applications</a>
            </li>
           <li role="upgrade" >
                <a href="<?php echo Yii::app()->createUrl("users/tabupgrade", array("id"=>$model->id)); ?>">Upgrade</a>
            </li>
        </ul>

       <?php 
        $meidatmodel = Media::model()->findByPk($profilemodel->id);
    
        $form=$this->beginWidget('CActiveForm', array(
                    	'id'=>'users-form',
                        'action'=>$this->createUrl('users/updateProfileDetails',array('id'=>$model->id)),
                    	'enableAjaxValidation'=>true,
                        'htmlOptions' => array('enctype' => 'multipart/form-data')
                    )); 
        
        
    ?>
        <?php echo  $form->errorSummary(array($model,$profilemodel,$addressmodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
            <div class="row">
                <div class="col-sm-3 col-md-2">
                    <div class="form-group">
                        <label for="photo_id" class="control-label">Profile Photo</label>
                        <div class="photo-section">
                          <?php 
                              if($profilemodel->photo_id > 0)
                                  $url = Yii::app()->baseUrl."/images/media/".$profilemodel->photo_id."/conversions/thumb.jpg";
                              else
                                  $url = Yii::app()->baseUrl."/images/defaultprofile.jpg";
                          ?>
                            <img src="<?php echo $url ?>" alt="" id="profile-pic" class="img-responsive">

                            <input type="hidden" name="photo_id" id="photo_id" value="<?php echo $profilemodel->photo_id ?>">
                            <button type="button" class="add-media btn btn-primary btn-xs" id="photo-btn">Add Logo</button>            
                            <button type="button" class="remove-media btn btn-primary btn-xs" id="remove-photo-btn">Remove Logo</button>
                        </div>
                    </div>
                </div>
                <div class="col-sm-9 col-md-10">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Account Details</h3>
                        </div>
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="forenames" class="control-label">Forenames</label>
                                        <?php echo $form->textField($model,'forenames',array('class'=>'form-control','placeholder'=>'Forenames')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="surname" class="control-label">Surname</label>
                                        <?php echo $form->textField($model,'surname',array('class'=>'form-control','placeholder'=>'Surname')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="email" class="control-label">Email</label>
                                        <?php echo $form->textField($model,'email',array('class'=>'form-control','placeholder'=>'Email')); ?>
                                    </div>
                                </div>
                            </div><!-- .row -->

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="password" class="control-label">Password</label>
                                        <?php echo $model->password = "";?>
                		        <?php echo $form->passwordField($model,'password',array('class'=>'form-control','placeholder'=>'Password')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="password_confirmation" class="control-label">Confirm Password</label>
                                        <input class="form-control" placeholder="Password" name="Users[confirmpassword]" id="Users_confirmpassword" type="password" value="">
                                    </div>
                                </div>
                            </div><!-- .row -->

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="slug" class="control-label">Link</label>
                                        <div class="input-group slug-field">
                                            <span class="input-group-addon"><?php echo "http://" . $_SERVER['SERVER_NAME'] . "/" ?></span>
                                            <?php echo $form->textField($profilemodel, 'slug', array('class' => 'form-control', 'id' => 'slug', 'placeholder' => 'Slug')); ?>
                                            <span class="input-group-addon help-block"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div><!-- .panel-body -->
                    </div><!-- .panel -->        <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Details</h3>
                        </div>
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="dob" class="control-label">Date of Birth</label>
                                        <div class="input-group">
                                            <input type="text" name="dob" class="dob form-control" value="<?php echo ((empty($model->dob) || $model->dob == null || $model->dob == "0000-00-00") ? "" : date("d/m/Y", strtotime($model->dob))); ?>">
                 
                                        </div>
                                    </div>
                                </div><!-- .col-sm-6 -->
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="tel" class="control-label">Telephone</label>
                                        <?php echo $form->textField($model,'tel',array('class'=>'form-control','placeholder'=>'Telephone')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="mobile" class="control-label">Mobile</label>
                                        <?php echo $form->textField($model,'mobile',array('class'=>'form-control','placeholder'=>'Mobile')); ?>
                                    </div>
                                </div><!-- .col-sm-6 -->
                            </div><!-- .row -->

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="current_job" class="control-label">Current Job</label>
                                        <?php echo $form->textField($model,'current_job',array('class'=>'form-control','placeholder'=>'Current Job')); ?>
                                    </div>
                                </div>
                            </div>
                               <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="current_job" class="control-label">Gender</label>
                                            <div class="input-group">
                                             <?php 
                                                $accountStatus = array('M'=>'Male', 'F'=>'Female');
                                                echo $form->radioButtonList($model, 'gender',$accountStatus,array('separator'=>' ')); ?>     
                                                </div>                          
                                        </div>
                                    </div>
                                </div>
                        </div><!-- .panel-body -->
                    </div><!-- .panel -->

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Address</h3>
                        </div>
                        <div class="panel-body">
                            <div id="location-fields">
                            <div class="form-group">
                                <label class="control-label">Street Address</label>
                                <?php echo $form->textField($addressmodel, 'address', array('class' => 'form-control', 'placeholder' => 'Address','id'=>'route')); ?>
                            </div>
                           
                            <div class="form-group">
                                <label class="control-label">Post Code</label>
                                <?php echo $form->textField($addressmodel, 'postcode', array('class' => 'form-control location-search', 'placeholder' => 'Post Code','id'=>'postal_code')); ?>
                                 <input id="location_lat" name="Addresses[latitude]" type="hidden" value="<?php echo $addressmodel->latitude ?>">
                                <input id="location_lng" name="Addresses[longitude]" type="hidden" value="<?php echo $addressmodel->longitude ?>">
                            </div>
                            
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">City</label>
                                        <?php echo $form->textField($addressmodel, 'town', array('class' => 'form-control', 'placeholder' => 'City','id'=>'postal_town')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">County</label>
                                        <?php echo $form->textField($addressmodel, 'county', array('class' => 'form-control', 'placeholder' => 'County','id'=>'administrative_area_level_2')); ?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Country</label>
                                        <?php echo $form->textField($addressmodel, 'country', array('class' => 'form-control', 'placeholder' => 'Country','id'=>'country')); ?>
                                    </div>
                                </div>
                            </div>
                                <?php //$this->renderPartial('//addresses/address-form', array('form'=>$form,'addressmodel'=>$addressmodel)); ?>
                            </div>

                        </div>
                    </div>  
                    
                    <div class="form-actions clearfix">
                        <?php echo CHtml::submitButton('Save',array("class"=>"btn btn-primary pull-right")); ?>
                    </div>
                </div>
            </div>
           <?php $this->endWidget(); ?>

        <div class="row">
            <div class="col-sm-offset-2 col-sm-10">
                <div class="panel panel-primary">
                    <div class="panel-body">
                        <form method="POST" action="<?php echo $this->createUrl('users/deleteCandidate',array('id'=>$model->id))  ?>" accept-charset="UTF-8">
                            <button type="submit" class="btn btn-danger">Delete Account</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

 <script type="text/javascript">
        <?php if(isset($profilemodel->id)){ ?>
        $(function(){
            new CandidateSlugGenerator(<?php echo $profilemodel->id; ?>);
        });
        <?php } ?>
    </script>