<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
	'Employers'=>array('index'),
	'Manage',
);

$this->menu=array(
// 	array('label'=>'List Users', 'url'=>array('index')),
	array('label'=>'Create Employers', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#users-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Employers</h1>


<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'employers-grid',
	'summaryText'=>'',
    'pager' => array(
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'header'=>'',
        'firstPageLabel' => '<<',
        'lastPageLabel' => '>>',
        'nextPageLabel' => '>>',
        'prevPageLabel' => '<<'),
			'ajaxUpdate'=>false,
	'dataProvider'=>$model->searchbyEmployers(),
	//'filter'=>$model,
	'columns'=>array(
			//'id',
			array(
					'header'=>'Name',
					'value'=>'CHtml::link($data->forenames." ".$data->surname,Yii::app()->createUrl("users/employerEdit",array("id"=>$data->id)),array("class"=>"profile-edit-button","id"=>"profile-edit"))',
			        'type'=>'raw'
			),
			array(
					'header'=>'Profile',
					'value'=>'CHtml::link("View",Yii::app()->createUrl("users/profile", array("id"=>$data->id)),array("class"=>"profile-view-button","id"=>"profile-view"))',
					'type'=>'raw'
			),
			array(
					'header'=>'Main User',
					'value'=>'',
			),
		
		array(
			'class'=>'CButtonColumn',
			'template'=>'{del}',
			'header'=>'Action',
			'buttons'=>array
			(
					'del' => array
					(
						'label'=>'<i class="fa fa-trash" aria-hidden="true"></i>',
						'url'=>'Yii::app()->createUrl("users/delete", array("id"=>$data->id))',
					),
			),
		),
	),
)); ?>
