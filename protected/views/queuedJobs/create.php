<?php
/* @var $this QueuedJobsController */
/* @var $model QueuedJobs */

$this->breadcrumbs=array(
	'Queued Jobs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List QueuedJobs', 'url'=>array('index')),
	array('label'=>'Manage QueuedJobs', 'url'=>array('admin')),
);
?>

<h1>Create QueuedJobs</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>