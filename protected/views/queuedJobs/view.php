<?php
/* @var $this QueuedJobsController */
/* @var $model QueuedJobs */

$this->breadcrumbs=array(
	'Queued Jobs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List QueuedJobs', 'url'=>array('index')),
	array('label'=>'Create QueuedJobs', 'url'=>array('create')),
	array('label'=>'Update QueuedJobs', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete QueuedJobs', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage QueuedJobs', 'url'=>array('admin')),
);
?>

<h1>View QueuedJobs #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'queue',
		'payload',
		'attempts',
		'reserved',
		'reserved_at',
		'available_at',
		'created_at',
	),
)); ?>
