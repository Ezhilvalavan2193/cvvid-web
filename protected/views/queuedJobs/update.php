<?php
/* @var $this QueuedJobsController */
/* @var $model QueuedJobs */

$this->breadcrumbs=array(
	'Queued Jobs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List QueuedJobs', 'url'=>array('index')),
	array('label'=>'Create QueuedJobs', 'url'=>array('create')),
	array('label'=>'View QueuedJobs', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage QueuedJobs', 'url'=>array('admin')),
);
?>

<h1>Update QueuedJobs <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>