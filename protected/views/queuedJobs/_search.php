<?php
/* @var $this QueuedJobsController */
/* @var $model QueuedJobs */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'queue'); ?>
		<?php echo $form->textField($model,'queue',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'payload'); ?>
		<?php echo $form->textArea($model,'payload',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'attempts'); ?>
		<?php echo $form->textField($model,'attempts'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'reserved'); ?>
		<?php echo $form->textField($model,'reserved'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'reserved_at'); ?>
		<?php echo $form->textField($model,'reserved_at',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'available_at'); ?>
		<?php echo $form->textField($model,'available_at',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_at'); ?>
		<?php echo $form->textField($model,'created_at',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->