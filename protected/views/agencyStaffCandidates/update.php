<?php
/* @var $this AgencyStaffCandidatesController */
/* @var $model AgencyStaffCandidates */

$this->breadcrumbs=array(
	'Agency Staff Candidates'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AgencyStaffCandidates', 'url'=>array('index')),
	array('label'=>'Create AgencyStaffCandidates', 'url'=>array('create')),
	array('label'=>'View AgencyStaffCandidates', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AgencyStaffCandidates', 'url'=>array('admin')),
);
?>

<h1>Update AgencyStaffCandidates <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>