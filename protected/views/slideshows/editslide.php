
<h1>Edit Slide</h1>
<div class="Admin__content__inner">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'ss-edit-form',
        'action' => Yii::app()->createUrl('slideshows/updateSlide', array('id' => $model->id)),
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data')
    ));
    ?>
    
    <div class="row">
        <div class="col-sm-9">

            <div class="form-group">
                <?php echo $form->textField($model, 'title', array('class' => 'form-control')); ?>
                <?php echo $form->error($model, 'title'); ?>
            </div>

            <div class="form-group">
                <?php echo $form->textArea($model, 'body', array('class' => 'form-control', 'id' => 'content-body')); ?>
                <?php echo $form->error($model, 'body'); ?>
            </div>

        </div>
        <div class="col-sm-3">

            <div class="form-group">
                <?php
                    $mediamodel = Media::model()->findByPk($model->media_id);
                    if($mediamodel != null)
                    {$src = Yii::app()->baseUrl . "/images/media/" . $model->media_id . "/" . $mediamodel->file_name;
                     $display = 1;
                    }
                    else
                        $src = "";
                     $display = 0;
                    ?>					
                    <img src="<?php echo $src ?>" id="profile-pic" class="img-responsive">          
                    <input type="hidden" name="photo_id" id="photo_id" value="<?php echo $model->media_id ?>">
                    <button type="button" class="add-media btn btn-primary btn-xs" id="photo-btn">Add Logo</button>           
                    <button type="button" class="remove-media btn btn-primary btn-xs" id="remove-photo-btn" style="display: <?php echo ($display == 1 ? 'block' : 'none') ?>">Remove Logo</button>
                
            </div>

            <div class="form-group">
                <label class="control-label">Image Horizontal</label>
                <input class="form-control" name="background_x" type="text" value="right">
                <span class="help-block">The horizontal position of the image, can be (left, center, right or percentage value)</span>
            </div>

            <div class="form-group">
                <label class="control-label">Image Vertical</label>
                <input class="form-control" name="background_y" type="text" value="center">
                <span class="help-block">The vertical position of the image, can be (top, center, bottom or percentage value)</span>
            </div>

        </div>
    </div>

    <div class="form-actions">
        <?php
        echo CHtml::submitButton('Save', array('class' => 'btn btn-primary'));
        echo CHtml::button('Back to slideshow', array("class" => "btn btn-primary", "onclick" => "window.location='" . Yii::app()->createUrl('slideshows/edit', array('id' => $slideshowmodel->id)) . "'"));
        ?>
    </div>
    
    <?php $this->endWidget(); ?>
</div>
