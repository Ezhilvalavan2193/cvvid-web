<?php
/* @var $this SlideshowsController */
/* @var $model Slideshows */
/* @var $form CActiveForm */
?>
<h1>Add Slide</h1>
<div class="Admin__content__inner">
    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'ss-form',
    'action'=>Yii::app()->createUrl('slideshows/saveSlide',array('id'=>$ssmodel->id)),
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
    'htmlOptions' => array('enctype' => 'multipart/form-data')
)); ?>
    
    <div class="row">
        <div class="col-sm-9">
            <div class="form-group">
		<?php echo $form->textField($model,'title',array('class'=>'form-control')); ?>
		<?php echo $form->error($model,'title'); ?>
            </div>
            <div class='form-group'>
		<?php echo $form->textArea($model,'body',array('class'=>'form-control','id'=>'content-body')); ?>
		<?php echo $form->error($model,'body'); ?>
            </div>
        </div>
        <div class='col-sm-3'>
            <div class='form-group'>
                <div class="photo-section">
                    <img src="" id="profile-pic" class="img-responsive">
                    <input type="hidden" name="photo_id" id="photo_id" value="">
                    <div class="buttons"><button type="button" class="add-media btn btn-primary btn-xs" id="photo-btn">Add Logo</button>         
                    <button type="button" class="remove-media btn btn-primary btn-xs" id="remove-photo-btn" style="display:none">Remove Logo</button></div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-actions">
        <?php echo CHtml::submitButton('Save',array('class'=>'btn btn-primary')); 
                echo CHtml::button('Back to slideshow',array("class"=>"btn btn-primary","onclick"=>"window.location='".Yii::app()->createUrl('slideshows/edit',array('id'=>$ssmodel->id))."'"));
           ?>
    </div>
    
    <?php $this->endWidget(); ?>
</div>
