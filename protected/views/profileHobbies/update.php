<?php
/* @var $this ProfileHobbiesController */
/* @var $model ProfileHobbies */

$this->breadcrumbs=array(
	'Profile Hobbies'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProfileHobbies', 'url'=>array('index')),
	array('label'=>'Create ProfileHobbies', 'url'=>array('create')),
	array('label'=>'View ProfileHobbies', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ProfileHobbies', 'url'=>array('admin')),
);
?>

<h1>Update ProfileHobbies <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>