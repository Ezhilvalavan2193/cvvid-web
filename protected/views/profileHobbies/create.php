<?php
/* @var $this ProfileHobbiesController */
/* @var $model ProfileHobbies */

$this->breadcrumbs=array(
	'Profile Hobbies'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ProfileHobbies', 'url'=>array('index')),
	array('label'=>'Manage ProfileHobbies', 'url'=>array('admin')),
);
?>

<h1>Create ProfileHobbies</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>