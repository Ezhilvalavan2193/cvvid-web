<?php
/* @var $this InstitutionUsersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Institution Users',
);

$this->menu=array(
	array('label'=>'Create InstitutionUsers', 'url'=>array('create')),
	array('label'=>'Manage InstitutionUsers', 'url'=>array('admin')),
);
?>

<h1>Institution Users</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
