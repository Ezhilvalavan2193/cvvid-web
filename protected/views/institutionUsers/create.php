<?php
/* @var $this InstitutionUsersController */
/* @var $model InstitutionUsers */

$this->breadcrumbs=array(
	'Institution Users'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List InstitutionUsers', 'url'=>array('index')),
	array('label'=>'Manage InstitutionUsers', 'url'=>array('admin')),
);
?>

<h1>Create InstitutionUsers</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>