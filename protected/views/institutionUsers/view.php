<?php
/* @var $this InstitutionUsersController */
/* @var $model InstitutionUsers */

$this->breadcrumbs=array(
	'Institution Users'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List InstitutionUsers', 'url'=>array('index')),
	array('label'=>'Create InstitutionUsers', 'url'=>array('create')),
	array('label'=>'Update InstitutionUsers', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete InstitutionUsers', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage InstitutionUsers', 'url'=>array('admin')),
);
?>

<h1>View InstitutionUsers #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'institution_id',
		'user_id',
		'role',
		'created_at',
		'updated_at',
	),
)); ?>
