<?php
/* @var $this MediaCategoriesController */
/* @var $model MediaCategories */

$this->breadcrumbs=array(
	'Media Categories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MediaCategories', 'url'=>array('index')),
	array('label'=>'Manage MediaCategories', 'url'=>array('admin')),
);
?>

<h1>Create MediaCategories</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>