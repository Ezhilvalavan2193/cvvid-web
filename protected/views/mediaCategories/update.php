<?php
/* @var $this MediaCategoriesController */
/* @var $model MediaCategories */

$this->breadcrumbs=array(
	'Media Categories'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MediaCategories', 'url'=>array('index')),
	array('label'=>'Create MediaCategories', 'url'=>array('create')),
	array('label'=>'View MediaCategories', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MediaCategories', 'url'=>array('admin')),
);
?>

<h1>Update MediaCategories <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>