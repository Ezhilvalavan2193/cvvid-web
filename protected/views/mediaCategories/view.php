<?php
/* @var $this MediaCategoriesController */
/* @var $model MediaCategories */

$this->breadcrumbs=array(
	'Media Categories'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List MediaCategories', 'url'=>array('index')),
	array('label'=>'Create MediaCategories', 'url'=>array('create')),
	array('label'=>'Update MediaCategories', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MediaCategories', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MediaCategories', 'url'=>array('admin')),
);
?>

<h1>View MediaCategories #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'created_at',
		'updated_at',
	),
)); ?>
