<?php
/* @var $this ProfileQualificationsController */
/* @var $model ProfileQualifications */

$this->breadcrumbs=array(
	'Profile Qualifications'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ProfileQualifications', 'url'=>array('index')),
	array('label'=>'Manage ProfileQualifications', 'url'=>array('admin')),
);
?>

<h1>Create ProfileQualifications</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>