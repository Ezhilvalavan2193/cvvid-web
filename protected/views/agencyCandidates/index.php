<?php
/* @var $this AgencyCandidatesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Agency Candidates',
);

$this->menu=array(
	array('label'=>'Create AgencyCandidates', 'url'=>array('create')),
	array('label'=>'Manage AgencyCandidates', 'url'=>array('admin')),
);
?>

<h1>Agency Candidates</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
