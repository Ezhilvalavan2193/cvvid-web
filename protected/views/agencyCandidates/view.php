<?php
/* @var $this AgencyCandidatesController */
/* @var $model AgencyCandidates */

$this->breadcrumbs=array(
	'Agency Candidates'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List AgencyCandidates', 'url'=>array('index')),
	array('label'=>'Create AgencyCandidates', 'url'=>array('create')),
	array('label'=>'Update AgencyCandidates', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete AgencyCandidates', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AgencyCandidates', 'url'=>array('admin')),
);
?>

<h1>View AgencyCandidates #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'agency_id',
		'user_id',
		'role',
		'created_at',
		'updated_at',
	),
)); ?>
