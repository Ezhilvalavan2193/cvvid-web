<?php
/* @var $this AgencyCandidatesController */
/* @var $model AgencyCandidates */

$this->breadcrumbs=array(
	'Agency Candidates'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AgencyCandidates', 'url'=>array('index')),
	array('label'=>'Manage AgencyCandidates', 'url'=>array('admin')),
);
?>

<h1>Create AgencyCandidates</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>