<?php
/* @var $this AgencyCandidatesController */
/* @var $model AgencyCandidates */

$this->breadcrumbs=array(
	'Agency Candidates'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AgencyCandidates', 'url'=>array('index')),
	array('label'=>'Create AgencyCandidates', 'url'=>array('create')),
	array('label'=>'View AgencyCandidates', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage AgencyCandidates', 'url'=>array('admin')),
);
?>

<h1>Update AgencyCandidates <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>