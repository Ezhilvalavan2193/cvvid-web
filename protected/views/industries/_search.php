<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl('industries/searchAdmin'),
	'method'=>'get',
)); ?>

<?php echo $form->textField($model, 'name', array('class' => 'form-control')); ?>
<?php 
echo CHtml::submitButton('Search',array("class"=>'btn btn-primary vid-marg')); 
?>

<?php $this->endWidget(); ?>
