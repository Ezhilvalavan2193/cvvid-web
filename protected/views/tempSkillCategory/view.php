<?php
/* @var $this TempSkillCategoryController */
/* @var $model TempSkillCategory */

$this->breadcrumbs=array(
	'Temp Skill Categories'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List TempSkillCategory', 'url'=>array('index')),
	array('label'=>'Create TempSkillCategory', 'url'=>array('create')),
	array('label'=>'Update TempSkillCategory', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete TempSkillCategory', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TempSkillCategory', 'url'=>array('admin')),
);
?>

<h1>View TempSkillCategory #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'employer_id',
		'skill_category_id',
		'active',
	),
)); ?>
