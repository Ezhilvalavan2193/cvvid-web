<?php
/* @var $this TempSkillCategoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Temp Skill Categories',
);

$this->menu=array(
	array('label'=>'Create TempSkillCategory', 'url'=>array('create')),
	array('label'=>'Manage TempSkillCategory', 'url'=>array('admin')),
);
?>

<h1>Temp Skill Categories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
