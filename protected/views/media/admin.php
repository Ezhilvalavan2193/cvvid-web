<?php
/* @var $this MediaController */
/* @var $model Media */

$this->breadcrumbs = array(
    'Medias' => array('index'),
    'Manage',
);

//$this->menu=array(
//	array('label'=>'List Media', 'url'=>array('index')),
//	array('label'=>'Create Media', 'url'=>array('create')),
//);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#media-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<script>
    $(document).ready(function () {
        $('body').on('click', '#media-photo-uploads', function (e) {
            e.preventDefault();
            $(".media-uploads").trigger('click');
        });

        $('body').on('change', '.media-uploads', function (e) {

            var data = new FormData();
            jQuery.each(jQuery(this)[0].files, function (i, file) {
                data.append('media', file);
            });
            data.append('collection_name', 'sample');
            // alert(JSON.stringify(data));
            $.ajax({
                url: '<?php echo $this->createUrl('media/insertMedia') ?>',
                type: 'POST',
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
//                    alert(data);
                    $("#result_para").load(location.href + " #result_para>*", "");
                    //$('#img-media').attr("src", data);
//                    $('#img-media').src('background-image', 'url(' + data + ')');
                },
                error: function (data) {
                    alert('err');
                }
            });

            return false;
        });

//      media_item_delete  
        $('body').on('click', '.MediaAdmin__item__delete', function (e) {

            if (confirm('Are you sure you want to delete this item?')) {

                var id = $(this).attr('id');

                $.ajax({
                    url: '<?php echo $this->createUrl('media/deleteMedia'); ?>',
                    type: 'POST',
                    data: {id: id},
                    success: function (data) {
                        //alert(data);
                        $(".MediaAdmin__items").load(location.href + " .MediaAdmin__items>*", "");
                    },
                    error: function (data) {
                        alert('err');
                    }
                });
            }

            return false;

        });
        
        
        $(".MediaAdmin__search").keyup(function() {
        
           var val = $(this).val();
           
           $.ajax({
                    url: '<?php echo $this->createUrl('media/getData'); ?>',
                    type: 'POST',
                    data: {val: val},
                    success: function (data) {
                       $('.MediaAdmin__items').html(data);
                    },
                    error: function (data) {
                        alert('err');
                    }
                });
                
         });
         
         $("#load").click(function () {

            var total_row = document.getElementById("total-row").value;
            var result_no = document.getElementById("result_no").value;
            if (total_row < result_no) {
                loadmore();
            } else {
                alert('No More Data..');
                $('#load').hide();
            }
        });


    });
    
    
    function loadmore()
    {
        var val = document.getElementById("result_no").value;
        //alert(val);
        $.ajax({
            type: 'post',
            url: '<?php echo $this->createUrl('media/getLoadData') ?>',
            data: {
                getresult: val
            },
            success: function (response) {
                //alert(response);
                var content = document.getElementById("result_para");
                content.innerHTML = content.innerHTML + response;
                // We increase the value by 20 because we limit the results by 20
                document.getElementById("result_no").value = Number(val) + 20;
            }
        });
    }
</script>
<h1>Media</h1>
<div class="Admin__content__inner">
    <div class="Admin__media-manager">
        <form class="MediaAdmin">
            <div class="MediaAdmin__filters">
                <div class="form-inline">
                    <?php
                        $this->renderPartial('_search', array(
                            'model' => $model,
                        ));
                    ?>
                    <div class="pull-right">
                        <input class="media-uploads" type="file" style="display:none;"/>
                         <?php
                        echo CHtml::button('Add Media', array("class" => "MediaAdmin__add btn btn-primary btn-sm dz-clickable", "id" => "media-photo-uploads"));
                        ?>
                    </div>
                </div>
            </div>
            <div class="MediaAdmin__previews"></div>
            <div class="MediaAdmin__items clearfix" id='result_para'>
                <?php
                $cri = new CDbCriteria();
                $cri->order = 'order_column DESC';
                $cri->limit = 20;
                $cri->offset = 0;
                $model = Media::model()->findAll($cri);
                foreach ($model as $media) {
                    if(file_exists('images/media/' . $media['id'] . '/conversions/thumb.jpg'))
                    {
                    ?>
                <div class="MediaAdmin__item">
                    <div class="MediaAdmin__item__inner">
                        <img id="img-media" src="<?php echo Yii::app()->baseUrl ?>/images/media/<?php echo $media['id'] ?>/conversions/thumb.jpg" alt="<?php echo $media['name'] ?>" class="img-responsive">
                         <button class="MediaAdmin__item__delete" id="<?php echo $media['id'] ?>" type="button"><i class="fa fa-close"></i></button>
                    </div>
                </div>
                <?php }  } ?>
                
            </div>
            <div class="MediaAdmin__actions">
                <input type="hidden" id="result_no" value="20">
                <input type="hidden" id="total-row" value="<?php echo Media::model()->count(); ?>">
                <button type="button" id='load' class="MediaAdmin__load-more btn btn-primary btn-sm pull-right">Load More</button>
            </div>
        </form>
    </div>
</div>

