<?php
/* @var $this AgencyVacanciesController */
/* @var $model AgencyVacancies */

$this->breadcrumbs=array(
	'Agency Vacancies'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AgencyVacancies', 'url'=>array('index')),
	array('label'=>'Manage AgencyVacancies', 'url'=>array('admin')),
);
?>

<h1>Create AgencyVacancies</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>