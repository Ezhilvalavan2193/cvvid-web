<?php
/* @var $this ProfileDocumentsController */
/* @var $model ProfileDocuments */

$this->breadcrumbs=array(
	'Profile Documents'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ProfileDocuments', 'url'=>array('index')),
	array('label'=>'Manage ProfileDocuments', 'url'=>array('admin')),
);
?>

<h1>Create ProfileDocuments</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>