<?php
/* @var $this ProfileDocumentsController */
/* @var $model ProfileDocuments */

$this->breadcrumbs=array(
	'Profile Documents'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProfileDocuments', 'url'=>array('index')),
	array('label'=>'Create ProfileDocuments', 'url'=>array('create')),
	array('label'=>'View ProfileDocuments', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ProfileDocuments', 'url'=>array('admin')),
);
?>

<h1>Update ProfileDocuments <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>