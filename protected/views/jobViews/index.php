<?php
/* @var $this JobViewsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Job Views',
);

$this->menu=array(
	array('label'=>'Create JobViews', 'url'=>array('create')),
	array('label'=>'Manage JobViews', 'url'=>array('admin')),
);
?>

<h1>Job Views</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
