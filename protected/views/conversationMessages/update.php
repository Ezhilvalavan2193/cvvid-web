<?php
/* @var $this ConversationMessagesController */
/* @var $model ConversationMessages */

$this->breadcrumbs=array(
	'Conversation Messages'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ConversationMessages', 'url'=>array('index')),
	array('label'=>'Create ConversationMessages', 'url'=>array('create')),
	array('label'=>'View ConversationMessages', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ConversationMessages', 'url'=>array('admin')),
);
?>

<h1>Update ConversationMessages <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>