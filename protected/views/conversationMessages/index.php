<?php
/* @var $this ConversationMessagesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Conversation Messages',
);

$this->menu=array(
	array('label'=>'Create ConversationMessages', 'url'=>array('create')),
	array('label'=>'Manage ConversationMessages', 'url'=>array('admin')),
);
?>

<h1>Conversation Messages</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
