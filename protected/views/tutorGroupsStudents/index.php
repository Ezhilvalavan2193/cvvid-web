<?php
/* @var $this TutorGroupsStudentsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Tutor Groups Students',
);

$this->menu=array(
	array('label'=>'Create TutorGroupsStudents', 'url'=>array('create')),
	array('label'=>'Manage TutorGroupsStudents', 'url'=>array('admin')),
);
?>

<h1>Tutor Groups Students</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
