<?php
/* @var $this TutorGroupsStudentsController */
/* @var $model TutorGroupsStudents */

$this->breadcrumbs=array(
	'Tutor Groups Students'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List TutorGroupsStudents', 'url'=>array('index')),
	array('label'=>'Manage TutorGroupsStudents', 'url'=>array('admin')),
);
?>

<h1>Create TutorGroupsStudents</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>