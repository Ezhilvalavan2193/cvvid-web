<?php
/* @var $this TutorGroupsStudentsController */
/* @var $model TutorGroupsStudents */

$this->breadcrumbs=array(
	'Tutor Groups Students'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List TutorGroupsStudents', 'url'=>array('index')),
	array('label'=>'Create TutorGroupsStudents', 'url'=>array('create')),
	array('label'=>'View TutorGroupsStudents', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage TutorGroupsStudents', 'url'=>array('admin')),
);
?>

<h1>Update TutorGroupsStudents <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>