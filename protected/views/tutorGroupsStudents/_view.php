<?php
/* @var $this TutorGroupsStudentsController */
/* @var $data TutorGroupsStudents */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('institution_id')); ?>:</b>
	<?php echo CHtml::encode($data->institution_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tutor_group_id')); ?>:</b>
	<?php echo CHtml::encode($data->tutor_group_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('student_id')); ?>:</b>
	<?php echo CHtml::encode($data->student_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('graduation_date')); ?>:</b>
	<?php echo CHtml::encode($data->graduation_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_at')); ?>:</b>
	<?php echo CHtml::encode($data->created_at); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_at')); ?>:</b>
	<?php echo CHtml::encode($data->updated_at); ?>
	<br />


</div>