<?php
/* @var $this AgencyController */
/* @var $model AgencyEmployers */

$this->breadcrumbs=array(
    'Agency'=>array('index'),
    'Manage',
);

// $this->menu=array(
// 	array('label'=>'List Institutions', 'url'=>array('index')),
// 	array('label'=>'Create Institutions', 'url'=>array('create')),
// );

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#inst-admin-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<script>
$(document).ready(function(){
	
	$('#export-candidate').on('click',function() {
	    window.location = '<?php echo $this->createUrl('users/exportCSV') ?>'
	});

	$('.AssignBtn').click(function(e){
		
        if($("#agency-can-grid").find("input:checked").length > 0)
        {
        	$('#assign_candidate_modal').addClass('in');
            $('body').addClass('modal-open');
            $('#assign_candidate_modal').show();       	
        }
        else
        {
           alert('Please select at least one item');
        }
	});

	$('.savebtn').click(function(e){

		 var staffid = $('#staff-select').val(); 
   		 var ids = $.fn.yiiGridView.getChecked("agency-can-grid", "selectedIds");
       	
 			  $.ajax({
 		            url: '<?php echo $this->createUrl('agency/assignCandidates',array('id'=>$agencymodel->id)) ?>',
 		            type: 'POST',
 		            data: {ids : ids, staff_id : staffid}, 	    
 		            success: function(data) {
 		            	$('#assign_candidate_modal').removeClass('in');
 		                $('body').removeClass('modal-open');
 		                $('#assign_candidate_modal').hide();
 		            	alert('Candidate assigned successfully!');
 		            },
 				    error: function(data) {		
 				        alert('err');
 				    }
 		         });
  	 	
	});
	
	$('#delete-candidates').click(function(e){

		
        if($("#agency-can-grid").find("input:checked").length > 0)
        {			
       	 if (confirm('Are you sure you want to delete these items?')) {

        		 var ids = $.fn.yiiGridView.getChecked("agency-can-grid", "selectedIds");
      			  $.ajax({
      		            url: '<?php echo $this->createUrl('agency/deleteCandidates') ?>',
      		            type: 'POST',
      		            data: {ids : ids}, 	    
      		            success: function(data) {
      		            	$.fn.yiiGridView.update('agency-can-grid');
      		            },
      				    error: function(data) {		
      				        alert('err');
      				    }
      		         });
       	 } 
       	
        }
        else
        {
           alert('Please select at least one item');
        }
	});
	
});

</script>
 <div class="Admin__content__inner">         
 						<div class="pull-right">       
 							<?php echo CHtml::button("Add Candidate", array("class" => "btn btn-primary","onclick" => "window.location='" . Yii::app()->createUrl('agency/createcandidate',array("id"=>$agencymodel->id)) . "'")) ?>         
                            <button type="button" class="AssignBtn btn btn-primary">Assign to staff</button>
                            <button type="button" class="DeleteBtn btn btn-danger" id="delete-candidates"><i class="fa fa-close"></i> Delete</button>
                        </div>
                
                  <?php if (Yii::app()->user->hasFlash('assign')): ?>
                <div class="alert alert-success alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <?php echo Yii::app()->user->getFlash('assign'); ?>
                </div>
            		<?php endif; ?>
                    <?php
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'agency-can-grid',
                         'itemsCssClass' => 'table',
                        'summaryText' => '',
                       // 'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                        'enableSorting' => true,
                        'ajaxUpdate' => true,
    'pager' => array(
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'header'=>'',
        'firstPageLabel' => '<<',
        'lastPageLabel' => '>>',
        'nextPageLabel' => '>>',
        'prevPageLabel' => '<<'),
                        'ajaxUpdate' => false,
                        'dataProvider' => (isset($staff_id) ? $model->searchByStaff($staff_id) : $model->search($agencymodel->id)),
                        //'filter'=>$model,
                        'columns' => array(
                            array(
                                'id' => 'selectedIds',
                                'value' => '$data->user_id',
                                'class' => 'CCheckBoxColumn',
                                'selectableRows' => 2,
                                'visible'=>isset($staff_id) ? false : true
                            ),
                            array(
                                'header' => 'Name',
                                'value' => 'CHtml::link("<i class=\"fa fa-pencil\"></i> ".$data->candidate->forenames." ".$data->candidate->surname,Yii::app()->createUrl("agency/editCandidate",array("id"=>$data->id)),array("class"=>"can-edit-button","id"=>"can-edit"))',
                                'type' => 'raw'
                            ),                            
                            array(
                                'header' => 'Email',
                                'value'=>'$data->candidate->email'
                            ),
                            array(
                                'header' => 'Current Job',
                                'value'=>'$data->candidate->current_job'
                            ),
                        ),
                    ));
                    ?>
                  
          
</div>


<!-- Assign candidate dialog -->

<div class="modal fade" id="assign_candidate_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">            
            <div class="MediaManager__header">
                <h3>Assign candidates to staff</h3>
            </div>            
            <div class="MediaManager__content">
             	<div class="col-md-4">
             	 <label>Select Staff</label>
               		 <select name="staff-select" class="form-control" id="staff-select">
                    	<?php 
                    	   $agencystaffmodel = AgencyStaff::model()->findAllByAttributes(array('agency_id'=>$agencymodel->id));
                    	   foreach ($agencystaffmodel as $staff)
                    	   {
                    	       $umodel = Users::model()->findByPk($staff['user_id']);
                    	?>
        					<option value="<?php echo $umodel->id ?>"><?php echo $umodel->forenames." ".$umodel->surname ?></option>
        				
        		  		<?php } ?>
        		    </select>    
    		    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default-btn" data-dismiss="modal" onclick="closepopup()">Close</button>
                <button type="button" class="btn main-btn select-submit savebtn">Save</button>
            </div>

        </div>
    </div>
</div>