<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */

$this->breadcrumbs=array(
    'Institutions'=>array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
    
");
?>

<?php
        $planmodel = Plans::model()->findByAttributes(array('owner_id'=>$model->id));
?>
    <h1>Edit Agency</h1>
    <div class="Admin__content__inner">
        <?php if (Yii::app()->user->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <?php echo Yii::app()->user->getFlash('success'); ?>
        </div>
    <?php endif; ?>
        <div class="InstitutionForm">
            <!-- Nav tabs -->
            <ul class="AdminTabs nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="<?php echo Yii::app()->createUrl("agency/tabedit", array("id" => $model->id)); ?>" aria-controls="details">Details</a></li>
                <li role="presentation"><a href="<?php echo Yii::app()->createUrl("agency/tabcandidates", array("id" => $model->id)); ?>">Candidates</a></li>
                <li role="presentation"><a href="<?php echo Yii::app()->createUrl("agency/tabstaff", array("id" => $model->id)); ?>">Staff</a></li>
                <li role="presentation"><a href="<?php echo Yii::app()->createUrl("agency/tabemployers", array("id" => $model->id)); ?>">Employers</a></li>
                <li role="presentation"><a href="<?php echo Yii::app()->createUrl("agency/tabvacancies", array("id" => $model->id)); ?>">Vacancies</a></li>
                                
            </ul>
            <?php 
                    
                    $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$model->id,'type'=>'agency'));
                   
                    $form = $this->beginWidget('CActiveForm', array(
                                    'id'=>'users-form',
                                    'action'=>$this->createUrl('agency/updateAgency',array('id'=>$model->id)),
                                    'enableAjaxValidation'=>false,
                                    'htmlOptions' => array('enctype' => 'multipart/form-data')
                            ));         

                ?>
                <div class="row">
                    <div class="col-sm-3 col-md-2">
                        <div class="form-group">
                            <label for="photo_id" class="control-label">Profile Photo</label>
                            <div class="photo-section">
                              <?php
                              if($profilemodel->photo_id > 0)
                                  $url = Yii::app()->baseUrl."/images/media/".$profilemodel->photo_id."/conversions/thumb.jpg";
                              else
                                  $url = Yii::app()->baseUrl."/images/defaultprofile.jpg";
                              ?>
                                <img src="<?php echo $url ?>" alt="" id="profile-pic" class="img-responsive">

                                        <input type="hidden" name="photo_id" id="photo_id" value="<?php echo $profilemodel->photo_id?>">
                                <button type="button" class="add-media btn btn-primary btn-xs" id="edit-photo-btn">Add Logo</button>            
                                <button type="button" class="remove-media btn btn-primary btn-xs" id="remove-photo-btn">Remove Logo</button>
                             </div>
                            
                        </div>
                    </div>
                    <div class="col-sm-9 col-md-10">
                        <h4>Account Details</h4>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                       <?php echo $form->textField($usermodel, 'forenames', array('class' => 'form-control','placeholder'=>'First Name')); ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <?php echo $form->textField($usermodel, 'surname', array('class' => 'form-control','placeholder'=>'Surname')); ?>
                                </div>
                            </div>
                             <div class="col-sm-3">
                                <div class="form-group">
                                    <?php echo $form->textField($usermodel, 'email', array('class' => 'form-control','placeholder'=>'Email Address')); ?>
                                </div>
                            </div>
                        </div>
        
                        <div class="row">
                           
                            <div class="col-sm-3">
                                <div class="form-group">
                                     <?php echo $usermodel->password = ""; ?>
                                    <?php echo $form->passwordField($usermodel, 'password', array('class' => 'form-control','placeholder'=>'Password')); ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                     <input class="form-control" placeholder="Confirm Password" name="Users[confirmpassword]" id="Users_confirmpassword" type="password" value="">
                                </div>
                            </div>
                        </div>
                        <h4>Agency details</h4>
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                      <?php echo $form->textField($model, 'name', array('class' => 'form-control','placeholder'=>'Agency Name')); ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                     <?php echo $form->textField($model, 'num_staff', array('class' => 'form-control','placeholder'=>'Number of Staff')); ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <select name="industry_id" class="form-control select2">
        <!--                                 <option selected="selected" disabled>Search Industry</option> -->
                                        <?php
                                        $industrymodel = Industries::model()->findAll();
                                        echo "<option value=''>Select Industry</option>";
                                        foreach ($industrymodel as $ind) {
                                            ?>                  
                                            <option value="<?php echo $ind['id'] ?>" <?php echo ($model->industry_id == $ind['id'] ? "selected" : "") ?>> <?php echo $ind['name'] ?> </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                         
                        </div>
        
                        <div class="form-container">
                            <h4>Address</h4>
                             <div id="location-fields">
                                <?php $this->renderPartial('//addresses/address-form', array('form' => $form, 'addressmodel' => $addressmodel)); ?>
                            </div>
        
                        </div>
                    </div>

                       
                    </div>
                </div>

               <?php echo CHtml::submitButton('Save',array("class"=>"btn btn-primary")); ?>       

             <?php $this->endWidget(); ?>

            <div class="row">
                <div class="col-sm-offset-2 col-sm-10">
                    <div class="panel panel-primary">
                        <div class="panel-body">     
                        		<?php echo CHtml::button('Delete Account',array('class'=>'btn btn-danger',"onclick"=>"window.location='".$this->createUrl('institutions/deleteInstitution',array('id'=>$model->id))."'"))?>                      
                               
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>


