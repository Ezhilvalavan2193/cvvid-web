<?php
/* @var $this SiteController */
?>

<div class="site-content">
    <div class="page-title text-left">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1>   
                        Edit Agency Details
                    </h1>
                </div>
            </div>
        </div>
    </div>
    
    
    <div id="page-content">
        <div class="container">

            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'agency-edit-form',
                'action' => $this->createUrl('agency/updateAgency',array('id'=>$model->id)),
                'enableAjaxValidation' => false,
                'htmlOptions' => array('enctype' => 'multipart/form-data')
            ));
            ?>
             <?php echo  $form->errorSummary(array($model,$usermodel,$addressmodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
                 <!-- Account -->
                <h4>Account Details</h4>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                               <?php echo $form->textField($usermodel, 'forenames', array('class' => 'form-control','placeholder'=>'First Name')); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <?php echo $form->textField($usermodel, 'surname', array('class' => 'form-control','placeholder'=>'Surname')); ?>
                        </div>
                    </div>
                     <div class="col-sm-3">
                        <div class="form-group">
                            <?php echo $form->textField($usermodel, 'email', array('class' => 'form-control','placeholder'=>'Email Address')); ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                   
                    <div class="col-sm-3">
                        <div class="form-group">
                             <?php echo $usermodel->password = ""; ?>
                            <?php echo $form->passwordField($usermodel, 'password', array('class' => 'form-control','placeholder'=>'Password')); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                             <input class="form-control" placeholder="Confirm Password" name="Users[confirmpassword]" id="Users_confirmpassword" type="password" value="">
                        </div>
                    </div>
                </div>
                <h4>Agency details</h4>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                              <?php echo $form->textField($model, 'name', array('class' => 'form-control','placeholder'=>'Agency Name')); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                             <?php echo $form->textField($model, 'num_staff', array('class' => 'form-control','placeholder'=>'Number of Staff')); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <select name="industry_id" class="form-control select2">
<!--                                 <option selected="selected" disabled>Search Industry</option> -->
                                <?php
                                $industrymodel = Industries::model()->findAll();
                                echo "<option value=''>Select Industry</option>";
                                foreach ($industrymodel as $ind) {
                                    ?>                  
                                    <option value="<?php echo $ind['id'] ?>" <?php echo ($model->industry_id == $ind['id'] ? "selected" : "") ?>> <?php echo $ind['name'] ?> </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                 
                </div>

                <div class="form-container">
                    <h4>Address</h4>
                     <div id="location-fields">
                        <?php $this->renderPartial('//addresses/address-form', array('form' => $form, 'addressmodel' => $addressmodel)); ?>
                    </div>

                </div>

                <div class="form-actions">
                     <?php echo CHtml::submitButton('Update', array("class" => "default-btn",'style'=>'')); ?>
                </div>
            <?php $this->endWidget(); ?>
        </div>
    </div>
    