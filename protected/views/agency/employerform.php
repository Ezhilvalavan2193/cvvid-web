<?php
/* @var $this AgencyController */
/* @var $model Agency */
/* @var $form CActiveForm */

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
");
?>
<script>
    $(document).ready(function () {

    });
</script>
<script>
$(document).ready(function(){
$(".noSpace").keydown(function(event){
if(event.keyCode == 32){
    event.preventDefault();
}
});
});
</script>
<div class="page-title text-left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <h1><?php echo $agencyempmodel->isNewRecord ? "Create" : "Edit" ?> Employer</h1>
            </div>
        </div>
    </div>
</div>


<div id="page-content">
    <div class="container">

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'agency-emp-form',
            'action' => ($agencyempmodel->isNewRecord ? $this->createUrl('agency/saveEmployer', array('id' => $model->id)) : $this->createUrl('agency/updateEmployer', array('id' => $agencyempmodel->id))),
            'enableAjaxValidation' => false,
            'htmlOptions' => array('enctype' => 'multipart/form-data')
        ));
        ?>
        <?php echo $form->errorSummary(array($model, $empmodel, $usermodel, $addressmodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <div class="form-container">
                    <!-- Your details -->
                    <h4>Your details</h4>

                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?php echo $form->textField($empmodel, 'name', array('class' => 'form-control', 'placeholder' => 'Company Name')); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?php echo $form->textField($empmodel, 'website', array('class' => 'form-control', 'placeholder' => 'Web Address')); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?php echo $form->textField($empmodel, 'tel', array('class' => 'form-control', 'placeholder' => 'Phone Number')); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <select name="industry_id" class="form-control select2">
                                    <option value=''>Select Industry</option>
                                    <?php
                                    $industrymodel = Industries::model()->findAll();
                                    foreach ($industrymodel as $ind) {
                                        ?>                  
                                        <option value="<?php echo $ind['id'] ?>"> <?php echo $ind['name'] ?> </option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-container">
                        <h4>Address</h4>
                        <div id="location-fields">
                            <?php $this->renderPartial('//addresses/address-form', array('form' => $form, 'addressmodel' => $addressmodel)); ?>
                        </div>

                    </div>

                    <!-- Account -->
                    <h4>Your Account</h4>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?php echo $form->textField($usermodel, 'forenames', array('class' => 'form-control noSpace', 'placeholder' => 'First Name')); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?php echo $form->textField($usermodel, 'surname', array('class' => 'form-control noSpace', 'placeholder' => 'Surname')); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?php echo $form->textField($usermodel, 'current_job', array('class' => 'form-control', 'placeholder' => 'Job Title')); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?php echo $form->textField($usermodel, 'email', array('class' => 'form-control noSpace', 'placeholder' => 'Email Address')); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?php echo $usermodel->password = ""; ?>
                                <?php echo $form->passwordField($usermodel, 'password', array('class' => 'form-control noSpace', 'placeholder' => 'Password')); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <input class="form-control noSpace" placeholder="Confirm Password" name="Users[confirmpassword]" id="Users_confirmpassword" type="password" value="">
                            </div>
                        </div>
                    </div>
                    
  					<!-- <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?php 
//                                     echo $form->checkBox($agencyempmodel, 'add_jobs', array('id'=>'emp_add_job')); 
//                                     echo CHtml::label("Add Jobs", "emp_add_job",array());
                                ?>
                            </div>
                        </div>
                   </div> -->

                    <div class="form-actions clearfix">
                        <div class="pull-left">
                            <?php
                            echo CHtml::submitButton('Save', array("class" => "btn btn-default", 'style' => 'margin-right:10px'));
                            echo CHtml::button('Cancel', array("class" => "btn btn-default", "onclick" => "window.location='" . Yii::app()->createUrl("agency/employers", array('id' => $model->id)) . "'"));
                            ?> 

                        </div>
                    </div>       
                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
    </div>
</div>


