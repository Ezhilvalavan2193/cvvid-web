<?php
/* @var $this AgencyController */
/* @var $model AgencyEmployers */

$this->breadcrumbs=array(
    'Agency'=>array('index'),
    'Manage',
);

// $this->menu=array(
// 	array('label'=>'List Institutions', 'url'=>array('index')),
// 	array('label'=>'Create Institutions', 'url'=>array('create')),
// );

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#inst-admin-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<script>
$(document).ready(function(){
	
	$('#export-candidate').on('click',function() {
	    window.location = '<?php echo $this->createUrl('users/exportCSV') ?>'
	});

});

</script>
<div class="Admin__content__inner">    
						<div class="pull-right">       
 							<?php echo CHtml::button("Add Employer", array("class" => "btn btn-primary", "onclick" => "window.location='" . Yii::app()->createUrl('agency/createemployer',array("id"=>$agencymodel->id)) . "'")) ?>
                        </div>   
                    <?php
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'agency-emp-grid',
                         'itemsCssClass' => 'table',
                        'summaryText' => '',
                       // 'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                        'enableSorting' => true,
                        'ajaxUpdate' => true,
    'pager' => array(
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'header'=>'',
        'firstPageLabel' => '<<',
        'lastPageLabel' => '>>',
        'nextPageLabel' => '>>',
        'prevPageLabel' => '<<'),
                        'ajaxUpdate' => false,
                        'dataProvider' => $model->search($agencymodel->id),
                        //'filter'=>$model,
                        'columns' => array(
                            array(
                                'header' => 'Name',
                                'value' => 'CHtml::link("<i class=\"fa fa-pencil\"></i> ".$data->employer->name,Yii::app()->createUrl("agency/editEmployer",array("id"=>$data->id)),array("class"=>"emp-edit-button","id"=>"emp-edit"))',
                                'type' => 'raw'
                            ),                            
                            array(
                                'header' => 'Email',
                                'value'=>'$data->employer->email'
                            ),
                            array(
                                'header' => 'Website',
                                'value'=>'$data->employer->website'
                            ),
                        ),
                    ));
                    ?>
                   
</div>
           

