<div class="page-title text-left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>   
                    Upgrade <?php echo ucfirst($type)?> Membership
                </h1>
            </div>
        </div>
    </div>
</div>
<div id="page-content">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-1 col-md-10">
                <div class="payment-form-container">
                    <div class="payment-inner">
                      
                        <form method="POST" action="<?php echo $this->createUrl('agency/upgradeUser') ?>" accept-charset="UTF-8" class="payment-form">
                            <div class="row">
                                <div class="col-md-offset-1 col-md-10">
                                    <div class="row">
                                     	<input type="hidden" name="agency_id" value="<?php echo $model->id ?>">
                                     	<input type="hidden" name="user_type" value="<?php echo $type ?>">
                                     	
                                        <?php
//                                       $annplans[] =  Membership::plan('Employer User Monthly Subscription', 'employer_user_monthly')
//                                             ->price(5)
//                                             ->price_suffix('per month')
//                                             ->permissions([
//                                                 'add_user' => 'Add User'
//                                             ]);
                                     // echo var_dump($plans);
                                      //foreach($annplans as $plan){ ?>
                                        <div class="col-md-6" style="float: none;margin: auto;">
                                            <div class="subscription-option selected">
                                                <div class="option-title">Upgrade <?php echo ucfirst($type)?></div>
                                                <?php 
                                                    if($type == "staff")
                                                    {
                                                        $amount = 50;
                                                        $suffix = "per staff";
                                                    }
                                                    else if($type == "employer")
                                                    {
                                                        $amount = 50;
                                                        $suffix = "per employer";
                                                    }
                                                    else if($type == "candidate")
                                                    {
                                                        $amount = 25;
                                                        $suffix = "per candidate";
                                                    }
                                                    else 
                                                    {
                                                        $amount = 100;
                                                        $suffix = "per job";
                                                    }
                                                        
                                                        
                                                ?>
                                                <input type="hidden" name="amount" value="<?php echo $amount ?>">
                                                <div class="option-price">&#163;<?php echo $amount ?><small><?php echo $suffix ?></small></div>
                                               
                                            </div>
                                        </div>
                                       <?php //} ?>
                                    </div>
                                </div>
                            </div><!-- .row -->
                            <div class="row">
                                <div class="col-md-offset-1 col-md-10">
                                    <div class="payment-details-container">

                                        <div class="row">
                                            <div class="col-md-offset-2 col-md-8">

                                                <span class="payment-errors"></span>

                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="selected-subscription">
                                                            <div class="selected-subscription"></div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <div class="input-view">
                                                                <input type="tel" size="20" class="form-control cc-name"
                                                                       placeholder="Cardholder Name" autocomplete="off"/>
                                                                <span class="card-icon"></span>
                                                                <div class="view-icon">
                                                                    <i class="fa fa-pencil-square-o"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!-- .row -->

                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <div class="input-view">
                                                                <input type="tel" size="20" class="form-control cc-number" data-stripe="number"
                                                                       placeholder="Card Number" autocomplete="off"/>
                                                                <span class="card-icon"></span>
                                                                <div class="view-icon">
                                                                    <i class="fa fa-credit-card"></i>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div><!-- .row -->

                                                <div class="row">
                                                    <div class="col-xs-7">
                                                        <div class="form-group form-expiration">
                                                            <span>Expiry</span>
                                                            <input id="cc-exp" type="tel" size="10" class="form-control cc-exp" name="card_expiry" autocomplete="cc-exp"
                                                                   placeholder="MM / YYYY" required>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-5">
                                                        <div class="form-group">
                                                            <div class="input-view cvc-input">
																<span>CVC</span>
                                                                <input type="tel" size="4" class="form-control cc-cvc" data-stripe="cvc"
                                                                        autocomplete="off"/>
<!--                                                                 <div class="view-icon"> -->
<!--                                                                     <i class="fa fa-lock"></i> -->
<!--                                                                 </div> -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button type="submit" class="submit-button btn btn-block main-btn">Submit Payment</button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div><!-- .row -->

                                    </div><!-- .payment-details-container -->
                                </div>
                            </div><!-- .row -->
                           
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    CVPayment.init('<?php echo Yii::app()->params['PUBLIC_Key']; ?>');
</script>