<?php
/* @var $this VideosController */
/* @var $model Videos */
/* @var $form CActiveForm */
?>
<script>
    $(document).ready(function () {

        $('#mark_video').click(function ()
        {
            $('#msgdialog').addClass('in');
            $('body').addClass('modal-open');
            $('#msgdialog').show();
        });

        $('#sendmsg_btn').click(function ()
        {
            var videoid = $("#video_id").val();
            var subject = $("#subject").val();
            var message = $("#message").val();
            if (videoid > 0)
            {
                $.ajax({
                    url: '<?php echo $this->createUrl('videos/markvideo') ?>',
                    type: 'POST',
                    data: {"id": videoid, "subject": subject, "message": message},
                    success: function (data) {
                        closepopup();
                    },
                    error: function (data) {
                        alert(data);
                    }
                });
            }
        });
        
     //   $("#message").tinymce().remove();
    
//        tinymce.get('message_flag').remove();
        
    });

//     tinymce.remove('#message_flag');
</script>
<?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
       <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
    <?php endif; ?>
    <h1>Video</h1>
    <div class="Admin__content__inner">
        <div class="VideoDisplay">
            <div class="row">
                <div class="col-sm-8">
                    <div class="form-inline">
                         <div class="form-group">
                            <label class="control-label">Uploaded By</label>
                            <p class="form-control-static"><?php echo $usermodel == null || $usermodel->forenames == "" ? "Admin" : $usermodel->forenames . " " . $usermodel->surname ?></p>
                        </div>
                        <div class="form-group pull-right">
                            <label class="control-label">Length</label>
                            <p class="form-control-static"><?php echo gmdate('i:s', $model->duration); ?></p>
                        </div>
                    </div>
                    
                    
                    <div class="ShowVideo">
                            <?php
                                $bucket = "cvvid-new";
                                //$videomodel = Videos::model()->findByPk($model->video_id);  
                                $videocatmodel = VideoCategories::model()->findByPk($model->category_id);
                                if($videocatmodel->slug != 'profile')
                                   $url = 'https://' . Yii::app()->params['AWS_BUCKET'] . '.s3.amazonaws.com/'.$videocatmodel->slug.'/' . $model->video_id;
                                else
                                   $url = 'https://' . Yii::app()->params['AWS_BUCKET'] . '.s3.amazonaws.com/' . $model->video_id;
                             ?>
                            <style>
                                                                                                                                                                                                                                                                                                                                                                                                                            
                                #my-video {
font-size: 0.9em;
height: 0;
overflow: hidden;
padding-bottom: 53%;
padding-top: 20px;
position: relative;
width:100%!important;
}
#my-video embed, #my-video object, #my-video iframe {
max-height: 100%;
max-width: 100%;
height: 100%;
left: 0pt;
position: absolute;
top: 0pt;
width: 100%;
}
.jwplayer {
    width: 100%!important;}
                            </style>
                                <div id="my-video"></div>
                                <script type="text/javascript">
                                jwplayer("my-video").setup({
                                 'file': '<?php echo $url; ?>',//'https://s3.us-east-2.amazonaws.com/cvvid/small.mp4',
//                                 width: "100%",
//                                 height: "100%",
                                // 'primary': 'flash',
                                 //'autostart': 'true'
                                });
                                
                       </script>
                    </div>
                    
                    <div class="form-group">
                        <a class="btn btn-default" id="mark_video" href="#">Mark as Inappropriate</a>
                    </div>
                    
                </div>
                <div class='col-sm-4'>
                    <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id' => 'videos-form',
                        'enableAjaxValidation' => false,
                        'action' => $this->createUrl('videos/videoUpdate', array('id' => $model->id)),
                    ));
                    ?>
                    <div class='form-group'>
                        <?php echo $form->labelEx($model, 'name'); ?>
                        <?php echo $form->textField($model, 'name', array('class' => 'form-control', 'required' => 'required')); ?>
                        <?php echo $form->error($model, 'name'); ?> 
                    </div>
                    <div class='form-group'>
                        <?php
                        $vcmodel = VideoCategories::model()->findAll();
                        $vclist = CHtml::listData($vcmodel, 'id', 'name');
                        ?>
                        <?php echo $form->labelEx($model, 'category_id'); ?>
                        <?php echo $form->dropDownList($model, 'category_id', $vclist, array('class' => 'form-control', 'empty' => '- Select Category -', 'required' => 'required')); ?>
                        <?php echo $form->error($model, 'category_id'); ?>
                    </div>
                    <input class="btn btn-primary" type="submit" name="yt1" value="Save Changes" <?php echo ($model->category_id == 1 ? 'disabled' : ''); ?>>
                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>
    </div>

<div id="msgdialog" class="modal fade">
    <div class="modal-dialog">
        <form action="<?php echo $this->createUrl('videos/markvideo') ?>" method="POST">
        <div class="modal-content">
            <div class="modal-header">
                <a class="close" data-dismiss="modal" onclick="closepopup()">×</a>
                <h4 class="msg-title">Flag video as Inappropriate</h4>
            </div>
            <div class="modal-body clearfix">
                <div class="form-group">
                    <input type="hidden" value="<?php echo $model->id; ?>" name="id" id="video_id">
                    <label for="">Subject</label>
                    <input class="form-control" id="subject" type="text" value="" name="subject">    
                </div>
                <div class="form-group">
                    <label for="">Message</label>
                    <textarea class="form-control" id="message_flag" name="message" aria-hidden="true"></textarea> 
                </div>
            </div>
            <div class="modal-footer">
                <input class="btn btn-primary" id="sendmsg_btn" name="yt0" type="submit" value="Send Message">
                <input class="btn btn-default" id="msg_cancel" onclick="closepopup()" name="yt1" type="button" value="Cancel">
            </div>
        </div>
      </form>
    </div>
</div>

