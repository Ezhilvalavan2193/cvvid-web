<?php
/* @var $this VideosController */
/* @var $model Videos */

$this->breadcrumbs=array(
	'Videoses'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Videos', 'url'=>array('index')),
	array('label'=>'Create Videos', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#videos-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Videos</h1>
<script>
    $(document).ready(function () {

        $('.Video__flag').click(function ()
        {
            var id = $(this).attr('id');
            
            $('#msgdialog-'+id).addClass('in');
            $('body').addClass('modal-open');
            $('#msgdialog-'+id).show();
        });
        
        $('.Video__delete').click(function ()
        {
            var id = $(this).attr('id');
            
            $('#deletedialog-'+id).addClass('in');
            $('body').addClass('modal-open');
            $('#deletedialog-'+id).show();
        });


        $('#sendmsg_btn').click(function ()
        {
            var videoid = $("#video_id").val();
            var subject = $("#subject").val();
            var message = $("#message").val();
            if (videoid > 0)
            {
                $.ajax({
                    url: '<?php echo $this->createUrl('videos/markvideo') ?>',
                    type: 'POST',
                    data: {"id": videoid, "subject": subject, "message": message},
                    success: function (data) {
                        closepopup();
                    },
                    error: function (data) {
                        alert(data);
                    }
                });
            }
        });
        
     //   $("#message").tinymce().remove();
    
//        tinymce.get('message_flag').remove();
        
    });

//     tinymce.remove('#message_flag');
</script>
 <?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
       <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
    <?php endif; ?>
<div class="Admin__content__inner">
<?php

$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
        ));
?>
    <div class="AdminFilters">
        <div class="form-inline">
            <div class="form-inline">
                <input class="form-control" placeholder="Search" name="search" type="text" value="<?php echo (isset($_GET['search']) && $_GET['search'] != "" ? $_GET['search'] : '') ?>">
                <select class="form-control" name="category">
                    <option value="" <?php echo (isset($_GET['category']) && $_GET['category'] == "" ? 'selected' : '') ?>>- Please Select -</option>
                    <option value="1" <?php echo (isset($_GET['category']) && $_GET['category'] == "1" ? 'selected' : '') ?>>Profile</option>
                    <option value="2" <?php echo (isset($_GET['category']) && $_GET['category']== "2" ? 'selected' : '') ?>>Success Stories</option>
                    <option value="3" <?php echo (isset($_GET['category'])&& $_GET['category'] == "3" ? 'selected' : '') ?>>How To</option>
                </select>
                <select class="form-control" name="status">
                    <option value="" <?php echo (isset($_GET['status']) && $_GET['status'] == "" ? 'selected' : '') ?>>- Filter Status - </option>
                    <option value="active" <?php echo (isset($_GET['status']) && $_GET['status'] == "active" ? 'selected' : '') ?>>Active</option>
                    <option value="flagged" <?php echo (isset($_GET['status']) && $_GET['status'] == "flagged" ? 'selected' : '') ?>>Flagged</option>
                </select>
                <button class="btn btn-primary">Search</button>
                <a class="btn btn-primary marg-rt" href="<?php echo Yii::app()->createUrl('videos/create'); ?>">Create</a>
            </div>
            
        </div>
    </div>
    <?php $this->endWidget(); ?>
    <table class="table">
            <thead>
                <tr>
                    <th width="20%">Video</th>
                    <th width="10%">Name</th>
                    <th>Category</th>
                    <th>Uploader</th>
                    <th>Length</th>
                    <th>Status</th>
                    <th width="20%">Uploaded On</th>
                    <th width="20%"></th>
                </tr>
            </thead>
            <tbody>
              <?php  
                 $search = isset($_GET['search']) ? $_GET['search'] : "";
                 $category = isset($_GET['category']) ? $_GET['category'] : "";
                 $status = isset($_GET['status']) ? $_GET['status'] : "";
        
                 $cri = new CDbCriteria();
                 
                 if ($search != "")
                     $cri->addCondition("name like '%" . $search . "%'");
                 
                 if($category != "")
                     $cri->addCondition("category_id = '".$category."'");
                 
                 if($status != "")
                     $cri->addCondition("status = '".$status."'");
                 
                 $cri->order='ordering DESC';
                 $videos = Videos::model()->findAll($cri);
                 if(count($videos) > 0)
                 {
                     foreach($videos as $video) 
                     {
                        $mediamodel = Media::model()->findByAttributes(array('model_id'=>$video['id'],'collection_name'=>'video'));
                        if($mediamodel != null)
                            $imgurl = Yii::app()->baseUrl."/images/media/".$mediamodel->id."/conversions/profile.jpg";
                        else
                            $imgurl = "";
                        
                        $umodel = Users::model()->findByPk($video['model_id']);
                        if($umodel != null)
                            $uploader = $umodel->forenames." ".$umodel->surname;
                        else
                            $uploader = "";
              ?>
              <tr>
                    <td>
                          <a href="<?php echo $this->createUrl("videos/edit", array("id"=>$video['id'])); ?>">
                              <img src="<?php echo $imgurl; ?>" alt="" class="img-responsive"/>
                          </a>
                          <input type="hidden" name="ordering[<?php echo $video['id']; ?>]" value="<?php echo $video['id']; ?>"/>
                    </td>
                    <td>
                        <?php if($video['status'] == 'flagged'): ?>
                            <i class="fa fa-flag"></i>
                        <?php endif; ?>
                        <?php echo $video['name']; ?>
                    </td>
                    <td><?php echo $video->category->name; ?></td>
                    <td><?php echo $uploader; ?></td>
                    <td><?php echo $video['duration']; ?></td>
                    <td><?php echo $video['status']; ?></td>
                    <td><?php echo $video['created_at']; ?></td>
                    <td>
                        <a class="Video__flag" id="<?php echo $video['id']; ?>" href="#">Mark as Inappropriate</a>
                        <a class="Video__delete" id="<?php echo $video['id']; ?>" href="#">Delete</a>
                        <div id="msgdialog-<?php echo $video['id']; ?>" class="modal fade">
                            <div class="modal-dialog">
                                <form action="<?php echo $this->createUrl('videos/markvideo') ?>" method="POST">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <a class="close" data-dismiss="modal" onclick="closepopup()">Ã—</a>
                                        <h4 class="msg-title">Flag video as Inappropriate</h4>
                                    </div>
                                    <div class="modal-body clearfix">
                                        <div class="form-group">
                                            <input type="hidden" value="<?php echo $video['id']; ?>" name="id" id="video_id">
                                            <label for="">Subject</label>
                                            <input class="form-control" id="subject" type="text" value="" name="subject">    
                                        </div>
                                        <div class="form-group">
                                            <label for="">Body</label>
                                            <textarea class="form-control" id="message_flag" name="message" aria-hidden="true"></textarea> 
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <input class="btn btn-primary" id="sendmsg_btn" name="yt0" type="submit" value="Send Message">
                                        <input class="btn btn-default" id="msg_cancel" onclick="closepopup()" name="yt1" type="button" value="Cancel">
                                    </div>
                                </div>
                              </form>
                            </div>
                        </div>
                        <div id="deletedialog-<?php echo $video['id']; ?>" class="modal fade">
                            <div class="modal-dialog">
                                <form action="<?php echo $this->createUrl('videos/deletevideo') ?>" method="POST">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <a class="close" data-dismiss="modal" onclick="closepopup()">Ã—</a>
                                        <h4 class="msg-title">Delete Video</h4>
                                    </div>
                                    <div class="modal-body clearfix">
                                        <div class="form-group">
                                            <input type="hidden" value="<?php echo $video['id']; ?>" name="id" id="video_id">
                                            <label for="">Subject</label>
                                            <input class="form-control" id="subject" type="text" value="" name="subject">    
                                        </div>
                                        <div class="form-group">
                                            <label for="">Body</label>
                                            <textarea class="form-control" id="message_delete" name="message" aria-hidden="true"></textarea> 
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <input class="btn btn-primary" id="sendmsg_btn" name="yt0" type="submit" value="Send Message">
                                        <input class="btn btn-default" id="msg_cancel" onclick="closepopup()" name="yt1" type="button" value="Cancel">
                                    </div>
                                </div>
                              </form>
                            </div>
                        </div>
                    </td>
                </tr>
                 <?php } ?>
                 <?php } else { ?>
                    <tr>
                        <td colspan="5">There are currently no videos</td>
                    </tr>
                 <?php } ?>
            </tbody>
        </table>

  
