<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'videos-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation' => false,
        ));
?>
<?php if (Yii::app()->user->hasFlash('VideoError')): ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php echo Yii::app()->user->getFlash('VideoError'); ?>
                        </div>
                    <?php endif; ?>
<div class="form-group">
    <?php echo $form->labelEx($model, 'video_id'); ?>
    <?php echo $form->textField($model, 'video_id', array('class' => 'form-control', 'required' => 'required')); ?>
    <?php echo $form->error($model, 'video_id'); ?>
</div>
<div class="form-group">
    <?php echo $form->labelEx($model, 'name'); ?>
    <?php echo $form->textField($model, 'name', array('class' => 'form-control', 'required' => 'required')); ?>
    <?php echo $form->error($model, 'name'); ?>
</div>
<div class="form-group">
    <?php
    $vcmodel = VideoCategories::model()->findAll();
    $vclist = CHtml::listData($vcmodel, 'id', 'name');
    ?>
    <?php echo $form->labelEx($model, 'category_id'); ?>
    <?php echo $form->dropDownList($model, 'category_id', $vclist, array('class' => 'form-control', 'empty' => '- Select Category -', 'required' => 'required')); ?>
    <?php echo $form->error($model, 'category_id'); ?>
</div>

<?php echo CHtml::submitButton('Upload', array('class' => 'btn btn-primary')); ?>

<?php $this->endWidget(); ?>
