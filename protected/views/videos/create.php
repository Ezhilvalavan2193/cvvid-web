<?php
/* @var $this VideosController */
/* @var $model Videos */

$this->breadcrumbs=array(
	'Videos'=>array('index'),
	'Create',
);

?>

<h1>Upload Video</h1>
<div class="Admin__content__inner">
<?php $this->renderPartial('_form', array('model'=>$model)); ?>
</div>