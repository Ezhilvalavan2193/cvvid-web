<?php
/* @var $this JobAnswersController */
/* @var $model JobAnswers */

$this->breadcrumbs=array(
	'Job Answers'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List JobAnswers', 'url'=>array('index')),
	array('label'=>'Create JobAnswers', 'url'=>array('create')),
	array('label'=>'View JobAnswers', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage JobAnswers', 'url'=>array('admin')),
);
?>

<h1>Update JobAnswers <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>