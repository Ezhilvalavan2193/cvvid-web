<?php
/* @var $this ProfileLanguagesController */
/* @var $model ProfileLanguages */

$this->breadcrumbs=array(
	'Profile Languages'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ProfileLanguages', 'url'=>array('index')),
	array('label'=>'Create ProfileLanguages', 'url'=>array('create')),
	array('label'=>'Update ProfileLanguages', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ProfileLanguages', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProfileLanguages', 'url'=>array('admin')),
);
?>

<h1>View ProfileLanguages #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'profile_id',
		'user_language_id',
		'created_at',
		'updated_at',
	),
)); ?>
