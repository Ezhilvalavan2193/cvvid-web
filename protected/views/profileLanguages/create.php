<?php
/* @var $this ProfileLanguagesController */
/* @var $model ProfileLanguages */

$this->breadcrumbs=array(
	'Profile Languages'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ProfileLanguages', 'url'=>array('index')),
	array('label'=>'Manage ProfileLanguages', 'url'=>array('admin')),
);
?>

<h1>Create ProfileLanguages</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>