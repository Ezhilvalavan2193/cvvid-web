<?php
/* @var $this UserStatementController */
/* @var $model UserStatement */

$this->breadcrumbs=array(
	'User Statements'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List UserStatement', 'url'=>array('index')),
	array('label'=>'Manage UserStatement', 'url'=>array('admin')),
);
?>

<h1>Create UserStatement</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>