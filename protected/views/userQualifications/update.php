<?php
/* @var $this UserQualificationsController */
/* @var $model UserQualifications */

$this->breadcrumbs=array(
	'User Qualifications'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List UserQualifications', 'url'=>array('index')),
	array('label'=>'Create UserQualifications', 'url'=>array('create')),
	array('label'=>'View UserQualifications', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage UserQualifications', 'url'=>array('admin')),
);
?>

<h1>Update UserQualifications <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>