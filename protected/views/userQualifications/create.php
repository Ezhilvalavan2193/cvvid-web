<?php
/* @var $this UserQualificationsController */
/* @var $model UserQualifications */

$this->breadcrumbs=array(
	'User Qualifications'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List UserQualifications', 'url'=>array('index')),
	array('label'=>'Manage UserQualifications', 'url'=>array('admin')),
);
?>

<h1>Create UserQualifications</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>