<?php
/* @var $this UserQualificationsController */
/* @var $model UserQualifications */

$this->breadcrumbs=array(
	'User Qualifications'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List UserQualifications', 'url'=>array('index')),
	array('label'=>'Create UserQualifications', 'url'=>array('create')),
	array('label'=>'Update UserQualifications', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete UserQualifications', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage UserQualifications', 'url'=>array('admin')),
);
?>

<h1>View UserQualifications #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'institution',
		'location',
		'field',
		'level',
		'completion_date',
		'grade',
		'description',
		'created_at',
		'updated_at',
		'deleted_at',
	),
)); ?>
