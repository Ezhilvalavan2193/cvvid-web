<?php 
/* @var $this InstitutionsController */
/* @var $model Institutions */

$this->breadcrumbs=array(
	'Institutions'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
");
?> 
<script>
$(document).ready(function(){

    $(".delete_btn").click(function(e){
        var id = $(this).attr('id');
           	 $.ajax({
                     url: '<?php echo $this->createUrl('institutions/deleteInstitution')?>',
                     type: 'POST',
        	         data: {"id" : id},        	        
                 	 success: function(data) {
                 		alert(data);
                 	 },
        		     error: function(data) {		
        		        alert(data);
        		     }
                  });	
             
    });
    
});
</script>
<div class="row">
<div class="col-sm-3 col-md-2">
  <?php 
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$model->id,'type'=>'institution'));
        $mediamodel = Media::model()->findByPk($profilemodel->id);
    
        $form = $this->beginWidget('CActiveForm', array(
                    	'id'=>'users-form',
                        'action'=>$this->createUrl('institutions/updateInstitution',array('id'=>$model->id)),
                    	'enableAjaxValidation'=>false,
                        'htmlOptions' => array('enctype' => 'multipart/form-data')
                ));         
        
    ?>
                
    <div class="form-group">
                <div style="text-align: center;">Profile Photo</div>
              <div class="photo-section">
                <img src="<?php echo Yii::app()->baseUrl?>/images/media/<?php echo $profilemodel->photo_id?>/conversions/thumb.jpg" alt="" id="profile-pic" class="img-responsive">
            	
          		<input type="hidden" name="photo_id" id="photo_id" value="<?php echo $profilemodel->photo_id?>">
            	<button type="button" class="profile-btn add-media" id="edit-photo-btn">Add Logo</button>            
                <button type="button" class="profile-btn" id="remove-photo-btn">Remove Logo</button>
             </div>
     </div>
</div>


<div class="col-sm-9 col-md-10">
       <div>
            
             <div class="form">
             	<div class="row">
               		<hr>                  
                 	 <h3 class="summary" style="text-align:left;margin-bottom: 20px;">Subscription</h3>
                	 <div class="formBlk col-xs-4">
                 			<div><?php echo CHtml::label('Plan','',array()); ?></div>
                    		<div><?php echo CHtml::label($planmodel->stripe_plan,'',array()); ?></div>   
                 	 </div>
                 	 <div class="formBlk col-xs-4">
                 			<div><?php echo CHtml::label('Card Number','',array()); ?></div>
                    		<div><?php echo CHtml::label("****_****_****_".$model->last_four,'',array()); ?></div>   
                 	 </div>
                 	 <div class="formBlk col-xs-4">
                 			<div><?php echo CHtml::label('Card Expiry','',array()); ?></div>
                    		<div><?php echo CHtml::label($model->card_expiry,'',array()); ?></div>   
                 	 </div>
               </div>
             
                <div class="row">
               		 <h3 class="summary" style="text-align:left;margin-bottom: 20px;">Institution Details</h3>
                  
                <div class="formBlk col-xs-4">
                	<div class="formRow">
                		<?php echo $form->labelEx($model,'name'); ?>
                		<?php echo $form->textField($model,'name',array('class'=>'textBox')); ?>
                	</div>
                </div>
                <div class="formBlk col-xs-4">
                	<div class="formRow">
                		<?php echo $form->labelEx($model,'address_1'); ?>
                		<?php echo $form->textField($model,'address_1',array('class'=>'textBox')); ?>
                	</div>
                </div>
                <div class="formBlk col-xs-4">
                	<div class="formRow">
                		<?php echo $form->labelEx($model,'address_2'); ?>
                		<?php echo $form->textField($model,'address_2',array('class'=>'textBox')); ?>
                	</div>
                </div>
                  <div class="formBlk col-xs-4">
                	<div class="formRow">
                		<?php echo $form->labelEx($model,'town'); ?>
                		<?php echo $form->textField($model,'town',array('class'=>'textBox')); ?>
                 	</div>
                  </div>
                   <div class="formBlk col-xs-4">
                	<div class="formRow">
                		<?php echo $form->labelEx($model,'post_code'); ?>
                		<?php echo $form->textField($model,'post_code',array('class'=>'textBox')); ?>
                 	</div>
                  </div>
                  
                  <div class="formBlk col-xs-4">
                	<div class="formRow">
                		<?php echo $form->labelEx($model,'num_pupils'); ?>
                		<?php echo $form->textField($model,'num_pupils',array('class'=>'textBox')); ?>
                 	</div>
                  </div>
                  
                   <div class="formBlk col-xs-4">
                	<div class="formRow">
                		<?php echo $form->labelEx($model,'status'); ?>
                		<select name="status" id="status" class="select-menu">
		                	<option value="active">Active</option>
		                    <option value="pending">Pending</option>		                
    		            </select>
                 	</div>
                  </div>
                  
                </div>
                <div class="row">
                    <hr>                  
                     <h3 class="summary" style="text-align:left;margin-bottom: 20px;">Admin Account Details</h3>
                     <?php $usermodel->password = ""; ?>
                    <div class="formBlk col-xs-4">
                    	<div class="formRow">
                    		<?php echo $form->labelEx($usermodel,'forenames'); ?>
                    		<?php echo $form->textField($usermodel,'forenames',array('class'=>'textBox')); ?>
                    	</div>
                    </div>
                     
                     <div class="formBlk col-xs-4">
                    	<div class="formRow">
                    		<?php echo $form->labelEx($usermodel,'surname'); ?>
                    		<?php echo $form->textField($usermodel,'surname',array('class'=>'textBox')); ?>
                    	</div>
                    </div>
                    
                      <div class="formBlk col-xs-4">
                    	<div class="formRow">
                    		<?php echo $form->labelEx($usermodel,'email'); ?>
                    		<?php echo $form->textField($usermodel,'email',array('class'=>'textBox')); ?>
                    	</div>
                     </div>
                     
                      <div class="formBlk col-xs-4">
                    	<div class="formRow">
                    		<?php echo $form->labelEx($usermodel,'password'); ?>
                    		<?php echo $form->passwordField($usermodel,'password',array('class'=>'textBox')); ?>
                    	</div>
                     </div>     
                     
                     <div class="formBlk col-xs-4">
                    	<div class="formRow">
                    		<?php echo CHtml::label('Confirm Password','',array()); ?>
                    		<?php echo CHtml::passwordField('Confirm Password','',array('class'=>'textBox')); ?>
                    	</div>
                     </div>                 
                </div>
                
            	 <div class="row buttons">
            		<?php echo CHtml::submitButton('Save',array("class"=>"btn btn-primary")); ?>                	
            		<?php echo CHtml::button('Delete',array("class"=>"btn btn-primary delete_btn","id"=>$model->id,"style"=>"margin-left: 50px;background-color:red")); ?>
            	 </div>
                <?php $this->endWidget(); ?>
                
                </div>
       </div>
       <div class="col-md-push-8 col-md-4">
        
       </div>
</div>
</div>