<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */

$this->breadcrumbs = array(
    'Institutions' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
    
");
?>
<?php
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$model->id,'type'=>$model->type));        
        $addressmodel = Addresses::model()->findByAttributes(array('model_id'=>$model->id));
        if($addressmodel == null)
            $addressmodel = Addresses::model(); 
?>
<h1>Edit Student User</h1>
<div class="Admin__content__inner">
    <div class="CandidateForm">
        <ul class="AdminTabs nav nav-tabs">
            <li role="presentation">
                <a href="<?php echo Yii::app()->createUrl("institutions/tabeditstudent", array("id" => $tgsmodel->id)); ?>">Details</a>
            </li>
            <li role="presentation">
                <a href="<?php echo Yii::app()->createUrl("institutions/tabeditprofile", array("id" => $tgsmodel->id)); ?>">Profile</a>
            </li>
            <li role="presentation" class="active">
                <a href="<?php echo Yii::app()->createUrl("institutions/tabeditdocuments", array("id" => $tgsmodel->id)); ?>">Documents</a>
            </li>
            <li role="presentation">
               <a href="<?php echo Yii::app()->createUrl("institutions/tabeditapplications", array("id" => $tgsmodel->id)); ?>">Applications</a>
            </li>
        </ul>

        <div class="row">
            <div class="col-sm-12">
                <div class="UserExperiences">
                    <h3>Professional Experience</h3>
                    <?php
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'profile-doc-grid',
                         'itemsCssClass' => 'table',
                        'summaryText' => '',
                       // 'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                        'dataProvider' => ProfileDocuments::model()->searchByID($profilemodel->id),
                        'columns' => array(
                            array(
                                'header' => 'Name',
                                'value' => '$data->userDocument->name',
                            ),
                            array(
                                'header' => 'Category',
                                'value' => '$data->userDocument->category->name',
                            ),
                            array(
                                'header' => 'Published',
                                'value' => '$data->userDocument->published',
                            ),
                            array(
                                'header' => 'Uploaded on',
                                'value' => '$data->userDocument->created_at',
                            )
                        ),
                    ));
                    ?>

                </div>

            </div>

        </div>


    </div>
</div>
