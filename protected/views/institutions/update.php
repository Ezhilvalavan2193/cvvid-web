<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */

$this->breadcrumbs=array(
	'Institutions'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Institutions', 'url'=>array('index')),
	array('label'=>'Create Institutions', 'url'=>array('create')),
	array('label'=>'View Institutions', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Institutions', 'url'=>array('admin')),
);
?>

<h1>Update Institutions <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>