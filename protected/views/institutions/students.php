<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */

$this->breadcrumbs=array(
	'Institutions'=>array('index'),
	'Manage',
);

// $this->menu=array(
// 	array('label'=>'List Institutions', 'url'=>array('index')),
// 	array('label'=>'Create Institutions', 'url'=>array('create')),
// );

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#ins-students-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<script>
$(document).ready(function(){
	
	$('#export-students').on('click',function() {
	    window.location = '<?php echo $this->createUrl('institutions/exportStudentsCSV') ?>'
	});
	
});

</script>

    <h1>Students</h1>
    <div class="Admin__content__inner">
        <?php if (Yii::app()->user->hasFlash('success')): ?>
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <?php echo Yii::app()->user->getFlash('success'); ?>
            </div>
        <?php endif; ?>
        <div class="AdminFilters clearfix">
            <div class="pull-left">
                    <div class="form-inline">
                        <?php 
                            $this->renderPartial('_searchstudents',array(
                                'model'=>$model,'insmodel'=>$insmodel,'fullname'=>$fullname
                            ));
                            ?>
                    </div>
            </div>
            <div class="pull-right">
<?php       
        echo CHtml::button('Create',array("class"=>"btn btn-primary","onclick"=>"window.location='".Yii::app()->createUrl('institutions/createIStudent')."'"));
        echo CHtml::button('Export',array("class"=>"btn btn-primary","style"=>"",'id'=>'export-students'));
?>
            </div>
        </div>


        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'ins-students-grid',
               'itemsCssClass' => 'table table-striped',
            'summaryText' => '',
           // 'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
            'enableSorting' => true,
            'ajaxUpdate' => true,
    'pager' => array(
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'header'=>'',
        'firstPageLabel' => '<<',
        'lastPageLabel' => '>>',
        'nextPageLabel' => '>>',
        'prevPageLabel' => '<<'),
            'ajaxUpdate' => false,
            'dataProvider' => ($namesearch > 0 ? $model->searchByName($namearray) : $model->search()),
            //'filter'=>$model,
            'columns' => array(
                array(
                    'header' => 'Name',
                    'value' => 'CHtml::link("<i class=\"fa fa-pencil\"></i> ".$data->student->forenames." ".$data->student->surname,Yii::app()->createUrl("institutions/tabeditstudent",array("id"=>$data->id)),array("class"=>"stud-edit-button","id"=>"profile-edit"))',
                    'type' => 'raw'
                ),
                array(
                    'header' => 'Email',
                    'value' => '$data->student->email',
                ),
                array(
                    'header' => 'School',
                    'value' => '$data->institution->name',
                ),
                array(
                    'header' => 'Graduation Date',
                    'value' => '$data->graduation_date',
                ),
                array(
                    'header' => 'Created At',
                    'value' => '$data->created_at',
                )
            )
        ));
        ?>
    </div>


