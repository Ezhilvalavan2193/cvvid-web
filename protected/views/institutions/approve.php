<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */
/* @var $form CActiveForm */
?>
 <script>

$(document).ready(function () {
    $('#payment_option').change(function()
    {
    	if($(this).val() == "free")
    	{
    		$("#free_sec").show();
    		$("#amount_sec").hide();
    		$("#additional_payment_sec").hide();
    		$("#total_payment_sec").hide();
    	}
    	if($(this).val() == "fixed")
    	{
    		$("#amount_sec").show();
    		$("#additional_payment_sec").hide();
    		$("#total_payment_sec").hide();
    		$("#free_sec").hide();
    	}
    	if($(this).val() == "specific")
    	{
    		$("#amount_sec").show();
    		$("#additional_payment_sec").show();
    		$("#total_payment_sec").show();
    		$("#free_sec").hide();
    	}
    });

    $('#amount').blur(function()
    {
        var amt =  parseInt($(this).val() > 0 ? $(this).val() : 0);
        var addamt = parseInt($("#additional_payment").val() > 0 ? $("#additional_payment").val() : 0);
    	$("#total_payment").val(amt + addamt);
    });
    $('#additional_payment').blur(function()
    {
    	var addamt =  parseInt($(this).val() > 0 ? $(this).val() : 0);
        var amt = parseInt($("#amount").val() > 0 ? $("#amount").val() : 0);
    	$("#total_payment").val(amt + addamt);
    });

    $("#upgrade-request-form").on("submit", function (event) {

    	if($("#ins_status").val() == "pending")
    	{
    		alert("Please change status from 'Pending' to 'Active'.");
			return false;
    	}
		if($("#payment_option").val() == "fixed" && $("#amount").val() <= 0)
		{
			alert("Please enter amount.");
			return false;
		}
		else if($("#payment_option").val() == "specific")
		{
			if($("#amount").val() <= 0)
			{
				alert("Please enter amount.");
				return false;
			}
			if($("#additional_payment").val() <= 0)
			{
				alert("Please enter Additional amount.");
				return false;
			}
		}
		else
			return true;
    });
});

</script>
<h1>Approve Institution Admin</h1>
<div class="Admin__content__inner">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Institution Details</div>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="name" class="control-label">Name</label>
                                <p class="form-control-static"><?php echo $model->name; ?></p>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label">Number of Pupils</label>
                                <p class="form-control-static"><?php echo $model->num_pupils; ?></p>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label">Email</label>
                                <p class="form-control-static"><?php echo $model->email; ?></p>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label">Telephone</label>
                                <p class="form-control-static"><?php echo $usermodel->tel; ?></p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label">Address</label>
                                <p class="form-control-static"><?php echo $model->address_1; ?></p>
                            </div>
                        </div>
                    </div>
                   
			
			
			<div class="col-sm-6 col-md-6">
			 <div class="row">
                    <div id="page-content">
    <div class="container">
       
                <div class="payment-form-container">
             
                    <div class="payment-inner">
                        <form method="POST" id="upgrade-request-form" action="<?php echo $this->createUrl('institutions/upgradeInstitution',array('id'=>$model->id)) ?>" accept-charset="UTF-8" class="payment-form">
                            <div class="row">
                                    <div class="payment-details-container">

                                        <div class="row">
                                        <label class="control-label">Activate / Upgrade Institution</label>
                                   <?php if (Yii::app()->user->hasFlash('success')): ?>
                                                    <div class="alert alert-success alert-dismissable">
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        <?php echo Yii::app()->user->getFlash('success'); ?>
                                                    </div>
                                            	<?php endif; ?>
                                                <span class="payment-errors"></span>

											<div class=row>
												<div class="col-sm-4">
											   <div class="form-group">
                                                        <select class="select2 form-control" id="ins_status" name="status" >
                                                            <option value="pending">Pending</option>
                                                            <option value="active">Active</option>
                                                        </select>
                                                    
                                                	</div>
                                            	</div>
											</div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                                <input type="number" required="required" name="number_students" class="form-control" id="number_students"
                                                                       placeholder="Number of Students" required="required"/>
                                                               
                                                            </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <select name="payment_option" id="payment_option" class="select2 form-control">
                                                         		<option value="free"> Free </option>  
                                                         		<option value="fixed"> Fixed Payment</option>
                                                         		<option value="specific"> Specific Payment</option> 
                                                            </select>
                                                         </div>
                                                    </div>
                                                </div><!-- .row -->
                                              <div class="row" id="free_sec">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <select name="free_date" id="free_date" class="select2 form-control">
                                                         		<option value="month"> 6 Months </option>  
                                                         		<option value="year"> 1 Year</option>
                                                            </select>
                                                         </div>
                                                    </div>
                                                </div><!-- .row -->
												<div class="row" style="display: none;" id="amount_sec">                                                    
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                                <input type="number" name="amount" id="amount" class="form-control" 
                                                                       placeholder="Enter Amount" />                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                
                                                <div class="row" style="display: none;" id="additional_payment_sec">                                                    
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                                <input type="number"  id="additional_payment" name="additional_amount" class="form-control" 
                                                                       placeholder="Additional Payment" />                                                                
                                                            </div>
                                                        </div>
                                                </div>
												<div class="row" style="display: none;" name="total_payment" id="total_payment_sec">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                                <input type="number" readonly="readonly" name="total_amount" class="form-control" id="total_payment"
                                                                       placeholder="Total Payment" />                                                               
                                                            </div>
                                                    </div>
                                                </div><!-- .row -->
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <button type="submit" name="submit" class="btn btn-primary">Submit Payment</button>
                                                    </div>
                                             
                                        </div><!-- .row -->

                                    </div><!-- .payment-details-container -->
                                
                            </div><!-- .row -->
                          
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
			 </div>
       </div>
                
                
            </div><!-- .panel -->
        </div>
    </div>
</div>



