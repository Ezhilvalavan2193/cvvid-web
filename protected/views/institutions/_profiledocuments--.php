<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
    'Candidates'=>array('index'),
    'Manage',
);
?>

<h3>Documents</h3>
<?php 
    $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'profile-doc-grid',
	'summaryText'=>'',
	'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',
    'dataProvider'=>ProfileDocuments::model()->searchByID($profilemodel->id),
        	'columns'=>array(
        	    
            			array(
            					'header'=>'Name',
            					'value'=>'$data->userDocument->name',        			        
            			),
            			array(
            					'header'=>'Category',
            			        'value'=>'$data->userDocument->category->name',
            					
            			),
                	    array(
                	        'header'=>'Published',
                	        'value'=>'$data->userDocument->published',
                	        
                	    ),
                	    array(
                	        'header'=>'Uploaded on',
                	        'value'=>'$data->userDocument->created_at',
                	    )
        	 ),
  ));
?>
 