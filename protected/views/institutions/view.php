<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */

$this->breadcrumbs=array(
	'Institutions'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Institutions', 'url'=>array('index')),
	array('label'=>'Create Institutions', 'url'=>array('create')),
	array('label'=>'Update Institutions', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Institutions', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Institutions', 'url'=>array('admin')),
);
?>

<h1>View Institutions #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'admin_id',
		'name',
		'email',
		'body',
		'address_1',
		'address_2',
		'town',
		'post_code',
		'status',
		'num_pupils',
		'stripe_active',
		'stripe_id',
		'stripe_subscription',
		'stripe_plan',
		'last_four',
		'card_expiry',
		'card_expiry_sent',
		'trial_ends_at',
		'subscription_ends_at',
		'created_at',
		'updated_at',
		'deleted_at',
	),
)); ?>
