<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */

?>

    <div class="page-title text-left">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1>    Edit Profile Details
                    </h1>
                </div>
            </div>
        </div>
    </div>
    <div id="page-content">
        <div class="container">
            <?php $form=$this->beginWidget('CActiveForm', array(
                    'id'=>'inst-acc-form',
                    'action'=>$this->createUrl('institutions/updateAccountInfo',array('id'=>$model->id)),
                    'enableAjaxValidation'=>false,
            )); ?>
            <?php echo  $form->errorSummary(array($model,$usermodel,$pmodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
                <h4>Institution details</h4>
                <div class="form-container">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?php echo $form->textField($model,'name',array('class'=>'form-control','placeholder'=>'Name')); ?>
                            </div>
                        </div>
                       <!-- <div class="col-sm-3">
                            <div class="form-group">
                                <?php echo $form->textField($model,'address_1',array('class'=>'form-control location-search','placeholder'=>'Location')); ?>
                                <input id="location_lat" name="location_lat" type="hidden">
                                <input id="location_lng" name="location_lng" type="hidden">
                            </div>
                        </div> --> 
                        <div class="col-sm-3">
                            <div class="form-group">
                                <input class="form-control" id="website" placeholder="Web Address" name="website" type="text">
                            </div>
                        </div>
                    </div><!-- .row -->
                </div><!-- .form-container -->
		<h4>Address</h4>
                    <div id="location-fields">
                        <div class="form-group">
                            <label class="control-label">Street Address</label>
                            <?php echo $form->textField($addressmodel, 'address', array('class' => 'form-control', 'placeholder' => 'Address','id'=>'route')); ?>
                        </div>
                        
                        <div class="form-group">
                            <label class="control-label">Post Code</label>
                            <?php echo $form->textField($addressmodel, 'postcode', array('class' => 'form-control location-search', 'placeholder' => 'Post Code','id'=>'postal_code')); ?>
                             <input id="location_lat" name="Addresses[latitude]" type="hidden" value="<?php echo $addressmodel->latitude ?>">
                            <input id="location_lng" name="Addresses[longitude]" type="hidden" value="<?php echo $addressmodel->longitude ?>">
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">City</label>
                                    <?php echo $form->textField($addressmodel, 'town', array('class' => 'form-control', 'placeholder' => 'City','id'=>'postal_town')); ?>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">County</label>
                                    <?php echo $form->textField($addressmodel, 'county', array('class' => 'form-control', 'placeholder' => 'County','id'=>'administrative_area_level_2')); ?>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Country</label>
                                    <?php echo $form->textField($addressmodel, 'country', array('class' => 'form-control', 'placeholder' => 'Country','id'=>'country')); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="form-container">
                    <!-- Custom url -->
                    <h4>Custom URL</h4>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-inline slug-field">
                                <div class="form-group"><?php echo 'http://'.$_SERVER['SERVER_NAME'].'/'; ?></div>
                                <div class="form-group">
		                    <?php echo $form->textField($pmodel,'slug',array('class'=>'form-control')); ?>
                                </div>
                                <div class="form-group">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-container">
                    <!-- Your details -->
                    <h4>Your details</h4>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?php echo $form->textField($usermodel,'forenames',array('class'=>'form-control','placeholder'=>'First Name')); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?php echo $form->textField($usermodel,'surname',array('class'=>'form-control','placeholder'=>'Surname')); ?>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <div class="form-group">
                                <div class="form-group">
                                    <?php echo $form->textField($usermodel,'current_job',array('class'=>'form-control','placeholder'=>'Current Job Title')); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <?php echo $form->textField($model,'email',array('class'=>'form-control','placeholder'=>'E-mail Address')); ?>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="form-actions">
                    <?php echo CHtml::submitButton('Update', array("class" => "btn main-btn")); ?>
                    <a href="<?php echo $this->createUrl('institutions/edit', array('id' => $model->id)) ?>" class="btn default-btn">Back</a>
                </div>

            <?php $this->endWidget(); ?>
        </div><!-- .container -->
    </div>

