<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */
/* @var $form CActiveForm */
?>
<script>
$(document).ready(function(){
	$("#deleteTutor").click(function(){
		
		 $.ajax({
	            url: '<?php echo $this->createUrl('institutions/deleteTutor') ?>',
	            type: 'POST',
	            data: {id : <?php echo $tutormodel->id?>}, 	    
	            success: function(data) {
		            window.location = "<?php echo $this->createUrl('institutions/insClasses',array('id'=>$model->id)); ?>"
	            },
			    error: function(data) {		
			        alert('err');
			    }
	         });  
	});
});
</script>

<div class="page-title text-left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $usermodel->isNewRecord ? "Create" : "Edit" ?> Class</h1>
            </div>
        </div>
    </div>
</div>
<div id="page-content">
    <div class="container">
        <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'class-form',
    'action'=>$this->createUrl('institutions/updateClass',array('id'=>$tutormodel->id)),
	'enableAjaxValidation'=>false
)); ?>
            <div id="user-group-form">
                <!-- Teacher -->
                <div id="user-manager">
                    <div class="form-container">
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="form-group">
                                    <label for="teacher_id" class="control-label">Teacher</label>
                                    <?php $teacherlist = array();
                                        $cri = new CDbCriteria();
                                        $cri->alias = "iu";
                                        $cri->condition = "iu.role = 'teacher' and iu.institution_id =".$model->id;//." and iu.user_id=".$tutormodel->teacher_id;
                                        $insusermodel = InstitutionUsers::model()->findAll($cri);
                                        foreach ($insusermodel as $insuser)
                                        {
                                            $imodel = Users::model()->findByPk($insuser['user_id']);
                                            $teacherlist[$insuser['user_id']] = $imodel->forenames." ".$imodel->surname;
                                        }
                                        $tgeachersmodel = TutorGroupsTeachers::model()->findAllByAttributes(array('tutor_group_id'=>$tutormodel->id,'institution_id'=>$tutormodel->institution_id));
                                        $teac = array();
                                        foreach ($tgeachersmodel as $tea)
                                        {
                                            $teac[] = $tea['teacher_id'];
                                        }
                                        $gteachers = new TutorGroupsTeachers();
                                        $gteachers->teacher_id = $teac;
                                        echo $form->dropDownList($gteachers,'teacher_id',$teacherlist,array('class'=>'form-control select2','id'=>'teacherslist','multiple'=>true)); ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="user-manager-form hidden">
                        <div class="form-container">
                            <!-- Your details -->
                            <h4>Teacher details</h4>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <input class="form-control" id="forenames" placeholder="First Name" autocomplete="off" name="admin[forenames]" type="text">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <input class="form-control" id="surname" placeholder="Surname" autocomplete="off" name="admin[surname]" type="text">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-container">
                            <h4>Account Details</h4>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        
                                        <input class="form-control" placeholder="Email" name="admin[email]" type="email">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Password" autocomplete="off" name="admin[password]" type="password" value="">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Confirm Password" autocomplete="off" name="admin[password_confirmation]" type="password" value="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Tutor Group -->
                <div class="form-container">
                    <h4>Tutor Group Details</h4>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="name" class="control-label">Tutor Group</label>
                                <?php echo $form->textField($tutormodel,'name',array('class'=>'form-control','placeholder'=>'Name')); ?>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="graduation_date" class="control-label">Graduation Date</label>
                                <div data-format="DD/MM/YYYY">
                                    <input type="text" autocomplete='off' name="graduation_date" class="form-control datepicker" date-format="DD/MM/YYYY" value="<?php echo ((empty($tutormodel->graduation_date) || $tutormodel->graduation_date == null || $tutormodel->graduation_date == "0000-00-00") ? "" : $tutormodel->graduation_date); ?>">
                                </div>
                            </div>
                        </div>
                        <!-- <div class="col-sm-4">
                            <div class="form-group">
                                <label for="location" class="control-label">Campus</label>
                                <?php //echo $form->textField($tutormodel,'location',array('class'=>'form-control','placeholder'=>'Campus')); ?>
                            </div>
                        </div> -->
                    </div>
                    <div class="form-container">
                        <h4>Allocated Students</h4>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-inline">
                                    <div class="form-group">
                                         <?php 
                                                $tgs_cnt = TutorGroupsStudents::model()->countByAttributes(array('institution_id'=>$model->id));

                                                echo CHtml::textField('allocated_students',$tutormodel->account_limit,array('id'=>'allocated_students','placeholder'=>'Max No. of Students','class'=>'form-control'));
                                           ?>
                                    </div>
                                    <p class="form-control-static hidden max-accounts-reached">You have reached the maximum students available.</p>                               
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="form-actions clearfix">
                    <div class="pull-left">
                        <?php echo CHtml::submitButton('Save',array('class'=>'btn btn-default')); ?>
		       		  <?php echo CHtml::button('Cancel',array('class'=>'btn btn-primary','onclick'=>'window.location="'.$this->createUrl('institutions/insClasses',array('id'=>$model->id)).'"')); ?>
                    </div>
                    <div class="pull-right">
                        <button class="btn btn-danger" id="deleteTutor">Delete</button>
                    </div>
                </div>

                <div class="modal fade" tabindex="-1" role="dialog" id="SampleDownload">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">�</span></button>
                                <h4 class="modal-title">Download Sample Upload File</h4>
                            </div>
                            <div class="modal-body">
                                <p>When using the sample document, please alter the data in the columns accordingly. Please however do not add additional columns or alter the column headings.</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <a href="<?php echo $this->createUrl('institutions/sampleDownload')?>" class="btn btn-primary">Download File</a>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->
            </div>
       <?php $this->endWidget(); ?>
    </div>
</div>