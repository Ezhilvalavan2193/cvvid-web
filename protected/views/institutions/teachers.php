<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */

$this->breadcrumbs=array(
	'Institutions'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#inst-teachers-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<script>
$(document).ready(function(){
	
	$('#export-teachers').on('click',function() {
		
	    window.location = '<?php echo $this->createUrl('institutions/exportTeachersCSV') ?>'
	});

});

</script>





    <h1>Teachers</h1>
    <div class="Admin__content__inner">
        <?php if(Yii::app()->user->hasFlash('success')):?>
<div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
   <?php echo Yii::app()->user->getFlash('success'); ?>
</div>
<?php endif; ?>
        <div class="AdminFilters clearfix">
            <div class="pull-left">
                    <div class="form-inline">
                        <?php
                        $this->renderPartial('_searchteachers', array(
                            'model' => $model, 'insmodel' => $insmodel, 'fullname' => $fullname
                        ));
                        ?>
                    </div>
            </div>
            <div class="pull-right">
                <?php       
                       echo CHtml::button('Create',array("class"=>"btn btn-primary","onclick"=>"window.location='".Yii::app()->createUrl('institutions/addTeacher')."'"));
                       echo CHtml::button('Export',array("class"=>"btn btn-primary","style"=>"margin-right:10px",'id'=>'export-teachers'));
                 ?>
            </div>
        </div>
        <?php
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'inst-teachers-grid',
             'itemsCssClass' => 'table table-striped',
            'summaryText' => '',
            //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
            'enableSorting' => true,
            'ajaxUpdate' => true,
    'pager' => array(
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'header'=>'',
        'firstPageLabel' => '<<',
        'lastPageLabel' => '>>',
        'nextPageLabel' => '>>',
        'prevPageLabel' => '<<'),
            'ajaxUpdate' => false,
            'dataProvider' => ($namesearch > 0 ? $model->searchByName($namearray) : $model->search()),
            //'filter'=>$model,
            'columns' => array(
                array(
                    'header' => 'Name',
                    'value' => 'CHtml::link("<i class=\"fa fa-pencil\"></i> ".$data->user->forenames." ".$data->user->surname,Yii::app()->createUrl("institutions/editInsTeacher", array("id"=>$data->id)),array("class"=>"profile-edit-button","id"=>"profile-edit"))',
                    'type' => 'raw'
                ),
                array(
                    'header' => 'Email',
                    'value' => '$data->user->email',
                    'type' => 'raw'
                ),
                array(
                    'header' => 'School',
                    'value' => '$data->institution->name',
                ),
                array(
                    'header' => 'Created At',
                    'value' => '$data->user->created_at',
                )
            )
        ));
        ?>
        
    </div>


