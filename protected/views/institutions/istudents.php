<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */

$this->breadcrumbs=array(
	'Institutions'=>array('index'),
	'Manage',
);

// $this->menu=array(
// 	array('label'=>'List Institutions', 'url'=>array('index')),
// 	array('label'=>'Create Institutions', 'url'=>array('create')),
// );

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#inst-admin-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<script>
$(document).ready(function(){
	
	$('#delete-candidate').click(function(e){

		
        if($("#inst-studs-grid").find("input:checked").length > 0)
        {

       	  		if (confirm('Are you sure you want to delete these items?')) {

        		 var ids = $.fn.yiiGridView.getChecked("inst-studs-grid", "selectedIds");
      			  $.ajax({
      		            url: '<?php echo $this->createUrl('institutions/deleteStudents') ?>',
      		            type: 'POST',
      		            data: {ids : ids}, 	    
      		            success: function(data) {
      		            	$.fn.yiiGridView.update('inst-studs-grid');
      		            },
      				    error: function(data) {
                            alert('err');
                        }
                    });
                }
               
        } 
    	else
        {
            alert('Please select at least one item');
        }
        });
});

</script>

    <div class="page-title text-left">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1><?php echo $model->name." Dashboard" ?></h1>
                </div>
            </div>
        </div>
    </div>
    <div id="page-content">
        <div class="container">

            <div class="row">
                <div class="col-sm-12">
                    <div class="box dashboard-header clearfix">
                       <?php
		        $tgscnt = TutorGroupsStudents::model()->countByAttributes(array('institution_id'=>$model->id));
		        $total = $model->num_pupils;
 			?>
                            
                    <div class="dashboard-header-section">
                        <div class="vertical-table">
                            <div class="vertical-cell">
                                <div id="progress" data-progress="" data-limit="<?php echo $total?>" data-current="<?php echo $tgscnt?>" style="position: relative;">
                                </div>
                            </div>
                            <div class="vertical-cell">
                               <?php echo $tgscnt ?> of <?php echo $total ?> accounts in use
                            </div>
                        </div>
                    </div>
                     <?php if($model->stripe_active > 0) { ?>
                    <div class="dashboard-header-section dashboard-header-section--buttons">
                        <a href="#" class="btn btn-primary">View Subscription</a>
                    </div>
					<?php } ?>
                </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3 col-md-2">
                    <ul class="box dashboard-links list-unstyled">
                        <li><a href="<?php echo $this->createUrl('institutions/insTeachers',array('id'=>$model->id))?>">Teachers</a></li>
                        <li><a href="<?php echo $this->createUrl('institutions/insClasses',array('id'=>$model->id))?>">Classes</a></li>
                        <li><a href="<?php echo $this->createUrl('institutions/insStudents',array('id'=>$model->id))?>">Students</a></li>
                    </ul>
                </div>
                <div class="col-sm-9 col-md-10">
                    <div class="form-container clearfix">
                        <div class="pull-right">
                            <button type="button" class="BulkAddBtn btn btn-default" onclick="window.location='<?php echo Yii::app()->createUrl('institutions/bulkStudent') ?>'"><i class="fa fa-plus"></i> Bulk Add Students</button>
                            <button type="button" class="AddBtn btn btn-default" onclick="window.location='<?php echo Yii::app()->createUrl('institutions/addStudent') ?>'"><i class="fa fa-plus"></i> Add Student</button>
                            <button type="button" class="DeleteBtn btn btn-danger" id="delete-candidate"><i class="fa fa-close"></i> Delete</button>
                        </div>
                    </div>

                    <div class="">
                        <?php
                        $this->widget('zii.widgets.grid.CGridView', array(
                            'id' => 'inst-studs-grid',
                            'summaryText' => '',
                            'itemsCssClass' => 'table',
                          //  'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                            'enableSorting' => true,
                            'ajaxUpdate' => true,
                        'pager' => array(
                            //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                            'header'=>'',
                            'firstPageLabel' => '<<',
                            'lastPageLabel' => '>>',
                            'nextPageLabel' => '>>',
                            'prevPageLabel' => '<<'),
                            'ajaxUpdate' => false,
                            'dataProvider' => $tutorgroupstud->searchByInstitution($model->id),
                            //'filter'=>$model,
                            'columns' => array(
                                array(
                                    // 'name' => 'check',
                                    'id' => 'selectedIds',
                                    'value' => '$data->id',
                                    'class' => 'CCheckBoxColumn',
                                    'selectableRows' => 2,
                                // 'checked'=>'Yii::app()->user->getState($data->id)',
                                // 'checkBoxHtmlOptions' => array('name' => 'idList[]'),
                                ),
                                
                                array(
                                    'header' => 'Class',
                                    'value' => 'CHtml::link(isset($data->student) ? $data->student->forenames." ".$data->student->surname : "",Yii::app()->createUrl("institutions/modifyStudent",array("id"=>$data->id)),array("class"=>"edit-button"))',
                                    'type' => 'raw'
                                ),
                               array(
                                    'header' => 'Profile',
                                    'value' => '$data->checkProfileExists() ? CHtml::link("Profile",Yii::app()->createUrl("users/profile",array("slug"=>$data->$data->getSlug())),array("class"=>"profile-button")) : ""',
                                    'type' => 'raw'
                                ),
                                array(
                                    'header' => 'Teacher',
                                    //'value' => '$data->getTeacher($data->tutorGroup->teacher_id)'
                                    'value' => '$data->getTeachers()'
                                ),
                                array(
                                    'header' => 'Class',
                                    'value' => '$data->tutorGroup->name'
                                ),
                                array(
                                    'header' => 'Graduation Date',
                                    'value' => '$data->graduation_date'
                                ),
                                array(
                                    'header' => 'Created',
                                    'value' => '$data->created_at'
                                )
                            ),
                        ));
                        ?>

                    </div>
                </div>
            </div>

        </div>
    </div>


