<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
    'Candidates'=>array('index'),
    'Manage',
);
?>


<?php
        $planmodel = Plans::model()->findByAttributes(array('owner_id'=>$model->id));
        $usermodel  = Users::model()->findByPk($model->admin_id);        
?>
    <h1>Teachers</h1>
    <div class="Admin__content__inner">
        <!-- Nav tabs -->
        <ul class="AdminTabs nav nav-tabs" role="tablist">
                <li role="presentation"><a href="<?php echo Yii::app()->createUrl("institutions/tabeditinstitute", array("id" => $model->id)); ?>" aria-controls="details">Details</a></li>
                <li role="presentation"><a href="<?php echo Yii::app()->createUrl("institutions/tabeditadmin", array("id" => $model->id)); ?>">Admin</a></li>
                <li role="presentation"  class="active"><a href="<?php echo Yii::app()->createUrl("institutions/tabeditteachers", array("id" => $model->id)); ?>">Teachers</a></li>
                <li role="presentation"><a href="<?php echo Yii::app()->createUrl("institutions/tabedittutorgroups", array("id" => $model->id)); ?>">Tutor Groups</a></li>
                <li role="presentation"><a href="<?php echo Yii::app()->createUrl("institutions/tabeditstudents", array("id" => $model->id)); ?>">Students</a></li>
            </ul>
        <?php echo CHtml::button('Create',array('class'=>'btn btn-primary','onclick'=>"window.location='".Yii::app()->createUrl('institutions/createTeacher',array('id'=>$model->id))."'")); ?>
<?php  
    
    $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'teacher-grid',
         'itemsCssClass' => 'table table-striped',
	'summaryText'=>'',
	//'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',
         'dataProvider'=>InstitutionUsers::model()->searchByInstitution($model->id),
        	'columns'=>array(
        	    
            			array(
            					'header'=>'Name',
            					'value'=>'CHtml::link($data->user->forenames." ".$data->user->surname,Yii::app()->createUrl("institutions/editTeacher", array("id"=>$data->id)),array("class"=>"teacher-edit-button","id"=>"teacher-edit"))',
            			        'type'=>'raw'
            			),
            			array(
            					'header'=>'Email',
            			        'value'=>'$data->user->email',
            					
            			),
                	    array(
                	        'header'=>'Created At',
                	        'value'=>'$data->user->created_at',
                	        
                	    ),
                	    
        	 ),
  ));
?>
 
    </div>


