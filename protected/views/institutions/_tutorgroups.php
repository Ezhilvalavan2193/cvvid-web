<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
    'Institutions'=>array('index'),
    'Manage',
);
?>
<?php  
    echo CHtml::button('Create',array('class'=>'btn btn-primary','onclick'=>"window.location='".Yii::app()->createUrl('institutions/createTutor',array('id'=>$model->id))."'"));
    
    $instusers = InstitutionUsers::model()->findByAttributes(array('institution_id'=>$model->id,'role'=>'teacher'));
  
    //$tutorgroup = TutorGroups::model()->findByAttributes(array('institution_id'=>$model->id,'teacher_id'=>$instusers->user_id));
    
    $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'teacher-grid',
	'summaryText'=>'',
	'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'dataProvider'=>TutorGroups::model()->searchTutorsByInstitution($model->id),
        	'columns'=>array(
        	    
            			array(
            					'header'=>'Name',
            					'value'=>'CHtml::link($data->name,Yii::app()->createUrl("institutions/editTutor", array("id"=>$data->id)),array())',
            			        'type'=>'raw'
            			),
            			array(
            					'header'=>'Teacher',
            			        'value'=>'($data->teacher_id > 0 ? $data->getTeacher($data->teacher_id) : "")'            					
            			),
                	    array(
                	        'header'=>'Students',
                	        'value'=>'($data->id > 0 ? $data->getCount($data->id) : "")',                	        
                	    ),
                	    array(
                	        'header'=>'Created At',
                	        'value'=>'$data->created_at',                	        
                	    )
                	    
        	 ),
  ));
?>
 