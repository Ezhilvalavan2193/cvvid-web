<?php 
use Stripe\Stripe; 
use Stripe\Invoice; 
use Stripe\Customer;
require Yii::app()->basePath . '/extensions/stripe/init.php';
Stripe::setApiKey(Yii::app()->params['SECRET_Key']);
?>

<div class="page-title text-left margin-bottom">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>  
                    My Subscription
                </h1>
            </div>
        </div>
    </div>
</div>

<div id="page-content">
    <div class="container">
        <?php if (Yii::app()->user->hasFlash('success')): ?>
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <?php echo Yii::app()->user->getFlash('success'); ?>
            </div>
        <?php endif; ?>
        <?php if (Yii::app()->user->hasFlash('error')): ?>
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <ul>
                <li> <?php echo Yii::app()->user->getFlash('error'); ?></li>
            </ul>
        </div>
        <?php endif; ?>
        <div class="row">
            <?php 
            $subscriber = Institutions::model()->findByAttributes(array('admin_id'=>Yii::app()->user->getState('userid')));
            if(CustomerExists($subscriber->stripe_id))
            {
            $invoiceitemlist = Invoice::all(array("customer" => $subscriber->stripe_id));
           
            ?>
            <?php if(! $subscriber->trial_ends_at && count($invoiceitemlist) > 0) { ?>
                <div class="col-sm-8">

                    <div class="panel panel-default">
                        <div class="panel-heading">Invoices</div>
                        <div class="panel-body">
                            <table class="table table-striped">
                                <tbody>
                                <?php
                                
                                foreach ($invoiceitemlist->autoPagingIterator() as $invoice) {
                                    $idate = gmdate("Y-m-d H:i:s", $invoice['date']);
                                    ?>
                                    <tr>
                                        <td><?php echo $idate; ?></td>
                                        <td><?php echo Yii::app()->numberFormatter->formatCurrency($invoice['amount_due'] / 100, 'GBP'); ?></td>
                                        <td><a href="#">Download</a></td>
                                    </tr>
                                <?php } ?> 
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>

                <?php } 
                
            }?>

            <?php  
            $umodel = Users::model()->findByPk($subscriber->admin_id);
            $student_cnt = TutorGroupsStudents::model()->countByAttributes(array('institution_id' => $subscriber->id));
            ?>
            <div class="col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading">Update Available Accounts</div>
                <div class="panel-body">
                  <form method="POST" action="<?php echo Yii::app()->createUrl('institutions/updatequantity'); ?>" accept-charset="UTF-8">
                    <div class="quantity-form">
                        <div class="alert alert-danger payment-errors"></div>

                        <p>If you would like to reduce/increase your number of accounts you can change this below.</p>

                        <div class="form-inline">
                            <div class="form-group">
                                <label class="control-label">Current Assigned Accounts</label>
                                <p class="form-control-static">
                                   <?php echo $student_cnt; ?> / <?php echo $subscriber->num_pupils; ?>
                                </p>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="input-view">
                                <input type="number" size="20" class="form-control" name="quantity" id="quantity"
                                       placeholder="New Quantity" autocomplete="off"/>
                                <span class="card-icon"></span>
                                <div class="view-icon">
                                    <i class="fa fa-users"></i>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="submit-button btn btn-block main-btn update-subs">Update Subscription</button>
                    </div>
                  </form>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Update Card Information</div>
                <div class="panel-body">
                    <form method="POST" action="<?php echo Yii::app()->createUrl('institutions/cardupdate'); ?>" accept-charset="UTF-8" class="payment-form">
                    <div class="card-form">
                        <div class="alert alert-danger payment-errors"></div>

                        <div class="form-group">
                            <div class="input-view">
                                <input type="tel" size="20" class="form-control cc-name"
                                       placeholder="Cardholder Name" autocomplete="off"/>
                                <span class="card-icon"></span>
                                <div class="view-icon">
                                    <i class="fa fa-pencil-square-o"></i>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-view">
                                <input type="tel" size="20" class="form-control cc-number" data-stripe="number"
                                       placeholder="Card Number" autocomplete="off"/>
                                <span class="card-icon"></span>
                                <div class="view-icon">
                                    <i class="fa fa-credit-card"></i>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group form-expiration">
                                    <span>Expiry</span>
                                    <input id="cc-exp" type="tel" size="10" class="form-control cc-exp" autocomplete="cc-exp"
                                           placeholder="•• / ••" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <div class="input-view cvc-input">
                                    
                                        <input type="tel" size="4" class="form-control cc-cvc" data-stripe="cvc"
                                              autocomplete="off"/>
<!--                                         <div class="view-icon"> -->
<!--                                             <i class="fa fa-lock"></i> -->
<!--                                         </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="submit-button btn btn-block main-btn">Submit Payment</button>
                    </div>
                     </form>
                </div>
            </div>
        </div>
            
        </div>
    </div><!-- .container -->
</div>
<?php 
function CustomerExists($id)
{
    try {
        Customer::retrieve($id);
        return true;
    } catch (Exception $e) {
    }
    return false;
}
?>
<script type="text/javascript">
    CVPayment.init('<?php echo Yii::app()->params['PUBLIC_Key']; ?>');
</script>