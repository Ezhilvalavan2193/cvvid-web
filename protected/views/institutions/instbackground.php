<div class="form-section-inner">
    <div class="section-header">
        <div class="row">
            <div class="col-sm-8">
                <h4>School Background</h4>
            </div>
            <div class="col-sm-4">
                <div class="pull-right">
                    <a class="edit-item edit-background" href="#"><i class="fa fa-pencil"></i>Edit</a>
                </div>
            </div>
        </div>
    </div>
    <div class="section-content">
        <div class="section-description" style="display: none;">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut et augue mollis, gravida neque eget, scelerisque urna. Donec vitae faucibus turpis. Morbi vulputate lorem in odio viverra, eu ultricies sem blandit. Integer in libero sollicitudin, ornare neque sed, faucibus lorem. Etiam rutrum diam in nisl elementum, eu sagittis ex volutpat. Fusce non sem sed lectus auctor consectetur. Praesent non urna fringilla, hendrerit nisl vel, vulputate ligula. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas vulputate vel risus et aliquam. Integer porta turpis at lectus pulvinar, in fringilla tellus hendrerit.
        </div>
        <div class="employer-body"><?php echo $model->body; ?></div>
    </div>
    <div class="section-form" style="display: none">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'ins-bground-form',
            'enableAjaxValidation' => false,
            'htmlOptions' => array('enctype' => 'multipart/form-data')
        ));
        ?>
        <textarea class="form-control" name="content" id="content" cols="50" rows="10"><?php echo $model->body; ?></textarea>
        <div class="form-actions">
            <button class="main-btn" type="submit"  id="ins_back_save">Save</button>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>

