<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */
/* @var $form CActiveForm */
?>
 
<h1>Add Institution Subscription Plan</h1>
<div class="Admin__content__inner">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Institution Details</div>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label for="name" class="control-label">Name</label>
                                <p class="form-control-static"><?php echo $model->name; ?></p>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label">Number of Pupils</label>
                                <p class="form-control-static"><?php echo $model->num_pupils; ?></p>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label">Email</label>
                                <p class="form-control-static"><?php echo $model->email; ?></p>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label class="control-label">Telephone</label>
                                <p class="form-control-static"><?php echo $usermodel->tel; ?></p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label class="control-label">Address</label>
                                <p class="form-control-static"><?php echo $model->address_1; ?></p>
                            </div>
                        </div>
                    </div>

                    <!-- <form action="<?php //echo Yii::app()->createUrl('institutions/saveStatus',array('id'=>$model->id)); ?>" method="POST">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label">Status</label>
                                    <div class="input-group">
                                        <select class="form-control" name="status">
                                            <option value="pending">Pending</option>
                                            <option value="active">Active</option>
                                        </select>
                                        <span class="input-group-btn">
                                            <button class="btn btn-primary">Update</button>
                                        </span>
                                    </div>
                                    <span class="help-block">Only use this if the account has not transitioned to active after payment.</span>
                                </div>
                            </div>
                        </div>
                    </form> -->

                </div>
                
                
            </div><!-- .panel -->
        </div>
    </div>
    <div class="row">
         <div class="col-sm-6 col-md-6">

            <div class="panel panel-default">
                <div class="panel-heading">Previous Plans</div>
                <div class="panel-body">
                    
                    <div id="plans-grid" class="grid-view">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th id="plans-grid_c0">Name</th>
                                    <th id="plans-grid_c1">Quantity</th>
                                    <th id="plans-grid_c2">Time</th>
                                    <th id="plans-grid_c3">Price(£)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $planarray = Plans::model()->findAllByAttributes(array("owner_id"=>$model->id));
                                if(count($planarray)>0){
                                foreach ($planarray as $plans) { ?>
                                <tr>
                                    <td><?php echo $plans['name']; ?></td>
                                    <td><?php echo $plans['quantity']; ?></td>
                                    <td><?php echo $plans['interval_count']; ?> <?php echo $plans['interval']; ?></td>
                                    <td>£<?php echo $plans['price']; ?></td>
                                </tr>  
                                <?php
                                } 
                                }else{?>
                                <tr>
                                    <td colspan="4" class="empty">
                                        <span class="empty">No results found.</span>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div><!-- .panel -->


        </div><!-- .col-sm-6 -->
       
        <div class="col-sm-6 col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">Subscription Plan</div>
                <div class="panel-body">
                    <?php 
                    $planmodel->price = "";
                       $form=$this->beginWidget('CActiveForm', array(
                        	'id'=>'subscription-form',
                        	'enableAjaxValidation'=>false,
                            'action'=>Yii::app()->createUrl('institutions/saveSubscription',array('id'=>$model->id))
                            )); ?>
                    <?php echo  $form->errorSummary(array($planmodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Payment Interval</label>
                                    <select class="form-control" name="interval" id="interval">
                                        <option value="month">Monthly</option>
                                        <option value="year">Yearly</option>
                                    </select>
                                    <small class="help-block">How often should the plan be billed</small>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Base Price</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">&pound;</span>
                                        <?php echo $form->textField($planmodel,'price',array('class'=>'form-control')); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Offer Expiry Date</label>
                                    <input type="text" autocomplete='off' name="active_to" class="datepicker form-control" value="<?php echo ((empty($planmodel->active_to) || $planmodel->active_to == null || $planmodel->active_to == "0000-00-00") ? "" : $planmodel->active_to); ?>">
                 
                                    <small class="help-block">When will this plan stop being available</small>
                                </div>
                            </div>
                        </div>                 
                       <?php echo CHtml::submitButton('Send Subscription Mail',array('class'=>'btn btn-primary','disabled'=> (count($planarray) == 0 ? FALSE : TRUE))); ?>
                      <?php $this->endWidget(); ?>
                </div>
            </div><!-- .panel -->
        </div><!-- .col-sm-6 -->
       
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <form method="POST" action="#" accept-charset="UTF-8">
                        <input name="_method" type="hidden" value="DELETE">
                        <?php echo CHtml::button('Delete Account', array("class" => "btn btn-danger", "onclick" => "window.location='" . Yii::app()->createUrl('institutions/deleteInstitution',array('id'=>$model->id)) . "'"));?>
                        
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>



