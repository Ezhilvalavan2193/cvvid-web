<?php 
/* @var $this InstitutionsController */
/* @var $model Institutions */

$this->breadcrumbs=array(
	'Institutions'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
    $('.search-button').click(function(){
    	$('.search-form').toggle();
    	return false;
    });
");
?> 
<script>

$(document).ready(function(){

	$('.photo-section').on('click','#photo-btn',function(e){
		 e.preventDefault();
		 $("#profile-photo").trigger('click');
	});

});

</script>
<div class="row">
	<div class="col-sm-6">
          <?php       
                $form = $this->beginWidget('CActiveForm', array(
                            	'id'=>'current-admin-form',
                                'action'=>$this->createUrl('institutions/updateCurrentAdmin',array('id'=>$model->id)),
                            	'enableAjaxValidation'=>false,
                                'htmlOptions' => array('enctype' => 'multipart/form-data')
                        ));              
           ?>
    
            <div class="form">
          			              
                         <h3 class="summary" style="text-align:left;margin-bottom: 20px;">Update Current Admin</h3>
                        	
                         <div class="formBlk col-xs-4">
                        	<div class="formRow">
                        		<?php echo $form->labelEx($usermodel,'forenames'); ?>
                        		<?php echo $form->textField($usermodel,'forenames',array('class'=>'textBox')); ?>
                        	</div>
                         </div>
                     
                          <div class="formBlk col-xs-4">
                        	<div class="formRow">
                        		<?php echo $form->labelEx($usermodel,'surname'); ?>
                        		<?php echo $form->textField($usermodel,'surname',array('class'=>'textBox')); ?>
                        	</div>
                         </div> 
                          <div class="formBlk col-xs-4">
                        	<div class="formRow">
                        		<?php echo $form->labelEx($usermodel,'email'); ?>
                        		<?php echo $form->textField($usermodel,'email',array('class'=>'textBox')); ?>
                        	</div>
                         </div>    
                         <div class="formBlk col-xs-4">
                        	<div class="formRow">
                        		<?php echo $form->labelEx($usermodel,'password'); ?>
                        		<?php echo $form->passwordField($usermodel,'password',array('class'=>'textBox')); ?>
                        	</div>
                         </div> 
                          <div class="formBlk col-xs-4">
                        	<div class="formRow">
                        		<?php echo CHtml::label('Current Password','',array()); ?>
                        		<?php echo CHtml::passwordField('current_password','',array('class'=>'textBox')); ?>
                        	</div>
                         </div> 
                    
              </div>
              
  			  <div class="buttons">
        		<?php echo CHtml::submitButton('Save',array("class"=>"btn btn-primary")); ?>    
        	  </div>
            	 
          <?php $this->endWidget(); ?>
</div>

<div class="col-sm-6">

              <?php  $form = $this->beginWidget('CActiveForm', array(
                    	'id'=>'new-admin-form',
                        'action'=>$this->createUrl('institutions/addNewAdmin',array('id'=>$model->id)),
                    	'enableAjaxValidation'=>false,
                        'htmlOptions' => array('enctype' => 'multipart/form-data')
                     ));  
              
              $newusermodel = Users::model();
              ?>
          
             	  <div class="form">
                          
                         <h3 class="summary" style="text-align:left;margin-bottom: 20px;"> Change Admin</h3>
                        <p><i>Note: This is will remove current admin but maintain a reference</i> </p>	
                         <div class="formBlk col-xs-4">
                        	<div class="formRow">
                        		<?php echo $form->labelEx($newusermodel,'forenames'); ?>
                        		<?php echo $form->textField($newusermodel,'forenames',array('class'=>'textBox')); ?>
                        	</div>
                         </div>
                     
                          <div class="formBlk col-xs-4">
                        	<div class="formRow">
                        		<?php echo $form->labelEx($newusermodel,'surname'); ?>
                        		<?php echo $form->textField($newusermodel,'surname',array('class'=>'textBox')); ?>
                        	</div>
                         </div> 
                          <div class="formBlk col-xs-4">
                        	<div class="formRow">
                        		<?php echo $form->labelEx($newusermodel,'email'); ?>
                        		<?php echo $form->textField($newusermodel,'email',array('class'=>'textBox')); ?>
                        	</div>
                         </div>    
                         <div class="formBlk col-xs-4">
                        	<div class="formRow">
                        		<?php echo $form->labelEx($newusermodel,'password'); ?>
                        		<?php echo $form->passwordField($newusermodel,'password',array('class'=>'textBox')); ?>
                        	</div>
                         </div> 
                          <div class="formBlk col-xs-4">
                        	<div class="formRow">
                        		<?php echo CHtml::label('Confirm Password','',array()); ?>
                        		<?php echo CHtml::passwordField('current_password','',array('class'=>'textBox')); ?>
                        	</div>
                         </div> 
                     </div>
                               
            	  <div class="buttons">
            			<?php echo CHtml::submitButton('Save',array("class"=>"btn btn-primary")); ?>   
            	  </div>
            	  
                 <?php $this->endWidget(); ?>
                
            </div>
       
</div>
