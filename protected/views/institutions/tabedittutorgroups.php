<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
    'Institutions'=>array('index'),
    'Manage',
);
?>

    


<?php
$planmodel = Plans::model()->findByAttributes(array('owner_id' => $model->id));
$usermodel = Users::model()->findByPk($model->admin_id);
?>
<h1>TIGHS: Tutor Groups</h1>
<div class="Admin__content__inner">
    <!-- Nav tabs -->
    <ul class="AdminTabs nav nav-tabs" role="tablist">
        <li role="presentation"><a href="<?php echo Yii::app()->createUrl("institutions/tabeditinstitute", array("id" => $model->id)); ?>" aria-controls="details">Details</a></li>
        <li role="presentation"><a href="<?php echo Yii::app()->createUrl("institutions/tabeditadmin", array("id" => $model->id)); ?>">Admin</a></li>
        <li role="presentation"><a href="<?php echo Yii::app()->createUrl("institutions/tabeditteachers", array("id" => $model->id)); ?>">Teachers</a></li>
        <li role="presentation" class="active"><a href="<?php echo Yii::app()->createUrl("institutions/tabedittutorgroups", array("id" => $model->id)); ?>">Tutor Groups</a></li>
       <li role="presentation"><a href="<?php echo Yii::app()->createUrl("institutions/tabeditstudents", array("id" => $model->id)); ?>">Students</a></li>
    </ul>
   <?php  
       $cri = new CDbCriteria();
       $cri->select = "sum(account_limit) as account_limit";
       $cri->condition = "institution_id =".$model->id;
       $tgmodel = TutorGroups::model()->find($cri);
       if($tgmodel->account_limit < $model->num_pupils)  
         echo CHtml::button('Create',array('class'=>'btn btn-primary','onclick'=>"window.location='".Yii::app()->createUrl('institutions/createTutor',array('id'=>$model->id))."'"));
    
    $instusers = InstitutionUsers::model()->findByAttributes(array('institution_id'=>$model->id,'role'=>'teacher'));
  
    //$tutorgroup = TutorGroups::model()->findByAttributes(array('institution_id'=>$model->id,'teacher_id'=>$instusers->user_id));
    
    $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'teacher-grid',
          'itemsCssClass' => 'table table-striped',
	'summaryText'=>'',
	//'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'dataProvider'=>TutorGroups::model()->searchTutorsByInstitution($model->id),
        	'columns'=>array(
        	    
            			array(
            					'header'=>'Name',
            					'value'=>'CHtml::link($data->name,Yii::app()->createUrl("institutions/editTutor", array("id"=>$data->id)),array())',
            			        'type'=>'raw'
            			),
            			array(
            					'header'=>'Teacher',
            			        'value'=>'($data->teacher_id > 0 ? $data->getTeacher($data->teacher_id) : "")'            					
            			),
                	    array(
                	        'header'=>'Students',
                	        'value'=>'($data->id > 0 ? $data->getCount($data->id) : "")',                	        
                	    ),
                	    array(
                	        'header'=>'Created At',
                	        'value'=>'$data->created_at',                	        
                	    )
                	    
        	 ),
  ));
?>

</div>

    
 