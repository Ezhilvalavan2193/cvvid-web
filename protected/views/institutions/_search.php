<?php

/* @var $this InstitutionsController */
/* @var $model Institutions */
/* @var $form CActiveForm */
?>


<?php

$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
        ));
?>

<?php echo $form->textField($model, 'name', array('class' => 'form-control', 'placeholder' => 'Enter Institution name')); ?>

<?php echo CHtml::submitButton('Search', array("class" => 'btn btn-primary')); ?>

<?php $this->endWidget(); ?>
