<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */

$this->breadcrumbs=array(
    'Institutions'=>array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
    
");
?>
<div>
<?php
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$model->id,'type'=>$model->type));        
        $addressmodel = Addresses::model()->findByAttributes(array('model_id'=>$model->id));
        if($addressmodel == null)
            $addressmodel = Addresses::model(); 
?>
        
    <div class="live-tab">
            <ul>
                <li data-id="details-section" class="active">Details</li>
                <li data-id="profile-section">Profile</li>
                <li data-id="documents-section">Documents</li>
                <li data-id="application-section">Applications</li>
            </ul>
    </div>
    
   		<section id="details-section" class="live-score-landing">
			
				<?php echo $this->renderPartial('_profiledetails',array('model'=>$model,'profilemodel'=>$profilemodel,'addressmodel'=>$addressmodel)); ?>
			
		</section>
		<section id="profile-section" class="live-score-landing">
			
				<?php echo $this->renderPartial('_profileedit',array('model'=>$model,'profilemodel'=>$profilemodel,'addressmodel'=>$addressmodel)); ?>
				
		
		</section>
		
		<section id="documents-section" class="live-score-landing">
			
				<?php echo $this->renderPartial('_profiledocuments',array('model'=>$model,'profilemodel'=>$profilemodel,'addressmodel'=>$addressmodel)); ?>
				
			
		</section>
		
		<section id="application-section" class="live-score-landing">
			
				<?php echo $this->renderPartial('_profilejobs',array('model'=>$model,'profilemodel'=>$profilemodel,'addressmodel'=>$addressmodel)); ?>
		
		</section>
 							
</div>
