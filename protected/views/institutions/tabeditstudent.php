<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */

$this->breadcrumbs=array(
    'Institutions'=>array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
    
");
?>
<script>
$(document).ready(function(){
	
	$('#delete_btn').click(function(){
		
		$.ajax({
            url: '<?php echo $this->createUrl('institutions/deleteStudent',array('id'=>$tgsmodel->id))?>',
            type: 'POST',
            success: function(data) {
                window.location = "<?php echo Yii::app()->createUrl('institutions/students'); ?>";
            },
		    error: function(data) {		
		       // alert(data);
		    }
        });
	});
	
	$('#inst_list').change(function(){
		
		var id = $(this).val();
		$('.tutor_det').hide();
		if(id > 0)
		{
		 $.ajax({
	            url: '<?php echo $this->createUrl('institutions/getInstituteDetails') ?>',
	            type: 'POST',
		        data: {id : id},
	            success: function(data) {
		            
	            	var result = jQuery.parseJSON(data);
	            	$('#iname').html(result.name);
	            	$('#iadmin').html(result.admin);
	            	$('#icount').html(result.student_count);

	            	$.ajax({
	    	            url: '<?php echo $this->createUrl('institutions/getTutors')?>',
	    	            type: 'POST',
	    		        data: {id : id},
	    	            success: function(data) {
	    	            	
	    	            	$('#tutor_list').html(data);	    	            		    		           
	    	            },
	    			    error: function(data) {		
	    			        alert(data);
	    			    }
	    	         });
	            },
//			    error: function(data) {		
//			        alert(data);
//			    }
	        });
		}
		else
		{
			$('#iname').html('');
        	$('#iadmin').html('');
        	$('#icount').html('');
        	$('#tutor_list').html('');
        	$('#tgname').html('');
        	$('#tname').html('');
        	$('#scount').html('');
		}
	});

	$('#tutor_list').change(function(){
		if($(this).val() > 0)
		{
			
		 $.ajax({
	            url: '<?php echo $this->createUrl('institutions/getTutorDetails')?>',
	            type: 'POST',
		        data: {id : $(this).val()},
	            success: function(data) {
		            
	            	var result = jQuery.parseJSON(data);
	            	$('#tgname').html(result.name);
	            	$('#tname').html(result.teacher);
	            	$('#scount').html(result.student_count);
	            	$('.tutor_det').show();
	            },
//			    error: function(data) {		
//			        alert(data);
//			    }
	        });
		}
		else
			$('.tutor_det').hide();
	});
	
});

</script>
<?php
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id'=>$model->id,'type'=>$model->type));        
        $addressmodel = Addresses::model()->findByAttributes(array('model_id'=>$model->id));
        if($addressmodel == null)
            $addressmodel = Addresses::model(); 
?>



        <h1>Edit Student User</h1>
        <div class="Admin__content__inner">
            <div class="CandidateForm">
                <ul class="AdminTabs nav nav-tabs">
                    <li role="presentation" class="active">
                        <a href="<?php echo Yii::app()->createUrl("institutions/tabeditstudent",array("id"=>$tgsmodel->id)); ?>">Details</a>
                    </li>
                    <li role="presentation">
                        <a href="<?php echo Yii::app()->createUrl("institutions/tabeditprofile",array("id"=>$tgsmodel->id)); ?>">Profile</a>
                    </li>
                    <li role="presentation">
                       <a href="<?php echo Yii::app()->createUrl("institutions/tabeditdocuments", array("id" => $tgsmodel->id)); ?>">Documents</a>
                    </li>
                    <li role="presentation">
                       <a href="<?php echo Yii::app()->createUrl("institutions/tabeditapplications", array("id" => $tgsmodel->id)); ?>">Applications</a>
                    </li>
                </ul>
               <?php 
     //   $meidatmodel = Media::model()->findByPk($profilemodel->id);
    
        $form=$this->beginWidget('CActiveForm', array(
                    	'id'=>'edit-stude-form',
                        'action'=>$this->createUrl('institutions/updateStudentDetails',array('id'=>$model->id)),
                    	'enableAjaxValidation'=>false,
                        'htmlOptions' => array('enctype' => 'multipart/form-data')
                    )); 
        
        
    ?>
                    <div class="row">
                        <div class="col-sm-3 col-md-2">
                            <div class="form-group">
                                <label for="photo_id" class="control-label">Profile Photo</label>
                                <div class="photo-section">
                                    <img src="<?php echo Yii::app()->baseUrl?>/images/media/<?php echo $profilemodel->photo_id?>/conversions/thumb.jpg" alt="" id="profile-pic" class="img-responsive">
                                     <input type="hidden" name="photo_id" id="photo_id" value="<?php echo $profilemodel->photo_id?>">
                                    <button type="button" class="add-media btn btn-primary btn-xs" id="photo-btn">Add Logo</button>            
                                    <button type="button" class="remove-media btn btn-primary btn-xs" id="remove-photo-btn">Remove Logo</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-9 col-md-10">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Account Details</h3>
                                </div>
                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="forenames" class="control-label">Forenames</label>
                                                <?php echo $form->textField($model,'forenames',array('class'=>'form-control')); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="surname" class="control-label">Surname</label>
                                                <?php echo $form->textField($model,'surname',array('class'=>'form-control')); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="email" class="control-label">Email</label>
                                                <?php echo $form->textField($model,'email',array('class'=>'form-control')); ?>
                                            </div>
                                        </div>
                                    </div><!-- .row -->

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="password" class="control-label">Password</label>
                                                <?php echo $model->password = "";?>
                		               <?php echo $form->passwordField($model,'password',array('class'=>'form-control')); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="password_confirmation" class="control-label">Confirm Password</label>
                                                <?php echo CHtml::passwordField('confirmpassword','',array('class'=>'form-control')); ?>
                                            </div>
                                        </div>
                                    </div><!-- .row -->

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="slug" class="control-label">Link</label>
                                                <div class="input-group slug-field">
                                                    <span class="input-group-addon">http://www.cvvid.com/</span>
                                                    <?php echo $form->textField($profilemodel,'slug',array('class'=>'form-control','id'=>'slug')); ?>
                                                    <span class="input-group-addon help-block"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div><!-- .panel-body -->
                            </div><!-- .panel -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Institution Details</h3>
                                </div>
                                <div class="panel-body">

                                    <div id="institution-tutor-form">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group">
                                                    <label class="control-label">Institution</label>
                                                     <?php             			
                                                        $insmodel = Institutions::model()->findAllByAttributes(array('deleted_at'=>null));
                                                        $instlist = CHtml::listData($insmodel, 'id', 'name');
     
                                                        echo $form->dropDownList($imodel, 'id', $instlist,array('class'=>'form-control select2','id'=>'inst_list','empty'=>"- select Institution -"));
                                                    ?>
                                                   
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Tutor Group</label>
                                                    <?php             				    
                                                        $tutorgroups = TutorGroups::model()->findAllByAttributes(array('institution_id'=>$tgsmodel->institution_id));
                                                        $tutorgrouplist = CHtml::listData($tutorgroups, 'id', 'name');
                                                        
                                                        echo CHtml::dropDownList('tutor_group_id',$tgsmodel->tutor_group_id,$tutorgrouplist,array('empty'=>'','class'=>'form-control select2','id'=>'tutor_list'));             				    
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-sm-8">
                                            <div class="institution-details">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Institution Name</label>
                                                            <p class="form-control-static" id="iname"><?php echo $imodel->name; ?></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                        <?php 
                                                           
                                                            $umodel = Users::model()->findByPk($imodel->admin_id);
                                                            
                                                            $student_cnt = TutorGroupsStudents::model()->countByAttributes(array('institution_id'=>$imodel->id));
                                                      		?>

                                                            <label class="control-label">Admin</label>
                                                            <p class="form-control-static" id="iadmin"><?php echo $umodel->forenames." ".$umodel->surname; ?></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Student Count</label>
                                                            <p class="form-control-static" id="icount"><?php echo $student_cnt.' / '.$imodel->num_pupils; ?></p>
                                                        </div>
                                                    </div>
                                                </div></div>
               		
               							    <div class="tutor-group-details">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                    <?php 
                                                        $tgmodel = TutorGroups::model()->findByPk($tgsmodel->tutor_group_id);
                                                        $inmodel = Institutions::model()->findByPk($tgmodel->institution_id);
                                                        $umodel = Users::model()->findByPk($tgmodel->teacher_id);
                                                        
                                                        $student_cnt = TutorGroupsStudents::model()->countByAttributes(array('institution_id'=>$inmodel->id));
                                                    ?>
                                                        <div class="form-group">
                                                            <label class="control-label">Tutor Group Name</label>
                                                            <p class="form-control-static" id="tgname"><?php echo $tgmodel->name?></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Teacher</label>
                                                            <p class="form-control-static" id="tname"><?php echo (isset($umodel) ? $umodel->forenames." ".$umodel->surname : "") ?></p>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <label class="control-label">Student Count</label>
                                                            <p class="form-control-static" id="scount"><?php echo $student_cnt.' / '.$tgmodel->account_limit?></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>

                                </div>
                            </div>


                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Details</h3>
                                </div>
                                <div class="panel-body">

                                    <div class="row">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="dob" class="control-label">Date of Birth</label>
                                                <div class="input-group">
                                                    <input type="text" name="dob" class="dob form-control" value="<?php echo ((empty($model->dob) || $model->dob == null || $model->dob == "0000-00-00") ? "" : date("d/m/Y", strtotime($model->dob))); ?>">
                                                   
                                                </div>
                                            </div>
                                        </div><!-- .col-sm-6 -->
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="tel" class="control-label">Telephone</label>
                                                <?php echo $form->textField($model,'tel',array('class'=>'form-control')); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label for="mobile" class="control-label">Mobile</label>
                                                <?php echo $form->textField($model,'mobile',array('class'=>'form-control')); ?>
                                            </div>
                                        </div><!-- .col-sm-6 -->
                                    </div><!-- .row -->

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="current_job" class="control-label">Current Job</label>
                                                <?php echo $form->textField($model,'current_job',array('class'=>'form-control')); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- .panel-body -->
                            </div><!-- .panel -->

                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Address</h3>
                                </div>
                                <div class="panel-body">
                                      <div id="location-fields">
                                            <?php $this->renderPartial('//addresses/address-form', array('form'=>$form,'addressmodel'=>$addressmodel)); ?>
                                        </div>

                                </div>
                            </div>              
                            <div class="form-actions clearfix">
                                <?php echo CHtml::submitButton('Save',array("class"=>"btn btn-primary pull-right")); ?>
                            </div>
                        </div>
                    </div>

                 <?php $this->endWidget(); ?>

                <div class="row">
                    <div class="col-sm-offset-2 col-sm-10">
                        <div class="panel panel-primary">
                            <div class="panel-body">
                                
                                <?php 
                                echo CHtml::button('Delete Account',array('class'=>'btn btn-danger','id'=>'delete_btn'));
                                ?>
                                
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        
    <script type="text/javascript">
        <?php if(isset($profilemodel->id)){ ?>
        $(function(){
            new CandidateSlugGenerator(<?php echo $profilemodel->id; ?>);
        });
        <?php } ?>
    </script>