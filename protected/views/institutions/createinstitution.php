<?php 
/* @var $this InstitutionsController */
/* @var $model Institutions */

$this->breadcrumbs=array(
	'Institutions'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
");
?> 


<h1>Create School/Institution</h1>
<div class="Admin__content__inner">
    <div class="InstitutionForm">
         <?php 
            $form = $this->beginWidget('CActiveForm', array(
                'id'=>'institute-form',
                'action'=>$this->createUrl('institutions/saveInstitution'),
                'enableAjaxValidation'=>true,
                'htmlOptions' => array('enctype' => 'multipart/form-data')
        ));         
        ?>
         <?php echo  $form->errorSummary(array($model, $addressmodel, $usermodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
            <div class="row">
                <div class="col-sm-3 col-md-2">
                    <div class="form-group">
                        <label for="photo_id" class="control-label">Profile Photo</label>
                        <div class="photo-section">
                            <img src="" alt="" id="profile-pic" class="img-responsive">
                            <input type="hidden" name="photo_id" id="photo_id" value="">
                            <button type="button" class="add-media btn btn-primary btn-xs" id="photo-btn">Add Logo</button>            
                            <button type="button" class="remove-media btn btn-primary btn-xs" id="remove-photo-btn" style="display: none">Remove Logo</button>
                        </div>
                    </div>
                </div>
                <div class="col-sm-9 col-md-10">

                    <div class="panel panel-default">
                        <div class="panel-heading">Institution Details</div>
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="name" class="control-label">Name</label>
                                        <?php echo $form->textField($model,'name',array('class'=>'form-control','placeholder'=>'')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Number of pupils</label>
                                        <?php echo $form->textField($model,'num_pupils',array('class'=>'form-control','placeholder'=>'')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label>Status</label>
                                        <select class="form-control" name="status">
                                            <option value="pending">Pending</option>
                                            <option value="active">Active</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- .panel -->

                    <div class="panel panel-default">
                        <div class="panel-heading">Address</div>
                        <div class="panel-body">
                           <div id="location-fields">
                                <?php $this->renderPartial('//addresses/address-form', array('form'=>$form,'addressmodel'=>$addressmodel)); ?>
                            </div> 

                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">Subscription Plan</div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Payment Interval</label>
                                        <select class="form-control" name="interval">
                                            <option value="month">Monthly</option>
                                            <option value="year">Yearly</option>
                                        </select>
                                        <small class="help-block">How often should the plan be billed</small>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Base Price</label>
                                        <div class="input-group">
                                            <span class="input-group-addon">£</span>
                                            <?php echo $form->textField($planmodel,'price',array('class'=>'form-control','placeholder'=>'')); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Offer Expiry Date</label>
                                        <input type="text"  autocomplete='off' name="active_to" class="datepicker form-control" value="<?php echo ((empty($planmodel->active_to) || $planmodel->active_to == null || $planmodel->active_to == "0000-00-00") ? "" : $planmodel->active_to); ?>">
                                        <small class="help-block">When will this plan stop being available</small>
                                    </div>
                                </div>
                            </div>                    
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">Admin Account Details</div>
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="forenames" class="control-label">Forenames</label>
                                        <?php echo $form->textField($usermodel,'forenames',array('class'=>'form-control','placeholder'=>'Forenames')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="surname" class="control-label">Surname</label>
                                        <?php echo $form->textField($usermodel,'surname',array('class'=>'form-control','placeholder'=>'Surname')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="email" class="control-label">Email</label>
                                            <?php echo $form->textField($usermodel,'email',array('class'=>'form-control','placeholder'=>'Email')); ?>
                                    </div>
                                </div>
                            </div><!-- .row -->

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="password" class="control-label">Password</label>
                                        <?php $usermodel->password = ""; ?>
                                        <?php echo $form->passwordField($usermodel,'password',array('class'=>'form-control','placeholder'=>'Password','id'=>'password')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="password_confirmation" class="control-label">Confirm Password</label>
                                        <input class="form-control" placeholder="Confirm Password" id="confirmpassword" name="Users[confirmpassword]" type="password">
                                    </div>
                                </div>
                            </div><!-- .row -->

                        </div>
                    </div>

                </div>
            </div>
            <?php echo CHtml::submitButton('Save',array("class"=>"btn btn-primary")); ?>   

         <?php $this->endWidget(); ?>
    </div>
</div>


