<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs=array(
    'Candidates'=>array('index'),
    'Manage',
);
?>

<h3>Job Applications</h3>
<?php 
    $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'profile-job-grid',
	'summaryText'=>'',
	'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',
    'dataProvider'=>JobApplications::model()->searchByID($model->id),
        	'columns'=>array(
        	    
            			array(
            					'header'=>'Company Name',
            					'value'=>'$data->getCompany($data->job->owner_id)',        			        
            			),
            			array(
            					'header'=>'Job Title',
            			        'value'=>'$data->job->title',
            					
            			),
                	    array(
                	        'header'=>'Salary',
                	        'value'=>'$data->job->salary_min." to ".$data->job->salary_min." per ".$data->job->salary_type',
                	        
                	    ),
                	    array(
                	        'header'=>'Location',
                	        'value'=>'$data->job->location',
                	        
                	    ),
                	    array(
                	        'header'=>'Applied on',
                	        'value'=>'$data->job->updated_at',
                	    )
        	 ),
  ));
?>