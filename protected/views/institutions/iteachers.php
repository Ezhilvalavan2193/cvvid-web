<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */

$this->breadcrumbs=array(
	'Institutions'=>array('index'),
	'Manage',
);

// $this->menu=array(
// 	array('label'=>'List Institutions', 'url'=>array('index')),
// 	array('label'=>'Create Institutions', 'url'=>array('create')),
// );

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#inst-admin-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<script>
$(document).ready(function(){
	
	$('#export-candidate').on('click',function() {
	    window.location = '<?php echo $this->createUrl('users/exportCSV') ?>'
	});

});

</script>

<div class="page-title text-left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $model->name." Dashboard" ?></h1>
            </div>
        </div>
    </div>
</div>
<div id="page-content">
    <div class="container">

        <div class="row">
            <div class="col-sm-12">
                <div class="box dashboard-header clearfix">
                       <?php
		        $tgscnt = TutorGroupsStudents::model()->countByAttributes(array('institution_id'=>$model->id));
		        $total = $model->num_pupils;
 			?>
                            
                    <div class="dashboard-header-section">
                        <div class="vertical-table">
                            <div class="vertical-cell">
                                <div id="progress" data-progress="" data-limit="<?php echo $total?>" data-current="<?php echo $tgscnt?>" style="position: relative;">
                                </div>
                            </div>
                            <div class="vertical-cell">
                               <?php echo $tgscnt ?> of <?php echo $total ?> accounts in use
                            </div>
                        </div>
                    </div>
                    <?php if($model->stripe_active > 0) { ?>
                    <div class="dashboard-header-section dashboard-header-section--buttons">
                        <a href="<?php echo $this->createUrl('institutions/mysubscription'); ?>" class="btn btn-primary">View Subscription</a>
                    </div>
					<?php } ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-3 col-md-2">
                <ul class="box dashboard-links list-unstyled">
                    <li><a href="<?php echo $this->createUrl('institutions/insTeachers',array('id'=>$model->id))?>">Teachers</a></li>
                    <li><a href="<?php echo $this->createUrl('institutions/insClasses',array('id'=>$model->id))?>">Classes</a></li>
                    <li><a href="<?php echo $this->createUrl('institutions/insStudents',array('id'=>$model->id))?>">Students</a></li>
                </ul>
            </div>
            <div class="col-sm-9 col-md-10">
                <div class="dashboard-table">

                    <?php
                    $this->widget('zii.widgets.grid.CGridView', array(
                        'id' => 'inst-teac-grid',
                         'itemsCssClass' => 'table',
                        'summaryText' => '',
                       // 'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                        'enableSorting' => true,
                        'ajaxUpdate' => true,
    'pager' => array(
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'header'=>'',
        'firstPageLabel' => '<<',
        'lastPageLabel' => '>>',
        'nextPageLabel' => '>>',
        'prevPageLabel' => '<<'),
                        'ajaxUpdate' => false,
                        'dataProvider' => $instusers->searchByInstitution($model->id),
                        //'filter'=>$model,
                        'columns' => array(
                            array(
                                'header' => 'Teacher',
                                'value' => 'CHtml::link($data->user->forenames." ".$data->user->surname,(Yii::app()->user->getState("role") == "admin" ? Yii::app()->createUrl("institutions/editInsTeacher", array("id"=>$data->id)) : Yii::app()->createUrl("institutions/ueditTeacher", array("id"=>$data->id))),array("class"=>"edit-button"))',
                                'type' => 'raw'
                            ),
                            array(
                                'header' => 'Class',
                                'value' => '$data->getClass()'
                            ),
                            array(
                                'header' => 'Created',
                                'value' => '$data->created_at'
                            ),
                            array(
                                'header' => 'Active Students',
                                'value' => '$data->getActiveStudents()'
                            )
                        ),
                    ));
                    ?>
                    <?php echo CHtml::button("Add Teacher", array("class" => "btn btn-default", "onclick" => "window.location='" . Yii::app()->createUrl('institutions/createteachers',array("id"=>$model->id)) . "'")) ?>


                </div>
            </div>
        </div>

    </div>
</div>

