<?php
/* @var $this InstitutionsController */
/* @var $model Institutions */

$this->breadcrumbs=array(
	'Institutions'=>array('index'),
	'Manage',
);

// $this->menu=array(
// 	array('label'=>'List Institutions', 'url'=>array('index')),
// 	array('label'=>'Create Institutions', 'url'=>array('create')),
// );

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#inst-tutor-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<script>
$(document).ready(function(){
	
	$('#export-candidate').on('click',function() {
	    window.location = '<?php echo $this->createUrl('users/exportTutorsCSV') ?>'
	});
	
});

</script>

    <h1>Tutor Groups</h1>
    <div class="Admin__content__inner">
        <div class="AdminFilters clearfix">
            <div class="pull-left">
                    <div class="form-inline">
                        <?php 
                        $this->renderPartial('_searchtutors',array(
                            'model'=>$model,'insmodel'=>$insmodel,'fullname'=>$fullname
                        )); 
                        ?>
                    </div>
            </div>
            <div class="pull-right">
                <?php       
                        echo CHtml::button('Create',array("class"=>"btn btn-primary","onclick"=>"window.location='".Yii::app()->createUrl('institutions/createTutorGroup')."'"));
                  ?>
            </div>
        </div>
        <?php 
        $this->widget('zii.widgets.grid.CGridView', array(
            'id' => 'inst-tutor-grid',
             'itemsCssClass' => 'table table-striped',
            'summaryText' => '',
           // 'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
            'enableSorting' => true,
            'ajaxUpdate' => true,
    'pager' => array(
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'header'=>'',
        'firstPageLabel' => '<<',
        'lastPageLabel' => '>>',
        'nextPageLabel' => '>>',
        'prevPageLabel' => '<<'),
            'ajaxUpdate' => false,
            'dataProvider' => ($namesearch > 0 ? $model->searchByName($namearray) : $model->search()),
            //'filter'=>$model,
            'columns' => array(
                array(
                    'header' => 'Name',
                    'value' => 'CHtml::link("<i class=\"fa fa-pencil\"></i> ".$data->name,Yii::app()->createUrl("institutions/editTutorGroup", array("id"=>$data->id)),array("class"=>"profile-edit-button","id"=>"profile-edit"))',
                    'type' => 'raw'
                ),
                array(
                    'header' => 'Teacher',
                    'value' => 'isset($data->teacher) ? $data->teacher->forenames." ".$data->teacher->surname : ""'
                ),
                array(
                    'header' => 'Institutions',
                    'value' => '$data->institution->name',
                ),
                array(
                    'header' => 'Students',
                    'value' => '$data->getCount($data->id)',
                ),
                array(
                    'header' => 'Created At',
                    'value' => '$data->created_at',
                )
            )
        ));
        ?>
        
    </div>


