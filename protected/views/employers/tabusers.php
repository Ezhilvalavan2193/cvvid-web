<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs = array(
    'Employers' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});

");
?>


<h1><?php echo $model->name . ": Users"; ?></h1>
<?php if(Yii::app()->user->hasFlash('success')):?>
<div class="alert alert-success alert-dismissable">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
   <?php echo Yii::app()->user->getFlash('success'); ?>
</div>
<?php endif; ?>
<div class="EmployerUsers">
    <!-- Nav tabs -->
    <ul class="AdminTabs nav nav-tabs" role="tablist">
        <li role="presentation"><a href="<?php echo Yii::app()->createUrl("employers/tabedit", array("id" => $model->id)); ?>" aria-controls="details">Details</a></li>
        <li role="presentation" class="active"><a href="<?php echo Yii::app()->createUrl("employers/tabusers", array("id" => $model->id)); ?>" aria-controls="users">Users</a></li>
        <li role="presentation"><a href="<?php echo Yii::app()->createUrl("employers/tabjobs", array("id" => $model->id)); ?>" aria-controls="jobs">Jobs</a></li>
        <li role="presentation"><a href="<?php echo Yii::app()->createUrl("employerOffices/admin", array("id" => $model->id)); ?>" aria-controls="offices">Offices</a></li>
        <li role="presentation"><a href="<?php echo Yii::app()->createUrl("employers/tabupgrade", array("id" => $model->id)); ?>" aria-controls="upgrade">Upgrade</a></li>
    </ul>
    <?php echo CHtml::button('Create', array("class" => "btn btn-primary", "onclick" => "window.location='" . Yii::app()->createUrl('employers/employeruserCreate', array('id' => $model->id)) . "'")); ?>
    <?php
    $this->widget('zii.widgets.grid.CGridView', array(
        'id' => 'employer-users-grid',
        'itemsCssClass' => 'table',
        'summaryText' => '',
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'dataProvider' => EmployerUsers::model()->searchByID($model->id),
        'columns' => array(
            array(
                'header' => 'Name',
                'value' => 'CHtml::link("<i class=\"fa fa-pencil\"></i> ".$data->user->forenames." ".$data->user->surname,Yii::app()->createUrl("employers/UsersEdit",array("id"=>$data->id)),array("class"=>"employer-users-button","id"=>"employer-users-edit"))',
                'type' => 'raw'
            ),
            array(
                'header' => 'Email',
                'value' => '$data->user->email',
            ),
            array(
                'header' => 'Account Role',
                'value' => '$data->role',
            ),
            array(
                'header' => 'Registered On',
                'value' => '$data->created_at',
            )
        ),
    ));
    ?>
</div>


