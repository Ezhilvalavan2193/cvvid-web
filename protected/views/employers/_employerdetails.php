<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs = array(
    'Users' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
");
?> 
<script>
//$(document).ready(function(){
//	
//	$('#slug_field').on('blur', function() {
//
//		 var ele = $(this);
//	 	 var value = $(this).val();
//	 	 if(value != "")
//	 	 {
//        	 $.ajax({
//                   url: '<?php echo $this->createUrl('users/checkSlug')?>',
//                   type: 'POST',
//        	       data: {slug : value},
//                   success: function(data) {
//        				if(data == -1)
//                            $(ele).closest('.slug-field').removeClass('has-success').addClass('has-error');
//        				else
//        					$(ele).closest('.slug-field').removeClass('has-error').addClass('has-success');
//                   },
//        		    error: function(data) {		
//        		      //  alert(data);
//        		    }
//               });			
//	 	 }
//	 	 else
//	 	 {
//	 		$(ele).closest('.slug-field').removeClass('has-success').addClass('has-error');
//	 	 }
//     	 return false;
//    });
//});

</script>
<div class="row">
    <div class="col-sm-3 col-md-2">
        <?php
        $usermodel = Users::model()->findByPk($model->user_id);
        $meidatmodel = Media::model()->findByPk($profilemodel->id);
        $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $model->id, 'type' => $usermodel->type));

        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'employer-form',
            'action' => $this->createUrl('employers/updateEmployerDetails', array('id' => $model->id)),
            'enableAjaxValidation' => false,
            'htmlOptions' => array('enctype' => 'multipart/form-data')
        ));
        ?>

        <div class="form-group">
            <div style="text-align: center;">Profile Photo</div>
            <div class="photo-section">
                <img src="<?php echo Yii::app()->baseUrl ?>/images/media/<?php echo $profilemodel->photo_id ?>/conversions/thumb.jpg" alt="" id="profile-pic" class="img-responsive">
                <input type="hidden" name="photo_id" id="photo_id" value="<?php echo $profilemodel->photo_id ?>">
                <button type="button" class="profile-btn add-media">Add Logo</button>            
                <button type="button" class="profile-btn" id="remove-photo-btn">Remove Logo</button>
            </div>
        </div>
    </div>


    <div class="col-sm-9 col-md-10">
        <div>

            <div class="form">
                <div class="row">
                    <h3 class="summary" style="text-align:left;margin-bottom: 20px;">Details</h3>

                    <div class="formBlk col-xs-6">
                        <div class="formRow">
                            <?php echo $form->labelEx($model, 'name'); ?>
                            <?php echo $form->textField($model, 'name', array('class' => 'textBox')); ?>
                            <?php echo $form->error($model, 'name'); ?>
                        </div>
                    </div>
                   <div class="formBlk col-xs-6">
                        <div class="form-group">
                            <?php echo $form->labelEx($profilemodel,'slug'); ?>
                            <div class="input-group slug-field <?php echo isset($slugerr) && $slugerr != "" ? "has-error" : ""?>">
                                <span class="input-group-addon"><?php echo "http://".$_SERVER['SERVER_NAME']."/"?></span>
                                <?php echo $form->textField($profilemodel,'slug', array('class' => 'form-control','id'=>'slug','placeholder'=>'Slug','required'=>'required')); ?>
                           	<span class="input-group-addon help-block"></span>
                         	<?php echo $form->error($profilemodel,'slug'); ?>
                            </div>
                            
                        </div>
                        
                    </div>
                    </div>
                      <div class="row">
                    <div class="formBlk col-xs-6">
                        <div class="formRow">
                            <?php echo $form->labelEx($model, 'website'); ?>
                            <?php echo $form->textField($model, 'website', array('class' => 'textBox')); ?>
                            <?php echo $form->error($model, 'website'); ?>
                        </div>
                    </div>
               

                <div class="formBlk col-xs-6">
                    <div class="formRow">
                        <?php echo $form->labelEx($model, 'tel'); ?>
                        <?php echo $form->textField($model, 'tel', array('class' => 'textBox')); ?>
                        <?php echo $form->error($model, 'tel'); ?>
                    </div>
                </div>
               </div>
                <div class="row">
                    <hr>
                    <h3 class="summary" style="text-align:left;margin-bottom: 20px;">Address</h3>

                    <div class="formBlk">
                        <div class="formRow">
                            <?php echo $form->labelEx($addressmodel, 'address'); ?>
                            <?php echo $form->textArea($addressmodel, 'address', array('class' => 'textArea')); ?>
                            <?php echo $form->error($addressmodel, 'address'); ?>
                        </div>
                    </div>

                    <div class="formBlk col-xs-4">
                        <div class="formRow">
                            <?php echo $form->labelEx($addressmodel, 'postcode'); ?>
                            <?php echo $form->textField($addressmodel, 'postcode', array('class' => 'textBox location-search')); ?>
                            <?php echo $form->error($addressmodel, 'postcode'); ?>
                             <input id="location_lat" name="Addresses[latitude]" type="hidden" value="<?php echo $addressmodel->latitude ?>">
                            <input id="location_lng" name="Addresses[longitude]" type="hidden" value="<?php echo $addressmodel->longitude ?>">
                        </div>
                    </div>

                    <div class="formBlk col-xs-4">
                        <div class="formRow">
                            <?php echo $form->labelEx($addressmodel, 'town'); ?>
                            <?php echo $form->textField($addressmodel, 'town', array('class' => 'textBox')); ?>
                            <?php echo $form->error($addressmodel, 'town'); ?>
                        </div>
                    </div>
                    <div class="formBlk col-xs-4">
                        <div class="formRow">
                            <?php echo $form->labelEx($addressmodel, 'county'); ?>
                            <?php echo $form->textField($addressmodel, 'county', array('class' => 'textBox')); ?>
                            <?php echo $form->error($addressmodel, 'county'); ?>
                        </div>
                    </div>
                    <div class="formBlk col-xs-4">
                        <div class="formRow">
                            <?php echo $form->labelEx($addressmodel, 'country'); ?>
                            <?php echo $form->textField($addressmodel, 'country', array('class' => 'textBox')); ?>
                            <?php echo $form->error($addressmodel, 'country'); ?>
                        </div>
                    </div>
                </div> 
                <div class="row">
                    <hr>
                    <h3 class="summary" style="text-align:left;margin-bottom: 20px;">Body</h3>

                    <div class="formBlk">
                        <div class="formRow">
                            <?php echo CHtml::textArea('body', $model->body, array('empty' => 'Click ball to enter/edit comments', 'style' => 'width:100%;', 'id' => 'comment_box')); ?>
                        </div></div>
                    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl . '/ckeditor/ckeditor.js' ?>"></script>
                    <script type="text/javascript">
    CKEDITOR.replace('body', {
        filebrowserBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=files',
        filebrowserImageBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=images',
        filebrowserFlashBrowseUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/browse.php?type=flash',
        filebrowserUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=files',
        filebrowserImageUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=images',
        filebrowserFlashUploadUrl: '<?php echo Yii::app()->baseUrl; ?>/kcfinder/upload.php?type=flash'
    });
                    </script>
                </div>
            </div>


        </div> 
        <div class="row buttons" style="margin-bottom: 13px;">
            <?php echo CHtml::submitButton('Save', array("class" => "btn btn-primary")); ?>

            <?php //echo CHtml::submitButton('Delete', array("class" => "btn btn-primary", "style" => "margin-left: 50px;background-color:red")); ?>
        </div>
        <?php $this->endWidget(); ?>

    </div>
</div>
<div class="col-sm-12">
    <div class="panel panel-primary" style="border-color: #337ab7;border: 1px solid">
        <div class="panel-body" style="padding: 15px;">
            <form method="POST" action="<?php echo $this->createUrl('employers/deleteEmployer', array('id' => $model->id)); ?>" accept-charset="UTF-8">
                <button class="btn btn-danger">Delete Account</button>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    <?PHP if ( isset($profilemodel->id) ) { ?>
    $(function(){
        new EmployerSlugGenerator(<?PHP ECHO $profilemodel->id ?>);
    });
   <?PHP } ?>
</script>
