<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs = array(
    'Employers' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});

");
?> 
<script>
    
    $(function () {
        $('#subscription_ends_at, #trial_ends_at').datetimepicker();
    });
$(document).ready(function () {
    
      $(".extendSubscription").click(function (e) {
            e.preventDefault();
            $('#extendSubscription').addClass('in');
            $('body').addClass('modal-open');
            $('#extendSubscription').show();
        });

        $(".extendTrail").click(function (e) {
            e.preventDefault();
            $('#extendTrail').addClass('in');
            $('body').addClass('modal-open');
            $('#extendTrail').show();
        });
        
       $('.subs .open-datetimepicker').click(function (event) {
            event.preventDefault();
            $('#subscription_ends_at').datetimepicker('show');
        });

        $('.trail .open-datetimepicker').click(function (event) {
            event.preventDefault();
            $('#trial_ends_at').datetimepicker('show');
        });
	
$('.ProfileEdit__operations').on('click', '.visiblity', function (e) {
    e.preventDefault();

    var status = $(this).attr('id');
    var employerid = $('#employer_id').val();

    $.ajax({
        url: '<?php echo $this->createUrl('employers/changeEmployerVisiblity') ?>',
        type: 'POST',
        async: false,
        data: {status: status, employerid: employerid},
        success: function (data) {
            $(".ProfileEdit__operations").load(location.href + " .ProfileEdit__operations>*", "");
        },
        error: function (data) {
            alert('err');
        }
    });
    return false;
});

$('.ProfileEdit__operations').on('click', '.emp-status', function (e) {
    e.preventDefault();

    var status = $(this).attr('id');
    var employerid = $('#employer_id').val();

    $.ajax({
        url: '<?php echo $this->createUrl('employers/adminChangeStatus') ?>',
        type: 'POST',
        async: false,
        data: {status: status, employerid: employerid},
        success: function (data) {
            $(".ProfileEdit__operations").load(location.href + " .ProfileEdit__operations>*", "");
        },
        error: function (data) {
            alert('err');
        }
    });
    return false;
});

$("#admin_upgrade").click(function()
{
	$('#NewConversationModal').addClass('in');
    $('body').addClass('modal-open');
    $('#NewConversationModal').show();
});

$(".SendDateBtn").click(function()
{
	    var date = $("#upgrade_end_date").val();
	    var employerid = $('#employer_id').val();

	    $.ajax({
	        url: '<?php echo $this->createUrl('employers/adminChangeEnddate') ?>',
	        type: 'POST',
	        async: false,
	        data: {date: date, employerid: employerid},
	        success: function (data) {
	        	  location.reload();
	        },
	        error: function (data) {
	            alert('err');
	        }
	    });

});

 $("#trails_end").click(function (e) {
        
           var trails = $('#trial_ends_at').val();
        
            $.ajax({
                url: '<?php echo $this->createUrl('employers/extendTrail') ?>',
                type: 'POST',
                data: {"employer_id":<?php echo $model->id ?>, "trial_ends_at" : trails},
                success: function (data) {
                    if (data > 0)
                    {   
                        alert('successfully updated');
                        location.reload();
                    }
                    else
                        alert('error in updated');

                }
            });

        });
        
        $("#subs_ends").click(function (e) {
        
           var trails = $('#subscription_ends_at').val();
        
            $.ajax({
                url: '<?php echo $this->createUrl('employers/extendSubscription') ?>',
                type: 'POST',
                data: {"employer_id":<?php echo $model->id ?>, "subscription_ends_at" : trails},
                success: function (data) {
                    if (data > 0)
                    {   
                        alert('successfully updated');
                        location.reload();
                    }
                    else
                        alert('error in updated');

                }
            });

        });
        
        $(".profileBasic").click(function (e) {
        
         if(confirm("are you sure to make basic"))
         {
             $.ajax({
                url: '<?php echo $this->createUrl('employers/makeBasic') ?>',
                type: 'POST',
                data: {"employer_id":<?php echo $model->id ?>},
                success: function (data) {
                    if (data > 0)
                    {   
                        alert('successfully updated');
                        location.reload();
                    }
                    else
                        alert('error in updated');

                }
            });
         }

        });

});
</script>

<div id="profile">
    <?php
    $employerid = isset($this->employerid) ? $this->employerid : 0;
    $userid = isset($this->userid) ? $this->userid : 0;
    $institutionid = isset($this->institutionid) ? $this->institutionid : 0;
    $edit = isset($this->edit) ? $this->edit : 0;
    $this->renderPartial('//profiles/Cover', array('userid' => $userid, 'employerid' => $employerid, 'institutionid' => $institutionid, 'edit' => $edit));
    ?>
    <?php
        $usermodel = new Users();
        $profilemodel = new Profiles();
        if($model->user_id > 0)
        {
            $usermodel = Users::model()->findByPk($model->user_id);
            $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $model->id, 'type' => $usermodel->type));
        }
    ?>
    <div id="form-details">
        <div class="container">
            <div class="form-details-inner">
                <div class="form-user-details form-section">
                    <div class="row">
                        <div class="text-center col-sm-4 col-md-3 col-lg-2">
                            <div class="ProfileEdit__photo">
                                <?php 									     
                                    if($profilemodel->photo_id > 0)
                                     {
                                         $mediamodel = Media::model()->findByPk($profilemodel->photo_id);
                                         $src = ((!empty($mediamodel->file_name) && $mediamodel->file_name != null) ? Yii::app()->baseUrl."/images/media/".$profilemodel->photo_id."/".$mediamodel->file_name : Yii::app()->baseUrl."/images/profile.png");
                                     } 
                                    else
                                    {
                                        $src = Yii::app()->baseUrl."/images/defaultprofile.jpg";
                                    }
                                    echo CHtml::image($src,"",array("id" => "profile-image",'alt'=>''));
                                   ?>
                                <?php if (isset($this->edit) && $this->edit) { ?>
                                    <a class="open-media profile-photo-btn" data-field="photo" data-target="#profile-image">
                                        <i class="fa fa-camera"></i>
                                        Change Photo
                                    </a>
                                <?php } ?>
                            </div>
                             <?php echo CHtml::hiddenField('employer_id', $model->id, array('id' => 'employer_id')); ?>
                            <?php if(Yii::app()->user->getState('role') == "employer") { 
                                $cri = new CDbCriteria();
                                $cri->condition = "owner_id =".$model->id." and type like '%employer%'";
                                $profilemodel = Profiles::model()->find($cri);
                                ?>
                            <a class="btn btn-primary" href="<?php echo $this->createUrl('employers/edit', array('slug' => $profilemodel->slug)) ?>">
                                <i class="fa fa-pencil"></i>
                                <span>Edit Profile</span>
                            </a>
                            <?php } ?>
                        </div>
                        <div class="col-sm-5 col-md-6 col-lg-6">
                            <div class="ProfileEdit__details">
                                <h3>
                                    <?php echo $model->name; ?>
                                        <?php
                                    $trail = "";
                                    if ($model->trial_ends_at != null) {
                                        $trail = date('Y-m-d', strtotime($model->trial_ends_at));
                                        $expiry = date('d/m/Y', strtotime($model->trial_ends_at));
                                    }
                                   // echo "Asdadsa==".(Membership::subscribed($model->stripe_active, $model->trial_ends_at, $model->subscription_ends_at) ? "33" : "dd");
                                    if(Membership::subscribed($model->stripe_active, $model->trial_ends_at, $model->subscription_ends_at)) {
//                                    if (($model->trial_ends_at != null && Membership::onTrail($trail)) || $model->stripe_active > 0) {
                                         $vacancy_txt = '';
                                         if($model->stripe_plan == 'employer_vacancy' && $model->current_vacancy_count > 0 )
                                            $vacancy_txt = '( '.$model->current_vacancy_count.' Vacancy)';
                                        ?>
                                        - <small class="subscription-marker subscription-marker--premium">Premium Account <?php echo $vacancy_txt ?></small>
                                    <?php } else { ?>
                                        - <small class="subscription-marker subscription-marker--basic">Basic Account</small>
                                    <?php } ?>
                                </h3>
                                <?php 
                                    $cri = new CDbCriteria();
                                    $cri->condition = "model_id =".$model->id." and model_type like '%employer%' and deleted_at is null";
                                    $addressmodel = Addresses::model()->find($cri);
                                    if($addressmodel != null)
                                    {
                                        $address = "";
                                        if($addressmodel->address != "")
                                            $address = $addressmodel->address.",";
                                        if($addressmodel->town != "")
                                            $address .= $addressmodel->town.",";
                                        if($addressmodel->postcode != "")
                                            $address .= $addressmodel->postcode.",";
                                        if($addressmodel->country != "")
                                            $address .= $addressmodel->country;
                                    }
                                ?>
                                <div>Location: <?php echo ($addressmodel == null ? $model->location : $address) ?></div>
                                <div>Phone: <?php echo $model->tel ?></div>
                                <div>Website: <?php echo $model->website ?></div>
                                <?php //if(Yii::app()->user->getState('role') == "admin")?>
                                <div>Contact: <?php echo $usermodel->forenames." ".$usermodel->surname;//Yii::app()->user->getState('username'); ?></div>
                            </div>
                        </div>
                        
                        <?php if(Yii::app()->user->getState('role') == "admin"){ ?>
                         <div class="col-sm-3 col-md-4 col-lg-4">
                            <div class="ProfileEdit__operations">
                                
                                    <div>
                                    <?php 
                                         $empusermodel = EmployerUsers::model()->findByAttributes(array('employer_id'=>$model->id));
                                         $umodel = Users::model()->findByPk($empusermodel->user_id);
                                    ?>
                                	<?php if ($umodel->status == "active") { ?>
                                        <a class="Profile__visibility btn emp-status btn-danger" id="<?php echo $umodel->status ?>" href="javascript::void(0)" title=""  aria-describedby="tooltippublic"><i class="fa fa-lock"></i>Block</a>
                                        
                                    <?php } else { ?>
                                        <a class="Profile__visibility btn emp-status btn-primary" id="<?php echo $umodel->status ?>" href="javascript::void(0)"  title="" aria-describedby="tooltipprivate"><i class="fa fa-unlock"></i>Unblock</a>
                                        
                                    <?php } ?>
<!--                                	</div>
                                	<div>-->
                                     <?php if ($profilemodel->visibility) { ?>
                                        <a class="Profile__visibility btn visiblity btn-success" id="<?php echo $profilemodel->visibility ?>" href="javascript::void(0)" data-original-title="Currently Private, only those who know your profile can find your account" title=""  aria-describedby="tooltippublic"><i class="fa fa-unlock"></i>Public</a>
                                        <div class="tooltip fade top" role="tooltip" id="tooltippublic" style="top: -50px; left: 146.766px; display: block;"><div class="tooltip-arrow" style="left: 50%;"></div><div class="tooltip-inner">Currently Public, your profile can be found by employer searches</div></div>
                                    <?php } else { ?>
                                        <a class="Profile__visibility btn visiblity btn-default" id="<?php echo $profilemodel->visibility ?>" href="javascript::void(0)"  data-original-title="Currently Public, your profile can be found by employer searches" title="" aria-describedby="tooltipprivate"><i class="fa fa-lock"></i>Private</a>
                                        <div class="tooltip fade top" role="tooltip" id="tooltipprivate" style="top: -67px; left: 146.766px; display: block;"><div class="tooltip-arrow" style="left: 50%;"></div><div class="tooltip-inner">Currently Private, only those who know your profile can find your account</div></div>
                                    <?php } ?>
                                        </div>
                                   <?php if (Membership::subscribed($model->stripe_active, $model->trial_ends_at, $model->subscription_ends_at) && $model->subscription_ends_at != null && Membership::onGracePeriod($model->subscription_ends_at)) { ?> 
                                        - <small class="subscription-marker subscription-marker--premium">Premium Account</small>
                                        <div>
                                            <a class="btn btn-default profileBasic" style="text-align: center"><i class="fa fa-check"></i> Basic</a>
                                            <a class="btn btn-default extendSubscription btn-success" style="text-align: center"><i class="fa fa-calendar"></i> Extend Subscription</a>
                                        </div>
                                    <?php } else if (Membership::subscribed($model->stripe_active, $model->trial_ends_at, $model->subscription_ends_at) && $model->trial_ends_at != null && Membership::onTrail($model->trial_ends_at)) { ?>
                                        - <small class="subscription-marker subscription-marker--basic">Free Trail Account</small>
                                        <div>
                                            <a class="btn btn-default profileBasic" style="text-align: center"><i class="fa fa-check"></i> Basic</a>
                                            <a class="btn btn-default extendTrail btn-success" style="text-align: center"><i class="fa fa-calendar"></i> Extend Trail</a>
                                        </div>
                                    <?php } else{ ?>
                                         - <small class="subscription-marker subscription-marker--basic">Basic Account</small>
                                        <div>
                                            <a class="btn btn-default extendSubscription" style="text-align: center"><i class="fa fa-check"></i> Upgrade</a>
                                        </div>
                                 <?php   } ?>
                                    
                                </div>
                        </div>
                        </div>
                        <?php } ?>
                        
                    </div>
                </div>
				<?php if($model->video_id > 0) { ?>
     				<div class="form-section">
                        <div class="form-section-inner">
                            <div class="section-header">
                                <h4>Company Video Profile</h4>
                            </div>
                         <?php $videomodel = Videos::model()->findByPk($model->video_id); ?>
                             <?php
//                             $pvmodel = ProfileVideos::model()->findByAttributes(array('profile_id' => $profilemodel->id, 'is_default' => 1));
                             if ($videomodel != null) {
                                $bucket = "cvvid-new";                               
                                $url = 'http://' . Yii::app()->params['AWS_BUCKET'] . '.s3.amazonaws.com/' . $videomodel->video_id;
                                ?>
                                <style>

                                    #my-video {
                                        font-size: 0.9em;
                                        height: 0;
                                        overflow: hidden;
                                        padding-bottom: 53%;
                                        padding-top: 20px;
                                        position: relative;
                                        width:100%!important;
                                    }
                                    #my-video embed, #my-video object, #my-video iframe {
                                        max-height: 100%;
                                        max-width: 100%;
                                        height: 100%;
                                        left: 0pt;
                                        position: absolute;
                                        top: 0pt;
                                        width: 100%;
                                    }
                                    .jwplayer {
                                        width: 100%!important;}
                                    </style>
                                    <div id="my-video"></div>
                                <script type="text/javascript">
                                    jwplayer("my-video").setup({
                                        'file': '<?php echo $url; ?>', //'https://s3.us-east-2.amazonaws.com/cvvid/small.mp4',
    //                                 width: "100%",
    //                                 height: "100%",
                                        // 'primary': 'flash',
                                        //'autostart': 'true'
                                    });

                                </script>
                            <!-- <div data-type="vimeo" data-video-id="<?php //echo $videomodel->video_id ?>"></div>-->
                        </div>
                    </div>
                <?php } } ?>
                <div class="form-section">
                    <div class="form-section-inner">
                        <div class="section-header">
                            <h4>Information</h4>
                        </div>
                        <div class="section-content">
                            <div class="EmployerProfile__body"><?php echo $model->body; ?></div>
                        </div>
                    </div>
                </div>

                <div class="form-section">
                    <div class="form-section-inner">
                        <div class="section-header">
                            <h4>Advertised Jobs</h4>
                        </div>
                        <div class="section-content">
                            <div class="EmployerProfile__jobs">
                                <?php 
                                $jcri = new CDbCriteria();
                                $jcri->condition = "careers <= 0 and owner_id =".$model->id;
                                $jobsmodel = Jobs::model()->findAll($jcri);
                                if (count($jobsmodel) > 0) {
                                ?>
                                <?php foreach ($jobsmodel as $job) { ?>
                                <div class="EmployerProfile__job">
                                    <h4><a href="<?php echo $this->createUrl('jobs/viewJob',array('id'=>$job['id'],'title'=>  str_replace(' ', '-', $job['title']))) ?>"><?php echo $job['title'] ?></a></h4>
                                    <ul>
                                        <li><?php echo "&pound;".$job['salary_min']; ?> - <?php echo "&pound;".$job['salary_max']; ?> per <?php echo $job['salary_type']; ?></li>
                                        <li><?php echo $job['location']; ?></li>
                                        <li><?php
                                $time_elapsed = timeAgo($job['created_at']);
                                echo $time_elapsed; ?></li>
                                    </ul>
                                    <div class="EmployerProfile__job__description"><?php echo $job['description'] ?></div>
                                </div>
                                <?php } ?>
                                <?php } else { ?>
                                <div class="EmployerProfile__job-empty">
                                    There are currently no jobs posted
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div><!-- /.container -->
    </div>
</div>

  <!--conver messgae-->
<!--    <div id="NewConversationModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closepopup()"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">End Date</h4>
                </div>
                <div class="modal-body">
                    <form class="MessageForm">
                    	<div class="row">
                         <div class="col-sm-6">
                            <div class="form-group">
                                <label>End Date</label>
                                <input type="text" name="end_date"  autocomplete='off' class="datepicker form-control" value="" id="upgrade_end_date">
                            </div>
                        </div>
                        </div>
                    </form>
                    <div class="modal-loader">
                        <img src="<?php echo Yii::app()->baseUrl?>/images/loading.gif">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default-btn" data-dismiss="modal" onclick="closepopup()">Close</button>
                    <button type="button" class="SendDateBtn btn main-btn">Submit</button>
                </div>
            </div> /.modal-content 
        </div> /.modal-dialog 
    </div>   -->

<?php 
//Function definition

function timeAgo($time_ago)
{
    $time_ago = strtotime($time_ago);
    $cur_time   = time();
    $time_elapsed   = $cur_time - $time_ago;
    $seconds    = $time_elapsed ;
    $minutes    = round($time_elapsed / 60 );
    $hours      = round($time_elapsed / 3600);
    $days       = round($time_elapsed / 86400 );
    $weeks      = round($time_elapsed / 604800);
    $months     = round($time_elapsed / 2600640 );
    $years      = round($time_elapsed / 31207680 );
    // Seconds
    if($seconds <= 60){
        return "just now";
    }
    //Minutes
    else if($minutes <=60){
        if($minutes==1){
            return "one minute ago";
        }
        else{
            return "$minutes minutes ago";
        }
    }
    //Hours
    else if($hours <=24){
        if($hours==1){
            return "an hour ago";
        }else{
            return "$hours hrs ago";
        }
    }
    //Days
    else if($days <= 7){
        if($days==1){
            return "yesterday";
        }else{
            return "$days days ago";
        }
    }
    //Weeks
    else if($weeks <= 4.3){
        if($weeks==1){
            return "a week ago";
        }else{
            return "$weeks weeks ago";
        }
    }
    //Months
    else if($months <=12){
        if($months==1){
            return "a month ago";
        }else{
            return "$months months ago";
        }
    }
    //Years
    else{
        if($years==1){
            return "one year ago";
        }else{
            return "$years years ago";
        }
    }
}
?>


<?php
$this->beginWidget(
        'booster.widgets.TbModal', array('id' => 'extendSubscription')
);
?>
<div class="modal-header">
    <a class="close" data-dismiss="modal" onclick='closepopup()'>&times;</a>
    <h4 class="msg-title">Extend Subscription</h4>
</div>

<div class="modal-body">

    <div class="MessageForm">
        <div class="alert alert-success" style="display:none;">
            Your message has been successfully sent
        </div>

        <div class="form-group">
            <label class="control-label">Subscription ends At</label>
            <div class='input-group date subs' id='datetimepicker1'>
                <?php 
                $dt = new DateTime();
                $current =  $dt->format('m/d/Y H:i');
                ?>
                <input type='text' name="subscription_ends_at" value="<?php echo ($model->subscription_ends_at == null ? $current : date("m/d/Y H:i", strtotime($model->subscription_ends_at)));  ?>" id="subscription_ends_at" class="form-control"/>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar open-datetimepicker"></span>
                </span>
            </div>
        </div>
        
    </div>


</div>

<div class="modal-footer">
    <?php
    echo CHtml::button('Send', array("class" => "btn btn-default", "id" => "subs_ends"));
    echo CHtml::button('Cancel', array("class" => "btn btn-default", "onclick" => "closepopup()"));
    ?>
</div>

<?php $this->endWidget(); ?>