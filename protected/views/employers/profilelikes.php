<?php
/* @var $this EmployersController */
/* @var $model Users */

$this->breadcrumbs = array(
    'Users' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
		
");
?> 

<script>
$(document).ready(function(){
	
	$('#profile-documents').on('submit','#new-doc-form',function(e){
		 e.preventDefault();	
		var formdata = new FormData(this);
		 $.ajax({
	            url: '<?php echo $this->createUrl('users/saveUserDoc')?>',
	            type: 'POST',
		        data: formdata,
				contentType: false,       
				cache: false,             
				processData:false,
	            success: function(data) {
						$('#profile-documents').html(data);	
	            },
			    error: function(data) {		
			      //  alert(data);
			    }
	        });			
		
	   return false;
   });
	   
	
});

</script>
<div id="profile">
    <?php
    $employerid = isset($this->employerid) ? $this->employerid : 0;
    $userid = isset($this->userid) ? $this->userid : 0;
    $institutionid = isset($this->institutionid) ? $this->institutionid : 0;
    $edit = isset($this->edit) ? $this->edit : 0;
    $this->renderPartial('//profiles/Cover', array('userid' => $userid, 'employerid' => $employerid, 'institutionid' => $institutionid, 'edit' => $edit));
    ?>
    <div id="form-details">
        <div class="container">
             <?php 
                echo CHtml::hiddenField('user_id',$userid,array('id'=>'user_id')); 
             ?>
            <div class="form-details-inner">
               <div class="form-user-details form-section">
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="text-center col-sm-4 col-md-3 col-lg-2">
                            <div class="ProfileEdit__photo">
                                <?php
                                if ($profilemodel->photo_id > 0) {
                                    $mediamodel = Media::model()->findByPk($profilemodel->photo_id);
                                    $src = ((!empty($mediamodel->file_name) && $mediamodel->file_name != null) ? Yii::app()->baseUrl . "/images/media/" . $profilemodel->photo_id . "/" . $mediamodel->file_name : Yii::app()->baseUrl . "/images/profile.png");
                                } else {
                                    $src = Yii::app()->baseUrl . "/images/defaultprofile.jpg";
                                }
                                echo CHtml::image($src, "", array("id" => "profile-image", 'alt' => ''));
                                ?>
                                <?php if (isset($this->edit) && $this->edit) { ?>
                                    <a class="open-media profile-photo-btn" data-field="photo" data-target="#profile-image">
                                        <i class="fa fa-camera"></i>
                                        Change Photo
                                    </a>
                                <?php } ?>
                            </div>
                            <a class="btn btn-primary" href="<?php echo $this->createUrl('employers/employer', array('slug' => $profilemodel->slug)) ?>">
                                <i class="fa fa-search"></i>
                                <span>View Profile</span>
                            </a>
                        </div>
                        <div class="col-sm-5 col-md-5 col-lg-6">
                            <div class="ProfileEdit__details">
                                <h3>
                                    <?php echo $model->name; ?>
                                    <?php
                                    $trail = "";
                                    if ($model->trial_ends_at != null) {
                                        $trail = date('Y-m-d', strtotime($model->trial_ends_at));
                                        $expiry = date('d/m/Y', strtotime($model->trial_ends_at));
                                    }

                                    if (($model->trial_ends_at != null && Membership::onTrail($trail)) || $model->stripe_active > 0) {
                                        ?>
                                        - <small class="subscription-marker subscription-marker--premium">Premium Account</small>
                                    <?php } else { ?>
                                        - <small class="subscription-marker subscription-marker--basic">Basic Account</small>
                                    <?php } ?>
                                </h3>
                                <div>Location: <?php echo $model->location ?></div>
                                <div>Phone: <?php echo $model->tel ?></div>
                                <div>Website: <?php echo "http://" . $_SERVER['SERVER_NAME'] . '/' . $profilemodel->slug ?></div>
                                <?php if (Yii::app()->user->getState('username'))  ?>
                                <div>Contact: <?php echo Yii::app()->user->getState('username'); ?></div>
                                 <ul class="ProfileEdit__details__meta list-inline">                                            
                                    <li><a href="<?php echo $this->createUrl('employers/viewLikes',array('id'=>$profilemodel->id))?>" class="viewlikes"><i class="fa fa-thumbs-up"></i><?php echo count(ProfileLikes::model()->findAllByAttributes(array('profile_id'=>$profilemodel->id))) ?> Likes</a></li>
                                  </ul>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-4 col-lg-4">
                            <div class="ProfileEdit__operations">
                                <div>
                                    <?php if ($model->published) { ?>
                                        <a class="Profile__active btn publish btn-success" id="<?php echo $model->published; ?>" href="javascript::void(0)"><i class="fa fa-check"></i>Active</a>
                                    <?php } else { ?>
                                        <a class="Profile__active btn publish btn-default" id="<?php echo $model->published; ?>" href="javascript::void(0)"><i class="fa fa-close"></i>Inactive</a>
                                    <?php } ?>

                                    <?php if ($profilemodel->visibility) { ?>
                                        <a class="Profile__visibility btn visiblity btn-success" id="<?php echo $profilemodel->visibility ?>" href="javascript::void(0)" data-original-title="Currently Private, only those who know your profile can find your account" title=""  aria-describedby="tooltippublic"><i class="fa fa-unlock"></i>Public</a>
                                        <div class="tooltip fade top" role="tooltip" id="tooltippublic" style="top: -50px; left: 146.766px; display: block;"><div class="tooltip-arrow" style="left: 50%;"></div><div class="tooltip-inner">Currently Public, your profile can be found by employer searches</div></div>
                                    <?php } else { ?>
                                        <a class="Profile__visibility btn visiblity btn-default" id="<?php echo $profilemodel->visibility ?>" href="javascript::void(0)"  data-original-title="Currently Public, your profile can be found by employer searches" title="" aria-describedby="tooltipprivate"><i class="fa fa-lock"></i>Private</a>
                                        <div class="tooltip fade top" role="tooltip" id="tooltipprivate" style="top: -67px; left: 146.766px; display: block;"><div class="tooltip-arrow" style="left: 50%;"></div><div class="tooltip-inner">Currently Private, only those who know your profile can find your account</div></div>
                                    <?php } ?>
                                </div>
                                <div>
                                    <?php  if (($model->trial_ends_at == null && !$model->stripe_active)){ ?>
                                        <a class="btn btn-default btn-upgrade" href="<?php echo $this->createUrl('employers/upgrade'); ?>">
                                            <i class="fa fa-user"></i>
                                            <span>Basic</span>
                                        </a>
                                    <?php } else { 
                                         if (Membership::onTrail($trail) || $model->stripe_active) {
                                            ?>
                                            <div class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Expires on <?php echo $expiry; ?>">
                                                <i class="fa fa-credit-card"></i>
                                                <span>Premium</span>
                                            </div>
                                        <?php } else { ?>
                                            <a class="btn btn-default" href="<?php echo $this->createUrl('employers/mysubscription') ?>">
                                                <i class="fa fa-credit-card"></i>
                                                <span>Account</span>
                                            </a>
                                        <?php
                                        }
                                     } ?>

                                    <a class="btn btn-default" href="<?php echo $this->createUrl('employers/employer', array('slug' => $profilemodel->slug)) ?>">
                                        <i class="fa fa-user"></i>
                                        <span>View</span>
                                    </a>
                                </div>
                                <?php
                                $owner = Employers::model()->findAllByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
                                if ($owner):
                                
                                $cri = new CDbCriteria();
                                $cri->condition = "owner_id =".$model->id." and type like '%employer%'";
                                $profilemodel = Profiles::model()->find($cri);
                                
                                    ?>      
                                    <div>
                                        <a class="btn default-btn" href="<?php echo $this->createUrl('employers/editDetails', array('slug' => $profilemodel->slug)) ?>">
                                            <i class="fa fa-pencil"></i>
                                            <span>Edit</span>
                                        </a>
                                        <a class="btn default-btn" href="<?php echo $this->createUrl('employers/users', array('id' => $model->id)) ?>">
                                            <i class="fa fa-group"></i>
                                            <span>Users</span>
                                        </a>
                                    </div>
                                <?php endif; ?>
                                <?php if (Membership::onTrail($trail) || $model->stripe_active): ?>
                                    <div>
                                        <?php
                                        $convmem = ConversationMembers::model()->findAllByAttributes(array('user_id' => $profilemodel->owner_id, 'last_read' => null));
                                        ?>
                                        <a class="btn default-btn" href="<?php echo $this->createUrl('employers/inbox', array('id' => $model->id)) ?>">
                                            <i class="fa fa-envelope"></i>
                                            <span>Inbox (<?php echo count($convmem) ?>)</span>
                                        </a>

                                        <a class="btn btn-default create-message" href="#">
                                            <i class="fa fa-envelope-o"></i>
                                            <span>Message</span>
                                        </a>
                                    </div>
                                <?php endif; ?>
                            </div>               
                        </div>
                    </div>
                </div>

                <dE iv class="form-section">
                    <div class="form-section-inner">
                        <h2>Liked Profiles</h2>
                        <div class="favourites-list">
                            <?php
                            $likemodel = ProfileLikes::model()->findAllByAttributes(array('profile_id' => $profilemodel->id));
                            if (count($likemodel) > 0) {
                                ?> 

                                <div class="row">
                                    <?php
                                    foreach ($likemodel as $like) {
                                        $plike = Profiles::model()->findByPk($like['like_id']);
                                        if ($plike->photo_id > 0) {
                                            $mediamodel = Media::model()->findByPk($plike->photo_id);
                                            $src = ((!empty($mediamodel->file_name) && $mediamodel->file_name != null) ? Yii::app()->baseUrl . "/images/media/" . $plike->photo_id . "/" . $mediamodel->file_name : Yii::app()->baseUrl . "/images/profile.png");
                                        } 
                                        else
                                            $src = Yii::app()->baseUrl . "/images/defaultprofile.jpg";
                                        ?>
                                        <div class="favourite-profile">
                                            <div class="col-sm-3">
                                                <img src="<?php echo $src ?>" class="img-responsive">
                                                <?php 
                                                $cri1 = new CDbCriteria();
                                                $cri1->condition = "owner_id =".$plike->owner_id." and type like '%candidate%'";
                                                $candprofile = Profiles::model()->find($cri1);
                                                ?>
                                                <a href="<?php echo $this->createUrl('users/profile', array('slug' => $candprofile['slug'])) ?>">
                                                    <h5 style="text-align: center;"><?php echo $plike->slug ?></h5>
                                                </a>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            <?php } else { ?>
                                <p>You currently dont have any liked profiles.</p>
                            <?php } ?>
                        </div>
                    </div>
                </div>

            </div>
        </div><!-- /.container -->
    </div>
</div>

