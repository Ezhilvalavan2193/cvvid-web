<?php
/* @var $this EmployersController */
/* @var $model Employers */
/* @var $form CActiveForm */
?>
<div class="page-title text-left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1> 
                    Edit Profile Details
                </h1>
            </div>
        </div>
    </div>
</div>
<div id="page-content">
    <div class="container">
       <?php
           
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'employer-form',
                'action' => $this->createUrl('employers/updateProfileDetails', array('id' => $model->id)),
                'enableAjaxValidation' => false,
                'htmlOptions' => array('enctype' => 'multipart/form-data')
            ));
            ?>
             <?php echo $form->errorSummary(array($model, $addressmodel,$usermodel,$profilemodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
          <?php 
              $usermodel = Users::model()->findByPk($model->user_id);
              $logusermodel = Users::model()->findByPk(Yii::app()->user->getState('userid'));
              $profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $model->id, 'type' => $usermodel->type));
              
          ?>
            <h4>Company details</h4>
            <div class="form-container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <?php echo $form->textField($model, 'name', array('class' => 'form-control','placeholder'=>"Name")); ?>
                            <?php echo $form->error($model, 'name'); ?>
                        </div>
                    </div>
<!--                     <div class="col-sm-3"> -->
<!--                         <div class="form-group"> -->
                            <?php //echo $form->textField($model, 'location', array('class' => 'form-control location-search')); ?>
                            <?php //echo $form->error($model, 'location'); ?>
                            <input id="location_lat" name="location_lat" type="hidden" value="<?php echo $model->location_lat; ?>">
                            <input id="location_lng" name="location_lng" type="hidden" value="<?php echo $model->location_lng; ?>">
<!--                         </div> -->
<!--                     </div> -->
                    <div class="col-sm-3">
                        <div class="form-group">
                            <?php echo $form->textField($model, 'website', array('class' => 'form-control','placeholder'=>"Web Address")); ?>
                            <?php echo $form->error($model, 'website'); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <select name="industry_id" class="form-control">
                                <?php
                                $industrymodel = Industries::model()->findAll();
                                foreach ($industrymodel as $ind) {
                                    ?>                  
                                    <option value="<?php echo $ind['id'] ?>" <?php echo ($profilemodel->industry_id == $ind['id'] ? 'selected' : '') ?>> <?php echo $ind['name'] ?> </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div><!-- .row -->
            </div><!-- .form-container -->

	 <div class="form-container">
                    <h4>Address</h4>
                     <div id="location-fields">
                        <?php $this->renderPartial('//addresses/address-form', array('form' => $form, 'addressmodel' => $addressmodel)); ?>
                    </div>

                </div>
            <div class="form-container">
                <!-- Custom url -->
                <h4>Custom URL</h4>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-inline slug-field">
                            <div class="form-group">http://www.cvvid.com/</div>
                            <div class="form-group">
                                <?php echo $form->textField($profilemodel, 'slug', array('class' => 'form-control','placeholder'=>"firstname-surname",'id'=>'slug')); ?>
                            </div>
                            <div class="form-group">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-container">
                <!-- Your details -->
                <h4>Your details</h4>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="form-group">
                            <?php echo $form->textField($logusermodel, 'forenames', array('class' => 'form-control','placeholder'=>"First Name")); ?>
                            <?php echo $form->error($logusermodel, 'forenames'); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                            <?php echo $form->textField($logusermodel, 'surname', array('class' => 'form-control','placeholder'=>"Surname")); ?>
                            <?php echo $form->error($logusermodel, 'surname'); ?>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="form-group">
                            <div class="form-group">
                                <?php echo $form->textField($logusermodel, 'current_job', array('class' => 'form-control','placeholder'=>"Current Job Title")); ?>
                                <?php echo $form->error($logusermodel, 'current_job'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                             <?php echo $form->textField($logusermodel, 'email', array('class' => 'form-control','placeholder'=>"E-mail Address")); ?>
                            <?php echo $form->error($logusermodel, 'email'); ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                           <?php  $logusermodel->password = "";?>
                             <?php echo $form->passwordField($logusermodel, 'password', array('class' => 'form-control','placeholder'=>"Password")); ?>
                          
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                             <?php echo $form->passwordField($logusermodel,'confirmpassword',array('class'=>'form-control','placeholder'=>"Confirm Password")); ?>
                        </div>
                    </div>
                </div>
            </div>


            <div class="form-actions">
                <?php
                echo CHtml::submitButton('Update', array("class" => "btn btn-primary"));
                $cri = new CDbCriteria();
                $cri->condition = "owner_id =".$model->id." and type like '%employer%'";
                $profilemodel = Profiles::model()->find($cri);
                ?>
                <a class="btn default-btn" href="<?php echo $this->createUrl('employers/edit', array('slug' => $profilemodel->slug)) ?>">Back</a>
            </div>

       <?php $this->endWidget(); ?>
    </div><!-- .container -->
</div>


<script type="text/javascript">
    <?PHP if (isset($profilemodel->id)) { ?>
    $(function(){
        new EmployerSlugGenerator(<?PHP ECHO $profilemodel->id ?>);
    });
   <?PHP } ?>
</script>
