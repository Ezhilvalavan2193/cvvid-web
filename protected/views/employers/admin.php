<?php
/* @var $this EmployersController */
/* @var $model Employers */

$this->breadcrumbs=array(
	'Employers'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#employers-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<script>
$(document).ready(function(){
	

	$('#delete-employer').click(function(e){

		
         if($("#employers-grid").find("input:checked").length > 0)
         {

        	 if (confirm('Are you sure you want to delete these employers?')) {

         		 var ids = $.fn.yiiGridView.getChecked("employers-grid", "selectedIds");
             	
       			  $.ajax({
       		            url: '<?php echo $this->createUrl('employers/deleteEmployers') ?>',
       		            type: 'POST',
       		            data: {ids : ids}, 	    
       		            success: function(data) {
                                $.fn.yiiGridView.update('employers-grid');
       			            //alert(data);
       		            },
                            error: function(data) {		
                                alert('err');
                            }
       		         });
        	 } 
        	
         }
         else
         {
            alert('Please select at least one item');
         }
	});
	
});

</script>
<h1>Employers</h1>
<?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
<div class="Admin__content__inner">
    <div class="AdminFilters">
        <div class="form-inline">
           <?php $this->renderPartial('_search',array(
                    'model'=>$model,
            )); ?>
            <div class="pull-right">
                  <?php  
                        echo CHtml::button('Bulk Create',array("class"=>"btn btn-primary","onclick"=>"window.location='".Yii::app()->createUrl('employers/bulkCreate')."'"));
                        echo CHtml::button('Create',array("class"=>"btn btn-primary","onclick"=>"window.location='".Yii::app()->createUrl('employers/create')."'"));
                        echo CHtml::button('Delete',array("class"=>"btn btn-primary",'id'=>'delete-employer'));
                  ?>
            </div>
        </div>
    </div>
 
  <?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'employers-grid',
        'itemsCssClass' => 'table',
	'summaryText'=>'',
	//'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',
    'pager' => array(
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'header'=>'',
        'firstPageLabel' => '<<',
        'lastPageLabel' => '>>',
        'nextPageLabel' => '>>',
        'prevPageLabel' => '<<'),
			'ajaxUpdate'=>false,
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
			//'id',
                        array(
                            // 'name' => 'check',
                             'id' => 'selectedIds',
                             'value' => '$data->id',
                             'class' => 'CCheckBoxColumn',
                             'selectableRows'  => 2,
                            // 'checked'=>'Yii::app()->user->getState($data->id)',
                            // 'checkBoxHtmlOptions' => array('name' => 'idList[]'),

                         ),
			array(
					'header'=>'Name',
					'value'=>'CHtml::link("<i class=\"fa fa-pencil\"></i> ".$data->name,Yii::app()->createUrl("employers/tabedit",array("id"=>$data->id)),array("class"=>"employer-edit-button","id"=>"employer-edit"))',
			                'type'=>'raw'
			),
			array(
					'header'=>'Profile',
					'value'=>'CHtml::link("View",$data->getSlugURL(),array("class"=>"profile-view-button","id"=>"profile-view"))',
					'type'=>'raw'
			),
			array(
					'header'=>'Main User',
					'value'=>'$data->user_id > 0 ? $data->user->forenames." ".$data->user->surname : ""',
			),
	),
)); ?>

</div>


