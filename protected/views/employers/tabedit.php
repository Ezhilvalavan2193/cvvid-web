<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs = array(
    'Employers' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});

");
?>
<script>
    $(document).ready(function () {

        $('#slug_field').on('blur', function () {

            var ele = $(this);
            var value = $(this).val();
            if (value != "")
            {
                $.ajax({
                    url: '<?php echo $this->createUrl('users/checkSlug') ?>',
                    type: 'POST',
                    data: {slug: value},
                    success: function (data) {
                        if (data == -1)
                            $(ele).closest('.slug-field').removeClass('has-success').addClass('has-error');
                        else
                            $(ele).closest('.slug-field').removeClass('has-error').addClass('has-success');
                    },
                    error: function (data) {
                        //  alert(data);
                    }
                });
            } else
            {
                $(ele).closest('.slug-field').removeClass('has-success').addClass('has-error');
            }
            return false;
        });
    });

</script>

<h1><?php echo $model->name; ?></h1>
<div class="EmployerForm">
    <!-- Nav tabs -->
    <ul class="AdminTabs nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="<?php echo Yii::app()->createUrl("employers/tabedit", array("id" => $model->id)); ?>" aria-controls="details">Details</a></li>
        <li role="presentation"><a href="<?php echo Yii::app()->createUrl("employers/tabusers", array("id" => $model->id)); ?>" aria-controls="users">Users</a></li>
        <li role="presentation"><a href="<?php echo Yii::app()->createUrl("employers/tabjobs", array("id" => $model->id)); ?>" aria-controls="jobs">Jobs</a></li>
        <li role="presentation"><a href="<?php echo Yii::app()->createUrl("employerOffices/admin", array("id" => $model->id)); ?>" aria-controls="offices">Offices</a></li>
        <li role="presentation"><a href="<?php echo Yii::app()->createUrl("employers/tabupgrade", array("id" => $model->id)); ?>" aria-controls="upgrade">Upgrade</a></li>
    </ul>


    <?php
    
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'employer-form',
        'action' => $this->createUrl('employers/updateEmployerDetails', array('id' => $model->id)),
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data')
    ));
    ?>
      <?php echo  $form->errorSummary(array($model,$usermodel,$profilemodel,$addressmodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
    <?php if(Yii::app()->user->hasFlash('success')):?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
       <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
    <?php endif; ?>
    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="details">

            <div class="row">
                <div class="col-sm-3 col-md-2">
                    <div class="form-group">
                        <div class="photo-section">
                            <?php
                             if ($profilemodel->photo_id > 0) {
                                    $mediamodel = Media::model()->findByPk($profilemodel->photo_id);
                                    $src = ((!empty($mediamodel->file_name) && $mediamodel->file_name != null) ? Yii::app()->baseUrl . "/images/media/" . $profilemodel->photo_id . "/" . $mediamodel->file_name : Yii::app()->baseUrl . "/images/profile.png");
                                } else {
                                    $src = Yii::app()->baseUrl . "/images/defaultprofile.jpg";
                                }
                            ?>
                            <img src="<?php echo $src ;?>" alt="" id="profile-pic" class="img-responsive">
                            <input type="hidden" name="photo_id" id="photo_id" value="<?php echo $profilemodel->photo_id ?>">
                            <button type="button" class="add-media btn btn-primary btn-xs">Add Logo</button>            
                            <button type="button" class="remove-media btn btn-primary btn-xs" id="remove-photo-btn" style="<?php  if ($profilemodel->photo_id < 1) { echo 'display:none'; } ?>">Remove Logo</button>
                        </div>
                    </div>
                </div>
                <div class="col-sm-9 col-md-10">

                    <div class="panel panel-default">
                        <div class="panel-heading">Details</div>
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <?php echo $form->labelEx($model, 'name'); ?>
                                        <?php echo $form->textField($model, 'name', array('class' => 'form-control', 'placeholder' => 'Name')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <?php echo $form->labelEx($profilemodel, 'slug'); ?>
                                        <div class="input-group slug-field <?php echo isset($slugerr) && $slugerr != "" ? "has-error" : "" ?>">
                                            <span class="input-group-addon"><?php echo "http://" . $_SERVER['SERVER_NAME'] . "/" ?></span>
                                            <?php echo $form->textField($profilemodel, 'slug', array('class' => 'form-control', 'id' => 'slug', 'placeholder' => 'Slug', 'required' => 'required')); ?>
                                            <span class="input-group-addon help-block"></span><?php echo $form->error($profilemodel, 'slug'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <?php echo $form->labelEx($model, 'website'); ?>
                                        <?php echo $form->textField($model, 'website', array('class' => 'form-control', 'placeholder' => 'Website')); ?>
                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <?php echo $form->labelEx($model, 'tel'); ?>
                                        <?php echo $form->textField($model, 'tel', array('class' => 'form-control', 'placeholder' => 'Tel')); ?>
                                    </div>
                                </div>
                            </div>
                             <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <?php echo $form->labelEx($model, 'email'); ?>
                                        <?php echo $form->textField($model, 'email', array('class' => 'form-control', 'placeholder' => 'Email')); ?>
                                    </div>

                                </div>
                             
                            </div>
                             <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                    		<?php $usermodel->password = "";?>
                                        <?php echo $form->labelEx($usermodel, 'password'); ?>
                                        <?php echo $form->passwordField($usermodel, 'password', array('class' => 'form-control', 'placeholder' => 'Password')); ?>
                                    </div>
                                </div>
                             	<div class="col-sm-6">
                                    <div class="form-group">
                                    		<?php echo $form->labelEx($usermodel, 'confirmpassword'); ?>
                               				 <input class="form-control" placeholder="Confirm Password" name="Users[confirmpassword]" id="Users_confirmpassword" type="password" value="">

                                    </div>
                                </div>
                            </div>
                             <div id="location-fields">
                            
                                <?php $this->renderPartial('//addresses/address-form', array('form'=>$form,'addressmodel'=>$addressmodel)); ?>
                            </div>


                            <div class="form-group">
                                <?php echo $form->labelEx($model, 'body'); ?>
                                <?php echo CHtml::textArea('body', $model->body, array('empty' => 'Click ball to enter/edit comments', 'style' => 'width:100%;', 'id' => 'comment_box')); ?>
                            </div>

                            <div class="form-actions clearfix">
                                <?php echo CHtml::submitButton('Save', array("class" => "btn btn-primary pull-right")); ?>
                            </div>


                        </div><!-- .panel-body -->
                    </div><!-- .panel-->

                </div><!-- .col-sm-8 -->
            </div><!-- .row -->

        </div>
    </div>

    <?php $this->endWidget(); ?>


    <div class="row">
        <div class="col-sm-offset-2 col-sm-10">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <form method="POST" action="<?php echo $this->createUrl('employers/deleteEmployer', array('id' => $model->id)); ?>" accept-charset="UTF-8">
                        <button class="btn btn-danger">Delete Account</button>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>
<script type="text/javascript">
    <?PHP if ( isset($profilemodel->id) ) { ?>
    $(function(){
        new EmployerSlugGenerator(<?PHP ECHO $profilemodel->id ?>);
    });
   <?PHP } ?>
</script>
