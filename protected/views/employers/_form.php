<?php
/* @var $this EmployersController */
/* @var $model Employers */
/* @var $form CActiveForm */
?>
<script>
//$(document).ready(function()
//{
//	$('#slug_field').on('blur', function() {
//	    
//		 var ele = $(this);
//	 	 var value = $(this).val();
//	 	 if(value != "")
//	 	 {
//      	   $.ajax({
//                 url: '<?php echo $this->createUrl('users/checkSlug')  ?>',
//                 type: 'POST',
//      	       	  data: {slug : value},
//                 success: function(data) {
//      				if(data == -1)
//                       $(ele).closest('.slug-field').removeClass('has-success').addClass('has-error');
//      				else
//      					$(ele).closest('.slug-field').removeClass('has-error').addClass('has-success');
//                 },
//      		    error: function(data) {		
//      		      //  alert(data);
//      		    }
//             });			
//	 	 }
//	 	 else
//	 	 {
//	 		$(ele).closest('.slug-field').removeClass('has-success').addClass('has-error');
//	 	 }
//   	 return false;
//  });
//
//	$('#forename,#surname').on('blur', function() {    		
//        var forename = $('#forename').val();
//        var surname = $('#surname').val();
//        $('#slug_field').val(forename+"-"+surname);
//        
//        $.ajax({
//            url: '<?php echo $this->createUrl('users/checkSlug')  ?>',
//            type: 'POST',
// 	       	  data: {slug :  $('#slug_field').val()},
//            success: function(data) {
// 				if(data == -1)
//                  $('.slug-field').removeClass('has-success').addClass('has-error');
// 				else
// 				   $('.slug-field').removeClass('has-error').addClass('has-success');
//            },
// 		    error: function(data) {		
// 		      //  alert(data);
// 		    }
//        });	             
//        
//	});
//});
</script>
<script>
$(document).ready(function() {
    $(".noSpace").keydown(function(event) {

     if (event.keyCode == 32) {

         event.preventDefault();

     }

  });

//     $('.noSpace').keyup(function() {
//  this.value = this.value.replace(/\s/g,'');
// });


});
</script>

<h1>Create Employer</h1>
<div class="Admin__content__inner">
    <div class="EmployerForm">
         <?php
         $form = $this->beginWidget('CActiveForm', array(
            'id' => 'employer-form',
            'action' => $this->createUrl('employers/createEmployerDetails'),
            'enableAjaxValidation' => false,
            'htmlOptions' => array('enctype' => 'multipart/form-data')
        ));
        ?>
       <?php echo  $form->errorSummary(array($model,$usermodel,$profilemodel,$addressmodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
            <div class="row">
                <div class="col-sm-3 col-md-2">
                    <div class="form-group">
                        <label for="photo_id" class="control-label">Profile Photo</label>
                        <div class="photo-section">
                            <img src="" alt="" id="profile-pic" class="img-responsive">
                            <input type="hidden" name="photo_id" id="photo_id" value="">
                            <button type="button" class="add-media btn btn-primary btn-xs" id="photo-btn">Add Logo</button>  
                            <button type="button" class="remove-media btn btn-primary btn-xs" id="remove-photo-btn" style="display: none">Remove Logo</button>
                        </div>
                    </div>
                </div>
                <div class="col-sm-9 col-md-10">

                    <div class="panel panel-default">
                        <div class="panel-heading">Admin Account Details</div>
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="forenames" class="control-label">Forenames</label>
                                        <?php echo $form->textField($usermodel, 'forenames', array('class' => 'form-control noSpace','placeholder'=>'Forenames','id'=>'forename')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="surname" class="control-label">Surname</label>
                                        <?php echo $form->textField($usermodel, 'surname', array('class' => 'form-control noSpace','placeholder'=>'Surname','id'=>'surname')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="email" class="control-label">Email</label>
                                         <?php echo $form->emailField($usermodel, 'email', array('class' => 'form-control noSpace','placeholder'=>'Email')); ?>
                                    </div>
                                </div>
                            </div><!-- .row -->

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="password" class="control-label">Password</label>
                                        <?php echo $usermodel->password = ""; ?>
                                        <?php echo $form->passwordField($usermodel, 'password', array('class' => 'form-control noSpace','placeholder'=>'Password')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="password_confirmation" class="control-label">Confirm Password</label>
                                       <input class="form-control noSpace" placeholder="Confirm Password" type="password" value="" name="Users[confirmpassword]" id="confirmpassword">
                                    </div>
                                </div>
                            </div><!-- .row -->

                        </div><!-- .panel-body -->
                    </div><!-- .panel -->

                    <div class="panel panel-default">
                        <div class="panel-heading">Employer Details</div>
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="name" class="control-label">Name</label>
                                        <?php echo $form->textField($model, 'name', array('class' => 'form-control','placeholder'=>'Name')); ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group slug-field">
                                        <label for="profile[slug]" class="control-label">Link</label>
                                        <div class="input-group slug-field">
                                            <span class="input-group-addon"><?php echo "http://".$_SERVER['SERVER_NAME']."/"?></span>
                                            <?php echo $form->textField($profilemodel,'slug', array('class' => 'form-control noSpace','id'=>'slug','placeholder'=>'Slug')); ?>
                                             <span class="input-group-addon help-block"></span>
                                        </div>
                            
                                        
                                    </div>
                                </div>
                            </div><!-- .row -->

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="website" class="control-label">Website</label>
                                        <?php echo $form->textField($model, 'website', array('class' => 'form-control','placeholder'=>'Website')); ?>
                                    </div>

                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="tel" class="control-label">Tel</label>
                                         <?php echo $form->textField($model, 'tel', array('class' => 'form-control','placeholder'=>'Tel')); ?>
                                    </div>
                                </div>
                            </div><!-- .row -->

                            <hr/>
                            <div id="location-fields">
                                <?php $this->renderPartial('//addresses/address-form', array('form'=>$form,'addressmodel'=>$addressmodel)); ?>
                            </div>


                            <div class="form-group">
                                <label for="body" class="control-label">Body</label>
                                <?php echo CHtml::textArea('body', '', array('empty' => 'Click ball to enter/edit comments', 'style' => 'width:100%;', 'id' => 'comment_box')); ?>
                            </div><!-- .row -->

                        </div><!-- .panel-body -->
                    </div><!-- .panel -->


                </div><!-- .col-md-9 -->
            </div><!-- .row -->

            <div class="form-actions clearfix">
                 <?php echo CHtml::submitButton('Save', array("class" => "btn btn-primary pull-right")); ?>
            </div>

         <?php $this->endWidget(); ?>
    </div><!-- .Employer -->
</div>


<script type="text/javascript">
       $(function(){
           new EmployerSlugGenerator();
       });
</script>


