<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs = array(
    'Users' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});");
?> 
<?php
$usermodel = Users::model()->findByPk(Yii::app()->user->getState('userid'));
$profilemodel = Profiles::model()->findByAttributes(array('owner_id' => $model->id, 'type' => $usermodel->type));
?>
<script>
function isVideo(filename) {
    var ext = getExtension(filename);
    switch (ext.toLowerCase()) {
        case 'm4v':
        case 'avi':
        case 'mpg':
        case 'mp4':
        case 'webm':
        case 'mov':
            return true;
    }
    return false;
}
function getExtension(filename) {
    var parts = filename.split('.');
    return parts[parts.length - 1];
}
    $(document).ready(function () {
        $('.form-section-inner').on('click', '.edit-item', function () {
            $('.section-content').toggle();
            $('.section-form').toggle();
            return false;
        });
        $('.ProfileEdit__operations').on('click', '.publish', function () {

            var status = $(this).attr('id');
            var employerid = $('#employer_id').val();

            $.ajax({
                url: '<?php echo $this->createUrl('employers/updatePublish') ?>',
                type: 'POST',
                data: {status: status, employerid: employerid},
                success: function (data) {
                    $(".ProfileEdit__operations").load(location.href + " .ProfileEdit__operations>*", "");
                },
                error: function (data) {
                    alert('err');
                }
            });


            return false;
        });

        $('.ProfileEdit__operations').on('click', '.visiblity', function (e) {
            e.preventDefault();

            var status = $(this).attr('id');
            var employerid = $('#employer_id').val();

            $.ajax({
                url: '<?php echo $this->createUrl('employers/changeEmployerVisiblity') ?>',
                type: 'POST',
                async: false,
                data: {status: status, employerid: employerid},
                success: function (data) {
                    $(".ProfileEdit__operations").load(location.href + " .ProfileEdit__operations>*", "");
                },
                error: function (data) {
                    alert('err');
                }
            });
            return false;
        });

        $('.section-form').on('click', '.main-btn', function () {
            var val = $('#body').val();
            var employer_id = $('#employer_id').val();
            if (val.length > 0) {
                $.ajax({
                    url: '<?php echo $this->createUrl('employers/saveBody') ?>',
                    type: 'POST',
                    async: false,
                    data: {body: val, employerid: employer_id},
                    success: function (data) {
                        // $("#profile-body").load(location.href + " #profile-body>*", "");
                        window.location.reload();
                    },
                    error: function (data) {
                        // alert('err');
                    }
                });
            } else {
                alert('Please fill in some details about your company.');
            }
            return false;
        });


        $('body').on('click', '.open-media', function () {
            var getValue = $(this).attr("data-field");
            if (getValue == 'cover') {
                $(".saveMedia").attr('id', 'save-media-cover');
            } else if (getValue == 'photo') {
                $(".saveMedia").attr('id', 'save-media-photo');
            }

            //alert(getValue);return false;
            $('#mediamanager_modal').addClass('in');
            $('body').addClass('modal-open');
            $('#mediamanager_modal').show();
        });

        $('body').on('click', '.create-message', function () {
            //alert(getValue);return false;
            $('#NewConversationModal').addClass('in');
            $('body').addClass('modal-open');
            $('#NewConversationModal').show();
        });

        $('.upload-media').on('click', '.medaiamanager_add', function () {
            //e.preventDefault();
            $(".media-file").trigger('click');
            //return false;
        });

        $('.modal-content').on('change', '.media-file', function (e) {

            var fileExtension = ['jpeg', 'jpg', 'png'];
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                alert("Only '.jpeg','.jpg','.png' formats are allowed.");
            } else {
                $('.MediaManager__loading').removeClass('hidden');
                var data = new FormData();
                jQuery.each(jQuery(this)[0].files, function (i, file) {
                    data.append('media', file);
                });
                data.append('collection_name', 'photos');
                data.append('model_type', "App\\Employer");
                data.append('model_id', <?php echo $model->id; ?>);
                // alert(JSON.stringify(data));
                $.ajax({
                    url: '<?php echo $this->createUrl('media/insertMediaManager') ?>',
                    type: 'POST',
                    data: data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $(".MediaManager__content").load(location.href + " .MediaManager__content>*", "");
                        $('.MediaManager__loading').addClass('hidden');
                    },
                    error: function (data) {
                        alert('err');
                    }
                });
            }

            // return false;
        });


        //  media_item_delete  
        $('body').on('click', '.MediaManager__item__delete', function (e) {

            if (confirm('Are you sure you want to delete this item?')) {

                var id = $(this).attr('id');

                $.ajax({
                    url: '<?php echo $this->createUrl('media/deleteMedia'); ?>',
                    type: 'POST',
                    data: {id: id},
                    success: function (data) {
                        //alert(data);
                        $(".MediaManager__content").load(location.href + " .MediaManager__content>*", "");
                        // $("#result_para").load(location.href + " #result_para>*", "");
                    },
                    error: function (data) {
                        alert('err');
                    }
                });
            }

            return false;

        });


        $('.modal-content').on('click', '.MediaManager__item', function (evt) {
            $('div.selected').removeClass('selected');
            $(this).addClass('selected');
        });

        $('.modal-content').on('click', '#save-media-cover', function (evt) {

            var mediaid = $(".MediaManager__content").find(".selected").attr('id');
            
            if(mediaid != undefined)
            {

            $.ajax({
                url: '<?php echo $this->createUrl('profiles/updateCover') ?>',
                type: 'POST',
                data: {mediaid: mediaid, profileid: <?php echo $profilemodel->id; ?>},
                success: function (data) {
                    window.location.reload();
                    // $("#colcover").load(location.href + " #colcover>*", "");
                    //  closepopup();
                },
                error: function (data) {
                    alert('err');
                }
            });
            
            }else{
            alert('please select image');
            }


        });


        $('.modal-content').on('click', '#save-media-photo', function (evt) {

            var mediaid = $(".MediaManager__content").find(".selected").attr('id');
            
            if(mediaid != undefined)
            {

            $.ajax({
                url: '<?php echo $this->createUrl('profiles/updatePhoto') ?>',
                type: 'POST',
                data: {mediaid: mediaid, profileid: <?php echo $profilemodel->id; ?>},
                success: function (data) {
                    window.location.reload();
                },
                error: function (data) {
                    alert('err');
                }
            });

}else{
            alert('please select image');
            }

        });


        $('#NewConversationModal').on('click', '.SendMessageBtn', function () {

            var errors = false;
            var errors_mgs = '';
            var $subject = $('input[id="subject"]');
            var subject_val = $subject.val();
            var $target_id = $('select[id="target_id"]');
            var target_val = $target_id.val();
            var $message = $('textarea[id="message"]')
            var message_val = $message.val();
            // Validate Subject
            if (target_val == '') {
                errors_mgs += '<div>The target field is required.</div>';
                errors = true;
            } else {
                errors = false;
            }

            // Validate Subject
            if (subject_val == '') {
                errors_mgs += '<div>The subject is required</div>';
                errors = true;
            } else {
                errors = false;
            }

            // Validate Message
            if (message_val == '') {
                errors_mgs += '<div>A message is required</div>';
                errors = true;
            } else {
                errors = false;
            }
            if (!errors) {
                $('.modal-loader').show();
                $.ajax({
                    url: '<?php echo $this->createUrl('employers/sendMessage') ?>',
                    type: 'POST',
                    data: {message: message_val, subject: subject_val, user_id: target_val},
                    success: function (data) {
                        $('#msg_scc_msg').show();
                        $('.modal-loader').hide();
                        setTimeout(function () {
                            closepopup();
                        }, 3000);
                    },
                });
            } else {
                $('#msg_err_msg').html(errors_mgs);
                $('#msg_err_msg').show();
            }



            return false;
        });

        $(".Profile__visibility").hover(
                function () {
                    $('.tooltip').addClass('in');
                }, function () {
            $('.tooltip').removeClass('in');
        }
        );

        $('body').on('click', '.assign-job', function (e) {
           
            $('#jobid').val($(this).attr('id'));
            $('#NewAssignModal').addClass('in');
            $('body').addClass('modal-open');
            $('#NewAssignModal').show();
            
            return false;
        });

        $('.AssignJobBtn').click(function() {
            var jobid = $('#jobid').val();
            var userid = $('#emp_user_id').val();
            $.ajax({
                url: '<?php echo $this->createUrl('employers/assignJob') ?>',
                type: 'POST',
                data: {jobid: jobid, userid : userid},
                success: function (data) {
                    
                	 $('#job_scc_msg').show();
                     setTimeout(function () {
                         closepopup();
                     }, 3000);
                },
                error: function (data) {
                    alert('err');
                }
            });
            return false;
        });
    });
      //configuration
        //var allowed_file_types = ['video/mp4', 'video/flv']; //allowed file types
        var result_output = '#output'; //ID of an element for response output
        var my_form_id = '#emp-video-form'; //ID of an element for response output
        var progress_bar_id = '.progress'; //ID of an element for response output

        //on form submit
        $(document).ready(function () {

        	$('.form-emp-videos').on('change', '#NewVideo', function (e) {
                $('#video-name').val(e.target.files[0].name);
            });
            
            $(my_form_id).on("submit", function (event) {
                if ($('#NewVideo').val() == "")
                {
                    alert("Please select video.")
                    return false;
                } 
                else
                {
                    var file = $('#NewVideo');
                    if (!isVideo(file.val())) {
                        alert('Please select a valid video file.');
                        return false;
                    }
                }
               
                event.preventDefault();
                var proceed = true; //set proceed flag
                var error = [];	//errors
                var total_files_size = 0;

                //reset progressbar
                $(".progress progress-bar").css("width", "0%");
                $(".progress .status").text("0%");
                
                if (!window.File && window.FileReader && window.FileList && window.Blob) { //if browser doesn't supports File API
                    error.push("Your browser does not support new File API! Please upgrade."); //push error text
                } else {
                    //iterate files in file input field
                    proceed = true;
                    var submit_btn = $(this).find("input[type=submit]"); //form submit button	

                    //if everything looks good, proceed with jQuery Ajax
                    if (proceed) {

                        submit_btn.val("Please Wait...").prop("disabled", true); //disable submit button
                        var form_data = new FormData(this); //Creates new FormData object
                        var post_url = $(this).attr("action"); //get action URL of form

                        //jQuery Ajax to Post form data
                        $.ajax({
                            url: post_url,
                            type: "POST",
                            data: form_data,
                            contentType: false,
                            cache: false,
                            processData: false,
                            xhr: function () {
                            	
                                //upload Progress
                                var xhr = $.ajaxSettings.xhr();
                                if (xhr.upload) {
                                    xhr.upload.addEventListener('progress', function (event) {
                                        var percent = 0;
                                        var position = event.loaded || event.position;
                                        $('.progress').show();
                                        var total = event.total;
                                        if (event.lengthComputable) {
                                            percent = Math.ceil(position / total * 100);
                                        }
                                        $(".progress").show();
                                        //update progressbar
                                        $(".progress .progress-bar").css("width", +percent + "%");
                                        $(".progress .status").text(percent + "%");

                                    }, true);
                                }
                                return xhr;
                            },
                            mimeType: "multipart/form-data"
                        }).done(function (res) {
                          
                            $(my_form_id)[0].reset(); //reset form
                            $(result_output).html(res); //output response from server
                            submit_btn.val("Upload Video").prop("disabled", false); //enable submit button once ajax is done
                            window.setTimeout(function () {
                                location.reload()
                            }, 2000);
                        });

                    }
                }

                $(result_output).html(""); //reset output 
                $(error).each(function (i) { //output any error to output element
                    $(result_output).append('<div class="error">' + error[i] + "</div>");
                });

            });

            
    });

</script>


<div id="profile">
    <?php
    $employerid = isset($this->employerid) ? $this->employerid : 0;
    $userid = isset($this->userid) ? $this->userid : 0;
    $institutionid = isset($this->institutionid) ? $this->institutionid : 0;
    $edit = isset($this->edit) ? $this->edit : 0;
    $this->renderPartial('//profiles/Cover', array('userid' => $userid, 'employerid' => $employerid, 'institutionid' => $institutionid, 'edit' => $edit));
    ?>
    <div id="form-details">
        <div class="container">
            <?php echo CHtml::hiddenField('employer_id', $model->id, array('id' => 'employer_id')); ?>
            <div class="form-details-inner">
                <div class="form-user-details form-section">
                  <?php if (Yii::app()->user->hasFlash('subscription-ends')): ?>
                		<div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                             <?php echo Yii::app()->user->getFlash('subscription-ends'); ?>
                        </div>
                     <?php endif; ?>
                    <?php if (Yii::app()->user->hasFlash('success')): ?>
                        <div class="alert alert-success alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php echo Yii::app()->user->getFlash('success'); ?>
                        </div>
                    <?php endif; ?>
					     <?php if (Yii::app()->user->hasFlash('stripe_error')): ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <?php echo Yii::app()->user->getFlash('stripe_error'); ?>
                        </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="text-center col-sm-4 col-md-3 col-lg-2">
                            <div class="ProfileEdit__photo">
                                <?php
                                if ($profilemodel->photo_id > 0) {
                                    $mediamodel = Media::model()->findByPk($profilemodel->photo_id);
                                    $src = ((!empty($mediamodel->file_name) && $mediamodel->file_name != null) ? Yii::app()->baseUrl . "/images/media/" . $profilemodel->photo_id . "/" . $mediamodel->file_name : Yii::app()->baseUrl . "/images/profile.png");
                                } else {
                                    $src = Yii::app()->baseUrl . "/images/defaultprofile.jpg";
                                }
                                echo CHtml::image($src, "", array("id" => "profile-image", 'alt' => ''));
                                ?>
                                <?php if (isset($this->edit) && $this->edit) { ?>
                                    <a class="open-media profile-photo-btn" data-field="photo" data-target="#profile-image">
                                        <i class="fa fa-camera"></i>
                                        Change Photo
                                    </a>
                                <?php } ?>
                            </div>
                            <?php 
                          
                            ?>
                            <a class="btn btn-primary" href="<?php echo $this->createUrl('employers/employer', array('slug' => $profilemodel->slug)) ?>">
                                <i class="fa fa-search"></i>
                                <span>View Profile</span>
                            </a>
                        </div>
                        <div class="col-sm-5 col-md-5 col-lg-6">
                            <div class="ProfileEdit__details">
                                <h3>
                                    <?php echo $model->name; ?>
                                    <?php
                                    $trail = "";
                                    if ($model->trial_ends_at != null) {
                                        $trail = date('Y-m-d', strtotime($model->trial_ends_at));
                                        $expiry = date('d/m/Y', strtotime($model->trial_ends_at));
                                    }
                                   // echo "Asdadsa==".(Membership::subscribed($model->stripe_active, $model->trial_ends_at, $model->subscription_ends_at) ? "33" : "dd");
                                    if(Membership::subscribed($model->stripe_active, $model->trial_ends_at, $model->subscription_ends_at)) {
//                                    if (($model->trial_ends_at != null && Membership::onTrail($trail)) || $model->stripe_active > 0) {
                                         $vacancy_txt = '';
                                         if($model->current_vacancy_count > 0 )
                                            $vacancy_txt = '( '.$model->current_vacancy_count.' Vacancy)';
                                        ?>
                                        - <small class="subscription-marker subscription-marker--premium">Premium Account <?php echo $vacancy_txt ?></small>
                                    <?php } else { ?>
                                        - <small class="subscription-marker subscription-marker--basic">Basic Account</small>
                                    <?php } ?>
                                </h3>
                                 <?php 
                                    $cri = new CDbCriteria();
                                    $cri->condition = "model_id =".$model->id." and model_type like '%employer%' and deleted_at is null";
                                    $addressmodel = Addresses::model()->find($cri);
                                    if($addressmodel != null)
                                    {
                                        $address = "";
                                        if($addressmodel->address != "")
                                            $address = $addressmodel->address.",";
                                        if($addressmodel->town != "")
                                            $address .= $addressmodel->town.",";
                                        if($addressmodel->postcode != "")
                                            $address .= $addressmodel->postcode.",";
                                        if($addressmodel->country != "")
                                            $address .= $addressmodel->country;
                                    }
                                ?>
                                <div>Location: <?php echo ($addressmodel == null ? $model->location : $address) ?></div>
                                <div>Phone: <?php echo $model->tel ?></div>
                                <div>Website: <?php echo $model->website ?></div>
                                <?php //if (Yii::app()->user->getState('username'))  ?>
                               <div>Contact: <?php echo $usermodel->forenames." ".$usermodel->surname;//Yii::app()->user->getState('username'); ?></div>
                                 <ul class="ProfileEdit__details__meta list-inline">                                            
                                    <li><a href="<?php echo $this->createUrl('employers/viewLikes',array('id'=>$profilemodel->id))?>" class="viewlikes"><i class="fa fa-thumbs-up"></i><?php echo count(ProfileLikes::model()->findAllByAttributes(array('profile_id'=>$profilemodel->id))) ?> Likes</a></li>
                                  </ul>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-4 col-lg-4">
                            <div class="ProfileEdit__operations">
                                <div>
                                    <?php if ($model->published) { ?>
                                        <a class="Profile__active btn publish btn-success" id="<?php echo $model->published; ?>" href="javascript::void(0)"><i class="fa fa-check"></i>Active</a>
                                    <?php } else { ?>
                                        <a class="Profile__active btn publish btn-default" id="<?php echo $model->published; ?>" href="javascript::void(0)"><i class="fa fa-close"></i>Inactive</a>
                                    <?php } ?>

                                    <?php if ($profilemodel->visibility) { ?>
                                        <a class="Profile__visibility btn visiblity btn-success" id="<?php echo $profilemodel->visibility ?>" href="javascript::void(0)" data-original-title="Currently Private, only those who know your profile can find your account" title=""  aria-describedby="tooltippublic"><i class="fa fa-unlock"></i>Public</a>
                                        <div class="tooltip fade top" role="tooltip" id="tooltippublic" style="top: -50px; left: 146.766px; display: block;"><div class="tooltip-arrow" style="left: 50%;"></div><div class="tooltip-inner">Currently Public, your profile can be found by employer searches</div></div>
                                    <?php } else { ?>
                                        <a class="Profile__visibility btn visiblity btn-default" id="<?php echo $profilemodel->visibility ?>" href="javascript::void(0)"  data-original-title="Currently Public, your profile can be found by employer searches" title="" aria-describedby="tooltipprivate"><i class="fa fa-lock"></i>Private</a>
                                        <div class="tooltip fade top" role="tooltip" id="tooltipprivate" style="top: -67px; left: 146.766px; display: block;"><div class="tooltip-arrow" style="left: 50%;"></div><div class="tooltip-inner">Currently Private, only those who know your profile can find your account</div></div>
                                    <?php } ?>
                                </div>
                                <div>
                                    <?php
                                    $target = Yii::app()->controller->getRoute() . "/" . $model->id;
                                    
//                                    free user
                                    if(!Membership::subscribed($model->stripe_active, $model->trial_ends_at, $model->subscription_ends_at))
                                    {
                                    ?>
                                    <a class="btn btn-default btn-upgrade upgrade" href="<?php echo $this->createUrl('employers/upgrade') ?>">
                                        <i class="fa fa-user"></i>
                                        <span>Basic</span>
                                    </a>
                                    <?php } else {
                                     if(Membership::onTrail($model->trial_ends_at)){ ?>
                                        <div class="btn btn-default" data-toggle="tooltip" data-placement="top" title="Expires on <?php echo $expiry; ?>">
                                           <i class="fa fa-credit-card"></i>
                                           <span>Premium</span>
                                       </div>
                                      <?php } else { ?>
                                         <a class="btn btn-default" href="<?php echo $this->createUrl('employers/mysubscription') ?>">
                                            <i class="fa fa-credit-card"></i>
                                            <span>Account</span>
                                        </a>
                                    <?php } } ?>
                                    
                                    <a class="btn btn-default" href="<?php echo $this->createUrl('employers/employer', array('slug' => $profilemodel->slug)) ?>">
                                        <i class="fa fa-user"></i>
                                        <span>View</span>
                                    </a>
                                </div>
                                
                                 <div>
                                 <?php  $cri = new CDbCriteria();
                                    $cri->condition = "owner_id =".$model->id." and type like '%employer%'";
                                    $profilemodel = Profiles::model()->find($cri);?>
                                        <a class="btn default-btn" href="<?php echo $this->createUrl('employers/editDetails', array('slug' => $profilemodel->slug)) ?>">
                                            <i class="fa fa-pencil"></i>
                                            <span>Edit</span>
                                        </a>
                                <?php
                                
                                $owner = Employers::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
                                if ($owner):
                                  
                                /* if(Membership::subscribed($model->stripe_active, $model->trial_ends_at, $model->subscription_ends_at))  //premium
                                      {                
                                          if($owner['premium_user_count'] > 0)
                                              $url = $this->createUrl('employers/users', array('id' => $model->id));
                                          else 
                                              $url = $this->createUrl('employers/payUser', array('id' => $model->id));
                                      }
                                    else   //Basic
                                      { 
                                          if($owner['basic_user_count'] > 0)
                                              $url = $this->createUrl('employers/users', array('id' => $model->id));
                                          else
                                              $url = $this->createUrl('employers/upgrade');
                                      }
                                          */ ?>
                                    
                                        <a class="btn default-btn" href="<?php echo $this->createUrl('employers/users', array('id' => $model->id)) ?>">
                                            <i class="fa fa-group"></i>
                                            <span>Users</span>
                                        </a>
                                       
                                  
                                <?php endif; ?>
                                  </div>
                                <?php
                               // if(Membership::subscribed($model->stripe_active, $model->trial_ends_at, $model->subscription_ends_at)): ?>
                                    <div>
                                        <?php
                                        $convmem = ConversationMembers::model()->findAllByAttributes(array('user_id' => $model->user_id, 'last_read' => null));
                                        ?>
                                        <a class="btn default-btn" href="<?php echo $this->createUrl('employers/inbox', array('id' => $model->id)) ?>">
                                            <i class="fa fa-envelope"></i>
                                            <span>Inbox (<?php echo count($convmem) ?>)</span>
                                        </a>

                                        <a class="btn btn-default create-message" href="#">
                                            <i class="fa fa-envelope-o"></i>
                                            <span>Message</span>
                                        </a>
                                    </div>
                                <?php //endif; 
                                 if ($owner): 
                                 
                                    $ofccount = count(EmployerOffices::model()->findAllByAttributes(array('employer_id' => $owner['id'])));
                                    
                                    if(Membership::subscribed($model->stripe_active, $model->trial_ends_at, $model->subscription_ends_at))  //premium
                                      {                  
                                            $ourl =  $this->createUrl('employerOffices/admin', array('id' => $model->id));
                                      }
                                    else   //Basic
                                      { 
                                          if($ofccount <= 0)
                                              $ourl =  $this->createUrl('employerOffices/admin', array('id' => $model->id));
                                          else
                                              $ourl = $this->createUrl('employers/upgrade');
                                      }
                                         ?>
                                  <div>
                                        <a class="btn default-btn" href="<?php echo $ourl ?>">
                                            <i class="fa fa-group"></i>
                                            <span>Offices</span>
                                        </a>
                                       
                                    </div>
                                 
                                <?php endif; ?>
                            </div>               
                        </div>
                    </div>
                </div>
				
				<div id="profile-documents" class="form-emp-videos form-profile-documents form-section">
					<?php 
					   $this->renderPartial('employerVideos', array('model' => $model)); 
					 ?>
				</div>
                <div id="profile-body" class="form-section">
                    <div class="form-section-inner">
                        <div class="section-header">
                            <div class="row">
                                <div class="col-sm-8">
                                    <h4>Company Background</h4>
                                </div>
                                <div class="col-sm-4">
                                    <div class="pull-right">
                                        <a class="edit-item" href="javascript::void(0)"><i class="fa fa-pencil"></i>Edit</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                       <div class="section-content">
                            <div class="section-description" <?php if ($model->body != "") { ?> style="display: none;"<?php } ?>>
                                Please fill in some details about your company.
                            </div>
                            <div class="employer-body"><?php echo $model->body; ?></div>
                        </div>
                        <div class="section-form">
                            <form>
                                <textarea class="form-control" id="body" name="body" cols="50" rows="10"><?php echo str_replace("<br />", null, $model->body); ?></textarea>
                                <div class="form-actions">
                                    <button class="main-btn" type="button">Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div id="profile-jobs" class="form-section">
                    <div class="form-section-inner">
                        <div class="section-header">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4>Posted Jobs</h4>
                                </div>
                            </div>
                        </div>
                        <div class="section-content">
                            <div class="section-description" style="overflow-x: scroll;">
                                <?php
                                $this->widget('zii.widgets.grid.CGridView', array(
                                    'id' => 'profile-jobs-grid',
                                    'itemsCssClass' => 'table table-striped',
                                    'summaryText' => '',
                                    //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
                                    'dataProvider' => Jobs::model()->searchByEmployer($model->id),
                                    'columns' => array(
                                        array(
                                            'header' => 'Job Title',
                                            'value' => 'CHtml::link($data->title,Yii::app()->createUrl("employers/jobsedit",array("slug"=>$data->getslug($data->owner_id),"id"=>$data->id)),array("class"=>"job-edit-button","id"=>"job-edit"))',
                                            'type' => 'raw',
                                        ),
                                         array(
                                            'header' => 'Salary',
                                            'value' => '$data->getSalary()',
                                        ),
                                        array(
                                            'header' => 'Type',
                                            'value' => '$data->type',
                                        ),
                                        array(
                                            'header' => 'Location',
                                            'value' => '$data->location',
                                        ),
                                        array(
                                            'header' => 'Date Added',
                                            'value' => '$data->created_at',
                                        ),
                                        array(
                                            'header' => 'Date Ending',
                                            'value' => '$data->end_date',
                                        ),
                                        array(
                                            'header' => 'Views',
                                            'value' => '$data->num_views',
                                        ),
                                        array(
                                            'header' => 'Applications',
                                            'value' => 'CHtml::link("View Applicants (".$data->getCount().")",urldecode(Yii::app()->createUrl("jobs/applications",array("id"=>$data->id,"title"=>str_replace("/","",str_replace(" ", "-", $data->title))))),array("class"=>"job-applications-button","id"=>"job-applications"))',
                                            'type' => 'raw'
                                        ),
                                        array(
                                            'header' => 'Assign',
                                            'value' => 'CHtml::link("Assign","#",array("class"=>"assign-job","id"=>$data->id))',
                                            'type' => 'raw',
                                          //  'visible'=>Jobs::model()->checkAccess()
                                        ),
                                        array(
                                            'header' => 'Duplicate',
                                            'value' => 'CHtml::link("Duplicate",Yii::app()->createUrl("employers/duplicateJobs",array("id"=>$data->id)),array())',
                                            'type' => 'raw',
                                            'visible'=>Jobs::model()->checkAccess()
                                        ),
                                      array(
                                            'header' => 'Delete',
                                            'value' => '$data->id > 0 ? (CHtml::link("<i class=\"fa fa-trash\"></i> ",Yii::app()->createUrl("jobs/jobdelete",array("id"=>$data->id)),array("class"=>"delete-button","id"=>"delete_btn"))) : ""',
                                            'type' => 'raw'
                                        ),
                                    ),
                                ));
                                ?>
                             <?php 
                              
                             if((Membership::subscribed($model->stripe_active, $model->trial_ends_at, $model->subscription_ends_at) && $model->stripe_plan != "employer_vacancy") || (Membership::subscribed($model->stripe_active, $model->trial_ends_at, $model->subscription_ends_at) && $model->stripe_plan == "employer_vacancy" && $model->current_vacancy_count > 0 )) 
                                 $url = $this->createUrl('employers/jobsadd', array('slug' => $profilemodel->slug));
                             else    
                                 $url = $this->createUrl('employers/upgrade');
                                 ?>
                                <?php //if (Membership::onTrail($trail) || $model->stripe_active): ?>
                                    <a class="main-btn" href="<?php echo $url ?>">Add New Job</a>
                                <?php //endif; ?>
                             <?php //}  ?>
                            </div>
                        </div>
                    </div>
                </div>

               <!-- Favourites -->
                <div class="form-section">
                    <div class="form-section-inner">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>Favourites</h4>
                                <div class="favourites-list mob-slideshow">
                                    <?php
                                    $favourites = ProfileFavourites::model()->findAllByAttributes(array('profile_id' => $profilemodel->id));
                                    foreach ($favourites as $cand) {
                                        
                                        $pmodel = Profiles::model()->findByPk($cand['favourite_id']);
                                        $candidate = Users::model()->findByPk($pmodel->owner_id);
                                        
                                        $agecanmodel = AgencyCandidates::model()->findByAttributes(array('user_id'=>$pmodel->owner_id));
                                        if($agecanmodel != null && Yii::app()->user->getState('role') == "employer")
                                        {
                                            $user_name = "XXXX";
                                        }
                                        else {
                                            $user_name = $candidate->forenames . " " . $candidate->surname;
                                        }
                                        
                                        if ($pmodel->photo_id > 0) {
                                            $mediamodel = Media::model()->findByPk($pmodel->photo_id);
                                            $src = Yii::app()->baseUrl . '/images/media/' . $mediamodel->id . '/conversions/search.jpg';
                                        } else {
                                            $src = Yii::app()->baseUrl . '/images/defaultprofile.jpg';
                                        }
                                        ?>
                                        <div class="search-item candidate">
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <div class="candidate-image">
                                                        <img src="<?php echo $src ?>" class="img-responsive"/>
                                                        <a class="btn hollow-btn" href=""><i class="fa fa-play"></i>Play Video CV</a>
                                                    </div>
                                                </div>
                                                <div class="col-sm-10">
                                                    <div class="candidate-name">
                                                        <a href="<?php echo $this->createUrl('users/profile', array('slug' => $pmodel->slug)) ?>">
                                                            <?php echo $user_name; ?>
                                                        </a>
                                                    </div>
                                                    <div class="candidate-meta">
                                                        <span class="candidate-job"><i class="fa fa-info"></i><?php echo $candidate->current_job; ?></span>
                                                        <span class="candidate-location"><i class="fa fa-map-marker"></i><?php echo $candidate->location; ?></span>
                                                    </div>
                                                    <div class="candidate-skills-container">
                                                        <div class="skill-label">Key Skills:</div>
                                                        <div class="candidate-skills">
                                                            <?php
                                                            $psmodel = ProfileSkills::model()->findAllByAttributes(array('profile_id' => $pmodel->id));
                                                            foreach ($psmodel as $ps) {
                                                                $skillmodel = Skills::model()->findByPk($ps['skill_id']);
                                                                ?>
                                                                <div class="candidate-skill"><?php echo $skillmodel->name ?> </div>                                    
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <div class="candidate-buttons">
                                                        <?php
                                                        $fstring = ProfileFavourites::model()->findByAttributes(array('favourite_id' => $pmodel->id));
                                                        $favourite_string = ($fstring == null) ? 'Add to Favourites' : 'Remove from Favourites';
                                                        ?>
                                                        <div><a class="favourite-link" id="<?php echo $pmodel->id ?>" href="<?php echo $this->createUrl('site/addfavourite') ?>"><i class="fa fa-heart"></i><span><?php echo $favourite_string; ?></span></a></div>
                                                        <!--  <div><a href="#" onclick="message(<?php echo $candidate->id ?>)"><i class="fa fa-envelope-o"></i>Send Message</a></div>-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div><!-- /.container -->
    </div>
</div>

<!--cover photo popup-->

<div class="modal fade" id="mediamanager_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="MediaManager__loading hidden">
                <img src="<?php echo $this->createAbsoluteUrl('//'); ?>/images/loading.gif">
            </div>
            <div class="MediaManager__header">
                <form action="" method="POST" enctype="multipart/form-data" class="add-new-form">
                    <div class="row">
                        <div class="col-xs-7">
                            <h4>Media Library</h4>
                        </div>
                        <div class="col-xs-5">
                            <div class="upload-media pull-right">
                                <button type="button" class="btn main-btn select-submit saveMedia">Save</button>
                                <label class="btn main-btn medaiamanager_add"><i class="fa fa-plus"></i>Upload a File</label>
                                <input class="media-file" type="file" style="display:none;"/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="MediaManager__content">
                <?php
                $mediamodel = Media::model()->findAll(array(
                    'condition' => 'model_id=:model_id AND model_type=:model_type AND collection_name=:collection_name',
                    'params' => array(':model_id' => $model->id, ':model_type' => 'App\Employer', ':collection_name' => 'photos'),
                ));
                if (count($mediamodel) > 0) {
                    ?>
                    <div class="row">
                        <?php foreach ($mediamodel as $media) { ?>
                            <div class="col-xs-6 col-sm-3">
                                <div class="MediaManager__item MediaManager__item--image" id="<?php echo $media['id'] ?>">
                                    <img src="<?php echo Yii::app()->baseUrl ?>/images/media/<?php echo $media['id'] ?>/conversions/search.jpg" class="img-responsive">
                                    <a class="MediaManager__item__delete" href="#" id="<?php echo $media['id'] ?>"><i class="fa fa-times"></i></a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
                <h3>CV Vid Image Library</h3>
                <?php
                $mediamodel1 = Media::model()->findAll(array(
                    'condition' => 'model_id=:model_id AND collection_name=:collection_name',
                    'params' => array(':model_id' => 1, ':collection_name' => 'sample'),
                ));
                if (count($mediamodel1) > 0) {
                    ?>
                    <div class="row">
                        <?php foreach ($mediamodel1 as $media) { ?>
                            <div class="col-xs-6 col-sm-3">
                                <div class="MediaManager__item MediaManager__item--image" id="<?php echo $media['id'] ?>">
                                    <img src="<?php echo Yii::app()->baseUrl ?>/images/media/<?php echo $media['id'] ?>/conversions/search.jpg" class="img-responsive">
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn default-btn" data-dismiss="modal" onclick="closepopup()">Close</button>
                <button type="button" class="btn main-btn select-submit saveMedia">Save</button>
            </div>

        </div>
    </div>
</div>

<!--conver messgae-->
<div id="NewConversationModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closepopup()"><span aria-hidden="true">Ã—</span></button>
                <h4 class="modal-title">Send a Message</h4>
            </div>
            <div class="modal-body">
                <form class="MessageForm">
                    <div class="alert alert-success" id='msg_scc_msg' style="display:none;">
                        Your message has been successfully sent
                    </div>
                    <div class="alert alert-danger" id='msg_err_msg' style="display: none">

                    </div>
                    <div class="form-group">
                        <label>Contact</label>
                        <select name="target_id" id="target_id" class="form-control">
                            <option value="">- Select Contact -</option>
                            <?php
                            $cri = new CDbCriteria();
                            $cri->condition = "employer_id = " . $model->id . " AND user_id <>" . Yii::app()->user->getState('userid');
                            $empusrmodel = EmployerUsers::model()->findAll($cri);
                            foreach ($empusrmodel as $empusr) {
                                $userarr = Users::model()->findByPk($empusr['user_id']);
                                ?>                  
                                <option value="<?php echo $userarr['id'] ?>"> <?php echo $userarr['forenames'] . " " . $userarr['surname']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="subject">Subject</label>
                        <input name="subject" type="text" id="subject" class="form-control" value="">
                    </div>
                    <div class="form-group">
                        <label for="message">Message</label>
                        <textarea name="message" id="message" class="form-control" rows="10"></textarea>
                    </div>
                </form>
                <div class="modal-loader">
                    <img src="http://localhost/cvvid_laran/public/images/loading.gif">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default-btn" data-dismiss="modal" onclick="closepopup()">Close</button>
                <button type="button" class="SendMessageBtn btn main-btn">Send Message</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!--conver messgae-->
<div id="NewAssignModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="closepopup()"><span aria-hidden="true">Ã—</span></button>
                <h4 class="modal-title">Assign Job to User</h4>
            </div>
            <div class="modal-body">
                <form class="MessageForm">
                    <div class="alert alert-success" id='job_scc_msg' style="display:none;">
                        Job Assigned successfully
                    </div>
                    <div class="alert alert-danger" id='job_err_msg' style="display: none">

                    </div>
                    
                    <div class="form-group">
                    	<input type="hidden" name="jobid" id="jobid"/>
                        <label for="emp_user_id">Select User</label>
                        <select name="emp_user_id" id="emp_user_id" class="form-control" >
                         <option value="">- Select User -</option>
                        	<?php 
                        	   if (Yii::app()->user->getState('role') == "employer")
                        	   {
                            	    $empusrmodel = EmployerUsers::model()->findByAttributes(array('user_id' => Yii::app()->user->getState('userid')));
                            	     if($empusrmodel->role == 'admin')
                                    {
                                        $cri = new CDbCriteria();
                                        $cri->condition = "employer_id =".$empusrmodel->employer_id." and role <> 'admin'";
                                        $empusrmodel = EmployerUsers::model()->findAll($cri);
                                        foreach ($empusrmodel as $usr)
                                        {
                                            $umodel = Users::model()->findByPk($usr['user_id']);
                                            echo "<option value=".$umodel->id.">".$umodel->forenames." ".$umodel->surname."</option>";
                                        }
                                    }
                        	   }                        	  
                            ?>
                        	
                        </select>
                    </div>
                </form>
                <div class="modal-loader">
                    <img src="http://localhost/cvvid_laran/public/images/loading.gif">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default-btn" data-dismiss="modal" onclick="closepopup()">Close</button>
                <button type="button" class="AssignJobBtn btn main-btn">Assign Job</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>