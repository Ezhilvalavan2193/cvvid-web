<?php
/* @var $this UsersController */
/* @var $model Users */

$this->breadcrumbs = array(
    'Employers' => array('index'),
    'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});

");
?>


<h1><?php echo $model->name.": Jobs"; ?></h1>
<div class="EmployerJobs">
    <!-- Nav tabs -->
    <ul class="AdminTabs nav nav-tabs" role="tablist">
        <li role="presentation"><a href="<?php echo Yii::app()->createUrl("employers/tabedit", array("id" => $model->id)); ?>" aria-controls="details">Details</a></li>
        <li role="presentation"><a href="<?php echo Yii::app()->createUrl("employers/tabusers", array("id" => $model->id)); ?>" aria-controls="users">Users</a></li>
        <li role="presentation"  class="active"><a href="<?php echo Yii::app()->createUrl("employers/tabjobs", array("id" => $model->id)); ?>" aria-controls="jobs">Jobs</a></li>
        <li role="presentation"><a href="<?php echo Yii::app()->createUrl("employerOffices/admin", array("id" => $model->id)); ?>">Offices</a></li>
        <li role="presentation"><a href="<?php echo Yii::app()->createUrl("employers/tabupgrade", array("id" => $model->id)); ?>" aria-controls="upgrade">Upgrade</a></li>
    </ul>
	 <?php echo CHtml::button('Create', array("class" => "btn btn-primary", "onclick" => "window.location='" . Yii::app()->createUrl('employers/employerjobCreate', array('id' => $model->id)) . "'")); ?>
<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'employer-jobs-grid',
     'itemsCssClass' => 'table',
    'summaryText' => '',
    //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
    'dataProvider' => Jobs::model()->searchByEmployer($model->id),
    'columns' => array(
        array(
            'header' => 'Name',
            'value' => '$data->title',
        ),
        array(
            'header' => 'Created By',
            'value' => '$data->user->forenames." ".$data->user->surname',
        ),
        array(
            'header' => 'End Date',
            'value' => '$data->end_date',
        ),
        array(
            'header' => 'Salary Min',
            'value' => '$data->salary_min',
        ),
        array(
            'header' => 'Salary Max',
            'value' => '$data->salary_max',
        )
    ),
));
?>
</div>


