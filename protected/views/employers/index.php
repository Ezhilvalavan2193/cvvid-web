<?php
/* @var $this EmployersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Employers',
);

$this->menu=array(
	array('label'=>'Create Employers', 'url'=>array('create')),
	array('label'=>'Manage Employers', 'url'=>array('admin')),
);
?>

<h1>Employers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
