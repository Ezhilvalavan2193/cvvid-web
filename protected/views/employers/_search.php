<?php

/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>

<?php

$form = $this->beginWidget('CActiveForm', array(
    'action' => Yii::app()->createUrl('employers/searchAdmin'), //Yii::app()->createUrl($this->route),
    'method' => 'get',
        ));
?>

<?php echo $form->textField($model, 'name', array('class' => 'form-control', 'placeholder' => 'search')); ?>

<?php

echo CHtml::submitButton('Search', array("class" => "btn btn-primary"));
//echo CHtml::button('Clear', array("style" => "margin-left:20px", "class" => "btn btn-primary", "onClick" => "window.location='" . Yii::app()->createUrl('employers/admin') . "'"));
?>

<?php $this->endWidget(); ?>
