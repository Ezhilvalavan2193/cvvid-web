<?php
/* @var $this UserExperiencesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'User Experiences',
);

$this->menu=array(
	array('label'=>'Create UserExperiences', 'url'=>array('create')),
	array('label'=>'Manage UserExperiences', 'url'=>array('admin')),
);
?>

<h1>User Experiences</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
