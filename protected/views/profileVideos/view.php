<?php
/* @var $this ProfileVideosController */
/* @var $model ProfileVideos */

$this->breadcrumbs=array(
	'Profile Videoses'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ProfileVideos', 'url'=>array('index')),
	array('label'=>'Create ProfileVideos', 'url'=>array('create')),
	array('label'=>'Update ProfileVideos', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ProfileVideos', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProfileVideos', 'url'=>array('admin')),
);
?>

<h1>View ProfileVideos #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'profile_id',
		'video_id',
		'is_default',
		'state',
		'created_at',
		'updated_at',
	),
)); ?>
