<?php
/* @var $this ProfileVideosController */
/* @var $model ProfileVideos */

$this->breadcrumbs=array(
	'Profile Videoses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ProfileVideos', 'url'=>array('index')),
	array('label'=>'Manage ProfileVideos', 'url'=>array('admin')),
);
?>

<h1>Create ProfileVideos</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>