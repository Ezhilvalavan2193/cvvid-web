<?php
/* @var $this ProfileExperiencesController */
/* @var $model ProfileExperiences */

$this->breadcrumbs=array(
	'Profile Experiences'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ProfileExperiences', 'url'=>array('index')),
	array('label'=>'Manage ProfileExperiences', 'url'=>array('admin')),
);
?>

<h1>Create ProfileExperiences</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>