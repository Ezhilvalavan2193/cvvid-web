<?php
/* @var $this ProfileExperiencesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Profile Experiences',
);

$this->menu=array(
	array('label'=>'Create ProfileExperiences', 'url'=>array('create')),
	array('label'=>'Manage ProfileExperiences', 'url'=>array('admin')),
);
?>

<h1>Profile Experiences</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
