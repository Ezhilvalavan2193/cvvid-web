<?php
/* @var $this InstitutionOfficesController */
/* @var $model InstitutionOffices */
/* @var $form CActiveForm */
?>
<div class="page-title text-left">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1><?php echo $model->isNewRecord ? "Add" : "Update"?> Office Location</h1>
            </div>
        </div>
    </div>
</div>
<div id="page-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
<?php
if($model->isNewRecord)
    $url = $this->createUrl('employerOffices/create',array('id'=>$insmodel->id));
else 
    $url = $this->createUrl('employerOffices/edit',array('id'=>$model->id));

   $form=$this->beginWidget('CActiveForm', array(
	   'id'=>'empp-offices-form',
       'action'=>$url,
    //'enableAjaxValidation'=>false
)); ?>
<?php echo  $form->errorSummary(array($model,$addressmodel), '<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '', array('class' => 'alert alert-danger')); ?>
<div class="panel panel-default">
    <div class="panel-body">
            <div id="location-fields">
                   <?php $this->renderPartial('//addresses/address-form', array('form'=>$form,'addressmodel'=>$addressmodel)); ?>
			</div>	
			<div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label class="control-label">Head Office</label>
                        <?php 
                            $addressmodel->head_office = 0;
                            echo $form->checkBox($addressmodel, 'head_office', array()); 
                        ?>
                    </div>
                </div>
            </div>
         </div>
     <div class="form-group">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-default')); ?>
        <?php echo CHtml::button('Cancel', array("class" => "btn btn-default", "onclick" => "window.location='" . Yii::app()->createUrl('employerOffices/admin',array('id'=>$insmodel->id)) . "'")); ?>
		
	</div>
	
</div>
<?php $this->endWidget(); ?>
</div>
</div><!-- form -->
</div>
</div>
