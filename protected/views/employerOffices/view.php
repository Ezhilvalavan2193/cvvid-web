<?php
/* @var $this InstitutionOfficesController */
/* @var $model InstitutionOffices */

$this->breadcrumbs=array(
	'Employer Offices'=>array('index'),
	$model->id,
);

$this->menu=array(
	
);
?>

<h1>View InstitutionOffices #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'address_id',
		'status',
		'created_at',
		'deleted_at',
	),
)); ?>
