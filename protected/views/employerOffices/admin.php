<?php
/* @var $this EmployerOfficesController */
/* @var $model EmployerOffices */

$this->breadcrumbs=array(
	'Employer Offices'=>array('index'),
	'Manage',
);

$this->menu=array(
	
);

Yii::app()->clientScript->registerScript('search', "

");
?>
<script>
$(document).ready(function(){
	

	$('#delete-office').click(function(e){

		
         if($("#emp-offices-grid").find("input:checked").length > 0)
         {

        	 if (confirm('Are you sure you want to delete these offices?')) {

         		 var ids = $.fn.yiiGridView.getChecked("emp-offices-grid", "selectedIds");
             	
       			  $.ajax({
       		            url: '<?php echo $this->createUrl('employerOffices/delete',array('id'=>$insmodel->id)) ?>',
       		            type: 'POST',
       		            data: {ids : ids}, 	    
       		            success: function(data) {
                                $.fn.yiiGridView.update('emp-offices-grid');
       			            //alert(data);
       		            },
                            error: function(data) {		
                                alert('err');
                            }
       		         });
        	 } 
        	
         }
         else
         {
            alert('Please select at least one item');
         }
	});
	
});

</script>
<h1>Offices</h1>
<div class="Admin__content__inner">

<div class="AdminFilters">
<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
 <?php //$this->renderPartial('_search',array(
// 	'model'=>$model,
// )); ?>
</div>
</div>
<div class="pull-right">
   <?php echo CHtml::button('Create', array("class" => "btn btn-primary", "onclick" => "window.location='" . Yii::app()->createUrl('employerOffices/create',array('id'=>$insmodel->id)) . "'"));?>
    <?php echo CHtml::button('Delete',array("class"=>"btn btn-primary","style"=>"margin-right:10px;margin-left:10px",'id'=>'delete-office'));?>
</div>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'emp-offices-grid',
    'itemsCssClass' => 'table',
    'summaryText' => '',
    //'cssFile'=>Yii::app()->baseUrl . '/themes/Dev/css/style.css',
    'enableSorting' => true,
    'ajaxUpdate' => true,
    'pager' => array(
        //'cssFile' => Yii::app()->baseUrl . '/themes/Dev/css/style.css',
        'header'=>'',
        'firstPageLabel' => '<<',
        'lastPageLabel' => '>>',
        'nextPageLabel' => '>>',
        'prevPageLabel' => '<<'),
    'ajaxUpdate' => false,
	'dataProvider'=>$model->searchByID($insmodel->id),
	'columns'=>array(
// 		'id',
	    array(
	        // 'name' => 'check',
	        'id' => 'selectedIds',
	        'value' => '$data->id',
	        'class' => 'CCheckBoxColumn',
	        'selectableRows'  => 2,
	        // 'checked'=>'Yii::app()->user->getState($data->id)',
	        // 'checkBoxHtmlOptions' => array('name' => 'idList[]'),
	        
	),
	    array(
	        'header' => 'Address',
	        'value' => '$data->address_id > 0 ? (CHtml::link("<i class=\"fa fa-pencil\"></i> ".$data->getAddress(),Yii::app()->createUrl("employerOffices/edit",array("id"=>$data->id)),array("class"=>"addr-edit-button","id"=>"office-edit"))) : ""',
	        'type' => 'raw'
	    ),
		'status'
	),
)); ?>
</div>