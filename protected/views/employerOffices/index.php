<?php
/* @var $this InstitutionOfficesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Employer Offices',
);

$this->menu=array(
	array('label'=>'Create Offices', 'url'=>array('create')),
	array('label'=>'ManageOffices', 'url'=>array('admin')),
);
?>

<h1>Institution Offices</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
