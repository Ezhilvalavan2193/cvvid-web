<?php
Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'CVVId.com',
	'theme'=>'Dev',
	// preloading 'log' component
	'preload'=>array('log'),
		'aliases' => array(
				//	'bootstrap' => realpath(__DIR__ . '/../extensions/bootstrap'), // change this if necessary
				'booster' => realpath(__DIR__ . '/../extensions/booster'),
		),
	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.extensions.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		 
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'admin',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
			
		),
		
	),

	// application components
	'components'=>array(
			'bootstrap'=>array(
					'class'=>'bootstrap.components.Bootstrap',
			),
		'user'=>array(
			//'loginUrl'=>array('admin'),
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),
	    'swiftMailer' => array(
	        'class' => 'ext.swiftMailer.SwiftMailer',
	    ),
		// uncomment the following to enable URLs in path-format
		
		'urlManager'=>array(
			'urlFormat'=>'path',
			 'showScriptName'=>false,
    		// 'caseSensitive'=>false,   
			'rules'=>array(
				 'users/<slug:[a-zA-Z0-9-]+>/edit/'=>'users/edit',
			    '/<slug:[a-zA-Z0-9-]+>/'=>'users/profile/',
			    'users/account/<slug:[a-zA-Z0-9-]+>/'=>'users/userAccountInfo',
			    'users/<slug:[a-zA-Z0-9-]+>/business-card/'=>'users/viewBCard',
			    'users/<slug:[a-zA-Z0-9-]+>/pdf/'=>'users/resume',
			    
			    
			    'employers/<slug:[a-zA-Z0-9-]+>/edit/'=>'employers/edit',
			    'employer/<slug:[a-zA-Z0-9-]+>/'=>'employers/employer',
			    'employers/account/<slug:[a-zA-Z0-9-]+>/'=>'employers/editDetails',
                            
                            
			    'employers/offices/<id:\d+>'=>'employerOffices/admin',
                            
                            
                            
                            			    
			    '<slug:[a-zA-Z0-9-]+>/jobs/add'=>'employers/jobsadd',
			    '<slug:[a-zA-Z0-9-]+>/jobs/<id:\d+>/edit'=>'employers/jobsedit',
			    'job/<title>-<id:\d+>'=>'jobs/viewJob',
                            
//                            job application
                            
                            'applications/<title>-<id:\d+>' =>'jobs/applications',
                            'shortlists/<title>-<id:\d+>' =>'jobs/shortlists',
                            'questions/<title>-<id:\d+>' =>'jobs/questions',
                            'interviewees/<title>-<id:\d+>' =>'jobs/interviews',
                            'answers/<title>-<id:\d+>' =>'jobs/answers',
			    
			    'site/employers/faqs'=>"site/employerfaqs",
			    'site/cv-video-showcase'=>"site/videoshowcase",
			    'site/about-us'=>"site/about",
			    'site/top-tips'=>"site/toptips",
			    'site/how-to'=>"site/howto",
			    'site/worry-list'=>"site/worrylist",
			    'site/job-search'=>"site/jobsearch",
			    'site/contact-us'=>"site/contact",
			   
				//'joblist/<title>'=>"site/searchJobs",
				'joblist/jobs-for-<title:.*?>-in-<loc:.*?>/'=>"site/searchJobs",
			    
				 'site/jobs-for-<category:[a-zA-Z0-9-]+>/'=>'site/searchCategory',
			    'site/jobs-in-<location:[a-zA-Z0-9-]+>/'=>'site/searchLocation',
			    'site/jobs-by-<company:[a-zA-Z0-9-]+>/'=>'site/searchCompany',
			    'site/job-for-<skills:[a-zA-Z0-9-]+>/'=>'site/searchSkills',
				
				//'<controller:\w+>/' => '<controller>/login',
                            '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                            '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                            '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
		
			),
		),
		

		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),
			
		'ePdf' => array(
					'class'         => 'ext.yii-pdf.EYiiPdf',
					'params'        => array(
							'mpdf'     => array(
									'librarySourcePath' => 'application.extensions.vendors.mpdf.*',
									'constants'         => array(
											'_MPDF_TEMP_PATH' => Yii::getPathOfAlias('application.runtime'),
									),
									'class'=>'mpdf', // the literal class filename to be loaded from the vendors folder
									/*'defaultParams'     => array( // More info: http://mpdf1.com/manual/index.php?tid=184
									 'mode'              => '', //  This parameter specifies the mode of the new document.
											'format'            => 'A4', // format A4, A5, ...
											'default_font_size' => 0, // Sets the default document font size in points (pt)
											'default_font'      => '', // Sets the default font-family for the new document.
											'mgl'               => 15, // margin_left. Sets the page margins for the new document.
											'mgr'               => 15, // margin_right
											'mgt'               => 16, // margin_top
											'mgb'               => 16, // margin_bottom
											'mgh'               => 9, // margin_header
											'mgf'               => 9, // margin_footer
											'orientation'       => 'P', // landscape or portrait orientation
									)*/
							),
							'HTML2PDF' => array(
									'librarySourcePath' => 'application.extensions.vendors.html2pdf.*',
									'classFile'         => 'Html2Pdf.php', // For adding to Yii::$classMap
									/*'defaultParams'     => array( // More info: http://wiki.spipu.net/doku.php?id=html2pdf:en:v4:accueil
									 'orientation' => 'P', // landscape or portrait orientation
											'format'      => 'A4', // format A4, A5, ...
											'language'    => 'en', // language: fr, en, it ...
											'unicode'     => true, // TRUE means clustering the input text IS unicode (default = true)
											'encoding'    => 'UTF-8', // charset encoding; Default is UTF-8
											'marges'      => array(5, 5, 5, 8), // margins by default, in order (left, top, right, bottom)
									)*/
							)
					),
			),
			
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
//                'mailer' => [
//                    'class' => 'yii\swiftmailer\Mailer',
//                    'transport' => [
//                        'class' => 'Swift_SmtpTransport',
//                        'host' => 'localhost',
//                        'username' => 'username',
//                        'password' => 'password',
//                        'port' => '587',
//                        'encryption' => 'tls',
//                    ],
//                ],
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// live credentials
	 'PUBLIC_Key' => 'pk_live_RBc83ZHDhgFPIUnzhvekcycK',
	   'SECRET_Key' => 'sk_live_3pSs9F8vLgFDeBX9Ymj2mFg0',
		

		//cvvid test 
		//'PUBLIC_Key' => 'pk_test_gSoorv4bVNE2MbSeExAxmuip',
	 // 'SECRET_Key' => 'sk_test_U9STfe99exZnA0cpsKuROUPI',
		
		//our testing credentials
	     //'PUBLIC_Key' => 'pk_test_BgbyUBWQhgPsWwHHk42LacXL',
	    // 'SECRET_Key' => 'sk_test_tX9tPyKw9rFuNvP6XNgAl1cQ',

	    'TAX_PERCENT' => 20,
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
		'EMAIL_USERNAME'=>'mail@cvvid.com',
	    'EMAIL_PASSWORD'=>'Yu3c96fbgnv*LjUc',
	    'AWS_BUCKET' => 'cvvid-new',
		 'DOMAIN_NAME'=>'https://cvvid.com',
	),
);


?>