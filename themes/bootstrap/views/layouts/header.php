<div class="container">
    <div class="row">
        <div class="Header__left col-sm-3">
            <div class="Header__branding">
                <div class="Header__logo">
                    <a href="<?php echo $this->createAbsoluteUrl('//'); ?>">
                        <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/cv-vid-logo.png" alt="CV Vid"/>
                    </a>
                </div>
            </div><!-- .Header__branding -->
        </div>
        <div class="Header__right col-sm-9">
           <?php if (!Yii::app()->user->isGuest) { ?>

            <div class="Header__user">
                <div class="dropdown">
                    <?php 
                        if(Yii::app()->user->getState('role') == "candidate")
                        {    $url = $this->createUrl('users/edit',array('id'=>Yii::app()->user->getState('userid')));
                             $displayName = Yii::app()->user->getState('username'); 
                        }
                        else if( Yii::app()->user->getState('role') == "employer")
                        {
                            $empusrmodel = EmployerUsers::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid')));
                            $currentUser = Employers::model()->findByPk($empusrmodel->employer_id);
                            $url = $this->createUrl('employers/edit',array('id'=>$empusrmodel->employer_id));
                            $displayName = $currentUser->name." ( ". Yii::app()->user->getState('username')." )";
                        }
                        else if(Yii::app()->user->getState('role') == "institution" || Yii::app()->user->getState('role') == "institution_admin")
                        {
                            $insmodel = Institutions::model()->findByAttributes(array('admin_id'=>Yii::app()->user->getState('userid')));
                            $url = $this->createUrl('institutions/edit',array('id'=>$insmodel->id));
                            $displayName = Yii::app()->user->getState('username');
                        }
                        else if(Yii::app()->user->getState('role') == "admin")
                        {
                            $url = Yii::app()->createUrl('admin/dashboard');
                            $displayName = Yii::app()->user->getState('username');
                        }
                        else if(Yii::app()->user->getState('role') == "teacher")
                        {    
                           // $insusrmodel = InstitutionUsers::model()->findByAttributes(array('user_id'=>Yii::app()->user->getState('userid')));
                            $url = Yii::app()->createUrl('institutions/teacherdashboard',array('id'=>Yii::app()->user->getState('userid')));
                            $displayName = Yii::app()->user->getState('username');
                        }
                     ?>
                    <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="Header__user__detail Header__user__name"><?php echo $displayName; ?></div>
                        <div class="Header__user__detail Header__user__arrow"><span class="caret"></span></div>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <?php if(Yii::app()->user->getState('role') == "teacher"){ ?>
                        <li><a href="<?php echo $url ?>">View Dashboard</a></li>
                        <?php } else { ?>
                        <li><a href="<?php echo $url ?>">Edit Profile</a></li>
                        <?php } ?>
                       <li><a href="<?php echo Yii::app()->createUrl('site/logout', array('basic' => 1)) ?>">Logout</a></li>
                    </ul>
                </div>
            </div><!-- .Header__user -->

           <?php } else { ?>

            <div class="Header__buttons">
                <div class="dropdown">
                    <a class="btn btn-default" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div>Register <span class="caret"></span></div>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                       <li><a href="<?php echo Yii::app()->createUrl('site/candidatememberships') ?>">Candidate</a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('site/employermemberships') ?>">Employer</a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('site/registerinstitution') ?>">Institution/School</a></li>
                    </ul>
                </div>
                <a class="btn btn-default" href="<?php echo Yii::app()->createUrl('site/login') ?>">Login</a>
            </div>
            <?php } ?>
        </div>
    </div>
</div>