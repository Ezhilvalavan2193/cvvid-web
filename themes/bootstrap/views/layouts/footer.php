<div class="Footer__links">
    <ul class="list-inline">
        <li><a href="#" data-toggle="modal" data-target="#TermsConditionsModal" data-backdrop="static">Terms and Conditions</a></li>
        <li><a href="#" data-toggle="modal" data-target="#PrivacyPolicyModal">Privacy Policy</a></li>
        <li><a href="#" data-toggle="modal" data-target="#CookiePolicyModal">Cookie Policy</a></li>
    </ul>
</div>