<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <link rel='shortcut icon' type='image/x-icon' href='<?php echo Yii::app()->theme->baseUrl; ?>/images/favicon.ico' />
    <meta name="base-url" content="<?php echo $this->createAbsoluteUrl('//'); ?>"/>

    <title>CVVid</title>

    <!-- Custom styles for this template -->
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/app.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.plyr.io/2.0.18/plyr.css">
    <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/payment.js"></script>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <header id="site-header">
       <?php $this->beginContent('//layouts/header'); ?><?php $this->endContent(); ?>
    </header>
    <nav id="site-navigation">
        <?php $this->beginContent('//layouts/nav'); ?><?php $this->endContent(); ?>
    </nav>

    <div id="site-content">
          <?php echo $content; ?>
    </div>
    
    <div id="site-footer" class="clearfix">
       <?php $this->beginContent('//layouts/footer'); ?><?php $this->endContent(); ?>
    </div>
    
     <?php $this->beginContent('//layouts/terms-conditions'); ?><?php $this->endContent(); ?>
     <?php $this->beginContent('//layouts/privacy-policy'); ?><?php $this->endContent(); ?>
     <?php $this->beginContent('//layouts/cookie-policy'); ?><?php $this->endContent(); ?>
    
    <!--media mangager--> 
    <div class="MediaModal modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <form class="MediaAdmin">
                        <div class="MediaAdmin__filters">
                            <div class="form-inline">
                                <input type="text" name="search" class="form-control MediaAdmin__search" placeholder="Search...">
                                <div class="pull-right">
                                    <button type="button" class="MediaAdmin__add btn btn-primary btn-sm dz-clickable"><i class="fa fa-plus"></i> Add Media</button>
                                </div>
                            </div>
                        </div>
                        <div class="MediaAdmin__previews"></div>
                        <div class="MediaAdmin__items clearfix" id="result_media">
                            <?php
                            $cri = new CDbCriteria();
                            $cri->order = 'order_column DESC';
                            $cri->limit = 20;
                            $cri->offset = 0;
                            $model = Media::model()->findAll($cri);
                            foreach ($model as $media) {
                                ?>
                            <div class="MediaAdmin__item">
                                <div class="MediaAdmin__item__inner">
                                    <img src="<?php echo Yii::app()->baseUrl ?>/images/media/<?php echo $media['id'] ?>/conversions/thumb.jpg" alt="<?php echo $media['name'] ?>" class="img-responsive img-thumbnail">
                                    <button class="MediaAdmin__item__delete" id="<?php echo $media['id'] ?>" type="button"><i class="fa fa-close"></i></button>
                                </div>
                            </div>
                             <?php } ?>
                        </div>
                        <div class="MediaAdmin__actions">
                            <input type="hidden" id="result_no" value="20">
                            <input type="hidden" id="total-row" value="<?php echo Media::model()->count(); ?>">
                            <button type="button" id="load-more" class="MediaAdmin__load-more btn btn-primary btn-sm pull-right">Load More</button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick='closepopup()'>Close</button>
                    <button type="button" id="insert-media" class="insert-media btn btn-primary">Insert Media</button>
                </div>
            </div>
        </div>
    </div>
    
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAd4jLN8LdoltMtZJfGS3C8ZTgPL7LDtdw&sensor=false&libraries=places"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bundle.js"></script>
    <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/generate-url.js"></script>
    <script src="https://cdn.plyr.io/2.0.18/plyr.js"></script>
    <script>plyr.setup();</script> 

    <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>

</body>
</html>
