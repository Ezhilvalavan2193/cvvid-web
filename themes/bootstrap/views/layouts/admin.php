<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <link rel='shortcut icon' type='image/x-icon' href='<?php echo Yii::app()->theme->baseUrl; ?>/images/favicon.ico' />
    <meta name="base-url" content="<?php echo $this->createAbsoluteUrl('//'); ?>"/>

    <title>CVVid - Admin</title>

    <!-- Custom styles for this template -->
    
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/css/admin.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.plyr.io/2.0.18/plyr.css">
    <script src='//cdn.tinymce.com/4/tinymce.min.js'></script>
    <script type="text/javascript">
        tinymce.init({
            selector: 'textarea',
            plugins: 'pagebreak code link',
            pagebreak_split_block: true,
            min_height: 500,
            link_list: [
                {title: 'Home', value: 'http://www.cvvid.com'},
                {title: 'Login', value: 'http://www.cvvid.com/auth/login'},
                {title: 'About Us', value: 'http://www.cvvid.com/about-us'},
                {title: 'Worry List', value: 'http://www.cvvid.com/worry-list'},
                {title: 'How To', value: 'http://www.cvvid.com/how-to'},
                {title: 'Employers', value: 'http://www.cvvid.com/employers'},
                {title: 'Top Tips', value: 'http:///www.cvvid.com/top-tips'},
                {title: 'Success Stories', value: 'http://www.cvvid.com/success-stories'},
                {title: 'Contact Us', value: 'http:///www.cvvid.com/contact-us'},
                {title: 'Candidate Register', value: 'http://www.cvvid.com/candidate/register'},
                {title: 'Employer Register', value: 'http://www.cvvid.com/employer/register'}
            ],
            link_class_list: [
                {title: 'None', value: ''},
                {title: 'Yellow Text', value: 'yellow'},
                {title: 'Default', value: 'btn btn-default'},
                {title: 'Primary', value: 'btn btn-primary'}
            ]
        });
    </script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body id="Admin">

    <div id="Admin__header">
        <div class="Admin__header-left">
            <a href="#" class="admin-menu-toggle">
                <i class="fa fa-bars"></i>
            </a>
            <a href="<?php echo $this->createAbsoluteUrl('//').'/admin/dashboard'; ?>" class="admin-logo">
                <img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/cv-vid-logo.png" alt="CV Vid"/>
            </a>
        </div>
        <div class="Admin__header-right">
            <a href="<?php echo Yii::app()->createUrl('site/logout', array('basic' => 1)) ?>"><i class="fa fa-sign-out"></i> Log out</a>
        </div>
    </div>

    <div id="Admin__sidebar">
        <?php $this->beginContent('//layouts/sidebar'); ?><?php $this->endContent(); ?>
    </div>
    <div id="Admin__content">
        <div class="Admin__header"></div>
        <div class="Admin__content">
            <?php echo $content; ?>
        </div>
        <div class="Admin__footer"></div>
    </div>
    
    <!--media mangager--> 
    <div class="MediaModal modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <form class="MediaAdmin">
                        <div class="MediaAdmin__filters">
                            <div class="form-inline">
                                <input type="text" name="search" class="form-control MediaAdmin__search" placeholder="Search...">
                                <div class="pull-right">
                                    <input class="media-img-upload" type="file" style="display:none;"/>
                                    <button type="button" class="MediaAdmin__add btn btn-primary btn-sm dz-clickable"><i class="fa fa-plus"></i> Add Media</button>
                                </div>
                            </div>
                        </div>
                        <div class="MediaAdmin__previews" id="result_media1"></div>
                        <div class="MediaAdmin__items clearfix" id="result_media">
                            <?php
                            $cri = new CDbCriteria();
                            $cri->order = 'order_column DESC';
                            $cri->limit = 20;
                            $cri->offset = 0;
                            $model = Media::model()->findAll($cri);
                            foreach ($model as $media) {
                                ?>
                            <div class="MediaAdmin__item">
                                <div class="MediaAdmin__item__inner">
                                    <img src="<?php echo Yii::app()->baseUrl ?>/images/media/<?php echo $media['id'] ?>/conversions/thumb.jpg" alt="<?php echo $media['name'] ?>" class="img-responsive img-thumbnail">
                                    <button class="MediaAdmin__item__delete" id="<?php echo $media['id'] ?>" type="button"><i class="fa fa-close"></i></button>
                                </div>
                            </div>
                             <?php } ?>
                        </div>
                        <div class="MediaAdmin__actions">
                            <input type="hidden" id="result_no" value="20">
                            <input type="hidden" id="total-row" value="<?php echo Media::model()->count(); ?>">
                            <button type="button" id="load-more" class="MediaAdmin__load-more btn btn-primary btn-sm pull-right">Load More</button>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick='closepopup()'>Close</button>
                    <button type="button" id="insert-media" class="insert-media btn btn-primary">Insert Media</button>
                </div>
            </div>
        </div>
    </div>
    
    
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAd4jLN8LdoltMtZJfGS3C8ZTgPL7LDtdw&sensor=false&libraries=places"></script>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bundle.js"></script>
    <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
    <script src="<?php echo Yii::app()->theme->baseUrl; ?>/js/generate-url.js"></script>
    <script src="https://cdn.plyr.io/2.0.18/plyr.js"></script>
    <script>plyr.setup();</script> 
</body>
</html>