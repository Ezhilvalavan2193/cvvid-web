function setScreen() {

    var winH = $(window).height(),
            winW = $(window).width(),
            headerH = $('header').innerHeight();

 //   $('.banner-section').css({height: winH - headerH});

    var banDesc = $('.banner-desc').innerHeight();

    var totDesc = banDesc / 2;

    $('.banner-desc').css({marginTop: -(totDesc)});

}

$(function () {

    $('.setCover').each(function () {

        var imgUrl = $(this).find('.img-bg').attr('src');

        $(this).css('background-image', 'url(' + imgUrl + ')');

    });
//    opacity: 1; width: 2700px; left: -1080px;



//    $('.site-navigation .nav').find('a').bind('click', function (e) {
//        e.preventDefault(); // prevent hard jump, the default behavior
//
//        var target = $(this).attr("href"); // Set the target as variable
//
//        // perform animated scrolling by getting top-position of target-element and set it as scroll target
//        $('html, body').stop().animate({
//            scrollTop: $(target).offset().top
//        }, 2000, function () {
//            location.hash = target; //attach the hash (#jumptarget) to the pageurl
//        });
//
//        return false;
//    });

    $('.listing li').matchHeight();
    
    $('li.dropdown').hover(function () {
        // alert ('hai');
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function () {
        // alert ('hai');
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });
    
    $('.dropdown-btn').click(function () {
        $('.dropdown').toggleClass('open');
    });

    $(".navbar-toggle").click(function () {
        $('.navbar-collapse').toggleClass('in');
    });

    $(".close-banner").click(function () {
        
        location.reload();
//        var id = $(this).attr('id');
//        $('.' + id).removeClass('expand-banner');
//        $('.close-banner').hide();
//
//        if (id === 'grid-2')
//        {   
//            $('.grid-1').show();
//        }
//        else
//        {
//            $('.grid-2').show();
//            
//        }
        

    });

    $(".candidate-btn").click(function (e) {
        // alert('hai');
        e.preventDefault();
        $('.grid-1').addClass('expand-banner');
        $('.grid-2').hide();
        $('.close-banner').attr('id', 'grid-1');
        $('.close-banner').show();
        $('.candidate-wrapper').slideDown();
        $('.employer-wrapper').slideUp();
         $('.register-drop').hide();
        $('.reg-btn').show();
        $('.reg-btn').addClass('candidate-reg-btn');
        $('.contactus-section').show();
        if ($(window).innerWidth() >= 700) {
            $('html, body').animate({
                scrollTop: $(".why-cvvid").offset().top
            }, 3000);
            } else if ($(window).innerWidth() < 600) {
                
                $('html, body').animate({
                    scrollTop: $("#why-cvvid2").offset().top
                }, 3000);
            }
     
      
        
    });
    
        $('body').on('click', '.employer-reg-btn', function () {
       $('html, body').animate({
            scrollTop: $("#employer-membership").offset().top
        }, 3000);
    });
    
      $('body').on('click', '.candidate-reg-btn', function () {
       $('html, body').animate({
            scrollTop: $("#candidate-membership").offset().top
        }, 3000);
    });
    
      $('body').on('click', '.faqs-btn', function () {
       $('html, body').animate({
            scrollTop: $("#faqs").offset().top
        }, 3000);
    });
    
      $('body').on('click', '.top-tips-btn', function () {
       $('html, body').animate({
            scrollTop: $("#top-tips").offset().top
        }, 3000);
    });
    
  

    
    
    $(".employer-btn").click(function (e) {
        // alert('hai');
        e.preventDefault();
        $('.grid-2').addClass('expand-banner');
        $('.grid-1').hide();
        $('.close-banner').attr('id', 'grid-2');
        $('.close-banner').show();
        $('.candidate-wrapper').slideUp();
        $('.employer-wrapper').slideDown();
        $('.register-drop').hide();
        $('.reg-btn').show();
        $('.contactus-section').show();
        $('.reg-btn').addClass('employer-reg-btn');
         $('html, body').animate({
            scrollTop: $(".employer-wrapper").offset().top
        }, 3000);
        
          var swiper = new Swiper('.swiper-container', {
            //   autoHeight: true,
              autoHeight: true,
      spaceBetween: 0,
      slidesPerView: 'auto',
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
        renderBullet: function (index, className) {
          return '<span class="' + className + '">' + (index + 1) + '</span>';
        },
      },
    });
//        alert(documentWidth - 60);
//          $('#faqs .slick-track').attr('style', 'opacity: 1; width: '+wd+'; left: -'+width+';');
         // $('#faqs .PageSlideshow__slide').attr('style', 'width: auto;');
         // $('#faqs .PageSlideshow').find('.slick-active').attr('data-slick-index', 0).focus();
//        $("#faqs").load(" #faqs > *");
    });
    

    $(".popup-close").click(function () {
        $('#load-video-div').html('<div></div>');
        $('#video-popup').fadeOut();
    });
    
    $(".terms-close").click(function () {
//         alert('fff');
        $('#terms-popup').fadeOut();
    });
    $("#terms-condition").click(function () {
//         alert('fff');
        $('#terms-popup').fadeIn();
    });
    
    $(".privacy-close").click(function () {
//         alert('fff');
        $('#privacy-popup').fadeOut();
    });
    $("#privacy-policy").click(function () {
//         alert('fff');
        $('#privacy-popup').fadeIn();
    });
    $(".cookie-close").click(function () {
//         alert('fff');
        $('#cookie-popup').fadeOut();
    });
    
    $("#cookie-policy").click(function () {
//         alert('fff');
        $('#cookie-popup').fadeIn();
    });
    
   
    
    setScreen();
    $('.PageSlideshow__slides').slick();
     $('.PageSlideshow').find('.slick-active').attr('tabindex', 0).focus();

//     $('.services-slider').slick({
//      draggable: true,
//      arrows: false,
//      dots: true,
//      fade: true,
//      speed: 900,
//      autoplay: true,
//      infinite: true,
//      touchThreshold: 100
//    });

 $('.faqs-btn').removeClass('top-tips-btn');

});



$(window).resize(function () {

    setScreen();

});

//$(window).on('load', function() {
//
//     $('.PageSlideshow__slide').attr('style', 'width: 540px;');
//})

//$(window).load(function () {
//  // run code.
// 
//});
