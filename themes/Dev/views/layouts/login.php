<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <link rel='shortcut icon' type='image/x-icon' href='<?php echo Yii::app()->theme->baseUrl; ?>/images/favicon.ico' />
    <meta name="base-url" content="<?php echo $this->createAbsoluteUrl('//'); ?>"/>

    <title>CVVid - careers</title>

 
</head>

<body id="Admin">
	 <?php echo $content; ?>
</body>
</html>
