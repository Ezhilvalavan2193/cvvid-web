<div class="modal fade" id="TermsConditionsModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" onclick="closepopup()" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">USER AGREEMENT – CVVID.COM</h4>
            </div>
            <div class="modal-body">
               <?php
               $pages = Pages::model()->findByAttributes(array('title'=>'Terms & Conditions','careers'=>0));
               echo $pages->body;
               ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="closepopup()" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>