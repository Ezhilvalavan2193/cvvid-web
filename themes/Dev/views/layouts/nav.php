<div class="container">
    <div class="row">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <i class="fa fa-bars"></i>
        </button>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="<?php echo Yii::app()->createUrl('site/home') ?>">Home</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-hover="dropdown"
                       data-animations="fadeInUp" aria-haspopup="true" aria-expanded="false">
                        Job Seekers<i class="fa fa-caret-down font-dropdown" aria-hidden="true"></i>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                       <li><a href="<?php echo Yii::app()->createUrl('site/about') ?>">Why CVVID?</a></li>
                            <li><a href="<?php echo Yii::app()->createUrl('site/toptips') ?>">Top Tips</a></li>
                            <li><a href="<?php echo Yii::app()->createUrl('site/howto') ?>">How To</a></li>
                            <li><a href="<?php echo Yii::app()->createUrl('site/worrylist') ?>">Worry List</a></li>
                            <li><a href="<?php echo Yii::app()->createUrl('site/videoshowcase') ?>">CV Video Showcase</a></li>
                    </ul>
                </li>

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-hover="dropdown"
                       data-animations="fadeInUp" aria-haspopup="true" aria-expanded="false">
                        Employers<i class="fa fa-caret-down font-dropdown" aria-hidden="true"></i>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                        <li><a href="<?php echo Yii::app()->createUrl('site/employers') ?>">Why CVVID?</a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('site/employerfaqs') ?>">FAQs</a></li>
                        <li><a href="<?php echo Yii::app()->createUrl('site/videoshowcase') ?>">CV Video Showcase</a></li>
                    </ul>
                </li>
                <?php if(Yii::app()->user->getState('role') == "candidate" || Yii::app()->user->isGuest) {?>
                    <li><a href="<?php echo Yii::app()->createUrl('site/jobsearch') ?>">Search Jobs</a></li>
                <?php } ?>

                <?php if(Yii::app()->user->getState('role') == "employer") {?>
                        <li><a href="<?php echo Yii::app()->createUrl('site/candidatesearch') ?>">Search Candidates</a></li>
               <?php } ?>
               <li><a id="career1" href="https://cvvidcareers.co.uk">Careers</a></li>
               <li><a id="career2" href="<?php echo Yii::app()->createUrl('site/career') ?>">Careers</a></li>
                <li><a href="<?php echo Yii::app()->createUrl('site/contact') ?>">Contact Us</a></li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>
<script>
$(document).ready(function () {
       
    
        if ($(window).innerWidth() >= 600) {
            $('#career1').show();
            $('#career2').hide();
        } else {
            $('#career2').show();
            $('#career1').hide();
        }

        });
            </script>