<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="row">
            <!-- uncomment code for absolute positioning tweek see top comment in css -->
            <!-- <div class="absolute-wrapper"> </div> -->
            <!-- Menu -->
           

            <!-- Main Content -->
           
 <div class="side-menu">

                <nav class="navbar navbar-default" role="navigation">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <div class="brand-wrapper">
                            <!-- Hamburger -->
                            <button type="button" class="navbar-toggle">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>

                            <!-- Brand -->
                            <div class="brand-name-wrapper">
                                <a class="navbar-brand" href="<?php echo $this->createAbsoluteUrl('//'); ?>" class="logo"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/images/logo.png" alt="CVVID"></a>
                            </div>
                        </div>

                    </div>

                    <!-- Main Menu -->
                    <div class="side-menu-container">
                        <ul class="nav navbar-nav">
                         <li class="panel panel-default" id="dropdown" style="display:<?php echo Yii::app()->user->isGuest ? 'none' : '' ?>">
                                <a href="#" class="data-toggle" data-toggle="dropdown"><i class="fa fa-lock"></i>&nbsp; Site Admin <span class="caret"></span></a>
                                <!-- Dropdown level 1 -->
                                <div id="dropdown-lvl1" class="dropdown-lv panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="nav navbar-nav">
                                             <li><a href="<?php echo Yii::app()->createUrl('users/manageUsers') ?>">Manage Users</a></li>
                                             <li><a href="<?php echo Yii::app()->createUrl('pages/admin') ?>">Pages</a></li>
                                             <li><a href="<?php echo Yii::app()->createUrl('videos/admin') ?>">Videos</a></li>
                                            <li><a href="<?php echo Yii::app()->createUrl('slideshows/admin') ?>">Slideshows</a></li>
                                           <li><a href="<?php echo Yii::app()->createUrl('media/admin') ?>">Image Library</a></li>
                                            <li><a href="<?php echo Yii::app()->createUrl('industries/admin') ?>">Industry Sectors</a></li>
                                            <li><a href="<?php echo Yii::app()->createUrl('skillCategories/admin') ?>">Industries</a></li>
                                            <li><a href="<?php echo Yii::app()->createUrl('skills/admin') ?>">Career Paths</a></li>
                                        </ul>
                                    </div>
                                </div>

                            </li>
                            <li class="panel panel-default" id="dropdown" style="display:<?php echo Yii::app()->user->isGuest ? 'none' : '' ?>">
                                <a href="#" class="data-toggle" data-toggle="dropdown"><i class="fa fa-building"></i>&nbsp; Employers <span class="caret"></span></a>
                                <!-- Dropdown level 1 -->
                                <div id="dropdown-lvl1" class="dropdown-lv panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="nav navbar-nav">
                                            <li><a href="<?php echo Yii::app()->createUrl('employers/admin') ?>">Overview</a></li>
                                            <li><a href="<?php echo Yii::app()->createUrl('employerUsers/admin') ?>">Users</a></li>
                                            <li><a href="<?php echo Yii::app()->createUrl('jobs/admin') ?>">Jobs</a></li>
                                        </ul>
                                    </div>
                                </div>

                            </li>
                            <li class="panel panel-default" id="dropdown" style="display:<?php echo Yii::app()->user->isGuest ? 'none' : '' ?>">
                                <a href="#" class="data-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>&nbsp; Candidates <span class="caret"></span></a>
                                <!-- Dropdown level 1 -->
                                <div id="dropdown-lvl1" class="dropdown-lv panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="nav navbar-nav">
                                            <li><a href="<?php echo Yii::app()->createUrl('users/admin',array('trial'=>1)) ?>">Free Trial Users</a></li>
                                            <li><a href="<?php echo Yii::app()->createUrl('users/admin',array('paid'=>1)) ?>">Paid Members</a></li>
                                            <li><a href="<?php echo Yii::app()->createUrl('users/admin',array('basic'=>1)) ?>">Basic Members</a></li>
                                        </ul>
                                    </div>
                                </div>

                            </li>
                           
                            <li class="panel panel-default" id="dropdown" style="display:<?php echo Yii::app()->user->isGuest ? 'none' : '' ?>">
                                <a href="#" class="data-toggle" data-toggle="dropdown"><i class="fa fa-graduation-cap"></i>&nbsp; Institutions <span class="caret"></span></a>
                                <!-- Dropdown level 1 -->
                                <div id="dropdown-lvl1" class="dropdown-lv panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="nav navbar-nav">
                                            <li><a href="<?php echo Yii::app()->createUrl('institutions/overview') ?>">Overview</a></li>
                                            <li><a href="<?php echo Yii::app()->createUrl('institutions/admin') ?>">Admins</a></li>
                                            <li><a href="<?php echo Yii::app()->createUrl('institutions/teachers') ?>">Teachers</a></li>
                                            <li><a href="<?php echo Yii::app()->createUrl('institutions/tutorgroups') ?>">Tutor Groups</a></li>
                                            <li><a href="<?php echo Yii::app()->createUrl('institutions/students') ?>">Students</a></li>
                                        </ul>
                                    </div>
                                </div>

                            </li>
                           
                            
                            <!-- <li style="display:<?php echo Yii::app()->user->isGuest ? 'none' : '' ?>"><a href="#">Welcome <?php echo Yii::app()->user->getState('username') ?></a></li> -->
                            <li style="display:<?php echo Yii::app()->user->isGuest ? '' : 'none' ?>"><a href="#">Register</a></li>
                            <li style="display:<?php echo Yii::app()->user->isGuest ? '' : 'none' ?>"><a href="#">Login</a></li>
                            <?php if (!Yii::app()->user->isGuest) { ?>
                                <li><a href="<?php echo $this->createUrl('/site/logout') ?>"><i class="fa fa-sign-out"></i>&nbsp; Logout</a></li>
                            <?php } ?>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </nav>
            </div>
            
 <div class="container-fluid">
        <div class="side-body">

            <div id="sub-wrapper">
               <!-- <div id="colcover" style="display: <?php echo (isset($this->dashboard) && $this->dashboard > 0) ? "none" : ""?>">
                <?php   $edit = isset($this->edit) ? $this->edit : 0;
                        $userid = isset($this->userid) ? $this->userid : 0;
                        $institutionid = isset($this->institutionid) ? $this->institutionid : 0;
                        $agencyid = isset($this->agencyid) ? $this->agencyid : 0;
                        $this->renderPartial('//users/userCover',array('userid'=>$userid,'institutionid'=>$institutionid,'agencyid'=>$agencyid,'edit'=>$edit)); 
                  ?>
               </div>
                    <div class="banner-title">
                    </div>-->
                
                    <section id="content-section">
                        <div class="row"> 
                                
                                   <!-- <div class="top-nav clearfix">
                                        <ul class="fR">
                                            <?php
                                                $this->beginWidget('zii.widgets.CPortlet', array(
                                                    //'title'=>'Operations',
                                                ));
                                                $this->widget('zii.widgets.CMenu', array(
                                                'items'=>$this->menu,
                                                'htmlOptions'=>array('class'=>'operations'),
                                                ));
                                                $this->endWidget();
                                            ?>
                                        </ul>
                                    </div> --> 
            
                                         <?php echo $content; ?>
                               
                        </div>
                    </section>
            </div>  
  </div>
  </div>
</div>  
<?php $this->endContent(); ?>
