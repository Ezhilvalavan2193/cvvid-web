<div class="copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-3 text-left" id="foot-col-3" style="color: black;">
                            &copy; 2019 - All Rights Reserved
                        </div>
                        <div class="col-xs-4" id="foot-col-4">
                            <ul class="list-inline">
                             		<li><a target="_blank" href="https://twitter.com/cvvid" class="twitter" data-toggle="modal"><i class="fa fa-twitter-square" aria-hidden="true" style="font-size: x-large;"></i></a></li>
                                    <li><a target="_blank" href="https://www.facebook.com/cvvid" class="fb" data-toggle="modal"><i class="fa fa-facebook-square" aria-hidden="true" style="font-size: x-large;"></i></a></li>
                                    <li><a target="_blank" href="https://www.instagram.com/cvvid" class="instagram" data-toggle="modal"><i class="fa fa-instagram" aria-hidden="true" style="font-size: x-large;"></i></a></li>
                                     <li><a target="_blank" href="https://in.linkedin.com/company/cvvid" class="linkedin" data-toggle="modal"><i class="fa fa-linkedin-square" aria-hidden="true" style="font-size: x-large;"></i></a></li>
                                    <li><a target="_blank" href="https://www.youtube.com/channel/UCnDnjMXYMWv31Zb4iMXODVw/videos" class="youtube" data-toggle="modal"><i class="fa fa-youtube-square" aria-hidden="true" style="font-size: x-large;"></i></a></li>
								<li><a target="_blank" href="https://play.google.com/store/apps/details?id=com.cvvid" class="android" data-toggle="modal"><i class="fa fa-android" aria-hidden="true" style="font-size: x-large;"></i></a></li>
                                <li><a target="_blank" href="https://itunes.apple.com/us/app/cvvid-candidate/id672692950?ls=1&mt=8" class="apple" data-toggle="modal"><i class="fa fa-apple" aria-hidden="true" style="font-size: x-large;"></i></a></li>
                                  </ul>
                            </div>      
                                  <div class="col-xs-5"  id="foot-col-5">
                                 <ul class="list-inline">    
                                    <li><a href="#" class="terms-condition" data-toggle="modal">Terms and Conditions</a></li>
                                    <li><a href="#" class="privacy-policy" data-toggle="modal">Privacy Policy</a></li>
                                    <li><a href="#" class="cookie-policy" data-toggle="modal">Cookie Policy</a></li>
                                </ul>
                        </div>
                        <div class="col-xs-12 text-left" id="foot-col-12" style="color: black;">
                            &copy; 2019 - All Rights Reserved
                        </div>
                        <div class="col-xs-5"  id="foot-col-55">
                                 <ul class="list-inline">    
                                    <li><a href="#" class="terms-condition" data-toggle="modal">Terms and Conditions</a></li>
                                    <li><a href="#" class="privacy-policy" data-toggle="modal">Privacy Policy</a></li>
                                    <li><a href="#" class="cookie-policy" data-toggle="modal">Cookie Policy</a></li>
                                </ul>
                        </div>
                        <div class="col-xs-12" id="foot-col-21">
                            <ul class="list-inline">
                             		<li><a target="_blank" href="https://twitter.com/cvvid" class="twitter" data-toggle="modal"><i class="fa fa-twitter-square" aria-hidden="true" style="font-size: x-large;"></i></a></li>
                                    <li><a target="_blank" href="https://www.facebook.com/cvvid" class="fb" data-toggle="modal"><i class="fa fa-facebook-square" aria-hidden="true" style="font-size: x-large;"></i></a></li>
                                    <li><a target="_blank" href="https://www.instagram.com/cvvid" class="instagram" data-toggle="modal"><i class="fa fa-instagram" aria-hidden="true" style="font-size: x-large;"></i></a></li>
                                     <li><a target="_blank" href="https://in.linkedin.com/company/cvvid" class="linkedin" data-toggle="modal"><i class="fa fa-linkedin-square" aria-hidden="true" style="font-size: x-large;"></i></a></li>
                                    <li><a target="_blank" href="https://www.youtube.com/channel/UCnDnjMXYMWv31Zb4iMXODVw/videos" class="youtube" data-toggle="modal"><i class="fa fa-youtube-square" aria-hidden="true" style="font-size: x-large;"></i></a></li>
								<li><a target="_blank" href="https://play.google.com/store/apps/details?id=com.cvvid" class="android" data-toggle="modal"><i class="fa fa-android" aria-hidden="true" style="font-size: x-large;"></i></a></li>
                                <li><a target="_blank" href="https://itunes.apple.com/us/app/cvvid-candidate/id672692950?ls=1&mt=8" class="apple" data-toggle="modal"><i class="fa fa-apple" aria-hidden="true" style="font-size: x-large;"></i></a></li>
                                  </ul>
                            </div>      
                           
                    </div>
                </div>
            </div>

      
