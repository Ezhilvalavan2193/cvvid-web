<ul class="Admin__menu">
    <li>
        <a href="#"><i class="fa fa-lock"></i> Site Admin <i class="fa fa-chevron-down"></i></a>
        <ul>
            <li>
                <a href="<?php echo Yii::app()->createUrl('users/manageUsers') ?>">Master Users</a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->createUrl('pages/admin') ?>">Pages</a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->createUrl('videos/admin') ?>">Videos</a>
            </li>
            <li><a href="<?php echo Yii::app()->createUrl('slideshows/admin') ?>">Slideshows</a></li>
            <li><a href="<?php echo Yii::app()->createUrl('media/admin') ?>">Image Library</a></li>
             <li><a href="<?php echo Yii::app()->createUrl('industries/admin') ?>">Industry Sectors</a></li>
             <li><a href="<?php echo Yii::app()->createUrl('skillCategories/admin') ?>">Industries</a></li>
             <li><a href="<?php echo Yii::app()->createUrl('skills/admin') ?>">Career Paths</a></li>
        </ul>
    </li>

    <li>
        <a href="#"><i class="fa fa-user"></i> Candidates <i class="fa fa-chevron-down"></i></a>
        <ul>
             <li><a href="<?php echo Yii::app()->createUrl('users/admin',array('trial'=>1)) ?>">Free Trial Users</a></li>
            <li><a href="<?php echo Yii::app()->createUrl('users/admin',array('paid'=>1)) ?>">Paid Members</a></li>
            <li><a href="<?php echo Yii::app()->createUrl('users/admin',array('basic'=>1)) ?>">Basic Members</a></li>
        </ul>
    </li>

    <li>
        <a href="#"><i class="fa fa-building"></i> Employers <i class="fa fa-chevron-down"></i></a>
        <ul>
            <li><a href="<?php echo Yii::app()->createUrl('employers/admin') ?>">Overview</a></li>
            <li><a href="<?php echo Yii::app()->createUrl('employerUsers/admin') ?>">Users</a></li>
            <li><a href="<?php echo Yii::app()->createUrl('jobs/admin') ?>">Jobs</a></li>
            <li><a href="<?php echo Yii::app()->createUrl('employers/approval') ?>">Approval</a></li>
        </ul>
    </li>

    <!--<li>
        <a href="#"><i class="fa fa-graduation-cap"></i> Institutions <i class="fa fa-chevron-down"></i></a>
        <ul>
            <li><a href="<?php echo Yii::app()->createUrl('institutions/overview') ?>">Overview</a></li>
             <li><a href="<?php echo Yii::app()->createUrl('institutions/pending') ?>">Approve</a></li>
           
            <li><a href="<?php echo Yii::app()->createUrl('institutions/teachers') ?>">Teachers</a></li>
            <li><a href="<?php echo Yii::app()->createUrl('institutions/tutorgroups') ?>">Tutor Groups</a></li>
            <li><a href="<?php echo Yii::app()->createUrl('institutions/students') ?>">Students</a></li>
        </ul>
    </li>-->

	 <li>
        <a href="#"><i class="fa fa-graduation-cap"></i> Recruitment<i class="fa fa-chevron-down"></i></a>
        <ul>
            <li><a href="<?php echo Yii::app()->createUrl('agency/overview') ?>">Overview</a></li>
             <!-- <li><a href="<?php //echo Yii::app()->createUrl('agency/employers') ?>">Employers</a></li>
            <li><a href="<?php //echo Yii::app()->createUrl('agency/candidates') ?>">Candidates</a></li>
            <li><a href="<?php //echo Yii::app()->createUrl('agency/staff') ?>">Staff</a></li>
            <li><a href="<?php //echo Yii::app()->createUrl('agency/analytics') ?>">Analytics</a></li> -->
        </ul>
   	 </li> 
   	  	 <li>
        <a href="#"><i class="fa fa-graduation-cap"></i> Alumni<i class="fa fa-chevron-down"></i></a>
        <ul>
            <li><a href="<?php echo Yii::app()->createUrl('site/alumini') ?>">Alumni</a></li>           
        </ul>
   	 </li> 
	<li>
        <a href="#"><i class="fa fa-graduation-cap"></i>Digital Passport<i class="fa fa-chevron-down"></i></a>
        <ul>
            <li><a href="<?php echo Yii::app()->createUrl('site/digitalpassport') ?>">Digital Passport</a></li>           
        </ul>
   	 </li>
    <li>
        <a href="<?php echo $this->createUrl('/site/logout') ?>"><i class="fa fa-sign-out"></i> Log out</a>
    </li>

</ul>