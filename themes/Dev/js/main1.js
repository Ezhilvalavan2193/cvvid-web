function setScreen(){

    var winH = $(window).height(),

        winW = $(window).width(),

        headerH = $('header').innerHeight();

//       $('.banner-section').css({height:winH - headerH});
       
//        var banDesc = $('.banner-desc').innerHeight();

//    var totDesc = banDesc / 2;

//    $('.banner-desc').css({marginTop: -(totDesc)});

//   $(window).scroll(function(){
//
//        if($(window).scrollTop() >= 100){
//
//            $('header').addClass('sticky');
//            $('.header-logo > img').attr('src', './images/cv-vid-logo.png');
//
//        }else{
//
//            $('header').removeClass('sticky');
//            $('.header-logo > img').attr('src', './images/cvvid_logo_light_32@2x.png');
//
//        } 
//
//    });

}

$(function(){  
    
    $('.setCover').each(function(){

        var imgUrl = $(this).find('.img-bg').attr('src');

        $(this).css('background-image', 'url('+ imgUrl +')');

    });

     

$('.services_box').matchHeight();
    
    
    
        $('body').on('click', '.employer-reg-btn', function () {
       $('html, body').animate({
            scrollTop: $("#employer-membership").offset().top
        }, 3000);
    });
    
      $('body').on('click', '.candidate-reg-btn', function () {
       $('html, body').animate({
            scrollTop: $("#candidate-membership").offset().top
        }, 3000);
    });
    
      $('body').on('click', '.faqs-btn', function () {
       $('html, body').animate({
            scrollTop: $("#faqs").offset().top
        }, 3000);
    });
    
      $('body').on('click', '.top-tips-btn', function () {
       $('html, body').animate({
            scrollTop: $("#top-tips").offset().top
        }, 3000);
    });
    
  
     // $('.listing li').matchHeight();
      

      $(".popup-close").click(function () {
          $('#load-video-div').html('<div></div>');
          $('#video-popup').fadeOut();
      });
      
      $(".terms-close").click(function () {
//           alert('fff');
          $('#terms-popup').fadeOut();
      });
      $("#terms-condition").click(function () {
//           alert('fff');
          $('#terms-popup').fadeIn();
      });
      
      $(".privacy-close").click(function () {
//           alert('fff');
          $('#privacy-popup').fadeOut();
      });
      $("#privacy-policy").click(function () {
//           alert('fff');
          $('#privacy-popup').fadeIn();
      });
      $(".cookie-close").click(function () {
//           alert('fff');
          $('#cookie-popup').fadeOut();
      });
      
      $("#cookie-policy").click(function () {
//           alert('fff');
          $('#cookie-popup').fadeIn();
      });
    
     
      $(".close-banner").click(function () {
          
          location.reload();
//          var id = $(this).attr('id');
//          $('.' + id).removeClass('expand-banner');
//          $('.close-banner').hide();
  //
//          if (id === 'grid-2')
//          {   
//              $('.grid-1').show();
//          }
//          else
//          {
//              $('.grid-2').show();
//              
//          }
          

      });
    

      $(".institution-btn").click(function (e) {
     	
       e.preventDefault();
       $('.grid-1').addClass('expand-banner');
       $('.grid-2').hide();
       $('.close-banner').attr('id', 'grid-1');
       $('.close-banner').show();
       $('.institution-wrapper').show();
       $('.candidate-wrapper').slideDown();
       $('.employer-wrapper').slideUp();
       $('.register-drop').hide();
       $('.reg-btn').show();
       $('.reg-btn').addClass('candidate-reg-btn');
       
       
        $('html, body').animate({
           scrollTop: $(".institution-wrapper").offset().top
       }, 3000);
     
       
   });
      
      $(".school-btn-admin").click(function(){
          $('#school-admin').show();
          $('#college-admin').hide();
          $('#university-admin').hide();
          $('html, body').animate({
           scrollTop: $(".school-section").offset().top
       }, 3000);
      });

      $(".college-btn-admin").click(function(){
       $('#school-admin').hide();
       $('#college-admin').show();
       $('#university-admin').hide();
       $('html, body').animate({
        scrollTop: $(".college-section").offset().top
    }, 3000);
   });

   $(".university-btn-admin").click(function(){
       $('#school-admin').hide();
       $('#college-admin').hide();
       $('#university-admin').show();
       $('html, body').animate({
        scrollTop: $(".university-section").offset().top
    }, 3000);
   });
   
      
   $(".school-btn-pass").click(function(){
	    $('#school-pass').show();
	    $('#college-pass').hide();
	    $('#university-pass').hide();
	    $('html, body').animate({
	     scrollTop: $(".school-sec-pass").offset().top
	 }, 3000);
	});

	$(".college-btn-pass").click(function(){
	 $('#school-pass').hide();
	 $('#college-pass').show();
	 $('#university-pass').hide();
	 $('html, body').animate({
	  scrollTop: $(".college-sec-pass").offset().top
	}, 3000);
	});

	$(".university-btn-pass").click(function(){
	 $('#school-pass').hide();
	 $('#college-pass').hide();
	 $('#university-pass').show();
	 $('html, body').animate({
	  scrollTop: $(".university-sec-pass").offset().top
	}, 3000);
	});
   
      $(".school-btn").click(function() {
          $('#school').show();
          $('#college').hide();
          $('#university').hide();
          $('#icon-section1').hide();
            $('#icon-section2').hide();
          var swiper = new Swiper('.school-swiper', {
              pagination: {
                el: '.swiper-pagination',
                clickable: true,
                renderBullet: function (index, className) {
                  return '<span class="' + className + '"></span>';
                },
              },
            });
//$('. PageSlideshow__slides').slick();
          
           $('html, body').animate({
            scrollTop: $(".school-sec").offset().top
        }, 3000);
        if ($(window).innerWidth() >= 600) {
            $('#benifit-school-stu1').add();
        $('#benifit-school-stu2').remove();
        $('#benifit-school1').add();
        $('#benifit-school2').remove();
       
        } else if ($(window).innerWidth() < 600) {
        $('#benifit-school-stu2').add();
      
   
        $('#benifit-school-stu1').remove();
        $('#benifit-school2').add();
      
   
      $('#benifit-school1').remove();
}
      });
      
      $(".college-btn").click(function() {
          $('#school').hide();
          $('#college').show();
          $('#university').hide();
          $('#icon-section1').hide();
            $('#icon-section2').hide();
          var swiper = new Swiper('.college-swiper', {
              pagination: {
                el: '.swiper-pagination',
                clickable: true,
                renderBullet: function (index, className) {
                  return '<span class="' + className + '"></span>';
                },
              },
            });
          $('html, body').animate({
            scrollTop: $(".college-sec").offset().top
        }, 3000);
        if ($(window).innerWidth() >= 600) {
            $('#benifit-college-stu1').add();
        $('#benifit-college-stu2').remove();
        $('#benifit-college1').add();
        $('#benifit-college2').remove();
       } else if ($(window).innerWidth() < 600) {
        $('#benifit-college-stu2').add();
        $('#benifit-college-stu1').remove();
        $('#benifit-college2').add();
        $('#benifit-college1').remove();
}
      });
      
      $(".university-btn").click(function() {
          $('#school').hide();
          $('#college').hide();
          $('#university').show();
          $('#icon-section1').hide();
            $('#icon-section2').hide();
          var swiper = new Swiper('.univ-swiper', {
              pagination: {
                el: '.swiper-pagination',
                clickable: true,
                renderBullet: function (index, className) {
                  return '<span class="' + className + '"></span>';
                },
              },
            });
          $('html, body').animate({
            scrollTop: $(".university-sec").offset().top
        }, 3000);
        if ($(window).innerWidth() >= 600) {
            $('#benifit-university-stu1').add();
        $('#benifit-university-stu2').remove();
        $('#benifit-university1').add();
        $('#benifit-university2').remove();
       } else if ($(window).innerWidth() < 600) {
        $('#benifit-university-stu2').add();
        $('#benifit-university-stu1').remove();
        $('#benifit-university2').add();
        $('#benifit-university1').remove();
}
      });
      
    $(".employer-btn").click(function (e) {
        e.preventDefault();
        $('.grid-2').addClass('expand-banner');
        $('.grid-1').hide();
        $('.close-banner').attr('id', 'grid-2');
        $('.close-banner').show();
        $('.candidate-wrapper').slideUp();
        $('.employer-wrapper').slideDown();
        $('.register-drop').hide();
        $('.reg-btn').show();
        $('.reg-btn').addClass('employer-reg-btn');
         $('html, body').animate({
            scrollTop: $("#why-cvvid").offset().top
        }, 3000);
        
          var swiper = new Swiper('.swiper-container', {
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
        renderBullet: function (index, className) {
          return '<span class="' + className + '"></span>';
        },
      },
    });

    var swiper = new Swiper('.swipers', {
        // pagination: {
        //   el: '.swiper-pagination',
        //   type: 'fraction',
        // },
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
      });
          

          
         
          
//        alert(documentWidth - 60);
//          $('#faqs .slick-track').attr('style', 'opacity: 1; width: '+wd+'; left: -'+width+';');
         // $('#faqs .PageSlideshow__slide').attr('style', 'width: auto;');
         // $('#faqs .PageSlideshow').find('.slick-active').attr('data-slick-index', 0).focus();
//        $("#faqs").load(" #faqs > *");
    });
    

 //    $('.site-navigation .nav').find('a').bind('click', function(e) {
//        e.preventDefault(); // prevent hard jump, the default behavior
//
//        var target = $(this).attr("href"); // Set the target as variable
//
//        // perform animated scrolling by getting top-position of target-element and set it as scroll target
//        $('html, body').stop().animate({
//                        scrollTop: $(target).offset().top
//        }, 2000, function() {
//                        location.hash = target; //attach the hash (#jumptarget) to the pageurl
//        });
//
//        return false;
//    });
    
    $('li.dropdown').hover(function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
      }, function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
      });
    
    $("#register").click(function() {
        $('html, body').animate({
            scrollTop: $("#membership").offset().top
        }, 2000);
    });

$(".navbar-toggle").click(function () {
        $('.navbar-collapse').toggleClass('in');
    });
        
     $(".close-banner").click(function() {
         var id = $(this).attr('id');
         $('.'+id).removeClass('expand-banner');
         $('.close-banner').hide();
         
         if(id === 'grid-2')
             $('.grid-1').show();
         else
             $('.grid-2').show();
         
      }); 
      
    $("#owl-demo").owlCarousel({autoPlay:1e4,items:3,itemsDesktop:[1199,3],itemsDesktopSmall:[979,3]});
    
//    $('.site-navigation .nav').find('a').click(function(){
//        var $href = $(this).attr('href');
//        
//        var $anchor = $('#'+$href).offset();
//        
//        $('html, body').animate({
//           scrollTop: $anchor.top
//        }, 2000);
//        
//        return false;
//    });
//    


    
//     $(".institution-btn").click(function() {
//        $(this).addClass('expand-banner');
//        $('.grid-emp-2').hide();
//        $('.close-banner').attr('id', 'grid-ins-1');
//        $('.close-banner').show();
//    }); 
    
//     $(".grid-1").click(function() {
//        $(this).addClass('expand-banner');
//        $('.grid-2').hide();
//        $('.close-banner').attr('id', 'grid-ins1');
//        $('.close-banner').show();
//    }); 
      
     $(".grid-2").click(function() {
          $(this).addClass('expand-banner');
          $('.grid-1').hide();
           $('.close-banner').attr('id', 'grid-2');
           $('.close-banner').show();
      });   
     $(".listing > li").click(function() {
//         alert('fff');
         $('#video-popup').fadeIn();
     });
     
       $(".popup-close").click(function() {
//         alert('fff');
         $('#video-popup').fadeOut();
     });
    setScreen();
    $('.PageSlideshow__slides').slick();
//     $('.PageSlideshow').find('.slick-active').attr('tabindex', 0).focus();
    
//     $('.services-slider').slick({
//      draggable: true,
//      arrows: false,
//      dots: true,
//      fade: true,
//      speed: 900,
//      autoplay: true,
//      infinite: true,
//      touchThreshold: 100
//    });

});



$(window).resize(function(){

     setScreen();

});
