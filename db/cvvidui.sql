-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 24, 2018 at 09:40 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cvvidui`
--

-- --------------------------------------------------------

--
-- Table structure for table `cv16_activities`
--

CREATE TABLE `cv16_activities` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model_id` int(10) UNSIGNED DEFAULT NULL,
  `model_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` longtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_activities`
--

INSERT INTO `cv16_activities` (`id`, `user_id`, `type`, `model_id`, `model_type`, `subject`, `message`, `created_at`, `updated_at`) VALUES
(1, 3, 'message', 5, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: Welcome to CVVid', 'Testing new message', '2016-06-14 09:43:55', '2016-06-14 09:43:55'),
(2, 5, 'message', 5, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: Welcome to CVVid', 'Testing new message', '2016-06-14 09:43:55', '2016-06-14 09:43:55'),
(4, 5, 'message', 6, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: Welcome to CVVid', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam id pulvinar tellus. Aenean suscipit ligula in ante congue dapibus. Aenean vestibulum elementum dui vel posuere. Suspendisse non semper quam. Cras diam turpis, tempus vitae augue commodo, lobortis tristique dolor. Nam sodales condimentum pharetra. Fusce lobortis rhoncus nibh, ut aliquam est luctus ac. Vestibulum pharetra egestas lacinia. Sed ornare cursus rutrum.', '2016-07-04 10:12:46', '2016-07-04 10:12:46'),
(5, 3, 'message', 6, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: Welcome to CVVid', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam id pulvinar tellus. Aenean suscipit ligula in ante congue dapibus. Aenean vestibulum elementum dui vel posuere. Suspendisse non semper quam. Cras diam turpis, tempus vitae augue commodo, lobortis tristique dolor. Nam sodales condimentum pharetra. Fusce lobortis rhoncus nibh, ut aliquam est luctus ac. Vestibulum pharetra egestas lacinia. Sed ornare cursus rutrum.', '2016-07-04 10:12:46', '2016-07-04 10:12:46'),
(7, 5, 'message', 7, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: Welcome to CVVid', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec neque urna, efficitur at lorem quis, fringilla rutrum sapien. In at rutrum nisi, quis pulvinar ex. Nullam a efficitur lacus. Suspendisse eget fermentum lorem, in bibendum enim. Mauris scelerisque dapibus sapien, quis pellentesque urna. Aenean nisl est, viverra eu semper eu, cursus a massa. Aenean gravida enim quis ligula maximus faucibus. Integer ultrices sem eu fringilla tempus. Phasellus blandit sed sem euismod rhoncus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vestibulum laoreet, dui sed bibendum facilisis, metus mauris commodo urna, vitae placerat leo ex ac purus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae ligula lorem. Nullam et porttitor lorem, nec vestibulum justo. Praesent a lacinia dolor.\r\n\r\nIn placerat, ipsum quis posuere commodo, enim dui lacinia lacus, at dignissim sapien dolor nec libero. Nunc dictum, urna sed blandit suscipit, tellus ligula vestibulum mi, vitae suscipit sem massa id nibh. Duis eu lacus hendrerit, hendrerit tortor ac, cursus ante. Vestibulum aliquet at odio eget tincidunt. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas maximus sem in sapien aliquam ullamcorper. Suspendisse ex magna, tincidunt sed varius eu, accumsan eu odio. Nulla facilisi. Proin vestibulum semper turpis, non tincidunt lorem. Fusce urna ligula, sagittis a quam sed, dignissim rutrum ipsum. Phasellus nec quam in tortor ullamcorper sollicitudin. Etiam id ligula aliquet arcu ultricies laoreet. Nulla facilisi. Pellentesque ac risus vel augue sodales suscipit.', '2016-07-08 14:48:35', '2016-07-08 14:48:35'),
(8, 3, 'message', 7, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: Welcome to CVVid', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec neque urna, efficitur at lorem quis, fringilla rutrum sapien. In at rutrum nisi, quis pulvinar ex. Nullam a efficitur lacus. Suspendisse eget fermentum lorem, in bibendum enim. Mauris scelerisque dapibus sapien, quis pellentesque urna. Aenean nisl est, viverra eu semper eu, cursus a massa. Aenean gravida enim quis ligula maximus faucibus. Integer ultrices sem eu fringilla tempus. Phasellus blandit sed sem euismod rhoncus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vestibulum laoreet, dui sed bibendum facilisis, metus mauris commodo urna, vitae placerat leo ex ac purus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae ligula lorem. Nullam et porttitor lorem, nec vestibulum justo. Praesent a lacinia dolor.\r\n\r\nIn placerat, ipsum quis posuere commodo, enim dui lacinia lacus, at dignissim sapien dolor nec libero. Nunc dictum, urna sed blandit suscipit, tellus ligula vestibulum mi, vitae suscipit sem massa id nibh. Duis eu lacus hendrerit, hendrerit tortor ac, cursus ante. Vestibulum aliquet at odio eget tincidunt. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas maximus sem in sapien aliquam ullamcorper. Suspendisse ex magna, tincidunt sed varius eu, accumsan eu odio. Nulla facilisi. Proin vestibulum semper turpis, non tincidunt lorem. Fusce urna ligula, sagittis a quam sed, dignissim rutrum ipsum. Phasellus nec quam in tortor ullamcorper sollicitudin. Etiam id ligula aliquet arcu ultricies laoreet. Nulla facilisi. Pellentesque ac risus vel augue sodales suscipit.', '2016-07-08 14:48:35', '2016-07-08 14:48:35'),
(10, 5, 'message', 8, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: Welcome to CVVid', 'Praesent ultrices, felis ut volutpat consequat, mi erat tincidunt mi, nec ornare metus felis sed ex. Suspendisse ac turpis nulla. Curabitur cursus pretium nulla, vel scelerisque leo aliquam id. Integer et nibh eu nisl pellentesque egestas. Vivamus vitae lectus egestas, convallis ante at, iaculis dui. Donec pulvinar sagittis augue, id aliquam tellus finibus vel. Cras facilisis urna ut pharetra efficitur. Nunc ultrices rutrum massa, in varius est suscipit et. Donec pharetra lorem et est gravida, gravida euismod ex efficitur. Proin porttitor hendrerit ante, vel sagittis enim venenatis eu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque risus nulla, tincidunt non auctor vitae, ultrices sit amet eros. Curabitur porta facilisis suscipit.\r\n\r\nSed mattis, felis non maximus accumsan, felis ante lacinia diam, et tincidunt nulla tellus quis turpis. Aenean eget faucibus est. Proin in dapibus lorem. Nullam in est convallis, finibus dolor sed, eleifend nulla. Donec dui ipsum, varius sit amet justo non, luctus hendrerit nisl. Sed dictum tempus ornare. Sed molestie nisl nec commodo sagittis. Proin non mauris eu libero molestie laoreet. Integer in libero magna.', '2016-07-08 14:51:08', '2016-07-08 14:51:08'),
(11, 3, 'message', 8, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: Welcome to CVVid', 'Praesent ultrices, felis ut volutpat consequat, mi erat tincidunt mi, nec ornare metus felis sed ex. Suspendisse ac turpis nulla. Curabitur cursus pretium nulla, vel scelerisque leo aliquam id. Integer et nibh eu nisl pellentesque egestas. Vivamus vitae lectus egestas, convallis ante at, iaculis dui. Donec pulvinar sagittis augue, id aliquam tellus finibus vel. Cras facilisis urna ut pharetra efficitur. Nunc ultrices rutrum massa, in varius est suscipit et. Donec pharetra lorem et est gravida, gravida euismod ex efficitur. Proin porttitor hendrerit ante, vel sagittis enim venenatis eu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque risus nulla, tincidunt non auctor vitae, ultrices sit amet eros. Curabitur porta facilisis suscipit.\r\n\r\nSed mattis, felis non maximus accumsan, felis ante lacinia diam, et tincidunt nulla tellus quis turpis. Aenean eget faucibus est. Proin in dapibus lorem. Nullam in est convallis, finibus dolor sed, eleifend nulla. Donec dui ipsum, varius sit amet justo non, luctus hendrerit nisl. Sed dictum tempus ornare. Sed molestie nisl nec commodo sagittis. Proin non mauris eu libero molestie laoreet. Integer in libero magna.', '2016-07-08 14:51:08', '2016-07-08 14:51:08'),
(13, 5, 'message', 9, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: Welcome to CVVid', 'Interested in a job? Get in touch', '2016-08-09 12:39:14', '2016-08-09 12:39:14'),
(14, 3, 'message', 9, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: Welcome to CVVid', 'Interested in a job? Get in touch', '2016-08-09 12:39:14', '2016-08-09 12:39:14'),
(15, 3, 'message', 10, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: Welcome to CVVid', 'No thanks', '2016-08-09 12:40:09', '2016-08-09 12:40:09'),
(16, 5, 'message', 10, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: Welcome to CVVid', 'No thanks', '2016-08-09 12:40:09', '2016-08-09 12:40:09'),
(18, 5, 'message', 11, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: Welcome to CVVid', 'We have a job you may be interested in.', '2016-08-30 14:26:54', '2016-08-30 14:26:54'),
(19, 3, 'message', 11, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: Welcome to CVVid', 'We have a job you may be interested in.', '2016-08-30 14:26:54', '2016-08-30 14:26:54'),
(20, 3, 'message', 12, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: Welcome to CVVid', 'Thank you for your message, i am interested in an interview.', '2016-08-30 14:29:28', '2016-08-30 14:29:28'),
(21, 5, 'message', 12, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: Welcome to CVVid', 'Thank you for your message, i am interested in an interview.', '2016-08-30 14:29:28', '2016-08-30 14:29:28'),
(23, 5, 'message', 13, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: Welcome to CVVid', 'Thats great we will be in touch tomorrow to arrange an interview date.', '2016-08-30 14:30:18', '2016-08-30 14:30:18'),
(24, 3, 'message', 13, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: Welcome to CVVid', 'Thats great we will be in touch tomorrow to arrange an interview date.', '2016-08-30 14:30:18', '2016-08-30 14:30:18'),
(26, 5, 'message', 14, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: Welcome to CVVid', 'Your interview has been scheduled for a week on monday at 2:30 PM, please confirm that this time is suitable.', '2016-08-30 14:36:39', '2016-08-30 14:36:39'),
(27, 3, 'message', 14, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: Welcome to CVVid', 'Your interview has been scheduled for a week on monday at 2:30 PM, please confirm that this time is suitable.', '2016-08-30 14:36:39', '2016-08-30 14:36:39'),
(28, 3, 'message', 15, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: Welcome to CVVid', 'Thank you for your message, i am interested in an interview.', '2016-08-30 14:39:05', '2016-08-30 14:39:05'),
(29, 5, 'message', 15, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: Welcome to CVVid', 'Thank you for your message, i am interested in an interview.', '2016-08-30 14:39:05', '2016-08-30 14:39:05'),
(30, 32, 'message', 16, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: Welcome to CVVid', 'Hi\nThank you for applying for the role. We will be in touch soon', '2017-01-26 16:58:18', '2017-01-26 16:58:18'),
(31, 5, 'message', 16, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: Welcome to CVVid', 'Hi\nThank you for applying for the role. We will be in touch soon', '2017-01-26 16:58:18', '2017-01-26 16:58:18'),
(32, 3, 'message', 16, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: Welcome to CVVid', 'Hi\nThank you for applying for the role. We will be in touch soon', '2017-01-26 16:58:18', '2017-01-26 16:58:18'),
(33, 40, 'message', 17, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: Welcome to CVVid', 'Thank you. I hope you like the video I have uploaded', '2017-01-26 16:59:29', '2017-01-26 16:59:29'),
(34, 5, 'message', 17, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: Welcome to CVVid', 'Thank you. I hope you like the video I have uploaded', '2017-01-26 16:59:29', '2017-01-26 16:59:29'),
(35, 3, 'message', 17, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: Welcome to CVVid', 'Thank you. I hope you like the video I have uploaded', '2017-01-26 16:59:29', '2017-01-26 16:59:29'),
(36, 34, 'message', 18, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: Welcome to CVVid', 'Please resubmit your application once you have filled out your profile and uploaded a video CV', '2017-02-15 12:20:39', '2017-02-15 12:20:39'),
(37, 5, 'message', 18, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: Welcome to CVVid', 'Please resubmit your application once you have filled out your profile and uploaded a video CV', '2017-02-15 12:20:39', '2017-02-15 12:20:39'),
(38, 3, 'message', 18, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: Welcome to CVVid', 'Please resubmit your application once you have filled out your profile and uploaded a video CV', '2017-02-15 12:20:39', '2017-02-15 12:20:39'),
(39, 34, 'message', 19, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: Welcome to CVVid', 'Thank you for your application. Unfortunately you have not been successful on this occasion. A proper filled out CV along with a video CV is required to be considered for shortlisting', '2017-02-24 17:15:44', '2017-02-24 17:15:44'),
(40, 5, 'message', 19, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: Welcome to CVVid', 'Thank you for your application. Unfortunately you have not been successful on this occasion. A proper filled out CV along with a video CV is required to be considered for shortlisting', '2017-02-24 17:15:44', '2017-02-24 17:15:44'),
(41, 3, 'message', 19, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: Welcome to CVVid', 'Thank you for your application. Unfortunately you have not been successful on this occasion. A proper filled out CV along with a video CV is required to be considered for shortlisting', '2017-02-24 17:15:44', '2017-02-24 17:15:44'),
(42, 33, 'message', 20, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: Welcome to CVVid', 'Sorry - you\'re over-qualified for the role!', '2017-03-08 14:14:33', '2017-03-08 14:14:33'),
(43, 5, 'message', 20, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: Welcome to CVVid', 'Sorry - you\'re over-qualified for the role!', '2017-03-08 14:14:33', '2017-03-08 14:14:33'),
(44, 3, 'message', 20, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: Welcome to CVVid', 'Sorry - you\'re over-qualified for the role!', '2017-03-08 14:14:33', '2017-03-08 14:14:33'),
(45, 34, 'message', 21, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: More info required', 'Thank you for applying. Can you provide more information about your education and experience please', '2017-05-25 06:55:23', '2017-05-25 06:55:23'),
(46, 91, 'message', 21, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: More info required', 'Thank you for applying. Can you provide more information about your education and experience please', '2017-05-25 06:55:23', '2017-05-25 06:55:23'),
(47, 121, 'message', 22, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: test', 'ji jp', '2017-07-28 07:57:20', '2017-07-28 07:57:20'),
(48, 119, 'message', 22, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: test', 'ji jp', '2017-07-28 07:57:20', '2017-07-28 07:57:20'),
(49, 119, 'message', 23, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: test', 'ok', '2017-07-28 07:59:09', '2017-07-28 07:59:09'),
(50, 121, 'message', 23, 'App\\Messaging\\ConversationMessage', 'You received a message in conversation: test', 'ok', '2017-07-28 07:59:09', '2017-07-28 07:59:09'),
(51, 14, 'message', 28, 'ConversationMessage', 'You sent a message in conversation: testqaz', 'testqaztestqaztestqaztestqaztestqaztestqaztestqaztestqaztestqaz', '2018-01-17 05:12:10', '2018-01-17 05:12:10'),
(52, 3, 'message', 28, 'ConversationMessage', 'You received a message in conversation: testqaz', 'testqaztestqaztestqaztestqaztestqaztestqaztestqaztestqaztestqaz', '2018-01-17 05:12:10', '2018-01-17 05:12:10'),
(53, 33, 'message', 29, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: Your Application has been rejected', 'testsadd', '2018-01-17 06:34:43', '2018-01-17 06:34:43'),
(54, 3, 'message', 29, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: Your Application has been rejected', 'testsadd', '2018-01-17 06:34:43', '2018-01-17 06:34:43'),
(55, 33, 'message', 30, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: Your Application has been rejected', 'test321', '2018-01-17 06:36:27', '2018-01-17 06:36:27'),
(56, 3, 'message', 30, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: Your Application has been rejected', 'test321', '2018-01-17 06:36:27', '2018-01-17 06:36:27'),
(57, 33, 'message', 31, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: Your Application has been rejected', 'aaagag', '2018-01-17 06:37:54', '2018-01-17 06:37:54'),
(58, 3, 'message', 31, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: Your Application has been rejected', 'aaagag', '2018-01-17 06:37:54', '2018-01-17 06:37:54'),
(59, 33, 'message', 32, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: Your Application has been rejected', 'fafaf', '2018-01-17 06:38:39', '2018-01-17 06:38:39'),
(60, 3, 'message', 32, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: Your Application has been rejected', 'fafaf', '2018-01-17 06:38:39', '2018-01-17 06:38:39'),
(61, 33, 'message', 33, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: Your Application has been rejected', 'asdasdasd', '2018-01-17 06:39:40', '2018-01-17 06:39:40'),
(62, 3, 'message', 33, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: Your Application has been rejected', 'asdasdasd', '2018-01-17 06:39:40', '2018-01-17 06:39:40'),
(63, 33, 'message', 34, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: Your Application has been rejected', 'asdafsaf', '2018-01-17 06:41:14', '2018-01-17 06:41:14'),
(64, 3, 'message', 34, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: Your Application has been rejected', 'asdafsaf', '2018-01-17 06:41:14', '2018-01-17 06:41:14'),
(65, 33, 'message', 35, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: Your Application has been rejected', 'asdadaf', '2018-01-17 06:46:02', '2018-01-17 06:46:02'),
(66, 3, 'message', 35, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: Your Application has been rejected', 'asdadaf', '2018-01-17 06:46:02', '2018-01-17 06:46:02'),
(67, 33, 'message', 36, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: Your Application has been rejected', 'asfgagag', '2018-01-17 06:47:54', '2018-01-17 06:47:54'),
(68, 3, 'message', 36, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: Your Application has been rejected', 'asfgagag', '2018-01-17 06:47:54', '2018-01-17 06:47:54'),
(69, 33, 'message', 37, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: Your Application has been rejected', 'asdaf', '2018-01-17 06:48:16', '2018-01-17 06:48:16'),
(70, 3, 'message', 37, 'App\\Messaging\\ConversationMessage', 'You sent a message in conversation: Your Application has been rejected', 'asdaf', '2018-01-17 06:48:16', '2018-01-17 06:48:16');

-- --------------------------------------------------------

--
-- Table structure for table `cv16_addresses`
--

CREATE TABLE `cv16_addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `model_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `street_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `town` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postcode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `county` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `latitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_addresses`
--

INSERT INTO `cv16_addresses` (`id`, `model_id`, `model_type`, `street_number`, `address`, `town`, `postcode`, `county`, `country`, `location`, `latitude`, `longitude`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 'Admin', NULL, 'Manchester Road', 'Altham', 'BB5 5TX', 'Lancashire', 'United Kingdom', NULL, '53.7769865', '-2.3693035999999665', '2017-02-24 18:15:43', '2017-12-30 01:56:30', NULL),
(2, 15, 'App\\Education\\Institution', NULL, '148 Laburnum Road', 'Blackburn', 'BB1 5EQ', 'Blackburn with Darwen', 'United Kingdom', NULL, '53.7623612', '-2.4698825000000397', '2017-04-11 08:37:00', '2017-04-11 08:37:00', NULL),
(3, 99, 'App\\Users\\Candidate', NULL, '7 Christian Close', 'Harlington', 'LU5 6LU', 'Central Bedfordshire', 'United Kingdom', NULL, '51.9609994', '-0.49428450000004887', '2017-04-27 19:13:16', '2017-11-24 06:34:44', NULL),
(4, 115, 'App\\Users\\Candidate', NULL, 'Street Address', 'Page', '86040', 'Coconino County', 'United States', NULL, '36.944402', '-111.42162109999998', '2017-06-15 11:23:32', '2017-06-15 11:23:32', NULL),
(5, 116, 'App\\Users\\Candidate', NULL, 'test', 'Waterford', '06385', 'New London County', 'United States', NULL, '41.3432197', '-72.11374799999999', '2017-06-15 11:39:09', '2017-06-15 11:39:09', NULL),
(6, 117, 'App\\Users\\Candidate', NULL, 'Camden 68', 'Londyn', 'NW5 1AL', 'Greater London', 'Great Brittain', NULL, '51.55684', '-0.13911069999994652', '2017-06-20 12:20:03', '2017-06-20 12:20:03', NULL),
(7, 118, 'App\\Users\\Candidate', NULL, '635 ABC', NULL, '03431-020', 'São Paulo', 'Brazil', NULL, '-23.550876', '-46.537799800000016', '2017-07-26 08:49:53', '2017-07-26 08:49:53', NULL),
(8, 119, 'App\\Users\\Candidate', NULL, 'ABCD', 'Bolwarra', '3305', 'Glenelg', 'Australia', NULL, '-38.2575434', '141.6244957', '2017-07-27 03:51:50', '2017-07-27 03:51:50', NULL),
(9, 120, 'App\\Users\\Candidate', NULL, 'hedgehog lab', NULL, 'NE1 2LA', 'Tyne and Wear', 'United Kingdom', NULL, '54.9723695', '-1.6051821999999447', '2017-07-27 08:07:04', '2017-07-27 08:07:04', NULL),
(10, 20, 'App\\Employer', NULL, 'ABCD', 'Heathmere', '3305', 'Glenelg', 'Australia', NULL, '-38.19523969999999', '141.6158994', '2017-07-27 09:44:26', '2017-07-27 09:44:26', NULL),
(11, 16, 'App\\Education\\Institution', NULL, 'Abc', 'Wandilo', '5291', 'DC of Grant', 'Australia', NULL, '-37.7885415', '140.70998459999998', '2017-07-27 12:04:01', '2017-07-27 12:04:01', NULL),
(12, 124, 'App\\Users\\Candidate', NULL, 'abc', 'Saint Clairsville', '43950', 'Belmont County', 'United States', NULL, '40.06077399999999', '-80.901366', '2017-08-14 12:05:33', '2017-08-14 12:05:33', NULL),
(13, 21, 'App\\Employer', NULL, 'hajshcashcanc', 'Heßheim', '67258', NULL, 'Germany', NULL, '49.5422821', '8.292750299999966', '2017-08-14 12:15:57', '2017-08-14 12:15:57', NULL),
(14, 126, 'App\\Users\\Candidate', NULL, 'Xyz', 'Burlington', '05401', 'Chittenden County', 'United States', NULL, '44.4758442', '-73.21212100000002', '2017-08-14 18:15:05', '2017-08-14 18:15:05', NULL),
(15, 127, 'App\\Users\\Candidate', NULL, 'erterterr', 'Preston', 'PR1 3BY', 'Lancashire', 'Wielka Brytania', NULL, '53.7602029', '-2.6928576999999905', '2017-08-15 15:56:28', '2017-08-15 15:56:28', NULL),
(16, 22, 'App\\Employer', NULL, 'sdferwerwwe', 'Preston', 'PR1 3BY', 'Lancashire', 'Wielka Brytania', NULL, '53.7602029', '-2.6928576999999905', '2017-08-15 16:05:28', '2017-08-15 16:05:28', NULL),
(17, 129, 'App\\Users\\Candidate', NULL, 'Cotton Court, Church Street', 'Preston', 'PR1 3BY', 'Lancashire', 'United Kingdom', NULL, '53.7602029', '-2.6928576999999905', '2017-08-22 07:50:59', '2017-08-22 07:50:59', NULL),
(18, 130, 'App\\Users\\Candidate', NULL, 'Cotton Court, Church Street', 'Preston', 'PR1 3BY', 'Lancashire', 'United Kingdom', NULL, '53.7602029', '-2.6928576999999905', '2017-08-22 08:37:30', '2017-08-22 08:37:30', NULL),
(19, 23, 'App\\Employer', NULL, 'Cotton Court, Church Street', 'Preston', 'PR1 3BY', 'Lancashire', 'United Kingdom', NULL, '53.7602029', '-2.6928576999999905', '2017-08-23 12:00:49', '2017-08-23 12:00:49', NULL),
(20, 24, 'App\\Employer', NULL, 'Church Street', 'Preston', 'PR1 3BY', 'Lancashire', 'United Kingdom', NULL, '53.7602029', '-2.6928576999999905', '2017-08-23 12:38:27', '2017-08-23 12:38:27', NULL),
(21, 17, 'App\\Education\\Institution', NULL, '19 Westlands', 'Leyland', 'PR26 7XT', 'Lancashire', 'United Kingdom', NULL, '53.68679710000001', '-2.727135800000042', '2017-08-24 11:46:13', '2017-08-24 11:46:13', NULL),
(22, 138, 'App\\Users\\Candidate', NULL, 'Azaza street 12', 'Вашингтон', '63090', NULL, 'Соединенные Штаты Америки', NULL, '38.54344389999999', '-91.03946350000001', '2017-09-11 08:54:10', '2017-09-11 08:54:10', NULL),
(23, 3, '', NULL, 'asdsd', 'twtwt', 'ddhh4', 'vsfsgs', 'cvccv', NULL, '', '', '2017-11-05 12:04:46', '2018-01-24 02:26:55', NULL),
(24, 25, 'employer', NULL, 'panruti1', 'cuddalore', '607106', 'Tamil Nadu', 'India', NULL, NULL, NULL, '2017-11-18 07:49:47', '2017-11-18 07:50:33', '2017-11-25 00:35:33'),
(25, 26, 'employer', NULL, 'sssssss', 'cuddalore', '', 'Tamil Nadu', 'India', NULL, NULL, NULL, '2017-12-07 07:59:17', '0000-00-00 00:00:00', NULL),
(26, 27, 'employer', NULL, 'sssssss', 'cuddalore', '', 'Tamil Nadu', 'India', NULL, NULL, NULL, '2017-12-07 08:07:15', '0000-00-00 00:00:00', '2017-12-14 05:28:54'),
(27, 158, 'Candidate', NULL, 'adada', 'cuddalore', '6087 Princes Highway, Tyrendarra East, Victoria, Australia', 'Tamil Nadu', 'India', NULL, NULL, NULL, '2017-12-08 01:47:37', '0000-00-00 00:00:00', NULL),
(28, 159, 'Candidate', NULL, 'aSDadAD', 'sss', '6087 Princes Highway, Tyrendarra East, Victoria, Australia', 'sss', 'sss', NULL, NULL, NULL, '2017-12-08 02:03:12', '0000-00-00 00:00:00', NULL),
(29, 18, 'Institutions', NULL, 'panruti', 'cuddalore', '607106, India', 'Tamil Nadu', 'India', NULL, '11.7698354', '79.53224239999997', '2017-12-08 04:42:27', '0000-00-00 00:00:00', NULL),
(30, 167, 'Admin', NULL, 'panruti', 'cuddalore', '607106', 'Tamil Nadu', 'India', NULL, '', '', '2017-12-30 02:03:24', '0000-00-00 00:00:00', NULL),
(31, 168, 'Admin', NULL, 'panruti', '', '607106', 'Tamil Nadu', 'India', NULL, '11.7698354', '79.53224239999997', '2017-12-30 02:08:02', '0000-00-00 00:00:00', NULL),
(32, 169, 'Admin', NULL, 'panruti', 'cuddalore', '607106', 'Tamil Nadu', 'India', NULL, '', '', '2017-12-30 02:11:03', '0000-00-00 00:00:00', NULL),
(33, 170, 'Candidate', NULL, 'panruti', '', '607106', 'Tamil Nadu', 'India', NULL, '11.7698354', '79.53224239999997', '2018-12-30 07:46:28', '0000-00-00 00:00:00', NULL),
(34, 171, 'Candidate', NULL, 'panruti', '', '607106', 'Tamil Nadu', 'India', NULL, '11.7698354', '79.53224239999997', '2017-12-30 07:48:13', '0000-00-00 00:00:00', NULL),
(35, 30, 'employer', NULL, 'ssv', '', 'C1043AAT', 'Comuna 1', 'Argentina', NULL, '-34.6035846', '-58.375389299999995', '2018-01-02 04:45:38', '0000-00-00 00:00:00', NULL),
(36, 31, 'employer', NULL, 'adad', '', '607106', '', 'India', NULL, '11.7698354', '79.53224239999997', '2018-01-02 04:53:14', '0000-00-00 00:00:00', NULL),
(37, 34, 'employer', NULL, 'adad', 'Tyrendarra East', '3285', 'Moyne', 'Australia', NULL, '-38.24817650000001', '141.88783690000002', '2018-01-02 04:55:28', '0000-00-00 00:00:00', NULL),
(38, 35, 'employer', NULL, 'adad', 'Tyrendarra East', '3285', 'Moyne', 'Australia', NULL, '-38.24817650000001', '141.88783690000002', '2018-01-02 04:57:54', '0000-00-00 00:00:00', NULL),
(39, 36, 'employer', NULL, 'adad', 'Toronto', 'M4N 2L8', 'Toronto Division', 'Canada', NULL, '43.7299481', '-79.40312290000003', '2018-01-02 05:29:41', '0000-00-00 00:00:00', NULL),
(40, 37, 'employer', NULL, 'ada', 'Offenbach', '63071', 'DA', 'Germany', NULL, '50.0856921', '8.782361100000003', '2018-01-02 05:32:31', '0000-00-00 00:00:00', NULL),
(41, 38, 'employer', NULL, 'adad', '', 'C1043AAT', 'Comuna 1', 'Argentina', NULL, '-34.6035846', '-58.375389299999995', '2018-01-02 05:35:57', '0000-00-00 00:00:00', NULL),
(42, 183, 'Candidate', NULL, '', '', 'G3 8AZ', 'Glasgow City', 'United Kingdom', NULL, '55.8583203', '-4.269161199999985', '2018-01-18 03:36:01', '0000-00-00 00:00:00', NULL),
(43, 184, 'Candidate', NULL, '', '', 'G40 4EH', 'Glasgow City', 'United Kingdom', NULL, '55.8417852', '-4.224288200000046', '2018-01-18 03:41:35', '0000-00-00 00:00:00', NULL),
(44, 0, 'Candidate', NULL, 'afsafagag', 'agagg', 'G13 1BJ', 'Glasgow City', 'United Kingdom', NULL, '55.89720149999999', '-4.32589710000002', '2018-01-23 10:36:34', '0000-00-00 00:00:00', NULL),
(45, 0, 'Candidate', NULL, 'afsafagag', 'agagg', 'G13 1BJ', 'Glasgow City', 'United Kingdom', NULL, '55.89720149999999', '-4.32589710000002', '2018-01-23 10:37:12', '0000-00-00 00:00:00', NULL),
(46, 0, 'Candidate', NULL, 'afsafagag', 'agagg', 'G13 1BJ', 'Glasgow City', 'United Kingdom', NULL, '55.89720149999999', '-4.32589710000002', '2018-01-23 10:38:25', '0000-00-00 00:00:00', NULL),
(47, 0, 'Candidate', NULL, 'fafaf', 'afaff', 'G13 1BJ', 'Glasgow City', 'United Kingdom', NULL, '55.89720149999999', '-4.32589710000002', '2018-01-24 02:25:59', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cv16_conversations`
--

CREATE TABLE `cv16_conversations` (
  `id` int(10) UNSIGNED NOT NULL,
  `subject` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_conversations`
--

INSERT INTO `cv16_conversations` (`id`, `subject`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Welcome to CVVid', '2016-03-18 17:11:55', '2017-03-16 17:49:32', NULL),
(2, 'We are interested in your services', '2016-07-04 10:12:46', '2017-03-16 17:49:32', NULL),
(3, 'Your Application has been rejected', '2016-07-08 14:48:35', '2017-03-16 17:49:32', NULL),
(4, 'Your Application has been accepted', '2016-07-08 14:51:08', '2017-03-16 17:49:32', NULL),
(5, 'I like your profile', '2016-08-09 12:39:14', '2017-03-16 17:49:32', NULL),
(6, 'Hello Please Call me', '2016-08-30 14:26:54', '2017-03-16 17:49:32', NULL),
(7, 'Application received', '2017-01-26 16:58:17', '2017-03-16 17:49:32', NULL),
(8, 'Job Application', '2017-02-15 12:20:39', '2017-03-16 17:49:32', NULL),
(9, 'Your Application has been rejected', '2017-02-24 17:15:44', '2017-03-16 17:49:32', NULL),
(10, 'Your Application has been rejected', '2017-03-08 14:14:32', '2017-03-16 17:49:32', NULL),
(11, 'More info required', '2017-05-25 06:55:23', '2017-05-25 06:55:23', NULL),
(12, 'test', '2017-07-28 07:57:20', '2017-07-28 07:59:09', NULL),
(13, 'hi', '2017-12-30 07:54:22', '2017-12-30 07:54:22', NULL),
(14, 'ggg', '2017-12-30 07:58:50', '2017-12-30 07:58:50', NULL),
(15, 'sss', '2017-12-30 07:59:05', '2017-12-30 07:59:05', NULL),
(16, 'dgdg', '2017-12-30 08:09:03', '2017-12-30 08:09:03', NULL),
(17, 'testqaz', '2018-01-17 05:12:10', '2018-01-17 05:12:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cv16_conversation_members`
--

CREATE TABLE `cv16_conversation_members` (
  `id` int(10) UNSIGNED NOT NULL,
  `conversation_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_read` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_conversation_members`
--

INSERT INTO `cv16_conversation_members` (`id`, `conversation_id`, `user_id`, `name`, `last_read`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 5, 'Alex Heeney', '2018-01-23 08:49:05', '2016-03-18 17:11:55', '2017-03-16 17:49:32', NULL),
(2, 1, 3, 'Arif Bangi', '2018-01-23 08:49:05', '2016-03-18 17:11:55', '2017-03-16 17:49:32', NULL),
(3, 2, 4, 'Unknown User', '2018-01-23 08:49:04', '2016-07-04 10:12:46', '2017-03-16 17:49:32', NULL),
(4, 2, 3, 'Arif Bangi', '2018-01-23 08:49:04', '2016-07-04 10:12:46', '2017-03-16 17:49:32', NULL),
(5, 3, 4, 'Unknown User', '2016-07-08 14:48:35', '2016-07-08 14:48:35', '2017-03-16 17:49:32', NULL),
(6, 3, 21, 'Jane Smith', NULL, '2016-07-08 14:48:35', '2017-03-16 17:49:32', NULL),
(7, 4, 4, 'Unknown User', '2016-07-08 14:51:08', '2016-07-08 14:51:08', '2017-03-16 17:49:32', NULL),
(8, 4, 21, 'Jane Smith', NULL, '2016-07-08 14:51:08', '2017-03-16 17:49:32', NULL),
(9, 5, 4, 'Unknown User', '2018-01-23 08:49:03', '2016-08-09 12:39:14', '2017-03-16 17:49:32', NULL),
(10, 5, 3, 'Arif Bangi', '2018-01-23 08:49:03', '2016-08-09 12:39:14', '2017-03-16 17:49:32', NULL),
(11, 6, 4, 'Unknown User', '2018-01-23 08:49:03', '2016-08-30 14:26:54', '2017-03-16 17:49:32', NULL),
(12, 6, 3, 'Arif Bangi', '2018-01-23 08:49:03', '2016-08-30 14:26:54', '2017-03-16 17:56:11', NULL),
(13, 7, 32, 'Hasina Jogee', '2017-01-26 16:58:17', '2017-01-26 16:58:17', '2017-03-16 17:49:32', NULL),
(14, 7, 40, 'Hasina Bangi', '2017-03-17 08:52:31', '2017-01-26 16:58:17', '2017-03-17 08:52:31', NULL),
(15, 8, 34, 'Admin Recruiter', '2017-09-05 15:16:27', '2017-02-15 12:20:39', '2017-09-05 15:16:27', NULL),
(16, 8, 48, 'Joe Emery', NULL, '2017-02-15 12:20:39', '2017-03-16 17:49:32', NULL),
(17, 9, 34, 'Admin Recruiter', '2017-02-24 17:15:44', '2017-02-24 17:15:44', '2017-03-16 17:49:32', NULL),
(18, 9, 48, 'Joe Emery', NULL, '2017-02-24 17:15:44', '2017-03-16 17:49:32', NULL),
(19, 10, 33, 'Emily-Jo Sutcliffe', '2018-01-23 08:49:02', '2017-03-08 14:14:32', '2017-03-16 17:50:40', NULL),
(20, 10, 3, 'Arif Bangi', '2018-01-23 08:49:02', '2017-03-08 14:14:32', '2017-03-16 17:56:04', NULL),
(21, 11, 34, 'Admin Recruiter', '2017-09-05 15:16:29', '2017-05-25 06:55:23', '2017-09-05 15:16:29', NULL),
(22, 11, 91, 'Stefanie Collins', '2017-05-25 06:56:09', '2017-05-25 06:55:23', '2017-05-25 06:56:09', NULL),
(23, 12, 121, 'j p', NULL, '2017-07-28 07:57:20', '2017-07-28 07:59:09', NULL),
(24, 12, 119, 'j p', '2017-07-28 07:58:55', '2017-07-28 07:57:20', '2017-07-28 07:58:55', NULL),
(25, 13, 2, 'Adam Wilson', '2017-12-30 07:54:22', '2017-12-30 07:54:22', '2017-12-30 07:54:22', NULL),
(26, 14, 2, 'Adam Wilson', '2017-12-30 07:58:50', '2017-12-30 07:58:50', '2017-12-30 07:58:50', NULL),
(27, 15, 2, 'Adam Wilson', '2017-12-30 07:59:05', '2017-12-30 07:59:05', '2017-12-30 07:59:05', NULL),
(28, 16, 2, 'Adam Wilson', '2017-12-30 08:09:03', '2017-12-30 08:09:03', '2017-12-30 08:09:03', NULL),
(29, 17, 14, 'John Doe', '2018-01-23 08:48:59', '2018-01-17 05:12:10', '2018-01-17 05:12:10', NULL),
(30, 17, 3, 'John Doe', '2018-01-23 08:48:59', '2018-01-17 05:12:10', '2018-01-17 05:12:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cv16_conversation_messages`
--

CREATE TABLE `cv16_conversation_messages` (
  `id` int(10) UNSIGNED NOT NULL,
  `conversation_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_conversation_messages`
--

INSERT INTO `cv16_conversation_messages` (`id`, `conversation_id`, `user_id`, `name`, `message`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 5, 'Alex Heeney', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus et facilisis diam, sed auctor diam. Aenean at enim sem. Phasellus tempor diam ut accumsan suscipit. Fusce ornare aliquam tristique. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus id lectus a libero facilisis volutpat sed quis nunc. Ut vehicula dignissim ex, non tristique ante. Curabitur porttitor pulvinar elit sit amet auctor. Quisque quis mattis nulla.', '2016-03-18 17:11:55', '2017-03-16 17:49:32', NULL),
(2, 1, 3, 'Arif Bangi', 'Testing Message', '2016-03-23 13:10:44', '2017-03-16 17:49:32', NULL),
(3, 1, 3, 'Arif Bangi', 'Testing 2', '2016-03-23 13:12:19', '2017-03-16 17:49:32', NULL),
(4, 1, 3, 'Arif Bangi', 'Hello Alex', '2016-03-24 15:10:07', '2017-03-16 17:49:32', NULL),
(5, 1, 3, 'Arif Bangi', 'Testing new message', '2016-06-14 09:43:55', '2017-03-16 17:49:32', NULL),
(6, 2, 4, 'Unknown User', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam id pulvinar tellus. Aenean suscipit ligula in ante congue dapibus. Aenean vestibulum elementum dui vel posuere. Suspendisse non semper quam. Cras diam turpis, tempus vitae augue commodo, lobortis tristique dolor. Nam sodales condimentum pharetra. Fusce lobortis rhoncus nibh, ut aliquam est luctus ac. Vestibulum pharetra egestas lacinia. Sed ornare cursus rutrum.', '2016-07-04 10:12:46', '2017-03-16 17:49:32', NULL),
(7, 3, 4, 'Unknown User', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec neque urna, efficitur at lorem quis, fringilla rutrum sapien. In at rutrum nisi, quis pulvinar ex. Nullam a efficitur lacus. Suspendisse eget fermentum lorem, in bibendum enim. Mauris scelerisque dapibus sapien, quis pellentesque urna. Aenean nisl est, viverra eu semper eu, cursus a massa. Aenean gravida enim quis ligula maximus faucibus. Integer ultrices sem eu fringilla tempus. Phasellus blandit sed sem euismod rhoncus. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Vestibulum laoreet, dui sed bibendum facilisis, metus mauris commodo urna, vitae placerat leo ex ac purus. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vitae ligula lorem. Nullam et porttitor lorem, nec vestibulum justo. Praesent a lacinia dolor.\r\n\r\nIn placerat, ipsum quis posuere commodo, enim dui lacinia lacus, at dignissim sapien dolor nec libero. Nunc dictum, urna sed blandit suscipit, tellus ligula vestibulum mi, vitae suscipit sem massa id nibh. Duis eu lacus hendrerit, hendrerit tortor ac, cursus ante. Vestibulum aliquet at odio eget tincidunt. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Maecenas maximus sem in sapien aliquam ullamcorper. Suspendisse ex magna, tincidunt sed varius eu, accumsan eu odio. Nulla facilisi. Proin vestibulum semper turpis, non tincidunt lorem. Fusce urna ligula, sagittis a quam sed, dignissim rutrum ipsum. Phasellus nec quam in tortor ullamcorper sollicitudin. Etiam id ligula aliquet arcu ultricies laoreet. Nulla facilisi. Pellentesque ac risus vel augue sodales suscipit.', '2016-07-08 14:48:35', '2017-03-16 17:49:32', NULL),
(8, 4, 4, 'Unknown User', 'Praesent ultrices, felis ut volutpat consequat, mi erat tincidunt mi, nec ornare metus felis sed ex. Suspendisse ac turpis nulla. Curabitur cursus pretium nulla, vel scelerisque leo aliquam id. Integer et nibh eu nisl pellentesque egestas. Vivamus vitae lectus egestas, convallis ante at, iaculis dui. Donec pulvinar sagittis augue, id aliquam tellus finibus vel. Cras facilisis urna ut pharetra efficitur. Nunc ultrices rutrum massa, in varius est suscipit et. Donec pharetra lorem et est gravida, gravida euismod ex efficitur. Proin porttitor hendrerit ante, vel sagittis enim venenatis eu. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Pellentesque risus nulla, tincidunt non auctor vitae, ultrices sit amet eros. Curabitur porta facilisis suscipit.\r\n\r\nSed mattis, felis non maximus accumsan, felis ante lacinia diam, et tincidunt nulla tellus quis turpis. Aenean eget faucibus est. Proin in dapibus lorem. Nullam in est convallis, finibus dolor sed, eleifend nulla. Donec dui ipsum, varius sit amet justo non, luctus hendrerit nisl. Sed dictum tempus ornare. Sed molestie nisl nec commodo sagittis. Proin non mauris eu libero molestie laoreet. Integer in libero magna.', '2016-07-08 14:51:08', '2017-03-16 17:49:32', NULL),
(9, 5, 4, 'Unknown User', 'Interested in a job? Get in touch', '2016-08-09 12:39:14', '2017-03-16 17:49:32', NULL),
(10, 5, 3, 'Arif Bangi', 'No thanks', '2016-08-09 12:40:09', '2017-03-16 17:49:32', NULL),
(11, 6, 4, 'Unknown User', 'We have a job you may be interested in.', '2016-08-30 14:26:54', '2017-03-16 17:49:32', NULL),
(12, 6, 3, 'Arif Bangi', 'Thank you for your message, i am interested in an interview.', '2016-08-30 14:29:28', '2017-03-16 17:49:32', NULL),
(13, 6, 4, 'Unknown User', 'Thats great we will be in touch tomorrow to arrange an interview date.', '2016-08-30 14:30:15', '2017-03-16 17:49:32', NULL),
(14, 6, 4, 'Unknown User', 'Your interview has been scheduled for a week on monday at 2:30 PM, please confirm that this time is suitable.', '2016-08-30 14:36:39', '2017-03-16 17:49:32', NULL),
(15, 6, 3, 'Arif Bangi', 'Thank you for your message, i am interested in an interview.', '2016-08-30 14:39:05', '2017-03-16 17:49:32', NULL),
(16, 7, 32, 'Hasina Jogee', 'Hi\nThank you for applying for the role. We will be in touch soon', '2017-01-26 16:58:17', '2017-03-16 17:49:32', NULL),
(17, 7, 40, 'Hasina Bangi', 'Thank you. I hope you like the video I have uploaded', '2017-01-26 16:59:29', '2017-03-16 17:49:32', NULL),
(18, 8, 34, 'Admin Recruiter', 'Please resubmit your application once you have filled out your profile and uploaded a video CV', '2017-02-15 12:20:39', '2017-03-16 17:49:32', NULL),
(19, 9, 34, 'Admin Recruiter', 'Thank you for your application. Unfortunately you have not been successful on this occasion. A proper filled out CV along with a video CV is required to be considered for shortlisting', '2017-02-24 17:15:44', '2017-03-16 17:49:32', NULL),
(20, 10, 33, 'Emily-Jo Sutcliffe', 'Sorry - you\'re over-qualified for the role!', '2017-03-08 14:14:32', '2017-03-16 17:49:32', NULL),
(21, 11, 34, 'Admin Recruiter', 'Thank you for applying. Can you provide more information about your education and experience please', '2017-05-25 06:55:23', '2017-05-25 06:55:23', NULL),
(22, 12, 121, 'j p', 'ji jp', '2017-07-28 07:57:20', '2017-07-28 07:57:20', NULL),
(23, 12, 119, 'j p', 'ok', '2017-07-28 07:59:09', '2017-07-28 07:59:09', NULL),
(24, 13, 2, 'Adam Wilson', 'ggggg', '2017-12-30 07:54:22', '2017-12-30 07:54:22', NULL),
(25, 14, 2, 'Adam Wilson', 'ggggg', '2017-12-30 07:58:50', '2017-12-30 07:58:50', NULL),
(26, 15, 2, 'Adam Wilson', 'ssss', '2017-12-30 07:59:05', '2017-12-30 07:59:05', NULL),
(27, 16, 2, 'Adam Wilson', 'dgdgd', '2017-12-30 08:09:03', '2017-12-30 08:09:03', NULL),
(28, 17, 14, 'John Doe', 'testqaztestqaztestqaztestqaztestqaztestqaztestqaztestqaztestqaz', '2018-01-17 05:12:10', '2018-01-17 05:12:10', NULL),
(29, 10, 33, 'Emily-Jo Sutcliffe', 'testsadd', '2018-01-17 06:34:43', '2018-01-17 06:34:43', NULL),
(30, 10, 33, 'Emily-Jo Sutcliffe', 'test321', '2018-01-17 06:36:27', '2018-01-17 06:36:27', NULL),
(31, 10, 33, 'Emily-Jo Sutcliffe', 'aaagag', '2018-01-17 06:37:54', '2018-01-17 06:37:54', NULL),
(32, 10, 33, 'Emily-Jo Sutcliffe', 'fafaf', '2018-01-17 06:38:39', '2018-01-17 06:38:39', NULL),
(33, 10, 33, 'Emily-Jo Sutcliffe', 'asdasdasd', '2018-01-17 06:39:40', '2018-01-17 06:39:40', NULL),
(34, 10, 33, 'Emily-Jo Sutcliffe', 'asdafsaf', '2018-01-17 06:41:14', '2018-01-17 06:41:14', NULL),
(35, 10, 33, 'Emily-Jo Sutcliffe', 'asdadaf', '2018-01-17 06:46:02', '2018-01-17 06:46:02', NULL),
(36, 10, 33, 'Emily-Jo Sutcliffe', 'asfgagag', '2018-01-17 06:47:54', '2018-01-17 06:47:54', NULL),
(37, 10, 33, 'Emily-Jo Sutcliffe', 'asdaf', '2018-01-17 06:48:16', '2018-01-17 06:48:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cv16_employers`
--

CREATE TABLE `cv16_employers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8_unicode_ci,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_lat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_lng` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stripe_active` tinyint(4) NOT NULL DEFAULT '0',
  `stripe_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stripe_subscription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stripe_plan` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_four` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_expiry` date DEFAULT NULL,
  `card_expiry_sent` date DEFAULT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `subscription_ends_at` timestamp NULL DEFAULT NULL,
  `published` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_employers`
--

INSERT INTO `cv16_employers` (`id`, `user_id`, `name`, `email`, `body`, `location`, `location_lat`, `location_lng`, `website`, `tel`, `stripe_active`, `stripe_id`, `stripe_subscription`, `stripe_plan`, `last_four`, `card_expiry`, `card_expiry_sent`, `trial_ends_at`, `subscription_ends_at`, `published`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3, 14, 'Apple', NULL, '<p>Testing 12</p>\n', 'California Street, San Francisco, CA, United States', '', '', 'http://www.apple.com', '022112 31312', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-06-15 15:08:02', '2017-12-14 05:30:56', NULL),
(4, 26, 'Anderson & Wyatt Limited', NULL, 'eeeeeee', 'Great Harwood, United Kingdom', '53.78191399999999', '-2.4002490000000307', 'Www.andersonwyatt.co.uk', '07983641125', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-10-03 19:48:31', '2017-01-26 11:46:52', NULL),
(5, 29, 'Ps Events Management Ltd ', NULL, '', 'Oldham, United Kingdom', '53.5409298', '-2.11136590000001', 'Www.psem.co.uk ', '07882906254', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2016-11-10 22:18:27', '2016-11-10 22:31:50', NULL),
(6, 32, 'My Kitchen Bakery', 'mykitchenbakery@outlook.com', 'My Kitchen Bakery is a small enterprise making delicious and mouth watering bakes, ranging from flapjacks and rocky roads to delicacies like macarons. ', 'Blackburn, United Kingdom', '53.748575', '-2.487528999999995', '', '01254265075', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-30 16:38:16', NULL, 1, '2016-12-30 16:38:14', '2016-12-30 23:45:35', NULL),
(7, 33, 'Red Fern Media', 'emily@red-fern.co.uk', NULL, 'Altham, United Kingdom', '53.7890755', '-2.347248499999978', 'http://www.red-fern.co.uk', '01254 300030', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-13 15:21:32', NULL, 1, '2017-01-13 15:21:30', '2017-01-13 15:21:32', NULL),
(8, 34, 'CVVID', 'careers@cvvid.com', 'At CVVID we believe in giving everyone the opportunity to excel. Candidates to promote themselves in a way they haven\'t previously done so and get the job they desire, and employers to recruit the best candidates to enhance their company further.\r\n\r\nBy choosing CVVID as an employer would mean you also aspire to excel in everything you do and will show that drive and commitment in your role. We employ people who are dynamic and share our vision of giving people a better chance of employment and employers a better workforce.', 'Blackburn, United Kingdom', '53.748575', '-2.487528999999995', 'www.cvvid.com', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-18 09:29:14', NULL, 1, '2017-01-18 09:29:12', '2017-01-18 09:42:07', NULL),
(9, 38, 'Fonezone', 'ish@fonezone.co.uk', NULL, 'Blackburn, United Kingdom', '53.748575', '-2.487528999999995', 'Www.fonezone.co.uk', '07970307402', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-20 21:34:29', NULL, 1, '2017-01-20 21:34:27', '2017-01-20 21:34:29', NULL),
(10, 39, 'Printhubdesign.co.uk', 'info@printhubdesign.co.uk', '', 'Blackburn', '', '', 'www.printhubdesign.co.uk', '01254 678821', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-24 15:33:07', NULL, 1, '2017-01-24 15:33:05', '2017-04-19 10:37:46', NULL),
(11, 41, 'Employer Test', 'dan@red-fern.co.uk', NULL, 'London, United Kingdom', '51.5073509', '-0.12775829999998223', 'http://employertest.test', '01232 123421', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-27 16:03:41', NULL, 1, '2017-01-27 16:03:40', '2017-01-27 16:03:41', '2017-12-14 02:28:46'),
(12, 42, 'Tienda Digital', 'daniel.fisher@tiendadigital.co.uk', NULL, 'Preston, United Kingdom', '', '', 'www.tiendadigital.co.uk', '01772 393989', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-01-30 10:05:21', '2017-01-30 10:05:21', '2017-12-14 01:33:49'),
(13, 44, 'ABTECH', 'bhaveshnayi@rlogical.com', NULL, 'Ahmedabad, Gujarat, India', '23.022505', '72.57136209999999', '', '1234567890', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-02-01 12:13:44', '2017-02-01 12:13:44', '2017-12-14 01:35:35'),
(14, 45, 'Marble Management', 'faz@marblemanagement.co.uk', '<p>Marble Management is a team of consultants who all have individual experience delivering world class events in both the UK and abroad, in countries such as Holland, Italy and Belgium. Whether it is a conference, wedding or a corporate team building activity, our event consultants bring with them a wealth of experience so you can rest assured that you will be hosting a first-rate event. Marble Management promises to take away the pressures of organising an event whatever the size. We also pride ourselves in being able to deliver tailor made events to suit your needs. Our consultants will ensure you are kept informed every step of the way, so your event or activity is exactly how you want it.</p>', 'Cardiff, United Kingdom', '51.48158100000001', '-3.1790899999999738', 'www.marblemanagement.co.uk', '02920508722', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-01 18:59:05', NULL, 0, '2017-02-01 18:59:02', '2017-04-19 10:38:50', NULL),
(15, 46, 'aniruddh ds', 'aniruddh@rlogical.com', '', 'Arizona, United States', '34.0489281', '-111.09373110000001', '', '012644522', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-02-02 12:51:12', '2017-02-02 12:51:58', '2017-12-14 05:18:52'),
(16, 47, 'Nova Adventure Challenges', 'susan.jones@novaadventurechallenges.com', NULL, 'Cardiff, United Kingdom', '51.48158100000001', '-3.1790899999999738', '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-02 16:10:29', NULL, 1, '2017-02-02 16:10:28', '2017-02-02 16:11:01', NULL),
(17, 50, 'Human Appeal', 'Mosharaf.ali@humanappeal.org.uk', NULL, '1 Cheadle Point, Cheadle, United Kingdom', '53.3860933', '-2.2098022999999785', '1 Cheadle Point,', '01612250225', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-02-14 10:40:05', '2017-02-14 10:40:05', NULL),
(19, 59, 'The Brand Alchemy', 'arif@arifbangi.com', '<p>The premier strategy boutique.</p>', 'Manchester, United Kingdom', '53.4807593', '-2.2426305000000184', 'http://thebrandalchemy.com', '01231', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-01 23:00:00', NULL, 1, '2017-02-21 17:15:10', '2017-02-21 17:15:10', NULL),
(20, 121, 'TEST', 'zcotestmail2@gmail.com', '', NULL, NULL, NULL, 'mo.com', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-27 09:44:27', NULL, 1, '2017-07-27 09:44:26', '2017-07-27 09:52:03', NULL),
(21, 125, 'ssc', 'shyampradeesh10@gmail.com', NULL, NULL, NULL, NULL, 'www.ssc.com', '123456789', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-14 12:16:06', NULL, 1, '2017-08-14 12:15:57', '2017-08-14 12:16:06', NULL),
(22, 128, 'Tienda Digital', 'developer2@tiendadigital.co.uk', NULL, NULL, NULL, NULL, 'http://tiendadigital.co.uk/', '00000000000', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-15 16:05:29', NULL, 1, '2017-08-15 16:05:28', '2017-08-15 16:05:29', NULL),
(23, 131, 'Tienda Digital', 'eleisha+1@tiendadigital.co.uk', 'Test company background.', '', '', '', 'tiendadigital.co.uk', '01772 393989', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-08-23 12:00:49', '2017-08-23 12:22:50', NULL),
(24, 133, 'Digital Tienda', 'eleisha+2@tiendadigital.co.uk', '', NULL, NULL, NULL, 'tiendadigital.co.uk', '01772 393989', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-23 12:38:27', NULL, 1, '2017-08-23 12:38:27', '2017-08-23 13:19:14', NULL),
(25, 143, 'Samsung', 'ezhil123@gmail.com', '<p>testing</p>\r\n', NULL, NULL, NULL, 'samsung.com', '123456', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-11-18 07:49:47', '2017-11-18 07:50:33', '2017-11-25 00:35:33'),
(26, 156, 'sss', 'sssssss234@gmail.com', NULL, NULL, NULL, NULL, 'sss', '022112 31312', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-12-07 07:59:17', '0000-00-00 00:00:00', NULL),
(27, 157, 'sss', 'sssssss23455@gmail.com', NULL, NULL, NULL, NULL, 'sss', '022112 31312', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2017-12-07 08:07:15', '0000-00-00 00:00:00', '2017-12-14 05:28:54'),
(28, NULL, 'xvxvx', 'sff@gmail.com', '', NULL, NULL, NULL, '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-01-02 03:31:59', '0000-00-00 00:00:00', NULL),
(29, NULL, 'scscs', 'dsdvd@gmail.com', '', NULL, NULL, NULL, '', '', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-01-02 03:37:16', '0000-00-00 00:00:00', NULL),
(30, 172, 'sfsfsf', 'sssssss2dd3455@gmail.com', '<p>sssss</p>', NULL, NULL, NULL, 'sss', 'ssvs', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-01-02 04:45:38', '0000-00-00 00:00:00', NULL),
(31, 173, 'dadadad', 'sscs@gmail.com', '<p>sacscs</p>', NULL, NULL, NULL, 'adad', 'adadad', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-01-02 04:53:14', '0000-00-00 00:00:00', NULL),
(32, 174, 'dadadad', 'sscs44@gmail.com', '<p>sacscs</p>', NULL, NULL, NULL, 'adad', 'adadad', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-01-02 04:54:43', '0000-00-00 00:00:00', NULL),
(33, 175, 'dadadadscs', 'sscs4433@gmail.com', '<p>adazdada</p>', NULL, NULL, NULL, 'adad', 'adadad', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-01-02 04:55:03', '0000-00-00 00:00:00', NULL),
(34, 176, 'dadadadscsw', 'sscs4w433@gmail.com', '<p>adadadad</p>', NULL, NULL, NULL, 'adad', 'adadad', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-01-02 04:55:28', '0000-00-00 00:00:00', NULL),
(35, 177, 'dadadadscsww', 'sscs4waq433@gmail.com', '', NULL, NULL, NULL, 'adad', 'adadad', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-01-02 04:57:54', '0000-00-00 00:00:00', NULL),
(36, NULL, 'dadadadscswwadad', 'sscs4wddaq433@gmail.com', '<p>adadadadadad</p>', NULL, NULL, NULL, 'adada', 'ada', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-01-02 05:29:41', '0000-00-00 00:00:00', NULL),
(37, 178, 'zdazda', 'adsad11@gmail.comss', '<p>bfbfbf</p>', NULL, NULL, NULL, 'adad', 'adad', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-01-02 05:32:31', '0000-00-00 00:00:00', NULL),
(38, 179, 'aadada', 'adaaaad@gmail.com', '<p>adadadada</p>', NULL, NULL, NULL, 'adada', 'adada', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2018-01-02 05:35:57', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cv16_employer_users`
--

CREATE TABLE `cv16_employer_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `employer_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_employer_users`
--

INSERT INTO `cv16_employer_users` (`id`, `employer_id`, `user_id`, `role`, `created_at`, `updated_at`) VALUES
(6, 3, 14, NULL, '2016-06-15 15:08:03', '2016-06-15 15:08:03'),
(7, 4, 26, NULL, '2016-10-03 19:48:35', '2016-10-03 19:48:35'),
(8, 5, 29, NULL, '2016-11-10 22:18:27', '2016-11-10 22:18:27'),
(9, 6, 32, 'admin', '2016-12-30 16:38:14', '2016-12-30 16:38:14'),
(10, 7, 33, 'admin', '2017-01-13 15:21:30', '2017-01-13 15:21:30'),
(11, 8, 34, 'admin', '2017-01-18 09:29:13', '2017-01-18 09:29:13'),
(12, 9, 38, 'admin', '2017-01-20 21:34:27', '2017-01-20 21:34:27'),
(13, 10, 39, 'admin', '2017-01-24 15:33:05', '2017-01-24 15:33:05'),
(14, 11, 41, 'admin', '2017-01-27 16:03:40', '2017-01-27 16:03:40'),
(15, 12, 42, 'admin', '2017-01-30 10:05:21', '2017-01-30 10:05:21'),
(16, 13, 44, 'admin', '2017-02-01 12:13:44', '2017-02-01 12:13:44'),
(17, 14, 45, 'admin', '2017-02-01 18:59:02', '2017-02-01 18:59:02'),
(18, 15, 46, 'admin', '2017-02-02 12:51:12', '2017-02-02 12:51:12'),
(19, 16, 47, 'admin', '2017-02-02 16:10:28', '2017-02-02 16:10:28'),
(20, 17, 50, 'admin', '2017-02-14 10:40:05', '2017-02-14 10:40:05'),
(21, 19, 59, 'admin', '2017-02-21 17:15:10', '2017-02-21 17:15:10'),
(22, 20, 121, 'admin', '2017-07-27 09:44:26', '2017-07-27 09:44:26'),
(23, 20, 123, NULL, '2017-07-28 06:39:47', '2017-07-28 06:39:47'),
(24, 21, 125, 'admin', '2017-08-14 12:15:57', '2017-08-14 12:15:57'),
(25, 22, 128, 'admin', '2017-08-15 16:05:28', '2017-08-15 16:05:28'),
(26, 23, 131, 'admin', '2017-08-23 12:00:49', '2017-08-23 12:00:49'),
(27, 23, 132, NULL, '2017-08-23 12:27:24', '2017-08-23 12:27:24'),
(28, 24, 133, 'admin', '2017-08-23 12:38:27', '2017-08-23 12:38:27'),
(29, 25, 143, 'admin', '2017-11-18 07:49:47', '0000-00-00 00:00:00'),
(30, 3, 34, 'admin', '2017-01-18 09:29:13', '2017-01-18 09:29:13'),
(31, 3, 145, 'admin', '2017-11-21 02:15:02', '0000-00-00 00:00:00'),
(32, 3, 146, 'admin', '2017-11-21 02:15:36', '0000-00-00 00:00:00'),
(33, 3, 149, 'admin', '2017-11-21 06:13:40', '0000-00-00 00:00:00'),
(34, 3, 150, 'admin', '2017-11-21 06:16:40', '0000-00-00 00:00:00'),
(35, 4, 151, 'admin', '2017-11-21 06:22:45', '0000-00-00 00:00:00'),
(36, 7, 153, 'admin', '2017-11-21 06:31:33', '0000-00-00 00:00:00'),
(37, 3, 154, 'admin', '2017-11-24 00:37:23', '2017-11-24 06:34:44'),
(38, 26, 156, NULL, '2017-12-07 07:59:17', '0000-00-00 00:00:00'),
(39, 27, 157, NULL, '2017-12-07 08:07:15', '0000-00-00 00:00:00'),
(40, 30, 172, 'admin', '2018-01-02 04:45:38', '0000-00-00 00:00:00'),
(41, 31, 173, 'admin', '2018-01-02 04:53:14', '0000-00-00 00:00:00'),
(42, 32, 174, 'admin', '2018-01-02 04:54:43', '0000-00-00 00:00:00'),
(43, 33, 175, 'admin', '2018-01-02 04:55:03', '0000-00-00 00:00:00'),
(44, 34, 176, 'admin', '2018-01-02 04:55:28', '0000-00-00 00:00:00'),
(45, 35, 177, 'admin', '2018-01-02 04:57:54', '0000-00-00 00:00:00'),
(46, 37, 178, 'admin', '2018-01-02 05:32:31', '0000-00-00 00:00:00'),
(47, 38, 179, 'admin', '2018-01-02 05:35:57', '0000-00-00 00:00:00'),
(52, 5, 180, 'admin', '2018-01-02 06:07:44', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cv16_industries`
--

CREATE TABLE `cv16_industries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_industries`
--

INSERT INTO `cv16_industries` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Accounting', '2016-03-18 12:11:15', '2017-12-19 05:03:06', NULL),
(2, 'Airlines/Aviation', '2016-03-18 12:11:15', '2016-03-18 12:11:15', NULL),
(3, 'Alternative Dispute Resolution', '2016-03-18 12:11:15', '2016-03-18 12:11:15', NULL),
(4, 'Alternative Medicine', '2016-03-18 12:11:15', '2016-03-18 12:11:15', NULL),
(5, 'Animation', '2016-03-18 12:11:15', '2016-03-18 12:11:15', NULL),
(6, 'Apparel & Fashion', '2016-03-18 12:11:15', '2016-03-18 12:11:15', NULL),
(7, 'Architecture & Planning', '2016-03-18 12:11:15', '2016-03-18 12:11:15', NULL),
(8, 'Arts and Crafts', '2016-03-18 12:11:15', '2016-03-18 12:11:15', NULL),
(9, 'Automotive', '2016-03-18 12:11:15', '2016-03-18 12:11:15', NULL),
(10, 'Aviation & Aerospace', '2016-03-18 12:11:15', '2016-03-18 12:11:15', NULL),
(11, 'Banking', '2016-03-18 12:11:15', '2016-03-18 12:11:15', NULL),
(12, 'Biotechnology', '2016-03-18 12:11:15', '2016-03-18 12:11:15', NULL),
(13, 'Broadcast Media', '2016-03-18 12:11:15', '2016-03-18 12:11:15', NULL),
(14, 'Building Materials', '2016-03-18 12:11:15', '2016-03-18 12:11:15', NULL),
(15, 'Business Supplies and Equipment', '2016-03-18 12:11:15', '2016-03-18 12:11:15', NULL),
(16, 'Capital Markets', '2016-03-18 12:11:15', '2016-03-18 12:11:15', NULL),
(17, 'Chemicals', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(18, 'Civic & Social Organization', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(19, 'Civil Engineering', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(20, 'Commercial Real Estate', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(21, 'Computer & Network Security', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(22, 'Computer Games', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(23, 'Computer Hardware', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(24, 'Computer Networking', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(25, 'Computer Software', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(26, 'Construction', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(27, 'Consumer Electronics', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(28, 'Consumer Goods', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(29, 'Consumer Services', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(30, 'Cosmetics', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(31, 'Dairy', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(32, 'Defense & Space', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(33, 'Design', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(34, 'Education Management', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(35, 'E-Learning', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(36, 'Electrical/Electronic Manufacturing', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(37, 'Entertainment', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(38, 'Environmental Services', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(39, 'Events Services', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(40, 'Executive Office', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(41, 'Facilities Services', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(42, 'Farming', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(43, 'Financial Services', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(44, 'Fine Art', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(45, 'Fishery', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(46, 'Food & Beverages', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(47, 'Food Production', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(48, 'Fund-Raising', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(49, 'Furniture', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(50, 'Gambling & Casinos', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(51, 'Glass, Ceramics & Concrete', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(52, 'Government Administration', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(53, 'Government Relations', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(54, 'Graphic Design', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(55, 'Health, Wellness and Fitness', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(56, 'Higher Education', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(57, 'Hospital & Health Care', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(58, 'Hospitality', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(59, 'Human Resources', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(60, 'Import and Export', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(61, 'Individual & Family Services', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(62, 'Industrial Automation', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(63, 'Information Services', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(64, 'Information Technology and Services', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(65, 'Insurance', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(66, 'International Affairs', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(67, 'International Trade and Development', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(68, 'Internet', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(69, 'Investment Banking', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(70, 'Investment Management', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(71, 'Judiciary', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(72, 'Law Enforcement', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(73, 'Law Practice', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(74, 'Legal Services', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(75, 'Legislative Office', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(76, 'Leisure, Travel & Tourism', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(77, 'Libraries', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(78, 'Logistics and Supply Chain', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(79, 'Luxury Goods & Jewelry', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(80, 'Machinery', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(81, 'Management Consulting', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(82, 'Maritime', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(83, 'Marketing and Advertising', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(84, 'Market Research', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(85, 'Mechanical or Industrial Engineering', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(86, 'Media Production', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(87, 'Medical Devices', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(88, 'Medical Practice', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(89, 'Mental Health Care', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(90, 'Military', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(91, 'Mining & Metals', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(92, 'Motion Pictures and Film', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(93, 'Museums and Institutions', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(94, 'Music', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(95, 'Nanotechnology', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(96, 'Newspapers', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(97, 'Nonprofit Organization Management', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(98, 'Oil & Energy', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(99, 'Online Media', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(100, 'Outsourcing/Offshoring', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(101, 'Package/Freight Delivery', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(102, 'Packaging and Containers', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(103, 'Paper & Forest Products', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(104, 'Performing Arts', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(105, 'Pharmaceuticals', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(106, 'Philanthropy', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(107, 'Photography', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(108, 'Plastics', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(109, 'Political Organization', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(110, 'Primary/Secondary Education', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(111, 'Printing', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(112, 'Professional Training & Coaching', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(113, 'Program Development', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(114, 'Public Policy', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(115, 'Public Relations and Communications', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(116, 'Public Safety', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(117, 'Publishing', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(118, 'Railroad Manufacture', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(119, 'Ranching', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(120, 'Real Estate', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(121, 'Recreational Facilities and Services', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(122, 'Religious Institutions', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(123, 'Renewables & Environment', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(124, 'Research', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(125, 'Restaurants', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(126, 'Retail', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(127, 'Security and Investigations', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(128, 'Semiconductors', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(129, 'Shipbuilding', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(130, 'Sporting Goods', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(131, 'Sports', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(132, 'Staffing and Recruiting', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(133, 'Supermarkets', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(134, 'Telecommunications', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(135, 'Textiles', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(136, 'Think Tanks', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(137, 'Tobacco', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(138, 'Translation and Localization', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(139, 'Transportation/Trucking/Railroad', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(140, 'Utilities', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(141, 'Venture Capital & Private Equity', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(142, 'Veterinary', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(143, 'Warehousing', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(144, 'Wholesale', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(145, 'Wine and Spirits', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(146, 'Wireless', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(147, 'Writing and Editing', '2016-03-18 12:11:16', '2016-03-18 12:11:16', NULL),
(148, 'Recruitment', '2017-01-10 16:31:44', '2017-01-10 16:31:44', NULL),
(149, 'ffff', '2017-12-01 05:52:57', '0000-00-00 00:00:00', '2017-12-01 06:20:15'),
(150, 'ffff', '2017-12-01 05:53:59', '0000-00-00 00:00:00', '2017-12-01 06:20:15');

-- --------------------------------------------------------

--
-- Table structure for table `cv16_institutions`
--

CREATE TABLE `cv16_institutions` (
  `id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8_unicode_ci,
  `address_1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `town` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `post_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `num_pupils` int(11) DEFAULT NULL,
  `stripe_active` tinyint(4) NOT NULL DEFAULT '0',
  `stripe_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stripe_subscription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stripe_plan` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_four` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_expiry` date DEFAULT NULL,
  `card_expiry_sent` date DEFAULT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `subscription_ends_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_institutions`
--

INSERT INTO `cv16_institutions` (`id`, `admin_id`, `name`, `email`, `body`, `address_1`, `address_2`, `town`, `post_code`, `status`, `num_pupils`, `stripe_active`, `stripe_id`, `stripe_subscription`, `stripe_plan`, `last_four`, `card_expiry`, `card_expiry_sent`, `trial_ends_at`, `subscription_ends_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 53, 'College of A Bangi', 'hello@cvvid.com', NULL, '14 Mandela Court', '', 'Blackburn', 'bb1 7lt', 'awaiting_payment', 16, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-20 22:22:48', '2017-02-20 22:25:18', NULL),
(3, 54, 'TIGHSs', 'asia.ali@tighs.tetrust.org', 'sssssssssssssss', 'Preston New Road', '', 'Blackburn', 'BB2 7AD', 'active', 16, 1, 'cus_ADHrGvp3XqF4oX', 'sub_ADHrZb0KtUPf0P', 'tighs', '0219', NULL, NULL, NULL, NULL, '2017-02-21 08:34:39', '2018-01-02 08:18:05', NULL),
(6, 60, 'Red Fern Test', 'awilsones@gmail.com', NULL, 'Media House', 'Media Village', 'Altham', 'BB5 5TX', 'acive', 5, 1, 'cus_C9awqcs7PKsGgV', 'sub_C9awa39P6LasgL', 'red_fern_test', '242', NULL, NULL, NULL, NULL, '2017-02-28 11:37:56', '2018-01-17 09:48:45', NULL),
(7, 74, 'GeorgePep', 'simmpybyqtan@mail.ru', NULL, 'Kakamega', 'Kakamega', 'Kakamega', 'http://www.countryheatdvd.com/', 'pending', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-21 13:18:59', '2017-03-21 13:18:59', NULL),
(8, 75, 'WilliamVax', 'sicrfcimer@mail.ru', NULL, 'Colonel Hill', 'Colonel Hill', 'Colonel Hill', 'http://www.countryheatdvd.com/', 'pending', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-21 13:53:34', '2017-03-21 13:53:34', NULL),
(9, 76, 'Robertidody', 'didnaxio@mail.ru', NULL, 'San Vicente De Tagua Tagua', 'San Vicente De Tagua Tagua', 'San Vicente De Tagua Tagua', 'http://www.coredeforcemma.com/', 'pending', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-21 14:21:42', '2017-03-21 14:21:42', NULL),
(10, 81, 'DanielBek', 'mccacszwyr@mail.ru', NULL, 'Lar', 'Lar', 'Lar', 'http://www.countryheatdvd.com/', 'pending', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-24 19:59:09', '2017-03-24 19:59:09', NULL),
(11, 82, 'JamesWew', 'polgnwsigwin@mail.ru', NULL, 'Hesperange', 'Hesperange', 'Hesperange', 'http://www.cizeshaunt.com/', 'pending', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-26 12:01:34', '2017-03-26 12:01:34', NULL),
(12, 84, 'DarwinHuh', 'vircetaecra@mail.ru', NULL, 'Duverge', 'Duverge', 'Duverge', 'http://www.countryheatbeachbody.com/', 'pending', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-30 01:49:58', '2017-03-30 01:49:58', NULL),
(13, 85, 'Arthurwap', 'himebvvjh@mail.ru', NULL, 'Havana', 'Havana', 'Havana', 'http://www.countryheatdvd.com/', 'pending', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-30 13:43:20', '2017-03-30 13:43:20', NULL),
(14, 86, 'ErvinCet', 'arlummucri@mail.ru', NULL, 'Santa Maria', 'Santa Maria', 'Santa Maria', 'http://www.countryheatdvd.com/', 'pending', 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-30 17:07:09', '2017-03-30 17:07:09', NULL),
(15, 87, 'College of CVVID', 'cvvidnew@gmail.com', 'CVVid College was set up to aspire young people to reach their potential, and be the ideal stepping stone towards a bright prosperous future. Join today to begin your journey of success.', NULL, NULL, NULL, NULL, 'active', 25, 1, 'cus_ASHmHYfjsf3Wik', 'sub_ASHmE39ZscuCDL', 'college_of_cvvid', '0024', NULL, NULL, NULL, NULL, '2017-04-11 08:36:59', '2017-04-11 10:33:21', NULL),
(16, 122, 'test', 'testmail3@gmail.com', NULL, NULL, NULL, NULL, NULL, 'pending', 40, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-27 12:04:01', '2017-07-27 12:04:01', NULL),
(17, 134, 'JS School', 'eleisha+3@tiendadigital.co.uk', NULL, NULL, NULL, NULL, NULL, 'awaiting_payment', 50, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-08-24 11:46:12', '2017-08-24 11:51:46', NULL),
(18, 160, '12 valavan', 'ezhil5555@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, 3, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-08 04:42:27', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cv16_institution_users`
--

CREATE TABLE `cv16_institution_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `institution_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_institution_users`
--

INSERT INTO `cv16_institution_users` (`id`, `institution_id`, `user_id`, `role`, `created_at`, `updated_at`) VALUES
(2, 2, 53, 'admin', '2017-02-20 22:22:48', '2017-02-20 22:22:48'),
(3, 3, 54, 'admin', '2017-02-21 08:34:40', '2017-02-21 08:34:40'),
(6, 6, 60, 'admin', '2017-02-28 11:37:57', '2017-02-28 11:37:57'),
(9, 3, 71, 'teacher', '2017-03-16 12:19:04', '2018-01-03 01:40:49'),
(10, 7, 74, 'admin', '2017-03-21 13:19:00', '2017-03-21 13:19:00'),
(11, 8, 75, 'admin', '2017-03-21 13:53:34', '2017-03-21 13:53:34'),
(12, 9, 76, 'admin', '2017-03-21 14:21:42', '2017-03-21 14:21:42'),
(13, 10, 81, 'admin', '2017-03-24 19:59:09', '2017-03-24 19:59:09'),
(14, 11, 82, 'admin', '2017-03-26 12:01:35', '2017-03-26 12:01:35'),
(15, 12, 84, 'admin', '2017-03-30 01:49:59', '2017-03-30 01:49:59'),
(16, 13, 85, 'admin', '2017-03-30 13:43:20', '2017-03-30 13:43:20'),
(17, 14, 86, 'admin', '2017-03-30 17:07:09', '2017-03-30 17:07:09'),
(18, 15, 87, 'admin', '2017-04-11 08:36:59', '2017-04-11 08:36:59'),
(19, 15, 88, 'teacher', '2017-04-11 10:36:49', '2017-04-11 10:36:49'),
(20, 15, 93, 'teacher', '2017-04-16 11:22:05', '2018-01-03 01:51:19'),
(21, 15, 95, 'teacher', '2017-04-16 11:29:33', '2017-04-16 11:29:33'),
(22, 15, 97, 'teacher', '2017-04-16 11:45:46', '2017-04-16 11:45:46'),
(23, 15, 100, 'teacher', '2017-05-24 11:13:11', '2017-05-24 11:13:11'),
(24, 16, 122, 'admin', '2017-07-27 12:04:01', '2017-07-27 12:04:01'),
(25, 17, 134, 'admin', '2017-08-24 11:46:13', '2017-08-24 11:46:13'),
(26, 3, 136, 'teacher', '2017-08-29 08:16:50', '2018-01-02 08:49:30'),
(27, 18, 160, 'admin', '2017-12-08 04:42:27', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cv16_jobs`
--

CREATE TABLE `cv16_jobs` (
  `id` int(10) UNSIGNED NOT NULL,
  `owner_id` int(10) UNSIGNED NOT NULL,
  `owner_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `industry_id` int(10) UNSIGNED DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `salary_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'annual',
  `salary_min` int(10) UNSIGNED DEFAULT NULL,
  `salary_max` int(10) UNSIGNED DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `skill_category_id` int(10) UNSIGNED DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_lat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_lng` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `additional` mediumtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `closed_date` timestamp NULL DEFAULT NULL,
  `successful_applicant_id` int(10) UNSIGNED DEFAULT NULL,
  `num_views` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_jobs`
--

INSERT INTO `cv16_jobs` (`id`, `owner_id`, `owner_type`, `user_id`, `industry_id`, `company_name`, `title`, `salary_type`, `salary_min`, `salary_max`, `end_date`, `skill_category_id`, `location`, `location_lat`, `location_lng`, `type`, `description`, `additional`, `created_at`, `updated_at`, `status`, `closed_date`, `successful_applicant_id`, `num_views`) VALUES
(4, 5, 'App\\Employer', 29, 31, NULL, 'Sia licensed security guard', 'hourly', 7, 9, '2017-03-31', NULL, 'Greater Manchester, United Kingdom', '53.45759549999999', '-2.157837699999959', 'Permanent', '<p>Sia licensed Staff required on a part time basis to work on Professional premiership football matches and high profile events. Mainly weekends and could be unsociable hours.</p>\r\n', '<p>Sia license required.</p>\r\n', '2016-11-10 22:36:00', '2017-11-17 23:55:04', 'active', NULL, NULL, 2),
(10, 6, 'App\\Employer', 32, 46, NULL, 'Delivery driver', 'hourly', 9, 12, '2017-11-29', 42, 'Blackburn, United Kingdom', '53.748575', '-2.487528999999995', 'Permanent', 'We are passionate about baking, are you passionate about driving? We are looking for delivery drivers on a part time basis who have a clean license and have a good knowledge of the area. Delivery times could be unsociable hours so flexibility is necessary. Must have your own vehicle and good social skills. We don\'t want our products which are baked with love delivered by an unsociable person so smiles are a must. ', '', '2017-01-16 14:04:22', '2017-09-11 10:06:33', 'active', NULL, NULL, 1),
(11, 8, 'App\\Employer', 34, 132, NULL, 'Admin support', 'annual', 12000, 16000, '2017-10-31', 23, 'Blackburn, United Kingdom', '53.748575', '-2.487528999999995', 'Permanent', '<p>We are looking for a dynamic person who performs administrative and office support activities for multiple supervisors. Duties will include fielding telephone calls, receiving and directing visitors, word processing, creating spreadsheets and presentations, and filing. Good communication skills and experience essential</p>', '', '2017-01-18 09:34:24', '2017-09-05 15:20:01', 'active', NULL, NULL, 5),
(12, 8, 'App\\Employer', 34, 132, NULL, 'Social Media Manager', 'annual', 15000, 17000, '2017-03-31', NULL, 'Blackburn, United Kingdom', '53.748575', '-2.487528999999995', 'Fixed Term Contract', 'Are you one who can inspire change? Are you able to engage people? That\'s what we want candidates to be able to do with employers. That\'s what type of person we want to be able to share that passion with others on our own social platforms. Someone exciting. Someone ambitious. And as our Social Media Manager, you’ll be part of it all. We’re talking new ideas, new thinking, and growth year on year. You’ll make it all possible. That’s why we’re growing our Marketing team. Investing in it too. And creating new opportunities to drive our success – as well as your own. \r\n\r\nAnd opportunities don’t come much bigger than having the chance to manage our entire social media presence. Facebook. Twitter. Wherever you think we need to be, whatever direction you think we need to take, you’ll be the one to make it happen. It’s a challenging role, as you’ll have lots of stakeholders to influence and bring on board, but, for the right person, this is a rare opportunity to shape the social strategy of one of the nation’s best-known insurance retailers – and then deliver it. ', 'We’ve already got some social media presence; we’re looking for someone who can build on that and take it to new places. That’s why it’s imperative that you’ve managed social campaigns across a range of channels before. In fact, you’ll know social inside out. You’ll keep us up to date with trends. And you’ll be as adept with strategy and stakeholder management as you are delivering the day-to-day content that gets people liking, following and sharing. \r\n\r\nYou’ll be part of an ambitious business that’s always thinking ahead – defining, planning and building for a better future. You’ll have your own rewards and benefits package, a personal development plan and lots of opportunities to do things the way you think they should be done. \r\n\r\nIn addition to your salary, you’ll receive a benefits package including 25 days holiday, pension, bonus potential, employee discounts and a choice of flexible benefits you can tailor to suit your needs.', '2017-01-26 11:23:59', '2017-02-24 17:14:10', 'active', NULL, NULL, 1),
(13, 8, 'App\\Employer', 34, 132, NULL, 'Sales co-ordinator London ', 'annual', 20000, 23000, '2017-11-30', 38, 'London, United Kingdom', '51.5073509', '-0.12775829999998223', 'Fixed Term Contract', 'We are looking for a dynamic sales person who can engage with recruitment firms and businesses and demonstrate the ability to market our services by being able to successfully explain the benefits of using CVVID.\r\n\r\nMain duties:\r\nTo be responsible for the day to day signing up of employers and recruitment agencies in the London and South region\r\n\r\nThe Role: \r\n- To deal with enquiries on the daily basis via email or over the phone\r\n- To visit employers and recruitment firms \r\n- To follow up quotations, orders and potential business opportunities\r\n- To manage relationships post sign up\r\n- To communicate effectively with others departments\r\n\r\n', 'The candidate:\r\n- Fluent in English– Essential\r\n- Previous experience in Sales or similar\r\n- Excellent communication and interpersonal skills\r\n- Able to work as part of a team as well as to take initiatives\r\n- Able to work under pressure in a fast pace environment\r\n- Proactive and dynamic personality\r\n- Computer literate (Excel, Word, Outlook…)\r\n- To be able to work independently \r\n- Able to drive and have their own transport\r\n\r\nThere will be a 6 month probation period.  Thereafter the salary can be renegotiated.', '2017-02-02 10:21:20', '2017-07-26 21:14:46', 'active', NULL, NULL, 0),
(14, 8, 'App\\Employer', 34, 132, NULL, 'Sales co-ordinator Midlands', 'annual', 20000, 23000, '2017-11-24', 38, 'Birmingham, United Kingdom', '52.48624299999999', '-1.8904009999999971', 'Fixed Term Contract', 'Main duties:\r\nTo be responsible for the day to day signing up of employers and recruitment agencies in the Midlands region\r\n\r\nThe Role: \r\n- To deal with enquiries on the daily basis via email or over the phone\r\n- To visit employers and recruitment firms \r\n- To follow up quotations, orders and potential business opportunities\r\n- To manage relationships post sign up\r\n- To communicate effectively with others departments\r\n\r\n', 'The candidate:\r\n- Fluent in English– Essential\r\n- Previous experience in Sales or similar\r\n- Excellent communication and interpersonal skills\r\n- Able to work as part of a team as well as to take initiatives\r\n- Able to work under pressure in a fast pace environment\r\n- Proactive and dynamic personality\r\n- Computer literate (Excel, Word, Outlook…)\r\n- To be able to work independently \r\n\r\nThere will be a 6 month probation period.  Thereafter the salary can be renegotiated.', '2017-02-02 11:07:10', '2017-09-11 17:40:44', 'active', NULL, NULL, 0),
(15, 8, 'App\\Employer', 34, 132, NULL, 'Sales co-ordinator Scotland', 'annual', 20000, 23000, '2017-10-31', 38, 'Scotland, United Kingdom', '56.49067119999999', '-4.2026458000000275', 'Fixed Term Contract', 'We are looking for a dynamic sales person who can engage with recruitment firms and businesses and demonstrate the ability to market our services by being able to successfully explain the benefits of using CVVID to recruit.\r\n\r\nMain duties:\r\nTo be responsible for the day to day signing up of employers and recruitment agencies in Scotland\r\n\r\nThe Role\r\n- To open up networks of opportunities and follow them up\r\n- To sign up businesses and recruitment firms to use CVVID\r\n- To deal with enquiries and follow them up\r\n- To visit employers and recruitment agencies\r\n- To manage and maintain relationships post sign up\r\n- To communicate effectively with the other departments\r\n\r\n', 'The candidate:\r\n- Fluent in English– Essential\r\n- Previous experience in Sales or similar\r\n- Excellent communication and interpersonal skills\r\n- Able to work as part of a team as well as to take initiatives\r\n- Able to work under pressure in a fast pace environment\r\n- Proactive and dynamic personality\r\n- Computer literate (Excel, Word, Outlook…)\r\n- To be able to work independently \r\n\r\nThere will be a 6 month probation period.  Thereafter the salary can be renegotiated.', '2017-02-02 11:14:39', '2017-07-26 21:16:36', 'active', NULL, NULL, 0),
(16, 8, 'App\\Employer', 34, 132, NULL, 'Sales co-ordinator North East', 'annual', 20000, 22000, '2017-05-31', 38, 'Newcastle upon Tyne, United Kingdom', '54.978252', '-1.6177800000000389', 'Fixed Term Contract', '<p>We are looking for a dynamic sales person who can engage with recruitment firms and businesses and demonstrate the ability to market our services by being able to successfully explain the benefits of using CVVID to recruit. Main duties: To be responsible for the day to day signing up of employers and recruitment agencies in the North East (Yorkshire and Humberside) The Role - To open up networks of opportunities and follow them up - To sign up businesses and recruitment firms to use CVVID - To deal with enquiries and follow them up - To visit employers and recruitment agencies - To manage and maintain relationships post sign up - To communicate effectively with the other departments</p>', '<p>The candidate: - Fluent in English&ndash; Essential - Previous experience in Sales or similar - Excellent communication and interpersonal skills - Able to work as part of a team as well as to take initiatives - Able to work under pressure in a fast pace environment - Proactive and dynamic personality - Computer literate (Excel, Word, Outlook&hellip;) - To be able to work independently There will be a 6 month probation period. Thereafter the salary can be renegotiated.</p>', '2017-02-02 11:17:40', '2017-05-20 12:41:25', 'active', NULL, NULL, 1),
(17, 8, 'App\\Employer', 34, 132, NULL, 'Sales co-ordinator North West', 'annual', 20000, 22000, '2017-10-31', 38, 'Manchester, United Kingdom', '53.4807593', '-2.2426305000000184', 'Fixed Term Contract', '<p>We are looking for a dynamic sales person who can engage with recruitment firms and businesses and demonstrate the ability to market our services by being able to successfully explain the benefits of using CVVID to recruit. Main duties: To be responsible for the day to day signing up of employers and recruitment agencies in the North West (Greater Manchester, Lancashire and Merseyside) The Role - To open up networks of opportunities and follow them up - To sign up businesses and recruitment firms to use CVVID - To deal with enquiries and follow them up - To visit employers and recruitment agencies - To manage and maintain relationships post sign up - To communicate effectively with the other departments</p>', '<p>The candidate: - Fluent in English– Essential - Previous experience in Sales or similar - Excellent communication and interpersonal skills - Able to work as part of a team as well as to take initiatives - Able to work under pressure in a fast pace environment - Proactive and dynamic personality - Computer literate (Excel, Word, Outlook…) - To be able to work independently There will be a 6 month probation period. Thereafter the salary can be renegotiated.</p>', '2017-02-02 11:20:59', '2017-07-31 11:28:18', 'active', NULL, NULL, 2),
(18, 8, 'App\\Employer', 34, 132, NULL, 'Schools Co-ordinator', 'annual', 25000, 27000, '2017-10-31', 38, 'Lancashire, United Kingdom', '53.7632254', '-2.7044051999999965', 'Fixed Term Contract', 'We are looking for an experiences sales person and/or someone who has worked in the education sector who can engage with schools and colleges to help them get their students with their careers education with employability. \r\nThe candidate must understand the dynamics of the education sector and demonstrate the ability to market our services by being able to successfully explain the benefits of using CVVID. \r\n\r\nMain duties: \r\nTo be responsible for the day to day signing up of schools and colleges in the London and South region \r\n\r\nThe Role: \r\n- To deal with enquiries on the daily basis via email or over the phone \r\n- To visit schools and colleges to secure them for CVVID\r\n- To put an action plan in place and execute it \r\n- To follow up quotations, orders and potential business opportunities \r\n- To manage relationships post sign up \r\n- To communicate effectively with others departments\r\n', 'The candidate:\r\n- Fluent in English– Essential\r\n- Previous experience in Sales and the education sector\r\n- Excellent communication and interpersonal skills\r\n- Able to work as part of a team as well as to take initiatives\r\n- Able to work under pressure in a fast pace environment\r\n- Proactive and dynamic personality\r\n- Computer literate (Excel, Word, Outlook…)\r\n- To be able to work independently \r\n\r\nThere will be a 6 month probation period.  Thereafter the salary can be renegotiated.', '2017-02-02 11:30:19', '2017-07-31 12:08:29', 'active', NULL, NULL, 2),
(21, 16, 'App\\Employer', 47, 76, NULL, 'Mountain Leaders and Guides', 'hourly', 9, 15, '2017-11-17', NULL, 'Cardiff, United Kingdom', '51.48158100000001', '-3.1790899999999738', 'Fixed Term Contract', '<p>We are looking for contractors who are qualified, reliable and flexible that are able to represent Nova Adventure Challenges. We are recruiting mountain leaders and guides who have the wealth of experience and also be able to open new opportunities.</p>', '<p>Candidates must have: - Experience - Passion and love for the outdoors - Great interpersonal skills - MIA or MIC certification or the equivalent - Be able to work flexible and unsociable working hours</p>', '2017-02-03 09:38:46', '2017-09-11 17:33:10', 'active', NULL, NULL, 1),
(22, 6, 'App\\Employer', 32, 54, NULL, 'Graphic Designer', 'hourly', 10, 13, '2017-11-15', 17, 'Blackburn, United Kingdom', '53.748575', '-2.487528999999995', 'Fixed Term Contract', '<p>We are looking for a graphic designer who can work on designing new campaigns to push on social media to attract new clients and highlight and showcase our existing work</p>', '<p>We are happy to consider someone who is fairly new in graphic design but must be able to demonstrate a good creative mind and ambitious in achieving goals. We need to them to be self starters and not someone who is just going to work on direction provided but rather uses initiative.</p>', '2017-02-24 16:02:11', '2017-09-05 15:21:50', 'active', NULL, NULL, 0),
(23, 7, 'App\\Employer', 33, 99, NULL, 'Web Designer', 'annual', 26000, 29000, '2017-04-26', 4, 'Altham, United Kingdom', '53.7890755', '-2.347248499999978', 'Temporary', 'Test', 'Required Test', '2017-02-24 16:05:36', '2017-03-08 14:13:10', 'active', NULL, NULL, 1),
(24, 7, 'App\\Employer', 33, 99, NULL, 'Web Developer', 'annual', 1000, 2000, '2017-04-06', 4, 'Altham, United Kingdom', '53.7890755', '-2.347248499999978', 'Temporary', 'Testing', 'Testing', '2018-01-21 07:37:35', '2017-02-24 16:37:35', 'active', NULL, NULL, 0),
(25, 10, 'App\\Employer', 2, 54, NULL, 'Senior Designer', 'hourly', 10, 15, '2017-11-15', 17, 'Blackburn, United Kingdom', '53.748575', '-2.487528999999995', 'Permanent', '<p>We are looking for a graphic designer to work from our office with at least 5 years experience. You will be expected to overlook, mentor and manage the junior designers. &nbsp;The successful applicant will start within 2 weeks of the deadline.&nbsp;</p>\r\n<p>At Print Hub Design we are committed to delivering the best service as well as new, innovative, fresh &amp; original designs to our clients.&nbsp;</p>', '<p>Ability to work with Illustrator, In Design and other household names in graphic design.&nbsp;</p>\r\n<p>5 years experience is also required</p>', '2017-04-19 10:26:58', '2017-09-11 17:49:23', 'active', NULL, NULL, 0),
(26, 14, 'App\\Employer', 2, 39, NULL, 'Business Development Manager', 'hourly', 15, 15, '2017-10-31', NULL, 'Cardiff, United Kingdom', '51.48158100000001', '-3.1790899999999738', 'Permanent', '<p>We are looking for a competent BDM for our growing business. We are recruiting a leader, who can walk into the role hands on, so knowledge of the technical side is mandatory.&nbsp;</p>\r\n<p>Working hours can be long, and unsociable hours, but it can also be enjoyable. Being a team player, and being able to think on your feet is part and parcel of the role.&nbsp;</p>\r\n<p>&nbsp;</p>', '<p>Full driving license is required, vehicle will be provided</p>\r\n<p>Techincally sound with AV set ups</p>\r\n<p>Sales and/or management history</p>\r\n<p>Multi lingual is beneficial but not mandatory</p>', '2017-04-19 10:33:47', '2017-09-11 17:49:49', 'active', NULL, NULL, 0),
(28, 16, 'App\\Employer', 47, 40, NULL, 'Admin support', 'annual', 16000, 16500, '2017-11-17', 23, 'Cardiff, United Kingdom', '51.48158100000001', '-3.1790899999999738', 'Permanent', 'We are looking for a dynamic office support staff who can hold the fort, handle calls, staff diaries and other clerical duties. 3 Months probation period. Good incentive packages offered. Start immediately.', 'Good communication skills\r\nOrganisation skills\r\n', '2017-09-11 17:46:35', '2017-09-11 17:46:35', 'active', NULL, NULL, 0),
(29, 8, 'App\\Employer', 34, 132, NULL, 'PHP Developer', 'annual', 27000, 30000, '2017-11-30', 4, 'Blackburn, United Kingdom', '53.748575', '-2.487528999999995', 'Permanent', 'PHP DEVELOPER \r\n\r\nTHE ROLE\r\nIntrigued by building business-focused software with modern techniques? Passionate about coding? Then this might just be the opportunity you are looking for! We\'re currently seeking a PHP Developer to join our team in Lancashire.  With all components of the business being brought in house, You will lead the in house development team. Focused on maintaining and enhancing our CMS. Our team aims at delivering the best user experience in the industry, both for our customers and also for our internal users. We\'re not afraid to experiment as our goal is to aim high!\r\n\r\nRESPONSIBILITIES\r\n\r\nAs a PHP Developer, you will be responsible for software development and testing. You will be responsible for managing back-end, PHP services and the interchange of data between the server and the users. Your primary focus will be the maintenance and development of all server-side logic, definition and maintenance of the central database. You will also be responsible for integrating the front-end elements built, into the application. You will be contributing directly to the success of our products by materializing our product teams\' visions into a tangible user experience. You will participate in sprint planning, task estimation, development, testing, code reviews, bug fixing, deploying great software, implementing new features as well as maintaining and improving the existing functionality\r\n\r\nWHAT WE OFFER\r\nWe offer a fun and challenging role in a helpful and dynamic environment. The opportunity to join an innovative and fast paced business that aims to be the best in the industry.  With this comes opportunities to excel and progress, as well as a good incentive package. We strive to be the best in whatever field we choose to play on, and we make sure to have fun along the way. Our motto is \'You at your best\', we want our staff to also live by it. ', '.REQUIREMENTS\r\n\r\nBe able to demonstrate a thorough knowledge of OO/MVC PHP;\r\nBe able to demonstrate a thorough knowledge of MySQL;\r\nBe able to demonstrate a thorough knowledge of JavaScript/jQuery;\r\nBe able to demonstrate a knowledge of HTML/CSS;\r\nFamiliarity with LAMP stack, including Linux CLI;\r\nBe skilled with testing software;\r\nBe able to demonstrate use of RESTful communication (XML/JSON);\r\nBe able to demonstrate use of SOAP communication;\r\nHave good interpersonal and communication skills.We see it as positive if you also haveReal world use of Drupal 7.x;\r\nGood documentation habits;\r\nSolid code practices: OO, patterns.\r\n\r\n', '2017-09-12 07:04:55', '2017-09-12 07:04:55', 'active', NULL, NULL, 0),
(30, 3, 'employer', 2, 39, NULL, 'test job', 'annual', 28, 52, '2018-01-18', 7, 'Glasgow G21, United Kingdom', '55.8804501', '-4.217650300000059', 'Permanent', '<p>aewearararrarrar</p>', '<p>aewearararrarraraewearararrarraraewearararrarraraewearararrarraraewearararrarrar</p>', '2018-01-17 05:34:33', '0000-00-00 00:00:00', 'active', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cv16_job_applications`
--

CREATE TABLE `cv16_job_applications` (
  `id` int(10) UNSIGNED NOT NULL,
  `job_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `message` text COLLATE utf8_unicode_ci,
  `status` int(11) NOT NULL DEFAULT '0',
  `interview_date` timestamp NULL DEFAULT NULL,
  `accepted_date` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_job_applications`
--

INSERT INTO `cv16_job_applications` (`id`, `job_id`, `user_id`, `message`, `status`, `interview_date`, `accepted_date`, `created_at`, `updated_at`, `deleted_at`) VALUES
(6, 10, 40, 'aaaaaaaaaa', 4, NULL, '2017-11-22 09:01:47', '2017-01-26 16:49:44', '2017-01-26 16:49:44', NULL),
(7, 12, 48, NULL, 1, NULL, NULL, '2017-02-10 21:20:56', '2017-02-24 17:15:43', NULL),
(8, 23, 3, 'test', 2, '0000-00-00 00:00:00', NULL, '2017-03-08 14:13:13', '2017-03-08 14:14:30', NULL),
(9, 22, 91, 'ssfsfsfsfsfsfsf', 3, '0000-00-00 00:00:00', NULL, '2017-05-20 12:41:29', '2017-05-20 12:41:29', NULL),
(11, 24, 3, NULL, 0, NULL, NULL, '2018-01-22 02:55:17', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cv16_job_skill`
--

CREATE TABLE `cv16_job_skill` (
  `job_id` int(10) UNSIGNED NOT NULL,
  `skill_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_job_skill`
--

INSERT INTO `cv16_job_skill` (`job_id`, `skill_id`) VALUES
(11, 154),
(11, 159),
(11, 160),
(12, 310),
(13, 244),
(13, 357),
(13, 356),
(14, 357),
(14, 356),
(15, 357),
(15, 356),
(16, 357),
(16, 356),
(17, 357),
(17, 356),
(18, 357),
(18, 356),
(21, 234),
(22, 130),
(23, 34),
(24, 331),
(24, 34),
(25, 130),
(26, 338),
(26, 343),
(26, 339),
(26, 341),
(26, 337),
(10, 309),
(28, 154),
(28, 155),
(28, 160),
(28, 161),
(29, 33),
(29, 333),
(29, 34),
(4, 337),
(30, 60);

-- --------------------------------------------------------

--
-- Table structure for table `cv16_job_views`
--

CREATE TABLE `cv16_job_views` (
  `id` int(10) UNSIGNED NOT NULL,
  `job_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model_id` int(10) UNSIGNED NOT NULL,
  `view_count` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_job_views`
--

INSERT INTO `cv16_job_views` (`id`, `job_id`, `model_type`, `model_id`, `view_count`, `created_at`, `updated_at`) VALUES
(1, 11, 'App\\Users\\Candidate', 40, 1, '2017-02-15 19:57:31', '2017-02-15 19:57:31'),
(2, 21, 'App\\Users\\Candidate', 3, 1, '2017-02-17 10:11:17', '2017-02-17 10:11:17'),
(3, 17, 'App\\Users\\Candidate', 3, 1, '2017-02-17 10:11:41', '2017-02-17 10:11:41'),
(4, 11, 'App\\Users\\Employer', 33, 1, '2017-02-21 14:17:51', '2017-02-21 14:17:51'),
(5, 4, 'App\\Users\\Employer', 33, 2, '2017-02-23 14:05:15', '2017-02-23 14:05:15'),
(6, 12, 'App\\Users\\Candidate', 40, 1, '2017-02-24 17:14:10', '2017-02-24 17:14:10'),
(7, 23, 'App\\Users\\Candidate', 3, 1, '2017-03-08 14:13:10', '2017-03-08 14:13:10'),
(8, 16, 'App\\Users\\Candidate', 91, 1, '2017-05-20 12:41:25', '2017-05-20 12:41:25'),
(9, 17, 'App\\Users\\Candidate', 90, 1, '2017-07-31 11:28:18', '2017-07-31 11:28:18'),
(10, 18, 'App\\Users\\Candidate', 90, 2, '2017-07-31 11:30:16', '2017-07-31 12:08:29'),
(11, 11, 'App\\Users\\Candidate', 90, 2, '2017-07-31 11:36:59', '2017-07-31 12:07:11'),
(12, 11, 'App\\Users\\Candidate', 30, 1, '2017-09-05 15:20:01', '2017-09-05 15:20:01'),
(13, 10, 'App\\Users\\Candidate', 138, 1, '2017-09-11 10:06:33', '2017-09-11 10:06:33');

-- --------------------------------------------------------

--
-- Table structure for table `cv16_media`
--

CREATE TABLE `cv16_media` (
  `id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `collection_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `disk` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `size` int(11) NOT NULL,
  `manipulations` text COLLATE utf8_unicode_ci NOT NULL,
  `custom_properties` text COLLATE utf8_unicode_ci NOT NULL,
  `order_column` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_media`
--

INSERT INTO `cv16_media` (`id`, `model_type`, `model_id`, `collection_name`, `name`, `file_name`, `disk`, `size`, `manipulations`, `custom_properties`, `order_column`, `created_at`, `updated_at`) VALUES
(2, 'App\\Site', 1, 'sample', 'download-1-thumb', 'download-1-thumb.jpg', 'media', 1557035, '[]', '[]', 2, '2016-03-15 08:35:46', '2016-03-15 08:35:46'),
(3, 'App\\Site', 1, 'sample', 'download-10-thumb', 'download-10-thumb.jpg', 'media', 1286279, '[]', '[]', 3, '2016-03-15 08:35:47', '2016-03-15 08:35:47'),
(4, 'App\\Site', 1, 'sample', 'download-11-thumb', 'download-11-thumb.jpg', 'media', 1874398, '[]', '[]', 4, '2016-03-15 08:35:48', '2016-03-15 08:35:48'),
(5, 'App\\Site', 1, 'sample', 'download-12-thumb', 'download-12-thumb.jpg', 'media', 1673162, '[]', '[]', 5, '2016-03-15 08:35:48', '2016-03-15 08:35:48'),
(6, 'App\\Site', 1, 'sample', 'Penguins', 'Penguins.jpg', 'media', 777835, '[]', '[]', 6, '2016-03-15 08:35:49', '2017-11-05 12:40:23'),
(7, 'App\\Site', 1, 'sample', 'download-14-thumb', 'download-14-thumb.jpg', 'media', 2270483, '[]', '[]', 7, '2016-03-15 08:35:50', '2016-03-15 08:35:50'),
(8, 'App\\Site', 1, 'sample', 'download-15-thumb', 'download-15-thumb.jpg', 'media', 2273778, '[]', '[]', 8, '2016-03-15 08:35:50', '2016-03-15 08:35:50'),
(9, 'App\\Site', 1, 'sample', 'download-16-thumb', 'download-16-thumb.jpg', 'media', 1681557, '[]', '[]', 9, '2016-03-15 08:35:51', '2016-03-15 08:35:51'),
(10, 'App\\Site', 1, 'sample', 'download-17-thumb', 'download-17-thumb.jpg', 'media', 1554384, '[]', '[]', 10, '2016-03-15 08:35:52', '2016-03-15 08:35:52'),
(11, 'App\\Site', 1, 'sample', 'download-18-thumb', 'download-18-thumb.jpg', 'media', 1879573, '[]', '[]', 11, '2016-03-15 08:35:52', '2016-03-15 08:35:52'),
(12, 'App\\Site', 1, 'sample', 'download-19-thumb', 'download-19-thumb.jpg', 'media', 1038355, '[]', '[]', 12, '2016-03-15 08:35:53', '2016-03-15 08:35:53'),
(13, 'App\\Site', 1, 'sample', 'download-2-thumb', 'download-2-thumb.jpg', 'media', 1850473, '[]', '[]', 13, '2016-03-15 08:35:53', '2016-03-15 08:35:53'),
(14, 'App\\Site', 1, 'sample', 'perfumes', 'perfumes.jpg', 'media', 401605, '[]', '[]', 14, '2016-03-15 08:35:54', '2017-12-05 07:59:45'),
(15, 'App\\Site', 1, 'sample', 'download-21-thumb', 'download-21-thumb.jpg', 'media', 1023421, '[]', '[]', 15, '2016-03-15 08:35:54', '2016-03-15 08:35:54'),
(16, 'App\\Site', 1, 'sample', 'download-22-thumb', 'download-22-thumb.jpg', 'media', 1502116, '[]', '[]', 16, '2016-03-15 08:35:55', '2016-03-15 08:35:55'),
(17, 'App\\Site', 1, 'sample', 'download-23-thumb', 'download-23-thumb.jpg', 'media', 1189368, '[]', '[]', 17, '2016-03-15 08:35:55', '2016-03-15 08:35:55'),
(18, 'App\\Site', 1, 'sample', 'download-24-thumb', 'download-24-thumb.jpg', 'media', 1101367, '[]', '[]', 18, '2016-03-15 08:35:56', '2016-03-15 08:35:56'),
(19, 'App\\Site', 1, 'sample', 'download-25-thumb', 'download-25-thumb.jpg', 'media', 1583085, '[]', '[]', 19, '2016-03-15 08:35:56', '2016-03-15 08:35:56'),
(20, 'App\\Site', 1, 'sample', 'download-26-thumb', 'download-26-thumb.jpg', 'media', 1621046, '[]', '[]', 20, '2016-03-15 08:35:57', '2016-03-15 08:35:57'),
(21, 'App\\Site', 1, 'sample', 'download-27-thumb', 'download-27-thumb.jpg', 'media', 1892897, '[]', '[]', 21, '2016-03-15 08:35:58', '2016-03-15 08:35:58'),
(22, 'App\\Site', 1, 'sample', 'download-28-thumb', 'download-28-thumb.jpg', 'media', 668682, '[]', '[]', 22, '2016-03-15 08:35:58', '2016-03-15 08:35:58'),
(23, 'App\\Site', 1, 'sample', 'download-29-thumb', 'download-29-thumb.jpg', 'media', 1511452, '[]', '[]', 23, '2016-03-15 08:35:59', '2016-03-15 08:35:59'),
(24, 'App\\Site', 1, 'sample', 'download-3-thumb', 'download-3-thumb.jpg', 'media', 1115548, '[]', '[]', 24, '2016-03-15 08:35:59', '2016-03-15 08:35:59'),
(25, 'App\\Site', 1, 'sample', 'download-30-thumb', 'download-30-thumb.jpg', 'media', 2654127, '[]', '[]', 25, '2016-03-15 08:36:00', '2016-03-15 08:36:00'),
(26, 'App\\Site', 1, 'sample', 'download-31-thumb', 'download-31-thumb.jpg', 'media', 4456404, '[]', '[]', 26, '2016-03-15 08:36:01', '2016-03-15 08:36:01'),
(27, 'App\\Site', 1, 'sample', 'download-32-thumb', 'download-32-thumb.jpg', 'media', 1424596, '[]', '[]', 27, '2016-03-15 08:36:02', '2016-03-15 08:36:02'),
(28, 'App\\Site', 1, 'sample', 'download-4-thumb', 'download-4-thumb.jpg', 'media', 2720898, '[]', '[]', 28, '2016-03-15 08:36:02', '2016-03-15 08:36:02'),
(29, 'App\\Site', 1, 'sample', 'download-5-thumb', 'download-5-thumb.jpg', 'media', 1283313, '[]', '[]', 29, '2016-03-15 08:36:03', '2016-03-15 08:36:03'),
(30, 'App\\Site', 1, 'sample', 'download-6-thumb', 'download-6-thumb.jpg', 'media', 2582986, '[]', '[]', 30, '2016-03-15 08:36:04', '2016-03-15 08:36:04'),
(31, 'App\\Site', 1, 'sample', 'download-7-thumb', 'download-7-thumb.jpg', 'media', 1929168, '[]', '[]', 31, '2016-03-15 08:36:05', '2016-03-15 08:36:05'),
(32, 'App\\Site', 1, 'sample', 'download-8-thumb', 'download-8-thumb.jpg', 'media', 3484561, '[]', '[]', 32, '2016-03-15 08:36:05', '2016-03-15 08:36:05'),
(33, 'App\\Site', 1, 'sample', 'download-9-thumb', 'download-9-thumb.jpg', 'media', 1623733, '[]', '[]', 33, '2016-03-15 08:36:06', '2016-03-15 08:36:06'),
(34, 'App\\Site', 1, 'sample', 'download-thumb', 'download-thumb.jpg', 'media', 1377260, '[]', '[]', 34, '2016-03-15 08:36:07', '2016-03-15 08:36:07'),
(75, 'App\\Site', 1, 'photos', 'slide-3', 'slide-3.jpg', 'media', 220118, '[]', '[]', 36, '2016-04-27 10:34:33', '2016-04-27 10:34:33'),
(76, 'App\\Site', 1, 'photos', 'slide-2', 'slide-2.jpg', 'media', 178019, '[]', '[]', 37, '2016-04-27 10:34:39', '2016-04-27 10:34:39'),
(77, 'App\\Site', 1, 'photos', 'slide-1', 'slide-1.jpg', 'media', 158773, '[]', '[]', 38, '2016-04-27 10:34:42', '2016-04-27 10:34:42'),
(78, 'App\\Site', 1, 'photos', 'slide-6', 'slide-6.jpg', 'media', 187831, '[]', '[]', 39, '2016-04-27 12:05:08', '2016-04-27 12:05:08'),
(79, 'App\\Site', 1, 'photos', 'slide-9', 'slide-9.jpg', 'media', 231840, '[]', '[]', 40, '2016-04-27 12:05:32', '2016-04-27 12:05:32'),
(80, 'App\\Site', 1, 'photos', 'slide-7', 'slide-7.jpg', 'media', 254048, '[]', '[]', 41, '2016-04-27 12:40:39', '2016-04-27 12:40:39'),
(81, 'App\\Site', 1, 'photos', 'slide-4', 'slide-4.jpg', 'media', 165872, '[]', '[]', 42, '2016-04-27 12:44:33', '2016-04-27 12:44:33'),
(82, 'App\\Site', 1, 'photos', 'slide-5', 'slide-5.jpg', 'media', 215026, '[]', '[]', 43, '2016-04-27 12:44:59', '2016-04-27 12:44:59'),
(83, 'App\\Site', 1, 'photos', 'slide-12', 'slide-12.jpg', 'media', 94940, '[]', '[]', 44, '2016-04-27 12:45:51', '2016-04-27 12:45:51'),
(84, 'App\\Site', 1, 'photos', 'slide-8', 'slide-8.jpg', 'media', 141644, '[]', '[]', 45, '2016-04-27 14:48:04', '2016-04-27 14:48:04'),
(85, 'App\\Site', 1, 'photos', 'slide2', 'slide2.jpg', 'media', 1268457, '[]', '[]', 46, '2016-04-27 14:50:50', '2016-04-27 14:50:50'),
(94, 'App\\Site', 1, 'photos', 'sample1', 'sample1.png', 'media', 1194592, '[]', '[]', 47, '2016-05-03 13:48:15', '2016-05-03 13:48:15'),
(95, 'App\\Site', 1, 'photos', 'sample2', 'sample2.png', 'media', 788868, '[]', '[]', 47, '2016-05-03 13:48:15', '2016-05-03 13:48:15'),
(96, 'App\\Site', 1, 'photos', 'shutterstock_164285141', 'shutterstock_164285141.jpg', 'media', 211608, '[]', '[]', 48, '2016-05-03 14:19:50', '2016-05-03 14:19:50'),
(97, 'App\\Users\\Candidate', 3, 'documents', 'isotope_commercial_organization_license', 'isotope_commercial_organization_license.pdf', 'documents', 134905, '[]', '[]', 49, '2016-05-06 09:02:30', '2016-05-06 09:02:30'),
(98, 'App\\Videos\\Video', 2, 'video', 'FVn4JnKnmhNsEv10', 'FVn4JnKnmhNsEv10.jpg', 'media', 45921, '[]', '[]', 50, '2016-06-09 10:04:45', '2016-06-09 10:04:45'),
(99, 'App\\Videos\\Video', 3, 'video', 'U9PAH6n63fN1AAOY', 'U9PAH6n63fN1AAOY.jpg', 'media', 60662, '[]', '[]', 51, '2016-06-09 10:08:54', '2016-06-09 10:08:54'),
(100, 'App\\Videos\\Video', 4, 'video', '1Fs1QkTh4Nk6vj4z', '1Fs1QkTh4Nk6vj4z.jpg', 'media', 52656, '[]', '[]', 52, '2016-06-09 10:15:30', '2016-06-09 10:15:30'),
(101, 'App\\Videos\\Video', 5, 'video', 'K1C6yxZFlkS7G1Ru', 'K1C6yxZFlkS7G1Ru.jpg', 'media', 49347, '[]', '[]', 53, '2016-06-09 10:24:05', '2016-06-09 10:24:05'),
(102, 'App\\Videos\\Video', 6, 'video', 'yDlNsRdpGELLhmyf', 'yDlNsRdpGELLhmyf.jpg', 'media', 58204, '[]', '[]', 54, '2016-06-09 13:25:03', '2016-06-09 13:25:03'),
(103, 'App\\Videos\\Video', 7, 'video', 'DzKRvLL7uq4S4dwL', 'DzKRvLL7uq4S4dwL.jpg', 'media', 30064, '[]', '[]', 55, '2016-06-09 13:25:34', '2016-06-09 13:25:34'),
(104, 'App\\Videos\\Video', 8, 'video', 'XAfCD2Cb2tKN3LO2', 'XAfCD2Cb2tKN3LO2.jpg', 'media', 39077, '[]', '[]', 56, '2016-06-09 13:25:59', '2016-06-09 13:25:59'),
(105, 'App\\Videos\\Video', 9, 'video', 'pz6sDQn3XVyapzeA', 'pz6sDQn3XVyapzeA.jpg', 'media', 64764, '[]', '[]', 57, '2016-06-09 13:26:14', '2016-06-09 13:26:14'),
(106, 'App\\Videos\\Video', 10, 'video', 'nkBEI9svr3hzfJJ8', 'nkBEI9svr3hzfJJ8.jpg', 'media', 49883, '[]', '[]', 58, '2016-06-09 13:41:41', '2016-06-09 13:41:41'),
(107, 'App\\Videos\\Video', 11, 'video', '2O0JlQYUFXhWC4LH', '2O0JlQYUFXhWC4LH.jpg', 'media', 49898, '[]', '[]', 59, '2016-06-09 13:42:38', '2016-06-09 13:42:38'),
(108, 'App\\Videos\\Video', 12, 'video', 'RfDrqcxV13Bt7RXO', 'RfDrqcxV13Bt7RXO.jpg', 'media', 50284, '[]', '[]', 60, '2016-06-09 13:43:01', '2016-06-09 13:43:01'),
(109, 'App\\Videos\\Video', 13, 'video', 'QXneviRlKYjh6l1p', 'QXneviRlKYjh6l1p.jpg', 'media', 49415, '[]', '[]', 61, '2016-06-09 13:43:16', '2016-06-09 13:43:16'),
(110, 'App\\Videos\\Video', 14, 'video', 'AhK0aLVaug5AXugo', 'AhK0aLVaug5AXugo.jpg', 'media', 50660, '[]', '[]', 62, '2016-06-09 13:43:31', '2016-06-09 13:43:31'),
(111, 'App\\Videos\\Video', 15, 'video', 'SXEG1mbYSvu95HSL', 'SXEG1mbYSvu95HSL.jpg', 'media', 51675, '[]', '[]', 63, '2016-06-09 13:43:49', '2016-06-09 13:43:49'),
(122, 'App\\Employer', 5, 'photos', 'IMG_20150905_234923', 'IMG_20150905_234923.jpg', 'media', 1815807, '[]', '[]', 69, '2016-11-10 22:21:08', '2016-11-10 22:21:08'),
(123, 'App\\Employer', 5, 'photos', 'FB_IMG_1435248790656', 'FB_IMG_1435248790656.jpg', 'media', 8962, '[]', '[]', 70, '2016-11-10 22:25:34', '2016-11-10 22:25:34'),
(124, 'App\\Employer', 8, 'photos', 'cvvid', 'cvvid.jpg', 'media', 8025, '[]', '[]', 71, '2017-01-18 09:35:35', '2017-01-18 09:35:35'),
(125, 'App\\Employer', 6, 'photos', 'MKB', 'MKB.jpg', 'media', 51796, '[]', '[]', 72, '2017-01-26 11:33:09', '2017-01-26 11:33:09'),
(126, 'App\\Users\\Candidate', 30, 'photos', 'IMG_5182', 'IMG_5182.JPG', 'media', 365776, '[]', '[]', 73, '2017-01-28 18:09:24', '2017-01-28 18:09:24'),
(127, 'App\\Videos\\Video', 21, 'video', 'OorgusCwT8BaeGMh', 'OorgusCwT8BaeGMh.jpg', 'media', 103913, '[]', '[]', 74, '2017-02-01 12:40:05', '2017-02-01 12:40:05'),
(128, 'App\\Employer', 14, 'photos', 'marble management logo', 'marble management logo.png', 'media', 27970, '[]', '[]', 75, '2017-02-01 19:01:24', '2017-02-01 19:01:24'),
(129, 'App\\Videos\\Video', 22, 'video', 'v1VzgSTvrVvToWE6', 'v1VzgSTvrVvToWE6.jpg', 'media', 135886, '[]', '[]', 76, '2017-02-03 11:30:09', '2017-02-03 11:30:09'),
(130, 'App\\Site', 1, 'photos', 'tighs', 'tighs.jpg', 'media', 2368, '[]', '[]', 77, '2017-03-03 10:00:52', '2017-03-03 10:00:52'),
(131, 'App\\Employer', 6, 'photos', 'MB signature', 'MB signature.png', 'media', 911, '[]', '[]', 78, '2017-03-17 09:12:49', '2017-03-17 09:12:49'),
(132, 'App\\Education\\Institution', 3, 'photos', 'logo', 'logo.png', 'media', 27960, '[]', '[]', 79, '2017-03-17 09:17:52', '2017-03-17 09:17:52'),
(133, 'App\\Education\\Institution', 3, 'photos', 'IMG_5758', 'IMG_5758.PNG', 'media', 4576, '[]', '[]', 80, '2017-03-17 09:21:38', '2017-03-17 09:21:38'),
(134, 'App\\Videos\\Video', 23, 'video', 'AXXJxobX8ATtTUi4', 'AXXJxobX8ATtTUi4.jpg', 'media', 194415, '[]', '[]', 81, '2017-03-17 10:55:04', '2017-03-17 10:55:04'),
(135, 'App\\Videos\\Video', 24, 'video', 'Vku0XMK2w9OtoWKv', 'Vku0XMK2w9OtoWKv.jpg', 'media', 185224, '[]', '[]', 82, '2017-03-17 11:30:04', '2017-03-17 11:30:04'),
(142, 'App\\Users\\Candidate', 92, 'photos', 'student 2', 'student 2.jpg', 'media', 53822, '[]', '[]', 89, '2017-04-16 11:09:13', '2017-04-16 11:09:13'),
(143, 'App\\Site', 1, 'photos', 'pHD', 'pHD.jpeg', 'media', 20158, '[]', '[]', 90, '2017-04-19 10:37:31', '2017-04-19 10:37:31'),
(144, 'App\\Site', 1, 'photos', 'MM', 'MM.png', 'media', 90439, '[]', '[]', 91, '2017-04-19 10:38:40', '2017-04-19 10:38:40'),
(148, 'App\\Videos\\Video', 30, 'video', 'Rzk1WHboqVRXB0B2', 'Rzk1WHboqVRXB0B2.jpg', 'media', 229936, '[]', '[]', 95, '2017-05-22 09:05:04', '2017-05-22 09:05:04'),
(149, 'App\\Videos\\Video', 31, 'video', 'C8mHLSHSacbX003d', 'C8mHLSHSacbX003d.jpg', 'media', 142325, '[]', '[]', 96, '2017-05-22 09:10:04', '2017-05-22 09:10:04'),
(150, 'App\\Videos\\Video', 32, 'video', 'hDQBoqqC94oD497r', 'hDQBoqqC94oD497r.jpg', 'media', 168674, '[]', '[]', 97, '2017-05-22 09:15:04', '2017-05-22 09:15:04'),
(154, 'App\\Videos\\Video', 37, 'video', 'zEp9tmik0gPqwsc7', 'zEp9tmik0gPqwsc7.jpg', 'media', 235310, '[]', '[]', 99, '2017-06-07 15:35:14', '2017-06-07 15:35:14'),
(156, 'App\\Videos\\Video', 38, 'video', 'lLtZemk32Pfd5xCH', 'lLtZemk32Pfd5xCH.jpg', 'media', 270369, '[]', '[]', 100, '2017-06-08 06:45:14', '2017-06-08 06:45:14'),
(163, 'App\\Videos\\Video', 40, 'video', 'DUAC53LgI4XYHA7W', 'DUAC53LgI4XYHA7W.jpg', 'media', 131037, '[]', '[]', 103, '2017-07-12 08:00:04', '2017-07-12 08:00:04'),
(169, 'App\\Videos\\Video', 45, 'video', 'QZHprvK0AraosNzN', 'QZHprvK0AraosNzN.jpg', 'media', 143479, '[]', '[]', 107, '2017-07-12 08:15:06', '2017-07-12 08:15:06'),
(172, 'App\\Videos\\Video', 49, 'video', 'G6qPl9hLIDJb0BaS', 'G6qPl9hLIDJb0BaS.jpg', 'media', 143041, '[]', '[]', 109, '2017-07-12 09:45:04', '2017-07-12 09:45:04'),
(174, 'App\\Videos\\Video', 50, 'video', '4WeC1sRavMlcjLBz', '4WeC1sRavMlcjLBz.jpg', 'media', 143978, '[]', '[]', 111, '2017-07-12 09:50:04', '2017-07-12 09:50:04'),
(175, 'App\\Videos\\Video', 51, 'video', 'h6YqnudOLbXF4w7N', 'h6YqnudOLbXF4w7N.jpg', 'media', 154086, '[]', '[]', 112, '2017-07-12 09:55:04', '2017-07-12 09:55:04'),
(177, 'App\\Videos\\Video', 54, 'video', 'BprPm4BtirgpaGZv', 'BprPm4BtirgpaGZv.jpg', 'media', 288220, '[]', '[]', 113, '2017-07-13 06:15:04', '2017-07-13 06:15:04'),
(179, 'App\\Videos\\Video', 55, 'video', 'DUiC1ge6OHbvAFr7', 'DUiC1ge6OHbvAFr7.jpg', 'media', 92364, '[]', '[]', 114, '2017-07-27 06:55:03', '2017-07-27 06:55:03'),
(180, 'App\\Employer', 16, 'photos', 'marble', 'marble.png', 'media', 90439, '[]', '[]', 115, '2017-09-11 17:42:24', '2017-09-11 17:42:24'),
(185, 'App\\Videos\\Video', 58, 'video', 'Tusdxo5qVKIcA2V3', 'Tusdxo5qVKIcA2V3.jpg', 'media', 215922, '[]', '[]', 118, '2017-09-12 06:30:05', '2017-09-12 06:30:05'),
(238, 'employer', 25, 'photos', 'aeo', 'aeo.png', '', 41363, '', '', 238, '2017-11-18 07:49:47', '2017-11-18 07:50:33'),
(250, 'App\\Site', 1, 'sample', 'aeo', 'aeo.png', '', 41363, '', '', NULL, '2017-11-28 03:36:03', '0000-00-00 00:00:00'),
(251, 'App\\Site', 1, 'sample', 'bug1', 'bug1.PNG', '', 475130, '', '', NULL, '2017-11-28 04:56:21', '0000-00-00 00:00:00'),
(252, 'App\\Site', 1, 'sample', 'aeo', 'aeo.png', '', 41363, '', '', NULL, '2017-11-28 05:02:01', '0000-00-00 00:00:00'),
(253, 'App\\Site', 1, 'photos', 'perfumes', 'perfumes.jpg', '', 401605, '', '', 239, '2017-12-05 07:37:42', '0000-00-00 00:00:00'),
(254, 'App\\Site', 1, 'photos', 'qurancube', 'qurancube.jpg', '', 93890, '', '', 240, '2017-12-05 07:39:07', '0000-00-00 00:00:00'),
(255, 'App\\Site', 1, 'photos', 'background', 'background.jpg', '', 658871, '', '', 241, '2017-12-08 01:50:38', '0000-00-00 00:00:00'),
(256, 'App\\Site', 1, 'photos', '3', '3.png', '', 76233, '', '', 242, '2017-12-08 01:51:04', '0000-00-00 00:00:00'),
(257, 'App\\Site', 1, 'photos', '2', '2.png', '', 84473, '', '', 243, '2017-12-08 01:51:11', '0000-00-00 00:00:00'),
(260, 'App\\Employer', 6, 'photos', 'bug1', 'bug1.PNG', '', 161694, '', '', 246, '2017-12-11 07:43:11', '0000-00-00 00:00:00'),
(261, 'App\\Users\\Candidate', 3, 'photos', 'bug1', 'bug1.PNG', '', 161694, '', '', 247, '2017-12-12 00:08:40', '0000-00-00 00:00:00'),
(263, 'App\\Education\\Institution', 6, 'photos', 'bug1', 'bug1.PNG', '', 161694, '', '', 249, '2017-12-12 01:42:55', '0000-00-00 00:00:00'),
(264, 'candidate', 59, 'video', 'default_200x150', 'default_200x150', 'media', 0, '', '', NULL, '2017-12-22 02:37:08', '0000-00-00 00:00:00'),
(265, 'candidate', 60, 'video', 'default_200x150', 'default_200x150', 'media', 0, '', '', NULL, '2017-12-22 02:44:00', '0000-00-00 00:00:00'),
(266, 'candidate', 61, 'video', 'default_200x150', 'default_200x150', 'media', 0, '', '', NULL, '2017-12-22 02:56:57', '0000-00-00 00:00:00'),
(267, 'Candidate', 40, 'documents', '', '', 'documents', 0, '', '', NULL, '2017-12-22 04:51:58', '0000-00-00 00:00:00'),
(268, 'App\\Site', 1, 'photos', 'employers-search', 'employers-search.jpg', '', 163956, '', '', 250, '2017-12-27 01:12:02', '0000-00-00 00:00:00'),
(269, 'App\\Site', 1, 'sample', 'employers-search', 'employers-search.jpg', '', 163956, '', '', 251, '2017-12-29 04:45:57', '0000-00-00 00:00:00'),
(271, 'App\\Site', 1, 'sample', 'employers-search', 'employers-search.jpg', '', 163956, '', '', 252, '2017-12-29 04:46:49', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cv16_media_categories`
--

CREATE TABLE `cv16_media_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_media_categories`
--

INSERT INTO `cv16_media_categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Certificates', '2016-03-15 08:14:07', '2016-03-15 08:14:07'),
(2, 'Qualifications', '2016-03-15 08:14:07', '2016-03-15 08:14:07'),
(3, 'Training skills/courses', '2016-03-15 08:14:07', '2016-03-15 08:14:07'),
(4, 'References', '2016-03-15 08:14:07', '2016-03-15 08:14:07');

-- --------------------------------------------------------

--
-- Table structure for table `cv16_migrations`
--

CREATE TABLE `cv16_migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_migrations`
--

INSERT INTO `cv16_migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_07_23_124428_create_roles_table', 1),
('2015_07_23_124608_create_permissions_table', 1),
('2015_07_23_124832_create_role_user_table', 1),
('2015_07_23_125056_create_permission_role_table', 1),
('2015_07_23_131400_create_industries_table', 1),
('2015_07_23_131401_create_media_table', 1),
('2015_07_23_131531_create_profiles_table', 1),
('2015_08_07_125725_create_skill_categories_table', 1),
('2015_08_07_130244_create_skills_table', 1),
('2015_09_11_142545_create_media_categories_table', 1),
('2015_09_15_124644_create_employers_table', 1),
('2015_09_21_092605_create_jobs_table', 1),
('2015_09_25_124958_create_job_skill_table', 1),
('2015_09_30_101010_add_cashier_columns', 1),
('2015_10_01_094940_create_profile_favourites_table', 1),
('2015_10_28_150942_create_sites_table', 1),
('2015_10_30_094328_create_profile_views_table', 1),
('2015_11_20_132807_create_videos_table', 1),
('2015_11_27_090951_create_profile_videos_table', 1),
('2015_12_08_100339_create_conversations_table', 1),
('2015_12_08_102727_create_conversation_members_table', 1),
('2015_12_08_103119_create_conversation_messages_table', 1),
('2016_01_08_142048_create_job_applications_table', 1),
('2016_01_21_113042_add_additional_fields_to_jobs_table', 1),
('2016_03_14_134555_create_user_qualifications_table', 2),
('2016_03_14_134621_create_user_experiences_table', 2),
('2016_03_14_134647_create_user_hobbies_table', 2),
('2016_03_14_134707_create_user_languages_table', 2),
('2016_03_14_134725_create_user_documents_table', 2),
('2016_03_14_140110_create_profile_experiences_table', 3),
('2016_03_14_140638_create_profile_qualifications_table', 3),
('2016_03_14_140729_create_profile_hobbies_table', 3),
('2016_03_14_141422_create_profile_languages_table', 3),
('2016_03_14_141519_create_profile_documents_table', 3),
('2016_03_14_142817_create_profile_skills_table', 4),
('2016_04_25_133718_create_pages_table', 5),
('2016_04_25_150834_create_slideshows_table', 6),
('2016_04_25_152245_create_slideshow_slides_table', 7),
('2016_04_27_120350_add_background_id_and_type_to_slideshows_table', 8),
('2016_05_06_152135_create_employer_users_table', 8),
('2016_05_19_101953_add_status_to_videos_table', 9),
('2016_06_09_112022_create_video_categories_table', 10),
('2016_06_09_120651_add_name_category_fields_to_videos_table', 10),
('2016_06_10_134253_create_queued_jobs_table', 11),
('2016_06_13_144555_create_activities_table', 12),
('2016_06_15_120802_add_account_holder_field_to_employer_users_table', 13),
('2016_06_24_164459_add_status_to_users_table', 14),
('2016_07_04_081915_add_background_position_to_slideshow_slides', 15),
('2016_07_04_105344_add_background_position_to_slideshow_table', 16),
('2016_08_03_142050_add_ordering_column_to_skill_categories', 17),
('2016_09_27_090910_create_institutions_table', 18),
('2016_09_30_075842_create_institution_users_table', 18),
('2016_10_03_124756_create_tutor_groups_table', 18),
('2016_10_04_152154_create_tutor_groups_students_table', 18),
('2016_11_01_135906_add_graduation_date_and_account_limit_to_tutor_groups_table', 18),
('2016_11_01_151008_remove_account_limit_from_institution_users_table', 18),
('2016_11_02_110825_add_cashier_columns_to_employers', 18),
('2016_11_02_112712_add_email_to_employers_table', 18),
('2016_11_02_124538_remove_account_holder_and_add_role_to_employer_users_table', 18),
('2016_11_03_123131_add_cashier_columns_to_institutions_table', 18),
('2016_11_04_104531_add_graduation_date_to_tutor_group_students_table', 18),
('2016_11_14_170511_change_tutor_groups_teacher_id_to_nullable', 18),
('2016_11_14_170532_add_foreign_keys_to_tutor_groups_table', 18),
('2016_11_15_144644_create_plans_table', 18),
('2016_12_21_130707_add_card_expiry_to_employers_table', 19),
('2016_12_21_140119_add_card_expiry_to_users_table', 19),
('2016_12_21_143348_add_card_expiry_to_institutions_table', 19),
('2017_01_19_203850_add_salary_type_to_jobs_table', 20),
('2017_02_02_164020_change_default_visibility_and_published_state_in_profiles', 21),
('2017_02_02_165131_create_job_views_table', 21),
('2017_02_02_173420_add_num_views_to_jobs_table', 21),
('2017_02_24_130552_create_addresses_table', 22),
('2017_03_13_143419_add_user_name_to_conversation_members_table', 23),
('2017_03_13_144141_add_user_name_to_conversation_messages_table', 23),
('2017_03_17_101121_add_body_field_to_institutions_table', 24),
('2017_06_05_092257_set_video_id_to_nullable', 25);

-- --------------------------------------------------------

--
-- Table structure for table `cv16_pages`
--

CREATE TABLE `cv16_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_pages`
--

INSERT INTO `cv16_pages` (`id`, `user_id`, `title`, `slug`, `body`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, 'Terms & Conditions', 'terms-and-conditions', '<h4>1 Definitions</h4>\r\n<p style=\"font-weight: normal;\">&ldquo;CVVID&rdquo;, &ldquo;Us&rdquo; or &ldquo;We&rdquo; means&nbsp;CVVID Limited&nbsp;of&nbsp;Witton Chambers, Cartmel Road, Blackburn BB2 2TA</p>\r\n<p style=\"font-weight: normal;\">&ldquo;Submissions&rdquo; will mean any content of whatever nature You submit to Us or the Site</p>\r\n<p style=\"font-weight: normal;\">&ldquo;the Site&rdquo; means CVVID.COM and any sub-site to which it allows access;</p>\r\n<p style=\"font-weight: normal;\">&ldquo;Unique URL&rdquo;&nbsp;&nbsp;&nbsp; means the code which We generate and which You require to allow others to access the unique URL for Your VCV</p>\r\n<p style=\"font-weight: normal;\">&ldquo;VCV&rdquo;&nbsp;&nbsp;means the Video CV produced using the Site </p>\r\n<p style=\"font-weight: normal;\">&ldquo;You&rdquo; &ldquo;Your&rdquo;&nbsp;&nbsp;means a visitor to the Site for any purpose whatsoever, whether as a registered user or Guest </p>\r\n<h4>2 This Agreement</h4>\r\n<p style=\"font-weight: normal;\">This User Agreement (together with any documents referred to in it) tells You the terms of use upon which You may make use of the Site and by using the Site, our services provided through the Site or any content or information provided as part of our services or on the Site, You are entering into a legally-binding contract with Us.</p>\r\n<p style=\"font-weight: normal;\">Please read this User Agreement carefully before You start to use the Site, as it will apply to Your use of the Site. We recommend that You print a copy of this User Agreement for future reference.</p>\r\n<p style=\"font-weight: normal;\">By using the Site, You confirm that You accept this User Agreement and that You agree to comply with them.</p>\r\n<p style=\"font-weight: normal;\"><strong>If You do not agree to the terms of this User Agreement, You must NOT use the Site.</strong></p>\r\n<h4>3 Other Applicable Terms</h4>\r\n<p style=\"font-weight: normal;\">This User Agreement refers to the following additional terms, which also apply to Your use of the Site:</p>\r\n<p style=\"font-weight: normal;\">Our Privacy Policy&nbsp;<a href=\"#\" data-toggle=\"modal\" data-target=\"#PrivacyPolicyModal\">here</a>, which sets out the terms on which We process any personal data We collect from You, or that You provide to Us. By using the Site, You consent to such processing and You warrant that all data provided by You is accurate.</p>\r\n<p style=\"font-weight: normal;\">Our Cookie Policy&nbsp;<a href=\"#\" data-toggle=\"modal\" data-target=\"#CookiePolicyModal\">here</a>, which sets out information about the cookies on the Site.</p>\r\n<h4>4 Information about Us</h4>\r\n<p style=\"font-weight: normal;\">CVVID.com is a site operated by CVVID Limited. We are registered in England and Wales under company number 07925935 and have Our registered office at Witton Chambers, Cartmel Road, Blackburn BB2 2TA</p>\r\n<h4>5 Changes to this User Agreement</h4>\r\n<p style=\"font-weight: normal;\">We may revise the terms of this User Agreement at any time by amending this page. Please check this page from time to time to take notice of any changes which We may have made, as they are binding on You.</p>\r\n<h4>6 Changes to the Site</h4>\r\n<p style=\"font-weight: normal;\">We may update the Site from time to time, and may change its content at any time. However, please note that any of the content on the site may be out of date at any given time, and We are under no obligation to update it .</p>\r\n<p style=\"font-weight: normal;\">We do not guarantee that the Site, or any content on it, will be free from errors or omissions.</p>\r\n<h4>7 Warranties</h4>\r\n<p style=\"font-weight: normal;\">By accessing and/or using the Site, You warrant to Us that: </p>\r\n<ul style=\"font-weight: normal;\">\r\n<li>You have the legal authority to enter into this User Agreement</li>\r\n<li>You will not\r\n<ol>\r\n<li>use the Site or its contents for any commercial purpose;</li>\r\n<li>access, monitor or copy any content or information of the Site using any robot, spider, scraper or other automated means or any manual process for any purpose without Our express written permission;</li>\r\n<li>violate the restrictions in any robot exclusion headers on the Site or bypass or circumvent other measures employed to prevent or limit access to the Site</li>\r\n<li>take any action that imposes, or may impose, in our discretion, an unreasonable or disproportionately large load on Our infrastructure</li>\r\n<li>deep-link to any portion of the Site for any purpose without Our express written permission;</li>\r\n<li>\"frame\", \"mirror\" or otherwise incorporate any part of the Site into any other website without Our prior written authorisation</li>\r\n<li>attempt to modify, translate, adapt, edit, decompile, disassemble, or reverse engineer any software programs used by Us in connection with the Site or the services which We offer;</li>\r\n</ol>\r\n</li>\r\n</ul>\r\n<h4>8 Indemnities</h4>\r\n<p style=\"font-weight: normal;\"> You agree to indemnify Us and Our affiliates, officers, directors, employees and agents from and against all loss or damage, claims, causes of action, demands,&nbsp; recoveries&nbsp; of any kind or nature including, but not limited to, reasonable legal and accounting fees, brought by any third party as a result of:</p>\r\n<ul style=\"font-weight: normal;\">\r\n<li>Your breach of this User Agreement or the documents referenced herein;</li>\r\n<li>Your violation of any law or the rights of a third party; or</li>\r\n<li>Your use of the Site: or</li>\r\n<li>Your violation of any of the rules and regulations of any other bodies; or any other activity on Your part which gives rise to such a claim, cause of action, demand or recovery.</li>\r\n</ul>\r\n<h4>9 Accessing and using the Site</h4>\r\n<p style=\"font-weight: normal;\"> Whilst access to the Site is free of charge, You must pay Us to use any Unique URL for such use at the rate specified on the Site (&ldquo;the Access Fee&rdquo;) and using any secure payment service which We, in Our sole discretion elect to use in relation to such payment from time to time.  </p>\r\n<p style=\"font-weight: normal;\">Any fee payable in respect of Your use of the Unique URL does not include any fee payable by the payment service which We use, nor any fee charged to You by Your Bank or other third parties.</p>\r\n<p style=\"font-weight: normal;\">We will refund part of the Access Fee to You if We, acting in our sole discretion, agree that:</p>\r\n<ul style=\"font-weight: normal;\">\r\n<li>any third party has accessed Your VCV fraudulently;</li>\r\n<li>any third party to whom You have given the Unique URL has accessed Your VCV on more than one occasion and such subsequent access was without Your permission;</li>\r\n</ul>\r\n<p style=\"font-weight: normal;\">The part of the Access Fee to be refunded will be calculated in accordance with the following formula: (AC/Y)xZ , where </p>\r\n<ul style=\"font-weight: normal;\">\r\n<li>AF is the Access Fee</li>\r\n<li>Y is the number of times You are permitted to access the VCV</li>\r\n<li>Z is the number of times incorrect access has been obtained</li>\r\n</ul>\r\n<p style=\"font-weight: normal;\">We will pay any refund due to You under the terms of this clause within 28 days of agreeing to do so.</p>\r\n<p style=\"font-weight: normal;\">No returns are permitted if You have been supplied with a valid Unique URL.</p>\r\n<p style=\"font-weight: normal;\">Once purchased, no Unique URL may be cancelled either by You or Us except in the case of a breach of this User Agreement, when We may in Our sole discretion but acting reasonably suspend or cancel Your Unique URL.</p>\r\n<p style=\"font-weight: normal;\">Any Unique URL purchased from the Site will be delivered to You by e-mail or such other method as We in Our absolute discretion decide. </p>\r\n<p style=\"font-weight: normal;\">Delivery of the Unique URL will be made only when any payment for it has been fully authorised by Your bank or financial institution and received in full by Our payment service</p>\r\n<p style=\"font-weight: normal;\">We do not guarantee that the Site, or any content on it, will always be available or be uninterrupted. Access to the Site is permitted on a temporary basis.</p>\r\n<p style=\"font-weight: normal;\">We may suspend, withdraw, discontinue or change all or any part of the Site without notice. We will not be liable to You if for any reason the Site is unavailable at any time or for any period.</p>\r\n<p style=\"font-weight: normal;\">You are responsible for making all arrangements necessary for You to have access to the Site.</p>\r\n<p style=\"font-weight: normal;\">You are also responsible for ensuring that all persons who access the Site through Your internet connection are aware of the terms of this User Agreement and any other applicable terms and conditions, and that they comply with them.</p>\r\n<p style=\"font-weight: normal;\"> Your further&nbsp;use of any VCV produced by Your use of the Site or any of its features or services is subject to the following conditions: </p>\r\n<ul style=\"font-weight: normal;\">\r\n<li>You will not record or attempt to record the VCV nor to store the VCV or the code with which it has been created nor link or attempt to link the Site on which the VCV is stored with any other site</li>\r\n<li>The VCV will be used only for the purpose of&nbsp; promoting the user which it identifies in his/her search for employment or reviewing the VCV in connection with a possible application for work</li>\r\n</ul>\r\n<h4>10 Your Username and Password</h4>\r\n<p style=\"font-weight: normal;\">When You choose or We provide You with a username, password, Unique URL or any other piece of information as part of Our security procedures, You must treat such information as confidential. You must not disclose it to any third party.</p>\r\n<p style=\"font-weight: normal;\">We have the right to disable any username, password or Unique URL, whether chosen by You or allocated by Us, at any time, if in Our reasonable opinion You have failed to comply with any of the provisions of this User Agreement.</p>\r\n<p style=\"font-weight: normal;\">If You know or suspect that anyone other than You knows Your user identification code or password, You must promptly notify Us at info@cvvid.com.</p>\r\n<h4>11 Intellectual Property Rights</h4>\r\n<p style=\"font-weight: normal;\">We are the owner or the licensee of all intellectual property rights in the Site (including, without limitation, the content and information on the Site, messages, data, information, text, music, sound, photos, graphics, video, maps, icons, software, code or any other material as well as the infrastructure used to provide such content and information) and any VCV created as a result of Your use of the Site and in the material published on it. Those works are protected by copyright laws and treaties around the world. All such rights are reserved.</p>\r\n<p style=\"font-weight: normal;\">Save where specifically authorised by Us You cannot copy, share, transmit, reproduce, post or redistribute all or any part of any of the information or material contained on the Site without Our permission in writing and without subjecting Yourself to any additional terms, whether financial or otherwise, which We in Our absolute discretion impose, save and except to provide access to the VCV using the authorisation of the Site</p>\r\n<p style=\"font-weight: normal;\">You agree not to modify, copy, distribute, transmit, display, perform, reproduce, publish, license, create derivative works from, transfer, or sell or re-sell any information, software, products, or services obtained from or through the Site.</p>\r\n<p style=\"font-weight: normal;\">Our status (and that of any identified contributors) as the authors of content on the Site must always be acknowledged.</p>\r\n<p style=\"font-weight: normal;\">If You copy or download any part of the Site in breach of this User Agreement, Your right to use the Site will cease immediately and You must, at Our option, return or destroy any copies of the materials You have made.</p>\r\n<p style=\"font-weight: normal;\">In respect of all and any Submissions made by You, You grant Us and Our affiliates a non-exclusive, royalty-free, perpetual, transferable, irrevocable and fully assignable right to&nbsp;use, reproduce, modify, adapt, translate, distribute, publish, create derivative works from and publicly display and perform such Submissions throughout the world in any media, now known or hereafter devised and&nbsp;use any name that You submit in connection with such Submission.</p>\r\n<p style=\"font-weight: normal;\">You acknowledge that We may choose to provide attribution of Your comments or reviews at its discretion.&nbsp; </p>\r\n<p style=\"font-weight: normal;\">You further grant Us the exclusive right to pursue at law any person or entity that violates Your or CVV&rsquo;s rights in the Submissions by a breach of this User Agreement and to retain all or any damages costs and other payments granted in such action.</p>\r\n<h4>12 No Reliance On Information</h4>\r\n<p style=\"font-weight: normal;\">The content on the Site and of any VCV created as a result of any user&rsquo;s use of the Site is provided for general information only. It is not intended to amount to advice on which You should rely. You must obtain professional or specialist advice before taking, or refraining from, any action on the basis of the content on the Site or the content of any VCV.</p>\r\n<p style=\"font-weight: normal;\">Although We make reasonable efforts to update the information on the Site, We make no representations, warranties or guarantees, whether express or implied, that the content on the Site or in any VCV is accurate, complete or up-to-date.</p>\r\n<h4>13 Limitation of Liability</h4>\r\n<p style=\"font-weight: normal;\">Nothing in this User Agreement excludes or limits Our liability for death or personal injury arising from Our negligence, or Our fraud or fraudulent misrepresentation, or any other liability that cannot be excluded or limited by English law.</p>\r\n<p style=\"font-weight: normal;\">To the extent permitted by law, We exclude all conditions, warranties, representations or other terms which may apply to the Site or any content on it, whether express or implied.</p>\r\n<p style=\"font-weight: normal;\">We will not be liable to any User for any loss or damage, whether in contract, tort (including negligence), breach of statutory duty, or otherwise, even if foreseeable, arising under or in connection with:</p>\r\n<ul style=\"font-weight: normal;\">\r\n<li>use of, or inability to use, the Site; or</li>\r\n<li>use of or reliance on any content displayed on the Site.</li>\r\n</ul>\r\n<p style=\"font-weight: normal;\">If You are a business user, please note that in particular, We will not be liable for:<br />loss of profits, sales, business, or revenue;</p>\r\n<ul style=\"font-weight: normal;\">\r\n<li>business interruption;</li>\r\n<li>loss of anticipated savings;</li>\r\n<li>loss of business opportunity, goodwill or reputation; or</li>\r\n<li>any indirect or consequential loss or damage.</li>\r\n</ul>\r\n<p style=\"font-weight: normal;\">If You are a consumer user, please note that We only provide the Site for private use. You agree not to use the Site for any commercial or business purposes, and We have no liability to You for any loss of profit, loss of business, business interruption, or loss of business opportunity.</p>\r\n<p style=\"font-weight: normal;\">We will not be liable for any loss or damage caused by a virus, distributed denial-of-service attack, or other technologically harmful material that may infect Your computer equipment, computer programs, data or other proprietary material due to Your use of the Site or to Your downloading of any content on it, or on any website linked to it.</p>\r\n<p style=\"font-weight: normal;\">We assume no responsibility for the content of websites linked to on the Site or in any VCV. Such links should not be interpreted as endorsement by Us of those linked websites. We will not be liable for any loss or damage that may arise from Your use of them.</p>\r\n<h4>14 Interactive Areas</h4>\r\n<p style=\"font-weight: normal;\">&ldquo;Interactive Area&rdquo; means all parts of the Site which allow comment posting, content uploading or any other form of interaction between You, Us and the Site.</p>\r\n<p style=\"font-weight: normal;\"> Whenever You make use of an Interactive Area, You must comply with any content standards which We may have from time to time in force. You warrant that any such contribution does comply with those standards, and You will be liable to Us and indemnify Us for any breach of that warranty. If You are a consumer User, this means You will be responsible for any loss or damage We suffer as a result of Your breach of warranty.</p>\r\n<p style=\"font-weight: normal;\">Any content You upload to the Site will be considered non-confidential and non-proprietary.</p>\r\n<p style=\"font-weight: normal;\">You retain all of Your ownership rights in content which You upload to the Site, save for the VCV, but You are required to grant Us and other users of the Site a limited licence to use, store and copy that content and to distribute and make it available to third parties.</p>\r\n<p style=\"font-weight: normal;\">We also have the right to disclose Your identity to any third party who is claiming that any content posted or uploaded by You to the Site constitutes a violation of their intellectual property rights, or of their right to privacy.</p>\r\n<p style=\"font-weight: normal;\">We will not be responsible, or liable to any third party, for the content or accuracy of any content posted by You or any other user of the Site.</p>\r\n<p style=\"font-weight: normal;\">We have the right to remove any posting You make on the Site if, in Our opinion, Your post does not comply with any content standards which We may have from time to time in force.</p>\r\n<p style=\"font-weight: normal;\">The views expressed by other users on the Site do not represent Our views or values.</p>\r\n<p style=\"font-weight: normal;\">When using an Interactive Area, You expressly agree not to post, upload to, transmit, distribute, store, create or otherwise publish through the Site any of the following:</p>\r\n<ul style=\"font-weight: normal;\">\r\n<li>Any content which We determine, in Our sole discretion, to be false, unlawful, misleading, libellous, defamatory, obscene, pornographic, indecent, lewd, suggestive, harassing or which advocates harassment of another person, is threatening, invasive of privacy or publicity rights, abusive, inflammatory, fraudulent or otherwise objectionable;</li>\r\n<li>Any content that is patently offensive to the online community, such as content that promotes racism, bigotry, hatred or physical harm of any kind against any group or individual;</li>\r\n<li>Any content which would constitute, encourage, promote or provide instructions for conduct of an illegal activity, criminal offence, give rise to civil liability, violate the rights of any party in any country in the World, or that would otherwise create or constitute any liability or violate any local, state, national or international law;</li>\r\n<li>Any content which provides instructional information about illegal activities such as making or buying illegal weapons, violating someone\'s privacy, or providing or creating computer viruses;</li>\r\n<li>Any content which may, in Our sole discretion, infringe any patent, trademark, trade secret, copyright or other intellectual or proprietary right of any party; in particular, content which promotes an unlawful or unauthorised copy of a third party&rsquo;s copyrighted work, such as providing pirated computer programs or links to them, providing information to circumvent manufacture-installed copy-protect devices, or providing pirated music or links to pirated music files;</li>\r\n<li>Any content which impersonates any person or entity or otherwise misrepresents Your affiliation with a person or entity, including Us;</li>\r\n<li>Any unsolicited promotion, mass mailing or &ldquo;spam&rdquo;, \"junk mail\", \"chain letters,\" political campaign material, advertising, contests, raffles, or solicitations;</li>\r\n<li>Any content relating to commercial activities without Our prior written consent including (but not limited to) contests, sweepstakes, barter, advertising, and pyramid schemes;</li>\r\n<li>Any content including private and/or personal information relating to any third party, including, without limitation, surname (family name) addresses, phone numbers, email addresses, Social Security numbers and credit card numbers;</li>\r\n<li>Any content which contains restricted or password- only access pages, or hidden pages or images (those not linked to or from another accessible page);</li>\r\n<li>Content which is unrelated to the topic of the Interactive Area(s) in which such Content is posted;</li>\r\n<li>Content or links to content which inhibits any other person from using or enjoying the Interactive Areas or the Site, or&nbsp;</li>\r\n<li>Content which may expose Us or Our affiliates or Our users to any prosecution harm or liability of any type.</li>\r\n</ul>\r\n<p style=\"font-weight: normal;\">We assume no responsibility or liability for any content posted, stored or uploaded by You or any third party, or for any loss or damage thereto, nor are We liable for any mistakes, defamation, slander, libel, omissions, falsehoods, obscenity, pornography or profanity which You may encounter via Your use of the Site.</p>\r\n<p style=\"font-weight: normal;\">As a provider of interactive services, We are not liable for any statements, representations or content provided by its users in any public forum, personal home page or other Interactive Area.&nbsp; </p>\r\n<p style=\"font-weight: normal;\">Without accepting any obligation so to do, We reserve the right to remove, screen, alter, edit or monitor any content posted to or distributed through any Interactive Area which We, in Our sole discretion, decide and which is posted or stored on the Site at any time and for any reason, and You are solely responsible for creating backup copies of and replacing any Content You post or store on the Site at Your sole cost and expense.</p>\r\n<p style=\"font-weight: normal;\"> If You use an Interactive Area or any other part of the Site in violation of this User Agreement, We may, in Our sole discretion, terminate or suspend Your right to use the Interactive Areas and/or the Site or any part or parts of it</p>\r\n<p style=\"font-weight: normal;\"> If required by any proper request of any Court, Law Enforcement or Government body or to protect CVV&rsquo;s systems and other users, We may access and disclose any information We considers necessary or appropriate to comply with that request, including, without limitation, user profile information (i.e. name, email address, etc.), IP address and traffic information, usage history, and posted Content. Our right to disclose any such information in these circumstances will take precedence over any terms of CVV&rsquo;s Privacy Policy. </p>\r\n<p style=\"font-weight: normal;\">If You find any content on the Site which You believe to be in any way offensive, libellous, incorrect or in any other way inappropriate, You should notify Us immediately by using the contact details shown on the Site and supplying any supporting information which We may request to validate or support Your assertion </p>\r\n<p style=\"font-weight: normal;\">If We determine, in Our sole discretion, that any content notified to Us by a user or other third party is inappropriate for the Site, We will take all reasonable steps to remove that Content as soon as possible.</p>\r\n<p style=\"font-weight: normal;\"> </p>\r\n<h4>15 Viruses</h4>\r\n<p style=\"font-weight: normal;\">We do not guarantee that the Site or any service which We provide will be secure or free from bugs or viruses.</p>\r\n<p style=\"font-weight: normal;\">You are responsible for configuring Your information technology, computer programmes and platform in order to access the Site. You should use Your own virus protection software.</p>\r\n<p style=\"font-weight: normal;\">You must not misuse the Site by knowingly introducing viruses, trojans, worms, logic bombs or other material which is malicious or technologically harmful. You must not attempt to gain unauthorised access to the Site, the server on which the Site is stored or any server, computer or database connected to the Site. You must not attack the Site via a denial-of-service attack or a distributed denial-of service attack. By breaching this provision, You would commit a criminal offence under the Computer Misuse Act 1990. We will report any such breach to the relevant law enforcement authorities and We will co-operate with those authorities by disclosing Your identity to them. In the event of such a breach, Your right to use the Site will cease immediately.</p>\r\n<h4>16 Linking to the Site</h4>\r\n<p style=\"font-weight: normal;\">You may link to Our home page, provided You do so in a way that is fair and legal and does not damage Our reputation or take advantage of it.</p>\r\n<p style=\"font-weight: normal;\">You must not establish a link in such a way as to suggest any form of association, approval or endorsement on Our part where none exists.</p>\r\n<p style=\"font-weight: normal;\">You must not establish a link to the site in any website that is not owned by You.</p>\r\n<p style=\"font-weight: normal;\">The Site must not be framed on any other site, nor may You create a link to any part of the Site other than the home page.</p>\r\n<p style=\"font-weight: normal;\">We reserve the right to withdraw linking permission without notice.</p>\r\n<p style=\"font-weight: normal;\">The website in which You are linking must comply in all respects with any content standards which we may have from time to time in force.</p>\r\n<p style=\"font-weight: normal;\">If You wish to make any use of content on the Site other than that set out above, please contact info@cvvid.com.</p>\r\n<h4>17 General</h4>\r\n<p style=\"font-weight: normal;\"><span class=\"underline\">Conflict</span></p>\r\n<p style=\"font-weight: normal;\">If there is an inconsistency between any of the provisions of this User Agreement and the provisions of any other document to which it refers, the provisions of this User Agreement shall prevail.</p>\r\n<p style=\"font-weight: normal;\"><span class=\"underline\">Variation</span></p>\r\n<p style=\"font-weight: normal;\">No variation of this User Agreement shall be effective unless it is in writing and signed by both You and Us (or Our authorised representatives).</p>\r\n<p style=\"font-weight: normal;\"><span class=\"underline\">Waiver</span></p>\r\n<p style=\"font-weight: normal;\">No failure or delay by You or Us to exercise any right or remedy provided under this User Agreement or by law shall constitute a waiver of that or any other right or remedy, nor shall it prevent or restrict the further exercise of that or any other right or remedy. No single or partial exercise of such right or remedy shall prevent or restrict the further exercise of that or any other right or remedy.</p>\r\n<p style=\"font-weight: normal;\"><span class=\"underline\">No Partnership or Agency</span></p>\r\n<p style=\"font-weight: normal;\">Other than that envisaged by its terms, nothing in this User Agreement is intended to, or shall be deemed to, establish any partnership or joint venture between You and Us, constitute either party the agent of another party, or authorise any party to make or enter into any commitments for or on behalf of any other party. You and We confirm that We are each acting on Our own behalf and not for the benefit of any other person.</p>\r\n<p style=\"font-weight: normal;\"><span class=\"underline\">Rights &amp; Remedies</span></p>\r\n<p style=\"font-weight: normal;\">Except as expressly provided in this User Agreement, the rights and remedies provided under this User Agreement are in addition to, and not exclusive of, any rights or remedies provided by law.</p>\r\n<p style=\"font-weight: normal;\"><span class=\"underline\">Language</span></p>\r\n<p style=\"font-weight: normal;\">This User Agreement is drafted in the English language. If this agreement is translated into any other language, the English language version shall prevail.</p>\r\n<p style=\"font-weight: normal;\">Any notice given under or in connection with this User Agreement shall be in the English language. All other documents provided under or in connection with this User Agreement shall be in the English language, or accompanied by a certified English translation. If such document is translated into any other language, the English language version shall prevail.</p>\r\n<p style=\"font-weight: normal;\"><span class=\"underline\">Unenforceability &amp; Severance</span></p>\r\n<p style=\"font-weight: normal;\">If any provision or part-provision of this User Agreement is or becomes invalid, illegal or unenforceable, it shall be deemed modified to the minimum extent necessary to make it valid, legal and enforceable. If such modification is not possible, the relevant provision or part-provision shall be deemed deleted. Any modification to or deletion of a provision or part-provision under this clause shall not affect the validity and enforceability of the rest of this User Agreement.</p>\r\n<p style=\"font-weight: normal;\">If any provision or part-provision of this User Agreement is invalid, illegal or unenforceable, You and We shall negotiate in good faith to amend such provision so that, as amended, it is legal, valid and enforceable, and, to the greatest extent possible, achieves the intended commercial result of the original provision.</p>\r\n<p style=\"font-weight: normal;\"><span class=\"underline\">Entire Agreement</span></p>\r\n<p style=\"font-weight: normal;\">This User Agreement constitutes the entire agreement between You and Us and supersedes and extinguishes all previous agreements, promises, assurances, warranties, representations and understandings between Us, whether written or oral, relating to its subject matter. You and We agree that We shall have no remedies in respect of any statement, representation, assurance or warranty (whether made innocently or negligently) that is not set out in this User Agreement and that We shall have no claim for innocent or negligent misrepresentation or negligent misstatement based on any statement in this User Agreement.</p>\r\n<p style=\"font-weight: normal;\"><span class=\"underline\">Notices</span></p>\r\n<p style=\"font-weight: normal;\">Any notice or other communication given to a party under or in connection with this User Agreement shall be in writing and shall be:</p>\r\n<ol style=\"font-weight: normal;\">\r\n<li>delivered by hand or by pre-paid first-class post or other next working day delivery service at its registered office (if a company) or its principal place of business (in any other case);</li>\r\n<li>sent by fax to its main fax number; or</li>\r\n<li>sent by e-mail to its main e-mail address</li>\r\n</ol>\r\n<p style=\"font-weight: normal;\">Any notice or communication shall be deemed to have been received:</p>\r\n<ol style=\"font-weight: normal;\">\r\n<li>if delivered by hand, on signature of a delivery receipt or at the time the notice is left at the proper address;</li>\r\n<li>if sent by pre-paid first-class post or other next working day delivery service, at 9.00 am on the second business day after posting or at the time recorded by the delivery service.</li>\r\n<li>if sent by fax, or e-mail at 9.00 am on the next business day after transmission.</li>\r\n</ol>\r\n<p style=\"font-weight: normal;\">This clause does not apply to the service of any proceedings or other documents in any legal action or, where applicable, any arbitration or other method of dispute resolution.</p>\r\n<p style=\"font-weight: normal;\"><span class=\"underline\">Third Party Rights</span></p>\r\n<p style=\"font-weight: normal;\">No one other than a party to this User Agreement, their successors and permitted assignees shall have any right to enforce any of its terms.</p>\r\n<p style=\"font-weight: normal;\"><span class=\"underline\">Governing Law &amp; Jurisdiction</span></p>\r\n<p style=\"font-weight: normal;\">This User Agreement and any dispute or claim arising out of or in connection with it or its subject matter or formation (including non-contractual disputes or claims) shall be governed by and construed in accordance with the law of England and Wales.</p>\r\n<p style=\"font-weight: normal;\">You and We irrevocably agree that the courts of England and Wales shall have exclusive jurisdiction to settle any dispute or claim arising out of or in connection with this User Agreement or its subject matter or formation (including non-contractual disputes or claims).</p>', 'published', '2016-04-25 13:11:13', '2016-09-02 14:45:06', NULL),
(2, 2, 'Privacy Policy', 'privacy-policy', '<p>CVVID Limited (\"We\") are committed to protecting and respecting your privacy.</p>\r\n<p>This policy (together with our User Agreement and any other documents referred to on it) sets out the basis on which any personal data we collect from you, or that you provide to us, will be processed by us. Please read the following carefully to understand our views and practices regarding your personal data and how we will treat it. By visiting www.cvvid.com you are accepting and consenting to the practices described in this policy.</p>\r\n<p>For the purpose of the Data Protection Act 1998 (the Act), the data controller is CVVID Limited of Witton Chambers, Cartmel Road, Blackburn BB2 2TA.</p>\r\n<h3>Information we may collect from you</h3>\r\n<p>We may collect and process the following data about you:</p>\r\n<ul>\r\n<li>Information you give us. You may give us information about you by filling in forms on the site www.cvvid.com (the Site) or by corresponding with us by phone, e-mail or otherwise. This includes information you provide when you register to use the Site, subscribe to our service, participate in discussion boards or other social media functions on the Site, upload content to the Site or otherwise make use of the Site and when you report a problem with the Site. The information you give us may include your name, address, e-mail address and phone number, personal description and photograph.</li>\r\n<li>Information we collect about you. With regard to each of your visits to the Site we may automatically collect the following information:\r\n<ul>\r\n<li>technical information, including the Internet protocol (IP) address used to connect your computer to the Internet, your login information, browser type and version, time zone setting, browser plug-in types and versions, operating system and platform;</li>\r\n<li>information about your visit, including the full Uniform Resource Locators (URL) clickstream to, through and from the Site (including date and time); products you viewed or searched for; page response times, download errors, length of visits to certain pages, page interaction information (such as scrolling, clicks, and mouse-overs), and methods used to browse away from the page and any phone number used to call our customer service number.</li>\r\n</ul>\r\n</li>\r\n<li>Information we receive from other sources. We may receive information about you if you use any of the other websites we operate or the other services we provide. In this case we will have informed you when we collected that data that it may be shared internally and combined with data collected on this site. We are also working closely with third parties (including, for example, business partners, sub-contractors in technical, payment and delivery services, advertising networks, analytics providers, search information providers, credit reference agencies) and may receive information about you from them.</li>\r\n</ul>\r\n<h3>Cookies</h3>\r\n<p>Our website uses cookies to distinguish you from other users of our website. This helps us to provide you with a good experience when you browse our website and also allows us to improve the Site. For detailed information on the cookies we use and the purposes for which we use them see our Cookie policy [LINK].</p>\r\n<h3>Uses made of the information</h3>\r\n<p>We use information held about you in the following ways:</p>\r\n<ul>\r\n<li>Information you give to us. We will use this information:\r\n<ul>\r\n<li>to carry out our obligations arising from any contracts entered into between you and us and to provide you with the information, products and services that you request from us;</li>\r\n<li>to provide you with information about other goods and services we offer that are similar to those that you have already purchased or enquired about;</li>\r\n<li>to provide you, or permit selected third parties to provide you, with information about goods or services we feel may interest you. If you are an existing customer, we will only contact you by electronic means (e-mail or SMS) with information about goods and services similar to those which were the subject of a previous sale or negotiations of a sale to you. If you are a new customer, and where we permit selected third parties to use your data, we (or they) will contact you by electronic means only if you have consented to this. If you do not want us to use your data in this way, or to pass your details on to third parties for marketing purposes, please tick the relevant box situated on the form on which we collect your data (the registration form);</li>\r\n<li>to notify you about changes to our service;</li>\r\n<li>to ensure that content from the Site is presented in the most effective manner for you and for your computer.</li>\r\n</ul>\r\n</li>\r\n</ul>\r\n<ul>\r\n<li>Information we collect about you. We will use this information:\r\n<ul>\r\n<li>to administer the Site and for internal operations, including troubleshooting, data analysis, testing, research, statistical and survey purposes;</li>\r\n<li>to improve the Site to ensure that content is presented in the most effective manner for you and for your computer;</li>\r\n<li>to allow you to participate in interactive features of our service, when you choose to do so;</li>\r\n<li>as part of our efforts to keep the Site safe and secure;</li>\r\n<li>to measure or understand the effectiveness of advertising we serve to you and others, and to deliver relevant advertising to you;</li>\r\n<li>to make suggestions and recommendations to you and other users of the Site about goods or services that may interest you or them.</li>\r\n</ul>\r\n</li>\r\n</ul>\r\n<ul>\r\n<li>Information we receive from other sources. We may combine this information with information you give to us and information we collect about you. We may us this information and the combined information for the purposes set out above (depending on the types of information we receive).</li>\r\n</ul>\r\n<h3>Disclosure of your information</h3>\r\n<p>We may share your personal information with any member of our group, which means our subsidiaries, our ultimate holding company and its subsidiaries, as defined in section 1159 of the UK Companies Act 2006.</p>\r\n<p>We may share your information with selected third parties including:</p>\r\n<ul>\r\n<li>Business partners, suppliers and sub-contractors for the performance of any contract we enter into with them or you.</li>\r\n<li>Advertisers and advertising networks that require the data to select and serve relevant adverts to you and others. We do not disclose information about identifiable individuals to our advertisers, but we may provide them with aggregate information about our users (for example, we may inform them that 500 men aged under 30 have clicked on their advertisement on any given day). We may also use such aggregate information to help advertisers reach the kind of audience they want to target (for example, women in SW1). We may make use of the personal data we have collected from you to enable us to comply with our advertisers\' wishes by displaying their advertisement to that target audience.</li>\r\n<li>Analytics and search engine providers that assist us in the improvement and optimisation of the Site.</li>\r\n<li>Credit reference agencies for the purpose of assessing your credit score where this is a condition of us entering into a contract with you.</li>\r\n</ul>\r\n<p>We may disclose your personal information to third parties:</p>\r\n<ul>\r\n<li>In the event that we sell or buy any business or assets, in which case we may disclose your personal data to the prospective seller or buyer of such business or assets.</li>\r\n<li>If CVVID Limited or substantially all of its assets are acquired by a third party, in which case personal data held by it about its customers will be one of the transferred assets.</li>\r\n<li>If we are under a duty to disclose or share your personal data in order to comply with any legal obligation, or in order to enforce or apply our User Agreement and other agreements; or to protect the rights, property, or safety of CVVID Limited, our customers, or others. This includes exchanging information with other companies and organisations for the purposes of fraud protection and credit risk reduction.</li>\r\n</ul>\r\n<h3>Where we store your personal data</h3>\r\n<p>The data that we collect from you may be transferred to, and stored at, a destination outside the European Economic Area (\"EEA\"). It may also be processed by staff operating outside the EEA who work for us or for one of our suppliers. Such staff maybe engaged in, among other things, the fulfilment of your order, the processing of your payment details and the provision of support services. By submitting your personal data, you agree to this transfer, storing or processing. CVVID Limited will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this privacy policy.</p>\r\n<p>All information you provide to us is stored on our secure servers. Where we have given you (or where you have chosen) a password which enables you to access certain parts of the Site, you are responsible for keeping this password confidential. We ask you not to share a password with anyone.</p>\r\n<p>Unfortunately, the transmission of information via the internet is not completely secure. Although we will do our best to protect your personal data, we cannot guarantee the security of your data transmitted to the Site; any transmission is at your own risk. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorised access.</p>\r\n<h3>Your rights</h3>\r\n<p>You have the right to ask us not to process your personal data for marketing purposes. We will usually inform you (before collecting your data) if we intend to use your data for such purposes or if we intend to disclose your information to any third party for such purposes. You can exercise your right to prevent such processing by checking certain boxes on the forms we use to collect your data. You can also exercise the right at any time by contacting us at info@cvvid.com.</p>\r\n<p>The Site may, from time to time, contain links to and from the websites of our partner networks, advertisers and affiliates. If you follow a link to any of these websites, please note that these websites have their own privacy policies and that we do not accept any responsibility or liability for these policies. Please check these policies before you submit any personal data to these websites.</p>\r\n<h3>Access to information</h3>\r\n<p>The Act gives you the right to access information held about you. Your right of access can be exercised in accordance with the Act. Any access request may be subject to a fee of &pound;10 to meet our costs in providing you with details of the information we hold about you.</p>\r\n<h3>Changes to our privacy policy</h3>\r\n<p>Any changes we may make to our privacy policy in the future will be posted on this page and, where appropriate, notified to you by e-mail. Please check back frequently to see any updates or changes to our privacy policy.</p>\r\n<h3>Contact</h3>\r\n<p>Questions, comments and requests regarding this privacy policy are welcomed and should be addressed to info@cvvid.com</p>', 'published', '2016-04-25 13:21:50', '2016-04-25 13:21:50', NULL),
(3, 2, 'Cookie Policy', 'cookie-policy', '<p>Our website uses cookies to distinguish you from other users of our website. This helps us to provide you with a good experience when you browse our website and also allows us to improve our site. By continuing to browse the site, you are agreeing to our use of cookies.</p>\r\n<p>A cookie is a small file of letters and numbers that we store on your browser or the hard drive of your computer if you agree. Cookies contain information that is transferred to your computer\'s hard drive.</p>\r\n<p>We use the following cookies:</p>\r\n<p><strong>Strictly necessary cookies</strong>. These are cookies that are required for the operation of our website. They include, for example, cookies that enable you to log into secure areas of our website, use a shopping cart or make use of e-billing services.</p>\r\n<p><strong>Analytical/performance cookies</strong>. They allow us to recognise and count the number of visitors and to see how visitors move around our website when they are using it. This helps us to improve the way our website works, for example, by ensuring that users are finding what they are looking for easily.</p>\r\n<p><strong>Targeting cookies</strong>. These cookies record your visit to our website, the pages you have visited and the links you have followed. We will use this information to make our website and the advertising displayed on it more relevant to your interests. We may also share this information with third parties for this purpose.</p>\r\n<p>Please note that third parties (including, for example, advertising networks and providers of external services like web traffic analysis services) may also use cookies, over which we have no control. These cookies are likely to be analytical/performance cookies or targeting cookies.</p>\r\n<p>You block cookies by activating the setting on your browser that allows you to refuse the setting of all or some cookies. However, if you use your browser settings to block all cookies (including essential cookies) you may not be able to access all or parts of our site.</p>', 'published', '2016-09-02 14:45:27', '2016-09-02 14:45:27', NULL),
(4, 2, 'aaa', 'aaa', '<p>aaaa</p>', 'published', '2017-12-29 03:10:26', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cv16_password_resets`
--

CREATE TABLE `cv16_password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_password_resets`
--

INSERT INTO `cv16_password_resets` (`email`, `token`, `created_at`) VALUES
('alex@red-fern.co.uk', 'e0b0705ec6fe5a4d96320ffd73dd82b978ad61926056b757eaeb724b42d3d3fa', '2016-05-19 07:12:33');

-- --------------------------------------------------------

--
-- Table structure for table `cv16_permissions`
--

CREATE TABLE `cv16_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cv16_permission_role`
--

CREATE TABLE `cv16_permission_role` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cv16_plans`
--

CREATE TABLE `cv16_plans` (
  `id` int(10) UNSIGNED NOT NULL,
  `owner_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `owner_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stripe_plan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `price` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `interval` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `interval_count` int(11) NOT NULL DEFAULT '1',
  `hash` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active_to` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_plans`
--

INSERT INTO `cv16_plans` (`id`, `owner_type`, `owner_id`, `name`, `stripe_plan`, `quantity`, `price`, `interval`, `interval_count`, `hash`, `active_to`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'App\\Education\\Institution', 1, 'Test Institution Annual', 'test_institution_annual', 450, '9.0000', 'year', 1, '$2y$10$88JlWWOlxQUL3LIpDuK7yu0skw18l4.Kt0dAnA7lpi925g7rlAG', '2017-02-14 00:00:00', '2017-01-20 13:53:51', '2017-01-20 13:53:53', NULL),
(3, 'App\\Education\\Institution', 2, 'College of A Bangi', 'college_of_a_bangi', 16, '3.0000', 'year', 1, '$2y$10$L8OEXEkHh6NfmURW34izVOzpG6cGXXyM43fp.JhYmIJCWHmGq1E86', '2017-02-28 00:00:00', '2017-02-20 22:23:28', '2017-02-20 22:24:20', '2017-02-20 22:24:20'),
(4, 'App\\Education\\Institution', 2, 'College of A Bangi', 'college_of_a_bangi', 16, '3.0000', 'year', 1, '$2y$10$DK1tP6V8YSpb06GPlabHiuGBj2QuwEEW8Erm1zbqWi2bL4.v8pXW', '2017-02-28 00:00:00', '2017-02-20 22:25:18', '2017-02-20 22:25:19', NULL),
(5, 'App\\Education\\Institution', 3, 'TIGHS', 'tighs', 16, '4.0000', 'year', 1, '$2y$10$fBPbn9gcA39EiMKWblF9uKSPL20LQqJcYdOC94V1eyjh3qSra', '2017-03-03 00:00:00', '2017-02-21 08:41:19', '2017-03-02 10:36:10', '2017-03-02 10:36:10'),
(8, 'App\\Education\\Institution', 6, 'Red Fern Test', 'red_fern_test', 10, '1.0000', 'year', 1, '$2y$10$mZPvpvUA2ADEXpd4otTtOhBlbdMgoRfQy7fIY.mk4reJvaJdus2C', '2017-04-30 00:00:00', '2017-02-28 11:38:42', '2017-02-28 12:00:56', '2017-02-28 12:00:56'),
(9, 'App\\Education\\Institution', 15, 'College of CVVID', 'college_of_cvvid', 25, '1.0000', 'year', 1, '$2y$10$GOAT3OmZbbx0qh92FjVZeeX.dHS4ICjfVAtY5Z7tC3GN9x5k2dOeK', '2017-04-28 00:00:00', '2017-04-11 08:37:00', '2017-04-11 10:30:39', '2017-04-11 10:30:39'),
(10, 'App\\Education\\Institution', 17, 'JS School', 'js_school', 50, '100.0000', 'month', 1, '$2y$10$o371R2Qra6o38jYcl1hu8OSa0T1e732ENuMui5I359aE5sIyc2Qq', '2017-09-08 00:00:00', '2017-08-24 11:51:46', '2017-08-24 11:51:47', NULL),
(11, 'Institution', 2, 'College of A Bangi', 'college_of_a_bangi', 16, '0.0000', 'month', 1, NULL, '2018-01-02 12:12:41', '2018-01-02 06:42:41', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cv16_profiles`
--

CREATE TABLE `cv16_profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `owner_id` int(10) UNSIGNED NOT NULL,
  `owner_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `industry_id` int(10) UNSIGNED DEFAULT NULL,
  `photo_id` int(10) UNSIGNED DEFAULT NULL,
  `cover_id` int(10) UNSIGNED DEFAULT NULL,
  `visibility` int(11) DEFAULT '1',
  `published` tinyint(1) DEFAULT '1',
  `num_likes` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `num_views` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_profiles`
--

INSERT INTO `cv16_profiles` (`id`, `owner_id`, `owner_type`, `type`, `slug`, `industry_id`, `photo_id`, `cover_id`, `visibility`, `published`, `num_likes`, `num_views`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 'App\\Users\\Candidate', 'candidate', 'Ariff-Bangi', NULL, 6, 5, 1, 1, 2, 49, '2016-03-14 13:12:46', '2018-01-24 02:26:55', NULL),
(3, 5, 'App\\Users\\Candidate', 'candidate', 'alex-heeney', NULL, 80, NULL, 1, 1, 1, 31, '2016-03-18 17:10:57', '2018-01-23 02:47:42', NULL),
(6, 10, 'App\\Users\\Candidate', 'candidate', 'austin-lindsay', NULL, 82, NULL, 1, 1, 0, 31, '2016-04-29 13:32:40', '2018-01-23 02:47:42', NULL),
(7, 3, 'App\\Employer', 'employer', 'apple', NULL, 4, 9, 1, 1, 0, 31, '2016-06-15 15:08:03', '2018-01-23 02:47:42', NULL),
(13, 21, 'App\\Users\\Candidate', 'candidate', 'jane-smith', NULL, NULL, NULL, 1, 1, 0, 31, '2016-07-08 14:24:03', '2018-01-23 02:47:42', NULL),
(14, 22, 'App\\Users\\Candidate', 'candidate', 'naeem-hajee', NULL, NULL, NULL, 1, 1, 0, 31, '2016-09-25 11:44:02', '2018-01-23 02:47:42', NULL),
(15, 23, 'App\\Users\\Candidate', 'candidate', 'aysha-youssouf', NULL, NULL, NULL, 1, 1, 0, 31, '2016-10-01 16:06:11', '2018-01-23 02:47:42', NULL),
(16, 24, 'App\\Users\\Candidate', 'candidate', 'ahmed-youssouf', NULL, NULL, NULL, 1, 1, 0, 31, '2016-10-02 21:53:41', '2018-01-23 02:47:42', NULL),
(17, 25, 'App\\Users\\Candidate', 'candidate', 'zeeshan-anwar', NULL, NULL, NULL, 1, 1, 0, 31, '2016-10-03 18:37:45', '2018-01-23 02:47:42', NULL),
(18, 4, 'App\\Employer', 'employer', 'andersonwyatt', NULL, NULL, NULL, 1, 1, 0, 31, '2016-10-03 19:48:35', '2018-01-23 02:47:42', NULL),
(19, 27, 'App\\Users\\Candidate', 'candidate', 'chrissi-hyde', NULL, NULL, NULL, 1, 1, 0, 31, '2016-10-22 11:43:28', '2018-01-23 02:47:42', NULL),
(20, 28, 'App\\Users\\Candidate', 'candidate', 'jkruger', NULL, NULL, NULL, 1, 1, 0, 31, '2016-11-08 08:03:25', '2018-01-23 02:47:42', NULL),
(21, 5, 'App\\Employer', 'employer', 'wwwpsemcouk', NULL, 122, NULL, 1, 1, 0, 31, '2016-11-10 22:18:27', '2018-01-23 02:47:42', NULL),
(22, 30, 'App\\Users\\Candidate', 'candidate', 'hasina-jogee', NULL, NULL, 6, 1, 1, 0, 31, '2016-11-15 23:20:12', '2018-01-23 02:47:42', NULL),
(23, 31, 'App\\Users\\Candidate', 'candidate', 'tayyab-sarwar', NULL, NULL, NULL, 1, 1, 0, 31, '2016-12-15 12:13:39', '2018-01-23 02:47:42', NULL),
(24, 6, 'App\\Employer', 'employer', 'mykitchenbakery', NULL, 125, 125, 1, 1, 0, 31, '2016-12-30 16:38:14', '2018-01-23 02:47:42', NULL),
(25, 7, 'App\\Employer', 'employer', 'red-fern', NULL, NULL, NULL, 1, 1, 0, 31, '2017-01-13 15:21:30', '2018-01-23 02:47:42', NULL),
(26, 8, 'App\\Employer', 'employer', 'cvvid', NULL, 124, NULL, 1, 1, 0, 31, '2017-01-18 09:29:13', '2018-01-23 02:47:42', NULL),
(28, 9, 'App\\Employer', 'employer', 'fonezone', NULL, NULL, NULL, 1, 1, 0, 31, '2017-01-20 21:34:27', '2018-01-23 02:47:42', NULL),
(29, 10, 'App\\Employer', 'employer', 'printhubdesign', NULL, 143, NULL, 1, 1, 0, 31, '2017-01-24 15:33:05', '2018-01-23 02:47:42', NULL),
(30, 40, 'App\\Users\\Candidate', 'candidate', 'hasina-bangi', NULL, NULL, 4, 1, 1, 0, 31, '2017-01-26 11:55:39', '2018-01-23 02:47:42', NULL),
(31, 11, 'App\\Employer', 'employer', 'employer-test', NULL, NULL, NULL, 1, 1, 0, 31, '2017-01-27 16:03:40', '2018-01-23 02:47:42', '2017-12-14 02:28:46'),
(32, 12, 'App\\Employer', 'employer', 'tienda-digital', NULL, NULL, NULL, 1, 1, 0, 31, '2017-01-30 10:05:21', '2018-01-23 02:47:42', NULL),
(34, 13, 'App\\Employer', 'employer', 'abtech', NULL, NULL, NULL, 1, 1, 0, 31, '2017-02-01 12:13:44', '2018-01-23 02:47:42', NULL),
(35, 14, 'App\\Employer', 'employer', 'marblemanagement', NULL, 144, NULL, 1, 1, 0, 31, '2017-02-01 18:59:02', '2018-01-23 02:47:42', NULL),
(36, 15, 'App\\Employer', 'employer', 'aniruddh-ds', NULL, NULL, NULL, 1, 1, 0, 31, '2017-02-02 12:51:12', '2018-01-23 02:47:42', '2017-12-14 05:18:52'),
(37, 16, 'App\\Employer', 'employer', 'novaadventures', NULL, 180, NULL, 1, 1, 0, 31, '2017-02-02 16:10:28', '2018-01-23 02:47:42', NULL),
(38, 48, 'App\\Users\\Candidate', 'candidate', 'joe-emery', NULL, NULL, NULL, 0, 0, 0, 31, '2017-02-10 21:20:28', '2018-01-23 02:47:42', NULL),
(40, 17, 'App\\Employer', 'employer', 'human-appeal', NULL, NULL, NULL, 0, 0, 0, 31, '2017-02-14 10:40:05', '2018-01-23 02:47:42', NULL),
(41, 51, 'App\\Users\\Candidate', 'candidate', 'sadiq-basha', NULL, NULL, NULL, 1, 1, 0, 31, '2017-02-16 16:18:36', '2018-01-23 02:47:42', NULL),
(43, 2, 'App\\Education\\Institution', 'institution', 'college-of-a-bangi', NULL, 6, 4, 1, 1, 0, 31, '2017-02-20 22:22:48', '2018-01-23 02:47:42', NULL),
(44, 3, 'App\\Education\\Institution', 'institution', 'tighs', NULL, 133, 9, 1, 1, 0, 31, '2017-02-21 08:34:39', '2018-01-23 02:47:42', NULL),
(47, 19, 'App\\Employer', 'employer', 'the-brand-alchemy', NULL, NULL, NULL, 1, 1, 0, 31, '2017-02-21 17:15:10', '2018-01-23 02:47:42', NULL),
(48, 6, 'App\\Education\\Institution', 'institution', 'red-fern-test', NULL, 263, NULL, 1, 1, 0, 31, '2017-02-28 11:37:56', '2018-01-23 02:47:42', NULL),
(51, 7, 'App\\Education\\Institution', 'institution', 'georgepep', NULL, NULL, NULL, 1, 1, 0, 31, '2017-03-21 13:19:00', '2018-01-23 02:47:42', NULL),
(52, 8, 'App\\Education\\Institution', 'institution', 'williamvax', NULL, NULL, NULL, 1, 1, 0, 31, '2017-03-21 13:53:34', '2018-01-23 02:47:42', NULL),
(53, 9, 'App\\Education\\Institution', 'institution', 'robertidody', NULL, NULL, NULL, 1, 1, 0, 31, '2017-03-21 14:21:42', '2018-01-23 02:47:42', NULL),
(55, 78, 'App\\Users\\Candidate', 'candidate', 'bashira-patel', NULL, NULL, NULL, 0, 1, 0, 31, '2017-03-24 08:50:28', '2018-01-23 02:47:42', NULL),
(58, 10, 'App\\Education\\Institution', 'institution', 'danielbek', NULL, NULL, NULL, 1, 1, 0, 31, '2017-03-24 19:59:09', '2018-01-23 02:47:42', NULL),
(59, 11, 'App\\Education\\Institution', 'institution', 'jameswew', NULL, NULL, NULL, 1, 1, 0, 31, '2017-03-26 12:01:35', '2018-01-23 02:47:42', NULL),
(61, 12, 'App\\Education\\Institution', 'institution', 'darwinhuh', NULL, NULL, NULL, 1, 1, 0, 31, '2017-03-30 01:49:59', '2018-01-23 02:47:42', NULL),
(62, 13, 'App\\Education\\Institution', 'institution', 'arthurwap', NULL, NULL, NULL, 1, 1, 0, 31, '2017-03-30 13:43:20', '2018-01-23 02:47:42', NULL),
(63, 14, 'App\\Education\\Institution', 'institution', 'ervincet', NULL, NULL, NULL, 1, 1, 0, 31, '2017-03-30 17:07:09', '2018-01-23 02:47:42', NULL),
(64, 15, 'App\\Education\\Institution', 'institution', 'college-of-cvvid', NULL, NULL, NULL, 1, 1, 0, 31, '2017-04-11 08:36:59', '2018-01-23 02:47:42', NULL),
(66, 90, 'App\\Users\\Candidate', 'candidate', 'adam-henderson', NULL, NULL, NULL, 1, 1, 0, 31, '2017-04-11 11:32:09', '2018-01-23 02:47:42', NULL),
(67, 91, 'App\\Users\\Candidate', 'candidate', 'stefanie-collins', NULL, NULL, NULL, 1, 1, 0, 31, '2017-04-16 10:52:24', '2018-01-23 02:47:42', NULL),
(68, 92, 'App\\Users\\Candidate', 'candidate', 'paul-williams', NULL, 142, NULL, 1, 1, 0, 31, '2017-04-16 11:07:47', '2018-01-23 02:47:42', NULL),
(71, 98, 'App\\Users\\Candidate', 'candidate', 'porsche-blue', NULL, NULL, NULL, 1, 1, 0, 31, '2017-04-16 11:45:46', '2018-01-23 02:47:42', NULL),
(72, 99, 'App\\Users\\Candidate', 'candidate', 'samuel-thornton-greet', NULL, 15, NULL, 1, 1, 0, 31, '2017-04-27 19:13:15', '2018-01-23 02:47:42', NULL),
(77, 105, 'App\\Users\\Candidate', 'candidate', 'ayah-shah', NULL, NULL, NULL, 1, 1, 0, 31, '2017-06-14 06:32:36', '2018-01-23 02:47:42', NULL),
(78, 106, 'App\\Users\\Candidate', 'candidate', 'sara-ziglam', NULL, NULL, NULL, 1, 1, 0, 31, '2017-06-14 06:33:23', '2018-01-23 02:47:42', NULL),
(79, 107, 'App\\Users\\Candidate', 'candidate', 'arifa-patel', NULL, NULL, NULL, 1, 1, 0, 31, '2017-06-14 06:34:58', '2018-01-23 02:47:42', NULL),
(80, 108, 'App\\Users\\Candidate', 'candidate', 'maryam-sharief', NULL, NULL, NULL, 1, 1, 0, 31, '2017-06-14 06:35:54', '2018-01-23 02:47:42', NULL),
(81, 109, 'App\\Users\\Candidate', 'candidate', 'khansa-iqbal', NULL, NULL, NULL, 0, 1, 0, 31, '2017-06-14 06:36:36', '2018-01-23 02:47:42', NULL),
(82, 110, 'App\\Users\\Candidate', 'candidate', 'fatimah-zahra-patel', NULL, NULL, NULL, 1, 1, 0, 31, '2017-06-14 06:37:45', '2018-01-23 02:47:42', NULL),
(83, 111, 'App\\Users\\Candidate', 'candidate', 'maryam-patel', NULL, NULL, NULL, 1, 1, 0, 31, '2017-06-14 06:38:33', '2018-01-23 02:47:42', NULL),
(84, 112, 'App\\Users\\Candidate', 'candidate', 'hafsah-patel', NULL, NULL, NULL, 1, 1, 0, 31, '2017-06-14 06:39:57', '2018-01-23 02:47:42', NULL),
(85, 113, 'App\\Users\\Candidate', 'candidate', 'aisha-daya', NULL, NULL, NULL, 1, 1, 0, 31, '2017-06-14 06:40:55', '2018-01-23 02:47:42', NULL),
(86, 114, 'App\\Users\\Candidate', 'candidate', 'aaiza-patel', NULL, NULL, NULL, 1, 1, 0, 31, '2017-06-14 06:41:43', '2018-01-23 02:47:42', NULL),
(87, 115, 'App\\Users\\Candidate', 'candidate', 'test-user', NULL, NULL, NULL, 1, 1, 0, 31, '2017-06-15 11:23:32', '2018-01-23 02:47:42', NULL),
(88, 116, 'App\\Users\\Candidate', 'candidate', 'candidate-surenam', NULL, NULL, NULL, 1, 1, 0, 31, '2017-06-15 11:39:09', '2018-01-23 02:47:42', NULL),
(89, 117, 'App\\Users\\Candidate', 'candidate', 'john-doe', NULL, NULL, NULL, 1, 1, 0, 31, '2017-06-20 12:20:03', '2018-01-23 02:47:42', NULL),
(90, 118, 'App\\Users\\Candidate', 'candidate', 'david-althar', NULL, NULL, NULL, 1, 1, 0, 31, '2017-07-26 08:49:53', '2018-01-23 02:47:42', NULL),
(91, 119, 'App\\Users\\Candidate', 'candidate', 'j-p', NULL, NULL, NULL, 1, 1, 1, 31, '2017-07-27 03:51:50', '2018-01-23 02:47:42', NULL),
(92, 120, 'App\\Users\\Candidate', 'candidate', 'ben-lind', NULL, NULL, NULL, 1, 1, 0, 31, '2017-07-27 08:07:04', '2018-01-23 02:47:42', NULL),
(93, 20, 'App\\Employer', 'employer', 'test', NULL, NULL, NULL, 1, 1, 0, 31, '2017-07-27 09:44:26', '2018-01-23 02:47:42', NULL),
(94, 16, 'App\\Education\\Institution', 'institution', 'test', NULL, NULL, NULL, 1, 1, 0, 31, '2017-07-27 12:04:01', '2018-01-23 02:47:42', NULL),
(95, 124, 'App\\Users\\Candidate', 'candidate', 'shyamtests247', NULL, NULL, NULL, 1, 1, 0, 31, '2017-08-14 12:05:32', '2018-01-23 02:47:42', NULL),
(96, 21, 'App\\Employer', 'employer', 'wwwssccom', NULL, NULL, NULL, 1, 1, 0, 31, '2017-08-14 12:15:57', '2018-01-23 02:47:42', NULL),
(97, 126, 'App\\Users\\Candidate', 'candidate', 'jeff-jaq', NULL, NULL, NULL, 1, 1, 0, 31, '2017-08-14 18:15:05', '2018-01-23 02:47:42', NULL),
(98, 127, 'App\\Users\\Candidate', 'candidate', 'dev1-tienda', NULL, NULL, NULL, 1, 1, 0, 31, '2017-08-15 15:56:28', '2018-01-23 02:47:42', NULL),
(99, 22, 'App\\Employer', 'employer', 'tienda-digital-emp', NULL, NULL, NULL, 1, 1, 0, 31, '2017-08-15 16:05:28', '2018-01-23 02:47:42', NULL),
(100, 129, 'App\\Users\\Candidate', 'candidate', 'eleisha-cartlidge', NULL, NULL, NULL, 1, 1, 0, 31, '2017-08-22 07:50:59', '2018-01-23 02:47:42', NULL),
(101, 130, 'App\\Users\\Candidate', 'candidate', 'martin-mulvey-1', NULL, NULL, NULL, 1, 1, 0, 31, '2017-08-22 08:37:30', '2018-01-23 02:47:42', NULL),
(102, 23, 'App\\Employer', 'employer', 'tienda-creative', NULL, NULL, NULL, 1, 1, 0, 31, '2017-08-23 12:00:49', '2018-01-23 02:47:42', NULL),
(103, 24, 'App\\Employer', 'employer', 'digital-tienda', NULL, NULL, NULL, 1, 1, 0, 31, '2017-08-23 12:38:27', '2018-01-23 02:47:42', NULL),
(104, 17, 'App\\Education\\Institution', 'institution', 'js-school', NULL, NULL, NULL, 1, 1, 0, 31, '2017-08-24 11:46:13', '2018-01-23 02:47:42', NULL),
(105, 135, 'App\\Users\\Candidate', 'candidate', 'eleisha-cartlidge-1', NULL, NULL, NULL, 1, 1, 0, 31, '2017-08-24 13:10:03', '2018-01-23 02:47:42', NULL),
(106, 137, 'App\\Users\\Candidate', 'candidate', 'martin-mulvey-1', NULL, NULL, NULL, 1, 1, 0, 31, '2017-08-29 08:26:07', '2018-01-23 02:47:42', NULL),
(107, 138, 'App\\Users\\Candidate', 'candidate', 'anastasia-barashkova', NULL, NULL, NULL, 1, 1, 0, 31, '2017-09-11 08:54:10', '2018-01-23 02:47:42', NULL),
(108, 141, '', 'candidate', 'ayan', NULL, NULL, NULL, 1, 1, 0, 31, '2017-10-07 08:00:49', '2018-01-23 02:47:42', NULL),
(109, 25, 'employer', 'employer', 'Samsung', NULL, 238, NULL, 1, 1, 0, 31, '0000-00-00 00:00:00', '2018-01-23 02:47:42', NULL),
(110, 26, 'employer', 'employer', 'pms', NULL, NULL, NULL, 1, 1, 0, 31, '0000-00-00 00:00:00', '2018-01-23 02:47:42', NULL),
(111, 27, 'employer', 'employer', 'pms', NULL, NULL, NULL, 1, 1, 0, 31, '0000-00-00 00:00:00', '2018-01-23 02:47:42', NULL),
(112, 158, 'Candidate', 'candidate', 'pms', NULL, NULL, NULL, 1, 1, 0, 31, '2017-12-08 01:47:37', '2018-01-23 02:47:42', NULL),
(113, 159, 'Candidate', 'candidate', 'pms', NULL, NULL, NULL, 1, 1, 0, 31, '2017-12-08 02:03:12', '2018-01-23 02:47:42', NULL),
(114, 161, 'Candidate', 'candidate', 'Tony-Stark', NULL, NULL, NULL, 1, 1, 0, 31, '2017-12-23 05:53:10', '2018-01-23 02:47:42', NULL),
(115, 162, 'Candidate', 'candidate', 'Steve-Rogers', NULL, NULL, NULL, 1, 1, 0, 31, '2017-12-23 05:53:10', '2018-01-23 02:47:42', NULL),
(116, 163, 'Candidate', 'candidate', 'Bruce-Banner', NULL, NULL, NULL, 1, 1, 0, 31, '2017-12-23 05:53:10', '2018-01-23 02:47:42', NULL),
(117, 164, 'Candidate', 'candidate', 'Maria-Hill', NULL, NULL, NULL, 1, 1, 0, 31, '2017-12-23 05:53:10', '2018-01-23 02:47:42', NULL),
(118, 165, 'Candidate', 'candidate', 'Clint-Barton', NULL, NULL, NULL, 1, 1, 0, 31, '2017-12-23 05:53:10', '2018-01-23 02:47:42', NULL),
(122, 30, 'employer', 'employer', 'sfsfsf', NULL, NULL, NULL, 1, 1, 0, 31, '0000-00-00 00:00:00', '2018-01-23 02:47:42', NULL),
(123, 31, 'employer', 'employer', 'dadadad', NULL, NULL, NULL, 1, 1, 0, 31, '0000-00-00 00:00:00', '2018-01-23 02:47:42', NULL),
(124, 33, 'employer', 'employer', 'dadadadscs', NULL, NULL, NULL, 1, 1, 0, 31, '0000-00-00 00:00:00', '2018-01-23 02:47:42', NULL),
(125, 34, 'employer', 'employer', 'dadadadscsw', NULL, NULL, NULL, 1, 1, 0, 31, '0000-00-00 00:00:00', '2018-01-23 02:47:42', NULL),
(126, 35, 'employer', 'employer', 'dadadadscsww', NULL, NULL, NULL, 1, 1, 0, 31, '0000-00-00 00:00:00', '2018-01-23 02:47:42', NULL),
(127, 36, 'employer', 'employer', 'dadadadscswwadad', NULL, NULL, NULL, 1, 1, 0, 31, '0000-00-00 00:00:00', '2018-01-23 02:47:42', NULL),
(128, 37, 'employer', 'employer', 'zdazda', NULL, NULL, NULL, 1, 1, 0, 31, '0000-00-00 00:00:00', '2018-01-23 02:47:42', NULL),
(129, 38, 'employer', 'employer', 'aadada', NULL, NULL, NULL, 1, 1, 0, 31, '0000-00-00 00:00:00', '2018-01-23 02:47:42', NULL),
(130, 0, 'Candidate', 'candidate', 'asdasd-asssdsa', NULL, NULL, NULL, 1, 1, 0, 31, '2018-01-18 03:29:47', '2018-01-23 02:47:42', NULL),
(131, 181, 'Candidate', 'candidate', 'asdasds-asssdsas', NULL, NULL, NULL, 1, 1, 0, 31, '2018-01-18 03:30:15', '2018-01-23 02:47:42', NULL),
(132, 183, 'Candidate', 'candidate', 'qa-wqw', NULL, NULL, NULL, 1, 1, 0, 31, '2018-01-18 03:36:01', '2018-01-23 02:47:42', NULL),
(133, 184, 'Candidate', 'candidate', 'va-we', NULL, NULL, NULL, 1, 1, 0, 31, '2018-01-18 03:41:35', '2018-01-23 02:47:42', NULL),
(134, 0, 'Candidate', 'candidate', 'asafaf-agga', NULL, NULL, NULL, 1, 1, 0, 0, '2018-01-23 10:36:34', '0000-00-00 00:00:00', NULL),
(135, 0, 'Candidate', 'candidate', 'asafaf-aggag', NULL, NULL, NULL, 1, 1, 0, 0, '2018-01-23 10:38:25', '0000-00-00 00:00:00', NULL),
(136, 0, 'Candidate', 'candidate', 'assafaf-gagg', NULL, NULL, NULL, 1, 1, 0, 0, '2018-01-24 02:25:59', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cv16_profile_documents`
--

CREATE TABLE `cv16_profile_documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `profile_id` int(10) UNSIGNED NOT NULL,
  `user_document_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_profile_documents`
--

INSERT INTO `cv16_profile_documents` (`id`, `profile_id`, `user_document_id`, `created_at`, `updated_at`) VALUES
(6, 30, 6, '2017-12-22 04:51:58', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cv16_profile_experiences`
--

CREATE TABLE `cv16_profile_experiences` (
  `id` int(10) UNSIGNED NOT NULL,
  `profile_id` int(10) UNSIGNED NOT NULL,
  `user_experience_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_profile_experiences`
--

INSERT INTO `cv16_profile_experiences` (`id`, `profile_id`, `user_experience_id`, `created_at`, `updated_at`) VALUES
(5, 1, 5, '2016-06-13 11:31:54', '2016-06-13 11:31:54'),
(6, 1, 6, '2016-06-23 08:16:18', '2016-06-23 08:16:18'),
(7, 30, 7, '2017-01-26 17:02:36', '2017-12-22 01:28:08'),
(8, 30, 8, '2017-01-26 17:05:57', '2017-12-22 01:28:27'),
(9, 41, 9, '2017-02-16 16:20:26', '2017-02-16 16:20:26'),
(10, 66, 10, '2017-04-11 11:42:06', '2017-04-11 11:42:06'),
(11, 87, 11, '2017-06-15 11:26:51', '2017-06-15 11:26:51'),
(12, 88, 12, '2017-06-15 11:41:25', '2017-06-15 11:41:25'),
(13, 100, 13, '2017-08-22 08:09:11', '2017-08-22 08:09:11');

-- --------------------------------------------------------

--
-- Table structure for table `cv16_profile_favourites`
--

CREATE TABLE `cv16_profile_favourites` (
  `profile_id` int(10) UNSIGNED NOT NULL,
  `favourite_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_profile_favourites`
--

INSERT INTO `cv16_profile_favourites` (`profile_id`, `favourite_id`) VALUES
(36, 1),
(93, 3),
(93, 91),
(110, 67),
(124, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cv16_profile_hobbies`
--

CREATE TABLE `cv16_profile_hobbies` (
  `id` int(10) UNSIGNED NOT NULL,
  `profile_id` int(10) UNSIGNED NOT NULL,
  `user_hobby_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_profile_hobbies`
--

INSERT INTO `cv16_profile_hobbies` (`id`, `profile_id`, `user_hobby_id`, `created_at`, `updated_at`) VALUES
(13, 1, 13, '2016-06-23 08:17:48', '2016-06-23 08:17:48'),
(14, 30, 14, '2017-01-26 11:57:57', '2017-12-22 02:03:48'),
(15, 30, 15, '2017-01-26 17:03:29', '2017-01-26 17:03:29'),
(16, 41, 16, '2017-02-16 16:20:54', '2017-02-16 16:20:54'),
(17, 3, 17, '2017-03-31 08:04:25', '2017-03-31 08:04:25'),
(18, 66, 18, '2017-04-11 11:45:54', '2017-04-11 11:45:54'),
(19, 66, 19, '2017-04-11 11:48:22', '2017-04-11 11:48:22'),
(20, 87, 20, '2017-06-15 11:26:12', '2017-06-15 11:26:12'),
(21, 88, 21, '2017-06-15 11:41:41', '2017-06-15 11:41:41'),
(22, 100, 22, '2017-08-22 08:14:14', '2017-08-22 08:14:14');

-- --------------------------------------------------------

--
-- Table structure for table `cv16_profile_languages`
--

CREATE TABLE `cv16_profile_languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `profile_id` int(10) UNSIGNED NOT NULL,
  `user_language_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_profile_languages`
--

INSERT INTO `cv16_profile_languages` (`id`, `profile_id`, `user_language_id`, `created_at`, `updated_at`) VALUES
(1, 16, 1, '2016-10-02 21:54:43', '2016-10-02 21:54:43'),
(2, 16, 2, '2016-10-02 21:54:55', '2016-10-02 21:54:55'),
(3, 16, 3, '2016-10-02 21:55:04', '2016-10-02 21:55:04'),
(4, 22, 4, '2017-01-17 16:24:10', '2017-01-17 16:24:10'),
(5, 22, 5, '2017-01-17 16:24:18', '2017-01-17 16:24:18'),
(6, 22, 6, '2017-01-17 16:24:27', '2017-01-17 16:24:27'),
(7, 30, 7, '2017-01-26 16:36:13', '2017-01-26 16:36:13'),
(9, 30, 9, '2017-01-26 16:36:42', '2017-01-26 16:36:42'),
(10, 41, 10, '2017-02-16 16:20:47', '2017-02-16 16:20:47'),
(11, 66, 11, '2017-04-11 11:48:32', '2017-04-11 11:48:32'),
(12, 66, 12, '2017-04-11 11:48:42', '2017-04-11 11:48:42'),
(13, 67, 13, '2017-05-20 12:46:32', '2017-05-20 12:46:32'),
(14, 67, 14, '2017-05-20 12:46:41', '2017-05-20 12:46:41'),
(15, 87, 15, '2017-06-15 11:25:35', '2017-06-15 11:25:35'),
(16, 88, 16, '2017-06-15 11:41:48', '2017-06-15 11:41:48'),
(17, 88, 17, '2017-06-15 11:41:58', '2017-06-15 11:41:58'),
(18, 100, 18, '2017-08-22 08:14:46', '2017-08-22 08:14:46');

-- --------------------------------------------------------

--
-- Table structure for table `cv16_profile_qualifications`
--

CREATE TABLE `cv16_profile_qualifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `profile_id` int(10) UNSIGNED NOT NULL,
  `user_qualification_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_profile_qualifications`
--

INSERT INTO `cv16_profile_qualifications` (`id`, `profile_id`, `user_qualification_id`, `created_at`, `updated_at`) VALUES
(2, 1, 2, '2016-06-13 11:26:52', '2016-06-13 11:26:52'),
(3, 1, 3, '2016-06-23 08:17:20', '2016-06-23 08:17:20'),
(4, 41, 4, '2017-02-16 16:21:24', '2017-02-16 16:21:24'),
(5, 30, 5, '2017-03-17 09:06:12', '2017-12-22 01:50:19'),
(6, 66, 6, '2017-04-11 11:37:42', '2017-04-11 11:37:42'),
(7, 66, 7, '2017-04-11 11:38:30', '2017-04-11 11:38:30'),
(8, 87, 8, '2017-06-15 11:26:26', '2017-06-15 11:26:26'),
(9, 88, 9, '2017-06-15 11:41:35', '2017-06-15 11:41:35'),
(10, 100, 10, '2017-08-22 08:12:49', '2017-08-22 08:12:49');

-- --------------------------------------------------------

--
-- Table structure for table `cv16_profile_skills`
--

CREATE TABLE `cv16_profile_skills` (
  `profile_id` int(10) UNSIGNED NOT NULL,
  `skill_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_profile_skills`
--

INSERT INTO `cv16_profile_skills` (`profile_id`, `skill_id`) VALUES
(1, 30),
(1, 29),
(1, 34),
(16, 301),
(22, 62),
(22, 67),
(30, 67),
(41, 149),
(66, 63),
(66, 64),
(66, 67),
(66, 244),
(66, 253),
(66, 357),
(66, 246),
(67, 229),
(67, 227),
(67, 151),
(67, 150),
(88, 164),
(88, 165),
(88, 168),
(90, 89),
(90, 85),
(90, 90),
(90, 172),
(90, 173),
(91, 167),
(91, 172),
(91, 164),
(91, 168),
(91, 91),
(98, 242),
(98, 243),
(100, 149),
(100, 151),
(107, 166),
(107, 168),
(1, 267),
(1, 269),
(1, 150),
(1, 167),
(1, 224),
(1, 226),
(1, 265),
(1, 13),
(1, 14),
(1, 164),
(1, 15),
(1, 270),
(1, 32),
(1, 335),
(1, 336),
(1, 332),
(1, 31),
(1, 31),
(30, 149),
(30, 150),
(30, 149),
(30, 150),
(30, 167),
(30, 168),
(30, 169),
(30, 85),
(30, 86),
(30, 226),
(30, 164),
(30, 165),
(30, 64),
(1, 16);

-- --------------------------------------------------------

--
-- Table structure for table `cv16_profile_videos`
--

CREATE TABLE `cv16_profile_videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `profile_id` int(10) UNSIGNED NOT NULL,
  `video_id` int(10) UNSIGNED NOT NULL,
  `is_default` int(11) NOT NULL DEFAULT '0',
  `state` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_profile_videos`
--

INSERT INTO `cv16_profile_videos` (`id`, `profile_id`, `video_id`, `is_default`, `state`, `created_at`, `updated_at`) VALUES
(7, 1, 22, 1, 1, '2017-02-03 11:26:12', '2017-02-03 11:43:50'),
(8, 3, 23, 0, 1, '2017-03-17 10:51:22', '2017-05-21 09:43:24'),
(9, 1, 24, 0, 1, '2017-03-17 11:25:15', '2017-03-17 11:25:15'),
(11, 67, 26, 1, 0, '2017-05-20 12:35:02', '2017-05-22 09:29:44'),
(15, 66, 30, 0, 1, '2017-05-22 09:02:23', '2017-05-22 09:30:38'),
(16, 71, 31, 1, 1, '2017-05-22 09:05:19', '2017-05-22 09:28:50'),
(17, 68, 32, 0, 1, '2017-05-22 09:09:35', '2017-05-22 09:29:00'),
(19, 3, 37, 1, 1, '2017-06-07 15:28:23', '2017-06-07 15:28:23'),
(20, 3, 38, 0, 1, '2017-06-08 06:37:13', '2017-06-08 06:51:52'),
(21, 88, 39, 0, 1, '2017-06-15 11:40:01', '2017-06-15 11:40:01'),
(22, 77, 40, 0, 1, '2017-07-12 07:50:22', '2017-07-12 07:50:22'),
(23, 78, 41, 0, 1, '2017-07-12 07:52:39', '2017-07-12 07:52:39'),
(24, 79, 42, 0, 1, '2017-07-12 07:54:55', '2017-07-12 07:54:55'),
(26, 83, 44, 0, 1, '2017-07-12 08:05:41', '2017-07-12 08:05:41'),
(27, 80, 45, 0, 1, '2017-07-12 08:07:52', '2017-07-12 08:07:52'),
(28, 84, 49, 0, 1, '2017-07-12 09:39:30', '2017-07-12 09:39:30'),
(29, 85, 50, 0, 1, '2017-07-12 09:42:38', '2017-07-12 09:42:38'),
(30, 86, 51, 0, 1, '2017-07-12 09:45:09', '2017-07-12 09:45:09'),
(31, 67, 54, 0, 1, '2017-07-13 06:06:24', '2017-07-13 06:06:24'),
(32, 30, 59, 0, 1, '2017-12-22 02:37:08', '0000-00-00 00:00:00'),
(33, 30, 60, 0, 1, '2017-12-22 02:44:00', '0000-00-00 00:00:00'),
(34, 30, 61, 0, 1, '2017-12-22 02:56:57', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cv16_profile_views`
--

CREATE TABLE `cv16_profile_views` (
  `id` int(10) UNSIGNED NOT NULL,
  `profile_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `model_id` int(10) UNSIGNED NOT NULL,
  `view_count` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_profile_views`
--

INSERT INTO `cv16_profile_views` (`id`, `profile_id`, `model_type`, `model_id`, `view_count`, `created_at`, `updated_at`) VALUES
(1, 1, 'App\\Users\\Candidate', 5, 1, '2016-03-18 17:11:13', '2016-03-18 17:11:13'),
(5, 21, 'App\\Users\\Employer', 29, 2, '2016-11-10 22:22:51', '2016-11-10 22:31:42'),
(6, 26, 'App\\Users\\Employer', 34, 1, '2017-01-18 09:45:07', '2017-01-18 09:45:07'),
(7, 30, 'App\\Users\\Employer', 32, 2, '2017-01-26 16:57:35', '2017-01-26 17:06:56'),
(8, 1, 'App\\Users\\Employer', 42, 2, '2017-01-30 10:06:21', '2017-08-14 12:30:17'),
(9, 34, 'App\\Users\\Employer', 44, 1, '2017-02-01 12:28:55', '2017-02-01 12:28:55'),
(10, 36, 'App\\Users\\Employer', 46, 1, '2017-02-02 12:51:48', '2017-02-02 12:51:48'),
(11, 1, 'App\\Users\\Employer', 46, 1, '2017-02-02 13:01:33', '2017-02-02 13:01:33'),
(12, 23, 'App\\Users\\Admin', 2, 14, '2017-02-02 14:54:01', '2017-12-30 08:08:56'),
(13, 1, 'App\\Users\\Employer', 33, 2, '2017-02-03 10:25:35', '2017-02-17 10:09:50'),
(14, 38, 'App\\Users\\Employer', 34, 2, '2017-02-15 12:19:52', '2017-02-24 17:15:50'),
(15, 41, 'App\\Users\\Employer', 33, 1, '2017-02-17 10:09:16', '2017-02-17 10:09:16'),
(16, 37, 'App\\Users\\Candidate', 3, 1, '2017-02-17 10:11:29', '2017-02-17 10:11:29'),
(17, 26, 'App\\Users\\Candidate', 3, 1, '2017-02-17 10:11:45', '2017-02-17 10:11:45'),
(18, 1, 'App\\Users\\Admin', 2, 38, '2017-02-17 14:55:08', '2018-01-23 08:50:49'),
(19, 47, 'App\\Users\\Admin', 2, 1, '2017-02-21 17:17:34', '2017-02-21 17:17:34'),
(20, 25, 'App\\Users\\Employer', 33, 1, '2017-02-24 16:36:30', '2017-02-24 16:36:30'),
(21, 44, 'App\\Education\\InstitutionAdmin', 54, 8, '2017-03-02 10:44:09', '2017-09-15 07:46:31'),
(22, 25, 'App\\Users\\Candidate', 3, 1, '2017-03-08 14:13:22', '2017-03-08 14:13:22'),
(23, 30, 'App\\Users\\Employer', 59, 1, '2017-03-17 09:30:15', '2017-03-17 09:30:15'),
(24, 44, 'App\\Users\\Admin', 2, 1, '2017-03-17 09:38:59', '2017-03-17 09:38:59'),
(25, 30, 'App\\Users\\Employer', 34, 1, '2017-03-31 07:05:30', '2017-03-31 07:05:30'),
(26, 64, 'App\\Education\\InstitutionAdmin', 87, 6, '2017-04-11 10:33:29', '2017-09-12 08:34:07'),
(27, 67, 'App\\Education\\InstitutionAdmin', 87, 1, '2017-05-22 09:29:12', '2017-05-22 09:29:12'),
(28, 71, 'App\\Education\\InstitutionAdmin', 87, 1, '2017-05-24 14:30:27', '2017-05-24 14:30:27'),
(29, 67, 'App\\Users\\Employer', 34, 1, '2017-05-25 06:54:05', '2017-05-25 06:54:05'),
(30, 1, 'App\\Users\\Employer', 121, 3, '2017-07-27 11:18:53', '2017-07-28 06:47:46'),
(31, 3, 'App\\Users\\Employer', 121, 1, '2017-07-27 11:44:47', '2017-07-27 11:44:47'),
(32, 14, 'App\\Users\\Employer', 121, 1, '2017-07-27 11:52:41', '2017-07-27 11:52:41'),
(33, 87, 'App\\Users\\Employer', 121, 1, '2017-07-28 07:55:50', '2017-07-28 07:55:50'),
(34, 91, 'App\\Users\\Employer', 121, 1, '2017-07-28 07:57:01', '2017-07-28 07:57:01'),
(35, 93, 'App\\Users\\Candidate', 119, 1, '2017-07-28 07:58:45', '2017-07-28 07:58:45'),
(36, 96, 'App\\Users\\Employer', 125, 2, '2017-08-14 12:17:09', '2017-08-14 12:22:41'),
(37, 32, 'App\\Users\\Employer', 42, 1, '2017-08-14 12:26:44', '2017-08-14 12:26:44'),
(38, 3, 'App\\Users\\Employer', 42, 1, '2017-08-14 12:30:48', '2017-08-14 12:30:48'),
(39, 102, 'App\\Users\\Employer', 131, 4, '2017-08-23 12:03:21', '2017-09-13 12:46:48'),
(40, 103, 'App\\Users\\Employer', 133, 3, '2017-08-23 13:11:08', '2017-09-14 13:05:04'),
(41, 55, 'App\\Education\\Teacher', 71, 1, '2017-08-25 07:01:47', '2017-08-25 07:01:47'),
(42, 37, 'App\\Users\\Admin', 2, 1, '2017-09-04 12:51:14', '2017-09-04 12:51:14'),
(43, 96, 'App\\Users\\Admin', 2, 1, '2017-09-04 12:51:33', '2017-09-04 12:51:33'),
(44, 24, 'App\\Users\\Candidate', 138, 1, '2017-09-11 10:06:39', '2017-09-11 10:06:39'),
(45, 30, 'Admin', 2, 7, '2017-12-12 00:52:14', '2017-12-22 03:36:05'),
(46, 38, 'Admin', 2, 1, '2017-12-21 04:14:20', '0000-00-00 00:00:00'),
(47, 13, 'Admin', 2, 1, '2017-12-22 05:29:49', '0000-00-00 00:00:00'),
(48, 1, 'Employer', 26, 1, '2017-12-26 07:01:07', '0000-00-00 00:00:00'),
(49, 3, 'Admin', 2, 1, '2017-12-30 07:53:19', '0000-00-00 00:00:00'),
(50, 14, 'Admin', 2, 4, '2017-12-30 07:53:29', '2017-12-30 08:02:51'),
(51, 1, 'Candidate', 3, 1, '2018-01-17 05:12:38', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cv16_queued_jobs`
--

CREATE TABLE `cv16_queued_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cv16_roles`
--

CREATE TABLE `cv16_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cv16_role_user`
--

CREATE TABLE `cv16_role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cv16_sites`
--

CREATE TABLE `cv16_sites` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_sites`
--

INSERT INTO `cv16_sites` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'CVVid', '2016-03-15 08:35:46', '2016-03-15 08:35:46');

-- --------------------------------------------------------

--
-- Table structure for table `cv16_skills`
--

CREATE TABLE `cv16_skills` (
  `id` int(10) UNSIGNED NOT NULL,
  `skill_category_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_skills`
--

INSERT INTO `cv16_skills` (`id`, `skill_category_id`, `name`, `created_at`, `updated_at`) VALUES
(13, 1, 'Navy', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(14, 1, 'RAF', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(15, 1, 'Army', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(16, 1, 'TA', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(17, 2, 'Chemical scientists', '2017-12-02 02:35:35', '2016-07-28 15:10:02'),
(18, 2, 'Biological scientists and biochemists', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(19, 2, 'Physical scientists', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(20, 2, 'Social and humanities scientists', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(21, 2, 'Natural and social science professionals n.e.c.', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(22, 3, 'Civil ', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(23, 3, 'Mechanical ', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(24, 3, 'Electrical ', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(25, 3, 'Electronics ', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(26, 3, 'Design and development ', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(27, 3, 'Production and process ', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(28, 3, 'Telecommunications ', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(29, 4, 'IT specialist managers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(30, 4, 'IT project and programme managers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(31, 4, 'IT business analysts', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(32, 4, 'Architects and systems designers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(33, 4, 'Programmers and software development professionals', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(34, 4, 'Web design and development professionals', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(35, 5, 'Conservation professionals', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(36, 5, 'Environmental specialists', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(37, 5, 'Renewable Energy specialists', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(38, 6, 'Medical practitioners', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(39, 6, 'Psychologists', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(40, 6, 'Pharmacists', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(41, 6, 'Ophthalmic opticians', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(42, 6, 'Dentists', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(43, 6, 'Veterinarians', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(44, 6, 'Medical radiographers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(45, 6, 'Podiatrists', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(46, 6, 'Alternative Medicine', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(47, 6, 'Physiotherapists', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(48, 6, 'Occupational therapists', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(49, 6, 'Speech and language therapists', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(50, 6, 'Biomechanical therapists', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(51, 6, 'Nurses', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(52, 6, 'Midwives', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(53, 6, 'Ambulance Service', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(54, 6, 'Lab Technician', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(55, 6, 'Paramedics', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(56, 6, 'Dispensing opticians', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(57, 6, 'Pharmaceutical technicians', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(58, 6, 'Dental technicians', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(59, 7, 'Professors', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(60, 7, 'Higher education ', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(61, 7, 'Further education ', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(62, 7, 'Secondary education ', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(63, 7, 'Primary and nursery education ', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(64, 7, 'Special needs ', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(65, 7, 'Senior professionals ', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(66, 7, 'Education advisers and school inspector', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(67, 7, 'Teaching Assistant', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(68, 8, 'Judges', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(69, 8, 'Barristers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(70, 8, 'Solicitors', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(71, 8, 'Legal Admin', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(72, 9, 'Chartered Accountant', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(73, 9, 'Certified Accountant', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(74, 9, 'Financial Advisor', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(75, 9, 'Tax Investigator', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(76, 9, 'Actuaries', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(77, 9, 'Statisticians', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(78, 9, 'Business Planning', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(79, 9, 'Book-keepers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(80, 9, 'payroll managers and wages clerks', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(81, 9, 'Finance officers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(82, 9, 'Bank Manager', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(83, 9, 'Bank Clerk', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(84, 9, 'Bank Relationship Manager', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(85, 10, 'Architects', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(86, 10, 'Draughtsperson', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(87, 10, 'Town planning ', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(88, 10, 'Quantity surveyors', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(89, 10, 'Chartered surveyors', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(90, 10, 'Chartered architectural technologists', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(91, 10, 'Construction project managers ', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(92, 11, 'Librarians', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(93, 11, 'Archivists and curators', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(94, 12, 'Quality control and planning engineers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(95, 12, 'Quality assurance and regulatory professionals', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(96, 12, 'Trading Standards', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(97, 13, 'Editors', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(98, 13, 'Presenter', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(99, 13, 'Journalists', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(100, 13, 'Reporters', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(101, 13, 'Cameraman', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(102, 13, 'Producer', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(103, 13, 'Runner', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(104, 14, 'Social workers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(105, 14, 'Probation officers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(106, 14, 'Counsellor', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(107, 14, 'Welfare Support Staff', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(108, 14, 'Youth and community workers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(109, 14, 'Child and early years officers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(110, 14, 'Housing officers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(111, 14, 'Health & Safety officers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(112, 14, 'Trading standards officers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(113, 15, 'Police officers (sergeant and below)', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(114, 15, 'Fire service officers (watch manager and below)', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(115, 15, 'Prison service officers (below principal officer)', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(116, 15, 'Police community support officers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(117, 15, 'Protective service professional (e.g. bodyguard).', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(118, 15, 'Coast Guard', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(119, 16, 'Theatre Manager', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(120, 16, 'Artists', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(121, 16, 'Authors', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(122, 16, 'Actors and entertainers ', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(123, 16, 'Dancers and choreographers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(124, 16, 'Musicians', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(125, 16, 'Director ', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(126, 16, 'Producers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(127, 16, 'Audio-visual Support', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(128, 16, 'Talent Management', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(129, 17, 'Artist', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(130, 17, 'Graphic design', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(131, 17, 'Fashion Design', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(132, 17, 'Product Design', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(133, 18, 'Sports Management', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(134, 18, 'Sports players', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(135, 18, 'Coaching', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(136, 18, 'Psychologist', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(137, 18, 'Fitness instructors', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(138, 18, 'Technician', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(139, 18, 'Nutritionist', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(140, 19, 'Pilots', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(141, 19, 'Air traffic controllers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(142, 19, 'Stewardess', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(143, 19, 'Ship Captain', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(144, 19, 'Ship Mate', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(145, 19, 'Deckhand', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(146, 20, 'Estimators, valuers and assessors', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(147, 20, 'Brokers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(148, 20, 'Insurance underwriters', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(149, 21, 'National government administrative occupations', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(150, 21, 'Local government administrative occupations', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(151, 21, 'Officers of non-governmental organisations', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(152, 22, 'Records clerks and assistants', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(153, 22, 'Archivists', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(154, 23, 'Office managers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(155, 23, 'Office supervisors', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(156, 23, 'Medical secretaries', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(157, 23, 'Legal secretaries', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(158, 23, 'School secretaries', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(159, 23, 'Company secretaries', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(160, 23, 'Personal assistants and other secretaries', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(161, 23, 'Receptionists', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(162, 23, 'Typists and related keyboard occupations', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(163, 23, 'Postal workers, mail sorters, messengers and couriers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(164, 24, 'Farmers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(165, 24, 'Horticultural trades', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(166, 24, 'Farm workers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(167, 24, 'Forestry workers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(168, 24, 'Fishing ', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(169, 24, 'Agricultural Support ', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(170, 24, 'Gardeners and landscape gardeners', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(171, 24, 'Groundsmen and greenkeepers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(172, 24, 'Park Keeper', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(173, 24, 'Florist', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(174, 25, 'Smiths and forge workers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(175, 25, 'Moulders, core makers and die casters', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(176, 25, 'Sheet metal workers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(177, 25, 'Metal plate workers, and riveters', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(178, 25, 'Welding trades', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(179, 25, 'Pipe fitters', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(180, 25, 'Metal machining setters and setter-operators', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(181, 25, 'Tool makers, tool fitters and markers-out', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(182, 25, 'Metal working production and maintenance fitters', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(183, 25, 'Precision instrument makers and repairers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(184, 25, 'Air-conditioning and refrigeration engineers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(185, 26, 'Vehicle technicians, mechanics and electricians', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(186, 26, 'Vehicle body builders and repairers ', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(187, 26, 'Vehicle paint technicians', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(188, 26, 'Aircraft maintenance and related trades', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(189, 26, 'Boat and ship builders and repairers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(190, 26, 'Rail and rolling stock builders and repairers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(191, 27, 'Steel erectors', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(192, 27, 'Bricklayers and masons', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(193, 27, 'Roofers, roof tilers and slaters', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(194, 27, 'Plumbers and heating and ventilating engineers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(195, 27, 'Carpenters and joiners', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(196, 27, 'Glaziers, window fabricators and fitters', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(197, 27, 'Construction and building trades n.e.c.', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(198, 27, 'Scaffolders, stagers and riggers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(199, 27, 'Road construction operatives', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(200, 27, 'Rail construction and maintenance operatives', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(201, 28, 'Plasterers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(202, 28, 'Floorers and wall tilers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(203, 28, 'Painters and decorators', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(204, 28, 'Furniture Maker', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(205, 29, 'Weavers and knitters', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(206, 29, 'Upholsterers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(207, 29, 'Footwear and leather working trades', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(208, 29, 'Tailors and dressmakers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(209, 29, 'Textiles, garments and related trades n.e.c.', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(210, 30, 'Pre-press technicians', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(211, 30, 'Printers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(212, 30, 'Print finishing and binding workers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(213, 31, 'Butchers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(214, 31, 'Bakers and flour confectioners', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(215, 31, 'Fishmongers and poultry dressers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(216, 31, 'Chefs', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(217, 31, 'Cooks', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(218, 31, 'Catering and bar managers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(219, 32, 'Nursery nurses and assistants', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(220, 32, 'Childminders and related occupations', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(221, 32, 'Playworkers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(222, 32, 'Teaching assistants', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(223, 32, 'Educational support assistants', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(224, 33, 'Veterinary nurses', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(225, 33, 'Pest control officers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(226, 33, 'Animal Support', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(227, 34, 'Houseparents and residential wardens', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(228, 34, 'Care workers and home carers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(229, 34, 'Senior care workers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(230, 34, 'Care escorts', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(231, 34, 'Undertakers, mortuary and crematorium assistants', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(232, 34, 'Hospice Workers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(233, 35, 'Stadium Support Staff', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(234, 35, 'Sports and leisure assistants', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(235, 35, 'Travel agents', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(236, 35, 'Air travel assistants', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(237, 35, 'Rail travel assistants', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(238, 35, 'Tour Guides', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(239, 36, 'Hairdressers and barbers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(240, 36, 'Beauticians and related occupations', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(241, 37, 'Housekeepers and related occupations', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(242, 37, 'Caretakers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(243, 37, 'Cleaning and housekeeping managers and supervisors', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(244, 38, 'Sales and retail assistants', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(245, 38, 'Retail cashiers and check-out operators', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(246, 38, 'Telephone Salespersons', '2016-07-28 15:10:02', '2017-01-23 10:06:52'),
(247, 38, 'Pharmacy and other dispensing assistants', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(248, 38, 'Vehicle and parts salespersons and advisers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(249, 38, 'Roundspersons and van salespersons', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(250, 38, 'Market and street traders and assistants', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(251, 38, 'Merchandisers and window dressers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(252, 38, 'Call and contact centre occupations', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(253, 38, 'Customer Service', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(254, 39, 'Collector salespersons and credit agents', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(255, 39, 'Debt, rent and other cash collectors', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(256, 40, 'Paper and wood machine operatives', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(257, 40, 'Coal mine operatives', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(258, 40, 'Quarry workers and related operatives', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(259, 40, 'Energy plant operatives', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(260, 40, 'Metal working machine operatives', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(261, 40, 'Water and sewerage plant operatives', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(262, 40, 'Printing machine assistants', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(263, 40, 'Plant and machine operatives n.e.c.', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(264, 41, 'Assemblers (electrical and electronic products)', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(265, 41, 'Assemblers (vehicles and metal goods)', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(266, 41, 'Routine inspectors and testers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(267, 41, 'Weighers, graders and sorters', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(268, 41, 'Tyre, exhaust and windscreen fitters', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(269, 41, 'Sewing machinists', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(270, 41, 'Assemblers and routine operatives n.e.c.', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(271, 43, 'Large goods vehicle drivers', '2016-07-28 15:10:02', '2017-01-28 20:10:41'),
(272, 43, 'Van drivers', '2016-07-28 15:10:02', '2017-01-28 20:11:15'),
(273, 43, 'Bus and coach drivers', '2016-07-28 15:10:02', '2017-01-28 20:11:41'),
(274, 43, 'Taxi Driver', '2016-07-28 15:10:02', '2017-01-28 20:12:26'),
(275, 43, 'Driving instructors', '2016-07-28 15:10:02', '2017-01-28 20:14:13'),
(276, 43, 'Chauffeurs', '2016-07-28 15:10:02', '2017-01-28 20:09:56'),
(277, 43, 'Crane drivers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(278, 43, 'Fork-lift truck drivers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(279, 43, 'Agricultural machinery drivers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(280, 43, 'Mobile machine drivers and operatives n.e.c.', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(281, 43, 'Train and tram drivers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(282, 43, 'Marine and waterways transport operatives', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(283, 44, 'Industrial cleaning process occupations', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(284, 44, 'Packers, bottlers, canners and fillers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(285, 44, 'Shelf fillers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(286, 45, 'Window cleaners', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(287, 45, 'Street cleaners', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(288, 45, 'Cleaners and domestics', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(289, 45, 'Launderers, dry cleaners and pressers', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(290, 45, 'Refuse and salvage occupations', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(291, 45, 'Vehicle valeters and cleaners', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(292, 45, 'Elementary cleaning occupations n.e.c.', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(293, 46, 'Security guards and related occupations', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(294, 46, 'Parking and civil enforcement occupations', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(295, 46, 'School midday and crossing patrol occupations', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(296, 47, 'Hospital porters', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(297, 47, 'Kitchen and catering assistants', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(298, 47, 'Waiters and waitresses', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(299, 47, 'Bar staff', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(300, 47, 'Leisure and theme park attendants', '2016-07-28 15:10:02', '2016-07-28 15:10:02'),
(301, 48, 'Marketing Strategy', '2016-08-30 13:54:21', '2016-08-30 13:54:21'),
(302, 48, 'Marketing Research', '2016-08-30 13:54:39', '2016-08-30 13:54:39'),
(303, 48, 'Advertising', '2016-08-30 13:54:58', '2016-08-30 13:54:58'),
(304, 48, 'Public Relations', '2016-08-30 13:55:06', '2016-08-30 13:55:06'),
(305, 48, 'Live Events', '2016-08-30 13:55:16', '2016-08-30 13:55:16'),
(306, 48, 'Exhibitions', '2016-08-30 13:55:23', '2016-08-30 13:55:23'),
(307, 48, 'Packaging Design', '2016-08-30 13:55:35', '2016-08-30 13:55:35'),
(308, 48, 'New Product Development', '2016-08-30 13:55:46', '2016-08-30 13:55:46'),
(309, 42, 'Delivery Driver', '2017-01-17 20:22:49', '2017-01-17 20:22:49'),
(310, 48, 'Social Media Manager', '2017-01-17 20:25:01', '2017-01-17 20:25:01'),
(311, 4, 'Junior Designer', '2017-01-17 20:42:38', '2017-01-17 20:42:38'),
(312, 6, 'Optometrist', '2017-01-17 20:45:22', '2017-01-17 20:45:22'),
(313, 6, 'Orthodontist', '2017-01-17 20:47:15', '2017-01-17 20:47:15'),
(314, 6, 'Associate Dentist', '2017-01-17 20:48:00', '2017-01-17 20:48:00'),
(315, 6, 'Hygienist', '2017-01-17 20:48:47', '2017-01-17 20:48:47'),
(316, 6, 'Dental Surgeon', '2017-01-17 20:50:33', '2017-01-17 20:50:33'),
(317, 6, 'Dental Assistant', '2017-01-17 20:51:03', '2017-01-17 20:51:03'),
(318, 6, 'Medical Secretary', '2017-01-17 20:53:58', '2017-01-17 20:53:58'),
(319, 6, 'General Practitioner', '2017-01-17 20:56:50', '2017-01-17 20:56:50'),
(320, 6, 'Podiatrist', '2017-01-17 20:58:34', '2017-01-17 20:58:34'),
(321, 6, 'Healthcare Worker', '2017-01-17 21:02:43', '2017-01-17 21:02:43'),
(322, 6, 'Ward Clerk', '2017-01-17 21:03:36', '2017-01-17 21:03:36'),
(323, 6, 'Care Assistant', '2017-01-17 21:05:02', '2017-01-17 21:05:02'),
(324, 6, 'Microbiologist', '2017-01-17 21:05:51', '2017-01-17 21:05:51'),
(325, 6, 'Medical Consultant', '2017-01-17 21:08:56', '2017-01-23 09:55:15'),
(326, 4, 'Technical Support Engineer', '2017-01-17 21:11:10', '2017-01-17 21:11:10'),
(327, 4, 'Field Technician', '2017-01-17 21:11:38', '2017-01-17 21:11:38'),
(328, 4, 'Analyst', '2017-01-17 21:12:02', '2017-01-17 21:12:02'),
(329, 4, 'Junior Developer', '2017-01-17 21:12:35', '2017-01-17 21:12:35'),
(330, 4, 'Communications Executive', '2017-01-17 21:13:01', '2017-01-17 21:13:01'),
(331, 4, 'Application Developer', '2017-01-17 21:15:15', '2017-01-17 21:15:15'),
(332, 4, 'Application Support Developer', '2017-01-17 21:15:39', '2017-01-17 21:15:39'),
(333, 4, 'Software Developer', '2017-01-17 21:16:25', '2017-01-17 21:16:25'),
(334, 4, 'Programmer', '2017-01-17 21:16:59', '2017-01-17 21:16:59'),
(335, 4, 'Systems Engineer', '2017-01-17 21:18:50', '2017-01-17 21:18:50'),
(336, 4, 'Digital Marketing Manager', '2017-01-17 21:23:02', '2017-01-17 21:23:02'),
(337, 49, 'Project Manager', '2017-01-18 12:19:26', '2017-01-18 12:19:26'),
(338, 49, 'Audio Visual Technician', '2017-01-18 12:20:00', '2017-01-18 12:20:00'),
(339, 49, 'Events Manager', '2017-01-18 12:20:38', '2017-01-18 12:20:38'),
(340, 49, 'Sales', '2017-01-18 12:21:16', '2017-01-18 12:21:16'),
(341, 49, 'Hospitality Manager', '2017-01-18 12:21:49', '2017-01-18 12:21:49'),
(342, 49, 'Hospitality staff', '2017-01-18 12:22:41', '2017-01-18 12:22:41'),
(343, 49, 'Events Coordinator', '2017-01-18 12:23:27', '2017-01-18 12:23:27'),
(344, 49, 'Sales Consultant', '2017-01-18 12:24:03', '2017-01-18 12:24:03'),
(345, 49, 'Waiting Staff', '2017-01-18 12:25:45', '2017-01-18 12:25:45'),
(346, 17, 'Furniture Design', '2017-01-20 14:03:47', '2017-01-20 14:03:47'),
(347, 27, 'Electrician', '2017-01-23 09:58:40', '2017-01-23 09:58:40'),
(348, 27, 'Gardener', '2017-01-23 09:59:04', '2017-01-23 09:59:04'),
(349, 27, 'Locksmith', '2017-01-23 09:59:32', '2017-01-23 09:59:32'),
(350, 27, 'Plasterer', '2017-01-23 10:00:11', '2017-01-23 10:00:11'),
(351, 27, 'Tiling', '2017-01-23 10:00:35', '2017-01-23 10:00:35'),
(352, 27, 'Roofing', '2017-01-23 10:01:13', '2017-01-23 10:01:13'),
(353, 27, 'Gas Engineer', '2017-01-23 10:01:31', '2017-01-23 10:01:31'),
(356, 38, 'Sales Manager', '2017-01-23 10:07:33', '2017-01-23 10:07:33'),
(357, 38, 'Sales Executive', '2017-01-23 10:07:57', '2017-01-23 10:07:57'),
(360, 35, 'Mountain Guides', '2017-02-03 09:29:16', '2017-02-03 09:30:36'),
(361, 35, 'Mountain Leaders', '2017-02-03 09:29:56', '2017-02-03 09:30:48');

-- --------------------------------------------------------

--
-- Table structure for table `cv16_skill_categories`
--

CREATE TABLE `cv16_skill_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_skill_categories`
--

INSERT INTO `cv16_skill_categories` (`id`, `name`, `created_at`, `updated_at`, `ordering`) VALUES
(1, 'Armed Forces', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 32),
(2, 'Natural and Social Science ', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 8),
(3, 'Engineering ', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 9),
(4, 'IT/Digital', '2016-07-28 14:58:14', '2016-08-30 14:13:52', 8),
(5, 'Conservation and Environment Professionals', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 38),
(6, 'Health Professionals', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 3),
(7, 'Education', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 6),
(8, 'Legal Professionals', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 5),
(9, 'Finance', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 4),
(10, 'Architects, Town Planners and Surveyors', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 9),
(11, 'Librarians and Related Professionals', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 0),
(12, 'Quality and Regulatory Professionals', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 8),
(13, 'News Media', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 9),
(14, 'Welfare Professionals', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 9),
(15, 'Protective Service Occupations', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 8),
(16, 'Performing Arts', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 13),
(17, 'Design Occupations', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 8),
(18, 'Sports and Fitness Occupations', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 14),
(19, 'Transport ', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 26),
(20, 'Insurance', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 2),
(21, 'Administrative Occupations: Government and Related Organisations', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 9),
(22, 'Records', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 8),
(23, 'Office/Clerical Work', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 12),
(24, 'Agricultural and Related Trades', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 36),
(25, 'Metal Forming, Welding and Related Trades', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 37),
(26, 'Vehicle Trades', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 25),
(27, 'Construction and Building Trades', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 22),
(28, 'Building Finishing Trades', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 21),
(29, 'Textiles and Garments Trades', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 19),
(30, 'Printing Trades', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 20),
(31, 'Food Preparation and Hospitality Trades', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 15),
(32, 'Childcare and Related Personal Services', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 16),
(33, 'Animal Care and Control Services', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 18),
(34, 'Caring Personal Services', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 17),
(36, 'Hairdressers and Related Services', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 30),
(37, 'Housekeeping and Related Services', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 31),
(38, 'Sales Related/Retail', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 7),
(39, 'Debt Collection', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 39),
(40, 'Plant and Machine Operatives', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 24),
(41, 'Assemblers and Routine Operatives', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 35),
(42, 'Road Transport Drivers', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 27),
(43, 'Drivers and Operatives', '2016-07-28 14:58:14', '2017-01-28 20:16:16', 28),
(44, 'Manual Labour', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 23),
(45, 'Elementary Cleaning Occupations', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 34),
(46, 'Elementary Security Occupations', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 33),
(47, 'Service Staff', '2016-07-28 14:58:14', '2016-08-30 14:12:24', 29),
(51, 'Marketing', '2017-12-02 01:07:56', '2017-12-02 01:08:31', 9),
(52, 'Events and Hospitality', '2017-12-02 01:08:04', '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `cv16_slideshows`
--

CREATE TABLE `cv16_slideshows` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8_unicode_ci,
  `status` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'published',
  `background_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `background_x` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `background_y` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_slideshows`
--

INSERT INTO `cv16_slideshows` (`id`, `user_id`, `title`, `slug`, `type`, `body`, `status`, `background_id`, `created_at`, `updated_at`, `deleted_at`, `background_x`, `background_y`) VALUES
(1, 2, 'Home', 'home', NULL, NULL, 'published', NULL, '2016-04-25 15:00:13', '2017-08-22 09:22:17', NULL, '0%', '0%'),
(2, 2, 'About Us', 'about-us', NULL, NULL, 'published', 75, '2016-04-26 12:11:20', '2016-07-04 10:00:41', NULL, '66%', 'center'),
(3, 2, 'Top Tips', 'top-tips', NULL, NULL, 'published', 78, '2016-04-26 14:17:19', '2016-07-04 09:59:00', NULL, '70%', 'center'),
(4, 2, 'Worry List', 'worry-list', NULL, NULL, 'published', 79, '2016-04-26 14:19:58', '2016-07-04 09:59:54', NULL, '66%', 'center'),
(5, NULL, 'ssss', 'sss', NULL, NULL, 'published', NULL, '2017-12-02 03:01:37', '0000-00-00 00:00:00', NULL, NULL, NULL),
(6, NULL, 'sss', 'ssssss', NULL, NULL, 'published', NULL, '2017-12-02 03:08:42', '0000-00-00 00:00:00', NULL, NULL, NULL),
(8, NULL, 'sadsfsfsf', 'sfsfsfsfs', NULL, NULL, 'published', NULL, '2017-12-02 03:09:25', '0000-00-00 00:00:00', NULL, NULL, NULL),
(10, NULL, 'sfsf', 'sfsf', NULL, NULL, 'published', NULL, '2017-12-02 03:18:16', '0000-00-00 00:00:00', '2017-12-29 03:25:03', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cv16_slideshow_slides`
--

CREATE TABLE `cv16_slideshow_slides` (
  `id` int(10) UNSIGNED NOT NULL,
  `slideshow_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `media_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `background_x` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `background_y` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_slideshow_slides`
--

INSERT INTO `cv16_slideshow_slides` (`id`, `slideshow_id`, `user_id`, `title`, `body`, `media_id`, `created_at`, `updated_at`, `deleted_at`, `background_x`, `background_y`) VALUES
(1, 2, 2, 'You never get a second chance to make a first impression', '<h2>You never get a second chance to make a first impression.</h2>\r\n<p>Okay, you want to give yourself the very best chance of getting the job you most want.<br />But how?</p>\r\n<p>A smart, well laid-out CV that makes you a shoe-in for the post. Right?</p>\r\n<p>Well yes, except that is so last century. Now there&rsquo;s a new way to make that first impression really count.</p>\r\n<p>Video CVs are the future. And don&rsquo;t worry if you&rsquo;re &ldquo;camera shy&rdquo;. We&rsquo;ll show you how to get past your insecurities &ndash; so you shine at your best!</p>', NULL, '2016-04-26 14:15:11', '2016-04-27 12:20:36', NULL, NULL, NULL),
(2, 2, 2, 'The problem with traditional CVs', '<h2>The problem with traditional CVs</h2>\r\n<p>They&rsquo;re not just a problem for you - they&rsquo;re a real hassle for employers. No matter how well you describe yourself, it is still just words on paper. When your potential boss &lsquo;meets&rsquo; you for the first time, whether online or in the flesh, he or she will form their opinion based on what they see - from the most obvious visual clues &hellip; to those more personal traits, relevant interests and enthusiasms that make you the lovable, indispensable asset you know yourself to be. No piece of paper can do that for you.</p>\r\n<p>The great news is &ndash; if you&rsquo;re a tiger, now you don&rsquo;t just have to be a paper tiger.</p>', NULL, '2016-04-26 14:15:27', '2016-04-26 14:15:27', NULL, NULL, NULL),
(3, 2, 2, 'Getting ahead of the Game - with CVVid', '<h2>Getting ahead of the Game -<br />with CVVid</h2>\r\n<p>More and more young people are discovering that this brand new alternative to the paper CV is giving them the edge out there in the workplace &ndash; seriously boosting their chances of getting an interview.</p>\r\n<p>Look at it from the employers&rsquo; point of view. CV Videos mean they don&rsquo;t have to wade through piles of paper trying to get an impression of the people they may want to hire. CVVID makes it so much easier for them &ndash; because it gives you the tools to present your true and best self. So your application stands out from the rest.</p>\r\n<p>CV enhancement has really taken off in the States. Soon it will be the norm on both sides of the Atlantic. Now is the perfect time to get ahead of the competition by joining the growing numbers of jobseekers showcasing their talents with a personalised video from CVVID.</p>', NULL, '2016-04-26 14:15:46', '2016-04-26 14:15:46', NULL, NULL, NULL),
(4, 2, 2, 'Why CVVID.COM?', '<h2>Why CVVID.COM?</h2>\r\n<p>We&rsquo;re experts in what employers want. And we help you get it to them in a way that&rsquo;s easy and enjoyable &ndash; for you, and them.</p>\r\n<p>By the way, did you know Jonny Depp was camera shy? Many people are. If you&rsquo;re one of them - don&rsquo;t worry. We&rsquo;ll get you through it &ndash; to you at your best.</p>\r\n<p>Your Video CV can be created on your phone in any setting you choose. It&rsquo;s really simple to do - you don&rsquo;t need to be an IT geek or a professional photographer. See for yourself.</p>\r\n<p><a class=\"btn btn-primary\" href=\"http://www.cvvid.com/top-tips\">Top Tips</a></p>', NULL, '2016-04-26 14:16:04', '2016-11-17 16:05:47', NULL, '0%', '0%'),
(5, 3, 2, 'And now for the How', '<h2>And now for The How &hellip;</h2>\r\n<p>The first thing is the simplest. Practice. It doesn&rsquo;t have to make it perfect, but it will always make it better.</p>\r\n<p>Prepare to be your best:</p>\r\n<ul>\r\n<li>When do you feel at your most confident? Imagine yourself there.</li>\r\n<li>Who makes you feel most like you? &hellip; Imagine you&rsquo;re with them. Better still, ask them to help you.</li>\r\n<li>In fact it&rsquo;s always a good idea to get someone you trust to help you.</li>\r\n<li>Treat this as a project. Think it through. Walk around it. Build it up. Get some feedback from others.</li>\r\n<li>Take a look at some of our Best Practice examples <a class=\"yellow\" href=\"http://www.cvvid.com/cv-video-showcase\">here</a></li>\r\n<li>BUT always remember, you don&rsquo;t have to be like anyone else, and you shouldn&rsquo;t be. That&rsquo;s the whole point: you just have to be you. You - at your best.</li>\r\n</ul>', NULL, '2016-04-26 14:18:00', '2017-08-14 06:37:33', NULL, '0%', '0%'),
(6, 3, 2, 'Get used to that camera', '<h2>Get used to that camera.</h2>\r\n<ul>\r\n<li>Make it fun!</li>\r\n<li>Shout, dance, sing, read, tell a joke, do a stand up in front of it &hellip; Until you start to relax and stop hunching your shoulders!</li>\r\n<li>Now try speaking directly to camera. Start off with a couple of sentences. If there&rsquo;s something you really care passionately about, then talk about it in this test.</li>\r\n<li>Okay, take a look at your recording. Don&rsquo;t think about what you&rsquo;ve said, but how you&rsquo;ve said it. Are you speaking Clearly? Confidently? With conviction? Those are the three most important things to go for.</li>\r\n<li>Have another go. And another. Tell that camera you&rsquo;re in charge.</li>\r\n<li>Get a friend to be your director.</li>\r\n<li>And don&rsquo;t forget to smile!</li>\r\n<li>Most of all, just be you. You &ndash; at your best.</li>\r\n</ul>\r\n<p>If cameras are a real problem for you, check out our Masterclass video: Act Natural - how to be YOU in front of a camera <a class=\"yellow\" href=\"http://www.cvvid.com/how-to\">here</a></p>', NULL, '2016-04-26 14:18:23', '2016-11-17 16:06:37', NULL, '0%', '0%'),
(7, 3, 2, 'So what should I say?', '<h2>So what should I say?</h2>\r\n<p>It&rsquo;s really important to plan this. (Really)</p>\r\n<p>You&rsquo;re showing your personality: who you are, how well you can communicate, what kind of team player you are, your skills, your experience and your ambition.</p>\r\n<p>All that needs to come across right from outset.</p>', NULL, '2016-04-26 14:18:46', '2016-04-26 14:18:46', NULL, NULL, NULL),
(8, 3, 2, 'Step 1', '<h2>Step 1</h2>\r\n<p>Start by imagining you&rsquo;re the employer.</p>\r\n<p>It&rsquo;s so important to understand that employers want genuine people, who have heart and warmth - and even vulnerability - as well as skills and drive and passion.</p>\r\n<p>If you allow them to meet you at your best, you&rsquo;ll stand out from a whole production line of script-reading Cyborgs.</p>\r\n<ul>\r\n<li>What are you looking for in someone you want to employ? Write down the words that you come up with.</li>\r\n<li>What might you miss if you-the-employer first met you-the-job-seeker?</li>\r\n</ul>\r\n<p>Right. So those are the things you want to try get into your CV Video.</p>', NULL, '2016-04-26 14:19:04', '2016-04-26 14:19:04', NULL, NULL, NULL),
(9, 3, 2, 'Step 2', '<h2>Step 2</h2>\r\n<p>Now it&rsquo;s time to build up the structure.</p>\r\n<p>For this you&rsquo;ll want to create a set of questions that you&rsquo;ll then answer in your CV Video.</p>\r\n<ul>\r\n<li>Make sure your questions cover the things you&rsquo;ve written down in step 1.</li>\r\n<li>Once you&rsquo;ve got your list, experiment with different ways to order them &ndash; until you&rsquo;ve got the best sequence.<br /> It&rsquo;s good idea to get someone to help with this too.</li>\r\n</ul>', NULL, '2016-04-26 14:19:19', '2016-04-26 14:19:19', NULL, NULL, NULL),
(10, 3, 2, 'Questions', '<h2>Questions</h2>\r\n<p>Here are some examples of the sorts of questions you might ask yourself. But remember &ndash; you&rsquo;re not trying to be a flawless super-hero, or deliver a Mask of Perfection.</p>\r\n<p>Make sure your questions give you the chance to be the real you. You &ndash; at your best.</p>\r\n<ol>\r\n<li>Tell me something surprising about yourself.</li>\r\n<li>What are your strong points?</li>\r\n<li>What about a failure &ndash; how did you learn from it and what did you learn?</li>\r\n<li>What do you see yourself doing in five years&rsquo; time?</li>\r\n<li>What motivates you?</li>\r\n<li>What are the things you most want to learn?</li>\r\n<li>Talk about a big event, achievement or experience &ndash; what did you learn about yourself.</li>\r\n<li>Talk about something that makes you smile.</li>\r\n<li>Talk about something that lets us see you at your best</li>\r\n</ol>', NULL, '2016-04-26 14:19:37', '2016-04-26 14:19:37', NULL, NULL, NULL),
(11, 4, 2, 'What if I’m not cut out to perform for the camera?', '<p>1. What if I&rsquo;m not cut out to perform for the camera?</p>\r\n<p>If you can handle the pressure of an interview, you can make a Video CV. <br />Really.</p>\r\n<p>For most people, it&rsquo;s just about doing it and getting over yourself.</p>\r\n<p>First of all, try the suggestions in the section &ldquo;Get used to the camera&rdquo;.</p>\r\n<p>You don&rsquo;t need to perform, just be as natural as you can be. If you make a mistake just do another take &hellip; until you&rsquo;re happy.</p>\r\n<p>You can have a friend hold up bullet points for you, just out of sight, as a prompt. But be sure it doesn&rsquo;t look &ndash; or sound - like you&rsquo;re reading from them.</p>\r\n<p>If in doubt, keep practicing. Take 2 &hellip; Take 7 &hellip; Take 12 &hellip; Until it&rsquo;s in the can.</p>\r\n<p>Yes you&rsquo;re on show, but you&rsquo;re not on trial. Relax and enjoy yourself. Unlike when you go for an interview, there&rsquo;s no time limit. So keep shaping your Video CV until you&rsquo;re ready to upload it to the site.</p>\r\n<p>If cameras are a real problem for you, check out our Masterclass video: Act Natural - how to be YOU in front of a camera <a class=\"yellow\" href=\"http://www.cvvid.com/how-to\">here</a></p>', NULL, '2016-04-26 14:20:46', '2016-11-17 16:07:29', NULL, '0%', '0%'),
(12, 4, 2, 'So it’s all about how I look?', '<p>2. So it&rsquo;s all about how I look?</p>\r\n<p>No way! Employers are looking to hire someone with a great personality first and foremost - someone they can work with, trust and rely on day to day. Plus they want to know about your qualifications and experience. Your Video CV can do so much of that work for them &ndash; and for you.</p>\r\n<p>And hey &ndash; if a potential employer rejects you on the basis of your looks, you&rsquo;ve just saved yourself a whole lot of time and effort turning up to be interviewed by someone you probably wouldn&rsquo;t want to work for anyway.</p>', NULL, '2016-04-26 14:21:04', '2016-04-26 14:21:04', NULL, NULL, NULL),
(13, 4, 2, 'How much is this going to cost me?', '<p>3. How much is this going to cost me?</p>\r\n<p> Registering for a CVVID account is completely free. Once you have created your profile, you can increase your chances of landing your dream job by upgrading to a Gold Account. (If your school or college is funding your CVVID profile then of course they&rsquo;ll foot the bill).</p>', NULL, '2016-04-26 14:21:22', '2016-04-26 14:21:22', NULL, NULL, NULL),
(14, 4, 2, 'What about security? Will anyone be able to view the video?', '<p>4. What about security? Will anyone be able to view the video?</p>\r\n<p>No they won&rsquo;t. At CVVID we take your privacy really seriously. Your profile is only viewable if you choose to make your account public. If you choose to keep it private, then only those employers who have been sent your unique link can view your Video CV.</p>\r\n<p>What&rsquo;s more, once you&rsquo;ve landed a job, you can de-activate your account so you don&rsquo;t get any unwanted attention!</p>', NULL, '2016-04-26 14:21:38', '2016-04-26 14:21:38', NULL, NULL, NULL),
(15, 4, 2, 'How do I know this new kind of CV really works?', '<p>5. How do I know this new kind of CV really works?</p>\r\n<p>CVVID is helping many graduates and jobseekers to find employment. You can see their Video CVs here</p>', NULL, '2016-04-26 14:22:09', '2016-04-26 14:22:09', NULL, NULL, NULL),
(16, 4, 2, 'Additional Documentation.', '<p>6. Additional Documentation.</p>\r\n<p>Your video is just one part of you. Your profile page can also include other stuff: a paper CV &ndash; some people will still need this - your portfolio, exam certificates, qualifications, testimonials &hellip; In fact anything that enhances your credentials.</p>', NULL, '2016-04-26 14:22:31', '2016-04-26 14:22:31', NULL, NULL, NULL),
(17, 1, 2, 'Slide 1', '<h1>CVVid: You - at your best</h1>\r\n<h4>You never get a second chance to make a first impression</h4>', 81, '2016-04-27 12:38:30', '2016-11-21 17:21:18', NULL, 'right', 'center'),
(18, 1, 2, 'Slide 2', '<h1>CVVid: Stand out from the crowd</h1>\r\n<h4>Secure your dream job in just 90 seconds</h4>', 82, '2016-04-27 12:38:48', '2016-11-21 17:21:26', NULL, '70%', 'center'),
(19, 1, 2, 'Slide 3', '<h1>CVVid: Paper CVs are so last century</h1>\r\n<h4>Recruitment fit for the digital age</h4>', 80, '2016-04-27 12:40:46', '2016-11-21 17:21:34', NULL, '62%', 'center'),
(20, 1, 2, 'Slide 4', '<h1>CVVid: Make the right decision every time</h1>\r\n<h4>Picking the best candidate has never been so easy</h4>', 77, '2016-04-27 12:43:02', '2016-11-21 17:21:42', NULL, '80%', 'center');

-- --------------------------------------------------------

--
-- Table structure for table `cv16_tutor_groups`
--

CREATE TABLE `cv16_tutor_groups` (
  `id` int(10) UNSIGNED NOT NULL,
  `institution_id` int(10) UNSIGNED NOT NULL,
  `teacher_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_lat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_lng` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `graduation_date` date DEFAULT NULL,
  `account_limit` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_tutor_groups`
--

INSERT INTO `cv16_tutor_groups` (`id`, `institution_id`, `teacher_id`, `name`, `location`, `location_lat`, `location_lng`, `graduation_date`, `account_limit`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 3, 71, 'Sixth form', 'TIGHS', NULL, NULL, '2017-06-30', 16, '2017-03-06 11:06:26', '2017-03-20 18:54:39', NULL),
(3, 15, 88, 'Sixth Form', '', NULL, NULL, '2018-07-31', 8, '2017-04-11 10:38:56', '2017-05-24 13:28:58', NULL),
(4, 15, 93, 'Year 11', 'Victoria', NULL, NULL, NULL, 5, '2017-04-16 11:22:05', '2017-04-16 11:22:05', NULL),
(5, 15, 95, 'Apprentices', 'Springfield', NULL, NULL, '2018-05-31', 5, '2017-04-16 11:31:20', '2017-04-16 11:31:20', NULL),
(6, 15, 97, 'Year 11b', 'Storm', NULL, NULL, '2018-07-31', 5, '2017-04-16 11:45:46', '2017-04-16 11:45:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cv16_tutor_groups_students`
--

CREATE TABLE `cv16_tutor_groups_students` (
  `id` int(10) UNSIGNED NOT NULL,
  `institution_id` int(10) UNSIGNED NOT NULL,
  `tutor_group_id` int(10) UNSIGNED NOT NULL,
  `student_id` int(10) UNSIGNED NOT NULL,
  `graduation_date` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_tutor_groups_students`
--

INSERT INTO `cv16_tutor_groups_students` (`id`, `institution_id`, `tutor_group_id`, `student_id`, `graduation_date`, `created_at`, `updated_at`) VALUES
(2, 3, 2, 78, '2018-07-31', '2017-03-24 08:50:28', '2017-03-24 08:50:28'),
(6, 15, 3, 90, '2018-07-31', '2017-04-11 11:32:10', '2017-04-11 11:32:10'),
(7, 15, 3, 91, '2018-06-30', '2017-04-16 10:52:26', '2017-04-16 10:52:26'),
(8, 15, 3, 92, '2018-06-30', '2017-04-16 11:07:48', '2017-04-16 11:07:48'),
(9, 15, 6, 98, NULL, '2017-04-16 11:45:54', '2017-04-16 11:45:54'),
(14, 3, 2, 105, '2018-08-31', '2017-06-14 06:32:37', '2017-06-14 06:32:37'),
(15, 3, 2, 106, '2018-08-31', '2017-06-14 06:33:23', '2017-06-14 06:33:23'),
(16, 3, 2, 107, '2018-08-31', '2017-06-14 06:34:58', '2017-06-14 06:34:58'),
(17, 3, 2, 108, '2018-08-31', '2017-06-14 06:35:54', '2017-06-14 06:35:54'),
(18, 3, 2, 109, '2018-08-31', '2017-06-14 06:36:36', '2017-06-14 06:36:36'),
(19, 3, 2, 110, '2018-08-31', '2017-06-14 06:37:45', '2017-06-14 06:37:45'),
(20, 3, 2, 111, '2018-08-31', '2017-06-14 06:38:33', '2017-06-14 06:38:33'),
(21, 3, 2, 112, '2018-08-31', '2017-06-14 06:39:58', '2017-06-14 06:39:58'),
(22, 3, 2, 113, '2018-08-31', '2017-06-14 06:40:55', '2017-06-14 06:40:55'),
(23, 3, 2, 114, '2018-08-31', '2017-06-14 06:41:44', '2017-06-14 06:41:44'),
(24, 3, 2, 135, '2017-09-30', '2017-08-24 13:10:03', '2017-08-24 13:10:03'),
(25, 15, 3, 161, NULL, '2017-12-23 05:53:10', '0000-00-00 00:00:00'),
(26, 15, 3, 162, NULL, '2017-12-23 05:53:10', '0000-00-00 00:00:00'),
(27, 15, 3, 163, NULL, '2017-12-23 05:53:10', '0000-00-00 00:00:00'),
(28, 15, 3, 164, NULL, '2017-12-23 05:53:10', '0000-00-00 00:00:00'),
(29, 15, 3, 165, NULL, '2017-12-23 05:53:10', '0000-00-00 00:00:00'),
(30, 3, 2, 181, NULL, '2018-01-18 03:30:15', '0000-00-00 00:00:00'),
(31, 3, 2, 183, NULL, '2018-01-18 03:36:01', '0000-00-00 00:00:00'),
(32, 3, 2, 184, NULL, '2018-01-18 03:41:35', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `cv16_users`
--

CREATE TABLE `cv16_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `forenames` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `surname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `dob` date DEFAULT NULL,
  `status` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'active',
  `is_premium` tinyint(4) DEFAULT '0',
  `tel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `current_job` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_lat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location_lng` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `stripe_active` tinyint(4) NOT NULL DEFAULT '0',
  `stripe_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stripe_subscription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `stripe_plan` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_four` varchar(4) COLLATE utf8_unicode_ci DEFAULT NULL,
  `card_expiry` date DEFAULT NULL,
  `card_expiry_sent` date DEFAULT NULL,
  `trial_ends_at` timestamp NULL DEFAULT NULL,
  `subscription_ends_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_users`
--

INSERT INTO `cv16_users` (`id`, `type`, `forenames`, `surname`, `email`, `password`, `dob`, `status`, `is_premium`, `tel`, `mobile`, `current_job`, `location`, `location_lat`, `location_lng`, `remember_token`, `created_at`, `updated_at`, `deleted_at`, `stripe_active`, `stripe_id`, `stripe_subscription`, `stripe_plan`, `last_four`, `card_expiry`, `card_expiry_sent`, `trial_ends_at`, `subscription_ends_at`) VALUES
(2, 'admin', 'Adam', 'Wilson', 'adam@red-fern.co.uk', 'admin', '1988-11-08', 'active', 1, '01254 300030', '07221 213422', 'Web Developer', '', '', '', 'BZeWDnIgcTpmc58sSjbSo47A1X9mDIkVLSsr2VH4YJqJENZPNhJCXrKNjOVn', '2016-03-14 12:24:07', '2017-09-11 17:50:51', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'candidate', 'Arif', 'Bangi', 'letstalk@arifbangi.com', 'testing', '2017-12-14', 'InActive', 0, '433535', '898989', 'Marketing Strategist', 'Manchester, United Kingdom', '53.4807593', '-2.2426305000000184', 'pyYiJqxZVcn3cwQ47pBt390qjQbGc7XhrWLILW04XQ7OzzFZmhWXsLYOuKpM', '2016-03-14 13:12:46', '2018-01-24 02:26:55', NULL, 1, 'cus_CBJeK0EAUQOx6g', 'sub_CBJeOdjWtsihXY', 'premium_monthly', '4242', NULL, NULL, '2019-01-22 00:04:27', NULL),
(5, 'candidate', 'Alex', 'Heeney', 'alex@red-fern.co.uk', '$2y$10$xGFugECGu3wUP6v7Gp.2Zu400BzDnq3DjNnxjAQiioOifk26ZPwFO', '0000-00-00', 'active', 0, '01254 300030', '07231 123 023', 'Creative Director', 'Accrington, United Kingdom', '53.753609', '-2.3721800000000712', 'nm8GJ0zu852SvEivsqQBUX5d1gEE89E5HPZ7LPy8GtzszGzpwof3gXmM9Ewh', '2016-03-18 17:10:57', '2017-06-07 15:19:54', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'admin', 'Sean', 'Redfearn', 'sean@red-fern.co.uk', '$2y$10$7OuLPp39rm.YgK6RWZR4HeCB9v0nDqT2/6lUHo.dsQaMRU9S1RxWm', '1979-04-08', 'active', 0, '01222 222222', '07272 727272', 'Managing Director', 'Manchester', '', '', NULL, '2016-04-29 10:34:44', '2016-04-29 10:34:44', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'candidate', 'Austin', 'Lindsay', 'austin@red-fern.co.uk', '$2y$10$bdfwHLbWzFzIUECWijt1HOw9Yl1toFZc50N9taoF5dmIreoc..rQG', '1988-04-06', 'active', 0, '01222 222222', '07272 727272', 'Web Designer', 'Helmshore', '', '', 'X3VGH34NzDHzIOGqJG3QF8qwYCp0Wb7a59EPOeTb4CmOJg2gk8a32HuGYGdc', '2016-04-29 13:32:40', '2016-06-24 15:58:16', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 'employer', 'John', 'Doe', 'john@test.com', '123456', NULL, 'active', 0, '022112 31312', NULL, NULL, 'California', '', '', NULL, '2016-06-15 15:08:03', '2017-11-21 07:46:48', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'candidate', 'Thurstan', 'Bowling', 'thurstan@red-fern.co.uk', '$2y$10$AHgAfZFltUdtrMO.YYS8que4KlE/pCfHBx0GffszOr4htvt.miE7K', '1980-04-18', 'active', 0, NULL, NULL, 'Web Developer', 'Exton, United Kingdom', '52.691015', '-0.6320369999999684', 'xktpH1JrbmDCJAvDjrfriPixfvdNPLhqg2y3CUogjWFkB2gkMVHpOVmw4ASR', '2016-06-24 09:55:53', '2016-06-24 12:57:57', '2016-06-24 12:57:57', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 'candidate', 'Jane', 'Smith', 'janesmith@test.com', '$2y$10$c3RifEWuToL7vg1kgn2FKOuvfHYXdQA5izuymqEfQfh98d1YWSpFS', '1990-10-12', 'active', 0, NULL, NULL, 'Customer Service Advisor', 'Luton, United Kingdom', '51.8786707', '-0.4200255000000652', 'to4AvcdD5uyvLjNqsJdfD9XVFlAH5o5FfGBmxTNF7KIBHPGoAVOy8bNazvW2', '2016-07-08 14:24:03', '2016-07-08 14:41:05', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 'candidate', 'Naeem', 'Hajee', 'naeemhajee@hotmail.co.uk', '$2y$10$AFVuAEILfBamObt2HUKTDOdWsHqu3GU7cqSb1erh5dj.OSPPSYVjy', '0000-00-00', 'active', 0, NULL, NULL, 'student', 'Bolton, United Kingdom', '53.57686469999999', '-2.4282192000000578', NULL, '2016-09-25 11:44:02', '2016-09-25 11:44:02', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 'candidate', 'Aysha', 'Youssouf', 'aysha-y1@hotmail.co.uk', '$2y$10$WwytljLKVjHp7bL/4WOsruBi/rPsnHqizNjdHg7QrkDMbVZDlpZZm', '1998-05-27', 'active', 0, NULL, NULL, '', 'Bolton', '', '', NULL, '2016-10-01 16:06:11', '2016-10-01 16:06:11', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 'candidate', 'Ahmed', 'Youssouf', 'ahmed.i.ysf@gmail.com', '$2y$10$86ZesEis.SqP3N1wdj.PDeGzvMBCRrXtdwmQr0PBx3Puvlso8j7yS', '1996-08-12', 'active', 0, NULL, NULL, '', '', '', '', NULL, '2016-10-02 21:53:41', '2016-10-02 21:55:04', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 'candidate', 'Zeeshan', 'Anwar', 'zeeshan_anw4r@hotmail.com', '$2y$10$PgbPPMgsnNSXHNXrGyzRS.ddeKZ5UL7yi02x7Ji6jvOf.opS7/9c6', '1989-01-15', 'active', 0, NULL, NULL, 'Solicitor', 'Manchester, United Kingdom', '53.4807593', '-2.2426305000000184', '8PoXm0ftXI7TSkG9w0srXVhJPlCRRV09R1krgZwj3aOYuRXY2ZAFYaOe73ko', '2016-10-03 18:37:45', '2016-10-03 18:48:43', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 'employer', 'Noordad', 'Aziz', 'noordad.aziz@anderswonwyatt.co.uk', 'admin', NULL, 'active', 0, '07983641125', NULL, 'Director', 'Great Harwood, United Kingdom', '53.78191399999999', '-2.4002490000000307', NULL, '2016-10-03 19:48:35', '2016-10-03 19:48:35', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 'candidate', 'Chrissi ', 'Hyde ', 'c.j.hyde@gmx.com', '$2y$10$OWvchInIr0o0g4qUx/zxB.kFhdfrRAz0l.RgG2Jc9EuyUYpQP7kRC', '0000-00-00', 'active', 0, NULL, NULL, 'Actress', 'Cheshire ', '', '', NULL, '2016-10-22 11:43:28', '2016-10-22 11:43:28', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 'candidate', 'Jan', 'Kruger', 'nicozandberg@gmail.com', '$2y$10$pmRGSe4Qgokmm4zeJFnC5etYUffOK/8deI.bJmueN27f4qr1EfOpu', '1988-07-05', 'active', 0, NULL, NULL, 'Analyst', 'Dubai', '', '', NULL, '2016-11-08 08:03:25', '2016-11-08 08:03:25', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 'employer', 'Mohammed ', 'Raza', 'info@psem.co.uk', '$2y$10$hv1vAE5UB25PbVbMacOuCuwIjCSvbz5zXpoWlbYFhrCBLP1aGesZK', NULL, 'active', 0, '07882906254', NULL, 'Operations manager ', 'Oldham, United Kingdom', '53.5409298', '-2.11136590000001', NULL, '2016-11-10 22:18:27', '2016-11-10 22:18:27', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, 'candidate', 'Hasina', 'Jogee', 'hasinasdt@yahoo.co.uk', '$2y$10$dw10JFp3fhy0IYbtRU31EuOBa542VvjFHlOBo4B0YEVKOPxyYcBc2', '1982-10-01', 'active', 0, NULL, NULL, 'Teacher', 'Blackburn, United Kingdom', '53.748575', '-2.487528999999995', 'brXC5ZntSO6RGSPxa6VhLhrYQejeSUsy2gdUcEsDr0RyIGU0tUHZicBBbyoE', '2016-11-15 23:20:12', '2017-09-05 15:20:30', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, 'candidate', 'Tayyab', 'Sarwar', 'tayyabsarwar@icloud.com', '$2y$10$ISzCPssI1xRT9JC/rKQ4WuUBm.DA0elhDmv4hrvlFXX.A9zfWsDm6', '1994-03-04', 'active', 0, NULL, NULL, 'Student', 'Bolton', '', '', NULL, '2016-12-15 12:13:38', '2016-12-15 12:13:39', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-15 12:13:39', NULL),
(32, 'employer', 'Hasina', 'Jogee', 'mykitchenbakery@outlook.com', '$2y$10$F8tDWNPyFC8OnZxWpVEYYO6Ny75oHebEl49Ov1no4D6L0CiAHjaku', NULL, 'active', 0, NULL, NULL, 'Director', NULL, NULL, NULL, 'BwuGi2Ic3PiFiG5nk3mUJwrO0fr6Zdg6ezLRZfwgb8P68D4QME1lfn5nPyAt', '2016-12-30 16:38:14', '2017-09-11 17:35:05', '2017-12-14 01:43:36', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, 'employer', 'Emily-Jo', 'Sutcliffe', 'emily@red-fern.co.uk', 'admin', NULL, 'active', 0, NULL, NULL, 'Studio Manager', NULL, NULL, NULL, 'SjXgkVWRFTz9UzRC2cEOtt06eEkU7LBXxUkgAEys2X3I21GPT4HziXTGVyYg', '2017-01-13 15:21:30', '2017-03-16 17:50:44', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 'employer', 'Admin', 'Recruiter', 'careers@cvvid.com', '$2y$10$vImXa4B4Y4jwfT0zQyoVOeA4485Oveb9roh56kbvCIZculIq4GVhq', NULL, 'active', 0, NULL, NULL, 'Staffing', NULL, NULL, NULL, 'SxwFsbZlWUwkWcH9HcpHH3socyC805tEwVVl4FuqMgDUyS7vEiTqOHEdmKVj', '2017-01-18 09:29:12', '2017-09-12 07:05:07', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, 'employer', 'Mohammed', 'Jogee', 'ish@fonezone.co.uk', '$2y$10$tsM7xr8mNxKRTWyslc1PteSOWn4UTr.hv4an6sjysoqQfwV6jNhQC', NULL, 'active', 0, NULL, NULL, 'Director', NULL, NULL, NULL, NULL, '2017-01-20 21:34:27', '2017-01-20 21:34:27', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, 'employer', 'Farooq', 'Umar', 'info@printhubdesign.co.uk', '$2y$10$zGS5iv7JDiLdy/cq8yjCnehuiglfS6K00yjgAcc0AId/sVKmUhlsu', NULL, 'active', 0, NULL, NULL, 'Mr', NULL, NULL, NULL, '1XF6yvYiqEHWagaiayTUHZuhvJsTnanl6NIxOF6fi8JDIsLbl6vdWffPvtDz', '2017-01-24 15:33:05', '2017-02-10 14:29:50', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, 'candidate', 'Hasina', 'Bangi', 'hjogee@outlook.com', '$2y$10$LnpH2Y.rBMB7sPGkE0QNWO0NQu0NHRQzYrGahYYcsRxB27.o5Zj/y', '1982-10-01', 'active', 0, NULL, NULL, 'Teacher', 'Blackburn, United Kingdom', '53.748575', '-2.487528999999995', 'ReGvcWK9rDSaTlKIKuuUNGFjUJ0PuzSCH8lhog896aHPEwXigMdCbYovVJ9Q', '2017-01-26 11:55:39', '2017-05-22 06:20:47', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-26 11:55:39', NULL),
(41, 'employer', 'Dan', 'Wilmott', 'dan@red-fern.co.uk', '$2y$10$68acjwob.afeAEl7QAKcSep5iHZ0OAGortgijAhW4iHfY5L/RWro2', NULL, 'active', 0, NULL, NULL, 'Web Designer', NULL, NULL, NULL, 'HXFEencC6fh4972WEdOZM5sLeJAhMFMJL5BCovhjzUmMrRz7uljO6ezHmH2r', '2017-01-27 16:03:40', '2017-01-27 17:51:42', '2017-12-14 02:28:46', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(42, 'employer', 'Daniel', 'Fisher', 'daniel.fisher@tiendadigital.co.uk', '$2y$10$Cwcm.owqFqTjO1BbwUk49.sQnUfWH64ZzxWFwJJP/KNknK3nQXqaC', NULL, 'active', 0, NULL, NULL, 'Director', NULL, NULL, NULL, NULL, '2017-01-30 10:05:21', '2017-01-30 10:05:21', '2017-12-14 01:33:49', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(44, 'employer', 'A', 'B', 'bhaveshnayi@rlogical.com', '$2y$10$GmDARPje8z/oCLm8WSUpzuTvk1JTI3RzAyv6YSbVNtnn0ZVmPtxfe', NULL, 'active', 0, NULL, NULL, 'ACCOUNT', NULL, NULL, NULL, 'JoYzIX1xKULFz9IpeqAstHlT0sBMFPn87zvu3NiBxTyJqYSpSZGomJ0UOkfa', '2017-02-01 12:13:44', '2017-02-01 12:30:31', '2017-12-14 01:35:35', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(45, 'employer', 'Fatehu', 'Tahir', 'faz@marblemanagement.co.uk', '$2y$10$iEWL.kgYou0VIqh/6cciZu9wRm1FO2nHUhZz5bt/UkZ/41PqnrflW', NULL, 'active', 0, NULL, NULL, 'Director', NULL, NULL, NULL, NULL, '2017-02-01 18:59:02', '2017-02-01 18:59:02', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(46, 'employer', 'anil', 'rathod', 'aniruddh@rlogical.com', '$2y$10$LLfIRTbHvbsBvQnAgnDTV.0J6SHihSkNuIquaYu9mtNb2tZ1Ettty', NULL, 'active', 0, NULL, NULL, 'hello', NULL, NULL, NULL, NULL, '2017-02-02 12:51:12', '2017-02-02 12:51:12', '2017-12-14 05:18:52', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(47, 'employer', 'Susan', 'Jones', 'susan.jones@novaadventurechallenges.com', '$2y$10$wVjKolODnCVTpldMwz8mmuQosr3QRcLJoCetrctEpvwF6k0cFHkJ2', NULL, 'active', 0, NULL, NULL, 'Project Manager', NULL, NULL, NULL, 'wdBsgQnObdkM2lF2GqBjMI5W1gINkBuCIjPLdX5bMzDi86VKV8iipkof6BCA', '2017-02-02 16:10:28', '2017-09-11 17:46:41', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, 'candidate', 'Joe', 'Emery', 'joe_emery@icloud.com', '$2y$10$cFWnUxPLI8BrdAGoFQVm0.oyeQgZedjkUaKL9h1nwNrnVrQC.GW7e', '1982-01-29', 'active', 0, NULL, NULL, 'Freelance Marketing Consultant', 'London, United Kingdom', '51.5073509', '-0.12775829999998223', NULL, '2017-02-10 21:20:28', '2017-02-10 21:20:28', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2018-02-10 21:20:28', NULL),
(50, 'employer', 'Mosharaf', 'Ali', 'Mosharaf.ali@humanappeal.org.uk', '$2y$10$GEHo2aSqDvgt1yARGMDYWeLkgHoWdkbMDU1wOSKvdau8M4q53enOu', NULL, 'active', 0, NULL, NULL, 'HR Administrator ', NULL, NULL, NULL, NULL, '2017-02-14 10:40:05', '2017-02-14 10:40:05', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, 'candidate', 'Sadiq', 'Basha', 'sadiq@iecabroad.com', '$2y$10$TEKTdlOWyy6lpG9qaPLeO.stfmFGS9Gt0JQFdmLJcdMcOzWsg6zue', '1980-01-07', 'active', 0, NULL, NULL, 'MD', 'Manchester, United Kingdom', '53.4807593', '-2.2426305000000184', NULL, '2017-02-16 16:18:36', '2017-02-16 16:21:24', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(53, 'institution_admin', 'Arif', 'Bangi', 'hello@cvvid.com', 'admin', NULL, 'pending', 0, '1234456', NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-20 22:22:48', '2017-02-20 22:22:48', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(54, 'institution_admin', 'Asia', 'Ali', 'asia.ali@tighs.tetrust.org', '$2y$10$XUo1jpyhPLabcul4/3IicePftZaP96Oh8ZK1iSeANaCG5.D9iXfFm', NULL, 'active', 0, '', NULL, NULL, NULL, NULL, NULL, 'dL78bpp7ianxx67NIMnFLOPx7DMPrAkoo0RkZeQbTl4IL1853EEszPOH4PqL', '2017-02-21 08:34:39', '2017-09-15 08:10:47', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, 'employer', 'Bangi', 'arif', 'arif.bangi@gmail.com', '$2y$10$qK0HJw0WLTKQVEYOT/vJFeihi8JDRCBTC3Y0YcZ2wli2DpX9Q7iYu', NULL, 'active', 0, '07714569570', NULL, NULL, 'Manchester', '', '', NULL, '2017-02-21 13:49:55', '2017-02-21 13:49:55', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(57, 'admin', 'Mr', 'Bangi', 'info@cvvid.com', '$2y$10$1t87sb07ZxdzXR/ChxAyXeh4GZ50Mrph38lInaEtWcZPxhe3HPQr6', '0000-00-00', 'active', 0, '07714569570', '', 'Brand Strategist', 'Manchester', '', '', NULL, '2017-02-21 14:08:34', '2017-02-21 14:08:34', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 'employer', 'Arif', 'Bangi', 'arif@arifbangi.com', '$2y$10$JHo0Yc2Zge/UmZI/hOD1je.AG9lT2VL.DpXOKveVPFqLGMpxmxaWy', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'eREq7BCXOukTfJwd2ukByF2Q6GaMNk26KboDf8Rh80G5o0NVingrEVsqYWyO', '2017-02-21 17:15:10', '2017-03-31 07:00:14', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 'institution_admin', 'Adam', 'Wilson', 'awilsones@gmail.com', 'admin', NULL, 'active', 0, '112414', NULL, NULL, NULL, NULL, NULL, NULL, '2017-02-28 11:37:56', '2017-02-28 12:00:56', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(71, 'teacher', 'Bashira', 'Patel', 'bashira.patel@tighs.tetrust.org', '$2y$10$2zr.DMC3zuVJPFWVUYPugOr/6/ycc.5P218sOjMajedkxVup5gxrC', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'YPdJo9DCeqEviqL2V92e5K6aYxToYjXqo833JRng54D3kYfTNaVBDvxH8Owu', '2017-03-16 12:19:04', '2017-08-29 09:08:04', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(74, 'institution_admin', 'GeorgePep', 'GeorgePep', 'simmpybyqtan@mail.ru', '', NULL, 'pending', 0, '89979775849', NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-21 13:18:59', '2017-03-21 13:18:59', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(75, 'institution_admin', 'WilliamVax', 'WilliamVax', 'sicrfcimer@mail.ru', '', NULL, 'pending', 0, '85594132318', NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-21 13:53:34', '2017-03-21 13:53:34', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(76, 'institution_admin', 'Robertidody', 'Robertidody', 'didnaxio@mail.ru', '', NULL, 'pending', 0, '81719548418', NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-21 14:21:42', '2017-03-21 14:21:42', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(78, 'candidate', 'Bashira', 'Patel', 'bcongress@me.com', '$2y$10$dORzWQb9I6u.FAAQmWx/J./IHonrWU6SsVt8fqPU5WFmd.1J3bkyO', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-24 08:50:28', '2017-03-24 08:50:28', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(81, 'institution_admin', 'DanielBek', 'DanielBek', 'mccacszwyr@mail.ru', '', NULL, 'pending', 0, '81751846529', NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-24 19:59:09', '2017-03-24 19:59:09', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(82, 'institution_admin', 'JamesWew', 'JamesWew', 'polgnwsigwin@mail.ru', '', NULL, 'pending', 0, '88673625361', NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-26 12:01:34', '2017-03-26 12:01:34', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(84, 'institution_admin', 'DarwinHuh', 'DarwinHuh', 'vircetaecra@mail.ru', '', NULL, 'pending', 0, '88288453536', NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-30 01:49:58', '2017-03-30 01:49:58', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(85, 'institution_admin', 'Arthurwap', 'Arthurwap', 'himebvvjh@mail.ru', '', NULL, 'pending', 0, '81269748352', NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-30 13:43:20', '2017-03-30 13:43:20', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(86, 'institution_admin', 'ErvinCet', 'ErvinCet', 'arlummucri@mail.ru', '', NULL, 'pending', 0, '89552892645', NULL, NULL, NULL, NULL, NULL, NULL, '2017-03-30 17:07:09', '2017-03-30 17:07:09', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(87, 'institution_admin', 'Colin', 'Andrews', 'cvvidnew@gmail.com', 'admin', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'vlZ5Q56yJEuZw9PH6NlnYArbZLd0GNw3pU8BIb2OYFNJgzSc9or6HEibueCf', '2017-04-11 08:36:59', '2017-09-12 08:40:10', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(88, 'teacher', 'Andrew', 'Colins', 'cvvidteacher@gmail.com', '$2y$10$abfBjgPBaWNNAmlTZmD15.0shpOVkrnhk61qOD1Jj18ZmA9vnezDe', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'HtpUcriO6di2ry8Sp0rwedYIzG4rPlGnTrvY1bhXwTjpCoh5g0EYkmxKgPtr', '2017-04-11 10:36:49', '2017-06-05 15:07:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(90, 'candidate', 'Adam', 'Henderson', 'adamhendersoncvvid@yahoo.com', '$2y$10$JCsPz99F3cOVv2S6.LLAGePIDrjaYtMVMmvK2h92CZyZSN/.oh8P.', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'DHqgL4KtQOoVgWdslxuQmxxU2MCyqvJrKFu9X8HIqnszvOtddtI7Ogk5kkfA', '2017-04-11 11:32:09', '2017-08-10 11:39:47', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(91, 'candidate', 'Stefanie', 'Collins', 'stefcvvid@mail.com', '$2y$10$XNiUJg7ggPlteQ01bBaMwu8xnf/TLBydZbdRcT7QSCUMGDd04vfK2', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, '82SIgRDTYEVU8NlCyXVYdBVT38kJBKYNqoaSkrr1Hrnl6gsh8TusLLDFTa96', '2017-04-16 10:52:24', '2017-09-12 08:41:38', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(92, 'candidate', 'Paul', 'Williams', 'paulcvvid@yahoo.com', '$2y$10$UsFQskjp8z7JYT50Uf894emvllnGY.B3ouZIYsrJk2OyLGttsNI.y', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'Hvs4pTkLzFLOkWqScAlBdlXQh6oMU70CVW4SQDQ7CPcygEbcBJB82MWolajD', '2017-04-16 11:07:47', '2017-04-16 11:10:47', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(93, 'teacher', 'Susan', 'Davies', 'susanteacher@yahoo.com', '$2y$10$XlPggmB.Mk/iwv9ChwVLiuX8EuIZd1xhurI0MZdL.1JfNtUSWnnuK', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, '31MST91mTyaVgl3sYq7nGqMyfAG8OCoFCMBmkybmaEyLAXuSbjZZcXd39Xn8', '2017-04-16 11:22:05', '2017-07-17 22:01:59', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(95, 'teacher', 'Peter', 'Warbutton', 'peterteacher@yahoo.com', '$2y$10$WEUkFgx4NSjPox5swFy65.5pN034PqyT0kDrCBQejrQFEQlkF7mqe', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-16 11:29:33', '2017-04-16 11:29:33', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(97, 'teacher', 'Samantha', 'York', 'samteacher@yahoo.com', '$2y$10$wSgj88vvw2Ldo6qzP5lDfOaPTHkyd8.Wp/0FswyyJfEN8B2slVmmq', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'emcDpUKstTZWKC8TCfAQr4ZJdqjj4MXMnRK9DM2eTTOmCr13EdYn31j2Nj29', '2017-04-16 11:45:46', '2017-06-05 19:17:12', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(98, 'candidate', 'Porsche', 'Blue', 'porsche@cars.com', '$2y$10$92YhI4QK5sSFrRxZf5lNgOlbC0IRYkqIYBb4c/FI2ZZSPtBrkH27q', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-04-16 11:45:46', '2017-04-16 11:45:46', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(99, 'candidate', 'Samuel', 'Thornton-Greet', 'samuelthorntongreet@gmail.com', '$2y$10$4e.VpvTK/hYFU/hBiuanXeUIFy.gZea/8jr3TTk850zmDIk0LMQae', '2001-08-14', 'active', 0, NULL, NULL, 'unimployed', NULL, NULL, NULL, NULL, '2017-04-27 19:13:15', '2017-04-27 19:13:15', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(100, 'teacher', 'Emily', 'James', 'emily@red-fern-test.co.uk', '$2y$10$8mR8JheSxysFwEyx.EFNxePe08ONFfDAPUjaTAmHbWW4DgXMVY4sm', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-05-24 11:13:11', '2017-05-24 11:13:11', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(105, 'candidate', 'Ayah ', 'Shah ', 'ayahshah00@gmail.com', '$2y$10$VqMq8URhzA5WS8Lhd8GRjemLBtsrYLuJFbjFak2N.9DNbEQkt/6IC', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-06-14 06:32:36', '2017-06-14 06:32:36', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(106, 'candidate', 'Sara', 'Ziglam', 'sosozige@hotmail.com', '$2y$10$DrQduJX49wLmn1v.aTC27OmwEwJPtCPI5q.LTqrSpZJ58qdbAP08C', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-06-14 06:33:23', '2017-06-14 06:33:23', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(107, 'candidate', 'Arifa', 'Patel', 'arifa.patel@outlook.com', '$2y$10$xm7H/B3hPqZRTZXryMFSc.jMOhq9CFY9BPzDay9Hyzb3lp8hp1rNe', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-06-14 06:34:58', '2017-06-14 06:34:58', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(108, 'candidate', 'Maryam ', 'Sharief', 'maryam.sharief@yahoo.co.uk', '$2y$10$u5cQZWdMmrp2ig/dYtww6ewTJRqoHW6tXECZvlTv1T/7CU5APuoXC', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-06-14 06:35:54', '2017-06-14 06:35:54', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(109, 'candidate', 'Khansa', 'Iqbal', 'khansa.iqbal@icloud.com', '$2y$10$mF2muilLwLAYdsT1wKQGuu43imNHRMF7R4Nb8k7NPxIoXxWauVel6', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, '69yuJbfu59bxckT0IHr77LdEiuj2nlPw7enTvS78GXfOLEzywuRX7yes4Brl', '2017-06-14 06:36:36', '2017-06-16 16:01:45', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(110, 'candidate', 'Fatimah Zahra', 'Patel', 'fatimahzahrapatel@gmail.com', '$2y$10$Gg5AwCblWpCW3GPIhgnuxOhUdW6ytIGXCCwQRg96d9C9UKy9ommxC', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-06-14 06:37:45', '2017-06-14 06:37:45', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(111, 'candidate', 'Maryam ', 'Patel', 'maryam_9@hotmail.co.uk', '$2y$10$RlzaqXEA6KB4UQk.nLnituoPa9MRxC0zVLRCYGqee/MeXAjsGe4pm', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-06-14 06:38:33', '2017-06-14 06:38:33', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(112, 'candidate', 'Hafsah', 'Patel', 'hafsahpatel15@hotmail.com', '$2y$10$5B/tArpOBsGTihkJSgts7OGj.lqLJQaTQ0.fgzMvEkT9q0z/aBKci', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-06-14 06:39:57', '2017-06-14 06:39:57', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(113, 'candidate', 'Aisha', 'Daya', 'aishadaya27@hotmail.co.uk', '$2y$10$jUi0UZyCJSA4vIWLVtdKSejp8F7V/mvcB5.xLUEa07YQzcD.f9k6O', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-06-14 06:40:55', '2017-06-14 06:40:55', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(114, 'candidate', 'Aaiza', 'Patel', 'aaiza.patel@gmail.com', '$2y$10$Va.YHqOwZOmVZoUITeBTCeykQgFuIBSWHmGrtpQtkIDxOl/ai5xU2', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-06-14 06:41:43', '2017-06-14 06:41:43', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(115, 'candidate', 'test', 'user', 'testusers@gmail.com', '$2y$10$LYwTnmXXmR1RpYHeeHgL8OtdhBH0bd8qItrAfAThrz1TsH1NsfCqq', '1990-10-01', 'active', 0, NULL, NULL, 'devloper', NULL, NULL, NULL, '1plvlwdIqgXP67HoCHGyMGXQjDBylxFahDhAWxr7o33xCXBAoyjX2vpu47nl', '2017-06-15 11:23:32', '2017-06-15 11:36:26', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(116, 'candidate', 'candidate', 'surenam', 'candidate@mailinator.com', '$2y$10$ZrA.MQ0iWV2J4x/iMUe.X.dtOvGXMBf5VpsS8AG3bL0XBaBqbfv3G', '1990-10-01', 'active', 0, NULL, NULL, 'devloper', NULL, NULL, NULL, NULL, '2017-06-15 11:39:09', '2017-06-15 11:41:58', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(117, 'candidate', 'John', 'Doe', 'info@lab6.net', '$2y$10$.A2ANfVTcrGncPIP0qFZtuZj3qK.UjA65PCzBDi1J/g/xQTTeKxne', '1995-03-20', 'active', 0, NULL, NULL, 'Web Developer', NULL, NULL, NULL, NULL, '2017-06-20 12:20:02', '2017-06-20 12:20:02', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(118, 'candidate', 'david', 'Althar', 'jpulikkottil@gmail.com', '$2y$10$A.FPK./1dd2DdKwF0QqIIevLH69Ch8sXtK958yBYasww70uqmn5u.', '1988-03-08', 'active', 0, NULL, NULL, 'SSE', NULL, NULL, NULL, 'EFDCWYUjrisHTLDRzEkpVYrki2DYmPH0jA39QbcvEouadsTmlabsU4ZOewr5', '2017-07-26 08:49:53', '2017-07-28 06:29:31', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(119, 'candidate', 'j', 'p', 'zcotestmail@gmail.com', '$2y$10$PWfX0bDlMYaylGBN0iSImeXga5I2J1jWQAyGBs/0O6H7sEt5NUWZy', '1990-03-03', 'active', 0, NULL, NULL, 'TEST', NULL, NULL, NULL, 'qiVUa5p7ulekObRl1mbDEisZrsNCSg9i5ZLOV26zEJRszsjvaMlFl4CnjUQu', '2017-07-27 03:51:50', '2017-07-28 07:56:27', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-27 03:51:50', NULL),
(120, 'candidate', 'Ben', 'Lind', 'ben@hedgehoglab.com', '$2y$10$IGXoUc7lIP72o5jj2Zqt1e.sUiq/GwpFyvzdEEhcsJxI./WjsA69.', '1988-09-09', 'active', 0, NULL, NULL, '', NULL, NULL, NULL, NULL, '2017-07-27 08:07:04', '2017-07-27 08:07:04', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(121, 'employer', 'j', 'p', 'zcotestmail2@gmail.com', '$2y$10$/QHPrcFRBedCnb5dOFdMP.Mx1yFADx/nmAgSpQAeBqUFvjCf/CMiK', NULL, 'active', 0, NULL, NULL, 'v', NULL, NULL, NULL, 'OmnTJwFJDD1sWugSHmhumjahRC7LGAswXf5MvkUQbstT4lYOLNN8UyozDDmg', '2017-07-27 09:44:26', '2017-07-28 07:57:54', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(122, 'institution_admin', 'test', 'tt', 'testmail3@gmail.com', '', NULL, 'pending', 0, '12', NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-27 12:04:01', '2017-07-27 12:04:01', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(123, 'employer', 'w', 'w', 'test@test.com', '$2y$10$GMckyDXMpe7fKn12qS8JdeKxQYykIkO/SpdJF23TrJS5V7y4NgewS', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-07-28 06:39:47', '2017-07-28 06:39:47', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(124, 'candidate', 'shyam', 'P', 'shyamtests247@gmail.com', '$2y$10$A.dGpkQV1./hPv6UqJ3EyOEyOTIubVSJSKrnhiveuRyQILiIqHzum', '1983-01-05', 'active', 0, NULL, NULL, 'SBA', NULL, NULL, NULL, 'LxLPlC4yoRkjVKt4aKonYaufGbFyWMjbJeot8T0g5XbMwh5imWQLEc1HPonY', '2017-08-14 12:05:32', '2017-08-14 12:10:39', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-14 12:05:33', NULL),
(125, 'employer', 'ssc', 'ssc', 'shyampradeesh10@gmail.com', '$2y$10$pP9w8ZIqulR.kO89czo15uh4Y78MEXv6Dmd0b3Ay/UFk7OoQXZgHC', NULL, 'active', 0, NULL, NULL, 'BA', NULL, NULL, NULL, 'Jn5puAt98IcPetIhbx0WZ2phTSHy9salJHchTeojMF150ZE5Ft1ndy79oBar', '2017-08-14 12:15:57', '2017-08-14 12:23:58', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(126, 'candidate', 'Jeff', 'Jaq', 'pravipravi@hotmail.com', '$2y$10$xDTOcgkAI/cjPPbmkRfk9OwHW6yq5ksQrkVAQzsI0Pzy4Ow.qE88C', '1986-03-10', 'active', 0, NULL, NULL, 'Engineer', NULL, NULL, NULL, 'e2qOoJdjDl47WnT9OFDc8iS1iu1VZH1hbY8UWCNrgXCPLwIDFZZbpD5jOodC', '2017-08-14 18:15:05', '2017-08-14 18:15:39', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(127, 'candidate', 'dev1', 'Tienda', 'developer1@tiendadigital.co.uk', '$2y$10$3Th9yFUg3SOqSelc5f30tOQUa.3hT0b/Wz9K2RftgdASY4ndK/mlS', '1989-07-10', 'active', 0, NULL, NULL, 'Developer', NULL, NULL, NULL, 'g6H0GQNU3tnAu6mxwVTQvnzQzQ1D8gZZQYufK3ctnzogrAh3FlVVM25uzeKP', '2017-08-15 15:56:27', '2017-08-15 16:02:14', '2017-12-14 01:33:37', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-15 15:56:28', NULL),
(128, 'employer', 'dev', 'tienda', 'developer2@tiendadigital.co.uk', '$2y$10$eSPExBlbp/aOEph0srgyuOICJxGHdLDAcZDuupVBGYpMpLTbUBQoy', NULL, 'active', 0, NULL, NULL, 'developer', NULL, NULL, NULL, 'cB9PuBGkN1WldbwK6ekFsbyrrkVpVdYhp3Ckd40SApwCWfLLNvJ6vySkglsJ', '2017-08-15 16:05:28', '2017-08-15 16:15:35', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(129, 'candidate', 'Eleisha', 'Cartlidge', 'eleisha.cartlidge@tiendadigital.co.uk', '$2y$10$5n61ItkT7HF9DeLtb3gzJuZqqwtY7aATCS1VJktAqyqhaAhjqFrLS', '1995-04-05', 'active', 0, NULL, NULL, 'Marketing Executive', NULL, NULL, NULL, 'ExVP6EpfSVDRGKlLFK779gYdP17HEAzeRFTLU64J9Vfj73iATdL5QFjbz7sh', '2017-08-22 07:50:59', '2017-09-13 12:19:37', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(130, 'candidate', 'Martin', 'Mulvey', 'eleisha@tiendadigital.co.uk', '$2y$10$QbqH5mOEbATECneiPoD0H./7MgVHabPLcJeeGC1AHjizBFCr3dvW2', '1988-05-02', 'active', 0, NULL, NULL, 'Account Manager', NULL, NULL, NULL, 'yuWNN99PRpQ2NSDw1HdPOzItWP3BFSJjQQ1zkhzmS36x05lKxYUiXeqROH47', '2017-08-22 08:37:30', '2017-08-29 14:08:54', '2017-12-14 01:33:31', 0, NULL, NULL, NULL, NULL, NULL, NULL, '2018-08-22 08:37:30', NULL),
(131, 'employer', 'Eleisha', 'Cartlidge', 'eleisha+1@tiendadigital.co.uk', '$2y$10$KtXSV3q2081ZgPftIny6auwsSymsPIcIPubHSpbqupbteua/vfxpa', NULL, 'active', 0, NULL, NULL, 'Marketing Executive', NULL, NULL, NULL, 'eWjCwTEqwPWruznPluq2KsBJTczb6wzof4Os7kg8wXTQX0iYBEIvqmXyB48j', '2017-08-23 12:00:49', '2017-09-04 11:42:04', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(132, 'employer', 'Daniel', 'Fisher', 'hello@tiendadigital.co.uk', '$2y$10$a7SUCCtpNkQuT.ZR88zAaepeJ6IGJM.jWecDSHEWvamgMHhteYoNS', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-08-23 12:27:24', '2017-08-23 12:27:24', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(133, 'employer', 'Martin', 'Mulvey', 'eleisha+2@tiendadigital.co.uk', '$2y$10$TSrHAyXOj/30hyK2kgXNz.QgU20ja3snDXBe8Px3SwgDqHG7V/EMO', NULL, 'active', 0, NULL, NULL, 'Account Manager', NULL, NULL, NULL, 'vAM1ILM9gTvkml50C2X0XW5wHx7Nc7UBJmxOH5ZrH8IPXIx0c7c61uskIkXq', '2017-08-23 12:38:27', '2017-09-14 13:25:29', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(134, 'institution_admin', 'John', 'Smith', 'eleisha+3@tiendadigital.co.uk', '', NULL, 'pending', 0, '01772 494855', NULL, NULL, NULL, NULL, NULL, NULL, '2017-08-24 11:46:12', '2017-08-24 11:46:12', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(135, 'candidate', 'Eleisha', 'Cartlidge', 'eleisha.cartlidge+1@tiendadigital.co.uk', '$2y$10$KGFqVCM1IqARRzE3WSHN4.Q5n.JBjfX1IgtWUtbHUP4JqL7S2qlM.', '0000-00-00', 'active', 0, NULL, NULL, '', '', '', '', '4lDKpMojVpDh4WhiIa5uJoIpxVuZopzGns2jaoNjyfEH3jbQYj2oJVi9vM4R', '2017-08-24 13:10:03', '2017-08-25 08:07:10', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(136, 'teacher', 'Eleisha', 'Cartlidge', 'eleisha.cartlidge+5@tiendadigital.co.uk', '$2y$10$Avc9APtnRh2uXoweEVGIBOLF4mFWlhRQ74SEwbDSrgm98Ge4F/AGi', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, 'oLRR0l1fE11AvdViqWiJDpxZopzThbGA1XW7lEBAwjN8TXBm1ltm3YZn7f4o', '2017-08-29 08:16:50', '2017-08-29 09:07:46', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(137, 'candidate', 'Martin', 'Mulvey', 'eleisha.cartlidge+4@tiendadigital.co.uk', '$2y$10$f9tzjD0hKroXRo3F8UAzGeVrPNNmD1GLmVEJR7jFk7Rxz31YtmJCG', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-08-29 08:26:07', '2017-08-29 08:26:07', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(138, 'candidate', 'Anastasia', 'Barashkova', 'nfb.fmd@yandex.ru', '$2y$10$etzvhBwlqwRm8BuSXOjppeVc2HwIXAOjYxdmXIlwDSMJxASBDTeva', '1987-06-05', 'active', 0, NULL, NULL, 'Designer', NULL, NULL, NULL, 'NR0X6srwQ4XBrTitLA1d9800FB4g1XPsxVBfJM7I6ol7NCK6Vkqzc27eq64p', '2017-09-11 08:54:10', '2017-09-11 10:09:22', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(141, 'candidate', 'Ayan', 'ayyanar', 'ayyanarpms@gmail.com', 'testing', NULL, 'active', 0, NULL, '46767889', NULL, NULL, NULL, NULL, NULL, '2017-10-07 08:00:49', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(142, 'candidate', 'aaaa', 'qqqq', 'testa@gmail.com', 'admin', NULL, 'active', 0, NULL, '88888888', NULL, NULL, NULL, NULL, NULL, '2017-11-09 07:06:49', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(143, 'employer', 'ezhil', 'valavan', 'ezhil123@gmail.com', '123456', NULL, 'active', 0, '123456', NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-18 07:49:47', '0000-00-00 00:00:00', '2017-11-25 00:35:33', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(145, 'employer', 'eeeeee', 'eeeee', 'eeeeee@gmail.com', '123456', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-21 02:15:02', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(146, 'employer', 'fffff', 'fffff', 'fff@gmail.com', '123456', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-21 02:15:36', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(149, 'employer', 'eeeee', 'eeeee', 'eeeee223e@gmail.com', '123456', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-21 06:13:40', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(150, 'employer', 'tttttt', 'ttttt', 'tttt@gmail@com', '123456', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-21 06:16:40', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(151, 'employer', 'dddddd', 'dddddd', 'sssssss@gmail.com', '123456', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-21 06:22:45', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(153, 'employer', 'ppppp', 'pppp', 'ppp@gmail.com', '123456', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-11-21 06:31:33', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(154, 'employer', 'ezhil', 'eee', 'ezhil234@gmail.com', '123456', NULL, 'active', 0, '433535', '898989', 'Marketing Strategist', NULL, NULL, NULL, NULL, '2017-11-24 00:37:23', '2017-11-24 06:34:44', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(156, 'employer', 'sss', 'sss', 'sssssss234@gmail.com', '123456', NULL, 'active', 1, '022112 31312', NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-07 07:59:17', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(157, 'employer', 'sss', 'sss', 'sssssss23455@gmail.com', '123456', NULL, 'active', 1, '022112 31312', NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-07 08:07:15', '0000-00-00 00:00:00', '2017-12-14 05:28:54', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(158, 'candidate', 'ss', 'sfsfs', 'sfsf@gmail.com', '123456', '2017-12-19', 'active', 0, NULL, NULL, 'Marketing Strategist', NULL, NULL, NULL, NULL, '2017-12-08 01:47:37', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(159, 'candidate', 'adada', 'adada', 'adameee@red-fern.co.uk', '123456', '2017-12-20', 'active', 1, NULL, NULL, 'Web Developer', NULL, '-38.24817650000001', '141.88783690000002', NULL, '2017-12-08 02:03:12', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(160, 'institution_admin', '12555', 'valavan', 'ezhil5555@gmail.com', '123456', NULL, 'active', 0, '433535', NULL, NULL, NULL, '11.7698354', '79.53224239999997', NULL, '2017-12-08 04:42:27', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(161, 'candidate', 'Tony', 'Stark', 'tony@test.com', 'testing', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-23 05:53:10', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(162, 'candidate', 'Steve', 'Rogers', 'steve@test.com', 'capamerica', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-23 05:53:10', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(163, 'candidate', 'Bruce', 'Banner', 'bruce@test.com', 'hulk', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-23 05:53:10', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(164, 'candidate', 'Maria', 'Hill', 'maria@test.com', 'maria', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-23 05:53:10', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(165, 'candidate', 'Clint', 'Barton', 'clint@test.com', 'hawkeye', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-12-23 05:53:10', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(166, 'candidate', 'aaa', 'aaaa', 'ezhil8877@gmail.com', '123456', '2017-12-12', 'active', 0, '433535', '898989', 'Marketing Strategist', NULL, NULL, NULL, NULL, '2017-12-29 06:10:40', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-29 06:10:40', NULL),
(167, 'admin', '12', 'valavan', 'ezhil555555@gmail.com', '123456', '1970-01-01', 'active', 0, '433535', '898989', 'Marketing Strategist', NULL, NULL, NULL, NULL, '2017-12-30 02:03:24', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(168, 'admin', 'dgdg', 'dgdgd', 'dgdgd@gmail.com', '123456', '0000-00-00', 'active', 0, '433535', '898989', 'Marketing Strategist', NULL, NULL, NULL, NULL, '2017-12-30 02:08:02', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(169, 'admin', '12', 'valavan', 'dgdgggd@gmail.com', '123456', '2017-12-21', 'active', 0, '433535', '898989', 'Marketing Strategist', NULL, NULL, NULL, NULL, '2017-12-30 02:11:03', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(170, 'candidate', 'cff', 'fff', 'fbfb@gmail.com', '123456', '2017-12-12', 'active', 0, '433535', '898989', 'Marketing Strategist', NULL, NULL, NULL, NULL, '2017-12-30 07:46:28', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2018-12-30 07:46:28', NULL),
(171, 'candidate', 'eeeee', 'eeeee', 'eeeee223ee@gmail.com', '123456', '2017-12-19', 'active', 0, '433535', '898989', 'Marketing Strategist', NULL, NULL, NULL, NULL, '2017-12-30 07:48:13', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(172, 'employer', 'ssss', 'ssss', 'sssssss2dd3455@gmail.com', '123456', NULL, 'active', 0, 'ssvs', NULL, NULL, NULL, '-34.6035846', '-58.375389299999995', NULL, '2018-01-02 04:45:38', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(173, 'employer', 'scsc', 'scscs', 'sscs@gmail.com', '123456', NULL, 'active', 0, 'adadad', NULL, NULL, NULL, '11.7698354', '79.53224239999997', NULL, '2018-01-02 04:53:14', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(174, 'employer', 'scsc', 'scscs', 'sscs44@gmail.com', '123456', NULL, 'active', 0, 'adadad', NULL, NULL, NULL, '', '', NULL, '2018-01-02 04:54:43', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(175, 'employer', 'scsc', 'scscs', 'sscs4433@gmail.com', '123456', NULL, 'active', 0, 'adadad', NULL, NULL, NULL, '', '', NULL, '2018-01-02 04:55:03', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(176, 'employer', 'scsc', 'scscs', 'sscs4w433@gmail.com', '123456', NULL, 'active', 0, 'adadad', NULL, NULL, NULL, '-38.24817650000001', '141.88783690000002', NULL, '2018-01-02 04:55:28', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(177, 'employer', 'scsc', 'scscs', 'sscs4waq433@gmail.com', '123456', NULL, 'active', 0, 'adadad', NULL, NULL, NULL, '-38.24817650000001', '141.88783690000002', NULL, '2018-01-02 04:57:54', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(178, 'employer', 'adad', 'adad', 'adsad11@gmail.comss', '123456', NULL, 'active', 0, 'adad', NULL, NULL, NULL, '50.0856921', '8.782361100000003', NULL, '2018-01-02 05:32:31', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(179, 'employer', 'aadad', 'adad', 'adaaaad@gmail.com', '123456', NULL, 'active', 0, 'adada', NULL, NULL, NULL, '-34.6035846', '-58.375389299999995', NULL, '2018-01-02 05:35:57', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(180, 'employer', 'dadadadscsww', 'aaa', 'sscsaa4waq4a33@gmail.com', '123456', NULL, 'active', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-02 06:07:44', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(181, 'candidate', 'asdasds', 'asssdsas', 'assddsd@gmail.com', 'testing', '2018-01-19', 'active', 0, '34535', '34535345', 'admin', NULL, NULL, NULL, NULL, '2018-01-18 03:30:15', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(183, 'candidate', 'qa', 'wqw', 'asd@gmail.com', 'testing', '2018-01-19', 'active', 0, '6545', '3656465', 'testug', NULL, NULL, NULL, NULL, '2018-01-18 03:36:01', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(184, 'candidate', 'va', 'we', 'wea@gmail.com', 'testing', '2018-01-19', 'active', 0, '65465', '465654', 'admin', NULL, NULL, NULL, NULL, '2018-01-18 03:41:35', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(185, 'candidate', '', '', '', '', '1970-01-01', 'active', 0, NULL, NULL, '', NULL, '', '', NULL, '2018-01-24 00:20:07', '0000-00-00 00:00:00', NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cv16_user_documents`
--

CREATE TABLE `cv16_user_documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `media_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `published` tinyint(4) DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_user_documents`
--

INSERT INTO `cv16_user_documents` (`id`, `user_id`, `media_id`, `name`, `category_id`, `published`, `created_at`, `updated_at`, `deleted_at`) VALUES
(6, 40, 267, 'sfsf', 1, 0, '2017-12-22 04:51:58', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cv16_user_experiences`
--

CREATE TABLE `cv16_user_experiences` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `industry` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `job_role` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_user_experiences`
--

INSERT INTO `cv16_user_experiences` (`id`, `user_id`, `start_date`, `end_date`, `location`, `company_name`, `industry`, `job_role`, `position`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(5, 3, '2013-10-01', NULL, 'Altham', 'Red Fern Media', NULL, NULL, 'Technical Director', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin consectetur quam eget convallis venenatis. Vestibulum luctus mauris ante, id tristique odio rutrum vitae. Quisque diam sem, elementum id hendrerit nec, fermentum eget neque. Pellentesque nunc leo, luctus vitae nisl quis, consequat lobortis lorem. Ut aliquam at risus eleifend tempor. Morbi in nulla aliquam, dignissim elit sit amet, cursus justo. Quisque sit amet turpis eget nisl interdum elementum vitae nec ligula. Sed at auctor eros. Duis lacinia aliquet urna ac elementum.', '2016-06-13 11:31:54', '2016-07-04 09:34:58', NULL),
(6, 3, '2011-01-01', '2013-10-01', 'Stockport', 'Contact Media', NULL, NULL, 'Web Developer', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin consectetur quam eget convallis venenatis. Vestibulum luctus mauris ante, id tristique odio rutrum vitae. Quisque diam sem, elementum id hendrerit nec, fermentum eget neque. Pellentesque nunc leo, luctus vitae nisl quis, consequat lobortis lorem. Ut aliquam at risus eleifend tempor. Morbi in nulla aliquam, dignissim elit sit amet, cursus justo. Quisque sit amet turpis eget nisl interdum elementum vitae nec ligula. Sed at auctor eros. Duis lacinia aliquet urna ac elementum. Nulla a faucibus leo. Aenean mauris orci, faucibus a imperdiet quis, volutpat ut lacus. Sed euismod elit ut odio feugiat finibus. Vivamus pellentesque vulputate lorem nec consectetur. Ut eget urna sed urna fringilla ultricies.', '2016-06-23 08:16:18', '2016-07-04 09:35:07', NULL),
(7, 40, '2008-01-01', '0000-00-00', 'Preston', 'NCO', NULL, NULL, 'Call Centre Sales', 'I managed both inbound and outbound calls and quickly became the top sales person in the team. I had the highest sales for 6 months continuously which was a record for the department.  ', '2017-01-26 17:02:36', '2017-12-22 01:28:08', NULL),
(8, 40, '2000-01-01', NULL, 'Preston', 'Girls Primary School', NULL, NULL, 'Teacher', 'I was teaching at a girls school in Preston and was part of the development committee that helped the school achieve an outstanding in the Ofsted report. \r\nTeaching is a passion and something I take very personal as I believe these are the most important years in a persons life and helps shape and mould their future. ', '2017-01-26 17:05:57', '2017-12-22 01:28:27', NULL),
(9, 51, '2005-01-01', NULL, 'UK', 'XY', NULL, NULL, 'MD', 'Test', '2017-02-16 16:20:26', '2017-02-16 16:20:26', NULL),
(10, 90, '2016-01-01', '2017-01-01', 'Blackburn', 'JD Sports', NULL, NULL, 'Sales assistant', 'I have been a sales assistant for a year, I have learnt many skills, especially to do with retail as retail management is where I see my future. ', '2017-04-11 11:42:06', '2017-04-11 11:42:06', NULL),
(11, 115, '1997-01-01', NULL, 'Sealdah Railway Station, Bepin Behari Ganguly Street, Raja Bazar, Kolkata, West Bengal, India', 'sad', NULL, NULL, 'sad', 'sd', '2017-06-15 11:26:51', '2017-06-15 11:26:51', NULL),
(12, 116, '2007-10-01', NULL, 'Tychy, Poland', 'tyutyu', NULL, NULL, 'tyu', 'tyutyu', '2017-06-15 11:41:25', '2017-06-15 11:41:25', NULL),
(13, 129, '2013-03-01', '2013-09-01', 'Leyland, United Kingdom', 'CBBQ', NULL, NULL, 'Sales Assistant', 'Test description', '2017-08-22 08:09:11', '2017-08-22 08:09:11', NULL),
(14, 3, '1998-01-01', '2004-03-01', 'testloc', 'testcompany', NULL, NULL, 'asdasd', 'testdesc', '2017-10-20 01:57:39', '0000-00-00 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cv16_user_hobbies`
--

CREATE TABLE `cv16_user_hobbies` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `activity` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_user_hobbies`
--

INSERT INTO `cv16_user_hobbies` (`id`, `user_id`, `activity`, `description`, `created_at`, `updated_at`) VALUES
(13, 3, 'Fencings', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas ornare nisl pellentesque congue porta.', '2016-06-23 08:17:48', '2017-10-20 07:11:55'),
(14, 40, 'Reading', 'I am an avid reader, I like to read anything from novels to thought provoking literature', '2017-01-26 11:57:57', '2017-12-22 02:03:48'),
(15, 40, 'Baking', 'As I am a mum of 2 I enjoy baking for the family. It also helps me unwind from a stressful day at work', '2017-01-26 17:03:29', '2017-01-26 17:03:29'),
(16, 51, 'Cricket', '', '2017-02-16 16:20:54', '2017-02-16 16:20:54'),
(17, 5, 'Hobby 1', 'odsbgjkdsbglkdas fgds g dsobgdafsm gmnasdf gksbdajg dfmnv ', '2017-03-31 08:04:25', '2017-03-31 08:04:25'),
(18, 90, 'Volunteering', 'I am keen to get involved in voluntary work at my local old peoples home and I also help out at the hospice. I recently dressed up in a bear mascot to raise money for the hospice which was fun and rewarding. It has taught me a lot about teamwork and also increased my confidence talking to different people.', '2017-04-11 11:45:54', '2017-04-11 11:45:54'),
(19, 90, 'Scouts', 'I have been in the scouts for many years now. \r\nScouts take part in activities as diverse as kayaking, abseiling, expeditions overseas, photography, climbing and zorbing. As a Scout you can learn survival skills, first aid, computer programming, or even flying a plane. I haven\'t ever done that, or skydiving! I want to do both before I am 30. \r\nI have made lots of friends and it has kept me very active especially during the school holidays.', '2017-04-11 11:48:18', '2017-04-11 11:48:18'),
(20, 115, 'Lorem Ipsum', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2017-06-15 11:26:12', '2017-06-15 11:26:12'),
(21, 116, 'ert', 'erterter', '2017-06-15 11:41:41', '2017-06-15 11:41:41'),
(22, 129, 'Gym', 'Test Description', '2017-08-22 08:14:14', '2017-08-22 08:14:14');

-- --------------------------------------------------------

--
-- Table structure for table `cv16_user_languages`
--

CREATE TABLE `cv16_user_languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `proficiency` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_user_languages`
--

INSERT INTO `cv16_user_languages` (`id`, `user_id`, `name`, `proficiency`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 24, 'Arabic (MSA)', 'Intermediate', '2016-10-02 21:54:43', '2016-10-02 21:54:43', NULL),
(2, 24, 'Persian', 'Basic', '2016-10-02 21:54:55', '2016-10-02 21:54:55', NULL),
(3, 24, 'English', 'Expert', '2016-10-02 21:55:04', '2016-10-02 21:55:04', NULL),
(4, 30, 'English', 'Expert', '2017-01-17 16:24:10', '2017-01-17 16:24:10', NULL),
(5, 30, 'Arabic', 'Basic', '2017-01-17 16:24:18', '2017-01-17 16:24:18', NULL),
(6, 30, 'Urdu', 'Intermediate', '2017-01-17 16:24:27', '2017-01-17 16:24:27', NULL),
(7, 40, 'English', 'Expert', '2017-01-26 16:36:12', '2017-01-26 16:36:12', NULL),
(9, 40, 'Arabic', 'Basic', '2017-01-26 16:36:42', '2017-01-26 16:36:42', NULL),
(10, 51, 'Arabic', 'Basic', '2017-02-16 16:20:47', '2017-02-16 16:20:47', NULL),
(11, 90, 'English', 'Expert', '2017-04-11 11:48:32', '2017-04-11 11:48:32', NULL),
(12, 90, 'French', 'Basic', '2017-04-11 11:48:42', '2017-04-11 11:48:42', NULL),
(13, 91, 'French', 'Intermediate', '2017-05-20 12:46:32', '2017-05-20 12:46:32', NULL),
(14, 91, 'German', 'Expert', '2017-05-20 12:46:41', '2017-05-20 12:46:41', NULL),
(15, 115, 'english', 'Expert', '2017-06-15 11:25:35', '2017-06-15 11:25:35', NULL),
(16, 116, 'rtert', 'Expert', '2017-06-15 11:41:46', '2017-06-15 11:41:46', NULL),
(17, 116, 'ytutyi', 'Expert', '2017-06-15 11:41:54', '2017-06-15 11:41:54', NULL),
(18, 129, 'English', 'Expert', '2017-08-22 08:14:46', '2017-08-22 08:14:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cv16_user_qualifications`
--

CREATE TABLE `cv16_user_qualifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `institution` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `field` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `completion_date` date DEFAULT NULL,
  `grade` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_user_qualifications`
--

INSERT INTO `cv16_user_qualifications` (`id`, `user_id`, `institution`, `location`, `field`, `level`, `completion_date`, `grade`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(2, 3, 'University of Central Lancashire', 'Preston', 'Computer Science', 'Undergraduate', '2010-06-01', '1st Class', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin consectetur quam eget convallis venenatis. Vestibulum luctus mauris ante, id tristique odio rutrum vitae. Quisque diam sem, elementum id hendrerit nec, fermentum eget neque. Pellentesque nunc leo, luctus vitae nisl quis, consequat lobortis lorem.', '2016-06-13 11:26:52', '2016-07-04 09:35:19', NULL),
(3, 3, 'St Marys College', 'Blackburn', 'Maths', 'A Levels', '2007-07-01', 'B', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin consectetur quam eget convallis venenatis. Vestibulum luctus mauris ante, id tristique odio rutrum vitae. Quisque diam sem, elementum id hendrerit nec, fermentum eget neque. Pellentesque nunc leo, luctus vitae nisl quis, consequat lobortis lorem.', '2016-06-23 08:17:20', '2016-07-04 09:35:23', NULL),
(4, 51, 'Greenwich University', 'London', 'Engineering', 'Postgraduate', '2002-08-01', '', '', '2017-02-16 16:21:24', '2017-02-16 16:21:24', NULL),
(5, 40, 'Blackburn University', 'Blackburn', 'English PGCE', 'Postgraduate', '2010-03-01', '1st Class', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin consectetur quam eget convallis venenatis. Vestibulum luctus mauris ante, id tristique odio rutrum vitae. Quisque diam sem, elementum id hendrerit nec, fermentum eget neque. Pellentesque nunc leo, luctus vitae nisl quis, consequat lobortis lorem.', '2017-03-17 09:06:12', '2017-12-22 01:50:19', NULL),
(6, 90, 'School of CVVID', 'Blackburn', 'Maths', 'GCSE', '2016-06-01', 'B', 'Passed with B in Maths ', '2017-04-11 11:37:40', '2017-04-11 11:37:40', NULL),
(7, 90, 'School of CVVID', 'Blackburn', 'French', 'GCSE', '2016-06-01', 'A*', 'Only person to get an A* in French from my school', '2017-04-11 11:38:30', '2017-04-11 11:38:30', NULL),
(8, 115, 'wdf', 'SDF Bus Stop, GP Block, Kolkata, West Bengal, India', 'sdfsd', 'GCSE', '2017-06-01', 'sdf', 'sdf', '2017-06-15 11:26:26', '2017-06-15 11:26:26', NULL),
(9, 116, 'rt', 'rtr', 'tret', 'GCSE', '2017-06-01', 'ret', 'rete', '2017-06-15 11:41:35', '2017-06-15 11:41:35', NULL),
(10, 129, 'University of Central Lancashire', 'Preston, United Kingdom', 'Marketing and Advertising', 'Undergraduate', '2017-07-01', 'First class honours', 'Test Description', '2017-08-22 08:12:49', '2017-08-22 08:12:49', NULL),
(15, 3, 'testist123', 'df', 'fafaf', 'GCSE', '1957-03-01', 'aa', 'agfgag', '2017-10-20 06:27:07', '2017-10-20 06:27:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cv16_videos`
--

CREATE TABLE `cv16_videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `video_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `is_processed` int(11) NOT NULL DEFAULT '0',
  `state` tinyint(4) NOT NULL DEFAULT '1',
  `status` varchar(100) COLLATE utf8_unicode_ci DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `ordering` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_videos`
--

INSERT INTO `cv16_videos` (`id`, `model_type`, `model_id`, `video_id`, `name`, `duration`, `category_id`, `is_processed`, `state`, `status`, `created_at`, `updated_at`, `deleted_at`, `ordering`) VALUES
(2, 'App\\Users\\Admin', 2, '169239600', 'Alwyn David - Engineer', 198, 2, 1, 1, 'active', '2016-06-09 10:04:45', '2016-06-09 11:53:16', NULL, 0),
(3, 'App\\Users\\Admin', 2, '169239725', 'Michael Grimmond - Gardener', 120, 2, 1, 1, 'active', '2016-06-09 10:08:54', '2016-06-09 11:53:51', NULL, 1),
(4, 'App\\Users\\Admin', 2, '169258609', 'Anita - Fitness Instructor', 135, 2, 1, 1, 'active', '2016-06-09 10:15:29', '2016-06-09 11:54:05', NULL, 6),
(5, 'App\\Users\\Admin', 2, '169258813', 'Ben - Anthropologist', 164, 2, 1, 1, 'active', '2016-06-09 10:24:04', '2016-06-09 11:54:20', NULL, 5),
(6, 'App\\Users\\Admin', 2, '169259539', 'Myles - Plumber', 201, 2, 1, 1, 'active', '2016-06-09 13:25:01', '2016-06-09 13:25:03', NULL, 3),
(7, 'App\\Users\\Admin', 2, '169259515', 'Annabel - Law Graduate', 211, 2, 1, 1, 'active', '2016-06-09 13:25:34', '2016-06-09 13:25:35', NULL, 4),
(8, 'App\\Users\\Admin', 2, '169696984', 'Zeynep - Film-maker', 241, 2, 1, 1, 'active', '2016-06-09 13:25:58', '2016-06-09 13:26:00', NULL, 2),
(9, 'App\\Users\\Admin', 2, '169696886', 'Dr Rabeah - Virologist', 273, 2, 1, 1, 'active', '2016-06-09 13:26:13', '2016-06-20 12:01:30', NULL, 7),
(10, 'App\\Users\\Admin', 2, '170023187', 'How to INTRO', 75, 3, 1, 1, 'active', '2016-06-09 13:41:41', '2016-06-09 13:41:42', NULL, 0),
(11, 'App\\Users\\Admin', 2, '170023203', 'How to RULE NO 1', 23, 3, 1, 1, 'active', '2016-06-09 13:42:38', '2016-06-09 13:42:39', NULL, 0),
(12, 'App\\Users\\Admin', 2, '170023204', 'How to RULE NO 2', 23, 3, 1, 1, 'active', '2016-06-09 13:43:01', '2016-06-09 13:43:02', NULL, 0),
(13, 'App\\Users\\Admin', 2, '170023207', 'How to RULE NO 3', 41, 3, 1, 1, 'active', '2016-06-09 13:43:15', '2016-06-09 13:43:16', NULL, 0),
(14, 'App\\Users\\Admin', 2, '170023205', 'How to RULE NO 4', 26, 3, 1, 1, 'active', '2016-06-09 13:43:30', '2016-06-09 13:43:31', NULL, 0),
(15, 'App\\Users\\Admin', 2, '170023206', 'How to RULE NO 5', 30, 3, 1, 1, 'active', '2016-06-09 13:43:48', '2016-06-09 13:43:49', NULL, 0),
(22, 'App\\Users\\Candidate', 3, '202364635', 'Test Video', 17, 1, 1, 1, 'active', '2017-02-03 11:26:11', '2017-02-03 11:30:11', NULL, 0),
(23, 'App\\Users\\Candidate', 5, '208807315', 'Alex Heeney', 30, 1, 1, 1, 'active', '2017-03-17 10:51:21', '2017-03-17 10:55:06', NULL, 0),
(24, 'App\\Users\\Candidate', 3, '208811410', 'Crown', 71, 1, 1, 1, 'active', '2017-03-17 11:25:15', '2017-03-17 11:30:05', NULL, 0),
(26, 'App\\Users\\Candidate', 91, '218275285', 'Seantest2', 160, 1, 1, 1, 'active', '2017-05-20 12:35:01', '2017-05-20 12:40:05', NULL, 0),
(30, 'App\\Education\\InstitutionAdmin', 87, '218440160', 'Test Video', 10, 1, 1, 1, 'active', '2017-05-22 09:02:23', '2017-05-22 09:05:06', NULL, 0),
(31, 'App\\Education\\InstitutionAdmin', 87, '218440523', 'Porsche 1', 17, 1, 1, 1, 'active', '2017-05-22 09:05:19', '2017-05-22 09:10:04', NULL, 0),
(32, 'App\\Education\\InstitutionAdmin', 87, '218440948', 'Video Paul', 19, 1, 1, 1, 'active', '2017-05-22 09:09:35', '2017-05-22 09:15:05', NULL, 0),
(37, 'App\\Users\\Candidate', 5, '220664694', 'TESTING VIDEO', 13, 1, 1, 1, 'active', '2017-06-07 15:28:23', '2017-06-07 15:35:15', NULL, 0),
(38, 'App\\Users\\Candidate', 5, '220762497', 'Large Video', 170, 1, 1, 1, 'active', '2017-06-08 06:37:01', '2017-06-08 06:45:16', NULL, 0),
(39, 'App\\Users\\Candidate', 116, '221734562', '1495105976_checked_checkbox', NULL, 1, 0, 1, 'active', '2017-06-15 11:39:59', '2017-06-15 11:40:08', NULL, 0),
(40, 'App\\Education\\Teacher', 71, '225219570', 'Ayah Shah Video CV', 92, 1, 1, 1, 'active', '2017-07-12 07:50:09', '2017-07-12 08:00:06', NULL, 0),
(41, 'App\\Education\\Teacher', 71, '225219587', 'Sara Ziglam Video CV', 104, 1, 1, 1, 'active', '2017-07-12 07:52:39', '2017-07-12 08:00:08', NULL, 0),
(42, 'App\\Education\\Teacher', 71, '225219593', 'Arifa Patel Video CV', 100, 1, 1, 1, 'active', '2017-07-12 07:54:55', '2017-07-12 08:00:10', NULL, 0),
(44, 'App\\Education\\Teacher', 71, '225220986', 'Maryam Patel Video CV', 152, 1, 1, 1, 'active', '2017-07-12 08:05:41', '2017-07-12 08:15:05', NULL, 0),
(45, 'App\\Education\\Teacher', 71, '225220997', 'Maryam Sharief Video CV', 100, 1, 1, 1, 'active', '2017-07-12 08:07:52', '2017-07-12 08:15:07', NULL, 0),
(49, 'App\\Education\\Teacher', 71, '225229639', 'Hafsah Patel Video CV', 128, 1, 1, 1, 'active', '2017-07-12 09:39:29', '2017-07-12 09:45:05', NULL, 0),
(50, 'App\\Education\\Teacher', 71, '225230083', 'Aisha Daya', 93, 1, 1, 1, 'active', '2017-07-12 09:42:38', '2017-07-12 09:50:05', NULL, 0),
(51, 'App\\Education\\Teacher', 71, '225230578', 'Aaiza Patel', 137, 1, 1, 1, 'active', '2017-07-12 09:45:09', '2017-07-12 09:55:05', NULL, 0),
(54, 'App\\Users\\Candidate', 91, '225367134', 'shutterstock_v2858227', 11, 1, 1, 1, 'active', '2017-07-13 06:06:23', '2017-07-13 06:15:06', NULL, 0),
(55, 'App\\Users\\Candidate', 119, '227221009', 'VB SYMPHONY', 75, 1, 1, 1, 'active', '2017-07-27 06:47:50', '2017-07-27 06:55:05', NULL, 0),
(58, 'App\\Users\\Candidate', 138, '233442834', 'humster', 14, 1, 1, 1, 'active', '2017-09-12 06:23:20', '2017-09-12 06:30:07', NULL, 0),
(59, 'candidate', 40, '248429423', 'SS', 0, 1, 1, 1, 'active', '2017-12-22 02:37:08', '0000-00-00 00:00:00', NULL, 0),
(60, 'candidate', 40, '248429836', 'undefined', 0, 1, 1, 1, 'active', '2017-12-22 02:44:00', '0000-00-00 00:00:00', NULL, 0),
(61, 'candidate', 40, '248430863', 'undefined', 0, 1, 1, 1, 'active', '2017-12-22 02:56:57', '0000-00-00 00:00:00', NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `cv16_video_categories`
--

CREATE TABLE `cv16_video_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cv16_video_categories`
--

INSERT INTO `cv16_video_categories` (`id`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Profile', 'profile', '2016-06-09 11:28:58', '2016-06-09 11:28:58'),
(2, 'Success Stories', 'success-stories', '2016-06-09 11:29:08', '2016-06-09 11:29:08'),
(3, 'How To', 'how-to', '2016-06-09 11:29:18', '2016-06-09 11:29:18');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cv16_activities`
--
ALTER TABLE `cv16_activities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `activities_user_id_foreign` (`user_id`),
  ADD KEY `activities_model_id_index` (`model_id`);

--
-- Indexes for table `cv16_addresses`
--
ALTER TABLE `cv16_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `addresses_model_id_index` (`model_id`),
  ADD KEY `addresses_model_type_index` (`model_type`);

--
-- Indexes for table `cv16_conversations`
--
ALTER TABLE `cv16_conversations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cv16_conversation_members`
--
ALTER TABLE `cv16_conversation_members`
  ADD PRIMARY KEY (`id`),
  ADD KEY `conversation_members_conversation_id_index` (`conversation_id`),
  ADD KEY `conversation_members_user_id_index` (`user_id`);

--
-- Indexes for table `cv16_conversation_messages`
--
ALTER TABLE `cv16_conversation_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `conversation_messages_conversation_id_index` (`conversation_id`),
  ADD KEY `conversation_messages_user_id_index` (`user_id`);

--
-- Indexes for table `cv16_employers`
--
ALTER TABLE `cv16_employers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employers_user_id_index` (`user_id`);

--
-- Indexes for table `cv16_employer_users`
--
ALTER TABLE `cv16_employer_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employer_users_employer_id_index` (`employer_id`),
  ADD KEY `employer_users_user_id_index` (`user_id`);

--
-- Indexes for table `cv16_industries`
--
ALTER TABLE `cv16_industries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cv16_institutions`
--
ALTER TABLE `cv16_institutions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `institutions_admin_id_index` (`admin_id`);

--
-- Indexes for table `cv16_institution_users`
--
ALTER TABLE `cv16_institution_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `institution_users_institution_id_index` (`institution_id`),
  ADD KEY `institution_users_user_id_index` (`user_id`);

--
-- Indexes for table `cv16_jobs`
--
ALTER TABLE `cv16_jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_owner_id_index` (`owner_id`),
  ADD KEY `jobs_user_id_index` (`user_id`),
  ADD KEY `jobs_industry_id_index` (`industry_id`),
  ADD KEY `jobs_skill_category_id_index` (`skill_category_id`),
  ADD KEY `jobs_successful_applicant_id_foreign` (`successful_applicant_id`);

--
-- Indexes for table `cv16_job_applications`
--
ALTER TABLE `cv16_job_applications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_applications_job_id_foreign` (`job_id`),
  ADD KEY `job_applications_user_id_foreign` (`user_id`);

--
-- Indexes for table `cv16_job_skill`
--
ALTER TABLE `cv16_job_skill`
  ADD KEY `job_skill_job_id_index` (`job_id`),
  ADD KEY `job_skill_skill_id_index` (`skill_id`);

--
-- Indexes for table `cv16_job_views`
--
ALTER TABLE `cv16_job_views`
  ADD PRIMARY KEY (`id`),
  ADD KEY `job_views_job_id_index` (`job_id`),
  ADD KEY `job_views_model_id_index` (`model_id`);

--
-- Indexes for table `cv16_media`
--
ALTER TABLE `cv16_media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cv16_media_categories`
--
ALTER TABLE `cv16_media_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cv16_pages`
--
ALTER TABLE `cv16_pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`),
  ADD KEY `pages_user_id_index` (`user_id`);

--
-- Indexes for table `cv16_password_resets`
--
ALTER TABLE `cv16_password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `cv16_permissions`
--
ALTER TABLE `cv16_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cv16_permission_role`
--
ALTER TABLE `cv16_permission_role`
  ADD KEY `permission_role_role_id_index` (`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`);

--
-- Indexes for table `cv16_plans`
--
ALTER TABLE `cv16_plans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `plans_owner_id_index` (`owner_id`);

--
-- Indexes for table `cv16_profiles`
--
ALTER TABLE `cv16_profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profiles_owner_id_index` (`owner_id`),
  ADD KEY `profiles_industry_id_index` (`industry_id`),
  ADD KEY `profiles_photo_id_index` (`photo_id`),
  ADD KEY `profiles_cover_id_index` (`cover_id`);

--
-- Indexes for table `cv16_profile_documents`
--
ALTER TABLE `cv16_profile_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profile_documents_profile_id_index` (`profile_id`),
  ADD KEY `profile_documents_user_document_id_index` (`user_document_id`);

--
-- Indexes for table `cv16_profile_experiences`
--
ALTER TABLE `cv16_profile_experiences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profile_experiences_profile_id_index` (`profile_id`),
  ADD KEY `profile_experiences_user_experience_id_index` (`user_experience_id`);

--
-- Indexes for table `cv16_profile_favourites`
--
ALTER TABLE `cv16_profile_favourites`
  ADD KEY `profile_favourites_profile_id_index` (`profile_id`),
  ADD KEY `profile_favourites_favourite_id_index` (`favourite_id`);

--
-- Indexes for table `cv16_profile_hobbies`
--
ALTER TABLE `cv16_profile_hobbies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profile_hobbies_profile_id_index` (`profile_id`),
  ADD KEY `profile_hobbies_user_hobby_id_index` (`user_hobby_id`);

--
-- Indexes for table `cv16_profile_languages`
--
ALTER TABLE `cv16_profile_languages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profile_languages_profile_id_index` (`profile_id`),
  ADD KEY `profile_languages_user_language_id_index` (`user_language_id`);

--
-- Indexes for table `cv16_profile_qualifications`
--
ALTER TABLE `cv16_profile_qualifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profile_qualifications_profile_id_index` (`profile_id`),
  ADD KEY `profile_qualifications_user_qualification_id_index` (`user_qualification_id`);

--
-- Indexes for table `cv16_profile_skills`
--
ALTER TABLE `cv16_profile_skills`
  ADD KEY `profile_skills_profile_id_index` (`profile_id`),
  ADD KEY `profile_skills_skill_id_index` (`skill_id`);

--
-- Indexes for table `cv16_profile_videos`
--
ALTER TABLE `cv16_profile_videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profile_videos_profile_id_index` (`profile_id`),
  ADD KEY `profile_videos_video_id_index` (`video_id`);

--
-- Indexes for table `cv16_profile_views`
--
ALTER TABLE `cv16_profile_views`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profile_views_profile_id_index` (`profile_id`),
  ADD KEY `profile_views_model_id_index` (`model_id`);

--
-- Indexes for table `cv16_queued_jobs`
--
ALTER TABLE `cv16_queued_jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `queued_jobs_queue_reserved_reserved_at_index` (`queue`,`reserved`,`reserved_at`);

--
-- Indexes for table `cv16_roles`
--
ALTER TABLE `cv16_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cv16_role_user`
--
ALTER TABLE `cv16_role_user`
  ADD KEY `role_user_user_id_index` (`user_id`),
  ADD KEY `role_user_role_id_index` (`role_id`);

--
-- Indexes for table `cv16_sites`
--
ALTER TABLE `cv16_sites`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cv16_skills`
--
ALTER TABLE `cv16_skills`
  ADD PRIMARY KEY (`id`),
  ADD KEY `skills_skill_category_id_index` (`skill_category_id`);

--
-- Indexes for table `cv16_skill_categories`
--
ALTER TABLE `cv16_skill_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cv16_slideshows`
--
ALTER TABLE `cv16_slideshows`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slideshows_slug_unique` (`slug`),
  ADD KEY `slideshows_user_id_index` (`user_id`),
  ADD KEY `slideshows_background_id_foreign` (`background_id`);

--
-- Indexes for table `cv16_slideshow_slides`
--
ALTER TABLE `cv16_slideshow_slides`
  ADD PRIMARY KEY (`id`),
  ADD KEY `slideshow_slides_slideshow_id_foreign` (`slideshow_id`),
  ADD KEY `slideshow_slides_user_id_index` (`user_id`),
  ADD KEY `slideshow_slides_media_id_index` (`media_id`);

--
-- Indexes for table `cv16_tutor_groups`
--
ALTER TABLE `cv16_tutor_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tutor_groups_institution_id_index` (`institution_id`),
  ADD KEY `tutor_groups_teacher_id_index` (`teacher_id`);

--
-- Indexes for table `cv16_tutor_groups_students`
--
ALTER TABLE `cv16_tutor_groups_students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tutor_groups_students_institution_id_index` (`institution_id`),
  ADD KEY `tutor_groups_students_tutor_group_id_index` (`tutor_group_id`),
  ADD KEY `tutor_groups_students_student_id_index` (`student_id`);

--
-- Indexes for table `cv16_users`
--
ALTER TABLE `cv16_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `cv16_user_documents`
--
ALTER TABLE `cv16_user_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_documents_user_id_index` (`user_id`),
  ADD KEY `user_documents_media_id_index` (`media_id`),
  ADD KEY `user_documents_category_id_index` (`category_id`);

--
-- Indexes for table `cv16_user_experiences`
--
ALTER TABLE `cv16_user_experiences`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_experiences_user_id_index` (`user_id`);

--
-- Indexes for table `cv16_user_hobbies`
--
ALTER TABLE `cv16_user_hobbies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_hobbies_user_id_index` (`user_id`);

--
-- Indexes for table `cv16_user_languages`
--
ALTER TABLE `cv16_user_languages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_languages_user_id_index` (`user_id`);

--
-- Indexes for table `cv16_user_qualifications`
--
ALTER TABLE `cv16_user_qualifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_qualifications_user_id_index` (`user_id`);

--
-- Indexes for table `cv16_videos`
--
ALTER TABLE `cv16_videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `videos_category_id_foreign` (`category_id`);

--
-- Indexes for table `cv16_video_categories`
--
ALTER TABLE `cv16_video_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `video_categories_slug_unique` (`slug`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cv16_activities`
--
ALTER TABLE `cv16_activities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;
--
-- AUTO_INCREMENT for table `cv16_addresses`
--
ALTER TABLE `cv16_addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT for table `cv16_conversations`
--
ALTER TABLE `cv16_conversations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `cv16_conversation_members`
--
ALTER TABLE `cv16_conversation_members`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `cv16_conversation_messages`
--
ALTER TABLE `cv16_conversation_messages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `cv16_employers`
--
ALTER TABLE `cv16_employers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `cv16_employer_users`
--
ALTER TABLE `cv16_employer_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `cv16_industries`
--
ALTER TABLE `cv16_industries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;
--
-- AUTO_INCREMENT for table `cv16_institutions`
--
ALTER TABLE `cv16_institutions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `cv16_institution_users`
--
ALTER TABLE `cv16_institution_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `cv16_jobs`
--
ALTER TABLE `cv16_jobs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `cv16_job_applications`
--
ALTER TABLE `cv16_job_applications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `cv16_job_views`
--
ALTER TABLE `cv16_job_views`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `cv16_media`
--
ALTER TABLE `cv16_media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=272;
--
-- AUTO_INCREMENT for table `cv16_media_categories`
--
ALTER TABLE `cv16_media_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `cv16_pages`
--
ALTER TABLE `cv16_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `cv16_permissions`
--
ALTER TABLE `cv16_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cv16_plans`
--
ALTER TABLE `cv16_plans`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `cv16_profiles`
--
ALTER TABLE `cv16_profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;
--
-- AUTO_INCREMENT for table `cv16_profile_documents`
--
ALTER TABLE `cv16_profile_documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `cv16_profile_experiences`
--
ALTER TABLE `cv16_profile_experiences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `cv16_profile_hobbies`
--
ALTER TABLE `cv16_profile_hobbies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `cv16_profile_languages`
--
ALTER TABLE `cv16_profile_languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `cv16_profile_qualifications`
--
ALTER TABLE `cv16_profile_qualifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `cv16_profile_videos`
--
ALTER TABLE `cv16_profile_videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `cv16_profile_views`
--
ALTER TABLE `cv16_profile_views`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `cv16_queued_jobs`
--
ALTER TABLE `cv16_queued_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cv16_roles`
--
ALTER TABLE `cv16_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cv16_sites`
--
ALTER TABLE `cv16_sites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cv16_skills`
--
ALTER TABLE `cv16_skills`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=362;
--
-- AUTO_INCREMENT for table `cv16_skill_categories`
--
ALTER TABLE `cv16_skill_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `cv16_slideshows`
--
ALTER TABLE `cv16_slideshows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `cv16_slideshow_slides`
--
ALTER TABLE `cv16_slideshow_slides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `cv16_tutor_groups`
--
ALTER TABLE `cv16_tutor_groups`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `cv16_tutor_groups_students`
--
ALTER TABLE `cv16_tutor_groups_students`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `cv16_users`
--
ALTER TABLE `cv16_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=188;
--
-- AUTO_INCREMENT for table `cv16_user_documents`
--
ALTER TABLE `cv16_user_documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `cv16_user_experiences`
--
ALTER TABLE `cv16_user_experiences`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `cv16_user_hobbies`
--
ALTER TABLE `cv16_user_hobbies`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `cv16_user_languages`
--
ALTER TABLE `cv16_user_languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `cv16_user_qualifications`
--
ALTER TABLE `cv16_user_qualifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `cv16_videos`
--
ALTER TABLE `cv16_videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `cv16_video_categories`
--
ALTER TABLE `cv16_video_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `cv16_activities`
--
ALTER TABLE `cv16_activities`
  ADD CONSTRAINT `activities_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `cv16_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cv16_employers`
--
ALTER TABLE `cv16_employers`
  ADD CONSTRAINT `employers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `cv16_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `cv16_employer_users`
--
ALTER TABLE `cv16_employer_users`
  ADD CONSTRAINT `employer_users_employer_id_foreign` FOREIGN KEY (`employer_id`) REFERENCES `cv16_employers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `employer_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `cv16_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cv16_institution_users`
--
ALTER TABLE `cv16_institution_users`
  ADD CONSTRAINT `institution_users_institution_id_foreign` FOREIGN KEY (`institution_id`) REFERENCES `cv16_institutions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `institution_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `cv16_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cv16_jobs`
--
ALTER TABLE `cv16_jobs`
  ADD CONSTRAINT `jobs_industry_id_foreign` FOREIGN KEY (`industry_id`) REFERENCES `cv16_industries` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `jobs_skill_category_id_foreign` FOREIGN KEY (`skill_category_id`) REFERENCES `cv16_skill_categories` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `jobs_successful_applicant_id_foreign` FOREIGN KEY (`successful_applicant_id`) REFERENCES `cv16_users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `jobs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `cv16_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `cv16_job_applications`
--
ALTER TABLE `cv16_job_applications`
  ADD CONSTRAINT `job_applications_job_id_foreign` FOREIGN KEY (`job_id`) REFERENCES `cv16_jobs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `job_applications_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `cv16_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cv16_job_skill`
--
ALTER TABLE `cv16_job_skill`
  ADD CONSTRAINT `job_skill_job_id_foreign` FOREIGN KEY (`job_id`) REFERENCES `cv16_jobs` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `job_skill_skill_id_foreign` FOREIGN KEY (`skill_id`) REFERENCES `cv16_skills` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cv16_job_views`
--
ALTER TABLE `cv16_job_views`
  ADD CONSTRAINT `job_views_job_id_foreign` FOREIGN KEY (`job_id`) REFERENCES `cv16_jobs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cv16_profiles`
--
ALTER TABLE `cv16_profiles`
  ADD CONSTRAINT `profiles_cover_id_foreign` FOREIGN KEY (`cover_id`) REFERENCES `cv16_media` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `profiles_industry_id_foreign` FOREIGN KEY (`industry_id`) REFERENCES `cv16_industries` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `profiles_photo_id_foreign` FOREIGN KEY (`photo_id`) REFERENCES `cv16_media` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `cv16_profile_documents`
--
ALTER TABLE `cv16_profile_documents`
  ADD CONSTRAINT `profile_documents_profile_id_foreign` FOREIGN KEY (`profile_id`) REFERENCES `cv16_profiles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `profile_documents_user_document_id_foreign` FOREIGN KEY (`user_document_id`) REFERENCES `cv16_user_documents` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cv16_profile_experiences`
--
ALTER TABLE `cv16_profile_experiences`
  ADD CONSTRAINT `profile_experiences_profile_id_foreign` FOREIGN KEY (`profile_id`) REFERENCES `cv16_profiles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `profile_experiences_user_experience_id_foreign` FOREIGN KEY (`user_experience_id`) REFERENCES `cv16_user_experiences` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cv16_profile_favourites`
--
ALTER TABLE `cv16_profile_favourites`
  ADD CONSTRAINT `profile_favourites_favourite_id_foreign` FOREIGN KEY (`favourite_id`) REFERENCES `cv16_profiles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `profile_favourites_profile_id_foreign` FOREIGN KEY (`profile_id`) REFERENCES `cv16_profiles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cv16_profile_hobbies`
--
ALTER TABLE `cv16_profile_hobbies`
  ADD CONSTRAINT `profile_hobbies_profile_id_foreign` FOREIGN KEY (`profile_id`) REFERENCES `cv16_profiles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `profile_hobbies_user_hobby_id_foreign` FOREIGN KEY (`user_hobby_id`) REFERENCES `cv16_user_hobbies` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cv16_profile_languages`
--
ALTER TABLE `cv16_profile_languages`
  ADD CONSTRAINT `profile_languages_profile_id_foreign` FOREIGN KEY (`profile_id`) REFERENCES `cv16_profiles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `profile_languages_user_language_id_foreign` FOREIGN KEY (`user_language_id`) REFERENCES `cv16_user_languages` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cv16_profile_qualifications`
--
ALTER TABLE `cv16_profile_qualifications`
  ADD CONSTRAINT `profile_qualifications_profile_id_foreign` FOREIGN KEY (`profile_id`) REFERENCES `cv16_profiles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `profile_qualifications_user_qualification_id_foreign` FOREIGN KEY (`user_qualification_id`) REFERENCES `cv16_user_qualifications` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cv16_profile_skills`
--
ALTER TABLE `cv16_profile_skills`
  ADD CONSTRAINT `profile_skills_profile_id_foreign` FOREIGN KEY (`profile_id`) REFERENCES `cv16_profiles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `profile_skills_skill_id_foreign` FOREIGN KEY (`skill_id`) REFERENCES `cv16_skills` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cv16_profile_videos`
--
ALTER TABLE `cv16_profile_videos`
  ADD CONSTRAINT `profile_videos_profile_id_foreign` FOREIGN KEY (`profile_id`) REFERENCES `cv16_profiles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `profile_videos_video_id_foreign` FOREIGN KEY (`video_id`) REFERENCES `cv16_videos` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cv16_profile_views`
--
ALTER TABLE `cv16_profile_views`
  ADD CONSTRAINT `profile_views_profile_id_foreign` FOREIGN KEY (`profile_id`) REFERENCES `cv16_profiles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cv16_slideshows`
--
ALTER TABLE `cv16_slideshows`
  ADD CONSTRAINT `slideshows_background_id_foreign` FOREIGN KEY (`background_id`) REFERENCES `cv16_media` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `cv16_slideshow_slides`
--
ALTER TABLE `cv16_slideshow_slides`
  ADD CONSTRAINT `slideshow_slides_media_id_foreign` FOREIGN KEY (`media_id`) REFERENCES `cv16_media` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `slideshow_slides_slideshow_id_foreign` FOREIGN KEY (`slideshow_id`) REFERENCES `cv16_slideshows` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cv16_tutor_groups`
--
ALTER TABLE `cv16_tutor_groups`
  ADD CONSTRAINT `tutor_groups_institution_id_foreign` FOREIGN KEY (`institution_id`) REFERENCES `cv16_institutions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tutor_groups_teacher_id_foreign` FOREIGN KEY (`teacher_id`) REFERENCES `cv16_users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `cv16_tutor_groups_students`
--
ALTER TABLE `cv16_tutor_groups_students`
  ADD CONSTRAINT `tutor_groups_students_institution_id_foreign` FOREIGN KEY (`institution_id`) REFERENCES `cv16_institutions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tutor_groups_students_student_id_foreign` FOREIGN KEY (`student_id`) REFERENCES `cv16_users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tutor_groups_students_tutor_group_id_foreign` FOREIGN KEY (`tutor_group_id`) REFERENCES `cv16_tutor_groups` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cv16_user_documents`
--
ALTER TABLE `cv16_user_documents`
  ADD CONSTRAINT `user_documents_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `cv16_media_categories` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `user_documents_media_id_foreign` FOREIGN KEY (`media_id`) REFERENCES `cv16_media` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_documents_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `cv16_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cv16_user_experiences`
--
ALTER TABLE `cv16_user_experiences`
  ADD CONSTRAINT `user_experiences_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `cv16_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cv16_user_hobbies`
--
ALTER TABLE `cv16_user_hobbies`
  ADD CONSTRAINT `user_hobbies_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `cv16_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cv16_user_languages`
--
ALTER TABLE `cv16_user_languages`
  ADD CONSTRAINT `user_languages_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `cv16_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cv16_user_qualifications`
--
ALTER TABLE `cv16_user_qualifications`
  ADD CONSTRAINT `user_qualifications_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `cv16_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cv16_videos`
--
ALTER TABLE `cv16_videos`
  ADD CONSTRAINT `videos_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `cv16_video_categories` (`id`) ON DELETE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
